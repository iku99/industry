/**
 * @format
 */
import React from 'react';
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import store from './src/stores';
import {Provider} from 'react-redux';
import RoutesClass from './src/components/RoutesClass';

const App = () => (
  <Provider store={store}>
    <RoutesClass />
  </Provider>
);

AppRegistry.registerComponent(appName, () => App);
