import {Dimensions, Platform} from 'react-native';
import FastImage from 'react-native-fast-image';
import {strings} from './languages/i18n';
import GlobalUtil from '../Utils/Common/GlobalUtil';
import Styles from "./Styles";
const isTablet = GlobalUtil.isTablet();
const columnNumber = isTablet ? 3 : 2;
const width=Styles.constants.X*4;
const ANIMATION_DELAY =
  require('react-native').Platform.OS === 'android' ? 300 : 600;
export default {
  LOG_TAG: 'megastore:',
  avatarKey: 'AVATAR_KEY',
  IS_IOS: Platform.OS === 'ios',
  columnNumber: columnNumber,
  RESULT_CUSTOMER_SALE: 159,
  DATE_FORMAT: 'DD-MM-YYYY',
  isTablet: isTablet,
  LOCAL_SIGNAL: '[LOCAL]',
  IMG_ONL:'https:',
  ENCRYPTION_K: 'RmDigitalBanking',
  ACCESS_ENCRYPTION: 'ACCESS_ENCRYPTION',
  USER_ENCRYPTION: 'USER_ENCRYPTION',
  ALLOW_FINGERPRINT: 'ALLOW_FINGERPRINT',
  CAMPAIGN_COUNT: 'CAMPAIGN_COUNT',
  NOTIFICATION_COUNT: 'NOTIFICATION_COUNT',
  FIREBASE_TOKEN: 'FIREBASE_TOKEN',
  INDEX_TABLE: 1,
  SALE_TABLE: 2,
  PLATFORM: require('react-native').Platform.OS,
  ANIMATION_DELAY: ANIMATION_DELAY,
  INITIAL_VECTOR: '0104201931042019',
  BORDER_ITEM_RADIUS: 5,
  VERTICAL_TYPE: 1,
  HORIZONTAL_TYPE: 2,
  FLOAT_BUTTON_SIZE: 46,
  defaultViewedProductItemWidth:width,
  LABEL_SIZE: 13,
  AVATAR_SIZE: 160,
  LIMIT: 30,
  TIME_OUT: 10000,
  LIMIT_COMMENT: 10,
  widthStyle: {
    maxWidth: 350,
    paddingHorizontal: '5%',
    width: '100%',
  },
  CheckoutType:{
    ADDRESS:0,
    LIST_PRODUCT:1,
    PAYMENT:2,
    INFO_PAYMENT:3
  },
  BANNER_POSITION: {
    MAIN: 'main',
    NEWS:'news',
    PRODUCT:'product',
    CATEGORY:'category',
    BEST_SELLER:'best-seller'
  },
  EventName: {
    ADD_NEW_CONVERSATION: 'ADD_NEW_CONVERSATION',
    ADD_NEW_STORE: 'ADD_NEW_STORE',
    LOGIN: 'LOGIN_EVENT',
    ECOMMERCE:"51",
    GO_TO_TAB: 'GO_TO_TAB',
    GO_TO_CATEGORY: 'GO_TO_CATEGORY',
    GET_TOTAL_CART: 'GET_TOTAL_CART',
    LIST_CART: 'LIST_CART',
    LOAD_GROUP_PRODUCT: 'LOAD_GROUP_PRODUCT',
    CHANGE_AVATAR: 'CHANGE_AVATAR',
    ORDER_SELLER:'ORDER_SELLER',
    ORDER_USER:'ORDER_USER',
    UPDATE_GROUP:'UPDATE_GROUP',
    ADD_PRODUCT:'ADD_PRODUCT'
  },

  SharedPreferencesKey: {
    UserData: 'UserData',
    IsLogin: 'IsLogin',
    IsGroupDes: 'IsGroupDes',
    token_type: 'token_type',
    access_token: 'access_token',
    user_name: 'user_name',
    userValidation: 'user_validation',
    lastTimeLogin: 'last_time_login',
    FATAL_DATA: 'FATAL_DATA',
    LOGIN_TYPE: 'LOGIN_TYPE',
    APPLE_KEY: 'APPLE_KEY',
  },
  GROUP_APPROVE_STATUS: {
    PENDING: 0,
    REJECTED: 10,
    APPROVED: 20,
    NOTHING: 30,
  },
  MyGroup: {
    BANNER: 0,
    OPTION: 1,
    POST: 2,
    TAB: 3,
    SUGGESTION_GROUP: 4,
    DESCRIPTION: 5,
    PRODUCT: 6,
    EMPTY_DATA: 7,
    TITLE: 8,
    EMPTY: 9,
  },
  IMAGE_TYPE: {
    COVER: 'cover',
    AVATAR: 'avatar',
  },
  homeItemType: {
    BANNER: 1,
    PRODUCT_MENU: 2,
    PRODUCT_DAY: 3,
    PRODUCT_BEST_SELLING: 4,
    COMMON_SEARCH: 5,
    GROUP: 6,
    TRADE_PROMOTION: 7,
    SUPPLIERS: 8,
    BIDDING: 9,
    TITLE_SUGGEST: 10,
    SUGGEST: 11,
    END: 12,
  },
  detailItemType: {
    BANNER: 1,
    DES_PRODUCT: 2,
    STORE_INFO: 3,
    PRODUCT_REVIEWS: 4,
    OTHER: 5,
    SUGGEST: 6,
    TITLE_SUGGEST: 7,
    PRODUCT: 8,
    END: 9,
  },
  productListCategory: {
    PRODUCT: 1,
    END: 2,
  },
  listGroups: {
    OUTSTANDING: 1,
    SUGGEST: 2,
    GROUPS: 3,
    END: 4,
  },
  listStore: {
    DES_STORE: 1,
  },
  listStoreDetail: {
    STORE_INFO: 1,
    TAB_INFORMATION: 2,
    BANNER_ACTION: 3,
    TEXT: 4,
    PRODUCT: 5,
    END: 6,
  },
  IMAGE_UPLOAD_TYPE: {
    PRODUCT: 10,
    STORE: 20,
    GROUP: 30,
    CATEGORY: 40,
    BANNER: 50,
    POST: 100,
    RATING: 150,
    REFUND: 160,
  },
  SELLER_DETAIL_TYPE: {
    STORE_INFO: 1,
    NOTIFICATION: 2,
    MY_SALE: 3,
    LIST_ITEM: 4,
    TITLE: 5,
    PRODUCT: 6,
    END: 7,
  },
  IMAGE_SCALE_TYPE: {
    COVER: FastImage.resizeMode.cover,
    CONTAIN: FastImage.resizeMode.contain,
    STRETCH: FastImage.resizeMode.stretch,
    CENTER: FastImage.resizeMode.center,
  },
  PRODUCT_STATUS: {
    NOT_YET_REVIEW: 0,
    REVIEWED: 10,
    REVIEWED_NOT_OK: 30,
  },
  GroupPercent: {
    TITLE: 0,
    PERCENT: 1,
    ADD_LEVEL: 2,
    subType: {
      DAI_LY: 0,
      GIOI_THIEU: 1,
    },
  },
  ADDRESS_LIST_TYPE: {
    ADDRESS: 0,
    BUTTON_ADD: 1,
  },
    ADDRESS_CATEGORY:{
    HOME:0,
    COMPANY:1
  }
  ,
  ADDRESS_DEFAULT:{
    DEFAULT:0,
    UN_DEFAULT:1,
  }
  ,
  BUSINESS_TYPES: [
    {
      type: 0,
      value: strings('enterprise'),
    },
    {
      type: 10,
      value: strings('individual'),
    },
  ],
  ROLE_POST: {
    id: 0,
    checkAll: false,
    name: 'Bài viết',
    roles: [
      {
        id: 7,
        name: 'Xem bài viết',
        key: 'view_post',
        check: true,
      },
      {
        id: 8,
        name: 'Đăng bài viết',
        key: 'add_post',
        check: false,
      },
      {
        id: 11,
        name: 'Kiểm duyệt bài viết',
        key: 'manage_post',
        check: false,
      },
    ],
  },
  ROLE_COMMENT: {
    id: 10,
    checkAll: false,
    name: 'Bình luận',
    roles: [
      {
        id: 9,
        name: 'Bình luận',
        key: 'add_comment',
        check: false,
      },
      {
        id: 10,
        name: 'Kiểm duyệt bình luận',
        key: 'manage_comment',
        check: false,
      },
    ],
  },
  GroupType: {
    JOINED: 1,
    OWNER: 2,
  },
  GROUP_PRIVACY: {
    PRIVATE: 1,
    PUBLIC: 0,
  },
  ROLE_PRODUCT: {
    id: 20,
    checkAll: false,
    name: 'Sản phẩm',
    roles: [
      {
        id: 4,
        name: 'Xem sản phẩm',
        key: 'view_product',
        check: false,
      },
      {
        id: 5,
        name: 'Đăng sản phẩm',
        key: 'add_product',
        check: false,
      },
      {
        id: 6,
        name: 'Kiểm duyệt sản phẩm',
        key: 'manage_product',
        check: false,
      },
    ],
  },
  ROLE_GROUP: {
    id: 20,
    checkAll: false,
    name: 'Hội nhóm',
    roles: [
      {
        id: 1,
        name: 'Xem thông tin giới thiệu',
        key: 'view_basic_info',
        check: false,
      },
      {
        id: 3,
        name: 'Sửa thông tin giới thiệu',
        key: 'edit_basic_info',
        check: false,
      },
      {
        id: 2,
        name: 'Xem thông tin hoa hồng',
        key: 'view_commission_info',
        check: false,
      },
      {
        id: 12,
        name: 'Xem thành viên trong nhóm',
        key: 'view_member',
        check: false,
      },
      {
        id: 13,
        name: 'Mời thành viên mới',
        key: 'invite_member',
        check: false,
      },
      {
        id: 14,
        name: 'Kiểm duyệt thành viên',
        key: 'manage_member',
        check: false,
      },
      {
        id: 15,
        name: 'Xem báo cáo, thống kê',
        key: 'view_stats',
        check: false,
      },
    ],
  },
  POST_DETAIL_TYPE: {
    INFO: 0,
    COMMENT: 1,
    LOAD_MORE: 3,
  },
  POST_STATUS: {
    PENDING: 0,
    REJECTED: 10,
    APPROVED: 20,
  },
  PRODUCT_LIST_GET_TYPE: {
    CATEGORY: 0,
  },
  ERROR_CODE: {
    INVALID_DATA: 422,
  },
  VOUCHER: {
    VOUCHER_TYPE: [
      {
        label: 'Giảm giá đơn hàng',
        value: 0,
      },
      {
        label: 'Giảm phí vận chuyển',
        value: 10,
      },
    ],
    VOUCHER_SCOPE: [
      {
        label: 'Tất cả sản phẩm',
        value: 0,
      },
      {
        label: 'Một số sản phẩm',
        value: 1,
      },
    ],
    DISCOUNT_TYPE: [
      {
        label: 'Theo số tiền',
        value: 0,
      },
      {
        label: 'Theo phần trăm',
        value: 1,
      },
    ],
    CONDITION_TYPE: [
      {
        label: 'Giá trị đơn hàng tối thiểu',
        value: 0,
      },
      {
        label: 'Số lượng sản phẩm tối thiểu',
        value: 1,
      },
    ],
    DISPLAY: {
      private: 0,
      public: 1,
    },
  },
  REFUND_METHOD: [
    {
      id: 20,
      name: 'Chuyển khoản',
    },
    {
      id: 30,
      name: 'Epoint',
    },
  ],
  MAP: {
    COORDINATE_DELTA: 0.0052,
    LATITUDE: 21.014571,
    LONGITUDE: 105.801679,
  },
  BANNER_ACTION_TYPE: {
    URL: 1,
    CATEGORY: 2,
    FLASH_SALE: 3,
  },
  LOGIN_TYPE: {
    USER: '1',
    FACEBOOK: '2',
    GOOGLE: '3',
    APPLE: '4',
  },
  RETURN_IMAGE_TYPE: {
    ADD_NEW: 0,
    IMAGE: 1,
  },
  MESSAGE_TYPE: {
    TEXT: 1,
    IMAGE: 2,
    DATE: 3,
  },
  ACTIVE_TYPE: {
    ACTIVE: 0,
    COMMENT: 1,
  },
  ORDER_STATUS: {
    ALL: 0,
    UNCONFIRMED: 1,
    CONFIRMED: 2,
    DELIVERING: 3,
    USER_CANCEL: 4,
    SHOP_CANCEL: 6,
    SUCCESS: 5,
  },
  TrangThaiDonHang: {
    UNCONFIRMED: 1,
    CONFIRMED: 10,
    DELIVERING: 20,
    DA_GIAO: 30,
    USER_CANCEL: 40,
    SHOP_CANCEL: 41,
    CANNOT_SHIP: 45,
    SUCCESS: 50,
  },
  STATUS_ORDER: {
    TO_PAY: 1,
    UNCONFIRMED: 2,
    CONFIRMED: 3,
    DELIVERING: 4,
    SUCCESS: 5,
    COMPLAINT:6,
    USER_CANCEL: 7,
    SHOP_CANCEL: 8,
    CANNOT_SHIP: 9,
  },
  SELLER_INCOME: {
    TO_RELEASE: 1,
    RELEASE: 2,
  },
  NotificationType: {
    ALL: -1,
    SYSTEM: 0,
    STORE: 20,
    GROUP: 30,
    ORDER: 80,
    EVENT: 90,
    CHAT: 100,
    PROMOTION:0,
    ACTIVE:1,

//     0: promotion(voucher)
//     1: active(like comment), 2:active (follow)
// 3: rate(assess product), 4: rate(response of store)
// 5: order (for customer)
//   6: order seller
// 7: teams
  },
  SHIP_TYPE: {
    GIAO_HANG_TIET_KIEM: 3,
    GIAO_HANG_NHANH: 1,
    GIAO_HANG_CHUAN:2
  },
  PAYMENT_TYPE:{
    COD:0,
    CREDIT_CARD:1,
    ATM:2
  },
  BIDDING:{
    BIDDING_VALUE:0,
    SOLICITORS:1,
  },
  AddProductType:{
    LIST_IMAGE:1,
    INFO_PRODUCT:2,
    LIST_CATEGORY:3,
    LIST_COM:4,
    END:6,
  },
  GetGroups:{
    JOIN_GROUP:1,
    MY_GROUP:2,
    SUGGEST_GROUP:3,
    OUTSTANDING: 4
  },
  POST_GROUPS:{
    BUTTON_ADD:1,
    LIST_ITEM:2,
  },
  MEMBER_GROUPS:{
    BUTTON_ADD:1,
    LIST_ITEM:2
  },
  LIST_MEMBER_TYPE:{
    BTN:1,
    LIST_ADMIN:2,
    LIST_MEMBER:3,
  },
  ROLE:{
    ADMIN:0,
    NOT_MEMBER:1,
    MEMBER:2
  },
  Status_Post:{
    ALL:1,

  },
  AddPost:{
    TITLE:1,
    TEXT_INPUT:2,
    BTN:3,
    LIST_IMAGE:4
  },
  typeNotification:{
    Promotion:5,
    Active:6,
    Rate:7,
    Order:1,
    OrderSeller:9,
    Teams:10
  },
  typeRating:{
    SHOP:'shop',
    USER:'buyer'
  },
  getNews:{
    NEWS:1,
    PROMOTION:2
  },
  listNewType:{
    BANNER:1,
    PRODUCT:2,
    END:3
  }
};
