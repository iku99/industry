export default {
  like: require('./icons/like.png'),
  //icon
  star: require('./icons/star.png'),
  unStar: require('./icons/un_star.png'),
  circle:require('./icons/Sign_in_circle.png'),

  icon_star: require('./icons/star.png'),
  icon_star_disable: require('./icons/un_star.png'),
  icon_bidPacket:require('./icons/bidPacket.png'),
  icon_bid:require('./icons/Bid.png'),
  icon_bidTime:require('./icons/time.png'),
 //imgtest
  img1:require('./icons/imageTest/img1.png'),
  img2:require('./icons/imageTest/img2.png'),
  img3:require('./icons/imageTest/img3.png'),

  profileDefault:require('./app/default_user.png'),
  voucher:require('./icons/Cart/voucher.png'),
  selectVoucher:require('./icons/Cart/selectVoucher.png'),

  logo:require('./logo/logo.png'),
  logoApp:require('./logo/logoApp.png'),
  //app
  imgBlur: require('./app/blur_img.jpg'),
  banner: require('./app/bannerGrop.png'),
  bg: require('./app/bg.png'),
  cartEmpty: require('./app/icon-empty.png'),
  product: require('./app/itemproduct.png'),
  bannerGroup:require('./app/bannerGroups.png'),
  //iconOrder
  icon_all: require('./icons/iconOrder/all.png'),
  icon_confirmed: require('./icons/iconOrder/cofi.png'),
  icon_delivering: require('./icons/iconOrder/delive.png'),
  icon_cancelled: require('./icons/iconOrder/cancell.png'),
  icon_success: require('./icons/iconOrder/shipped.png'),
  img_success: require('./app/success.png'),

  //iconProfile
  icon_point: require('./icons/iconProfile/point.png'),
  icon_cart: require('./icons/iconProfile/cart.png'),
  icon_house: require('./icons/iconProfile/house.png'),
  icon_dollar: require('./icons/iconProfile/dollar.png'),
  icon_review: require('./icons/iconProfile/mess.png'),
  icon_shops: require('./icons/iconProfile/shop.png'),
  icon_groups: require('./icons/iconProfile/group.png'),
  icon_setting: require('./icons/iconProfile/setting.png'),
  icon_help: require('./icons/iconProfile/help.png'),

  //icon pay
  iconMatter:require('./icons/pay/card.png'),
  iconMony:require('./icons/pay/mony.png'),

  //seller
  bgSeller:require('./app/backSeller.png'),
  avtSeller:require('./app/avtSeller.jpg'),
  myProduct:require('./icons/iconSeller/myProduct.png'),
  addProduct:require('./icons/iconSeller/add_circle.png'),
  myIncome:require('./icons/iconSeller/myIcome.png'),
  makg:require('./icons/iconSeller/marketing.png'),
  myShipping:require('./icons/iconSeller/shipping.png'),
  rating:require('./icons/iconSeller/rating.png'),
  business:require('./icons/iconSeller/business.png'),
  ads:require('./icons/iconSeller/ads.png'),
  support:require('./icons/iconSeller/support.png'),
};
