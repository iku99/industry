import {Dimensions, PixelRatio} from 'react-native';
import ColorStyle from './ColorStyle';
import AppConstants from './AppConstants';
import GlobalUtil from '../Utils/Common/GlobalUtil';
const isTablet = GlobalUtil.isTablet();
var widthScreen = Dimensions.get('window').width;
var heightScreen = Dimensions.get('window').height;

const productItemWidth =
  (widthScreen - (isTablet ? 48 : 32)) / (isTablet ? 3 : 2);
var screenSizeByInch =
  (Math.sqrt(Math.pow(widthScreen, 2) + Math.pow(heightScreen, 2)) /
    (PixelRatio.get() * 160)) *
  2;
var X =
  (widthScreen < heightScreen ? widthScreen : heightScreen) /
  (screenSizeByInch < 7 ? 9.25 : 12);
const marginTopAll = X * 0.6;
const paddingVeAll = X * 0.3;
const marginHorizontalAll = X * 0.3;
const marginHorizontal20 = X * 0.47;
const widthScreenMg24 = widthScreen - marginHorizontalAll * 2;
const widthIcon = (widthScreen - X * 1.2) / 5 - X * 0.2;
export default {
  constants: {
    X,
    productItemWidth,
    widthScreen,
    heightScreen,
    marginTopAll,
    marginHorizontalAll,
    widthScreenMg24,
    widthIcon,
    paddingVeAll,
    marginHorizontal20,
  },
  container: {
    backgroundColor: ColorStyle.backgroundScreen,
    flex: 1,
    flexDirection: 'column',
  },
  containerItemHome: {
    width: widthScreenMg24,
    alignItems: 'center',
    marginTop: marginTopAll,
    marginHorizontal: marginHorizontalAll,
  },
  containerItemHomeFullWidth: {
    width: widthScreen,
    marginTop: marginTopAll,
    paddingHorizontal: marginHorizontalAll,
    backgroundColor: ColorStyle.tabWhite,
    borderColor: ColorStyle.borderItemHome,
    elevation: 2,
    shadowColor: ColorStyle.borderItemHome,
    shadowOffset: {width: 1, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 1,
  },
  margin: {
    marginHorizontal16: {
      marginHorizontal: 16,
    },
  },
  search: {
    searchBar: {
      flexDirection: 'row',
      paddingTop: 36,
      paddingHorizontal: 16,
      position: 'absolute',
      alignItems: 'center',
      justifyContent: 'center',
    },
    searchInput: {
      flexDirection: 'row',
      backgroundColor: ColorStyle.tabWhite,
      borderRadius: 10,
      height: X*0.8,
      alignItems: 'center',
      paddingLeft: 10,
      marginVertical: 25,
      width: '100%',
    },
    searchText: {
      backgroundColor: 'transparent',
      textAlign: 'left',
      color: ColorStyle.optionTextColor,
    },
    searchButton: {
      height: 34,
      width: 40,
      backgroundColor: '#09B1BA',
      borderTopRightRadius: 5,
      borderBottomRightRadius: 5,
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
  toolbar: {
    toolbar: {
      paddingTop: X,
      paddingHorizontal: X * 0.6,
      backgroundColor: ColorStyle.tabActive,
      width: '100%',
      borderBottomLeftRadius: 20,
      borderBottomRightRadius: 20,
    },
    toolbarText: {
      marginTop: X,
    },
  },
  text: {
    text10: {
      fontSize: X*0.23,
      color: ColorStyle.tabInactive,
    },
    text11: {
      fontSize: X*0.26,
      color: ColorStyle.tabBlack,
    },
    text12: {
      fontSize:  X*0.28,
      color: ColorStyle.tabWhite,
    },
    text14: {
      fontSize:  X*0.31,
      color: ColorStyle.tabBlack,
    },
    text15: {
      fontSize:  X*0.35,
      color: ColorStyle.tabBlack,
    },
    text16: {
      fontSize: X*0.38,
      color: ColorStyle.tabWhite,
    },
    text24: {
      fontSize: X*0.57,
      color: ColorStyle.tabWhite,
    },
    text18: {
      fontSize: X*0.43,
      color: ColorStyle.tabWhite,
    },
    text20: {
      fontSize: X*0.47,
      color: ColorStyle.tabWhite,
    },
    text30: {
      fontSize: X*0.71,
      color: ColorStyle.tabBlack,
      textAlign: 'center',
      maxWidth: widthScreen - 100,
    },
    textTitleHome: {
      fontSize: 24,
      color: ColorStyle.textTitle24,
      left: 0,
      fontWeight: '700',
      maxWidth: X * 4.6,
    },
  },
  button: {
    buttonCart: {
      height: X,
      padding: 5,
      marginLeft: 10,
      alignItems: 'center',
      justifyContent: 'center',
    },
    buttonLogin: {
      width: widthScreen - 48,
      height: X * 1.2,
      marginHorizontal: 24,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
  input: {
    inputLogin: {
      minHeight: 50,
      flexDirection: 'row',
      width: widthScreenMg24,
      backgroundColor: '#F5F2FF',
      alignItems: 'center',
      borderRadius: 12,
      color:ColorStyle.tabBlack,
      // textDecoration: 'none',
      paddingHorizontal: 11.47,
      shadowColor: 'rgba(0, 0, 0, 0.25)',
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.8,
      shadowRadius: 1,
    },
    codeInput: {
      // height: X * 1.1,
      height: X * 1.1,
      backgroundColor: ColorStyle.tabWhite,
      borderRadius: 12,
      borderColor: ColorStyle.borderItemHome,
      paddingVertical: 15,
      justifyContent:'center',
      paddingLeft: 10,
      color:ColorStyle.tabBlack,
      shadowColor: 'rgba(0, 0, 0, 0.25)',
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.8,
      shadowRadius: 1,
    },
  },
  icon: {
    iconCart: {
      width: 20,
      height: 20,
    },
    iconLogoSmall: {
      width: X * 3,
      height: X * 1.5,
    },
    iconAvatarSmall: {
      width: X /1.3,
      height: X /1.3,
     marginRight: X/2.5,
      borderRadius:X,
      borderWidth:1,
      borderColor:ColorStyle.tabWhite
    },
    iconLogoBig: {
      width: X * 3,
      height: X * 3,
    },
    iconOrder: {
      width: X * 0.6,
      height: X * 0.6,
    },
    iconShow: {
      width: X * 0.9,
      height: X * 0.9,
    },
    icon_avatar: {
      width: X * 1.7,
      height: X * 1.7,
      borderRadius: X * 1.7,
      borderWidth: 1,
      borderColor: ColorStyle.colorPersonal,
      backgroundColor: 'transparent',
    },
    avatarGroup: {
      width: X * 1.9,
      height: X * 1.9,
      borderRadius: X * 1.9,
      borderWidth: 1,
      borderColor: ColorStyle.tabBlack,
      backgroundColor: 'transparent',
    },
    icon_notification: {
      width: X,
      height: X,
      justifyContent: 'center',
      alignSelf: 'center',
    },
    iconProfile: {
      width: X * 0.4,
      height: X * 0.4,
    },
    iconGroup: {
      width: X * 0.8,
      height: X * 0.8,
    },
    imgGroup: {
      width: X * 1.3,
      height: X * 1.3,
    },
    img_avatar: {
      width: X * 2.8,
      height: X * 2.8,
    },
    img_comment: {
      width: X * 1.9,
      height: X * 1.9,
    },
    img_product: {
      width: X * 2.5,
      height: X * 3,
    },
  },
  loading: {
    container: {
      alignItems: 'center',
    },
  },
  quickMenuButton: {
    padding: 10,
    // justifyContent:'center',
    alignItems: 'center',
    width: 100,
  },
  quickMenuImage: {
    width: 80,
    height: 80,
  },
  titleView: {
    width: widthScreen - 40,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 15,
  },
  border: {
    borderText: {
      borderRadius: 20,
      borderColor: ColorStyle.borderText,
      paddingHorizontal: 20,
      borderWidth: 1,
      paddingVertical: 7,
      marginLeft: 15,
    },
    borderItemHome: {
      borderRadius: 5,
      borderColor: 'rgba(0, 0, 0, 0.1)',
      elevation: 4,
      shadowColor: 'rgba(0, 0, 0, 0.1)',
      shadowOffset: {width: 0, height: 1},
      shadowOpacity: 0.8,
      shadowRadius: 1,
    },
  },
};
