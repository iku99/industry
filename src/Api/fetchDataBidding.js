import {strings} from '../resource/languages/i18n';

export default {
  data: [
    {
      id: 0,
      name: strings('commodity'),
      price: 953361283858,
      quantity: 257,
      child_package: [
        {
          id: '294178HSFJ',
          name_unit: 'Công an tỉnh Nghệ An',
          name_project: 'Mua sắm trang thiết bị,phầm mềm ứng dụng phục vụ công',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',
          address_perform: 'Hà Nội',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'KJFHDSJKFSN',
          name_unit: 'Công ty cổ phần AZKIDO Việt Nam',
          name_project:
            'GTMacca001 thuộc Dự án "Đấu thầu dự án trồng cây mắc ca"',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hà Nội',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'SFJKSDFHJ',
          name_unit: 'Cục Hậu cần, Bộ Công an',
          name_project:
            'Cải tạo, sửa chữa hệ thống kỹ thuật điện - nước, điều...	',
          status: false,
          address_perform: 'Hà Nội',
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'SADFSAF',
          name_unit: 'Bệnh viện Sản Nhi tỉnh Quảng Ninh	',
          name_project:
            'Mua sắm máy siêu âm tổng quát (màu 4D) cho Bệnh viện...	',
          status: false,
          address_perform: 'Quảng Ninh',
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'ADSWQ12',
          name_unit: 'Công ty TNHH MTV Điện lực Ninh Bình	',
          name_project:
            'Dự toán trang bị thay thế, bổ sung máy điều hòa, hút...	',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Ninh Bình',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'SD212',
          name_unit:
            'Trung tâm Giám sát, điều hành đô thị thông minh tỉnh Thừa Thiên Huế	',
          name_project: 'Xây dựng Hệ thống báo cáo số liệu dịch Covid-19	',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Thừa Thiên Huế',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'ASDSA2',
          name_unit: 'Công ty cổ phần Rượu Sâm Ngọc Linh Kon Tum	',
          name_project:
            'Hoàn thiện quy trình sản xuất cây giống in-vitro bằng...	',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Kon Tum',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: '',
          name_unit: 'Bệnh viện quận Gò Vấp	',
          name_project:
            'Mua sắm vật tư tiêu hao, hóa chất sử dụng cho máy tách...	',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hồ Chí Minh',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'FDSSD2',
          name_unit: 'Ủy ban nhân dân phường Ngô Mây	',
          name_project:
            'Nhà mẫu giáo kết hợp khu SHND khu vực 6, phường Ngô Mây	',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Quy Nhơn',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'ASDSA22',
          name_unit:
            'Công ty cổ phần Tư vấn quy hoạch và Thiết kế xây dựng Hải Dương	',
          name_project: 'Cải tạo, sửa chữa Trạm CSGT Ba Hàng - Phòng PC08	',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hải Dương',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'FSDFW',
          name_unit:
            'Công ty cổ phần Tư vấn quy hoạch và Thiết kế xây dựng Hải Dương	',
          name_project: 'Cải tạo, sửa chữa Công an huyện Cẩm Giàng	',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hải Dương',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'ASDADSA',
          name_unit: 'Công ty TNHH MTV Tư vấn và Dịch vụ Xây dựng Thăng Long	',
          name_project:
            'Tu sửa cấp thiết di tích đình Ngọc Uyên, phường Ngọc...	',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hà Nội',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'DASDAA',
          name_unit: 'Công ty cổ phần Nhân Phước Quảng Nam',
          name_project: 'Nhà làm việc Ban Chỉ huy Quân sự xã Atiêng	',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Quảng Nam',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
      ],
    },
    {
      id: 1,
      name: strings('construct'),
      price: 3296127925555,
      quantity: 192,
      child_package: [
        {
          id: 'AAAA1',
          name_unit: 'Công ty TNHH Thương mại Dịch vụ Việt Trí Tín	',
          name_project: 'Nút giao đường Hùng Vương - Quốc lộ 62 (TP. Tân An)	',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Phú Thọ',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'FSDSFSS',
          name_unit: 'Công ty TNHH Xây dựng Khánh Thịnh Việt	',
          name_project:
            'Đường tổ 8 Khu phố Hiệp Tâm 1 và Hiệp Tâm 2, thị trấn...	',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hà Nội',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'DSFSDFS',
          name_unit:
            'Công ty cổ phần Tư vấn quy hoạch và Thiết kế xây dựng Hải Dương	',
          name_project:
            'Cải tạo, sửa chữa nhà tạm giam, tạm giữ và phòng hỏi...	',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hải Dương',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'SSASA',
          name_unit:
            'Công ty cổ phần Tư vấn quy hoạch và Thiết kế xây dựng Hải Dương	',
          name_project:
            'Cải tạo, sửa chữa nhà giao ban và các phòng làm việc...	',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hải Dương',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'DVDASSA',
          name_unit: 'Công ty TNHH Đăng Bích',
          name_project:
            'Xây dựng hệ thống đường chiếu sáng tuyến đường Thượng...\t',
          status: false,
          address_perform: 'Hà Nội',
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'DBSAS',
          name_unit: 'Công ty cổ phần Đầu tư và Xây dựng Hưng Long\t',
          name_project:
            'Xây dựng trường Tiểu học Tân Phong, thành phố Biên Hòa\t',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Biên Hòa',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'SDASS',
          name_unit: 'Ban QLDA ĐTXD Bảo tàng Lịch sử Quân sự Việt Nam\t',
          name_project:
            'Đầu tư xây dựng Bảo tàng Lịch sử Quân sự Việt Nam...\t',
          status: false,
          address_perform: 'Hà Nội',
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'ASSSC',
          name_unit: 'Công ty Điện lực Nghệ An',
          name_project: 'Cải tạo, nâng cấp Nhà điều dưỡng Cửa Lò\t',
          status: false,
          address_perform: 'Nghệ An',
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'SADASD',
          name_unit: 'Công ty cổ phần Đầu tư Xây dựng Thương mại Sông Lam\t',
          name_project:
            'Sửa chữa phòng họp tầng 2 Trụ sở làm việc Văn phòng...\t',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hà Nội',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'SADSAAA',
          name_unit:
            'Công ty TNHH Tư vấn đầu tư xây dựng phát triển Hoàng Yến\t',
          name_project:
            'Xây dựng Công viên vườn sao - vườn dầu và khu luyện...\t',
          status: false,
          address_perform: 'Hà Nội',
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'ASBBB12',
          name_unit: 'Công ty cổ phần Tư vấn thiết kế sáng tạo\t',
          name_project:
            'Cải tạo, sửa chữa, nâng cấp Trạm Y tế xã Bế Văn Đàn,...\t',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hà Nội',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: '12SASD',
          name_unit: 'Phòng Kinh tế thuộc Ủy ban nhân dân thành phố Cao Bằng\t',
          name_project: 'Trụ sở, cửa hàng giới thiệu và bán sản phẩm nông...\t',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Cao Bằng',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'SFDFS',
          name_unit:
            'Ban Quản lý dự án đầu tư xây dựng và Phát triển quỹ đất huyện Cao Phong\t',
          name_project: 'Đường Hợp Phong, Cao Phong\t',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hoà Bình',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'BBBDSA',
          name_unit: 'Ban Quản lý dự án đầu tư xây dựng thành phố Phan Thiết\t',
          name_project:
            'Trường Trung học cơ sở Nguyễn Du (giai đoạn 1), thành...\t',
          status: false,
          address_perform: 'Phan Thiết',
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'SADSAAS',
          name_unit: 'Công ty cổ phần Tư vấn thiết kế sáng tạo\t',
          name_project:
            'Cải tạo, sửa chữa nâng cấp Trạm Y tế xã Độc Lập, huyện...\t',
          status: false,
          address_perform: 'Hà Nội',
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'QIQW6789',
          name_unit: 'Công ty TNHH Xây dựng Đinh Lâm\t',
          name_project:
            'Xây dựng nhà ăn bán trú, nhà vệ sinh, cổng, hàng rào...\t',
          status: false,

          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hà Nội',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: '986756CXV',
          name_unit: 'Ủy ban nhân dân xã Kim Hoa\t',
          name_project:
            'Cải tạo, nâng cấp rãnh thoát nước và đường giao thông...\t',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hà Nội',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'CADAS',
          name_unit: 'Bảo hiểm xã hội tỉnh Quảng Ninh\t',
          name_project:
            'Trụ sở Bảo hiểm xã hội thị xã Quảng Yên, tỉnh Quảng Ninh\t',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Quảng Ninh',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'PIOLIU4',
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          name_unit: 'Công ty TNHH Thương mại Dịch vụ và Tư vấn Sài Gòn Mới\t',
          name_project:
            'Sửa chữa, cải tạo, nâng cấp Trường MG Lộc Giang, TH Lê...\t',
          status: false,
          address_perform: 'Hồ Chí Minh',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'ZC232',
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          name_unit: 'Trường Trung học phổ thông Bảo Lâm\t',
          name_project:
            'Sửa chữa Trường Trung học phổ thông Bảo Lâm, huyện Bảo Lâm\t',
          status: false,
          address_perform: 'Hà Nội',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
      ],
    },
    {
      id: 2,
      name: strings('advisory'),
      price: 142717836529,
      quantity: 72,
      child_package: [
        {
          id: 'GHJH43',
          name_unit: 'Công ty Điện lực Quảng Ninh\t',
          name_project: 'Công ty Điện lực Quảng Ninh\t',
          status: false,
          address_perform: 'Quảng Ninh',
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: '435RDFG',
          name_unit: 'Ban Quản lý dự án đầu tư xây dựng huyện Việt Yên\t',
          name_project: 'Ban Quản lý dự án đầu tư xây dựng huyện Việt Yên\t',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Việt Yên',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'DGF345',
          name_unit: 'Ủy ban nhân dân xã Vĩnh Thịnh\t',
          name_project:
            'Cải tạo, nâng cấp Mương thủy lợi xã Vĩnh Thịnh, huyện...\t',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hà Nội',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: '3T4V34',
          name_unit: 'Ban Quản lý dự án đầu tư xây dựng thị xã Bỉm Sơn\t',
          name_project:
            'Quy hoạch chi tiết sắp xếp lại dân cư và cải tạo môi...\t',
          status: false,
          address_perform: 'Hà Nội',
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'WEREWRE',
          name_unit: 'Bệnh viện Lão khoa Trung ương\t',
          name_project:
            'Đầu tư xây dựng Bệnh viện Lão khoa Trung ương cơ sở 2\t',
          status: false,
          address_perform: 'Hà Nội',
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'WERWC',
          name_unit: 'Trường Trung cấp công nghệ số 8 Nam Định\t',
          name_project:
            'Đầu tư xây dựng Trường Trung cấp Công nghệ số 8 - cơ sở 1\t',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hà Nội',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
      ],
    },
    {
      id: 3,
      name: strings('consulting'),
      price: 123577108012,
      quantity: 84,
      child_package: [
        {
          id: 'CXVCVX',
          name_unit:
            'Ban Quản lý dự án xây dựng công trình hạ tầng thành phố Việt Trì\t',
          name_project: 'Hồ công viên Văn Lang giai đoạn 2016 - 2020\t',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Việt Trì',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'VXCVXCE',
          name_unit: 'Ban Quản lý các dự án đầu tư xây dựng tỉnh Gia Lai\t',
          name_project: 'Nâng cấp, cải tạo đường tỉnh 663, tỉnh Gia Lai\t',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Gia Lai',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: '3245FD',
          name_unit:
            'Ban Quản lý dự án các công trình Nông nghiệp và Phát triển nông thôn tỉnh Điện Biên\t',
          name_project:
            'Quản lý đa thiên tai lưu vực sông Nậm Rốm nhằm bảo vệ...\t',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hà Nội',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: '32345VVCX',
          name_unit: 'Học viện Quân y\t',
          name_project:
            'Dự án Đầu tư xây dựng Bệnh viện Quân y 103/Học viện...\t',
          status: false,
          address_perform: 'Hà Nội',
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: '2435VCXZ',
          name_unit: 'Quỹ Đầu tư phát triển tỉnh Bắc Giang\t',
          name_project:
            'Khu dân cư tổ dân phố Trung, thị trấn Bích Động, huyện...\t',
          status: false,
          name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',


          address_perform: 'Bắc Giang',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: 'SFGB34',
          name_unit: 'Ban Quản lý dự án đầu tư xây dựng tỉnh Hải Dương\t',
          name_project:
            'Đầu tư xây dựng đường trục Đông - Tây, tỉnh Hải Dương\t',
          status: false,name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',


          address_perform: 'Hải Dương',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
        {
          id: '345VDBGH',
          name_unit: 'Công ty TNHH Tư vấn Đấu thầu Cửu Long Bình Phước\t',
          name_project:
            'Lập đồ án quy hoạch chi tiết tỷ lệ 1/500 Phần diện...\t',
          status: false,name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Bình Phước',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
      ],
    },
    {
      id: 4,
      name: strings('mixture'),
      price: 1111666198000,
      quantity: 2,
      child_package: [
        {
          id: 'DGH89T',
          name_unit:
            '\tBan Quản lý dự án đầu tư xây dựng Trung ương Đoàn TNCS Hồ Chí Minh',
          name_project:
            'Xây dựng Khối nhà hiệu bộ và giảng đường chính Học...\t',
          status: false,name_packge:'Thi công xây dựng (Số thông báo: 20211175592 - 03....	',

          address_perform: 'Hồ Chí Minh',
          created_at: '2019-10-19T05:54:03.000+00:00',
        },
      ],
    },
  ],
};
