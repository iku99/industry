import axios from 'axios';

const header = (token_type, access_token) => {
  let mHeaders = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Accept-Charset': 'UTF-8',
  };
  if (token_type !== undefined && access_token !== undefined) {
    mHeaders = {
      ...mHeaders,
      authorization: token_type + ' ' + access_token,
    };
  }
  return mHeaders;
};
const headerDeleteMethod = (token_type, access_token) => ({
  'Content-Type': 'application/json',
  authorization: token_type + ' ' + access_token,
  accept: '*/*',
});
const headerWithoutToken = () => ({
  'Content-Type': 'application/json',
  Accept: '*/*',
  'Accept-Charset': 'UTF-8',
});
const headerUpload = (token_type, access_token) => ({
  'Content-Type': 'multipart/form-data',
  authorization: token_type + ' ' + access_token,
});
function GET(url, params = {}) {
  return axios
    .get(url, {
      params: params,
    })
    .then(res => res);
}
function POST(url, params) {
  return axios.post(url, {body: params}, {}).then(res => res);
}
function PUT(url, params = {}) {
  return axios.put(url, params).then(res => res);
}

function DELETE(url, params = {}) {
  return axios.delete(url, params).then(res => res);
}

function isOk(result) {
  return (
    result.status === 200 || result.status === 201 || result.status === 202
  );
}

export const Api = {
  GET,
  POST,
  PUT,
  DELETE,
  isOk,
};
