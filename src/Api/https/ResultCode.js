import { strings } from "../../resource/languages/i18n";

export default {
  SUCCESS: 200,
  UNAUTHORIZED: 401,
  BAD_REQUEST: 500,
  NOT_FOUND: 404,
  METHOD_NOT_ALLOWED: 502,
  INTERNAL_SERVER_ERROR: 403,
  BAD_GATEWAY: 502,
  SERVICE_UNAVAILABLE: 503,

  getMessage(resultCode) {
    switch (resultCode) {
      case this.UNAUTHORIZED:
        return strings('ws_unauthorized');
      case this.BAD_REQUEST:
        return strings('ws_bad_request');
      case this.NOT_FOUND:
        return strings('ws_not_found');
      case this.METHOD_NOT_ALLOWED:
        return strings('ws_method_not_allowed');
      case this.BAD_GATEWAY:
        return strings('ws_bad_request');
      case this.SERVICE_UNAVAILABLE:
        return strings('ws_unavailable');
      case this.INTERNAL_SERVER_ERROR:
        return strings('ws_internal_server');
      default:
        return strings('ws_success');
    }
  },
};
