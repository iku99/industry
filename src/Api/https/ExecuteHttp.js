import RNFetchBlob from 'rn-fetch-blob';

import {AsyncStorage} from 'react-native';
import futch from './api';
import AppConstants from '../../resource/AppConstants';
import GlobalUtil from '../../Utils/Common/GlobalUtil';
import GlobalInfo from '../../Utils/Common/GlobalInfo';
import axios from 'axios';

/**
 * @created by DucND/STeam
 * Http request helper
 */
const type = 'Bearer';
export default class ExecuteHttp {
  GET_METHOD = 'GET';
  POST_METHOD = 'POST';
  PUT_METHOD = 'PUT';
  DELETE_METHOD = 'DELETE';
  static executeHttp = null;

  static getInstance() {
    if (ExecuteHttp.executeHttp == null) {
      ExecuteHttp.executeHttp = new ExecuteHttp();
    }
    return this.executeHttp;
  }

  header = access_token => {
    let mHeaders = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      commerce_id: AppConstants.EventName.ECOMMERCE,
      'Accept-Charset': 'UTF-8',
    };
    if (access_token !== undefined) {
      mHeaders = {
        ...mHeaders,
        authorization: type + ' ' + access_token,
      };
    }
    return mHeaders;
  };
  headerDeleteMethod = access_token => ({
    'Content-Type': 'application/json',
    ecommerce_id: AppConstants.EventName.ECOMMERCE,
    authorization: type + ' ' + access_token,
    accept: '*/*',
  });
  headerWithoutToken = () => ({
    'Content-Type': 'application/json',
    Accept: '*/*',
    ecommerce_id: AppConstants.EventName.ECOMMERCE,
    'Accept-Charset': 'UTF-8',
  });
  headerUpload = access_token => ({
    'Content-Type': 'multipart/form-data',
    ecommerce_id: AppConstants.EventName.ECOMMERCE,
    authorization: type + ' ' + access_token,
  });

  async authService(urlEndpoint, data) {
    try {
      let cookies;
      return RNFetchBlob.config({
        trusty: true,
        timeout: AppConstants.TIME_OUT,
      })
        .fetch(
          this.POST_METHOD,
          urlEndpoint,
          this.header(''),
          JSON.stringify(data),
        )
        .then(response => {
          cookies = response.respInfo.headers['Set-Cookie'];
          return response.json();
        })
        .then(responseData => {
          if (!GlobalUtil.checkIsNull(cookies)) {
            responseData.cookies = cookies;
            return responseData;
          } else {
            return {};
          }
        })
        .catch(error => console.log('authService parse error: ' + error));
    } catch (error) {
      console.log('authService catch error: ' + error);
    }
  }

  async post(urlEndpoint, data) {
    await this.getTokenHeader();
    try {
      let access_token = GlobalInfo.access_token;

      if (access_token === undefined) {
        return await this.postWithoutToken(urlEndpoint, data);
      }
      let init = {
        method: this.POST_METHOD,
        headers: this.header(access_token),
        body: data,
      };
      let response = await fetch(urlEndpoint, {
        method: this.POST_METHOD,
        headers: this.header(access_token),
        body: JSON.stringify(data),
      })
        .then(response => {
          return response.json();
        })
        .then(responseData => {
          return responseData;
        })
        .catch(error => {
          console.log('PostApiMethod parse error: ' + error);
        });
      return response;
    } catch (error) {
      console.log('PostApiMethod catch error: ' + error);
    }
  }
  async postWithoutToken(urlEndpoint, data) {
    try {
      return RNFetchBlob.config({
        trusty: true,
        timeout: AppConstants.TIME_OUT,
      })
        .fetch(
          this.POST_METHOD,
          urlEndpoint,
          this.headerWithoutToken(),
          JSON.stringify(data),
        )
        .then(response => response.json())
        .then(responseData => {
          return responseData;
        })
        .catch(error => {
          console.log('PostApiMethod parse error: ' + error);
        });
    } catch (error) {
      console.log('PostApiMethod catch error: ' + error);
    }
  }

  async postUpload(urlEndpoint, data, onProgress, onFinish) {
    await this.getTokenHeader();
    try {
      let token_type = GlobalInfo.token_type;
      let access_token = GlobalInfo.access_token;
      futch(
        urlEndpoint,
        {
          method: this.POST_METHOD,
          body: data,
          headers: this.headerUpload(token_type, access_token),
        },
        progressEvent => {
          const progress = progressEvent.loaded / progressEvent.total;
          onProgress(progress);
        },
      ).then(
        res => {
          onFinish(true, res);
        },
        err => onFinish(false, err),
      );
    } catch (error) {
      console.log('PostApiMethod catch error: ' + error);
    }
  }

  async putUpload(urlEndpoint, data, onProgress, onFinish) {
    await this.getTokenHeader();
    try {
      let token_type = GlobalInfo.token_type;
      let access_token = GlobalInfo.access_token;
      futch(
        urlEndpoint,
        {
          method: this.PUT_METHOD,
          body: data,
          headers: this.headerUpload(token_type, access_token),
        },
        progressEvent => {
          const progress = progressEvent.loaded / progressEvent.total;
          onProgress(progress);
        },
      ).then(
        res => {
          onFinish(true, res);
        },
        err => onFinish(false, err),
      );
    } catch (error) {
      console.log('PostApiMethod catch error: ' + error);
    }
  }

  async get(urlEndpoint) {
    await this.getTokenHeader();
    try {
      let access_token = GlobalInfo.access_token;
      if (access_token === undefined) {
        return await this.getWithoutToken(urlEndpoint);
      }
      return RNFetchBlob.config({
        trusty: true,
        timeout: AppConstants.TIME_OUT,
      })
        .fetch(this.GET_METHOD, urlEndpoint, this.header(access_token))
        .then(response => response.json())
        .then(responseData => {
          return responseData;
        })
        .catch(error => {
          console.log('GetApiMethod parse error: ' + error);
        });
    } catch (error) {
      console.log('GetApiMethod error: ' + error);
    }
  }
  async getWithoutToken(urlEndpoint) {
    try {
      return RNFetchBlob.config({
        trusty: true,
        timeout: AppConstants.TIME_OUT,
      })
        .fetch(this.GET_METHOD, urlEndpoint, this.headerWithoutToken())
        .then(response => response.json())
        .then(responseData => {
          return responseData;
        })
        .catch(error => {
          console.log('GetApiMethod parse error: ' + error);
        });
    } catch (error) {
      console.log('GetApiMethod error: ' + error);
    }
  }

  async put(urlEndpoint, data) {
    await this.getTokenHeader();
    try {
      let access_token = GlobalInfo.access_token;
      return RNFetchBlob.config({
        trusty: true,
        timeout: AppConstants.TIME_OUT,
      })
        .fetch(
          this.PUT_METHOD,
          urlEndpoint,
          this.header(access_token),
          JSON.stringify(data),
        )
        .then(response => response.json())
        .then(responseData => {
          return responseData;
        })
        .catch(error => {
          console.log('PostApiMethod parse error: ' + error);
        });
    } catch (error) {
      console.log('PostApiMethod catch error: ' + error);
    }
  }

  async delete2(urlEndpoint, data) {
    await this.getTokenHeader();
    try {
      let token_type = GlobalInfo.token_type;
      let access_token = GlobalInfo.access_token;
      return RNFetchBlob.config({
        trusty: true,
        timeout: AppConstants.TIME_OUT,
      })
        .fetch(
          this.DELETE_METHOD,
          urlEndpoint,
          this.header(token_type, access_token),
          JSON.stringify(data),
        )
        .then(response => response.json())
        .then(responseData => {
          return responseData;
        })
        .catch(error => {
          console.log('PostApiMethod parse error: ' + error);
        });
    } catch (error) {
      console.log('PostApiMethod catch error: ' + error);
    }
  }

  async delete(urlEndpoint, param) {
    await this.getTokenHeader();
    try {
      let access_token = GlobalInfo.access_token;
      return await fetch(urlEndpoint, {
        method: this.DELETE_METHOD,
        headers: this.header(access_token),
        body: JSON.stringify(param),
      })
        .then(response => {
          // console.log(JSON.stringify(response))
          return response.status;
        })
        .catch(error => {
          console.log('DeleteApiMethod parse error:1 ' + error);
        });
    } catch (error) {
      console.log('DeleteApiMethod catch error:2 ' + error);
    }
  }

  async getOriginalResponse(urlEndpoint) {
    await this.getTokenHeader();
    try {
      let tokenStr = GlobalInfo.sessionId;
      let userName = GlobalInfo.userInfo.userName;
      return RNFetchBlob.config({
        trusty: true,
        timeout: AppConstants.TIME_OUT,
      })
        .fetch(this.GET_METHOD, urlEndpoint, {Cookie: tokenStr})
        .then(response => response)
        .catch(error => {
          console.log('getOriginalResponse parse error: ' + error);
        });
    } catch (error) {
      console.log('getOriginalResponse error: ' + error);
    }
  }
  async getTokenHeader() {
    if (GlobalInfo.token_type === undefined || GlobalInfo.token_type === '') {
      GlobalInfo.token_type = await AsyncStorage.getItem(
        AppConstants.SharedPreferencesKey.token_type,
        null,
      );
    }
    if (
      GlobalInfo.access_token === undefined ||
      GlobalInfo.access_token === ''
    ) {
      GlobalInfo.access_token = await AsyncStorage.getItem(
        AppConstants.SharedPreferencesKey.access_token,
        null,
      );
    }
  }
}
