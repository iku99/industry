const host = 'http://103.9.159.151:22290';
// const host = 'http://192.168.100.91:4000';
export default {
  host,
  hostWeb: 'https://eplaza.vn',
  paymentUrl: 'https://eplaza.vn',
  paymentUrl2: 'https://test.eplaza.vn',
  socketHost: 'wss://sock.eplaza.vn/prod',
  apiEndpoint: {
    getNewLaw: host + '/getNewRaw',
    onePay: '/onepay/checkout',
    getQuestionList: '/getQuestionList',
    getLawList: '/getLawList',
    getProductList: '/products/all-paging',
    getProductDetail: '/products/',
    getRating: '/assess/all-paging/product/',
    getRatingUser: '/assess/my-assesses',
    getRatingStore: '/stores/get-rating',
    getRatingStats: '/products/',
    sendRating: '/assess/product',
    getUser: '/customers',
    //
    getStatistical: '/stores/statistical',
    //
    searchProduct: '/products/search',
    filterProduct: '/products/filter',
    //
    deleteProducts: '/products/',
    getProductFeature: '/product-property/get-by-category',
    uploadProduct: '/products',
    getCategoryDetail: '/categories/parent/',
    updateProduct: '/products/',
    getProductBuStore: '/products/store/',
    getProductByIds: '/products/by-ids',
    checkBoughtProduct: '/products/{productId}/bought/{userId}',
    ratingProduct: '/products/{productId}/ratings',
    likeProduct: '/likes/',
    checkLikeProduct: '/products/{productId}/likes',
    getRatingCount: '/assess/statistical/product/',
    getShip: '/stores/sellect-ship-unit/',
    getMethodShip: '/orders/vietchem/available-services',
    billShip: '/orders/fee',
    //user
    // checkExistedEmail: '/users/check-email',
    registerPhone: '/users/register-phone',
    phoneVerify: '/users/register-phone/verify',
    userNameVerify: '/users/verify',
    checkOTP: '/customers/check-otp',
    // register: '/users/register-email',
    // login: '/users/login',
    googleLogin: '/users/login-with-google',
    facebookLogin: '/users/login-with-facebook',
    appleLogin: '/users/login/apple',
    forgetPassword: '/users/forgot-password',
    getUserInfo: '/users/info/{userId}',
    updateUserInfo: '/customers',
    changePassword: '/customers/change-password',
    searchUser: '/customers/search',

    //store
    createStore: '/stores',
    getMyStore: '/stores/my-store',
    getStoreDetail: '/stores/',
    updateStore: '/stores/update',
    getTopStore: '/stores/get-top',
    getStoreRateCount: '/stores/{storeId}/ratings/count',
    getStoreRateAvg: '/stores/{storeId}/ratings/avg',
    countProductOfStore: '/stores/{storeId}/products/count',
    getStoreCategory: '/stores/category/',
    viewStore: '/update-views/',
    //order
    getOrderByUser: '/orders/status',
    updateOrder: '/orders/status/',
    getOrderDetail: '/orders/',
    order: '/orders/check-out',
    getOrderByStore: '/orders/status',
    //group

    getGroup: '/teams/all-paging',
    getSuggestionGroup: '/groups/suggest',
    getDetailGroup: '/teams/',
    groupPath: '/members/',
    registerGroup: '/teams/',
    getMyGroup: '/groups',
    updateGroup: '/teams/',
    deleteGroup: '/teams/',
    //notification

    getProductGroup: '/groups/{id}/products',
    getRoleGroup: '/groups/{id}/roles',
    approveGroupProduct: '/groups/{groupId}/products/{productId}',
    deleteRoleGroup: '/groups/{groupId}/roles',
    getRoleGroupDetail: '/groups/{groupId}/roles/{roleId}',
    updateRoleGroup: '/groups/{groupId}/roles/{roleId}',
    addRoleGroup: '/groups/{groupId}/roles',
    getGroupMembers: '/members/teams/',
    updateGroupMemberStatus: '/members/',
    deleteGroupMember: '/groups/{groupId}/members/{userId}',
    updateRoleGroupMember: '/members/',
    postProductToGroup: '/groups/{groupId}/products',
    postPostToGroup: '/posts/',

    //post
    getGroupPost: '/posts/team/',
    getPostDetail: '/posts/',
    getPostComment: '/comments/all-paging',
    getPostLike: '/likes/',
    likePost: '/likes/',
    getAllPost: '/posts/feeds',
    commentToPost: '/comments/',
    deleteGroupPostComment: '/comments/',
    insertGroupPostComment: '/comments/',
    deleteGroupPost: '/post/',
    updateStatusPost: '/posts/status/',
    viewPost: '/seen/',
    //media
    uploadImage: '/upload/upload-single',
    getImage: '/upload/upload-array',
    uploadProfileImage: '/upload/upload-array',

    //cart
    addToCart: '/cart',
    getCart: '/cart',
    deleteProductFromCart: '/cart',
    updateProductFromCart: '/cart',
    getTotalOrder: '/cart/total',
    updateOnepayStatus: '/api/onepay/ipn',

    //location
    getProvinces: '/cities',
    getDistrict: '/districts/city/',
    getWards: '/wards/district/',

    //notification
    getNotifications: '/notifications/my-notifications',
    updateNotifications: '/notifications/change-status',
    getCountNotification: '/notifications/count',
    //mesaage
    getMessageInbox: '/chat/inboxes',
    sendTextMsg: '/chat/messages',
    sendImageMsg: '/chat/messages/image',
    getMessageList: '/chat/inboxes/user/',

    //address
    // getAddressList: '/users/addresses',
    // createAddress: '/users/addresses',
    // deleteAddress: '/users/addresses/',
    // updateAddress: '/users/addresses/',

    getAddressList: '/address/my-address',
    createAddress: '/address',
    deleteAddress: '/address/',
    updateAddress: '/address/',
    //event
    getEventInfo: '/events',

    //banner
    getBanners: '/banners/new',
    getBannersInfo: '/media/multiple',

    //voucher
    getVoucherList: '/vouchers',
    deleteVoucher: '/vouchers/',
    createVoucher: '/vouchers/',
    updateVoucher: '/vouchers/',
    getVoucher: '/vouchers/get-voucher',
    getVoucherEco: '/vouchers/get-voucher-can-apply',
    getVoucherStore: '/vouchers/get-voucher-can-apply-store',
    getVoucherByCode: '/vouchers/set-by-code',
    getStoreRefund: '/refunds',
    applyVoucher: '/vouchers/apply',

    //Vote
    getVoteHandle: '/stores/ratings',
    getVoteOfUser: '/ratings',
    deleteVoteOfUser: '/ratings',

    //Epoint
    getEpoint: '/users/points',
    getEpointHistory: '/users/points/history',

    //refund
    getRefundMeta: '/refunds/meta',
    refunds: '/refunds',
    updateRefunds: '/refunds/{id}',

    //Google map api
    searchCoordinate:
      'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=',

    //lam moi
    //auth
    login: '/users/login',
    register: '/users/register',
    checkExistedEmail: '/customers/check-exist',
    //category
    getCategoryMenu: '/categories/',
    getCategoryChildren: '/categories/get-all',
    getProductCategory: '/products/category/',

    //news
    getNews: '/news/all-paging',
    getNewsCate: '/news-cate',
    getNewsDetail: '/news/',
  },
};
