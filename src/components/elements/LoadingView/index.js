import React from 'react'
import {Text, View} from "react-native";
import ColorStyle from "../../../resource/ColorStyle";
import {BarIndicator, DotIndicator,SkypeIndicator} from "react-native-indicators";

export default function LoadingView({loading}){
    return(
        <View
            style={{
                position: 'absolute',
                display: loading ? 'flex' : 'none',
                flex: 1,
                alignItems:'center',
                backgroundColor: ColorStyle.loading,
                width: '100%',
                height: '100%',
                top: 0,
            }}>
            <View >
                <SkypeIndicator color={ColorStyle.tabActive} size={50} />
            </View>
        </View>
    )
}
