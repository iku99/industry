import React from 'react';
import {
  Animated,
  Platform,
  StyleSheet,
  View,
  Dimensions,
} from 'react-native';
import Styles from '../../../resource/Styles';
import AppConstants from "../../../resource/AppConstants";

const ios = Platform.OS === 'ios';
const {width, height} = Dimensions.get('window');
// from native-base
const isIphoneX = ios && (height === 812 || width === 812);
const iphoneXTopInset = Styles.toolbar.toolbar.paddingTop;
const initToolbarHeight = ios ? 46 : 56;

const paddingTop = Styles.toolbar.toolbar.paddingTop;
const topInset = isIphoneX ? iphoneXTopInset : 0;

const toolbarHeight = initToolbarHeight + topInset + paddingTop;

export default class Header extends React.PureComponent {
  constructor(props) {
    super(props);
    this.headerHeight = props.headerMaxHeight;
    this.state = {
      scrollOffset: new Animated.Value(0),
      left: 0,
      bottom: 0,
    };
  }

  onScroll = e => {
    if (this.props.disabled) {
      return;
    }
    this.state.scrollOffset.setValue(e.nativeEvent.contentOffset.y);
  };
  _getHeight = () => {
    const {scrollOffset} = this.state;
    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight],
      outputRange: [this.headerHeight, toolbarHeight],
      extrapolate: 'clamp',
    });
  };
  _getWidth = () => {
    const {scrollOffset} = this.state;
    return scrollOffset.interpolate({
      inputRange: [0, Styles.constants.widthScreenMg24],
      outputRange: [
        Styles.constants.widthScreenMg24,
        Styles.constants.widthScreenMg24-Styles.constants.X*2.5,
      ],
      extrapolate: 'clamp',
    });
  };

  _getBottom = () => {
    const {scrollOffset} = this.state;
    const bottom =
      this.props.titleStyle.bottom || Header.defaultProps.titleStyle.bottom;
    return scrollOffset.interpolate({
      inputRange: [0, this.headerHeight],
      outputRange: [bottom-Styles.constants.X/1.5,AppConstants.IS_IOS?this.state.bottom-Styles.constants.X/3:this.state.bottom],
      extrapolate: 'clamp',
    });
  };
  _getImageOpacity = () => {
    const {scrollOffset} = this.state;
    return this.props.imageSource
      ? scrollOffset.interpolate({
          inputRange: [0, this.headerHeight - toolbarHeight],
          outputRange: [1, 0],
          extrapolate: 'clamp',
        })
      : 0;
  };

  _getImageScaleStyle = () => {
    if (!this.props.parallax) {
      return undefined;
    }
    const {scrollOffset} = this.state;
    const scale = scrollOffset.interpolate({
      inputRange: [-100, -0],
      outputRange: [1.5, 1],
      extrapolate: 'clamp',
    });

    return {
      transform: [
        {
          scale,
        },
      ],
    };
  };

  render() {
    const {
      imageSource,
      toolbarColor,
    } = this.props;
    const height = this._getHeight();
    const bottom = this._getBottom();
    const imageOpacity = this._getImageOpacity();
    return (
      <Animated.View
        style={[
          styles.header,
          {
            height: height,
            backgroundColor: toolbarColor,
          },
        ]}>
        <View style={styles.toolbarContainer}>
          <View style={styles.toolbar}>
            {this.props.renderLeft && this.props.renderLeft()}

            <Animated.Image
              style={[
                {
                  width: Styles.constants.X*3,
                  height: Styles.constants.X*0.6,
                  opacity: imageOpacity,
                },
                this._getImageScaleStyle(),
              ]}
              source={imageSource}
            />
            {this.props.renderRight && this.props.renderRight()}
          </View>
        </View>

        <Animated.View
          style={{
            width: this._getWidth(),
            position: 'absolute',
            bottom: bottom,
            alignSelf: 'center',
          }}>
          {this.props.renderSearch && this.props.renderSearch()}
        </Animated.View>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  toolbarContainer: {
    height: toolbarHeight,
  },
  statusBar: {
    height: topInset + paddingTop,
  },
  toolbar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    paddingTop: Styles.toolbar.toolbar.paddingTop,
  },
  titleButton: {
    flexDirection: 'row',
  },
  flexView: {
    flex: 1,
  },
});

Header.defaultProps = {
  bodyText: '',
  title: '',
  renderLeft: undefined,
  renderRight: undefined,
  backStyle: {marginLeft: 10},
  bodyTextStyle: {fontSize: 16},
  titleStyle: {fontSize: 20, left: 40, bottom: 30},
  toolbarColor: '#FFF',
  headerMaxHeight: 200,
  disabled: false,
  imageSource: undefined,
};
