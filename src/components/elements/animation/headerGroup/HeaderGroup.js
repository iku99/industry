import {Animated, Dimensions, Platform, StyleSheet, Text, View} from "react-native";
import Styles from "../../../../resource/Styles";
import React from "react";
import {Animation} from "react-native-popup-dialog/src";
import GroupStyle from "../../../screen/group/GroupStyle";
import AppConstants from "../../../../resource/AppConstants";
const ios = Platform.OS === 'ios';
const {width, height} = Dimensions.get('window');
// from native-base
const isIphoneX = ios && (height === 812 || width === 812);
const iphoneXTopInset = Styles.toolbar.toolbar.paddingTop;
const initToolbarHeight = ios ? 46 : 56;

const paddingTop = Styles.toolbar.toolbar.paddingTop;
const topInset = isIphoneX ? iphoneXTopInset : 0;

const toolbarHeight = initToolbarHeight + topInset + paddingTop;

export default class HeaderGroup extends React.PureComponent {
    constructor(props) {
        super(props);
        this.headerHeight = props.headerMaxHeight;
        this.state = {
            scrollOffset: new Animated.Value(0),
            left: 0,
            bottom: 0,
        };
    }

    onScroll = e => {
        if (this.props.disabled) {
            return;
        }
        this.state.scrollOffset.setValue(e.nativeEvent.contentOffset.y);
    };

    onBackLayout = e => {
        const layout = e.nativeEvent.layout;
        const bottom =
            toolbarHeight - layout.y - layout.height - paddingTop - topInset;
        this.setState({bottom: bottom, left: e.nativeEvent.layout.x});
    };

    _getFontSize = () => {
        const {scrollOffset} = this.state;
        const backFontSize =
            this.props.bodyTextStyle.fontSize ||
            Header.defaultProps.bodyTextStyle.fontSize;
        const titleFontSize =
            this.props.titleStyle.fontSize || Header.defaultProps.titleStyle.fontSize;
        return scrollOffset.interpolate({
            inputRange: [0, this.headerHeight],
            outputRange: [titleFontSize, backFontSize],
            extrapolate: 'clamp',
        });
    };

    _getLeft = () => {
        const {scrollOffset} = this.state;
        const left =
            this.props.titleStyle.left || Header.defaultProps.titleStyle.left;
        return scrollOffset.interpolate({
            inputRange: [0, this.headerHeight - toolbarHeight],
            outputRange: [left, this.state.left],
            extrapolate: 'clamp',
        });
    };

    _getHeight = () => {
        const {scrollOffset} = this.state;
        return scrollOffset.interpolate({
            inputRange: [0, this.headerHeight - toolbarHeight],
            outputRange: [this.headerHeight, toolbarHeight],
            extrapolate: 'clamp',
        });
    };
    _getWidth = () => {
        const {scrollOffset} = this.state;
        return scrollOffset.interpolate({
            inputRange: [0, Styles.constants.widthScreenMg24],
            outputRange: [
                Styles.constants.widthScreenMg24,
                Styles.constants.widthScreenMg24-Styles.constants.X*2,
            ],
            extrapolate: 'clamp',
        });
    };

    _getBottom = () => {
        const {scrollOffset} = this.state;
        const bottom =
            this.props.titleStyle.bottom || HeaderGroup.defaultProps.titleStyle.bottom;
        return scrollOffset.interpolate({
            inputRange: [0, this.headerHeight ],
            outputRange: [bottom+Styles.constants.X*2,AppConstants.IS_IOS?this.state.bottom:this.state.bottom],
            extrapolate: 'clamp',
        });
    };
    _getBottom2 = () => {
        const {scrollOffset} = this.state;
        const bottom =
            this.props.titleStyle.bottom || HeaderGroup.defaultProps.titleStyle.bottom;
        return scrollOffset.interpolate({
            inputRange: [0, this.headerHeight ],
            outputRange: [5, this.state.bottom-Styles.constants.X*2],
            extrapolate: 'clamp',
        });
    };

    _getOpacity = () => {
        const {scrollOffset} = this.state;
        return this.props.bodyText
            ? scrollOffset.interpolate({
                inputRange: [0, this.headerHeight - (toolbarHeight+paddingTop)],
                outputRange: [1, 0],
                extrapolate: 'clamp',
            })
            : 0;
    };

    _getImageOpacity = () => {
        const {scrollOffset} = this.state;
        return this.props.imageSource
            ? scrollOffset.interpolate({
                inputRange: [0, this.headerHeight - toolbarHeight],
                outputRange: [1, 0],
                extrapolate: 'clamp',
            })
            : 0;
    };

    _getImageScaleStyle = () => {
        if (!this.props.parallax) {
            return undefined;
        }
        const {scrollOffset} = this.state;
        const scale = scrollOffset.interpolate({
            inputRange: [-100, -0],
            outputRange: [1.5, 1],
            extrapolate: 'clamp',
        });

        return {
            transform: [
                {
                    scale,
                },
            ],
        };
    };

    render() {
        const {
            imageSource,
            toolbarColor,
            titleStyle,
            onBackPress,
            backStyle,
            bodyTextStyle,
        } = this.props;
        const height = this._getHeight();
        const width = this._getWidth();
        const left = this._getLeft();
        const bottom = this._getBottom();
        const bottom2 = this._getBottom2();
        const opacity = this._getOpacity();
        const fontSize = this._getFontSize();
        const imageOpacity = this._getImageOpacity();
        return (
            <Animated.View
                style={[
                    styles.header,
                    {
                        height: height,
                    },
                ]}>
                <View style={{...styles.toolbarContainer,  backgroundColor: toolbarColor,    borderBottomLeftRadius: 20,
                    borderBottomRightRadius: 20,}}>

                    <Animated.View style={{ position:'absolute', opacity: opacity}}>
                        {this.props.renderBody && this.props.renderBody()}
                    </Animated.View>
                    <View style={{...styles.toolbar}}>
                        {this.props.renderLeft && this.props.renderLeft()}
                        {this.props.renderRight && this.props.renderRight()}
                    </View>
                </View>

                <Animated.View
                    style={{
                        width: this._getWidth(),
                        position: 'absolute',
                        bottom: bottom,
                        alignSelf: 'center',
                    }}>
                    {this.props.renderInfo && this.props.renderInfo()}
                </Animated.View>
                <Animated.View style={{width:"100%",height:Styles.constants.X*2 , position: 'absolute',
                    bottom: bottom2}}>
                    {this.props.tabView && this.props.tabView()}
                </Animated.View>
            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    toolbarContainer: {
        height: toolbarHeight,
    },
    statusBar: {
        height: topInset + paddingTop,
    },
    toolbar: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: Styles.toolbar.toolbar.paddingTop,
        justifyContent: 'space-between',
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,

    },
    titleButton: {
        flexDirection: 'row',
    },
    flexView: {
        flex: 1,
    },
});

HeaderGroup.defaultProps = {
    bodyText: '',
    title: '',
    renderLeft: undefined,
    renderRight: undefined,
    backStyle: {marginLeft: 10},
    bodyTextStyle: {fontSize: 16},
    titleStyle: {fontSize: 20, left: 40, bottom: 30},
    toolbarColor: '#FFF',
    headerMaxHeight: 200,
    disabled: false,
    imageSource: undefined,
};
