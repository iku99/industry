import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  Animated, FlatList,
} from "react-native";
import Styles from '../../resource/Styles';
import {strings} from '../../resource/languages/i18n';
import NavigationUtils from '../../Utils/NavigationUtils';
import ImageHelper from '../../resource/images/ImageHelper';
import {Icon} from 'react-native-elements';
import ColorStyle from '../../resource/ColorStyle';
import {Actions} from 'react-native-router-flux';
import BadgeView from './BadgeView';
import ParallaxScrollView from "react-native-parallax-scroll-view";
const height = (Styles.constants.heightScreen / 3) * 1.2;
const header_Max_Height = (Styles.constants.heightScreen / 3) * 1.2;
const header_Min_Height = 100;
export default class AppHeaderBar extends Component {
  state = {
    fadeAnim: new Animated.Value(0),
    userName: '',
  };

  componentDidMount() {
    // this.setState({fadeAnim:this.props.statusBarAlpha})
  }

  render() {
    const {onScroll = () => {}} = this.props;
    return (
      <View style={[Styles.container]}>
        <ParallaxScrollView
          onScroll={onScroll}
          stickyHeaderHeight={80}
          parallaxHeaderHeight={250}
          backgroundSpeed={10}
          backgroundColor={ColorStyle.tabActive}
          contentBackgroundColor={'#fff'}
          renderForeground={() => (
            <View style={[Styles.toolbar.toolbar]}>
              <View style={Styles.toolbar.toolbarText}>
                <Text style={[Styles.text.text16, {fontWeight: '400'}]}>
                  {strings('greeting')}, {this.state.userName}
                </Text>
                <Text
                  style={[
                    Styles.text.text24,
                    {
                      fontWeight: '700',
                      maxWidth: (Styles.constants.widthScreen / 3) * 2,
                      lineHeight: 32,
                    },
                  ]}>
                  {strings('textToolbar')}
                </Text>
              </View>
              <TouchableOpacity
                style={Styles.search.searchInput}
                onPress={() => {
                  NavigationUtils.goToSearchProduct();
                }}>
                <TouchableOpacity>
                  <Image
                    style={{height: 20, width: 20}}
                    source={ImageHelper.search}
                  />
                </TouchableOpacity>
                <Text
                  style={Styles.search.searchText}
                  underlineColorAndroid="transparent">
                  {strings('textSearch')}
                </Text>
              </TouchableOpacity>
            </View>
          )}
          renderStickyHeader={() => (
            <View
              key="sticky-header"
              style={{
                height: 50,
                width: window.width,
                alignItems: 'center',
                top: Styles.constants.X * 0.8,
                justifyContent: 'flex-end',
              }}>
              <TouchableOpacity
                style={[
                  Styles.search.searchInput,
                  {width: '60%', marginVertical: 10},
                ]}
                onPress={() => {
                  NavigationUtils.goToSearchProduct();
                }}>
                <TouchableOpacity>
                  <Image
                    style={{height: 20, width: 20}}
                    source={ImageHelper.search}
                  />
                </TouchableOpacity>
                <Text
                  style={Styles.search.searchText}
                  underlineColorAndroid="transparent">
                  {strings('textSearch')}
                </Text>
              </TouchableOpacity>
            </View>
          )}
          renderFixedHeader={() => (
            <View
              key="fixed-header"
              style={{
                width: Styles.constants.widthScreenMg24,
                top: Styles.constants.X * 0.8,
                alignSelf: 'center',
                position: 'absolute',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                  <Icon
                    name="menu"
                    style="entypo"
                    size={25}
                    color={ColorStyle.tabWhite}
                  />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                  <Icon
                    name="shopping-cart"
                    style="entypo"
                    size={25}
                    color={ColorStyle.tabWhite}
                  />
                  {/*<Text style={{position:"absolute",right:0,top:0}}>1</Text>*/}
                  <BadgeView />
                </TouchableOpacity>
              </View>
            </View>
          )}>
          <View style={{flex: 1}}>
            <FlatList
              style={{flex: 1}}
              data={Dataa}
              ref="ListView"
              renderItem={item => (
                <View
                  style={{
                    overflow: 'hidden',
                    paddingHorizontal: 10,
                    height: ROW_HEIGHT,
                    backgroundColor: 'white',
                    borderColor: '#ccc',
                    borderBottomWidth: 1,
                    justifyContent: 'center',
                  }}>
                  <Text>{item.item}</Text>
                </View>
              )}
            />
          </View>
        </ParallaxScrollView>
      </View>
    );
  }
}
