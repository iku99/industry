import React from 'react'
import {Text, View} from "react-native";
import ColorStyle from "../../../resource/ColorStyle";
import {BarIndicator, DotIndicator,SkypeIndicator} from "react-native-indicators";
import {strings} from "../../../resource/languages/i18n";
import Styles from "../../../resource/Styles";
const X=Styles.constants.X
export default function LoadingClick({loading}){
    return(
        <View
            style={{
                position: 'absolute',
                display: loading ? 'flex' : 'none',
                flex: 1,
                alignItems:'center',
                justifyContent:'center',
                backgroundColor: ColorStyle.loading,
                width: '100%',
                height: '100%',
                top: 0,
                elevation:1
            }}>
            <View style={{width:'60%',
                alignItems:'center',
                height:X*3,
                backgroundColor:ColorStyle.tabActive,
                paddingVertical:X/5,
                shadowColor: ColorStyle.tabActive,
                shadowOffset: {
                    width: 0,
                    height: 1,
                },
                shadowOpacity: 0.22,
                shadowRadius: 2.22,
                borderRadius:X/5,
                elevation: 3,}}>
                <SkypeIndicator color={ColorStyle.tabWhite} size={50} />
                <Text style={{...Styles.text.text14,color:ColorStyle.tabWhite,fontWeight:'600'}}>{strings('loading')}</Text>
            </View>
        </View>
    )
}
