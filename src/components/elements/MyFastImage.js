import React, {Component} from 'react';
import {TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';

export default class MyFastImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
  }
  render() {
    if (this.props.onPress !== undefined) {
      return (
        <TouchableOpacity style={this.props.style} onPress={this.props.onPress}>
          <FastImage
            {...this.props}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          />
        </TouchableOpacity>
      );
    } else {
      return (
        <View style={this.props.style}>
          <FastImage
            {...this.props}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          />
        </View>
      );
    }
  }
}
