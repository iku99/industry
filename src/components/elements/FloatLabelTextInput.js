import React, {Component} from 'react';
import {Animated, StyleSheet, Text, TextInput, View} from 'react-native';
import PropTypes from 'prop-types';
import AppConstants from '../../resource/AppConstants';

class FloatingLabel extends Component {
  constructor(props) {
    super(props);

    let initialPadding = 9;
    let initialOpacity = 0;

    if (this.props.visible) {
      initialPadding = 5;
      initialOpacity = 1;
    }

    this.state = {
      paddingAnim: new Animated.Value(initialPadding),
      opacityAnim: new Animated.Value(initialOpacity),
    };
  }

  componentWillReceiveProps(newProps) {
    Animated.timing(this.state.paddingAnim, {
      toValue: newProps.visible ? 5 : 9,
      duration: 230,
    }).start();

    return Animated.timing(this.state.opacityAnim, {
      toValue: newProps.visible ? 1 : 0,
      duration: 230,
    }).start();
  }

  render() {
    return (
      <Animated.View
        style={[
          styles.floatingLabel,
          {paddingTop: this.state.paddingAnim, opacity: this.state.opacityAnim},
        ]}>
        {this.props.children}
      </Animated.View>
    );
  }
}

class TextFieldHolder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      marginAnim: new Animated.Value(this.props.withValue ? 10 : 0),
    };
  }

  componentWillReceiveProps(newProps) {
    return Animated.timing(this.state.marginAnim, {
      toValue: newProps.withValue ? 10 : 0,
      duration: 230,
    }).start();
  }

  render() {
    return (
      <Animated.View style={{marginTop: this.state.marginAnim}}>
        {this.props.children}
      </Animated.View>
    );
  }
}

class FloatLabelTextField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focused: false,
      text: this.props.valueText,
    };
  }

  componentWillReceiveProps(newProps) {
    if (
      newProps.hasOwnProperty('valueText') &&
      newProps.valueText !== this.state.text
    ) {
      this.setState({text: newProps.valueText});
    }
  }

  leftPadding() {
    return {width: this.props.leftPadding || 0};
  }

  withBorder() {
    if (!this.props.noBorder) {
      return this.state.focused ? styles.withBorderSelected : styles.withBorder;
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.viewContainer}>
          <View style={[styles.paddingView, this.leftPadding()]} />
          <View style={[styles.fieldContainer, this.withBorder()]}>
            <FloatingLabel visible={this.state.text}>
              <Text style={[styles.fieldLabel, this.labelStyle()]}>
                {this.placeholderValue()}
              </Text>
            </FloatingLabel>
            <TextFieldHolder withValue={this.state.text}>
              <TextInput
                underlineColorAndroid="transparent"
                style={[styles.valueText, {color: this.props.contentTextColor}]}
                defaultValue={this.state.text}
                value={this.state.text}
                maxLength={this.props.maxLength}
                onFocus={() => this.setFocus()}
                onBlur={() => this.unsetFocus()}
                onChangeText={value => this.setText(value)}
                placeholderTextColor={this.props.placeholderTextColor}
                keyboardType={this.props.keyboardTypeInput}
                placeholder={this.props.placeholder}
                secureTextEntry={this.props.secureTextEntry}
                {...this.props}
                returnKeyType={this.props.returnKeyType}
                onSubmitEditing={this.props.onSubmitEditing}
                blurOnSubmit={false}
                ref={this.props.refFunc}
              />
            </TextFieldHolder>
          </View>
        </View>
      </View>
    );
  }

  inputRef() {
    return this.refs.input;
  }

  focus() {
    this.inputRef().focus();
  }

  blur() {
    this.inputRef().blur();
  }

  isFocused() {
    return this.inputRef().isFocused();
  }

  clear() {
    this.inputRef().clear();
  }

  setFocus() {
    this.setState({
      focused: true,
    });
    try {
      return this.props.onFocus();
    } catch (_error) {}
  }

  unsetFocus() {
    this.setState({
      focused: false,
    });
    try {
      return this.props.onBlur();
    } catch (_error) {}
  }

  labelStyle() {
    if (this.state.focused) {
      return {color: this.props.focusedTextColor};
    }
    return {color: this.props.floatingTextColor};
  }

  placeholderValue() {
    return this.props.placeholder;
  }

  setText(value) {
    this.setState({
      text: value,
    });
    try {
      this.props.onChangeTextValue(value);
    } catch (_error) {}
  }
}

FloatLabelTextField.propTypes = {
  placeholderTextColor: PropTypes.string,
  contentTextColor: PropTypes.string,
  focusedTextColor: PropTypes.string,
  floatingTextColor: PropTypes.string,
};

FloatLabelTextField.defaultProps = {
  placeholderTextColor: '#595959',
  contentTextColor: '#000000',
  focusedTextColor: '#1482fe',
  floatingTextColor: '#a2a2a2',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 60,
    justifyContent: 'center',
    paddingHorizontal: 15,
    marginTop: 10,
  },
  viewContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  paddingView: {
    width: 15,
  },
  floatingLabel: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
  fieldLabel: {
    height: 15,
    fontSize: AppConstants.LABEL_SIZE,
  },
  fieldContainer: {
    flex: 1,
    justifyContent: 'center',
    position: 'relative',
  },
  withBorder: {
    borderBottomWidth: 1,
    borderColor: '#a2a2a2',
  },
  withBorderSelected: {
    borderBottomWidth: 1.5,
    borderColor: '#3b5998',
  },
  valueText: {
    height: 50,
    fontSize: AppConstants.TEXT_SIZE,
    marginTop: 8,
  },
  focused: {
    color: '#1482fe',
  },
});

export default FloatLabelTextField;
