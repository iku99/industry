import React, {Component} from "react";
import {Text, TouchableOpacity} from "react-native";
import {CheckBox} from "react-native-elements";
import ColorStyle from "../../../resource/ColorStyle";
import PropTypes from "prop-types";

export default class AppCheckBoxView extends Component {
    render() {
        let checked = this.props.checked;
        let canCheck = this.props.checked !== undefined ? this.props.checked : true;
        return (
            <TouchableOpacity
                style={{
                    paddingHorizontal: 10,
                    flexDirection: 'row',
                    alignItems: 'center',
                    flex:1,
                    ...this.props.containerStyle,
                }}
                onPress={() => {
                    this.onChange(!checked);
                }}>
                <CheckBox
                    checked={checked}
                    containerStyle={{borderColor: 'transparent', padding: 0}}
                    onPress={() => {
                        this.onChange(!checked);
                    }}
                    iconType="material-community"
                    uncheckedIcon={
                        this.props.isSquareType
                            ? 'checkbox-blank-circle-outline'
                            : 'checkbox-blank-outline'
                    }
                    uncheckedColor={ColorStyle.gray }
                    checkedIcon={
                        this.props.isSquareType
                            ? 'checkbox-marked-circle'
                            : 'checkbox-marked'
                    }
                    checkedColor={canCheck ? ColorStyle.tabActive : ColorStyle.gray}
                    textStyle={{fontWeight: 'normal'}}
                />
                {this.props.viewContainer}
            </TouchableOpacity>
        );
    }
    onChange(checked) {
        let canCheck =
            this.props.canCheck !== undefined ? this.props.canCheck : true;
        if (this.props.onChange != null && canCheck) {
            this.props.onChange(checked);
        }
    }
}
AppCheckBox.defaultProps = {
    checked: false,
    isSquareType: true,
};
AppCheckBox.propTypes = {
    checked: PropTypes.boolean,
    text: PropTypes.string.isRequired,
    onChange: PropTypes.function,
    containerStyle: PropTypes.object,
    isSquareType: PropTypes.boolean,
};
