import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import Styles from '../../resource/Styles';
import ColorStyle from '../../resource/ColorStyle';

export default class TitleLogin extends Component {
  render() {
    return (
      <View style={{width: '100%', left: 0, marginTop: 45, marginBottom: 40}}>
        <TouchableOpacity
          style={{
            padding: 5,
            width: 35,
            height: 35,
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => {
            Actions.pop();
          }}>
          <Icon name="chevron-left" type="feather" size={25} />
        </TouchableOpacity>
        <View style={{alignItems: 'center', marginTop: 36}}>
          <Text style={[Styles.text.text30, {color: ColorStyle.tabActive,fontWeight:'700'}]}>
            {this.props.title}
          </Text>
        </View>
      </View>
    );
  }
}
