import React, {Component} from 'react';
import {Image, Text, View} from 'react-native';
import ImageHelper from '../../../resource/images/ImageHelper';

export default class EmptyView extends Component {
  render() {
    return (
      <View
        style={{
            ...this.props.styles,
          alignItems: 'center',
          flex: 1,
          justifyContent:'center'
        }}>
        <Image source={ImageHelper.cartEmpty} resizeMode={'cover'} />
        <Text>{this.props.text}</Text>
      </View>
    );
  }
}
