
import Styles from "../../../resource/Styles";
import Colors from "../../../resource/ColorStyle";
const X = Styles.constants.X;
const width = Styles.constants.widthScreen;
const height = Styles.constants.heightScreen;
export default {
  container: {
    ...Styles.container,
  },
  selectProductContainer: {
    width: '100%',
    height: '90%',
    backgroundColor: 'white',
    borderRadius: 6,
    overflow: 'hidden',
  },
  selectProductContainerContent: {
    paddingTop: 35,
    paddingHorizontal: 10,
    height: '100%',
  },
  buttonText: {
    color: Colors.gray,
    marginTop: 40,
  },
  previousBT: {

    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.white,
    borderRadius: 20,
    borderWidth: 0,
    padding: 12,
    marginTop: 30,
    elevation: 5,
  },
  nextBT: {
    ...Colors.box,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.tabActive,
    borderRadius: 20,
    borderWidth: 0,
    padding: 12,
    marginTop: 30,
    elevation: 5,
  },
  closeButton: {
    position: 'absolute',
    top: 10,
    right: 10,
  },
};
