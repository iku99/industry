import React, {Component} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Icon} from 'react-native-elements';
import styles from "./styles";
import {strings} from "../../../resource/languages/i18n";
import EmptyView from "../reminder/EmptyView";
import Ripple from "react-native-material-ripple";
import ColorStyle from "../../../resource/ColorStyle";
import PostProductSelectItem from "../../screen/product/PostProductSelect";
import DataUtils from "../../../Utils/DataUtils";
import ProductHandle from "../../../sagas/ProductHandle";

let LIMIT = 2;
export default class SelectMyProduct extends Component {
  constructor(props) {
    super(props);
    let loadNew = true;
    let oldState = this.props.selectProductState;
    if (oldState === undefined || DataUtils.listNullOrEmpty(oldState.data)) {
      let initProducts = this.props.initProducts;
      if (initProducts === undefined) {
        initProducts = [];
      }
      initProducts.forEach((product, index) => {
        initProducts[index] = {
          ...product,
          checked: true,
        };
      });
      this.state = {
        initProducts,
        data: initProducts,
        loading: true,
        page: 0,
        refreshing: false,
        canLoadData: true,
        posting: false,
        initProduct: this.props.initProduct,
      };
    } else {
      loadNew = false;
      this.state = oldState;
    }
    if (loadNew) {
      setTimeout(() => this.getProductList(), 300);
    }
  }
  render() {
    return (
      <View style={styles.selectProductContainer}>
        <View style={styles.selectProductContainerContent}>
          <Text style={{fontWeight: '600', fontSize: 17}}>
            {strings('selectProductToTag')}
            <Text
              style={{fontWeight: 'normal', fontStyle: 'italic', fontSize: 14}}>
              {strings('selectProductToTagDes')}
              <Text
                onPress={() => {
                  if (this.props.onSelectProduct !== undefined) {
                    this.props.onSelectProduct();
                  }
                  this.closeView();
                }}
                style={styles.buttonText}>
                {strings('postProductNow')}
              </Text>
            </Text>
          </Text>
          {this.state.data.length > 0 && (
            <FlatList
              contentContainerStyle={{width: '100%'}}
              showsVerticalScrollIndicator={false}
              data={this.state.data}
              keyExtractor={this.keyExtractor}
              renderItem={this._renderItem}
              style={{flex: 1, backgroundColor: 'transparent', marginTop: 10}}
              ListFooterComponent={this.renderFooter.bind(this)}
              onEndReachedThreshold={0.4}
              onEndReached={() => this.handleLoadMore()}
              ref={ref => {
                this.listRef = ref;
              }}
              refreshing={this.state.refreshing}
            />
          )}
          {this.state.data.length === 0 && (
            <EmptyView containerStyle={{flex: 1}} title={strings('postNow')} />
          )}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
            }}>
            <View style={{flex: 1, marginHorizontal: 10}}>
              <Ripple
                onPress={() => {
                  this.closeView();
                }}
                style={{...styles.previousBT, marginTop: 10}}
                animation="fadeInUpBig">
                <Text style={{color: ColorStyle.previousText, fontSize: 14}}>
                  {strings('addProductCancel')}
                </Text>
              </Ripple>
            </View>
            <View style={{flex: 1, marginHorizontal: 10}}>
              <Ripple
                onPress={() => {
                  this.onTag();
                }}
                style={{...styles.nextBT, marginTop: 10}}
                animation="fadeInUpBig">
                <Text style={{color: 'white', fontSize: 14}}>
                  {strings('addProductDone')}
                </Text>
              </Ripple>
            </View>
          </View>
        </View>

        <TouchableOpacity
          style={styles.closeButton}
          onPress={() => {
            this.closeView();
          }}>
          <Icon
            name="close"
            type="font-awesome"
            size={20}
            color={ColorStyle.tabInactive}
          />
        </TouchableOpacity>
      </View>
    );
  }
  keyExtractor = (item, index) => index.toString();
  _renderItem = ({item, index}) => <View>{this.renderItem(item, index)}</View>;
  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <ActivityIndicator style={{color: '#000'}} />;
  };
  renderItem(item, index) {
    return (
      <PostProductSelectItem
        index={index}
        item={item}
        onChangeDataCallback={(item, index) => {
          let data = this.state.data;
          data[index] = item;
          this.setState({data});
        }}
      />
    );
  }
  handleLoadMore() {
    if (!this.state.canLoadData) {
      return;
    }
    if (this.state.loading) {
      return;
    }
    this.setState({loading: true, page: this.state.page + 1}, () =>
      this.getProductList(),
    );
  }
  getProductList() {
    let param = {
      page_index: this.state.page,
      page_size: LIMIT,
    };
    ProductHandle.getInstance().getProductStore(
        param,this.state.idStore,
        (isSuccess, responseData) => {
          let data = this.state.data;
          let newData = responseData.data;
          let newProduct = [];
          newData.forEach(product => {
            let initProducts = this.state.initProducts;
            let size = initProducts.length;
            let existIndex = -1;
            for (let i = 0; i < size; i++) {
              if (product.id === initProducts[i].id) {
                existIndex = i;
                break;
              }
            }
            if (existIndex !== -1) {
              initProducts = DataUtils.removeItem(initProducts, existIndex);
              this.setState({initProducts});
            } else {
              newProduct.push(product);
            }
          });
          data = data.concat(newProduct);
          let canLoadData = newData.length ===LIMIT;
          this.setState({data: data, canLoadData});
          this.setState({loading: false});
        },
    );

  }
  closeView() {
    if (this.props.onClose !== undefined) {
      this.state = this.props.selectProductState;
      this.props.onClose();
    }
  }
  onTag() {
    let data = this.state.data;
    let selectedData = [];
    data.forEach(product => {
      if (product.checked) {
        selectedData.push(product);
      }
    });
    if (this.props.onTag !== undefined) {
      this.props.onTag(selectedData, this.state);
    }
  }
}
