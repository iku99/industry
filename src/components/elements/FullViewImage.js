import Styles from '../../resource/Styles';
import React, {Component} from 'react';
import {TouchableOpacity, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import ViewUtils from '../../Utils/ViewUtils';
import {strings} from '../../resource/languages/i18n';
import DataUtils from '../../Utils/DataUtils';
import Carousel from 'react-native-snap-carousel';
import MyFastImage from './MyFastImage';
import ColorStyle from "../../resource/ColorStyle";

const BannerWidth = Styles.constants.widthScreen;
let currentIndex = 0;
export default class FullViewImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: this.props.images,
    };
    currentIndex = this.props.initIndex;
  }

  render() {
    let images = this.state.images;
    return (
      <View style={[{backgroundColor: 'black'}]}>
        {this.renderContent(images, currentIndex)}
        <TouchableOpacity
          style={{position: 'absolute', top: 50, left: 20}}
          onPress={() => {
            Actions.pop();
          }}>
          <Icon name="chevron-back-outline" type="ionicon" size={25} color={ColorStyle.tabWhite}/>
        </TouchableOpacity>
        {this.props.onDeleteImage != null && (
          <TouchableOpacity style={{position: 'absolute', top: 50, right: 20}} onPress={()=>{

            ViewUtils.showAskAlertDialog(strings('deleteImage'),()=>{
              if(this.props.onDeleteImage !== undefined){
                let images = this.state.images;
                images = DataUtils.removeItem(images, currentIndex);
                this.setState({images:[]}, ()=>this.setState({images}));
                this.props.onDeleteImage(currentIndex);
                if(images.length ===0)
                  Actions.pop()
              }
            },null)
          }}>
            <Icon name="trash" type="font-awesome" size={25} />
          </TouchableOpacity>
        )}
      </View>
    );
  }


  renderContent(images, currentIndex) {
    if (images.length === 1) {
      currentIndex = 0;
      return <View>{this.renderPage1(images[0], 0)}</View>;
    } else {
      return (
       <View style={{width:'100%',height:'100%'}}>
           <Carousel
               autoplay={false}
               index={currentIndex}
               sliderWidth={BannerWidth}
               itemWidth={BannerWidth}
               pageSize={BannerWidth}
               data={images}
               pageIndicatorStyle={{backgroundColor: 'rgba(255,255,255,0.4)'}}
              renderItem={this.renderPage}
               onPageChanged={index => {
                   currentIndex = index;
               }}/>
       </View>
      );
    }
  }
  renderPage({item, index}) {
    return (
      <MyFastImage
        key={index}
        style={{width: '100%', height: '100%'}}
        source={{
          uri: item,
          headers: {Authorization: 'someAuthToken'},
        }}
        resizeMode={'contain'}
      />
    );
  }
    renderPage1(image, index) {

        return (
            <MyFastImage
                key={index}
                style={{width: '100%', height: '100%'}}
                source={{
                    uri: image,
                    headers: {Authorization: 'someAuthToken'},
                }}
                resizeMode={'contain'}
            />
        );
    }
}
