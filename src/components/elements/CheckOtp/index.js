import React, {Component} from "react";
import {Text, TouchableOpacity, View} from "react-native";
import ColorStyle from "../../../resource/ColorStyle";
import Styles from "../../../resource/Styles";
import OtpInputs from "react-native-otp-inputs";
import {strings} from "../../../resource/languages/i18n";
import TitleLogin from "../TitleLogin";
import AccountHandle from "../../../sagas/AccountHandle";
import {EventRegister} from "react-native-event-listeners";
import AppConstants from "../../../resource/AppConstants";
import {Actions} from "react-native-router-flux";
import ViewUtils from "../../../Utils/ViewUtils";
let TimeReSenOTP = 120;
export default class CheckOtp extends Component{
    constructor(props) {
        super(props);
        this.state={
            userName:this.props.params.userName,
            otp:'',
            validOtp: false,
            minute: 2,
            second: 0,
        }
    }

    componentDidMount() {
        this.timeSendOtp()
    }

    timeSendOtp = () => {
        setInterval(() => {
            TimeReSenOTP = TimeReSenOTP - 1;
            if (TimeReSenOTP >= 0) {
                this.setState({
                    minute: Math.floor(TimeReSenOTP / 60 === 60 ? 0 : TimeReSenOTP / 60),
                });
                let second = TimeReSenOTP % 60;
                this.setState({second});
            }
        }, 1000);
        if (TimeReSenOTP === 0) {
            clearInterval(this.timeSendOtp());
            return;
        }
    };
    sendOtp(){
        let params={
            user_name: this.state.userName
        }
        AccountHandle.getInstance().verifyUserName(
            params,
            (isSuccess, dataResponse) => {
                if (isSuccess) {
                    this.setState({dialogMsg:''})
                    this.timeSendOtp()
                    return;

                }else
                    this.setState({dialogMsg:strings('errUserNameNotServer')})
            },
        );
    }
    renderTime(){
        let minute = '0' + this.state.minute;
        let second =
            this.state.second < 10 ? '0' + this.state.second : this.state.second;
        let time = minute + ':' + second;
        if(minute !== '00' && second !=='00'){
            return(
                <Text style={{textAlign: 'center', marginTop: 16}}>{time}</Text>
            )
        }else {
            return (
                <View style={{flexDirection:'row',alignItems:'center',justifyContent:'flex-start',marginTop:20}}>
                    <Text
                        style={{color: ColorStyle.tabBlack }}>
                        {strings('sendLai')}
                    </Text>
                    <TouchableOpacity onPress={()=>this.sendOtp()}>
                        <Text
                            style={{
                                color: ColorStyle.tabActive,
                                textAlign: 'left',
                                fontWeight:'700',
                                marginLeft:10
                            }}>
                            {strings('sendLai1')}
                        </Text></TouchableOpacity>
                </View>
            )
        }
    }
    checkOtp(){
        if(this.state.otp.length<6) return this.setState({dialogMsg:strings('errOTP')})
        let params={
            otp: this.state.otp
        }
        AccountHandle.getInstance().checkOTP(
            params,
            (isSuccess, dataResponse) => {
                if (isSuccess) {
                    AccountHandle.getInstance().login(
                        this.props.params,
                        (isSuccess, dataResponse) => {
                            if (isSuccess) {
                                ViewUtils.showAskAlertDialog(
                                    strings('registerSuccess'),
                                    () => {  EventRegister.emit(AppConstants.EventName.LOGIN, true);
                                        Actions.reset('drawer')},undefined)

                            }
                        },
                    );
                    return;
                }else
                    this.setState({dialogMsg:strings('errOTP1')})
            },
        );
    }
    render() {
        let minute = '0' + this.state.minute;
        let second =
            this.state.second < 10 ? '0' + this.state.second : this.state.second;
        return(
            <View style={Styles.container}>
                <TitleLogin title={strings('checkOTP')} />
                <View style={{alignItems: 'center',marginTop:20}}>

                    <Text style={{fontSize:16,color:ColorStyle.tabActive,fontWeight:'600',marginVertical:20}}>{strings('txtUserName')} {this.state.userName}</Text>
                    <OtpInputs
                        style={{
                            height: 50,
                            width: '90%',
                            flexDirection: 'row',
                        }}
                        inputStyles={{
                            borderRadius: 8,
                            borderColor: ColorStyle.tabActive,
                            borderWidth: 1.5,
                            marginRight: 5.15,
                            height: 50,
                            width: (Styles.constants.widthScreen - 40) / 6 - 3.5,
                            textAlign: 'center',
                            color:ColorStyle.tabActive
                        }}
                        handleChange={code => this.setState({otp:code})}
                        numberOfInputs={6}
                    />

                    <Text
                        style={{
                            display: this.state.dialogMsg === '' ? 'none' : 'flex',
                            color: 'red',
                            textAlign: 'left',
                            marginTop: 5,
                            width: Styles.constants.widthScreen - 40,
                        }}>
                        {this.state.dialogMsg}
                    </Text>

                    <TouchableOpacity
                        disabled={minute === '00' && second==='00'}
                        onPress={()=>this.checkOtp()}
                        style={[
                            Styles.button.buttonLogin,
                            {
                                backgroundColor: ColorStyle.tabActive,
                                borderColor: 'rgba(250, 111, 52, 0.5)',
                                borderWidth: 1,
                                marginTop: 20,
                            },
                        ]}
                        animation="fadeInUpBig">
                        <Text style={[Styles.text.text18, {fontWeight: '500'}]}>
                            {strings('xd')}
                        </Text>
                    </TouchableOpacity>
                    {this.renderTime()}
                </View>
            </View>
        )
    }
}
