import React, {Component} from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Carousel from 'react-native-banner-carousel-updated';
import constants from '../../../Api/constants';
import Styles from "../../../resource/Styles";
import BannerLoading from "../SkeletonPlaceholder/BannerLoading";
const BannerWidth = Dimensions.get('window').width;
const BannerHeight = (Styles.constants.widthScreen * 2.5) / 3;

export default class BannerProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.item?.image_url,
      index: 0,
    };
  }

  renderPage(banner, index) {
    let url = '';
    if (banner === undefined || banner.url === undefined) {
      url = 'https://picsum.photos/300/500?productId=' + index;
    } else {
      url = constants.host + banner.url;
    }

    return (
      <View key={index}>
        <Image
          style={{width: BannerWidth, height: BannerHeight}}
          source={{uri: url}}
        />
      </View>
    );
  }

  render() {
     let data=this.props.item?.image_url
   if(data === undefined || data.length===0){
     return (
        <View/>
     )
   }
   else{
       return (
           <View style={styles.container}>
               <Carousel
                   autoplay
                   autoplayTimeout={3000}
                   loop
                   showsPageIndicator={false}
                   index={this.state.index}
                   onPageChanged={i => {
                       this.setState({index: i});
                   }}
                   pageSize={BannerWidth}>
                   {data.map((image, index) => this.renderPage(image, index))}
               </Carousel>
               <View
                   style={{width: 50, backgroundColor: 'rgba(51, 51, 51, 0.65)', flexDirection: 'row', borderRadius:10, justifyContent:"center", position: 'absolute', bottom: 0, right: 0}}>
                   <Text>
                       {this.state.index + 1}/{data.length}
                   </Text>
               </View>
           </View>
       );
   }
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
});
