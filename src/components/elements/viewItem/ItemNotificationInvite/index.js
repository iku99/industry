import React from "react";
import {Image, Text, TouchableOpacity, View} from "react-native";
import constants from "../../../../Api/constants";
import ColorStyle from "../../../../resource/ColorStyle";
import DataUtils from "../../../../Utils/DataUtils";
import Styles from "../../../../resource/Styles";
import {Icon} from "react-native-elements";
import DateUtil from "../../../../Utils/DateUtil";
import styles from "./styles";
import {strings} from "../../../../resource/languages/i18n";
export default function ItemNotificationInvite({item,onNotificationClick,index}){

    function onAccept(){

    }

    return(
        <View
            style={styles.body}>
                <View style={styles.images}>
                    <Icon
                        name="bell"
                        type="font-awesome"
                        color={ColorStyle.tabActive}
                        size={20}
                    />
                </View>
            <View style={styles.viewInfo}>
                <Text style={{...Styles.text.text14, color: ColorStyle.tabBlack ,fontWeight:'600'}}>
                    {item.name}
                </Text>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                        flex: 1,
                    }}>
                    <Icon
                        name="clock"
                        type="feather"
                        size={10}
                        color={ColorStyle.gray}
                    />
                    <Text
                        style={[
                            Styles.text.text10,
                            {fontWeight: '400', color: ColorStyle.gray, marginLeft: 5},
                        ]}>
                        {DateUtil.formatDate('HH:mm DD.MM.YYYY', item.created_at)}
                    </Text>
                </View>
                <Text style={{...Styles.text.text12, color: ColorStyle.tabBlack}} >
                    {item.content}
                </Text>
                <View style={styles.viewBtn}>
                    <TouchableOpacity style={styles.btnCancel}>
                        <Text style={styles.textCancel}>{strings('reject')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnAccept}>
                        <Text style={styles.textAccept}>{strings('accept')}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}
