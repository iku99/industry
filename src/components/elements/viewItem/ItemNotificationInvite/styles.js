import Styles from "../../../../resource/Styles";
import ColorStyle from "../../../../resource/ColorStyle";

const X =Styles.constants.X
export default {
    body:{
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor:ColorStyle.tabWhite,

        padding: X * 0.3,
        marginVertical:X/6
    },
    images:{
        width:'20%',
    },
    viewInfo:{
        width:'80%',
    },
    viewBtn:{
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        height:X,
    },
    btnCancel:{
        backgroundColor: ColorStyle.tabWhite,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        paddingVertical: X/5,
        paddingHorizontal: X/3,
        elevation: 3,
    },
    btnAccept:{
        backgroundColor: ColorStyle.tabActive,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        paddingVertical: X/5,
        paddingHorizontal: X/3,
        elevation: 3,
    },
    textCancel:{
        ...Styles.text.text12,
        color:ColorStyle.tabBlack,
        fontWeight:'600'
    },
    textAccept:{
        ...Styles.text.text12,
        color:ColorStyle.tabWhite,
        fontWeight:'600'
    },
}
