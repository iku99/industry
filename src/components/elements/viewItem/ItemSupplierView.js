import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Ripple from 'react-native-material-ripple';
import ViewProductStyle from './ViewedProduct/ViewProductStyle';
import ColorStyle from '../../../resource/ColorStyle';
import NavigationUtils from '../../../Utils/NavigationUtils';
import MyFastImage from '../MyFastImage';
import AppConstants from '../../../resource/AppConstants';
import constants from '../../../Api/constants';
import Styles from '../../../resource/Styles';
import {Icon} from 'react-native-elements';
import {strings} from '../../../resource/languages/i18n';
import MediaUtils from "../../../Utils/MediaUtils";
const X = Styles.constants.X;
export default class ItemSupplierView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      favorite: 0,
    };
  }

  render() {
    return (
      <Ripple
        style={[
          ViewProductStyle.container,
          {
            width: this.getItemWidth(),
            height: X * 5.5,
            paddingHorizontal: 0,
            backgroundColor: ColorStyle.tabWhite,
            borderColor:'#014B56',
            borderWidth:1,
            borderRadius:5,
          },
        ]}
        onPress={() => {
            NavigationUtils.goToShopDetail(this.props.item.id)
        }}>
        <Image
          source={MediaUtils.getStoreCover(this.props.item.image_url,this.props.item?.id)}
          style={{width: '100%', height: X * 1.7, position: 'absolute', top: 0}}
        />
        <View
          style={{
            marginTop: (X * 1.7) / 2,
            alignSelf: 'center',
            alignItems: 'center',
          }}>
          <Image
              source={MediaUtils.getStoreAvatar(this.props.item?.avatar)}
            style={[Styles.icon.avatarGroup]}
          />
          <Text
              style={
                  {...Styles.text.text18,fontWeight: '700', color: ColorStyle.tabBlack,textAlign:'center'}}
              numberOfLines={2}>
            {this.props.item?.name}
          </Text>
          <Text
              numberOfLines={4}
            style={
              {...Styles.text.text10,margin:10,fontWeight: '400', color: 'rgba(0, 0, 0, 0.6)'}
            }>
            {this.props.item?.content}
          </Text>
        </View>

      </Ripple>
    );
  }

  getImageUrl(images) {
    if (images != null && images.length > 0) {
      return constants.host + images[0].thumbnail_url;
    } else {
      return 'https://picsum.photos/130/214?' + this.props.index;
    }
  }

  getItemWidth() {
    return this.props.itemWidth == null
      ? AppConstants.defaultViewedProductItemWidth
      : this.props.itemWidth;
  }
}
