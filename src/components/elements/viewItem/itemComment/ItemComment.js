import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import CurrencyFormatter from '../../../../Utils/CurrencyFormatter';
import AppConstants from '../../../../resource/AppConstants';
import MediaUtils from '../../../../Utils/MediaUtils';
import MyFastImage from '../../MyFastImage';
import LinearGradient from 'react-native-linear-gradient';
import constants from '../../../../Api/constants';
import FuncUtils from '../../../../Utils/FuncUtils';
import GroupHandle from '../../../../sagas/GroupHandle';
import {strings} from '../../../../resource/languages/i18n';
import {Icon} from 'react-native-elements';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
import RatingStar from '../../../RatingStar';
import ImageHelper from "../../../../resource/images/ImageHelper";

export default class ItemComment extends Component {
  render() {
    let data = this.props.item;
    let avatar;
    let name;
    if(data.customer===null){
        avatar='';
        name='not name'
    }else {
        name=data.customer?.full_name,
            avatar=MediaUtils.getAvatarUrl(data)
    }
    return (
      <View
        style={{
          flexDirection: 'row',
          paddingVertical: 8,
          marginVertical:20
        }}>
        <View style={{flex: 1, alignItems: 'center'}}>
          <Image
            style={[
              Styles.icon.iconGroup,
              {borderRadius: 50, marginBottom: 10},
            ]}
            source={avatar}
          />
        </View>
        <View style={{flex: 4}}>
          <Text
            style={{
              ...Styles.text.text12,
              color: ColorStyle.tabBlack,
              fontWeight: '700',
            }}>
            {name}
          </Text>
          <Text
            style={{
              ...Styles.text.text10,
              fontWeight: '400',
              marginVertical: 5,
            }}>
            {data.des}
          </Text>
          <RatingStar size={15} rating={data.rate} />
          {/*<View style={{flexDirection: 'row', marginTop: 14}}>*/}
          {/*  {data.images.map(i => (*/}
          {/*    <Image*/}
          {/*      source={{uri: this.getImageUrl(i)}}*/}
          {/*      style={{*/}
          {/*        ...Styles.icon.img_comment,*/}
          {/*        alignItems: 'flex-start',*/}
          {/*        marginHorizontal: 5,*/}
          {/*      }}*/}
          {/*    />*/}
          {/*  ))}*/}
          {/*</View>*/}
        </View>
      </View>
    );
  }
  getImageUrl(images) {
    return constants.host + images.url;
  }
  // getImageUrl() {
  //   let cover = this.props.item.cover;
  //   if (cover !== undefined && cover.thumbnail_url !== undefined) {
  //     return constants.host + cover.thumbnail_url;
  //   }
  //   return 'https://picsum.photos/130/214?' + this.props.index;
  // }
  getGroupName() {
    return this.props.item.name;
  }
  getAvatarUrl() {
    let avatar = this.props.item.avatar;
    if (avatar !== undefined) {
      // return constants.host + avatar.thumbnail_url;
      return avatar;
    }
    return 'https://picsum.photos/130/214?' + this.props.index;
  }
}
