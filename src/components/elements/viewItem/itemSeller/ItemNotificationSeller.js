import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
import constants from '../../../../Api/constants';
const X = Styles.constants.X;
export default class ItemNotificationSeller extends Component {
  render() {
    let data = this.props.item;
    return (
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          backgroundColor: ColorStyle.tabWhite,
          paddingVertical: X * 0.3,
          paddingHorizontal: X * 0.3,
          borderRadius:15,
          marginVertical:6,
          elevation:2,
          borderColor:ColorStyle.borderItemHome
        }}
        //                   onPress={()=>
        //     CategoryHandle.getInstance().goToProductCategoryId(this.props.item.id)
        // }
      >
        <Image
          source={{uri: this.getImageUrl()}}
          style={{...Styles.icon.icon_notification}}
        />
        <View style={{marginLeft: X * 0.4, justifyContent: 'space-between'}}>
          <Text style={{...Styles.text.text12, color: ColorStyle.tabBlack,fontWeight:'700'}}>
            {data.title}
          </Text>
          <Text
            style={{
              ...Styles.text.text10,
              maxWidth: X * 5.9,
              lineHeight: X * 0.3,
            }}>
            {data.description}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
  getImageUrl() {
    return constants.host + this.props.item.url;
  }
}
