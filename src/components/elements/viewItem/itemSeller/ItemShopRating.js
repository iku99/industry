import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
import constants from '../../../../Api/constants';
import { Icon } from "react-native-elements";
import DateUtil from "../../../../Utils/DateUtil";
import AppConstants from "../../../../resource/AppConstants";
import MediaUtils from "../../../../Utils/MediaUtils";
const X = Styles.constants.X;
export default class ItemShopRating extends Component {
  render() {
    let data = this.props.item;
    return (
      <TouchableOpacity
        style={{

          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center', width:Styles.constants.widthScreenMg24,
          backgroundColor: ColorStyle.tabWhite,
          paddingVertical: X * 0.3,
          paddingHorizontal: X * 0.3,
          borderRadius:15,
          marginVertical:6,
          elevation:2,
          borderColor:ColorStyle.borderItemHome
        }}
        //                   onPress={()=>
        //     CategoryHandle.getInstance().goToProductCategoryId(this.props.item.id)
        // }
      >
        <Image
          source={MediaUtils.getAvatar(data.customer)}
          style={{...Styles.icon.icon_notification}}
        />
        <View style={{marginLeft: X * 0.4, justifyContent: 'space-between'}}>
          <Text style={{...Styles.text.text12, color: ColorStyle.tabBlack,fontWeight:'700'}}>
            {data.customer.full_name}
          </Text>
          <Text
            style={{
              ...Styles.text.text10,
              maxWidth: X * 5.9,
              lineHeight: X * 0.3,
            }}>
            {this.props.type===AppConstants.typeRating.SHOP?data.des:data.response}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
              flex: 1,
            }}>
            <Icon name="clock" type="feather" size={10} color={ColorStyle.gray}/>
            <Text
              style={[
                Styles.text.text10,
                {fontWeight: '400', color: ColorStyle.gray, marginLeft: 5},
              ]}>
              {DateUtil.formatDate('HH:mm DD.MM.YYYY', data.created_at)}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
