import React, {useEffect} from "react";
import {Text, TextInput, TouchableOpacity, View} from "react-native";
import ColorStyle from "../../../../resource/ColorStyle";
import {Icon} from "react-native-elements";
import styles from "./styles";
import Styles from "../../../../resource/Styles";
import {strings} from "../../../../resource/languages/i18n";
import TextInputUnit from "../TextInputUnit";
import {TextInputMask} from "react-native-masked-text";
export default function TextInputInfoItem({name,value, onChangeName, placeholder,
                                           title,nameOption,onChangeValue,onDelete}){



    function renderInput(value,placeholder,callback){
        return(
            <View style={styles.containerInput}>
                <TextInput
                    style={styles.textInput}
                    placeholder={placeholder}
                    value={value}
                    placeholderTextColor={ColorStyle.textInput}
                    onChangeText={text => {
                        callback(text)
                    }}
                />
            </View>
        )
    }
    return(
        <View style={styles.container}>
            <View style={{...styles.itemRow,}}>
               <Text>{title}</Text>
            <TouchableOpacity onPress={()=>onDelete()}>
                <Icon name={'close'} size={20}/>
            </TouchableOpacity>
            </View>
            <View style={{...styles.itemRow,}}>
                {renderInput(name,nameOption,text=>onChangeName(text))}
                {renderInput(value,strings('value'),text=>onChangeValue(text))}
            </View>

        </View>
    )
}
