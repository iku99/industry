import React, {useEffect} from "react";
import {Text, TextInput, View} from "react-native";
import ColorStyle from "../../../../resource/ColorStyle";
import {Icon} from "react-native-elements";
import styles from "./styles";
import Styles from "../../../../resource/Styles";
import {strings} from "../../../../resource/languages/i18n";
export default function TextInputContent({value, onChange, placeholder,
                                           title,keyboardType}){
    return(
        <View style={styles.container}>
            <Text
                style={styles.title}>
                {title}*
            </Text>
            <TextInput
                style={styles.textInput}
                placeholder={placeholder}
                value={value}
                keyboardType={keyboardType===undefined?'email-address':keyboardType}
                placeholderTextColor={ColorStyle.textInput}
                onChangeText={text => {
                    onChange(text)
                }}
                textAlignVertical={'top'}
                multiline
                numberOfLines={5}
                editable
            />
        </View>
    )
}
