import Styles from "../../../../resource/Styles";
import ColorStyle from "../../../../resource/ColorStyle";

const width= Styles.constants.X;
export default {
    icon:{
        width:width,
        height:width,
        aspectRatio: 1,
        borderRadius:width,
        overflow:'hidden',
    },
    name:{
        ...Styles.text.text12,
        textAlign:'center',
        color:ColorStyle.tabBlack,
        marginTop:12,
    },
    container:{
        width:85,
        justifyContent:'center', alignItems:'center', padding:5,
        paddingHorizontal: 15
    },
    imageContainer: {
        padding: 1,
        justifyContent:'flex-start',
        alignItems:'flex-start',
    }
}
