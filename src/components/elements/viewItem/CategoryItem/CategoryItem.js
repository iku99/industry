import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import CategoryStyle from './CategoryStyle';
import MyFastImage from '../../MyFastImage';
import constants from '../../../../Api/constants';
import {EventRegister} from 'react-native-event-listeners';
import AppConstants from '../../../../resource/AppConstants';

export default class CategoryItem extends Component {
  render() {
    return (
      <TouchableOpacity
        style={CategoryStyle.container}
        onPress={() => {
          EventRegister.emit(AppConstants.EventName.GO_TO_TAB, 2);
          EventRegister.emit(AppConstants.EventName.GO_TO_CATEGORY, this.props.index);
        }}>
        <View style={CategoryStyle.imageContainer}>
          <MyFastImage
            style={CategoryStyle.icon}
            source={{
              uri: this.getImageUrl(this.props.item.image_url,this.props.index),
              headers: {Authorization: 'someAuthToken'},
            }}
            resizeMode={'cover'}
          />
        </View>
        <Text
          style={{...CategoryStyle.name, marginBottom: 15}}
          numberOfLines={2}
          ellipsizeMode="tail">
          {this.props.item.name}
        </Text>
      </TouchableOpacity>
    );
  }
    getImageUrl(images,index) {
        if (images != null ) {
            return constants.host + images;
        } else {
            return 'https://picsum.photos/130/214?' + index;
        }
    }
}
