import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import {Icon} from 'react-native-elements';
import CartUtils from '../../../../Utils/CartUtils';
import MyFastImage from '../../MyFastImage';
import ProductUtils from '../../../../Utils/ProductUtils';
import CurrencyFormatter from '../../../../Utils/CurrencyFormatter';
import {strings} from '../../../../resource/languages/i18n';
import TimeUtils from '../../../../Utils/TimeUtils';
import DataUtils from '../../../../Utils/DataUtils';
import constants from '../../../../Api/constants';
import DateUtil from '../../../../Utils/DateUtil';
import MediaUtils from '../../../../Utils/MediaUtils';

export default class ItemOrderSeller extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: this.props.item,
    };
  }
  componentDidMount() {}

  render() {
    let hideActionBT = this.props.hideActionBT === true;
    let total = 0;
    let numProduct = 0;
    let order = this.state.item;
    let index = this.props.index;
    if (order.details === undefined) {
      return null;
    }
    console.log('order:',order);
    let productList = order.details.map((item, _) => {
      let product = item.product;

      total = total + item.price;
      numProduct = numProduct + item.quantity;
      return (
        <TouchableOpacity
          key={`ItemOrderSeller+${_ + toString()}`}
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            marginBottom: 10,
            paddingVertical: 10,
          }}
          onPress={() => {
            // NavigationUtils.goToProductDetail(products.product_snapshot);
          }}>
          <MyFastImage
            style={{flex: 1, height: 70}}
            source={{
              uri: ProductUtils.getImages(product)[0],
              headers: {Authorization: 'someAuthToken'},
            }}
            resizeMode={'cover'}
          />
          <View style={{marginLeft: 10, flex: 3}}>
            <Text
              style={[
                Styles.text.text14,
                {
                  fontWeight: '500',
                },
              ]}>
              {product.name}
            </Text>
            <Text
              style={[
                Styles.text.text11,
                {
                  fontWeight: '400',
                  color: ColorStyle.gray,
                  textAlign: 'right',
                },
              ]}>
              x{item.quantity}
            </Text>
            <Text
              style={[
                Styles.text.text11,
                {
                  fontWeight: '400',
                  textAlign: 'right',
                  color: ColorStyle.tabActive,
                },
              ]}>
              {CurrencyFormatter(product.price)}
            </Text>
          </View>
        </TouchableOpacity>
      );
    });
    return (
      <TouchableOpacity
        style={{
          paddingVertical: 16,
          paddingHorizontal: Styles.constants.marginHorizontalAll,
          backgroundColor: ColorStyle.tabWhite,
          marginTop: 10,
          borderColor: ColorStyle.borderItemHome,
          elevation: 2,
        }}
        onPress={() =>
          NavigationUtils.goToOrderDetailSeller(order, hideActionBT, type => {
            if (this.props.orderFunc != null) {
              this.props.orderFunc(order, type);
            }
          })
        }>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            style={{flexDirection: 'row', alignItems: 'center'}}
            onPress={() => {
              this.goToStore(order);
            }}>
            <Image
              source={MediaUtils.getAvatar(order.customer)}
              style={{...Styles.icon.iconOrder, borderRadius: 50}}
            />
            <Text
              style={[
                Styles.text.text14,
                {
                  fontWeight: '500',
                  color: ColorStyle.tabBlack,
                  marginLeft: 10,
                },
              ]}
              numberOfLines={1}>
              {order.customer.full_name}
            </Text>
          </TouchableOpacity>
          <Text
            style={[
              Styles.text.text14,
              {
                fontWeight: '500',
                color: CartUtils.getColorText(order.status),
              },
            ]}
            numberOfLines={1}>{`${CartUtils.getCartStatus(
            order.status,
          )}`}</Text>
        </View>
        {productList}
        <TouchableOpacity
          onPress={() =>
            NavigationUtils.goToOrderDetailSeller(order, hideActionBT, type => {
              if (this.props.orderFunc != null) {
                this.props.orderFunc(order, type);
              }
            })
          }
          style={{
            paddingVertical: 8,
          }}>
          <Text
            style={[
              Styles.text.text11,
              {
                fontWeight: '400',
                color: ColorStyle.gray,
                textAlign: 'center',
              },
            ]}>
            {strings('viewMoreProduct')}
          </Text>
        </TouchableOpacity>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingVertical: 5,
            borderTopWidth: 1,
            borderBottomWidth: 1,
            borderColor: ColorStyle.borderItemHome,
          }}>
          <Text
            style={[
              Styles.text.text11,
              {fontWeight: '500', color: ColorStyle.optionTextColor},
            ]}>
            {numProduct} {strings('product1')}
          </Text>
          <Text
            style={[
              Styles.text.text12,
              {fontWeight: '500', color: ColorStyle.tabBlack},
            ]}>
            {strings('totalPrice')}:
            <Text
              style={[
                Styles.text.text12,
                {fontWeight: '500', color: ColorStyle.tabActive},
              ]}>
              {CurrencyFormatter(total + order.shipping_fee)}
            </Text>
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingTop: 10,
            alignItems: 'center',
          }}>
          <Text style={{...Styles.text.text11, color: ColorStyle.tabBlack}}>
            {strings('orderID')}
          </Text>
          <Text style={{...Styles.text.text12, color: ColorStyle.tabBlack}}>
            {order.code}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  goToStore(order) {
    NavigationUtils.goToShopDetail(order.store_id);
  }
  getImageUrl(item, index) {
    let url = item.avatar;
    return DataUtils.stringNullOrEmpty(url)
      ? 'https://picsum.photos/50/50?avatar' + index
      : constants.host + url;
  }
  // getStoreDetail(storeId) {
  //   StoreHandle.getInstance().getStoreDetail(storeId, (isSuccess, store) => {
  //     if (isSuccess) {
  //       this.getABc(store.name);
  //     }
  //
  //   });
  // }
}
