import React, {Component, useEffect, useState} from 'react';
import {Alert, Image, Text, TouchableOpacity, View} from 'react-native';
import ColorStyle from '../../../../resource/ColorStyle';
import Accordion from 'react-native-collapsible/Accordion';
import {CheckBox, Icon} from 'react-native-elements';
import Styles from '../../../../resource/Styles';
import StoreHelper from '../../../../Utils/StoreHelper';
import ProductUtils from '../../../../Utils/ProductUtils';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import CurrencyFormatter from '../../../../Utils/CurrencyFormatter';
import {strings} from '../../../../resource/languages/i18n';
import QuantityControl from '../../QuantityControl';
import CartHandle from '../../../../sagas/CartHandle';
import ViewUtils from '../../../../Utils/ViewUtils';
import constants from '../../../../Api/constants';
import styles from './styles';
import ShipHandle from '../../../../sagas/ShipHandle';
import ShippingUtils from '../../../../Utils/ShippingUtils';
import DateUtil from '../../../../Utils/DateUtil';
import DataUtils from '../../../../Utils/DataUtils';
import AppConstants from '../../../../resource/AppConstants';
import VoucherUtils from '../../../../Utils/VoucherUtils';
import ImageHelper from '../../../../resource/images/ImageHelper';
export default function CartItemCheckout({
  item,
  address,
  index,
  callback,
  dataVoucher,
  onChange,
  onVoucher,
}) {
  const [itemCart, setItemCart] = useState();
  const [itemAddress, setItemAddress] = useState();
  const [data, setData] = useState(undefined);
  const [dataItem, setDataItem] = useState();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setItemCart(item);
    setItemAddress(address);
  }, [itemCart]);
  useEffect(() => {
    getShipping(item, address);
  }, [address]);
  const getShipping = (valuesItemCart, valuesAddress) => {
    if (valuesAddress !== undefined) {
      let params = {
        ship_unit_id: valuesItemCart?.store?.ship_unit_id,
        from_district: valuesItemCart?.store?.address?.district?.ghn_id,
        to_district: valuesAddress?.address_detail?.district?.ghn_id,
        to_ward_code: valuesAddress?.address_detail?.ward?.ghn_id,
        products: valuesItemCart.products,
        insurance_value: valuesItemCart.total,
        coupon: null,
      };
      setDataItem(params);
      ShipHandle.getInstance().billShip(params, (isSuccess, res) => {
        console.log('res:',res);
        if (isSuccess && res.code === 0) {
          setData(res.data[0]);
          callback(res.data[0], false);
          setLoading(false);
        }
      });
    }
  };
  function renderProduct() {
    let product = item.products.map((product, index) => {
      return (
        <TouchableOpacity
          key={`checkoutItem_${index.toString()}`}
          onPress={() => {
            NavigationUtils.goToProductDetail(product);
          }}
          style={{
            flexDirection: 'row',
            margin: 15,
            paddingHorizontal: Styles.constants.X / 5,
          }}>
          <View style={{alignItems: 'flex-start'}}>
            <Image
              source={{
                uri: ProductUtils.getImageUrl(
                  product?.image_url,
                  product?.index,
                ),
                headers: {Authorization: 'someAuthToken'},
              }}
              style={{width: 80, height: 80}}
            />
          </View>
          <View
            style={{flex: 3, justifyContent: 'space-between', marginLeft: 10}}>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
                fontWeight: '500',
              }}>
              {product?.name?.trim()}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  ...Styles.text.text14,
                  color: ColorStyle.tabBlack,
                  fontWeight: '500',
                }}>
                {CurrencyFormatter(product.price)}
              </Text>
              <Text
                style={{
                  ...Styles.text.text14,
                  color: ColorStyle.tabBlack,
                  fontWeight: '500',
                }}>
                x{product.quantity}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      );
    });
    return product;
  }
  function renderVoucher() {
    return (
      <TouchableOpacity onPress={() => onVoucher()} style={styles.bodyVoucher}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <Image
            source={
              item?.voucher_store === undefined
                ? ImageHelper.voucher
                : ImageHelper.selectVoucher
            }
            style={{
              width: Styles.constants.X / 1.5,
              height: Styles.constants.X / 2,
              marginRight: 10,
            }}
          />
          <Text style={{...styles.textTitle}}>{strings('voucherStore')}</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <Text
            style={{
              ...styles.textDes,
              color:
                item?.voucher_store === undefined
                  ? ColorStyle.gray
                  : ColorStyle.tabBlack,
            }}>
            {item?.voucher_store === undefined
              ? strings('selectVoucher')
              : CurrencyFormatter(-item?.voucher_store?.reduce_value)}
          </Text>
          <Icon
            name={'right'}
            type={'antdesign'}
            color={
              dataVoucher === undefined ? ColorStyle.gray : ColorStyle.tabBlack
            }
            size={15}
          />
        </View>
      </TouchableOpacity>
    );
  }
  function renderShip() {
    if (loading) {
      return <View />;
    } else {
      return (
        <View
          onPress={() => {
            NavigationUtils.goToListShipping(dataItem, item => {
              onChange(item);
              setData(item);
            });
          }}
          style={styles.bodyShipping}>
          <Text
            style={{
              ...Styles.text.text15,
              color: ColorStyle.shipping,
              fontWeight: '500',
            }}>
            {strings('selectShip')}
          </Text>
          <TouchableOpacity style={styles.itemShipping}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginBottom: 10,
              }}>
              <Text style={{...styles.textTitle}}>
                {ShippingUtils.checkNameShipping(
                  data === undefined ? 0 : data.service_type_id,
                )}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <Text style={styles.textDes}>
                  {CurrencyFormatter(data === undefined ? 0 : data?.total)}
                </Text>
                <Icon
                  name={'right'}
                  type={'antdesign'}
                  color={ColorStyle.tabBlack}
                  size={15}
                />
              </View>
            </View>
            <Text style={styles.textDes}>{strings('sendProduct')}</Text>
          </TouchableOpacity>
        </View>
      );
    }
  }
  if (itemCart === undefined) {
    return null;
  }
  return (
    <View
      style={{
        backgroundColor: ColorStyle.white,
        borderRadius: 6,
      }}>
      <View
        style={{
          flexDirection: 'row',
          marginVertical: Styles.constants.X / 4,
          alignItems: 'center',
          paddingHorizontal: Styles.constants.X / 4,
        }}>
        <Icon
          name={'storefront'}
          type={'materialicons'}
          size={20}
          color={ColorStyle.gray}
        />
        <Text
          style={{
            ...Styles.text.text14,
            color: ColorStyle.tabBlack,
            fontWeight: '600',
            marginHorizontal: 7,
          }}>
          {StoreHelper.getName(item?.store)}
        </Text>
      </View>
      {renderProduct()}
      {renderVoucher()}
      {renderShip()}
      <View style={styles.bodyVoucher}>
        <Text style={styles.textTitle}>{strings('totalPrice')}</Text>
        <Text
          style={{
            ...styles.textDes,
            color: ColorStyle.tabActive,
            fontWeight: '700',
          }}>
          {CurrencyFormatter(item?.discount_detail?.total)}
        </Text>
      </View>
    </View>
  );
}
