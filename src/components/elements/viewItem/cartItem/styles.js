import Styles from "../../../../resource/Styles";
import ColorStyle from "../../../../resource/ColorStyle";

const X=Styles.constants.X
export default {

    bodyShipping:{
        padding:X/4,
        borderTopColor:ColorStyle.tabActive,
    },
    bodyVoucher:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent: 'space-between',
        padding:X/4,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        backgroundColor:ColorStyle.tabWhite,
        elevation: 2,
    },
    textTitle:{
        ...Styles.text.text14,
        color: ColorStyle.tabBlack,
        fontWeight: '600',
    },
    textDes:{
        ...Styles.text.text12,
        color: ColorStyle.tabBlack,
        fontWeight: '500',
    },
    viewBodyCart:{
        backgroundColor:ColorStyle.tabWhite,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        marginTop:X/9,
        elevation: 2,
    },
    viewQuantityControl:{
        backgroundColor:ColorStyle.tabWhite,
        height: X*0.6,
        width:X*2,
    },
    styleText:{
        ...Styles.text.text12,
        marginHorizontal: 0,
        color: ColorStyle.tabBlack,
        height: X*0.6,
        padding:0,
        width:'40%',
        backgroundColor:ColorStyle.tabWhite,
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 8,
    },
    btnQuantity:{
        paddingHorizontal: 0,
        borderRadius:0,
        borderWidth:0,
        alignItems:'center',
        justifyContent: 'center',
        backgroundColor:ColorStyle.tabWhite,
        width:'30%',
        height: X*0.6,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    btnDelete:{
        alignItems:'center',
        justifyContent: 'center',
        backgroundColor:ColorStyle.tabWhite,
        width: X*0.8,
        height:  X*0.8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        borderRadius: X,
        elevation: 4,
    },
    itemShipping:{
        borderColor:ColorStyle.textOr,
        borderWidth: 1,
        backgroundColor:'rgba(0, 158, 116, 0.2)',
        padding:X/4,
        marginTop: X/5,
        borderRadius:X/4
    }

}
