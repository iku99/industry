import React, {Component} from 'react';
import {Alert, Image, Text, TouchableOpacity, View} from 'react-native';
import {strings} from '../../../../resource/languages/i18n';
import CartHandle from '../../../../sagas/CartHandle';
import ViewUtils from '../../../../Utils/ViewUtils';
import {CheckBox, Icon} from 'react-native-elements';
import ColorStyle from '../../../../resource/ColorStyle';
import ProductUtils from '../../../../Utils/ProductUtils';
import CurrencyFormatter from '../../../../Utils/CurrencyFormatter';
import QuantityControl from '../../QuantityControl';
import Styles from '../../../../resource/Styles';
import StoreHelper from '../../../../Utils/StoreHelper';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import Accordion from 'react-native-collapsible/Accordion';
import styles from "./styles";
let updateCartId = -1;
let quantityControlRef = [];
export default class CartItem extends Component {
  constructor(props) {
    super(props);
    let cartItem = this.props.cartItem;
    this.state = {
      activeSections: [0],
      cartItem: cartItem,
      quantity: cartItem !== undefined ? cartItem.qty : 0,
      selectAllItem: cartItem.checked,
      products: [],
    };
  }

  render() {
    return (
      <View
        style={styles.viewBodyCart}>
        <Accordion
          sections={[{}]}
          activeSections={this.state.activeSections}
          renderHeader={this._renderHeader}
          renderContent={this._renderContent}
          onChange={this._updateSections}
          touchableComponent={TouchableOpacity}
        />
      </View>
    );
  }
  _renderHeader = (section, _, isActive) => {
    let cartItem = this.state.cartItem;
    if (cartItem === undefined) {
      return null;
    }
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          width: '100%',
          borderRadius: 6,
          backgroundColor: ColorStyle.white,
          paddingVertical: 8,
          marginHorizontal: 0,
          paddingEnd: 16,
          overflow: 'hidden',
        }}>
        <CheckBox
          checked={this.state.selectAllItem}
          containerStyle={{borderColor: 'transparent', padding: 0}}
          onPress={() => {
            this.setState({selectAllItem: !this.state.selectAllItem}, () =>
              this.changeAllItem(this.state.selectAllItem),
            );
          }}
          iconType="material-community"
          uncheckedIcon="checkbox-blank-outline"
          uncheckedColor={'#DEDEDE'}
          checkedIcon="checkbox-marked"
          checkedColor={ColorStyle.tabActive}
        />
        <Icon
          name={'storefront'}
          type={'materialicons'}
          size={20}
          color={ColorStyle.gray}
        />
        <Text
          style={{
            ...Styles.text.text14,
            color: ColorStyle.tabBlack,
            fontWeight: '600',
            marginHorizontal: 7,
          }}>
          {StoreHelper.getName(cartItem)}
        </Text>
        <Icon name={isActive ? 'right' : 'up'} type={'antdesign'} size={15} />
      </View>
    );
  };
  _renderContent = section => {
    let cartItem = this.state.cartItem;
    let stock = true;
    if (cartItem === undefined) {
      return null;
    }

    let products = cartItem.products.map((product, index) => {
      return (
        <TouchableOpacity
          key={index}
          onPress={() => {
            NavigationUtils.goToProductDetail1(product);
          }}
          style={{flexDirection: 'row',marginHorizontal:10, marginVertical: 15}}>
          <View style={{flex: 1}}>
            <CheckBox
              checked={product.checked}
              containerStyle={{borderColor: 'transparent', padding: 0}}
              onPress={() => {
                this.changeOnselectItem(product, index);
              }}
              iconType="material-community"
              uncheckedIcon="checkbox-blank-outline"
              uncheckedColor={'#DEDEDE'}
              checkedIcon="checkbox-marked"
              checkedColor={ColorStyle.tabActive}
              textStyle={{fontWeight: 'normal'}}
            />
          </View>
          <View style={{alignItems: 'flex-start'}}>
            <Image
              source={{
                uri: ProductUtils.getImages(product)[0],
                headers: {Authorization: 'someAuthToken'},
              }}
              style={{width: Styles.constants.X*2, height: Styles.constants.X*2,marginHorizontal:10}}
            />
          </View>
          <View style={{flex: 3, justifyContent: 'space-between'}}>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
                fontWeight: '500',
              }}>
              {product.name}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
              }}>
              <Text>{CurrencyFormatter(product.price)}</Text>
              <Text
                style={{
                  ...Styles.text.text14,
                  color: product.quantity_in_stock!==0 ? ColorStyle.tabActive : '#BBBBBB',
                  fontWeight: '500',
                  marginLeft: 10,
                }}>
                {product.quantity_in_stock!==0 ? strings('stock') : strings('outStock')}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginTop: 10,
              }}>
              <QuantityControl
                minValue={0}
                containerStyle={styles.viewQuantityControl}
                btnQuantity={styles.btnQuantity}
                styleText={styles.styleText}
                maxValue={product.quantity_in_stock}
                quantity={product.quantity}
                ref={ref => {
                  quantityControlRef[index] = ref;
                }}
                changeValue={quantity => {
                  if (quantity === 0) {
                    this.removeProductFromCart(
                      product,
                      this.props.parentIndex,
                      index,
                      () => {
                        quantityControlRef[index].updateQuantity(1);
                      },
                    );
                  } else {
                    this.updateProductInCart(
                      product,
                      this.props.parentIndex,
                      index,
                      quantity,
                    );
                  }
                }}
              />

              <TouchableOpacity
                onPress={() =>
                  this.removeProductFromCart(
                    product,
                    this.props.parentIndex,
                    index,
                  )
                }
                style={styles.btnDelete}>
                <Icon
                  name={'delete'}
                  type={'material-community-icons'}
                  size={Styles.constants.X/2.4}
                  color={ColorStyle.tabActive}
                />
              </TouchableOpacity>
            </View>
          </View>
        </TouchableOpacity>
      );
    });
    return products;
  };

  _updateSections = activeSections => {
    this.setState({activeSections});
  };
  removeProductFromCart(product, storeIndex, productIndex, callback) {

    Alert.alert(
      '',
      strings('removeProductFromCart').replace('{name}',product.name),
      [
        {
          text: strings('delete'),
          onPress: () => {
            let params = {
             id:[product.id_order_detail]
            };
            CartHandle.getInstance().removeProductFromCart(
              params,
              (isSuccess, res) => {
                if (isSuccess) {
                    if (this.props.onProduceRemoved != null) {
                    this.props.onProduceRemoved(storeIndex, productIndex);
                    this.changeSelectCallback();
                  }
                }
              },
            );
          },
        },
        {
          text: strings('cancel'),
          onPress: () => {
            if (callback !== undefined) {
              callback();
            }
          },
          style: 'cancel',
        },
      ],
      {cancelable: false},
    );
  }
  updateProductInCart(product, storeIndex, productIndex, quantity, item) {
    //Tranh thay doi qua nhieu thi sau khi cap nhat 1s moi goi api
    clearTimeout(updateCartId);
    updateCartId = setTimeout(() => {
      let params = {
          quantity: quantity,
      };
      product.quantity = quantity;
      this.changeItem(product, item, productIndex);
      CartHandle.getInstance().updateProductInCart(
        params,product.id_order_detail,
        (isSuccess, dataResponse) => {
          if (!isSuccess) {
            ViewUtils.showAlertDialog('error');
          }
        },
      );
    }, 1000);
  }
  changeOnselectItem(product, index) {
      product.checked = !product.checked;
    this.changeItem(product, index);
  }
  changeItem(product, index) {
    let cartItem = this.state.cartItem;
    let checkSelect=cartItem.products.filter(elm=>elm.checked===true)
    if(checkSelect.length===0){ cartItem.checked = false}
    if(checkSelect.length !==0){cartItem.checked = true}
    let newProducts = cartItem.products;
    newProducts[index] = product;
    cartItem.products = newProducts;
    this.setState({cartItem}, () => {
      if (!product.checked) {
        this.setState({selectAllItem: false});
      }
    });
    this.changeSelectCallback();
  }
  changeAllItem(select) {
    let cartItem = this.state.cartItem;
    let newProducts = cartItem.products;
    newProducts.forEach((product, index) => {
        product.checked = select;
        newProducts[index] = product;
    });
    cartItem.checked = !cartItem.checked;
    cartItem.products = newProducts;
    this.setState({cartItem});
    this.changeSelectCallback();
  }
  changeSelectCallback() {
    if (this.props.changeSelectCallback) {
      this.props.changeSelectCallback(this.state.cartItem);
    }
  }
}
