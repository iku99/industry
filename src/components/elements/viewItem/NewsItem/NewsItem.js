import React, {Component} from "react";
import constants from "../../../../Api/constants";
import ElevatedView from "react-native-elevated-view";
import GroupStyle from "../../../screen/group/GroupStyle";
import {FlatList, Image, Share, Text, TouchableOpacity, View} from "react-native";
import Styles from "../../../../resource/Styles";
import ColorStyle from "../../../../resource/ColorStyle";
import DateUtil from "../../../../Utils/DateUtil";
import HTMLView from "react-native-htmlview";
import ImageGrid from "../../ImageGrid";
import {Icon} from "react-native-elements";
import GroupHandle from "../../../../sagas/GroupHandle";
import {strings} from "../../../../resource/languages/i18n";
import DataUtils from "../../../../Utils/DataUtils";
import {Actions} from "react-native-router-flux";
import GroupPostHandle from "../../../../sagas/GroupPostHandle";
import ProductHandle from "../../../../sagas/ProductHandle";
import AccountHandle from "../../../../sagas/AccountHandle";
import ViewProductItem from "../ViewedProduct/ViewProductItem";
import NavigationUtils from "../../../../Utils/NavigationUtils";
import MediaUtils from "../../../../Utils/MediaUtils";
import GlobalInfo from "../../../../Utils/Common/GlobalInfo";

let isPostingLike = false;
export default class NewsItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: this.props.item,
        };
    }
    render() {
        let item = this.state.item
        return (
            <TouchableOpacity
                onPress={()=>{this.goToPostDetail()}}
                style={{
                    ...GroupStyle.groupPost1,
                    backgroundColor:ColorStyle.tabWhite,
                    alignSelf:'center',
                    width:Styles.constants.widthScreenMg24,
                    borderRadius:5,
                    shadowColor: ColorStyle.borderItemHome,
                    shadowOffset: { width: 0, height: 1 },
                    shadowOpacity: 0.8,
                    shadowRadius: 1,
                }}>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <Image source={{uri:this.getImageList(item)[0]}} style={{flex:1,height:Styles.constants.X*3,aspectRatio: 1}}/>
                    <View style={{flex:2,paddingLeft:Styles.constants.X/4}}>
                        <View style={{flexDirection: 'row',flex:1, paddingHorizontal: 5,alignItems:'center'}}>
                            <Image
                                style={{...Styles.icon.iconOrder, borderRadius: 50}}
                                source={MediaUtils.getAvatarUrl(item)}
                            />
                            <View
                                style={{
                                    flex: 1,
                                    justifyContent: 'space-between',
                                    marginLeft: 10,
                                    paddingHorizontal: 5,
                                }}>
                                <Text
                                    style={{
                                        ...Styles.text.text14,
                                        color: ColorStyle.tabBlack,
                                        fontWeight: '700',
                                    }}>
                                    {item.customer.full_name}
                                </Text>
                                <Text
                                    style={{
                                        ...Styles.text.text10,
                                        color: ColorStyle.gray,
                                        fontWeight: '400',
                                        marginTop:5,
                                    }}>
                                    {DateUtil.formatDate('HH:MM DD/MM/YYYY',item.created_date)}
                                </Text>
                            </View>
                        </View>
                        <Text style={{...Styles.text.text14,color:ColorStyle.tabBlack,flex:2,fontWeight:'600',marginTop:Styles.constants.X/3}} numberOfLines={4}>{item.name}</Text>
                    </View>
                </View>
                <Text style={{...Styles.text.text12,color:ColorStyle.tabBlack,fontWeight:'600',marginVertical:Styles.constants.X/3}} numberOfLines={2}>{item.des}</Text>
                <View style={{flexDirection:'row',alignItems:'center',alignSelf:'flex-end'}}>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <Icon name={'like2'} type={'antdesign'} size={18} color={ColorStyle.tabActive}/>
                        <Text style={{...Styles.text.text14 ,marginLeft:5}}>
                            {item.total_like===null?0:item.total_like} </Text>
                    </View>
                    <View style={{flexDirection:'row',alignItems:'center',marginLeft:5}}>
                        <Icon name={'comment'} type={'fontisto'} size={15} color={ColorStyle.tabActive}/>
                        <Text style={{...Styles.text.text14 ,marginLeft:5}}>
                            {item.total_comment===null?0:item.total_comment} </Text>
                    </View>
                    <View style={{flexDirection:'row',alignItems:'center',marginLeft:5}}>
                        <Icon name={'eye'} type={'entypo'} size={18} color={ColorStyle.tabActive}/>
                        <Text style={{...Styles.text.text14 ,marginLeft:5}}>
                            {item.total_seen===null?0:item.total_seen} </Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    getImageList() {
        let item = this.state.item;
        let imgList = [];
        let images = item.image_url;
        if (images === undefined) {
            images = [];
        }
        images.forEach((image, index) => {
            imgList.push(constants.host + image.url);
        });
        return imgList;
    }
    imageOnPress(initIndex) {
        Actions.jump('fullViewImage', {images: this.getImageList(), initIndex});
    }
    onChangeData() {
        if (this.props.onChangeItemData !== undefined) {
            this.props.onChangeItemData(this.state.item, this.props.index);
        }
    }
    getImage(item,index){
        let avatar = '';
        if (
            item !== null
        ) {
            avatar = GlobalInfo.initApiEndpoint(item)
        } else {
            avatar = 'https://picsum.photos/84/84?' + index;
        }
        return avatar;
    }
    goToPostDetail() {
        if (this.props.onGoToPostDetail !== undefined) {
            this.props.onGoToPostDetail();
        }
    }
    keyExtractor = (item, index) => `groupPostRow_${index.toString()}`;
    changeDataItem(item) {
        this.setState({item});
    }
}
