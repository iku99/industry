import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Ripple from 'react-native-material-ripple';
import ViewProductStyle from './ViewProductStyle';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import MyFastImage from '../../MyFastImage';
import AppConstants from '../../../../resource/AppConstants';
import ProductUtils from '../../../../Utils/ProductUtils';
import constants from '../../../../Api/constants';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
import {Icon} from 'react-native-elements';
import {strings} from '../../../../resource/languages/i18n';
import CurrencyFormatter from '../../../../Utils/CurrencyFormatter';
export default class ViewProductItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      favorite: 0,
    };
  }
  render() {
    return (
      <Ripple
        style={
          {
              ...ViewProductStyle.container,
            width: this.getItemWidth() ,
            backgroundColor: ColorStyle.tabWhite,
          }}
        onPress={() => {
          if (this.props.onClick != null) {
            this.props.onClick(this.props.item);
          }
        }}>
      <View style={{  overflow:'hidden'}}>
          <MyFastImage
              style={{...ViewProductStyle.imageProduct,
                  width: this.getItemWidth(),
                  height: this.getItemWidth(),
              }}
              source={{
                  uri: this.getImageUrl(this.props.item?.image_url),
              }}
              resizeMode={AppConstants.IMAGE_SCALE_TYPE.CONTAIN}
          />
          <View style={{paddingHorizontal:10}}>
              <Text
                  style={[
                      Styles.text.text14,
                      {
                          width: '100%',
                          fontWeight: '600',
                          marginTop: 20,
                      },
                  ]}
                  numberOfLines={2}
                  ellipsizeMode="tail">
                  {this.props.item.name}
              </Text>
              <View
                  style={{
                      flexDirection: 'row',
                      paddingVertical: 5,
                      justifyContent: 'space-between',
                  }}>
                  <Text style={ViewProductStyle.textFormatPrice}>
                      {CurrencyFormatter(this.props.item.price)}
                  </Text>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Text style={[Styles.text.text12, {color: ColorStyle.tabBlack}]}>
                          {this.props.item.total_sold===null?0:this.props.item.total_sold} {strings('sold')}
                      </Text>

                  </View>
              </View>
          </View>
          <View style={{...ViewProductStyle.viewSale,display:this.props.item?.sale===undefined ||this.props.item?.sale===0 ? 'none':'flex'}}>
              <Icon name={'burst-sale'} type={'foundation'} color={ColorStyle.tabWhite} size={25}/>
              <Text style={ViewProductStyle.textSale}>{this.props.item?.sale}</Text>
          </View>
      </View>
      </Ripple>
    );
  }
    getImageUrl(images) {
        if (images != null &&images != undefined && images.length > 0) {
            return constants.host + images[0].url;
        } else {
            return 'https://picsum.photos/130/214?' + this.props.index;
        }
    }

    getItemWidth() {
        return this.props.itemWidth == null
            ? AppConstants.defaultViewedProductItemWidth
            : this.props.itemWidth;
    }
}
