import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Ripple from 'react-native-material-ripple';
import ViewProductStyle from './ViewProductStyle';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import MyFastImage from '../../MyFastImage';
import AppConstants from '../../../../resource/AppConstants';
import ProductUtils from '../../../../Utils/ProductUtils';
import constants from '../../../../Api/constants';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
import {Icon} from 'react-native-elements';
import {strings} from '../../../../resource/languages/i18n';
import CurrencyFormatter from '../../../../Utils/CurrencyFormatter';
export default class ViewItemProductSeller extends Component {
  constructor(props) {
    super(props);
    this.state = {
      favorite: 0,
    };
  }
  componentDidMount() {
  }

  render() {
    return (
      <View style={{marginVertical: 10, height: Styles.constants.X * 3.6}}>
        <TouchableOpacity
            onPress={()=>this.props.onClick(this.props.item)}
          style={[
            {
              width: Styles.constants.widthScreenMg24,
              backgroundColor: ColorStyle.tabWhite,
              borderRadius: 10,
              alignSelf: 'center',
              paddingVertical: 27,
              paddingHorizontal: 21,
              flexDirection: 'row',
              borderColor: ColorStyle.borderItemHome,
                elevation: 4,
                shadowColor: ColorStyle.borderItemHome,
                shadowOffset: { width: 1, height: 2 },
                shadowOpacity: 0.8,
                shadowRadius: 1,
            },
          ]}>
          <MyFastImage
            style={[
              ViewProductStyle.imageProduct,
              {
                width: 75,
                height: 75,
              },
            ]}
            source={{
              uri: this.getImageUrl(this.props.item.image_url),
            }}
            resizeMode={AppConstants.IMAGE_SCALE_TYPE.CONTAIN}
          />
          <View
            style={{
              flex: 2,
              paddingVertical: 5,
              marginHorizontal: 20,
              justifyContent: 'space-between',
            }}>
            <Text
              style={{
                ...Styles.text.text18,
                color: ColorStyle.tabBlack,
                fontWeight: '700',
              }}
              numberOfLines={2}
              ellipsizeMode="tail">
              {this.props.item.name}
            </Text>
            <Text
              style={{
                ...Styles.text.text18,
                color: ColorStyle.tabActive,
                fontWeight: '700',
              }}>
              {CurrencyFormatter(this.props.item.price)}
            </Text>
            <Text style={{...Styles.text.text14, color: ColorStyle.tabBlack}}>
              {strings('inventories') + ':' + this.props.item.quantity}
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              if (this.props.onClick != null) {
                this.props.onUpdate(this.props.item.id, this.props.index);
              }
            }}>
            <Icon
              name={'pencil'}
              type="foundation"
              size={15}
              color={ColorStyle.tabActive}
            />
          </TouchableOpacity>
        </TouchableOpacity>
        {/*<TouchableOpacity*/}
        {/*  onPress={() => {*/}
        {/*    if (this.props.onClick != null) {*/}
        {/*      this.props.onUpdate(this.props.item.id, this.props.index);*/}
        {/*    }*/}
        {/*  }}*/}
        {/*  style={{*/}
        {/*    position: 'absolute',*/}
        {/*    backgroundColor: ProductUtils.getProductStatusColor(*/}
        {/*      this.props.item,*/}
        {/*    ),*/}
        {/*    width: Styles.constants.widthScreenMg24,*/}
        {/*    height: Styles.constants.X * 3.6,*/}
        {/*    alignSelf: 'center',*/}
        {/*    borderRadius: 10,*/}
        {/*    alignItems: 'center',*/}
        {/*    justifyContent: 'center',*/}
        {/*    flexDirection: 'row',*/}
        {/*    display:*/}
        {/*      this.props.item?.status === AppConstants.PRODUCT_STATUS.REVIEWED*/}
        {/*        ? 'none'*/}
        {/*        : 'flex',*/}
        {/*  }}>*/}
        {/*  <Icon*/}
        {/*    name={'infocirlce'}*/}
        {/*    type={'antdesign'}*/}
        {/*    color={ColorStyle.tabWhite}*/}
        {/*    size={15}*/}
        {/*  />*/}
        {/*  <Text*/}
        {/*    style={{*/}
        {/*      ...Styles.text.text14,*/}
        {/*      marginLeft: 10,*/}
        {/*      fontWeight: '500',*/}
        {/*      color: ColorStyle.tabWhite,*/}
        {/*    }}>*/}
        {/*    {ProductUtils.getProductStatus(this.props.item)}*/}
        {/*  </Text>*/}
        {/*</TouchableOpacity>*/}
      </View>
    );
  }

  getImageUrl(images) {
    if (images != null && images.length > 0) {
      return constants.host + images[0].url;
    } else {
      return 'https://picsum.photos/130/214?' + this.props.index;
    }
  }

  getItemWidth() {
    return this.props.itemWidth == null
      ? AppConstants.defaultViewedProductItemWidth
      : this.props.itemWidth;
  }
}
