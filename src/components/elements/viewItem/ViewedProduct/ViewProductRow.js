import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Ripple from 'react-native-material-ripple';
import ViewProductStyle from './ViewProductStyle';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import MyFastImage from '../../MyFastImage';
import AppConstants from '../../../../resource/AppConstants';
import constants from '../../../../Api/constants';
import Styles from '../../../../resource/Styles';
import CurrencyFormatter from '../../../../Utils/CurrencyFormatter';
export default class ViewProductRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      favorite: 0,
    };
  }
  componentDidMount() {}

  render() {
    return (
      <Ripple
        style={[
          ViewProductStyle.container,
          {
            width: this.getItemWidth() + 20,
            backgroundColor: 'transform',
          },
        ]}
        onPress={() => {
          if (this.props.onClick != null) {
            this.props.onClick(this.props.item);
          }
        }}>
        <MyFastImage
          style={[
            ViewProductStyle.imageProduct,
            {
              width: this.getItemWidth(),
              height: this.getItemWidth(),
            },
          ]}
          source={{
            uri: this.getImageUrl(this.props.item.images),
          }}
          resizeMode={AppConstants.IMAGE_SCALE_TYPE.CONTAIN}
        />
        <Text
          style={[
            Styles.text.text14,
            {
              width: '100%',
              fontWeight: '500',
              marginTop: 20,
            },
          ]}
          numberOfLines={2}
          ellipsizeMode="tail">
          {this.props.item.name}
        </Text>
        <Text style={ViewProductStyle.textFormatPrice}>
          {CurrencyFormatter(this.props.item.price)}
        </Text>
      </Ripple>
    );
  }
  getImageUrl(images) {
    if (images != null && images.length > 0) {
      return constants.host + images[0].thumbnail_url;
    } else {
      return 'https://picsum.photos/130/214?' + this.props.index;
    }
  }

  getItemWidth() {
    return this.props.itemWidth == null
      ? AppConstants.defaultViewedProductItemWidth
      : this.props.itemWidth;
  }
}
