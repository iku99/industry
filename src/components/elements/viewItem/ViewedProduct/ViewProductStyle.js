import ColorStyle from '../../../../resource/ColorStyle';
import Styles from "../../../../resource/Styles";
const X=Styles.constants.X
export default {
  container: {
    margin: 8,
    justifyContent:'space-between',
    borderRadius: 15,
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOffset: {width: 1, height: 1},
    shadowOpacity: 0.26,
    elevation: 2,
  },
  imageProduct: {
    overflow: 'hidden',
  },
  likeButton: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    borderWidth: 0,
    backgroundColor: 'white',
    elevation: 5,
    position: 'absolute',
    right: 0,
  },
  textFormatPrice: {
    textAlign: 'left',
    color: ColorStyle.tabActive,
    marginVertical: 10,
    fontStyle: 'normal',
    fontWeight: '700',
  },
  iconFa: {
    width: 13,
    height: 12,
    alignItems: 'center',
  },
  viewSale:{
    width:X*2,
    height:X/1.4,
    position: 'absolute',
    top:X/4,
    flexDirection:'row',
    backgroundColor:ColorStyle.tabActive,
    shadowOffset: {width: 1, height: 1},
    shadowOpacity: 0.26,
    paddingHorizontal:X/5,
    alignItems:'center',
    elevation: 2,
  },
  textSale:{
    ...Styles.text.text16,
    fontWeight: '500',
    color: ColorStyle.tabWhite,
    marginLeft:X/5
  }
};
