import React from "react";
import {Text, TouchableOpacity, View} from "react-native";
import Styles from "../../../../resource/Styles";
import ColorStyle from "../../../../resource/ColorStyle";
import {Icon} from "react-native-elements";
import {strings} from "../../../../resource/languages/i18n";
const X=Styles.constants.X
export default function VoucherManageItem({item,onChange,onDelete}){

    return(
        <TouchableOpacity style={{flex:1,backgroundColor:ColorStyle.tabWhite,shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 2,
            },
            shadowOpacity: 0.23,
            shadowRadius: 2.62,
            flexDirection:'row',alignItems:'center',
            borderRadius:X/6,
            elevation: 4,width:'95%',padding:X/5,marginTop:X/7,alignSelf:'center'}} onPress={()=>onChange()}>
           <View style={{flex:6}}>
               <Text style={{...Styles.text.text14,color:ColorStyle.tabBlack,fontWeight:'500',marginVertical:X/10}}>{strings('code')}:<Text style={{...Styles.text.text15,color:ColorStyle.tabActive,fontStyle:'italic'}}>{item.code}</Text></Text>

               <Text style={{...Styles.text.text14,color:ColorStyle.tabBlack,fontWeight:'500',marginVertical:X/10}}>{strings('nameVoucher')}<Text style={{...Styles.text.text15,color:ColorStyle.tabBlack}}>{item.name}</Text></Text>
               <Text style={{...Styles.text.text14,color:ColorStyle.tabBlack,fontWeight:'500',marginVertical:X/10}}>{strings('des2')}<Text numberOfLines={2} style={{...Styles.text.text12,color:ColorStyle.tabBlack}}>{item.des}</Text></Text>

               <Text style={{...Styles.text.text14,color:ColorStyle.tabBlack,fontWeight:'500',marginVertical:X/10}}>{strings('voucherType')}:<Text style={{...Styles.text.text15,color:ColorStyle.tabBlack}}>{item.name}</Text></Text>

           </View>
            <View style={{flex:1,flexDirection:'row',alignItems:'center'}}>
                <TouchableOpacity onPress={()=>onDelete()}>
                <Icon name={'delete'} type={'material-community-icons'} size={20} color={ColorStyle.tabActive}/>
            </TouchableOpacity>
                <TouchableOpacity onPress={()=>onChange()}>
                    <Icon name={'edit'} type={'antdesign'} size={20} color={ColorStyle.tabActive}/>
                </TouchableOpacity>
            </View>
        </TouchableOpacity>
    )
}
