import React from "react";
import {Text, TouchableOpacity, View} from "react-native";
import Styles from "../../../../resource/Styles";
import ColorStyle from "../../../../resource/ColorStyle";
import {CheckBox, Icon} from "react-native-elements";
import {strings} from "../../../../resource/languages/i18n";
import AppConstants from "../../../../resource/AppConstants";
import styles from "../../../screen/voucher/styles";
import DateUtil from "../../../../Utils/DateUtil";
const X=Styles.constants.X
export default function VoucherStoreItem({item,onChange,onDelete}){
    function onChangeItem(values){
        values={
            ...values,
            checked: !values.checked
        }
        onChange(values)
    }
    if(item.voucher_type===AppConstants.VOUCHER.VOUCHER_TYPE[0].value){
        return(
            <TouchableOpacity
                onPress={() => onChangeItem(item)}
                style={styles.bodyRow}>
                <View style={{...styles.viewImg,width:'30%'}}>
                    <Text style={{...styles.textImg,color:ColorStyle.tabActive}}>{strings('discount')}</Text>
                    <View style={styles.viewDes}>
                        <Text style={styles.des}>{strings('COUPON')}</Text>
                    </View>
                </View>
                <View style={{...styles.bodyText,width:'55%'}}>
                    <Text style={styles.textName}>{item.name}</Text>
                    <Text style={styles.textDate}>{strings('to')} {DateUtil.formatDate('DD/MM',item.valid_from)} {strings('from')} {DateUtil.formatDate('DD/MM/YYYY',item.valid_to)}</Text>
                </View>
                <CheckBox
                    checked={item.checked}
                    containerStyle={{borderColor: 'transparent', padding: 0}}
                    onPress={() => {
                        onChangeItem(item)
                    }}
                    iconType="material-icons"
                    checkedIcon="radio-button-checked"
                    uncheckedColor={'#DEDEDE'}
                    uncheckedIcon="radio-button-unchecked"
                    checkedColor={ColorStyle.tabActive}
                    textStyle={{fontWeight: 'normal'}}
                />
            </TouchableOpacity>

        )
    }else {
        return (
            <TouchableOpacity
                onPress={() => onChangeItem(item)}
                style={styles.bodyRow}>
                <View style={{...styles.viewImg,width:'30%'}}>
                    <Text style={styles.textImg}>{strings('freeShip')}</Text>
                </View>
                <View style={{...styles.bodyText,width:'55%'}}>
                    <Text style={styles.textName}>{item.name}</Text>
                    <Text style={styles.textDate}>{strings('unDate')}{DateUtil.formatDate('DD/MM/YYYY',item.valid_from)}</Text>
                </View>
                <CheckBox
                    checked={item.checked}
                    containerStyle={{borderColor: 'transparent', padding: 0}}
                    onPress={() => {
                            onChangeItem(item)
                    }}
                    iconType="material-icons"
                    checkedIcon="radio-button-checked"
                    uncheckedColor={'#DEDEDE'}
                    uncheckedIcon="radio-button-unchecked"
                    checkedColor={ColorStyle.tabActive}
                    textStyle={{fontWeight: 'normal'}}
                />
            </TouchableOpacity>
        )
    }
}
