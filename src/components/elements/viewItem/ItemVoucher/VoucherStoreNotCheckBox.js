import Styles from "../../../../resource/Styles";
import AppConstants from "../../../../resource/AppConstants";
import {Text, TouchableOpacity, View} from "react-native";
import styles from "../../../screen/voucher/styles";
import ColorStyle from "../../../../resource/ColorStyle";
import {strings} from "../../../../resource/languages/i18n";
import DateUtil from "../../../../Utils/DateUtil";
import {CheckBox} from "react-native-elements";
import React from "react";

const X=Styles.constants.X
export default function VoucherStoreNotCheckBox({item,onChange,onDelete}){
    function onChangeItem(values){
        onChange(values)
    }
    if(item.voucher_type===AppConstants.VOUCHER.VOUCHER_TYPE[0].value){
        return(
            <TouchableOpacity
                onPress={() => onChangeItem(item)}
                style={{...styles.bodyRow,shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 1,
                    },
                    shadowOpacity: 0.20,
                    shadowRadius: 1.41,
                    backgroundColor:ColorStyle.tabWhite,
                    marginBottom:X/6,
                    elevation: 2,
                    borderWidth:item.checked?2:0,
                    borderColor:ColorStyle.tabActive
                }}>
                <View style={{...styles.viewImg,width:'30%'}}>
                    <Text style={{...styles.textImg,color:ColorStyle.tabActive}}>{strings('discount')}</Text>
                    <View style={styles.viewDes}>
                        <Text style={styles.des}>{strings('COUPON')}</Text>
                    </View>
                </View>
                <View style={{...styles.bodyText,width:'55%'}}>
                    <Text style={styles.textName}>{item.name}</Text>
                    <Text style={styles.textDate}>{strings('to')} {DateUtil.formatDate('DD/MM',item.valid_from)} {strings('from')} {DateUtil.formatDate('DD/MM/YYYY',item.valid_to)}</Text>
                </View>
            </TouchableOpacity>

        )
    }else {
        return (
            <TouchableOpacity
                onPress={() => onChangeItem(item)}
                style={{...styles.bodyRow,
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 1,
                    },
                    shadowOpacity: 0.20,
                    shadowRadius: 1.41,
                    backgroundColor:ColorStyle.tabWhite,
marginVertical:X/6,
                    elevation: 2,}}>
                <View style={{...styles.viewImg,width:'30%'}}>
                    <Text style={styles.textImg}>{strings('freeShip')}</Text>
                </View>
                <View style={{...styles.bodyText,width:'55%'}}>
                    <Text style={styles.textName}>{item.name}</Text>
                    <Text style={styles.textDate}>{strings('unDate')}{DateUtil.formatDate('DD/MM/YYYY',item.valid_from)}</Text>
                </View>
            </TouchableOpacity>

        )
    }
}
