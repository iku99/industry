import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import constants from '../../../../Api/constants';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
import HTMLView from 'react-native-htmlview';
import DataUtils from '../../../../Utils/DataUtils';
import {Icon} from 'react-native-elements';
import DateUtil from "../../../../Utils/DateUtil";
const X = Styles.constants.X;
export default class ItemNotificationOrder extends Component {
  render() {
    let data = this.props.item;
    // let url = this.props.item.body?.image_url;
    let read = data.status;
    return (
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          backgroundColor: data.status===1
            ? ColorStyle.colorPersonal
            : ColorStyle.tabWhite,
          paddingVertical: X * 0.3,
          paddingHorizontal: X * 0.3,
        }}
        onPress={() => {
          if (this.props.onNotificationClick != null) {
            this.props.onNotificationClick(data, this.props.index);
          }
        }}>
        {!DataUtils.stringNullOrEmpty(data.image_url) && (
          <Image
            source={{uri: this.getImageUrl()}}
            style={{...Styles.icon.icon_notification}}
          />
        )}
        {DataUtils.stringNullOrEmpty(data.image_url) && (
          <View
            style={{...Styles.icon.icon_notification}}>
            <Icon
              name="bell"
              type="font-awesome"
              color={ColorStyle.tabActive}
              size={20}
            />
          </View>
        )}
        <View style={{marginLeft: X * 0.4, justifyContent: 'space-between'}}>
          <Text style={{...Styles.text.text12, color: ColorStyle.tabBlack}}>
            {data.name}
          </Text>
        <Text style={{...Styles.text.text14, color: ColorStyle.tabBlack,maxWidth:'95%'}} >{data.content}</Text>
          {/*{this.getMsgHtml(data)}*/}
        </View>
      </TouchableOpacity>
    );
  }
  getImageUrl() {
    return constants.host + this.props.item?.image_url;
  }
  getMsgHtml(item) {
    let body = item.body;
    let msg = item.body.text;
    let extra_data = body.extra_data;
    let properties = DataUtils.getProperties(extra_data);
    if (!DataUtils.listNullOrEmpty(properties)) {
      properties.forEach(item => {
        msg = msg.replace(`{${item}}`, `<a>${extra_data[item]} </a>`);
      });
    }

    return (
      <HTMLView
        value={`<body>${msg}<body>`}
        stylesheet={{
          body: {
            fontSize: Styles.text.text10.fontSize,
            maxWidth: X * 6,
            marginTop: 10,
            fontWeight: item.seen ? '300' : '500',
            // color: item.seen ? ColorStyle.red : ColorStyle.tabActive,
          },
          a: {
            color: '#4967CE',
          },
        }}
      />
    );
  }
}
