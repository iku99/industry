import {Image, TouchableOpacity, View} from "react-native";
import Styles from "../../../resource/Styles";
import SellerStyles from "../../screen/seller/SellerStyles";
import {Icon} from "react-native-elements";
import React from "react";

export default function ItemImageProduct({item,index,callback}){
    return(
        <View
            style={{
                borderColor: 'black',
                elevation: 3,
                borderRadius: 10,
                marginLeft: 20,
                width: Styles.constants.X * 2,
                height: Styles.constants.X * 2,
            }}>
            <Image
                style={{...SellerStyles.imageItem}}
                source={{uri: item.node.image.uri}}
            />
            <TouchableOpacity
                style={{position: 'absolute', top: 0, right: 0}}
                onPress={() => callback(index)}>
                <Icon name="x" type="feather" size={24} color="red" />
            </TouchableOpacity>
        </View>
    )
}
