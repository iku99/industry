import React, {Component} from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import ColorStyle from "../../../../resource/ColorStyle";
import constants from "../../../../Api/constants";
import DataUtils from "../../../../Utils/DataUtils";
import MediaUtils from "../../../../Utils/MediaUtils";
import Styles from "../../../../resource/Styles";
import {Actions} from "react-native-router-flux";
import GlobalUtil from "../../../../Utils/Common/GlobalUtil";
const X=Styles.constants.X
 export default class TradePromotionItem extends Component {
  render() {
    let {item, index} = this.props;
    let height = X*4
    return (
        <TouchableOpacity
            onPress={() => {
              Actions.jump('TradePromotionDetail',{id:item.id})
              // NavigationUtils.goToNewsDetail(item?.id);
            }}
            style={{
              width: GlobalUtil.isTablet() ?Styles.constants.widthScreen/3-Styles.constants.X/4:Styles.constants.widthScreen/2-Styles.constants.X/4,
              height: height,
              alignSelf: 'center',
              backgroundColor: ColorStyle.backgroundScreen,
              borderRadius: 4,
              marginHorizontal: 5,
              marginVertical: 10,
              marginTop: 20
            }}>
          <View style={{width: '100%', height: 100}}>
            <Image
                source={{uri: MediaUtils.getProductImage(item)}}
                style={{
                  width: '100%',
                  height: 100,
                  borderRadius: 4,
                  position: 'absolute',
                }}
            />
          </View>
          <View style={{marginHorizontal: 8, marginBottom: 16}}>
            <Text style={styles.elementName} numberOfLines={2}>
              {item?.name}
            </Text>
          </View>
        </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  img: {
    width: 130,
    height: 100,
    marginVertical: 25,
    marginLeft: 19,
    borderRadius: 10,
  },
  boxInfor: {
    flexDirection: 'column',
    marginTop: 20,
    marginLeft: 17,
    marginBottom: 25,
  },
  elementName: {
    ...Styles.text.text15,
    marginVertical  :5,
    lineHeight: 18,
    color: '#181725',
    fontWeight: 'bold',
  },
  textDes:{
    ...Styles.text.text12,
    lineHeight: 18,
    color: '#181725',
  },
  elementStatus: {
    color: '#53B175',
  },
  elementPrice: {
    fontWeight: '500',
  },
  elementBtnAddToCart: {
    width: 60,
    height: 50,
    backgroundColor: '#53B175',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    borderBottomRightRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end',
    position: 'absolute',
    bottom: 0,
  },
  elementIcon: {
    width: 24,
    height: 24,
  },
});
