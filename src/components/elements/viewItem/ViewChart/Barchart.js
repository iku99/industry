import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
export default class Barchart extends Component {
  render() {
    return (
      <View style={{flex: 1, marginBottom: 20, height: Styles.constants.X}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between',marginBottom:10}}>
          <Text
              numberOfLines={2}
            style={[
              Styles.text.text12,
              {color: ColorStyle.tabBlack, fontWeight: '500',maxWidth:'80%'},
            ]}>
            {this.props.item.name}
          </Text>
          <Text
            style={[
              Styles.text.text10,
              {color: ColorStyle.tabBlack, fontWeight: '700'},
            ]}>
            {this.props.value? this.formatDollar(this.props.item.price):this.props.item.quantity}
          </Text>
        </View>
        <View
          style={{
            height: 10,
            width:
              Styles.constants.widthScreen -
              Styles.constants.marginHorizontalAll * 2,
            borderColor: 'black',
            borderWidth: 0.5,
            borderRadius: 5,

          }}>
          <View
            style={{
              backgroundColor: this.props.item.color,
              width: this.props.value?`${this.conversionPercent(this.props.item.price)}%`:`${this.conversionPercent(this.props.item.quantity)}%`,
              height: 9,
              borderRadius: 5,
            }}
          />
        </View>
      </View>
    );
  }
  conversionPercent(value) {
    let priceMax = this.props.pricerMax;
    let percent = (value / priceMax) * 100;
    return percent - 5;
  }
  formatDollar(value) {
    return Number(value)
      .toFixed(2)
      .replace(/\d(?=(\d{3})+\.)/g, '$&,');
  }
}
