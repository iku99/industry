import React, {Component} from 'react';
import {Text, View} from 'react-native';
import FlashSaleStyle from './FlashSaleStyle';
import Ripple from 'react-native-material-ripple';
import CurrencyFormatter from '../../../../Utils/CurrencyFormatter';
import AppConstants from '../../../../resource/AppConstants';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import MyFastImage from '../../MyFastImage';
import ProductUtils from '../../../../Utils/ProductUtils';
import MediaUtils from '../../../../Utils/MediaUtils';
import ColorStyle from '../../../../resource/ColorStyle';
import {strings} from '../../../../resource/languages/i18n';
import Styles from '../../../../resource/Styles';

export default class FlashSaleProductItem extends Component {
  render() {
    let tyLeBan = (this.props.item.sell_count / this.props.item.qty) * 100;
    let restProduct = this.props.item.qty - this.props.item.sell_count;

    return (
      <Ripple
        style={[
          FlashSaleStyle.containerProduct,
          {
            padding: 2,
            width: Styles.constants.X * 4,
            height: Styles.constants.X * 4.5,
            marginTop: 19,

          },
        ]}
        onPress={() => {
          // if(this.props.onClick != null) this.props.onClick(this.props.item)
          NavigationUtils.goToProductDetail(this.props.item, undefined, {
            hasFlashSale: this.props.hasFlashSale,
          });
        }}>
        <MyFastImage
          style={FlashSaleStyle.imageProduct}
          source={{
            uri:ProductUtils.getImages(this.props.item)[0],
            headers: {Authorization: 'someAuthToken'},
          }}
          resizeMode={AppConstants.IMAGE_SCALE_TYPE.CONTAIN}
        />
        <Text
          style={[
            FlashSaleStyle.price,
            {marginTop: 9, fontWeight: 'bold', color: '#0D2B28'},
          ]}>
          {CurrencyFormatter(this.props.item.price)}
        </Text>
        <View
          style={{
            height: Styles.constants.X * 0.5,
            borderRadius: 10,
            backgroundColor: restProduct > 0 ? 'rgba(1, 66, 148, 0.1)' : ColorStyle.outStock,
            marginTop: 10,
            width: '100%',
          }}>
          <View
            style={{
              borderBottomLeftRadius: 10,
              borderTopLeftRadius: 10,
              position: 'absolute',
              height: '100%',
              // width: tyLeBan + '%',
              // borderRadius: tyLeBan < 95 ? 0 : 10,
              opacity: 0.8,
            }}
          />
          <Text
            style={[
              FlashSaleStyle.availableProduct,
              {color: restProduct > 0 ? ColorStyle.blue : '#979797'},
            ]}>
            {restProduct > 0
              ? `${strings('rest')} ${this.props.item.sell_count} ${strings(
                  'product',
                )}`
              : `${strings('Product')} ${strings('over')}`}
          </Text>
        </View>
        {ProductUtils.getPercent(this.props.item) !== 0 ? (
          <Text
            style={{
              ...FlashSaleStyle.percent,
              fontSize: 12,
              borderRadius: 45,
              alignItems: 'center',
              padding: 10,
            }}>{`-${ProductUtils.getPercent(this.props.item)}%`}</Text>
        ) : null}
      </Ripple>
    );
  }
}
