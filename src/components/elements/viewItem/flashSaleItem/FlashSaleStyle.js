import React from 'react';
import ColorStyle from "../../../../resource/ColorStyle";

const statusBg = {
  paddingHorizontal: 10,
  borderRadius: 12,
  borderWidth: 0,
  paddingVertical: 4,
  textAlign: 'center',
  fontSize: 12,
};
const itemWidth = 150;
export default {
  container: {
    width: itemWidth,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 8,
  },
  containerProduct: {
    width: itemWidth,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 8,
  },
  imageProduct: {
    width: '100%',
    height: 119,
    borderWidth: 0,
    borderRadius: 10,
    overflow: 'hidden',
  },
  price: {
    color: '#DB3022',
    // fontSize: AppStyle.textFontSize16,
  },

  availableProduct: {
    borderRadius: 10,
    overflow: 'hidden',
    // height: '100%',
    // width: '100%',
    // fontSize: AppConstants.textSize12,
    textAlign: 'center',
  },
  outOfProduct: {
    ...statusBg,
    color: '#979797',
    backgroundColor: '#eaeaea',
    marginTop: 3,
    height: 15,
    width: '90%',
  },
  percent: {
    backgroundColor: ColorStyle.tabActive,
    color: 'white',
    fontSize: 12,
    ...statusBg,
    position: 'absolute',
    top: 10,
    left: 10,
    overflow: 'hidden',
  },
  bottomView: {
    backgroundColor: '#DC0C1F',
    marginHorizontal: 15,
    height: 5,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    marginTop: 7,
  },
};
