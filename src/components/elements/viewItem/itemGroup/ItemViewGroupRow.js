import React, {Component} from "react";
import AppConstants from "../../../../resource/AppConstants";
import {Text, TouchableOpacity, View} from "react-native";
import ColorStyle from "../../../../resource/ColorStyle";
import MyFastImage from "../../MyFastImage";
import Styles from "../../../../resource/Styles";
import {strings} from "../../../../resource/languages/i18n";
import TimeUtils from "../../../../Utils/TimeUtils";
import constants from "../../../../Api/constants";
import GroupHandle from "../../../../sagas/GroupHandle";

export default class ItemViewGroupRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            members: 0,
            canSendRequest: false,
            requestState: AppConstants.GROUP_APPROVE_STATUS.NOTHING,
            item: this.props.item,
        };
    }
    componentDidMount() {this.getButtonText(this.props.item.is_request);}
    render() {
        return (
            <TouchableOpacity
                onPress={() => {
                    this.props.onPress(this.props.item);
                }}
                style={{
                    width:Styles.constants.X*4,
                    height:Styles.constants.X*4,
                    marginHorizontal: 10,
                    backgroundColor: ColorStyle.tabWhite,
                    borderBottomColor: 'rgba(0, 0, 0, 0.05)',
                    shadowColor: 'black',
                    shadowOffset: {width: 2, height: 1},
                    shadowOpacity: 0.26, elevation: 2,
                    borderRadius: 10,
                }}>
                <MyFastImage
                    source={{
                        uri: this.getAvatarUrl(),
                        headers: {Authorization: 'someAuthToken'},
                    }}
                    style={{ width:Styles.constants.X*4,height: '40%',marginRight: 10}}
                    resizeMode={'cover'}
                />
                <View
                    style={{
                        alignItems: 'flex-start',
                        justifyContent: 'space-between',
                        flex: 6,
                    }}>
                    <Text
                        numberOfLines={2}
                        style={
                        {... Styles.text.text16,color: ColorStyle.tabBlack, fontWeight: '700', maxWidth: '90%',margin:10}
                        }>
                        {this.getGroupName()}
                    </Text>
                    <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>

                        <Text
                            style={[
                                Styles.text.text10,
                                {color: ColorStyle.gray, fontWeight: '400', marginLeft: 5,marginBottom:10},
                            ]}
                            numberOfLines={1}
                            ellipsizeMode="tail">
                            {strings('createDate')}: {TimeUtils.timeDiff(this.props.item.created_date)}
                        </Text>
                    </View>
                </View>

            </TouchableOpacity>
        );
    }
    getImageUrl() {
        let cover = this.props.item.cover;
        if (cover !== undefined && cover.thumbnail_url !== undefined) {
            return constants.host + cover.thumbnail_url;
        }
        return 'https://picsum.photos/130/214?' + this.props.index;
    }
    getGroupName() {
        return this.props.item.name;
    }
    joinGroup() {
        let param = {
            team_id:this.props.item.id,
            team_name:this.props.item.name
        };
        GroupHandle.getInstance().requestJoinGroup(
            param,
            (isSuccess, responseData) => {
                if(isSuccess){
                    this.getButtonText(true)
                }
            },
        );
    }
    getAvatarUrl() {
        let avatar = this.props.item.image_url;
        if (avatar !== undefined ) {
            return constants.host + avatar;
        }
        return 'https://picsum.photos/130/214?' + this.props.index;
    }
    getMembers() {
        let param = {};
        GroupHandle.getInstance().getMemberOfGroup(
            this.props.item.id,
            param,
            (isSuccess, dataResponse) => {
                if (isSuccess && dataResponse.data.list !== undefined) {
                    this.setState({members: dataResponse.data.list.length});
                }
            },
        );
    }
    getButtonText(state) {
        if (!state) {
            this.setState({canSendRequest: true, text: strings('join')});
        } else {
            this.setState({canSendRequest: false, text: strings('sentRequest')});
        }
    }
}
