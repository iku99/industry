import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import CurrencyFormatter from '../../../../Utils/CurrencyFormatter';
import AppConstants from '../../../../resource/AppConstants';
import MediaUtils from '../../../../Utils/MediaUtils';
import MyFastImage from '../../MyFastImage';
import LinearGradient from 'react-native-linear-gradient';
import constants from '../../../../Api/constants';
import FuncUtils from '../../../../Utils/FuncUtils';
import GroupHandle from '../../../../sagas/GroupHandle';
import {strings} from '../../../../resource/languages/i18n';
import {Icon} from 'react-native-elements';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';

export default class BestGroupItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      canSendRequest: false,
      requestState: AppConstants.GROUP_APPROVE_STATUS.NOTHING,
      item: this.props.item,
      members:0
    };
  }
  componentDidMount() {
    this.getButtonText(this.props.item.state);
  }
  render() {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.onPress(this.props.item);
        }}
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 15,
          marginVertical: 5,
          borderWidth: 0.5,
          borderColor: '#A7CDCC',
          borderRadius: 10,
        }}>
        <MyFastImage
          source={MediaUtils.getStoreAvatar(this.props.item.image_url,this.props.item.id)}
          style={[Styles.icon.iconGroup, {marginRight: 10}]}
          resizeMode={'cover'}
        />
        <View style={{alignItems: 'flex-start', flex: 6}}>
          <Text
            numberOfLines={1}
            style={[
              Styles.text.text12,
              {color: ColorStyle.tabBlack, fontWeight: '700', maxWidth: '70%'},
            ]}>
            {this.getGroupName()}
          </Text>
          <Text
            style={[
              Styles.text.text10,
              {color: ColorStyle.gray, fontWeight: '400',marginTop:5},
            ]}
            numberOfLines={1}
            ellipsizeMode="tail">
            {this.state.members} {strings('members')}
          </Text>
        </View>
        <View
          style={{
            padding: 5,
            borderRadius: 5,
            backgroundColor: '#F6F6F6',
            justifyContent: 'center',
          }}>
          <Icon name={'add'} size={22} />
        </View>
      </TouchableOpacity>
    );
  }
  getGroupName() {
    return this.props.item.name;
  }
  joinGroup() {
    FuncUtils.getInstance().callRequireLogin(() => {
      let param = {
          team_id:this.props.item.id
      };
      GroupHandle.getInstance().requestJoinGroup(
        param,
        (isSuccess, responseData) => {
          this.getButtonText(AppConstants.GROUP_APPROVE_STATUS.PENDING);
        },
      );
    });
  }
    getImageUrl(image,index) {
        let cover = image.image_url;
        if (cover !== undefined ) {
            return constants.host + cover;
        }
        return 'https://picsum.photos/130/214?' +index;
    }
  getButtonText(state) {
    if (
      state === undefined ||
      state === AppConstants.GROUP_APPROVE_STATUS.NOTHING
    ) {
      this.setState({canSendRequest: true, text: strings('joinGroupCap')});
    } else {
      this.setState({canSendRequest: false, text: strings('sentRequest')});
    }
  }
}
