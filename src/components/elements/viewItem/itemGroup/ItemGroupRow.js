import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import CurrencyFormatter from '../../../../Utils/CurrencyFormatter';
import AppConstants from '../../../../resource/AppConstants';
import MediaUtils from '../../../../Utils/MediaUtils';
import MyFastImage from '../../MyFastImage';
import LinearGradient from 'react-native-linear-gradient';
import constants from '../../../../Api/constants';
import FuncUtils from '../../../../Utils/FuncUtils';
import GroupHandle from '../../../../sagas/GroupHandle';
import {strings} from '../../../../resource/languages/i18n';
import {Icon} from 'react-native-elements';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';

export default class ItemGroupRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      canSendRequest: false,
      requestState: AppConstants.GROUP_APPROVE_STATUS.NOTHING,
      item: this.props.item,
      members:0
    };
  }
  componentDidMount() {}
  render() {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.onPress(this.props.item);
        }}
        style={{
          justifyContent: 'center',
          width: 100,
          height: 100,
          alignItems: 'center',
          padding: 15,
          marginVertical: 5,
          borderWidth: 0.5,
          borderColor: '#A7CDCC',
          borderRadius: 10,
          marginHorizontal: 5,
        }}>
        <Image
          style={[Styles.icon.iconGroup, {borderRadius: 50, marginBottom: 10}]}
          source={{uri: this.getImageUrl(this.props.item)}}
        />
        <View style={{alignItems: 'center'}}>
          <Text
            numberOfLines={1}
            style={[
              Styles.text.text12,
              {
                color: ColorStyle.tabBlack,
                fontWeight: '700',
                width: '100%',
                maxWidth: '95%',
              },
            ]}>
            {this.getGroupName()}
          </Text>
          <Text
            numberOfLines={1}
            style={[
              Styles.text.text10,
              {color: ColorStyle.gray, fontWeight: '400', marginTop: 5},
            ]}
            ellipsizeMode="tail">
            {this.props.item.total_member} {strings('members')}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
  getMembers() {
    let param = {};
    GroupHandle.getInstance().getMemberOfGroup(
      this.props.item.id,
      param,
      (isSuccess, dataResponse) => {
        if (isSuccess && dataResponse.data.list !== undefined) {
          this.setState({members: dataResponse.data.list.length});
        }
      },
    );
  }
  getImageUrl(image,index) {
    let cover = image.image_url;
    if (cover !== undefined ) {
      return constants.host + cover;
    }
    return 'https://picsum.photos/130/214?' +index;
  }
  getGroupName() {
    return this.props.item.name;
  }
  joinGroup() {
    FuncUtils.getInstance().callRequireLogin(() => {
      let param = {};
      GroupHandle.getInstance().requestJoinGroup(
        this.props.item.id,
        param,
        (isSuccess, responseData) => {
          this.getButtonText(AppConstants.GROUP_APPROVE_STATUS.PENDING);
        },
      );
    });
  }
  getAvatarUrl() {
    let avatar = this.props.item.avatar;
    if (avatar !== undefined && avatar.thumbnail_url !== undefined) {
      return constants.host + avatar.thumbnail_url;
    }
    return 'https://picsum.photos/130/214?' + this.props.index;
  }
  getButtonText(state) {
    if (
      state === undefined ||
      state === AppConstants.GROUP_APPROVE_STATUS.NOTHING
    ) {
      this.setState({canSendRequest: true, text: strings('joinGroupCap')});
    } else {
      this.setState({canSendRequest: false, text: strings('sentRequest')});
    }
  }
}
