import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import CurrencyFormatter from '../../../../Utils/CurrencyFormatter';
import AppConstants from '../../../../resource/AppConstants';
import MediaUtils from '../../../../Utils/MediaUtils';
import MyFastImage from '../../MyFastImage';
import LinearGradient from 'react-native-linear-gradient';
import constants from '../../../../Api/constants';
import FuncUtils from '../../../../Utils/FuncUtils';
import GroupHandle from '../../../../sagas/GroupHandle';
import {strings} from '../../../../resource/languages/i18n';
import {Icon} from 'react-native-elements';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
import TimeUtils from '../../../../Utils/TimeUtils';

export default class ItemGroups extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      members: 0,
      canSendRequest: false,
      requestState: AppConstants.GROUP_APPROVE_STATUS.NOTHING,
      item: this.props.item,
    };
  }
  componentDidMount() {this.getButtonText(this.props.item.is_request);}
  render() {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.onPress(this.props.item);
        }}
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 15,
          marginVertical: 10,
            backgroundColor: ColorStyle.tabWhite,
            borderBottomColor: 'rgba(0, 0, 0, 0.05)',
            shadowColor: 'black',
            shadowOffset: {width: 2, height: 1},
            shadowOpacity: 0.26, elevation: 2,
          borderRadius: 10,
        }}>
        <MyFastImage
          source={{
            uri: this.getAvatarUrl(),
            headers: {Authorization: 'someAuthToken'},
          }}
          style={[Styles.icon.imgGroup, {marginRight: 10}]}
          resizeMode={'cover'}
        />
        <View
          style={{
            alignItems: 'flex-start',
            justifyContent: 'space-between',
            flex: 6,
          }}>
          <Text
            numberOfLines={1}
            style={[
              Styles.text.text16,
              {color: ColorStyle.tabBlack, fontWeight: '700', maxWidth: '90%'} ,
            ]}>
            {this.getGroupName()}
          </Text>
          <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
            <Text
              style={[
                Styles.text.text10,
                {color: ColorStyle.gray, fontWeight: '400'},
              ]}
              numberOfLines={1}
              ellipsizeMode="tail">
              +{this.props.item.total_member} {strings('members')}
            </Text>
            <Text
              style={[
                Styles.text.text10,
                {color: ColorStyle.gray, fontWeight: '400', marginLeft: 5},
              ]}
              numberOfLines={1}
              ellipsizeMode="tail">
              + {TimeUtils.timeDiff(this.props.item.created_date)}
            </Text>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => this.joinGroup()}
          style={{
            borderRadius: 5,
            padding:5,
            alignSelf: 'center',
            alignItems: 'center',
            backgroundColor: '#b7b0b0',
            justifyContent: 'center',
            display:this.props.showJoin?'none':'flex'
          }}>
          <Text style={{...Styles.text.text11, fontWeight: '700'}}>
            {this.state.text}
          </Text>
        </TouchableOpacity>
      </TouchableOpacity>
    );
  }
  getImageUrl() {
    let cover = this.props.item.cover;
    if (cover !== undefined && cover.thumbnail_url !== undefined) {
      return constants.host + cover.thumbnail_url;
    }
    return 'https://picsum.photos/130/214?' + this.props.index;
  }
  getGroupName() {
    return this.props.item.name;
  }
  joinGroup() {
    let param = {
        team_id:this.props.item.id,
        team_name:this.props.item.name
    };
    GroupHandle.getInstance().requestJoinGroup(
      param,
      (isSuccess, responseData) => {
       if(isSuccess){
           this.getButtonText(true)
       }
      },
    );
  }
  getAvatarUrl() {
    let avatar = this.props.item.image_url;
    if (avatar !== undefined ) {
      return constants.host + avatar;
    }
    return 'https://picsum.photos/130/214?' + this.props.index;
  }
  getMembers() {
    let param = {};
    GroupHandle.getInstance().getMemberOfGroup(
      this.props.item.id,
      param,
      (isSuccess, dataResponse) => {
        if (isSuccess && dataResponse.data.list !== undefined) {
          this.setState({members: dataResponse.data.list.length});
        }
      },
    );
  }
  getButtonText(state) {
    if (!state) {
      this.setState({canSendRequest: true, text: strings('join')});
    } else {
      this.setState({canSendRequest: false, text: strings('sentRequest')});
    }
  }
}
