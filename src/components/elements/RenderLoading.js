import React from 'react';
import {ActivityIndicator, View} from 'react-native';
import Styles from '../../resource/Styles';
import ColorStyle from '../../resource/ColorStyle';
const X = Styles.constants.X;
export default function RenderLoading({loading, styles}) {
  if (loading) {
    return (
      <View
        style={{
          width: '100%',
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          ...styles,
        }}>
        <View
          style={{
            position: 'absolute',
            width: X * 1.5,
            height: X * 1.5,
            backgroundColor: ColorStyle.tabWhite,
            alignItems: 'center',
            justifyContent: 'center',
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.2,
            shadowRadius: 1.41,
            alignSelf: 'center',
            elevation: 2,
            borderRadius: X / 8,
          }}>
          <ActivityIndicator color={ColorStyle.tabActive} size={X * 0.8} />
        </View>
      </View>
    );
  } else {
    return <View />;
  }
}
