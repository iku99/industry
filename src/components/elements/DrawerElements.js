import React, {Component} from 'react';
import {
  Image,
  SafeAreaView,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  FlatList,
  AsyncStorage,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import ColorStyle from '../../resource/ColorStyle';
import Styles from '../../resource/Styles';
import {Icon} from 'react-native-elements';
import BadgeView from './BadgeView';
import ImageHelper from '../../resource/images/ImageHelper';
import {strings} from '../../resource/languages/i18n';
import AppConstants from '../../resource/AppConstants';
import GlobalInfo from '../../Utils/Common/GlobalInfo';
import DataUtils from '../../Utils/DataUtils';
import {EventRegister} from 'react-native-event-listeners';
import StoreHandle from '../../sagas/StoreHandle';
import MediaUtils from '../../Utils/MediaUtils';

const X = Styles.constants.X;
class DrawerElements extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogged: false,
      avatar: undefined,
      userName: '',
      hasStore: undefined,
      data: undefined,
    };
  }

  componentDidMount() {
    this.setState({data: GlobalInfo.userInfo});
    this.getMyStoreInfo();
    AsyncStorage.getItem(
      AppConstants.SharedPreferencesKey.IsLogin,
      (error, isLogin) => {
        this.setState({isLogged: isLogin === '1'});
      },
    );

    EventRegister.addEventListener(AppConstants.EventName.ADD_NEW_STORE, _ => {
      this.setState({hasStore: true});
    });
    EventRegister.addEventListener(
      AppConstants.EventName.CHANGE_AVATAR,
      data => {
        this.setState({data: {}}, () => {
          this.setState({data: data});
        });
      },
    );
  }
  render() {
    if (this.state.isLogged) {
      let name = this.state.data?.full_name;
      let mail = this.state.data?.email;
      if (DataUtils.stringNullOrEmpty(name)) {
        name = strings('noName');
      }
      if (DataUtils.stringNullOrEmpty(name)) {
        mail = strings('noMail');
      }
      return (
        <View
          style={{
            width: Styles.constants.widthScreen,
            height: '100%',
            backgroundColor: ColorStyle.tabActive,
          }}>
          <View
            style={{
              width: Styles.constants.widthScreenMg24,
              alignSelf: 'center',
              position: 'absolute',
              marginTop: Styles.constants.X * 0.9,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={() => Actions.drawerClose()}>
                <Icon
                  name="close"
                  style="entypo"
                  size={25}
                  color={ColorStyle.tabWhite}
                />
              </TouchableOpacity>
              <Image
                source={ImageHelper.logo}
                style={{
                  width: Styles.constants.X * 3,
                  height: Styles.constants.X * 0.6,
                }}
              />
              <TouchableOpacity
                onPress={() => {
                  EventRegister.emit(AppConstants.EventName.GO_TO_TAB, 3);
                  Actions.drawerClose();
                }}>
                <Icon
                  name="shopping-cart"
                  style="entypo"
                  size={25}
                  color={ColorStyle.tabWhite}
                />
                <BadgeView />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() => Actions.jump('personal')}
              style={{
                marginTop: X * 0.2,
                padding: X * 0.2,
                justifyContent: 'flex-start',
                flexDirection: 'row',
                alignItems: 'center',
                display: this.state.isLogged ? 'flex' : 'none',
              }}>
              <Image
                source={MediaUtils.getAvatar(this.state.data)}
                style={Styles.icon.icon_avatar}
              />
              <Text
                style={[
                  Styles.text.text20,
                  {marginLeft: X * 0.4, fontWeight: '700'},
                ]}>
                {name}
              </Text>
            </TouchableOpacity>
            <View
              style={{
                width: '100%',
                height: 1,
                backgroundColor: ColorStyle.tabWhite,
              }}
            />
            {this.renderItem(
              strings('home'),
              {name: 'home', type: 'simple-line-icons'},
              () => Actions.drawerClose(),
            )}
            {this.renderItem(
              strings('supplier'),
              {name: 'storefront', type: 'materialicons'},
              () => {
                Actions.jump('supplierScreen');
              },
            )}
            {this.renderItem(
              strings('group'),
              {name: 'account-group', type: 'material-community'},
              () => Actions.jump('groups'),
            )}
            {this.renderItem(
              strings('news'),
              {name: 'newspaper-o', type: 'font-awesome'},
              () => Actions.jump('newsScreen'),
            )}
            {this.renderItem(
              strings('bidding'),
              {name: 'crane', type: 'material-community'},
              () => Actions.jump('biddingScreen'),
            )}
          </View>
        </View>
      );
    } else {
      return (
        <View style={{}}>
          <View
            style={{
              width: Styles.constants.widthScreen,
              height: '100%',
              backgroundColor: ColorStyle.tabActive,
            }}>
            <View
              style={{
                width: Styles.constants.widthScreenMg24,
                alignSelf: 'center',
                position: 'absolute',
                marginTop: Styles.constants.X * 0.9,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <TouchableOpacity onPress={() => Actions.drawerClose()}>
                  <Icon
                    name="close"
                    style="entypo"
                    size={25}
                    color={ColorStyle.tabWhite}
                  />
                </TouchableOpacity>
                <Image
                  source={ImageHelper.logo}
                  style={{
                    width: Styles.constants.X * 3,
                    height: Styles.constants.X * 0.6,
                  }}
                />
                <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                  <Icon
                    name="shopping-cart"
                    style="entypo"
                    size={25}
                    color={ColorStyle.tabWhite}
                  />
                  {/*<Text style={{position:"absolute",right:0,top:0}}>1</Text>*/}
                  <BadgeView />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  marginTop: X * 0.2,
                  padding: X * 0.2,
                  justifyContent: 'flex-start',
                  borderBottomColor: ColorStyle.colorPersonal,
                  borderBottomWidth: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  display: this.state.isLogged ? 'flex' : 'none',
                }}
              />
              {this.renderItem(
                strings('home'),
                {name: 'home', type: 'simple-line-icons'},
                () => Actions.drawerClose(),
              )}
              {this.renderItem(
                strings('supplier'),
                {name: 'storefront', type: 'materialicons'},
                () => {
                  Actions.jump('supplierScreen');
                },
              )}
              {this.renderItem(
                strings('group'),
                {name: 'account-group', type: 'material-community'},
                () => Actions.jump('login'),
              )}
              {this.renderItem(
                strings('news'),
                {name: 'newspaper-o', type: 'font-awesome'},
                () => Actions.jump('newsScreen'),
              )}
              {this.renderItem(
                strings('bidding'),
                {name: 'crane', type: 'material-community'},
                () => Actions.jump('biddingScreen'),
              )}
              <View
                style={{
                  marginTop: X * 0.7,
                  borderTopColor: ColorStyle.colorPersonal,
                  borderTopWidth: 1,
                  alignItems: 'center',
                  paddingTop: X * 0.7,
                }}>
                <TouchableOpacity
                  onPress={() => Actions.jump('login')}
                  style={[
                    Styles.button.buttonLogin,
                    {backgroundColor: ColorStyle.tabWhite},
                  ]}>
                  <Text
                    style={[
                      Styles.text.text20,
                      {color: ColorStyle.textTitle24, fontWeight: '700'},
                    ]}>
                    {strings('member')}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      );
    }
  }
  renderItem(title, icon, onPress) {
    return (
      <TouchableOpacity
        style={{
          marginTop: X * 0.7,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-start',
        }}
        onPress={onPress}>
        <Icon
          name={icon.name}
          type={icon.type}
          size={25}
          color={ColorStyle.tabWhite}
        />
        <Text style={[Styles.text.text18, {fontWeight: '700', marginLeft: 10}]}>
          {title.toUpperCase()}
        </Text>
      </TouchableOpacity>
    );
  }
  async getMyStoreInfo() {
    StoreHandle.getInstance().getMyStore((isSuccess, responseData) => {
      if (isSuccess && responseData !== undefined) {
        if (responseData.data != null) {
          this.setState({hasStore: true});
          return;
        }
      }
      this.setState({hasStore: false});
    });
  }
}
export default DrawerElements;
