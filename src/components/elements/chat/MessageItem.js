import {Text, View} from 'react-native';
import React, {Component} from 'react';
import AppConstants from '../../../resource/AppConstants';
import constants from '../../../Api/constants';
import MyFastImage from '../MyFastImage';
import Styles from '../../../resource/Styles';
import Ripple from 'react-native-material-ripple';
import GlobalInfo from '../../../Utils/Common/GlobalInfo';
import NavigationUtils from '../../../Utils/NavigationUtils';
import ColorStyle from '../../../resource/ColorStyle';
import DateUtil from '../../../Utils/DateUtil';
let imagWidth = 200;
let imagHeight = 200;
export default class MessageItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      calcImgHeight: imagHeight,
      calcImgWidth: imagWidth,
    };
  }
  render() {
    let item = this.props.item;

    let user_info = this.props.user_info;
    let sameNextMsgSender = this.props.sameNextMsgSender;
    let msgType = item.messageType;
    let isOutComming = item.user === undefined || item.user.id === user_info.id;

    switch (msgType) {
      case AppConstants.MESSAGE_TYPE.IMAGE:
        return (
          <View style={{marginTop: 10}}>
            {isOutComming
              ? this.imgageOutcoming(item, sameNextMsgSender)
              : this.imgaeIncoming(item, sameNextMsgSender)}
          </View>
        );
      case AppConstants.MESSAGE_TYPE.TEXT:
        return (
          <View style={{marginTop: 10}}>
            {isOutComming
              ? this.textOutcoming(item, sameNextMsgSender)
              : this.textIncoming(item, sameNextMsgSender)}
          </View>
        );
      case AppConstants.MESSAGE_TYPE.DATE:
        return this.dateItem(item);
      default:
        return null;
    }
  }

  imgaeIncoming(item, sameNextMsgSender) {
    let url = constants.host + item.image_link;
    return (
      <View style={{alignItems: 'flex-start'}}>
        <View style={{alignItems: 'flex-end', flexDirection: 'row'}}>
          <Ripple
            style={{
              padding: 0,
              overflow:'hidden',
            }}
            onPress={() => {
              this.openImage(url);
            }}>
            <MyFastImage
              style={{
                width: this.state.calcImgWidth,
                height: this.state.calcImgHeight,
              }}
              source={{
                uri: url,
                headers: {Authorization: 'someAuthToken'},
              }}
              resizeMode={'contain'}
              onLoad={evt => {
                let calcImgWidth = 0;
                let calcImgHeight = 0;
                if (evt.nativeEvent.height > evt.nativeEvent.width) {
                  //anh doc
                  calcImgWidth =
                    imagWidth < 0.65 * Styles.constants.widthScreen
                      ? imagWidth
                      : 0.65 * Styles.constants.widthScreen;
                  calcImgHeight =
                    (evt.nativeEvent.height / evt.nativeEvent.width) *
                    calcImgWidth;
                } else {
                  //anh ngang
                  calcImgWidth =
                    (evt.nativeEvent.width / evt.nativeEvent.height) *
                    imagHeight;
                  calcImgWidth =
                    calcImgWidth < 0.65 * Styles.constants.widthScreen
                      ? calcImgWidth
                      : 0.65 * Styles.constants.widthScreen;
                  calcImgHeight =
                    (evt.nativeEvent.height / evt.nativeEvent.width) *
                    calcImgWidth;
                }
                this.setState({calcImgWidth, calcImgHeight});
              }}
            />
            <Text
              style={{
                color: 'white',
                maxWidth: this.state.calcImgWidth,
                padding: 8,
              }}>
              {item.body}
            </Text>
          </Ripple>
        </View>
      </View>
    );
  }
  imgageOutcoming(item, sameNextMsgSender) {
    let url = this.getUrl(item.image_link);
    return (
      <View style={{alignItems: 'flex-end'}}>
        <View style={{alignItems: 'center', flexDirection: 'row'}}>
          <Ripple
            style={{
              padding: 0,
              overflow:'hidden',
            }}
            onPress={() => {
              this.openImage(url);
            }}>
            <MyFastImage
              style={{
                width: this.state.calcImgWidth,
                height: this.state.calcImgHeight,
              }}
              source={{
                uri: url,
                headers: {Authorization: 'someAuthToken'},
              }}
              resizeMode={'contain'}
              onLoad={evt => {
                let calcImgWidth = 0;
                let calcImgHeight = 0;
                if (evt.nativeEvent.height > evt.nativeEvent.width) {
                  //anh doc
                  calcImgWidth =
                    imagWidth < 0.65 * Styles.constants.widthScreen
                      ? imagWidth
                      : 0.65 * Styles.constants.widthScreen;
                  calcImgHeight =
                    (evt.nativeEvent.height / evt.nativeEvent.width) *
                    calcImgWidth;
                } else {
                  //anh ngang
                  calcImgWidth =
                    (evt.nativeEvent.width / evt.nativeEvent.height) *
                    imagHeight;
                  calcImgWidth =
                    calcImgWidth < 0.65 * Styles.constants.widthScreen
                      ? calcImgWidth
                      : 0.65 * Styles.constants.widthScreen;
                  calcImgHeight =
                    (evt.nativeEvent.height / evt.nativeEvent.width) *
                    calcImgWidth;
                }
                this.setState({calcImgWidth, calcImgHeight});
              }}
            />
            <Text
              style={{
                color: 'white',
                maxWidth: this.state.calcImgWidth,
                padding: 8,
              }}>
              {item.body}
            </Text>
          </Ripple>
        </View>
      </View>
    );
  }
  textIncoming(item, sameNextMsgSender) {
    return (
      <View style={{alignItems: 'flex-start'}}>
        <Text style={{...Styles.text.text10}}>
          {DateUtil.formatDate('HH:MM DD/MM/YYYY', item.created_at)}
        </Text>

        <View style={{alignItems: 'flex-end', flexDirection: 'row'}}>
          <Ripple
            style={{
              paddingVertical: 14,
              paddingHorizontal: 10,
              backgroundColor: ColorStyle.tabWhite,
              borderRadius: 15,
            }}>
            <Text
              style={{
                color: ColorStyle.tabBlack,
                ...Styles.text.text14,
                maxWidth: Styles.constants.widthScreen / 2,
              }}>
              {item.body}
            </Text>
          </Ripple>
        </View>
      </View>
    );
  }
  textOutcoming(item) {
    return (
      <View style={{alignItems: 'flex-end'}}>
        <Text style={{...Styles.text.text10}}>
          {' '}
          {DateUtil.formatDate('HH:MM DD/MM/YYYY', item.created_at)}
        </Text>

        <View style={{alignItems: 'center', flexDirection: 'row'}}>
          <Ripple
            style={{
              paddingVertical: 14,
              paddingHorizontal: 10,
              backgroundColor: '#014B56',
              borderRadius: 15,
            }}>
            <Text
              style={{
                ...Styles.text.text14,
                maxWidth: Styles.constants.widthScreen / 2,
                color: 'white',
              }}>
              {item.body}
            </Text>
          </Ripple>
        </View>
      </View>
    );
  }

  dateItem(item) {
    return (
      <View
        style={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          paddingVertical: 10,
        }}>
        <Text style={{}}>{item.messageTime}</Text>
      </View>
    );
  }
  getUrl(thumbnail) {
    let url = '';
    if (thumbnail.startsWith(AppConstants.LOCAL_SIGNAL)) {
      url = thumbnail.replace(AppConstants.LOCAL_SIGNAL, '');
    } else {
      url = GlobalInfo.initApiEndpoint(thumbnail);
    }
    return url;
  }
  openImage(imageUrl) {
    NavigationUtils.goToFullViewImageList([imageUrl], 0);
  }
}
