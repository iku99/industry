import React, {Component} from 'react';
import {Text, View} from 'react-native';
import Ripple from 'react-native-material-ripple';
import MyFastImage from '../MyFastImage';
import constants from '../../../Api/constants';
import {strings} from '../../../resource/languages/i18n';
import Styles from '../../../resource/Styles';
import ColorStyle from '../../../resource/ColorStyle';
import DateUtil from "../../../Utils/DateUtil";
export default class ChatItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {}
  render() {
    let item = this.props.item;
    let unreadCount = item.unread_count > 0;
    return (
      <Ripple
        style={{
          paddingVertical: 12,
          backgroundColor: ColorStyle.tabWhite,
          borderColor: 'rgba(0, 0, 0, 0.1)',
          marginVertical: 5,
          elevation: 2,
          paddingLeft: 35,
          paddingRight: 5,
          flexDirection:'row'
        }}
        onPress={() => {
          if (
            this.props.goToConversation != null &&
            item !== undefined &&
            item.user !== undefined &&
            item.user.id !== undefined
          ) {
            this.props.goToConversation(item.user.id);
          }
        }}>
        <MyFastImage
          style={{
            ...Styles.icon.iconShow,
            borderRadius: 20,
            overflow: 'hidden',
          }}
          source={{
            uri: this.getAvatarUrl(item),
            headers: {Authorization: 'someAuthToken'},
          }}
          resizeMode={'cover'}
        />

        <View
          style={{
            flex: 1,
            justifyContent: 'space-between',
            marginHorizontal: 10,
          }}>
          <Text numberOfLines={1} style={{...Styles.text.text12,fontWeight:'700',color:ColorStyle.tabBlack}} ellipsizeMode="tail">
            {this.getName(item)}
          </Text>
          <Text  style={{...Styles.text.text10,fontWeight:'400',color:'rgba(49, 49, 49, 0.5)'}} numberOfLines={1} ellipsizeMode="tail">
            {this.getMsg(item).trim()}
          </Text>
        </View>

        <View style={{position:'absolute',right:10,top:5}}>
          <Text style={{...Styles.text.text10,fontWeight:'400',color:'rgba(49, 49, 49, 0.5)'}}>  {DateUtil.formatDate('DD.MM.YYYY', item.created_at)}</Text>
        </View>
        {this.props.count - 1 > this.props.index && (
          <View
            style={{
              position: 'absolute',
              bottom: 0,
              left: '5%',
              height: 1,
              right: '5%',
            }}
          />
        )}
      </Ripple>
    );
  }
  getAvatarUrl(item) {
    if (
      item === undefined ||
      item.user === undefined ||
      item.user.avatar === undefined
    ) {
      return 'https://scr.vn/wp-content/uploads/2020/07/Avatar-Facebook-tr%E1%BA%AFng.jpg';
    }
    return item.user.avatar;
  }
  getMsg(item) {
    if (item.typing) {
      return strings('isTyping');
    }
    return item.last_message;
  }
  getName(item) {
    if (item === undefined || item.user === undefined) {
      return '';
    }
    return item.user.full_name;
  }
}
