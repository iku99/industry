import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../resource/Styles';
import {Icon} from 'react-native-elements';
import ColorStyle from '../../../resource/ColorStyle';
import {Actions} from 'react-native-router-flux';

export default class ToolbarSeller extends Component {
  render() {
    return (
      <View style={[styles.container,{backgroundColor:this.props.backgroundColor===undefined?ColorStyle.tabWhite:ColorStyle.tabActive}]}>
        <Text
          style={[
            Styles.text.text20,
            {
              color:this.props.backgroundColor===undefined?ColorStyle.tabActive:ColorStyle.tabWhite,
              fontWeight: '500',
              top: 0,
              textAlign: 'center',
            },
          ]}
          numberOfLines={1}>
          {this.props.title}
        </Text>
        <TouchableOpacity
          onPress={() => Actions.pop()}
          style={{
            borderRadius: 200,
            position: 'absolute',
            paddingVertical: Styles.constants.X  ,
            paddingHorizontal: Styles.constants.X * 0.4,
            top: 0,
            left: 0,
          }}>
          <Icon
            name="left"
            type="antdesign"
            size={25}
            color={this.props.backgroundColor===undefined?ColorStyle.tabActive:ColorStyle.tabWhite}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.props.onPressMess}
          style={{
            position: 'absolute',
            display: this.props.message ? 'flex' : 'none',
            padding: Styles.constants.X ,
            top: 0,
            right: 0,
          }}>
          <Icon
            name="message-processing-outline"
            type="material-community"
            size={25}
            color={this.props.backgroundColor===undefined?ColorStyle.tabActive:ColorStyle.tabWhite}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    paddingTop: Styles.constants.X,
    paddingBottom: Styles.constants.X * 0.4,
    paddingHorizontal: Styles.constants.X*0.65,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: ColorStyle.tabWhite,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
      elevation: 4,
      shadowColor: 'rgba(63,63,63,0.1)',
      shadowOffset: {width: 0, height: 3},
      shadowOpacity: 0.8,
      shadowRadius: 1,
  },
});
