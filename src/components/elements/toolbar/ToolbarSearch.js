import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../resource/Styles';
import {Icon} from 'react-native-elements';
import ColorStyle from '../../../resource/ColorStyle';
import {Actions} from 'react-native-router-flux';
import {strings} from '../../../resource/languages/i18n';
import NavigationUtils from '../../../Utils/NavigationUtils';
import ImageHelper from '../../../resource/images/ImageHelper';
import ViewUtils from '../../../Utils/ViewUtils';
import AccountHandle from '../../../sagas/AccountHandle';
import BadgeView from '../BadgeView';

export default class ToolbarSearch extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => Actions.pop()}>
          <Icon
            name="left"
            type="antdesign"
            size={25}
            color={ColorStyle.tabWhite}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            Styles.search.searchInput,
            {width: '75%', marginVertical: 10},
          ]}
          onPress={() => {
            NavigationUtils.goToSearchProduct();
          }}>
              <Icon name={'search'} type={'evilicons'} size={25} color={'#a59999'}/>
          <Text
            style={Styles.search.searchText}
            underlineColorAndroid="transparent">
            {strings('textSearch')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{padding: 5}} onPress={()=>NavigationUtils.goToCart()}>
          <Icon
            name="shopping-cart"
            style="entypo"
            size={25}
            color={ColorStyle.tabWhite}
          />
          <BadgeView />
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    paddingTop: Styles.constants.X,
    paddingHorizontal: Styles.constants.X * 0.4,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor:ColorStyle.tabActive,
    borderBottomLeftRadius:20,
    borderBottomEndRadius:20,
  },
});
