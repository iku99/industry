import React, {Component} from 'react';
import {AsyncStorage, Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../resource/Styles';
import {Icon} from 'react-native-elements';
import ColorStyle from '../../../resource/ColorStyle';
import {Actions} from 'react-native-router-flux';
import BadgeView from '../BadgeView';
import {strings} from '../../../resource/languages/i18n';
import MediaUtils from "../../../Utils/MediaUtils";
import {EventRegister} from "react-native-event-listeners";
import AppConstants from "../../../resource/AppConstants";
import GlobalInfo from "../../../Utils/Common/GlobalInfo";

export default class ToolbarMain extends Component {
    constructor(props) {
        super(props);
        this.state={
            isLogged:false,
            userName:undefined
        }
    }
    componentDidMount() {
        AsyncStorage.getItem(
            AppConstants.SharedPreferencesKey.IsLogin,
            (error, isLogin) => {
                this.setState({isLogged: isLogin === '1'});
            },
        );
        EventRegister.addEventListener(
            AppConstants.EventName.CHANGE_AVATAR,
            data => {
                this.setState({userName:{}},()=>{this.setState({userName:data})});
            },
        );
        this.setState({userName: GlobalInfo.userInfo})
    }

    render() {
    return (
      <View
        style={[
          Styles.toolbar.toolbar,
          {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingBottom: Styles.constants.X * 0.4,
          },
        ]}>
        <TouchableOpacity
          onPress={() => Actions.pop()}
          style={{
            display: this.props.iconBack ? 'flex' : 'none',
          }}>
          <Icon
            name="arrow-back-ios"
            type="materialicons"
            size={20}
            color={ColorStyle.tabWhite}
          />
        </TouchableOpacity>
        <Text
          style={[
            Styles.text.text20,
            {
              color: ColorStyle.tabWhite,
              fontWeight: '700',
              textAlign: 'center',
            },
          ]}>
          {this.props.title}
        </Text>

          <TouchableOpacity  style={{display: this.props.iconProfile ? 'flex' : 'none'}} onPress={() => Actions.jump('profile')}>
              {!this.state.isLogged?(<Icon
                  name="user"
                  type="antdesign"
                  size={25}
                  color={ColorStyle.tabWhite}
              />):(
                  <Image source={MediaUtils.getAvatar(this.state.userName)} style={{...Styles.icon.iconAvatarSmall,marginRight:0,}} />
              )}
          </TouchableOpacity>
        <TouchableOpacity
          style={{display: this.props.iconSearch ? 'flex' : 'none'}}
          onPress={() => Actions.jump('profile')}>
          <Icon
            name="search1"
            type="antdesign"
            size={25}
            color={ColorStyle.tabWhite}
          />
        </TouchableOpacity>
        <TouchableOpacity
            onPress={this.props.callBackReadAll}
          style={{display: this.props.readAll ? 'flex' : 'none'}}>
          <Text
            style={{
              ...Styles.text.text12,
              color: ColorStyle.tabWhite,
              fontWeight: '700',
            }}>
            {strings('readAll')}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    width: Styles.constants.widthScreen,
    backgroundColor: ColorStyle.tabActive,
  },
});
