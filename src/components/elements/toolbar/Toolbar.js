import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../resource/Styles';
import {Icon} from 'react-native-elements';
import ColorStyle from '../../../resource/ColorStyle';
import {Actions} from 'react-native-router-flux';

export default class Toolbar extends Component {
  render() {
    return (
      <View
        style={[
          styles.container,
          {
              paddingTop: Styles.constants.X,
            backgroundColor: this.props.backgroundColor,
            paddingBottom: 10,
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20,
          },
        ]}>
        <Text
          style={
            {  ...Styles.text.text20,
              color: this.props.textColor,
              fontWeight: '700',
              textAlign: 'center',
            }}
          numberOfLines={1}>
          {this.props.title}
        </Text>
        <TouchableOpacity
          onPress={() => Actions.pop()}
          style={{
            position: 'absolute',
            paddingTop: Styles.constants.X ,
            paddingHorizontal: Styles.constants.X * 0.4,
            top: 0,
            left: 0,
          }}>
          <Icon
            name="left"
            type="antdesign"
            size={25}
            color={this.props.textColor}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.props.onPress}
          style={{
            borderRadius: 200,
            position: 'absolute',
            display: this.props.edit ? 'flex' : 'none',
              paddingTop: Styles.constants.X ,
              paddingRight:Styles.constants.X * 0.4,
            top: 0,
            right: 0,
          }}>
          <Icon
            name={this.props.status ? 'info' : 'pencil'}
            type="foundation"
            size={25}
            color={ColorStyle.tabWhite}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.props.onPressAdd}
          style={{
            borderRadius: 200,
            position: 'absolute',
            display: this.props.add ? 'flex' : 'none',
            paddingTop: Styles.constants.X ,
              paddingRight:Styles.constants.X * 0.4,
            top: 0,
            right: 0,
          }}>
          <Icon
            name={'add-circle-outline'}
            type="materialicons"
            size={25}
            color={ColorStyle.tabWhite}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.props.onPressSearch}
          style={{
            borderRadius: 200,
            position: 'absolute',
            display: this.props.search ? 'flex' : 'none',
              paddingTop: Styles.constants.X ,
              paddingRight:Styles.constants.X * 0.4,
            top: 0,
            right: 0,
          }}>
          <Icon
            name={'search1'}
            type="antdesign"
            size={25}
            color={ColorStyle.tabWhite}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    paddingBottom: Styles.constants.X * 0.2,
    paddingHorizontal: Styles.constants.X * 0.65,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
