import Styles from "../../../resource/Styles";
import ColorStyle from "../../../resource/ColorStyle";

const X=Styles.constants.X
export default {
    container: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "rgba(82,82,82,0.43)",
        alignItems:'center',
        justifyContent:'center'
        // opacity: 0,
        // weight:100,
        // height:200
    },
    body:{
        width:'80%',
        height:'40%',
        paddingTop:X/5,
        backgroundColor: ColorStyle.tabWhite,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
    },
    bodyIcon:{
        height:'50%',

        alignSelf:'center',
    },
    textTitle:{
        ...Styles.text.text16,
        color:ColorStyle.tabBlack,
        fontWeight:'700',
        paddingHorizontal:X/3,
        textAlign:'center',
        height:'30%'
    },
    viewBtn:{
        width:'100%',
        height:'15%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent: 'space-between',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.4,
        shadowRadius: 1.41,

        elevation: 2,
    },
    btnCancel:{
        width:'50%',
        height:'100%',
        alignItems:'center',
        justifyContent: 'center',
        backgroundColor:ColorStyle.tabWhite
    },
    btnSuccess:{
        width:'50%',
        height:'15%',
        alignItems:'center',
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor:ColorStyle.tabActive,
    },
    textBtnSuccess:{
        ...Styles.text.text15,
        fontWeight: '600',
        color: ColorStyle.tabWhite
    },
    textBtnCancel:{
        ...Styles.text.text15,
        fontWeight: '600',
        color: ColorStyle.tabBlack
    }

}
