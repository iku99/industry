import React from "react";
import {Text, TouchableOpacity, View} from "react-native";
import styles from "./styles";
import {strings} from "../../../resource/languages/i18n";
import {Icon} from "react-native-elements";
import Styles from "../../../resource/Styles";
import ColorStyle from "../../../resource/ColorStyle";
export default function DialogSuccess({item,onSuccess,onCancel}){

    return(
        <View style={styles.container}>
                <View style={styles.body}>

                    <View style={styles.bodyIcon}>
                    <Icon name={'checkcircle'} type={'antdesign'} size={Styles.constants.X*3} color={ColorStyle.tabActive}/>
                    </View>
                    <Text style={styles.textTitle}>{strings('successfulGroup').replace('{name}',item?.name)}</Text>
                    {/*<View style={styles.viewBtn}>*/}
                        {/*<TouchableOpacity style={styles.btnCancel}>*/}
                        {/*    <Text style={styles.textBtnCancel} onPress={()=>onCancel}>{strings('cancel')}</Text>*/}
                        {/*</TouchableOpacity>*/}
                        <TouchableOpacity style={styles.btnSuccess} onPress={()=>onSuccess()}>
                            <Text style={styles.textBtnSuccess}>{strings('goToGroup')}</Text>
                        </TouchableOpacity>
                    {/*</View>*/}
                </View>
        </View>
    )
}
