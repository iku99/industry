import React, {Component} from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {TextInputMask} from 'react-native-masked-text';
import {Dropdown} from 'react-native-material-dropdown-v2';
import SimpleToast from 'react-native-simple-toast';
import ImagePicker from 'react-native-image-crop-picker';
import {PERMISSIONS} from 'react-native-permissions';
import RefundMetaUtils from '../../Utils/RefundMetaUtils';
import GlobalInfo from '../../Utils/Common/GlobalInfo';
import DataUtils from '../../Utils/DataUtils';
import ColorStyle from '../../resource/ColorStyle';
import ImageHelper from '../../resource/images/ImageHelper';
import {strings} from '../../resource/languages/i18n';
import Styles from '../../resource/Styles';
import ViewUtils from '../../Utils/ViewUtils';
import AppConstants from "../../resource/AppConstants";
import MultiSelect from "./MultipleSelect/react-native-multi-select";
import PermissionUtils from "../../Utils/PermissionUtils";
import MediaHandle from "../../sagas/MediaHandle";

let checkEmailId = -1;
let checkPhoneId = -1;
let numberOfLines = 5;
let images = [];
export default class RequestHIACView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItems: [],
      items: this.props.cartInfos,
      email: '',
      phone: '',
      refund: 0,
      note: '',
      validEmail: false,
      validPhone: false,
      validNote: false,
      validPrice: false,
      reasonId: undefined,
      expectResultId: undefined,
      refundMethodId: undefined,
      reasons: [],
      expectResults: [],
      images: [
        {
          type: AppConstants.RETURN_IMAGE_TYPE.ADD_NEW,
        },
      ],
    };
  }
  componentDidMount() {
    this.getDefaultInfo();
    RefundMetaUtils.getInstance().getData(
      (isSuccess, reasons, expectResults) => {
        if (isSuccess) {
          this.setState({reasons, expectResults});
        }
      },
    );
  }
  async getDefaultInfo() {
    let userInfo = await GlobalInfo.getUserInfo();
    this.setState({email: userInfo.email, phone: userInfo.phone}, () => {


    });
  }

  onSelectedItemsChange = selectedItems => {
    this.setState({selectedItems});
  };
  render() {
    return (
      <View
        behavior={'padding'}
        enabled
        style={{
          paddingHorizontal: 10,
          paddingTop: 20,
          width: '100%',
          height: '100%',
        }}>
        <Text>{strings('requestReturn')}</Text>
        <View style={{height: 650}}>
            <FlatList
              contentContainerStyle={{marginBottom: 30}}
              showsHorizontalScrollIndicator={false}
              horizontal={true}
              data={this.state.images}
              keyExtractor={this.keyExtractor}
              renderItem={this._renderItem}
              removeClippedSubviews={true} // Unmount components when outside of window
            />
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={{marginTop: 10}}
            keyboardShouldPersistTaps={'always'}>
            {this.renderSelectingProductView()}
            {this.renderSelectedProduct()}
            {this.renderTextInputWithTitle(
              strings('storeMail'),
              'email',
              'validEmail',
              'checkEmailId',
              () => this.checkEmail(),
              'email-address',
              'next',
              input => {
                this.mailRef = input;
              },
              () => {
                this.phoneRef.focus();
              },
            )}
            {this.renderTextInputWithTitle(
              strings('storePhone'),
              'phone',
              'validPhone',
              'checkPhoneId',
              () => this.checkPhone(),
              'phone-pad',
              'next',
              input => {
                this.phoneRef = input;
              },
              () => {},
            )}
            {this.renderReason()}
            {this.expectResult()}
            <View style={{flex: 1}}>
              <Text>{strings('amountReturn')}</Text>
              <TextInputMask
                type={'money'}
                value={this.state.refund}
                onChangeText={price => {
                  this.changePrice(DataUtils.trimNumber(price));
                }}
                style={{...Styles.input.codeInput, paddingRight: 30}}
                options={{
                  precision: 0,
                  separator: '.',
                  delimiter: ',',
                  unit: '',
                  suffixUnit: '',
                }}
                ref={ref => (this.priceTxt = ref)}
              />
              <Icon
                name={this.state.validPrice ? 'check' : 'alert-circle'}
                type="feather"
                size={15}
                color={this.state.validPrice ? 'green' : 'red'}
                containerStyle={{position: 'absolute', right: 20, bottom: 12}}
              />
            </View>
            {this.renderRefundMethod()}
            <Text>{strings('returnNote')}</Text>
            <TextInput
              style={{marginTop: 5}}
              value={this.state.editComment}
              autoCorrect={false}
              placeholderTextColor={'#000'}
              onChangeText={note => {
                this.setState({note, validNote: note.length > 5});
              }}
              ref={ref => {
                this.txtComment = ref;
              }}
              multiline={true}
              numberOfLines={numberOfLines}
              minHeight={20 * (numberOfLines + 2)}
              underlineColorAndroid="transparent"
            />
            <Text>{strings('returnImages')}</Text>
            {/*<FlatList*/}
            {/*  contentContainerStyle={{marginBottom: 30}}*/}
            {/*  showsHorizontalScrollIndicator={false}*/}
            {/*  horizontal={true}*/}
            {/*  data={this.state.images}*/}
            {/*  keyExtractor={this.keyExtractor}*/}
            {/*  renderItem={this._renderItem}*/}
            {/*  removeClippedSubviews={true} // Unmount components when outside of window*/}
            {/*/>*/}
          </ScrollView>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            style={{flex: 1, marginHorizontal: 10}}
            onPress={() => {
              if (this.props.onClose !== undefined) {
                this.props.onClose();
              }
            }}>
            <View style={{marginTop: 0}} animation="fadeInUpBig">
              <Text style={{color: ColorStyle.previousText, fontSize: 14}}>
                {strings('cancel')}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{flex: 1, marginHorizontal: 10}}
            onPress={() => {
              this.refundOrder();
            }}>
            <View animation="fadeInUpBig">
              <Text style={{color: 'white', fontSize: 14}}>
                {strings('sendRequest')}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  keyExtractor = (item, index) => `requestHiacView_${index.toString()}`;
  _renderItem = ({item, index}) => this.renderItem(item, index);
  renderItem(item, index) {
    if (item.type === AppConstants.RETURN_IMAGE_TYPE.ADD_NEW) {
      return (
        <TouchableOpacity
          style={{
            borderWidth: 1,
            borderStyle: 'dashed',
            flex: 1,
            width: 100,
            borderRadius: 5,
            justifyContent: 'center',
            alignItems: 'center',
            aspectRatio: 1,
            marginHorizontal: 4,
            padding: 3,
          }}
          onPress={() => {
            this.onSelectImage(index);
          }}>
          <Icon name="plus" type="evilicon" size={30} />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          style={{
            marginHorizontal: 4,
            padding: 3,
          }}>
          <Image
            style={{width: 100, height: 100}}
            source={{uri: item.path}}
            resizeMode={AppConstants.IMAGE_SCALE_TYPE.COVER}
          />
          <TouchableOpacity
            style={{
              width: 20,
              height: 20,
              borderRadius: 10,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => {
              images = DataUtils.removeItem(images, index - 1);
              let data = [
                {
                  type: AppConstants.RETURN_IMAGE_TYPE.ADD_NEW,
                },
                ...images,
              ];
              this.setState({images: data});
            }}>
            <Icon
              name="close"
              type="font-awesome"
              size={20}
              color={ColorStyle.red}
            />
          </TouchableOpacity>
        </TouchableOpacity>
      );
    }
  }
  renderSelectedProduct() {
    return (
      <FlatList
        scrollEventThrottle={1900}
        data={this.state.selectedItems}
        numColumns={5}
        horizontal={false}
        renderItem={({item, index}) => (
          <View style={{flexDirection: 'row'}}>
            <Text style={{paddingRight: 10}}>{item.name}</Text>
            <TouchableOpacity
              onPress={() => {
                this.removeSelectedProduct(index);
              }}
              style={{
                justifyContent: 'center',
                backgroundColor: ColorStyle.red,
                padding: 5,
                borderRadius: 11,
                overflow: 'hidden',
                alignItems: 'center',
              }}>
              <Image source={ImageHelper.home} style={{width: 8, height: 8}} />
            </TouchableOpacity>
          </View>
        )}
        keyExtractor={(item, index) => `requestHiacView${index.toString()}`}
        contentContainerStyle={{padding: 10}}
      />
    );
  }
  removeSelectedProduct(index) {
    let selectedItems = this.state.selectedItems;
    selectedItems = DataUtils.removeItem(selectedItems, index);
    this.setState({selectedItems});
  }
  renderSelectingProductView() {
    const {selectedItems} = this.state;
    return (
      <MultiSelect
        hideTags
        items={this.state.items}
        uniqueKey="order_detail_id"
        ref={component => {
          this.multiSelect = component;
        }}
        onSelectedItemsChange={this.onSelectedItemsChange}
        selectedItems={selectedItems}
        selectText={strings('selectReturnProduct')}
        searchInputPlaceholderText={strings('searchProduct')}
        submitButtonText={strings('done')}
        onChangeInput={text => console.log(text)}
        tagRemoveIconColor="#555555"
        tagBorderColor="#555555"
        tagTextColor="#555555"
        selectedItemTextColor={ColorStyle.red}
        selectedItemIconColor={ColorStyle.red}
        itemTextColor="#000"
        displayKey="name"
        searchInputStyle={styles.searchInputStyle}
        styleInputGroup={styles.styleInputGroup}
        styleItemsContainer={styles.styleItemsContainer}
        styleMainWrapper={styles.styleMainWrapper}
        styleRowList={styles.styleRowList}
        styleDropdownMenu={styles.styleDropdownMenu}
        styleDropdownMenuSubsection={styles.styleDropdownMenuSubsection}
        styleTextDropdownSelected={styles.styleTextDropdownSelected}
        styleTextDropdown={styles.styleTextDropdown}
        // submitButtonColor={ColorStyle.inActiveText}
      />
    );
  }
  renderReason() {
    let reasons = this.state.reasons;
    return (
      <Dropdown
        labelExtractor={item => item.name}
        valueExtractor={item => item.id}
        onChangeText={(v, i, data) => {
          this.setState({reasonId: v});
        }}
        labelFontSize={13}
        fontSize={14}
        data={reasons}
        label={strings('returnReason')}
      />
    );
  }
  expectResult() {
    let expectResults = this.state.expectResults;
    return (
      <Dropdown
        labelExtractor={item => item.name}
        valueExtractor={item => item.id}
        onChangeText={(v, i, data) => {
          this.setState({expectResultId: v});
        }}
        labelFontSize={13}
        fontSize={14}
        data={expectResults}
        label={strings('expectResult')}
      />
    );
  }
  renderRefundMethod() {
    return (
      <Dropdown
        labelExtractor={item => item.name}
        valueExtractor={item => item.id}
        onChangeText={(v, i, data) => {
          this.setState({refundMethodId: v});
        }}

        labelFontSize={13}
        fontSize={14}
        data={AppConstants.REFUND_METHOD}
        label={strings('refundMethod')}
      />
    );
  }
  checkEmail() {
    return this.setState({
      validEmail:
        this.state.email !== undefined && DataUtils.validMail(this.state.email),
    });
  }
  checkPhone() {
    return this.setState({
      validPhone:
        this.state.phone !== undefined &&
        DataUtils.validPhone(this.state.phone),
    });
  }
  renderTextInputWithTitle(
    title,
    stateName,
    validStateName,
    checkId,
    checkFunc,
    keyboradType,
    returnKeyType,
    ref,
    onSubmitEditing,
  ) {
    let mValidStateName = this.state[validStateName];
    return (
      <View>
        <Text>{title}</Text>
        <TextInput
          style={{...Styles.input.codeInput, paddingRight: 30}}
          value={this.state[stateName]}
          onChangeText={text => {
            this.setState({[stateName]: text}, () => {
              if (!DataUtils.stringNullOrEmpty(checkId) && checkFunc != null) {
                clearTimeout(this[checkId]);
                this[checkId] = setTimeout(() => {
                  checkFunc();
                }, 500);
              }
            });
          }}
          ref={ref}
          onSubmitEditing={onSubmitEditing}
          returnKeyType={returnKeyType}
          keyboardType={
            !DataUtils.stringNullOrEmpty(keyboradType)
              ? keyboradType
              : 'ascii-capable'
          }
        />

        {!DataUtils.stringNullOrEmpty(validStateName) && (
          <Icon
            name={mValidStateName ? 'check' : 'alert-circle'}
            type="feather"
            size={15}
            color={mValidStateName ? 'green' : 'red'}
            containerStyle={{position: 'absolute', right: 20, bottom: 12}}
          />
        )}
      </View>
    );
  }
  changePrice(price) {
    if (price === undefined) {
      price = 0;
    }
    this.setState({refund: price, validPrice: !isNaN(price)});
  }
  refundOrder() {
    if (
      this.state.selectedItems.length === undefined ||
      this.state.selectedItems.length === 0
    ) {
      this.showToast(strings('refundProductInvalid'));
      return;
    }
    let productIds = [];
    this.state.selectedItems.forEach(item => {
      productIds.push(item.order_detail_id);
    });
    if (!this.state.validEmail) {
      this.showToast(strings('refundEmailInvalid'));
      return;
    }
    if (!this.state.validPhone) {
      this.showToast(strings('refundPhoneInvalid'));
      return;
    }
    if (this.state.reasonId === undefined) {
      this.showToast(strings('refundReasonInvalid'));
      return;
    }
    if (this.state.expectResultId === undefined) {
      this.showToast(strings('refundResultInvalid'));
      return;
    }
    if (!this.state.validPrice) {
      this.showToast(strings('refundTotal'));
      return;
    }
    if (this.state.refundMethodId === undefined) {
      this.showToast(strings('refundReturnMethodInvalid'));
      return;
    }
    if (!this.state.validNote) {
      this.showToast(strings('refundNoteInvalid'));
      return;
    }
    let params = {
      customer_email: this.state.email,
      customer_phone: this.state.phone,
      refund_reason_id: this.state.reasonId,
      refund_results_id: this.state.expectResultId,
      total_price: Number(this.state.refund),
      customer_note_reason: this.state.note,
      payment_method: this.state.refundMethodId,
      order_detail_ids: productIds,
    };
    RefundMetaUtils.getInstance().refundOrder(
      params,
      (isSuccess, dataResponse) => {
        if (isSuccess && dataResponse.data !== undefined) {
          ViewUtils.showAlertDialog(strings('refundSuccess'), () => {
            let refundId = dataResponse.data.id;
            this.uploadImage(refundId);
            if (this.props.onClose !== undefined) {
              this.props.onClose();
            }
          });
        } else {
          ViewUtils.showAlertDialog(strings('refundError'), undefined);
        }
      },
    );
  }
  showToast(msg) {
    SimpleToast.show(msg, SimpleToast.SHORT);
  }
  onSelectImage(index) {
    if (PermissionUtils.checkPermission(PERMISSIONS.IOS.PHOTO_LIBRARY)) {
      ImagePicker.openPicker({
        cropping: true,
        freeStyleCropEnabled: true,
        compressImageMaxWidth: 800,
        compressImageMaxHeight: 800,
      }).then(image => {
        images = [
          {
            type: AppConstants.RETURN_IMAGE_TYPE.IMAGE,
            path: image.path,
          },
          ...images,
        ];
        let data = [
          {
            type: AppConstants.RETURN_IMAGE_TYPE.ADD_NEW,
          },
          ...images,
        ];
        this.setState({images: data});
      });
    }
  }
  uploadImage(refundId) {
    if (images.length > 0) {
      let params = {
        object_type: AppConstants.IMAGE_UPLOAD_TYPE.REFUND,
        type: 'customer',
        object_id: refundId,
      };
      images.forEach(image => {
        MediaHandle.getInstance().uploadImageFile(
          image.path,
          params,
          process => {},
          () => {},
        );
      });
    }
  }
}
const styles = StyleSheet.create({
  searchInputStyle: {
    color: '#000',
    height: 45,
  },
  styleInputGroup: {
    backgroundColor: 'transparent',
    borderRadius: 10,
    overflow: 'hidden',
  },
  styleItemsContainer: {
    paddingVertical: 10,
    backgroundColor: 'white',
  },
  styleRowList: {
    paddingBottom: 10,
  },
  styleMainWrapper: {
    marginTop: 20,
  },
  styleDropdownMenu: {
    height: 45,
    borderRadius: 8,
    overflow: 'hidden',
  },
  styleTextDropdown: {
    marginHorizontal: 10,
  },
  styleTextDropdownSelected: {
    marginHorizontal: 10,
  },
});
