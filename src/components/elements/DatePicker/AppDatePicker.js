import React, {Component} from 'react';
import DatePicker from 'react-native-date-picker';
import {Dimensions, Text, TouchableOpacity, View} from 'react-native';
import PropTypes from 'prop-types';
import {strings} from '../../../resource/languages/i18n';

const width = Dimensions.get('window').width;
export default class AppDatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: this.props.date,
    };
  }

  render() {
    return (
      <View
        style={{
          backgroundColor: 'rgba(52, 52, 52, 0.4)',
          position: 'absolute',
          top: 0,
          left: 0,
          width: width,
          justifyContent: 'flex-end',
          height: '100%',
        }}>
        <View
          style={{
            width: width,
            backgroundColor: 'white',
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            overflow: 'hidden',
            justifyContent: 'center',
            paddingVertical: 20,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.onFinish(false, null);
              }}>
              <Text
                style={{
                  paddingHorizontal: 10,
                  color: 'red',
                  fontSize: 18,
                  paddingBottom: 15,
                }}>
                {strings('cancel')}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.onFinish(true, this.state.date);
              }}>
              <Text
                style={{
                  paddingHorizontal: 10,
                  fontSize: 18,
                  paddingBottom: 15,
                }}>
                {strings('accept')}
              </Text>
            </TouchableOpacity>
          </View>
          <DatePicker
            style={{width: width}}
            date={this.state.date}
            onDateChange={date => this.setState({date})}
            mode="date"
            locale="vi"
            maximumDate={this.props.maximumDate}
            minimumDate={this.props.minimumDate}
          />
        </View>
      </View>
    );
  }
}
// validate props
AppDatePicker.propTypes = {
  onFinish: PropTypes.func.isRequired,
  maximumDate: PropTypes.instanceOf(Date),
  minimumDate: PropTypes.instanceOf(Date),
};

// set default props
AppDatePicker.defaultProps = {
  maximumDate: null,
  minimumDate: null,
};
