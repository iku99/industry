import React, {useEffect} from "react";
import {Text, View} from "react-native";
import styles from "./styles";
import {TextInputMask} from "react-native-masked-text";
export default function TextInputNumber({value, onChange, placeholder,
                                           title,v}){
    return(
        <View style={styles.container}>
            <Text
                style={styles.title}>
                {title}*
            </Text>
            <View style={{...styles.bodyInput}}>
                <TextInputMask
                    type={'money'}
                    style={styles.textInput}
                    value={value}
                    onChangeText={text => {
                        let formatText=text.replace(',','')
                        onChange(formatText)
                    }}
                    options={{
                        precision: 0,
                        separator: '.',
                        delimiter: ',',
                        unit: '',
                        suffixUnit: '',
                    }}
                />
            </View>
        </View>
    )
}
