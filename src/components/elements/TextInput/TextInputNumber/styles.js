import ColorStyle from "../../../../resource/ColorStyle";
import Styles from "../../../../resource/Styles";
const X=Styles.constants.X
export default {
    container:{
        width:'100%',
        alignSelf: 'center',
        marginTop:X/5
    },
    bodyInput:{
        ...Styles.input.codeInput,
        paddingVertical:0,

        marginTop:10,
        flexDirection:'row'  ,
        overflow:'hidden' ,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        backgroundColor: ColorStyle.tabWhite,
        elevation: 2,
        alignItems:'center',
        paddingLeft: 0,
        justifyContent:'flex-start'
    },
    textInput:{
        ...Styles.input.codeInput,
        paddingVertical: 10,
        paddingLeft: 10,
        marginVertical: 8,
        width:'100%',
    },
    title:{
        ...Styles.text.text15,
        color: ColorStyle.tabBlack,
    }
}
