import ColorStyle from "../../../../resource/ColorStyle";
import Styles from "../../../../resource/Styles";
const X=Styles.constants.X
export default {
    container:{
        alignSelf: 'center',
        marginTop:X/5
    },
    textInput:{
        minHeight: 50,
        flexDirection: 'row',
        width: Styles.constants.widthScreenMg24,
        backgroundColor: '#F5F2FF',
        alignItems: 'center',
        borderRadius: 12,
        color:ColorStyle.tabBlack,
        // textDecoration: 'none',
        paddingHorizontal: 11.47,
        shadowColor: 'rgba(0, 0, 0, 0.25)',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 1,

    },
    textError:{
        ...Styles.text.text14,
        color: ColorStyle.red,
        marginHorizontal:X/4,
        marginTop:X/6
    }
}
