import React, {useEffect} from "react";
import {Text, TextInput, View} from "react-native";
import ColorStyle from "../../../../resource/ColorStyle";
import {Icon} from "react-native-elements";
import styles from "./styles";
export default function TextInputLogin({value,
                                           onChange,
                                           placeholder,
                                           error,
                                           valid,
                                           eye,
                                           mShowPass,
                                           onShow}){


    useEffect(()=>{
        // console.log(mShowPass)
    },[])
    return(
        <View style={styles.container}>
            <TextInput
                style={styles.textInput}
                placeholder={placeholder}
                value={value}
                secureTextEntry={eye?!mShowPass:false}
                placeholderTextColor={ColorStyle.textInput}
                onChangeText={text => {
                    onChange(text)
                }}
            />
            {error !=='' && (
                <Text style={styles.textError}>{error}</Text>
            )}
            {eye && (
                <Icon
                    name={mShowPass ? 'eye' : 'eye-with-line'}
                    type="entypo"
                    onPress={() =>onShow()}
                    size={18}
                    color={ColorStyle.tabBlack}
                    containerStyle={{
                        position: 'absolute',
                        right: 5,
                        alignItems: 'flex-end',
                        bottom: '30%',
                    }}
                />
            )}
        </View>
    )
}
