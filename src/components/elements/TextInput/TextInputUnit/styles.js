import ColorStyle from "../../../../resource/ColorStyle";
import Styles from "../../../../resource/Styles";
const X=Styles.constants.X
export default {
    container:{
        width:'48%',
        alignSelf: 'center',
        marginTop:X/5,
    },
    bodyView:{
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        backgroundColor: ColorStyle.tabWhite,
        marginTop:10,
        borderRadius: 12,
    },
    bodyInput:{
        ...Styles.input.codeInput,
        paddingVertical:0,
        flexDirection:'row'  ,
        overflow:'hidden' ,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        backgroundColor: ColorStyle.tabWhite,
        elevation: 2,
        alignItems:'center',
        paddingLeft: 0,
        justifyContent:'flex-start'
    },
    textInput:{
        ...Styles.input.codeInput,
        paddingVertical: 10,
        paddingLeft: 10,
        marginVertical: 8,
        width:'80%',
    },
    viewUnit:{
        width:'20%',
        backgroundColor:ColorStyle.tabActive,
        alignItems: 'center',
        justifyContent: 'center',
        height: X * 1.1,
    },
    title:{
        ...Styles.text.text15,
        color: ColorStyle.tabBlack,
    },
    unitText:{
        ...Styles.text.text12,
        color: ColorStyle.tabWhite,
    }
}
