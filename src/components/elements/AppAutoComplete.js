import React, {Component} from 'react';
import {Text, TouchableOpacity} from 'react-native';
import Autocomplete from './Autocomplete';
import AppConstants from '../../resource/AppConstants';
import ColorStyle from '../../resource/ColorStyle';
import StringUtils from './StringUtils';
import PropTypes from 'prop-types';
import Styles from '../../resource/Styles';

export default class AppAutoComplete extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataList: this.props.dataList,
      defaultValue:
        this.props.defaultValue !== undefined ? this.props.defaultValue : '',
      item: this.props.item,
      hideResults: true,
      limitResult: true,
      isSelected: true,
    };
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.defaultValue !== prevProps.defaultValue) {
      this.setState({
        defaultValue:
          this.props.defaultValue !== undefined ? this.props.defaultValue : '',
      });
    }
  }

  render() {
    let {defaultValue} = this.state;
    const data = this.findData(defaultValue);
    return (
      <Autocomplete
        data={data}
        alwaysShowOnBottom={this.props.alwaysShowOnBottom}
        alwaysShowOnTop={this.props.alwaysShowOnTop}
        defaultValue={defaultValue}
        placeholder={this.props.placeholder}
        hideResults={this.state.hideResults}
        style={{...Styles.input.codeInput, marginVertical: 7}}
        containerStyle={{
          zIndex: 1000,
        }}
        onChangeText={text => {
          this.setState({hideResults: false, defaultValue: text});
        }}
        onFocus={() => {
          this.setState({hideResults: false, isSelected: false});
          this.findData(defaultValue);
          if (this.props.onFocus !== undefined) {
            this.props.onFocus();
          }
        }}
        renderItem={({item}) => (
          <TouchableOpacity
            style={{
              paddingHorizontal: 10,
              paddingVertical: 10,
              backgroundColor: ColorStyle.tabWhite,
            }}
            onPress={() => this.changeItem(item)}>
            <Text style={{...Styles.text.text12, color: ColorStyle.tabBlack}}>
              {item.name}
            </Text>
          </TouchableOpacity>
        )}
      />
    );
  }
  findData(query) {
    const {dataList} = this.state;
    if (query === '') {
      if (!this.props.showWhenFocus) {
        return [];
      } else {
        return dataList;
      }
    }

    query = StringUtils.removeAscent(query);
    const regex = new RegExp(`${query.trim()}`, 'i');
    let result = [];
    for (let i = 0; i < dataList.length; i++) {
      let item = dataList[i];
      let name = StringUtils.removeAscent(item.name);
      if (name.search(regex) >= 0) {
        result.push(item);
      }
      if (this.state.limitResult && result.length > 5) {
        break;
      }
    }
    return result;
  }

  changeItem(item) {
    this.setState(
      {hideResults: true, item, defaultValue: item.name, isSelected: true},
      () => {
        if (this.props.onItemChange != null) {
          this.props.onItemChange(item);
        }
      },
    );
  }
  isSelected() {
    return this.state.isSelected;
  }
}

AppAutoComplete.defaultProps = {
  dataList: [],
  showWhenFocus: false,
  defaultValue: '',
};

AppAutoComplete.propTypes = {
  dataList: PropTypes.array.required,
  onItemChange: PropTypes.function,
  onFocus: PropTypes.function,
  alwaysShowOnTop: PropTypes.boolean,
  alwaysShowOnBottom: PropTypes.boolean,
  showWhenFocus: PropTypes.boolean,
  defaultValue: PropTypes.string,
};
