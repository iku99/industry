import React, {Component} from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {View} from 'react-native';
import Styles from '../../../resource/Styles';
export default class BannerLoading extends Component {
  render() {
    return (
      <SkeletonPlaceholder>
        <View
          style={{
            width: Styles.constants.widthScreenMg24,
            height: (Styles.constants.widthScreen * 2) / 3,
            marginTop:10,
            borderRadius:20,
            alignSelf: 'center',
          }}
        />
      </SkeletonPlaceholder>
    );
  }
}
