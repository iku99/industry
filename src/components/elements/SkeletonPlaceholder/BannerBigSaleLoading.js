import React, {Component} from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {View} from 'react-native';
import Styles from '../../../resource/Styles';
export default class BannerBigSaleLoading extends Component {
  render() {
    return (
      <SkeletonPlaceholder>
        <View
          style={{
            width: Styles.constants.widthScreen - 32,
            height: (Styles.constants.heightScreen * 2) / 4,
            alignSelf: 'center',
            marginTop: Styles.constants.marginTopAll,
          }}
        />
      </SkeletonPlaceholder>
    );
  }
}
