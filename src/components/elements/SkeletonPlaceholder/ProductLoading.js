import React, {Component} from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {FlatList, View} from 'react-native';
import Styles from '../../../resource/Styles';
import CategoryStyle from '../viewItem/CategoryItem/CategoryStyle';
import ColorStyle from '../../../resource/ColorStyle';
const X = Styles.constants.X;
export default class ProductLoading extends Component {
  render() {
    return (
      <SkeletonPlaceholder>
        <View style={Styles.containerItemHome}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              width: Styles.constants.widthScreenMg24,
            }}>
            <View
              style={{marginTop: 12, width: 155, height: 200, borderRadius: 4}}
            />
            <View
              style={{marginTop: 12, width: 155, height: 200, borderRadius: 4}}
            />
          </View>
        </View>
      </SkeletonPlaceholder>
    );
  }
}
