import React, {Component} from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {FlatList, View} from 'react-native';
import Styles from '../../../resource/Styles';
import CategoryStyle from '../viewItem/CategoryItem/CategoryStyle';
const Data = [
  {index: 1},
  {index: 1},
  {index: 1},
  {index: 1},
  {index: 1},
  {index: 1},
  {index: 1},
  {index: 1},
];

export default class CategoryLoading extends Component {
  render() {
    return (
      <FlatList
        data={Data}
        numColumns={4}
        renderItem={this.renderItem}
        style={{
          width: Styles.constants.widthScreenMg24,
          height: (Styles.constants.widthScreen * 2) / 3,
          marginHorizontal: Styles.constants.marginHorizontalAll,
          marginTop: Styles.constants.marginTopAll,
            alignItems: 'center',
        }}
      />
    );
  }
  renderItem = ({item}) => {
    return (
      <SkeletonPlaceholder>
        <View
          style={{
            flexDirection: 'column',
            alignItems: 'center',
            padding: 5,
            paddingHorizontal: 10,
          }}>
          <View style={[CategoryStyle.icon, {borderRadius: 30}]} />
          <View
            style={{marginTop: 12, width: 60, height: 15, borderRadius: 4}}
          />
        </View>
      </SkeletonPlaceholder>
    );
  };
}
