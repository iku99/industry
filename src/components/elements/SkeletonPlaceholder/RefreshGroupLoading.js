import React, {Component} from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {View} from 'react-native';
import Styles from '../../../resource/Styles';
import ColorStyle from '../../../resource/ColorStyle';
const X = Styles.constants.X;
export default class RefreshGroupLoading extends Component {
  render() {
    return (
      <SkeletonPlaceholder>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            width: Styles.constants.widthScreenMg24,
            height: X * 1.2,
            marginVertical: 10,
          }}
        />
        <View
          style={{
            ...Styles.border.borderItemHome,
            width: Styles.constants.widthScreenMg24,
            height: X,
            alignItems: 'flex-start',
            backgroundColor: ColorStyle.tabWhite,
            paddingHorizontal: 20,
          }}
        />
        <View
          style={{
            ...Styles.border.borderItemHome,
            width: Styles.constants.widthScreenMg24,
            height: X,
            alignItems: 'flex-start',
            backgroundColor: ColorStyle.tabWhite,
            paddingHorizontal: 20,
            marginVertical: 10,
          }}
        />
        <View
          style={{
            ...Styles.border.borderItemHome,
            width: Styles.constants.widthScreenMg24,
            height: X,
            alignItems: 'flex-start',
            marginVertical: 10,
            backgroundColor: ColorStyle.tabWhite,
            paddingHorizontal: 20,
          }}
        />
      </SkeletonPlaceholder>
    );
  }
}
