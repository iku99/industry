import React, {Component} from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {FlatList, View} from 'react-native';
import Styles from '../../../resource/Styles';
import CategoryStyle from '../viewItem/CategoryItem/CategoryStyle';
import ColorStyle from '../../../resource/ColorStyle';
const X = Styles.constants.X;
export default class ViewStoreLoading extends Component {
  render() {
    return (
      <SkeletonPlaceholder>
        <View
          style={{
            ...Styles.border.borderItemHome,
            width: Styles.constants.widthScreen,
            height: X * 3.4,
            alignItems: 'flex-start',
            backgroundColor: ColorStyle.tabWhite,
            paddingHorizontal: 20,
          }}
        />
      </SkeletonPlaceholder>
    );
  }
}
