import React, {Component} from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import {strings} from '../../resource/languages/i18n';
import {Icon} from 'react-native-elements';
import PropTypes from 'prop-types';
import Styles from '../../resource/Styles';
import ColorStyle from "../../resource/ColorStyle";

export default class QuantityControl extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: this.props.quantity,
    };
  }
  render() {
    return (
      <View style={this.props.containerStyle}>
        {this.props.showTitle && (
          <Text style={{...Styles.text.text16,color:ColorStyle.tabBlack}}>{strings('quantity')}</Text>
        )}
        <View
          style={[
            {
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            },
          ]}>
          <TouchableOpacity
            style={{paddingHorizontal: 2,borderColor:ColorStyle.tabBlack,borderRadius:5,borderWidth:1,...this.props.btnQuantity}}
            onPress={() => {
              this.subtractQuantity();
            }}>
            <Icon name="ios-remove" type="ionicon" size={20} color={ColorStyle.tabBlack} />
          </TouchableOpacity>
          <TextInput
            keyboardType="numeric"
            style={{
              ...Styles.text.text14,
              marginHorizontal: 10,
              color: ColorStyle.tabBlack,
              height: 35,
              alignItems: 'center',
              ...this.props.styleText,
            }}
            textAlign={'center'}
            value={`${this.state.quantity}`}
            onBlur={() => {
              if (this.state.quantity === '') {
                this.changeValue(0);
              }
            }}
            onChangeText={quantity => {
              this.changeValue(quantity);
            }}
          />
          <TouchableOpacity
            style={{paddingHorizontal: 2,borderColor:ColorStyle.tabBlack,borderRadius:5,borderWidth:1,...this.props.btnQuantity,}}
            onPress={() => {
              this.addQuantity();
            }}>
            <Icon name="ios-add" type="ionicon" size={20} color={ColorStyle.tabBlack} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  subtractQuantity() {
    let quantity = this.state.quantity;
    if (quantity > this.props.minValue) {
      quantity = quantity - 1;
      this.changeValue(quantity);
    }
  }
  addQuantity() {
    let quantity = this.state.quantity;
    if (quantity < this.props.maxValue - 1) {
      quantity = quantity + 1;
      this.changeValue(quantity);
    }
  }
  changeValue(quantity) {
    this.setState({quantity});
    this.props.changeValue(quantity);
  }
  updateQuantity(quantity) {
    this.setState({quantity});
  }
}
QuantityControl.defaultProps = {
  quantity: 1,
  minValue: 0,
  maxValue: 100,
  showTitle: false,
  containerStyle: {},
};
//
// QuantityControl.propTypes = {
//   quantity: PropTypes.number,
//   minValue: PropTypes.number.required,
//   maxValue: PropTypes.number.isRequired,
//   changeValue: PropTypes.func.isRequired,
//   showTitle: PropTypes.boolean,
//   containerStyle: PropTypes.style,
// };
