import React, {Component} from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../resource/Styles';
import ColorStyle from '../../resource/ColorStyle';
let selectedIndex = 0;
export default class AppTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data,
    };
  }
  //index === selectedIndex
  //           ? MessageStyle.tabSelected
  //           : MessageStyle.tabNormal
  componentDidMount() {}
  keyExtractor = (item, index) => `appTab_${index.toString()}`;
  _renderItem = ({item, index}) => (
    <TouchableOpacity
      style={{
        height: Styles.constants.X * 1.3,
        backgroundColor: ColorStyle.tabWhite,
        alignItems: 'center',
        justifyContent: 'center',
        width: this.props.widthTab
          ? Styles.constants.widthScreen / 2
          : undefined,
        paddingHorizontal: 15,
        borderColor: ColorStyle.tabActive,
        borderBottomWidth: index === selectedIndex ? 1 : 0,
      }}
      onPress={() => {
        if (this.props.onPress != null) {
          this.props.onPress(index);
        }
      }}>
      <Text
        style={{
          color:
            index === selectedIndex
              ? ColorStyle.tabActive
              : ColorStyle.optionTextColor,
          fontWeight: index === selectedIndex? '700':'500',
          textAlign: 'center',
        }}>
        {item.title}
      </Text>
    </TouchableOpacity>
  );
  render() {
    selectedIndex = this.props.index;
    return (
      <View>
        <FlatList
          ref={ref => {
            this.flatListRef = ref;
          }}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={this.state.data}
          keyExtractor={this.keyExtractor}
          scroll
          renderItem={this._renderItem}
          style={{width: '100%', backgroundColor: 'transform'}}
        />
      </View>
    );
  }
  onTabChanged(i) {
    this.flatListRef.scrollToIndex({
      animated: true,
      index: i === 0 ? i : i - 1,
    });
  }
}
