import React, {Component} from 'react';
import FastImage from 'react-native-fast-image';
import AppConstants from '../../resource/AppConstants';

let width = 0;
export default class AutoHeightImage extends Component {
  constructor(props) {
    super(props);
    width = this.props.width;
    this.state = {
      height: 0,
    };
  }
  render() {
    return (
      <FastImage
        {...this.props}
        style={{width: width, height: this.state.height,}}
        onLoad={e => {
          let height = (e.nativeEvent.height * width) / e.nativeEvent.width;
          this.setState({height});
        }}
        resizeMode={AppConstants.IMAGE_SCALE_TYPE.CONTAIN}
      />
    );
  }
}
