import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  FlatList,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View,
  ViewPropTypes as RNViewPropTypes,
} from 'react-native';
import ColorStyle from "../../../resource/ColorStyle";

const ViewPropTypes = RNViewPropTypes || View.propTypes;

class Autocomplete extends Component {
  static propTypes = {
    ...TextInput.propTypes,
    /**
     * These styles will be applied to the container which
     * surrounds the autocomplete component.
     */
    containerStyle: ViewPropTypes.style,
    /**
     * Assign an array of data objects which should be
     * rendered in respect to the entered text.
     */
    data: PropTypes.array,
    /**
     * Set to `true` to hide the suggestion list.
     */
    hideResults: PropTypes.bool,
    /*
     * These styles will be applied to the container which surrounds
     * the textInput component.
     */
    inputContainerStyle: ViewPropTypes.style,
    /*
     * Set `keyboardShouldPersistTaps` to true if RN version is <= 0.39.
     */
    keyboardShouldPersistTaps: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.bool,
    ]),
    /*
     * These styles will be applied to the container which surrounds
     * the result list.
     */
    listContainerStyle: ViewPropTypes.style,
    /**
     * These style will be applied to the result list.
     */
    listStyle: ViewPropTypes.style,
    /**
     * `onShowResults` will be called when list is going to
     * show/hide results.
     */
    onShowResults: PropTypes.func,
    /**
     * method for intercepting swipe on ListView. Used for ScrollView support on Android
     */
    onStartShouldSetResponderCapture: PropTypes.func,
    /**
     * `renderItem` will be called to render the data objects
     * which will be displayed in the result view below the
     * text input.
     */
    renderItem: PropTypes.func,
    keyExtractor: PropTypes.func,
    /**
     * `renderSeparator` will be called to render the list separators
     * which will be displayed between the list elements in the result view
     * below the text input.
     */
    renderSeparator: PropTypes.func,
    /**
     * renders custom TextInput. All props passed to this function.
     */
    renderTextInput: PropTypes.func,
    flatListProps: PropTypes.object,
  };

  static defaultProps = {
    data: [],
    defaultValue: '',
    placeholder:'',
    keyboardShouldPersistTaps: 'always',
    onStartShouldSetResponderCapture: () => false,
    renderItem: ({item}) => <Text style={{padding: 10}}>{item}</Text>,
    renderSeparator: null,
    flatListProps: {},
  };

  renderTextInput(props) {
    return (
      <TextInput
        {...props}
        placeholder={this.props.placeholder}
        placeholderTextColor={ColorStyle.gray}
        onTouchStart={e => {
          let shouldAtTop = true;
          if (this.props.alwaysShowOnTop) {
            shouldAtTop = true;
          } else if (this.props.alwaysShowOnBottom) {
            shouldAtTop = false;
          } else {
            shouldAtTop = e.nativeEvent.pageY > 350;
          }
          this.setState({shouldAtTop});
        }}
      />
    );
  }

  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      shouldAtTop: true,
    };
    this.resultList = null;
    this.textInput = null;

    this.onRefListView = this.onRefListView.bind(this);
    this.onRefTextInput = this.onRefTextInput.bind(this);
    this.onEndEditing = this.onEndEditing.bind(this);
  }

  componentWillReceiveProps({data}) {
    this.setState({data});
  }

  onEndEditing(e) {
    this.props.onEndEditing && this.props.onEndEditing(e);
  }

  onRefListView(resultList) {
    this.resultList = resultList;
  }

  onRefTextInput(textInput) {
    this.textInput = textInput;
  }

  /**
   * Proxy `blur()` to autocomplete's text input.
   */
  blur() {
    const {textInput} = this;
    textInput && textInput.blur();
  }

  /**
   * Proxy `focus()` to autocomplete's text input.
   */
  focus() {
    const {textInput} = this;
    textInput && textInput.focus();
  }

  /**
   * Proxy `isFocused()` to autocomplete's text input.
   */
  isFocused() {
    const {textInput} = this;
    return textInput && textInput.isFocused();
  }

  renderResultList() {
    const {data} = this.state;
    const {
      listStyle,
      renderItem,
      keyExtractor,
      renderSeparator,
      keyboardShouldPersistTaps,
      flatListProps,
      onEndReached,
      onEndReachedThreshold,
      alwaysShowOnTop,
      alwaysShowOnBottom,
    } = this.props;

    return (
      <FlatList
        ref={this.onRefListView}
        data={data}
        keyboardShouldPersistTaps={keyboardShouldPersistTaps}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        renderSeparator={renderSeparator}
        onEndReached={onEndReached}
        onEndReachedThreshold={onEndReachedThreshold}
        // style={[styles.list, listStyle,this.getListPosition()]}
        {...flatListProps}
      />
    );
  }

  getListPosition() {
    if (this.state.shouldAtTop) {
      return {
        zIndex: 1,
        bottom: 0,
        left: 0,
        position: 'absolute',
        right: 0,
        borderBottomWidth: 0,
        borderTopWidth: 1,
      };
    } else {
      return {
        zIndex: 1,
        borderTopWidth: 0,
        borderBottomWidth: 1,
        top: 0,
        left: 0,
        position: 'absolute',
        right: 0,
      };
    }
  }
  _renderTextInput() {
    const {renderTextInput, style} = this.props;
    const props = {
      style: [styles.input, style],
      ref: this.onRefTextInput,
      onEndEditing: this.onEndEditing,
      ...this.props,
    };

    return this.renderTextInput(props);
  }

  render() {
    const {data} = this.state;
    const {
      containerStyle,
      hideResults,
      inputContainerStyle,
      listContainerStyle,
      onShowResults,
      onStartShouldSetResponderCapture,
    } = this.props;
    const showResults = data.length > 0;

    // Notify listener if the suggestion will be shown.
    onShowResults && onShowResults(showResults);

    return (
      <View style={[styles.container, containerStyle]}>
        {this._renderTextInput()}
        {!hideResults && this.renderResultList()}
        {/*{this.state.shouldAtTop && !hideResults && (*/}
        {/*    <View*/}
        {/*        style={listContainerStyle}*/}
        {/*        onStartShouldSetResponderCapture={onStartShouldSetResponderCapture}>*/}
        {/*      {showResults && this.renderResultList()}*/}
        {/*    </View>*/}
        {/*)}*/}
        {/*<View style={[styles.inputContainer, inputContainerStyle]}>*/}
        {/*  {this._renderTextInput()}*/}
        {/*</View>*/}

        {/*{!this.state.shouldAtTop && !hideResults && (*/}
        {/*    <View*/}
        {/*        style={listContainerStyle}*/}
        {/*        onStartShouldSetResponderCapture={onStartShouldSetResponderCapture}>*/}
        {/*      { showResults  && this.renderResultList()}*/}
        {/*    </View>*/}
        {/*)}*/}
      </View>
    );
  }
}

const border = {
  borderColor: 'transparent',
  // borderRadius: 5,
  // borderWidth: 1
};

const listBorder = {
  borderColor: 'transparent',
  // borderRadius: 5,
  // borderWidth: 1,
};

const androidStyles = {
  container: {
    flex: 1,
    // zIndex: 1,
  },
  inputContainer: {
    ...border,
    marginBottom: 0,
  },
  list: {
    ...listBorder,
    backgroundColor: '#dbdbdb',
    padding: 1,
  },
};

const iosStyles = {
  container: {
    zIndex: 1,
  },
  inputContainer: {
    ...border,
  },
  input: {
    backgroundColor: 'white',
    height: 40,
    paddingLeft: 3,
  },
  list: {
    ...listBorder,
    backgroundColor: '#dbdbdb',
    padding: 1,
  },
};

const styles = StyleSheet.create({
  input: {
    backgroundColor: 'white',
    height: 40,
    paddingLeft: 3,
  },
  ...Platform.select({
    android: {...androidStyles},
    ios: {...iosStyles},
  }),
});

export default Autocomplete;
