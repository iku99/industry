import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../resource/Styles';
import ColorStyle from '../../resource/ColorStyle';
import {strings} from '../../resource/languages/i18n';
import { Actions } from "react-native-router-flux";
export default class CheckLogin extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    if (!this.props.status) {
      return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity
            onPress={()=>Actions.jump('login')}
            style={[
              Styles.button.buttonLogin,
              {
                backgroundColor: ColorStyle.tabActive,
                borderColor: 'rgba(250, 111, 52, 0.5)',
                borderWidth: 1,
                marginBottom: 20,
                elevation:2
              },
            ]}>
            <Text style={[Styles.text.text18, {fontWeight: '500'}]}>
              {strings('signIn')}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={()=>Actions.jump('register')}
            style={[
              Styles.button.buttonLogin,
              {
                backgroundColor: ColorStyle.tabWhite,
                borderColor: ColorStyle.outStock,
                borderWidth: 1,
                elevation:2
              },
            ]}>
            <Text
              style={[
                Styles.text.text18,
                {fontWeight: '500', color: ColorStyle.tabBlack},
              ]}>
              {strings('signUp')}
            </Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return this.props.views;
    }
  }
}
