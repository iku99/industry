import React, {Component} from 'react';
import Dialog, {
  DialogContent,
  DialogFooter,
  DialogTitle,
  SlideAnimation,
} from 'react-native-popup-dialog';
import {Text, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import { strings } from "../../resource/languages/i18n";
export default class ConfirmDialog extends Component {
  footerView() {
    return (
      <DialogFooter
        style={{
          height: 42,
          justifyContent: 'space-around',
          borderTopWidth: 2,
          borderColor: '#fbfbfb',
        }}>
        <TouchableOpacity
          activeScale={0.8}
          onPress={this.props.okFunc}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            padding: 10,
            height: '100%',
            width: '100%',
          }}>
          <Text
            style={{
              color: 'black',
              fontSize: 18,
              paddingHorizontal: 10,
              fontWeight: 'bold',
            }}>
            {this.props.okTitle}
          </Text>
        </TouchableOpacity>
      </DialogFooter>
    );
  }

  render() {
    return (
      <Dialog
        width={0.8}
        visible={this.props.isShowing}
        onTouchOutside={() => {
          this.setState({showDialog: false});
        }}
        dialogAnimation={
          new SlideAnimation({
            slideFrom: 'bottom',
          })
        }
        dialogTitle={<DialogTitle title={this.props.titleString} />}
        footer={this.footerView()}>
        <DialogContent style={{justifyContent: 'center', alignItems: 'center'}}>
          {this.props.bodyContent}
        </DialogContent>
      </Dialog>
    );
  }
}
ConfirmDialog.propTypes = {
  titleString: PropTypes.string.isRequired,
  okTitle: PropTypes.string,
  okFunc: PropTypes.func.isRequired,
  isShowing: PropTypes.bool.isRequired,
  bodyContent: PropTypes.object.isRequired,
};
ConfirmDialog.defaultProps = {
  titleString: strings('notification'),
  okTitle: strings('close'),
  cancelTitle: strings('cancel'),
  isShowing: false,
};
