import React, {Component} from 'react';
import {Text, TouchableOpacity} from 'react-native';
import CartUtils from '../../Utils/CartUtils';
import FuncUtils from '../../Utils/FuncUtils';
import NavigationUtils from '../../Utils/NavigationUtils';
import ColorStyle from '../../resource/ColorStyle';
import CartHandle from '../../sagas/CartHandle';
import Styles from '../../resource/Styles';
import AppConstants from '../../resource/AppConstants';
import {EventRegister} from 'react-native-event-listeners';
import {Actions} from "react-native-router-flux";

export default class BadgeView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalCart: 0,
      isLogged: false,
    };
  }
  componentDidMount() {
    EventRegister.addEventListener(AppConstants.EventName.LIST_CART, total => {
      this.setState({totalCart:this.state.totalCart+1})
      // console.log('update',Actions.currentScene)
      // this.getCart()
    });
    FuncUtils.getInstance().callOptionLogin(() =>
      this.getCart()
    );
    // EventRegister.addEventListener(AppConstants.EventName.LOGIN, isLogin => {
    //   if (isLogin) {
    //     CartHandle.getInstance().getCart(null);
    //   }
    //   this.setState({isLogged: isLogin === '1'});
    // });
  }
  getCart() {
    CartHandle.getInstance().getCart((isSuccess, dataResponse) => {
      if (isSuccess) {
        let productList = dataResponse.data;
        if (productList?.store.length !== 0) {
          this.setState({totalCart:productList.total})
          return
        }
        this.setState({totalCart:0})
      }
    });
  }
  render() {
    let cartTotal = this.state.totalCart;
    let text = `${cartTotal}`;
    let badgeSize = 15;
    if (cartTotal === 0) {
      return null;
    } else if (cartTotal < 10) {
      badgeSize = 20;
    } else if (cartTotal < 100) {
      badgeSize = 24;
    } else {
      text = '99+';
      badgeSize = 24;
    }
    let styleSize = {
      borderRadius: badgeSize / 2,
      width: badgeSize,
      height: 14,
    };

    return (
      <TouchableOpacity
        style={{
          position: 'absolute',
          top: -5,
          right: -10,
          display: !this.state.isLogged ? 'flex' : 'none',
          borderRadius: 10,
          width: badgeSize,
          backgroundColor: ColorStyle.tabBlack,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Text style={{...Styles.text.text10, color: ColorStyle.tabWhite}}>
          {text}
        </Text>
      </TouchableOpacity>
    );
  }
}
