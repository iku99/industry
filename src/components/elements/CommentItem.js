import React, {Component} from 'react';
import {Image, Text, View} from 'react-native';
import constants from '../../Api/constants';
import Ripple from 'react-native-material-ripple';
import AccountHandle from '../../sagas/AccountHandle';
import Styles from '../../resource/Styles';
import ColorStyle from '../../resource/ColorStyle';
import MediaUtils from "../../Utils/MediaUtils";
import MyFastImage from "./MyFastImage";
import CurrencyFormatter from "../../Utils/CurrencyFormatter";
import DateUtil from "../../Utils/DateUtil";
import {Icon} from "react-native-elements";
import GlobalInfo from "../../Utils/Common/GlobalInfo";

export default class CommentItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: this.props.item,
    };
  }
  componentDidMount() {
    let item = this.state.item;
  }

  render() {
    let item = this.state.item;
      console.log("item:",item.customer?.avatar)
    return (
      <View
            style={{
                flexDirection: 'row',
                paddingHorizontal: 30,
                paddingVertical: 5,
                alignItems:'flex-start',
                justifyContent:'flex-start'
            }}>
            <Image
                source={MediaUtils.getAvatarUrl(item)}
                style={{...Styles.icon.iconShow, borderRadius: 50}}
            />
            <Ripple
                style={{
                    width:'90%',
                    marginLeft:10,
                    backgroundColor:ColorStyle.tabWhite,
                    borderRadius:5,
                    elevation:3,
                    paddingVertical:10,
                    shadowColor: 'rgba(0, 0, 0, 0.25)',
                    shadowOffset: {width: 0, height: 1},
                    shadowOpacity: 0.8,
                    shadowRadius: 1,
                }}
                onPress={() => {
                    if (this.props.onOptionClick !== undefined) {
                        this.props.onOptionClick();
                    }
                }}>
                <Text
                    style={{
                        ...Styles.text.text14,
                        color: ColorStyle.tabBlack,
                        fontWeight: '700',
                        marginLeft:10
                    }}>
                    {this.getName(item)}
                </Text>
                <Text style={{...Styles.text.text14, marginLeft: 20,marginTop:10,marginRight:5}}>
                    {item.content}
                </Text>
                <Icon name={'setting'} type={'antdesign'} size={20} color={ColorStyle.tabActive} containerStyle={{position:'absolute',right:0,bottom:0,display:item.customer_id === GlobalInfo.userInfo.id?'flex':'none'}}/>

            </Ripple>
        </View>

    );
  }
  getName(item){
      if(item.customer !==null ){
          return item.customer.full_name
      }
      return 'no name'
  }
}
