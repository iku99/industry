import {EventRegister} from 'react-native-event-listeners';
import {Text, TouchableOpacity, View} from 'react-native';
import React, {Component} from 'react';
import {strings} from '../../resource/languages/i18n';
import AppConstants from '../../resource/AppConstants';
import ColorStyle from '../../resource/ColorStyle';
import {Icon} from 'react-native-elements';
import SimpleToast from "react-native-simple-toast";
import {Actions} from "react-native-router-flux";
import ViewUtils from "../../Utils/ViewUtils";
import AppleAuth from "../../sagas/auth/AppleAuth";
import GoogleAuth from "../../sagas/auth/GoogleAuthen";
import FacebookAuth from "../../sagas/auth/FacebookAuth";
export default class OtherLogin extends Component {
    componentDidMount() {
        // GoogleAuth.getInstance().signOut()

    }

    render() {
    return (
      <View>
        <Text style={{color: ColorStyle.textOr, textAlign: 'center'}}>
          {strings('orLoginBy')}
        </Text>
        <View
          style={{
            marginVertical: 20,
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'center',
          }}>
          {AppConstants.IS_IOS &&
            this.renderSocialButton(
              'apple1',
              'antdesign',
              'black',
              () => {},
              // this.onAppleButtonPress(),
            )}
          {this.renderSocialButton(
            'google',
            'antdesign',
            ColorStyle.tabActive,
            () => {
                this._signIn()
            },
          )}
          {this.renderSocialButton(
            'facebook-with-circle',
            'entypo',
            'black',
            () => {
                this.props.loading(true)
              FacebookAuth.getInstance().loginFacebook(isSuccess => {

                this.handleLogin(isSuccess);
              });
            },
          )}
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <Text style={{fontSize:14,color:ColorStyle.tabBlack}}>{this.props.title}</Text>
          <TouchableOpacity
            onPress={() => {
              this.props.callback();
            }}>
            <Text
              style={{
                paddingLeft: 10,
                fontWeight: 'bold',
                color: ColorStyle.tabActive,
              }}>
              {this.props.titleBT}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  renderSocialButton(icon, type, color, mOnPress) {
    return (
      <TouchableOpacity
        style={{
          width: 50,
          height: 50,
          alignItems: 'center',
          justifyContent: 'center',
          borderColor: ColorStyle.borderButton,
          borderWidth: 1,
          borderRadius: 10,
        }}
        onPress={mOnPress}>
        <View animation="flipInY">
          <Icon name={icon} type={type} color={color} size={18} />
        </View>
      </TouchableOpacity>
    );
  }
  _signIn = async () => {
        this.props.loading(true)
    GoogleAuth.getInstance().signInToApp(isSuccess => {

    this.handleLogin(isSuccess);
    }, true);
  };
  handleLogin(isSuccess) {
    if (isSuccess) {
        this.props.loading(false)
      EventRegister.emit(AppConstants.EventName.LOGIN, true);
      Actions.reset('flashscreen');
    } else {
      ViewUtils.showAlertDialog(strings('loginInfoIncorrect'), undefined);
        this.props.loading(false)
    }
  }
  onAppleButtonPress() {
    AppleAuth.getInstance().login(isSuccess => {
      this.handleLogin(isSuccess);
    });
  }
}
