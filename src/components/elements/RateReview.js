import React,{Component} from "react";
import { Text, TouchableOpacity, View ,StyleSheet} from "react-native";
import RatingStar from "../RatingStar";
import { strings } from "../../resource/languages/i18n";
import ColorStyle from "../../resource/ColorStyle";
const maxWidthStar = 114;
export default class RateReview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ratingAvg: this.props.ratingAvg,
      ratingCount: this.props.ratingCount,
      showRatingBT: this.props.showRatingBT,
      statsCount: this.props.statsCount,
      selectRow:0
    };
  }
  shouldComponentUpdate(nextProps, nextState, nextContext) {
    let shouldUpdate = false;
    if (nextProps.ratingAvg !== this.state.ratingAvg) {
      this.setState({
        ratingAvg: nextProps.ratingAvg,
      });
      shouldUpdate = true;
    }
    if (nextProps.showRatingBT !== this.state.showRatingBT) {
      this.setState({showRatingBT: nextProps.showRatingBT});
      shouldUpdate = true;
    }
    if (nextProps.ratingCount !== this.state.ratingCount) {
      this.setState({ratingCount: nextProps.ratingCount});
      shouldUpdate = true;
    }
    if (nextProps.statsCount !== this.state.statsCount) {
      this.setState({statsCount: nextProps.statsCount});
      shouldUpdate = true;
    }
    return shouldUpdate;
  }


  render(){
    let statsCount = this.state.statsCount;
    let stats = [
      statsCount['5'] !== undefined ? statsCount['5'] : 0,
      statsCount['4'] !== undefined ? statsCount['4'] : 0,
      statsCount['3'] !== undefined ? statsCount['3'] : 0,
      statsCount['2'] !== undefined ? statsCount['2'] : 0,
      statsCount['1'] !== undefined ? statsCount['1'] : 0,
    ];
    let maxRate = 1;
    stats.forEach(starCount => {
      if (starCount > maxRate) {
        maxRate = starCount;
      }
    });
    let starView = stats.map((star, index) => {
      let currentStar = 5 - Number(index);
      return (
        <TouchableOpacity onPress={()=>{
          this.setState({selectRow:index},()=>{this.props.callback(currentStar)})}}
          key={`rate_review_${index.toString()}`} style={{...styles.rateRow,borderColor:this.state.selectRow===index?ColorStyle.tabActive:ColorStyle.transparent,borderWidth:this.state.selectRow===index?1:0}}>
          <RatingStar
            size={13}
            rating={currentStar}
            style={styles.star}
            hideUnStar={true}
            rightToLeft={true}
          />
          <View style={{width: maxWidthStar, marginStart: 11, marginEnd: 22}}>
            <View
              style={{
                ...styles.rateProgress,
                width: (star * maxWidthStar) / maxRate,
                minWidth: 1,
              }}
            />
          </View>
          <Text style={styles.textRateCount}>{star}</Text>
        </TouchableOpacity>
      );
    });
    return (
      <View style={{width: '100%', backgroundColor: ColorStyle.white}}>
        <View style={{alignItems: 'center'}}>
          <View
            style={{
              flexDirection: 'row',
              marginVertical: 16,
              justifyContent: 'center',
            }}>
            <View style={{alignItems: 'center'}}>
              <Text
                style={{
color:ColorStyle.tabBlack,
                  fontWeight: 'bold',
                  fontSize: 34,
                  fontStyle: 'normal',
                }}>
                {this.state.ratingAvg}
              </Text>
              <Text
                style={{
                  color: '#9B9B9B',
                  fontSize: 14,
                  marginTop: 8,
                }}>{`${this.state.ratingCount} đánh giá`}</Text>
            </View>
            <View style={{marginStart: 20}}>{starView}</View>
          </View>

          {this.state.showRatingBT && (
            <TouchableOpacity
              style={styles.postRateBT}
              onPress={this.props.rateNow}>
              <Text style={styles.postRateText}>
                {strings('commentAboutProduct')}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  textRateCount: {
    color: '#9B9B9B',
    fontSize: 12,
    width: 30,
  },
  rateRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 8,
    paddingVertical:2
  },
  rateProgress: {
    backgroundColor: '#DC0C1F',
    borderRadius: 4,
    overflow: 'hidden',
    height: 8,
  },
  star: {
    width: 80,
  },
  postRateText: {
    color: ColorStyle.tabBlack,
  },
  postRateBT: {
    borderRadius: 20,
    paddingVertical: 10,
    paddingHorizontal: 8,
    width: 300,
    borderWidth: 1,
    borderColor: ColorStyle.tabBlack,
    backgroundColor: ColorStyle.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
