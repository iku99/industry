import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const ImageItem = props => {
  return props.image ? (
    <TouchableOpacity
      style={__styles.image_view}
      onPress={() => props.onPress(props.image, props.index)}>
      <Image
        style={__styles.image}
        resizeMode="cover"
        source={{
          uri: props.image,
        }}
      />
    </TouchableOpacity>
  ) : (
    <View />
  );
};

const TwoImages = props => {
  return (
    <>
      <ImageItem image={props.images[0]} onPress={props.onPress} index={0} />
      <ImageItem image={props.images[1]} onPress={props.onPress} index={1} />
    </>
  );
};

const renderImages = (start, overflow, images, onPress) => {
  return (
    <>
      <ImageItem image={images[start]} onPress={onPress} index={start} />
      {images[start + 1] && (
        <View style={__styles.image_view}>
          <ImageItem
            image={images[start + 1]}
            onPress={onPress}
            index={start + 1}
          />
          {overflow && (
            <TouchableOpacity
              onPress={event => onPress(images[start + 1], start + 1)}
              style={__styles.item_view_overlay}>
              <Text style={__styles.text}>{`+${images.length - 5}`}</Text>
            </TouchableOpacity>
          )}
        </View>
      )}
    </>
  );
};

export default class ImageGrid extends Component {
  render() {
    const {images, style, onPress} = this.props;
    return images.length > 0 ? (
      <View style={{...__styles.container_row, ...style}}>
        {images.length < 3 ? (
          <TwoImages images={images} onPress={onPress} />
        ) : (
          <ImageItem image={images[0]} onPress={onPress} index={0} />
        )}
        {images.length > 2 && (
          <View style={__styles.container}>
            {renderImages(1, false, images, onPress)}
          </View>
        )}
        {images.length > 3 && (
          <View style={__styles.container}>
            {renderImages(3, images.length > 5, images, onPress)}
          </View>
        )}
      </View>
    ) : null;
  }
}

export const __styles = StyleSheet.create({
  container_row: {
    flexDirection: 'row',
  },

  container: {
    flex: 1,
  },

  image_view: {
    flex: 1,
  },

  image: {
    width: '100%',
    height: '100%',
    backgroundColor: 'grey',
    borderWidth: 1,
    borderColor: 'white',
  },

  item_view: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },

  item_view_overlay: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'white',
  },

  text: {
    color: 'white',
    fontSize: 18,
  },
});
