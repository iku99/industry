import React, {useEffect} from 'react';
import {Drawer, Router, Scene, Stack} from 'react-native-router-flux';
import {AppState, AsyncStorage, StatusBar} from 'react-native';
import AppConstants from '../resource/AppConstants';
import MainScreen from './screen/main/MainScreen';
import FlashScreen from './screen/FlashScreen/FlashScreen';
import LoginScreen from './screen/login/LoginScreen';
import RegisterScreen from './screen/register/registerScreen';
import ForgotPasswordScreen from './screen/ForgotPasswoedScreen/ForgotPasswordScreen';
import DrawerElements from './elements/DrawerElements';
import Styles from '../resource/Styles';
import PersonalScreen from './screen/main/profile/components/PersonalScreen';
import AddressScreen from './screen/main/profile/components/address/AddressScreen';
import PaymentScreen from './screen/main/profile/components/PaymentScreen';
import OrderScreen from './screen/order/OrderScreen';
import NotificationList from './screen/main/notification/component/NotificationList';
import ProfileScreen from './screen/main/profile/ProfilePresentation';
import ProductDetail from './screen/product/productDetail/ProductDetail';
import BannerProduct from './elements/carousel/BannerProduct';
import ListProductCategory from './screen/product/ListProductCategory';
import GroupScreen from './screen/group/GroupScreen';
import GroupDetail from './screen/group/groupDetail/GroupDetail';
import PostDetail from './screen/group/NewGroupStep/PostDetail';
import NewGroup from './screen/group/NewGroup';
import SuccessGroup from './screen/group/NewGroupStep/SuccessGroup';
import StoreDetail from './screen/store/StoreDetail';
import MemberScreen from './screen/group/member/MemberScreen';
import MemberManage from './screen/group/groupDetail/manage/MemberManage';
import DetailMember from './screen/group/modal/DetailMember';
import Test from './Test/Test';
import PostManager from './screen/group/groupDetail/manage/PostManager';
import InformationGroup from './screen/group/groupDetail/manage/InformationGroup';
import CartScreen from './screen/main/cart/CartScreen';
import CheckOutScreen from './screen/checkout/CheckOutScreen';
import WebviewScreen from './screen/checkout/WebViewScreen';
import BiddingScreen from './screen/bidding/BiddingScreen';
import ConversationView from './screen/main/message/ConversationView';
import SendImageScreen from './screen/main/message/SendImageScreen';
import FullViewImage from './elements/FullViewImage';
import SettingScreen from './screen/main/profile/components/SettingScreen';
import HelpScreen from './screen/main/profile/components/HelpScreen';
import ChangePasswordScreen from './screen/changePassword/ChangePasswordScreen';
import AddressModify from './screen/main/profile/components/address/AddressModify';
import OrderDetail from './screen/order/OrderDetail';
import ProductReview from './screen/main/profile/components/ProductReview';
import FlashSellerScreen from './screen/seller/FlashSellerScreen';
import RegisterStoreScreen from './screen/seller/RegisterStoreScreen';
import TransportList from './screen/seller/TransportList';
import SellerDetail from './screen/seller/sellerDetail/SellerDetail';
import AddProduct from './screen/seller/AddProduct';
import ListProduct from './screen/seller/ListProduct';
import IncomeScreen from './screen/seller/IncomeScreen';
import RatingShop from './screen/seller/RatinggShop';
import StatisticalScreen from './screen/seller/StatisticalScreen';
import FollowingScreen from './screen/seller/FollowingScreen';
import FollowerScreen from './screen/seller/FollowerScreen';
import MySaleScreen from './screen/seller/MySaleScreen';
import SupplierScreen from './screen/supplier/SupplierScreen';
import NewsScreen from './screen/news/NewsScreen';
import SearchScreen from './screen/search/SearchScreen';
import VoucherList from './screen/voucher/VoucherList';
import OrderSuccess from './screen/orderSuccess/OrderSuccess';
import OrderDetailSeller from './screen/seller/OrderDetailSeller';
import ViewSeller from "./screen/seller/ViewSeller";
import UpdateSeller from "./screen/seller/UpdateSeller";
import NewsDetail from "./screen/news/NewsDetail";
import VoucherStore from "./screen/seller/VoucherStore";
import UploadVoucher from "./screen/seller/UploadVocher/UploadVoucher";
import AboutVietChem from "./screen/main/profile/components/AboutVietChem";
import PolicyApp from "./screen/main/profile/components/PolicyApp";
import CustomerService from "./screen/main/profile/components/CustomerService";
import ReviewScreen from "./screen/ReviewScreen";
import TradePromotionScreen from "./screen/TradePromotionScreen";
import TradePromotionDetail from "./screen/TradePromotionScreen/TradePromotionDetail";
import CheckOtp from "./elements/CheckOtp";
import AddPostToGroup from "./screen/group/post/AddPostToGroup";
import ActiveTab from "./screen/main/notification/component/active/ActiveTab";
export default function RoutesClass() {
  useEffect(() => {
    statusBarConfig();
    console.disableYellowBox = true;
    if (!AppConstants.IS_IOS) {
      AppState.addEventListener('focus', () => {
        statusBarConfig();
      });
    }

    let originalHandler = global.ErrorUtils.getGlobalHandler();
    async function errorHandler(e, isFatal) {
      originalHandler(e, isFatal);
    }

    global.ErrorUtils.setGlobalHandler(errorHandler);
    return () => {
      AppState.removeEventListener('focus');
    };
  });

  const statusBarConfig = () => {
    if (!AppConstants.IS_IOS) {
      StatusBar.setTranslucent(true);
      StatusBar.setBackgroundColor('transparent');
    }
    StatusBar.setHidden(false);
    StatusBar.setBarStyle('dark-content');
  };
  return (
    <Router>
      <Stack key="root">
        <Drawer
          key="drawer"
          hideBackImage={false}
          hideNavBar={true}
          headerMode={'screen'}
          drawerIcon={true}
          onClose={() => this.closeDrawer()}
          contentComponent={DrawerElements}
          drawerWidth={Styles.constants.widthScreen}
          drawerPosition="left">
          <Scene key="main" hideNavBar={true} component={MainScreen} />
        </Drawer>
        <Scene
          key="flashscreen"
          component={FlashScreen}
          hideNavBar={true}
          initial={true}
         />
        <Scene key="TradePromotionScreen" component={TradePromotionScreen} hideNavBar={true} />
        <Scene key="TradePromotionDetail" component={TradePromotionDetail} hideNavBar={true} />
        <Scene key="login" component={LoginScreen} hideNavBar={true} />
        <Scene key="register" component={RegisterScreen} hideNavBar={true} />
        <Scene key="checkOtp" component={CheckOtp} hideNavBar={true} />
        <Scene
          key="forgot"
          component={ForgotPasswordScreen}
          hideNavBar={true}
        />
        <Scene key="personal" component={PersonalScreen} hideNavBar={true} />
        <Scene key="address" component={AddressScreen} hideNavBar={true} />
        <Scene
          key="addressModify"
          component={AddressModify}
          hideNavBar={true}
        />
        <Scene key="payment" component={PaymentScreen} hideNavBar={true} />
        <Scene key="order" component={OrderScreen} hideNavBar={true} />
        <Scene key="orderDetail" component={OrderDetail} hideNavBar={true} />
        <Scene key="helpScreen" component={HelpScreen} hideNavBar={true} />
        <Scene  key="setting" component={SettingScreen} hideNavBar={true} />
        <Scene key="aboutVietChem" component={AboutVietChem} hideNavBar={true} />
        <Scene  key="policyApp" component={PolicyApp} hideNavBar={true} />
        <Scene key="customerService" component={CustomerService} hideNavBar={true} />
        <Scene key="reviewScreen" component={ReviewScreen} hideNavBar={true} />
        <Scene
          key="changePassword"
          component={ChangePasswordScreen}
          hideNavBar={true}
        />
        <Scene
          key="notificationList"
          component={NotificationList}
          hideNavBar={true}
        />
        <Scene key="active" component={ActiveTab} hideNavBar={true} />
        <Scene key="profile" component={ProfileScreen} hideNavBar={true} />
        <Scene key="test" component={BannerProduct} hideNavBar={true} />
        <Scene
          key="productDetail"
          component={ProductDetail}
          hideNavBar={true}
        />
        <Scene
          key="productReview"
          component={ProductReview}
          hideNavBar={true}
        />
        <Scene
          key="productList"
          component={ListProductCategory}
          hideNavBar={true}
        />
        <Scene key="groups" component={GroupScreen} hideNavBar={true} />
        <Scene key="newGroup" component={NewGroup} hideNavBar={true} />
        <Scene key="successGroup" component={SuccessGroup} hideNavBar={true} />
        <Scene key="groupDetail" component={GroupDetail} hideNavBar={true} />
        <Scene key="postDetail" component={PostDetail} hideNavBar={true} />
        <Scene key="postManager" component={PostManager} hideNavBar={true} />
        <Scene key="memberManager" component={MemberManage} hideNavBar={true} />

        <Scene key="storeDetail" component={StoreDetail} hideNavBar={true} />
        <Scene
          key="addPostToGroup"
          component={AddPostToGroup}
          hideNavBar={true}
        />
        <Scene key="detailMember" component={DetailMember} hideNavBar={true} />
        <Scene
          key="informationGroupManage"
          component={InformationGroup}
          hideNavBar={true}
        />
        <Scene key="memberGroup" component={MemberScreen} hideNavBar={true} />
        <Scene key="cartScreen" component={CartScreen} hideNavBar={true} />
        <Scene key="checkout" component={CheckOutScreen} hideNavBar={true} />
        <Scene
          key="webviewScreen"
          component={WebviewScreen}
          hideNavBar={true}
        />

        <Scene
          key="biddingScreen"
          component={BiddingScreen}
          hideNavBar={true}
        />
        <Scene
          key="conversationView"
          component={ConversationView}
          hideNavBar={true}
        />
        <Scene
          key="sendImageScreen"
          component={SendImageScreen}
          hideNavBar={true}
        />
        <Scene
          key="fullViewImage"
          component={FullViewImage}
          hideNavBar={true}
        />

        <Scene
          key="flashSellerScreen"
          component={FlashSellerScreen}
          hideNavBar={true}
        />
        <Scene
          key="registerStore"
          component={RegisterStoreScreen}
          hideNavBar={true}
        />
        <Scene
          key="transportList"
          component={TransportList}
          hideNavBar={true}
        />
        <Scene key="sellerDetail" component={SellerDetail} hideNavBar={true} />
        <Scene key="uploadProduct" component={AddProduct} hideNavBar={true} />
        <Scene key="listProduct" component={ListProduct} hideNavBar={true} />
        <Scene key="incomeScreen" component={IncomeScreen} hideNavBar={true} />
        <Scene key="ratingShop" component={RatingShop} hideNavBar={true} />
        <Scene key="viewSeller" component={ViewSeller} hideNavBar={true} />
        <Scene key="updateSeller" component={UpdateSeller} hideNavBar={true} />
        <Scene
          key="statisticalScreen"
          component={StatisticalScreen}
          hideNavBar={true}
        />
        <Scene
          key="followingScreen"
          component={FollowingScreen}
          hideNavBar={true}
        />
        <Scene
          key="followerScreen"
          component={FollowerScreen}
          hideNavBar={true}
        />
        <Scene key="mySaleScreen" component={MySaleScreen} hideNavBar={true} />
        <Scene
          key="supplierScreen"
          component={SupplierScreen}
          hideNavBar={true}
        />
        <Scene key="newsScreen" component={NewsScreen} hideNavBar={true} />
        <Scene key="newsDetail" component={NewsDetail} hideNavBar={true} />
        <Scene
          key="searchScreen"
          component={SearchScreen}
          hideNavBar={true}
        />
        <Scene key="voucherList" component={VoucherList} hideNavBar={true} />
        <Scene key="voucherStore" component={VoucherStore} hideNavBar={true} />
        <Scene key="uploadVoucher" component={UploadVoucher} hideNavBar={true} />
        <Scene key="orderSuccess" component={OrderSuccess} hideNavBar={true} />
        <Scene
          key="orderDetailSeller"
          component={OrderDetailSeller}
          hideNavBar={true}
        />
      </Stack>
    </Router>
  );
}
