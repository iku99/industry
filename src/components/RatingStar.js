import React, {Component} from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import PropTypes from 'prop-types';
import ImageHelper from "../resource/images/ImageHelper";

export default class RatingStar extends Component {
  render() {
    let paddingStar =
      this.props.paddingStar !== undefined ? this.props.paddingStar : 0;
    let {rating, style} = this.props;
    let stars = [];
    let ratingPoint = [1, 2, 3, 4, 5];
    let onRate = this.props.onRate;
    if (this.props.rightToLeft) {
      ratingPoint.reverse();
    }
    stars = ratingPoint.map((point, index) => {
      let path = ImageHelper.star;
      if (point > rating) {
        path = this.props.hideUnStar ? null : ImageHelper.unStar;
      }

      return (
        <View
            key={`ratingPoint_${index.toString()}`}
          style={{paddingHorizontal: paddingStar}}
          onPress={() => {
            if (onRate !== undefined) {
              onRate(point);
            }
          }}>
          <Image
            style={{
              height: this.props.size,
              width: this.props.size,
              marginHorizontal: 1,
            }}
            source={path}
          />
        </View>
      );
    });

    return <View style={[styles.container, style ? style : {}]}>{stars}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'transparent',
    flexDirection: 'row',
  },
});
