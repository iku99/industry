import React, {Component} from 'react';
import {ActivityIndicator, FlatList, View} from "react-native";
import Styles from "../../../resource/Styles";
import RatingHandle from "../../../sagas/RatingHandle";
import Toolbar from "../../elements/toolbar/Toolbar";
import ColorStyle from "../../../resource/ColorStyle";
import {strings} from "../../../resource/languages/i18n";
import RateReview from "../../elements/RateReview";
import EmptyView from "../../elements/reminder/EmptyView";
import ItemComment from "../../elements/viewItem/itemComment/ItemComment";
const countItem=10
export default class ReviewScreen extends Component{
    constructor(props) {
        super(props);
        this.state={
            data:[],
            page:1,
            ratingAvg:0,
            statsCount:{
                '1': 0,
                '2': 0,
                '3': 0,
                '4': 0,
                '5': 0,
            },
            ratingCount:0,
            loading:true,
            canLoadData: true,
            selectRate:5,
        }
    }
    componentDidMount() {
           this.getReviewOfRate()
    }
    getRatingProduct() {
        let params = {
            page_index:this.state.page,
            page_size:countItem,
            star:this.state.selectRate
        };
        RatingHandle.getInstance().getProductRating(
            this.props.idProduct,
            params,
            (isSuccess, dataResponse) => {
                if (isSuccess) {
                    if (dataResponse.data !== undefined) {
                        let data=this.state.data
                        let mData=dataResponse.data.data
                         if(mData.length<countItem) this.setState({canLoadData: true});
                       data= data.concat(mData)
                        this.setState({data:data,loading:false});
                    }
                }

            },
        );
    }
    getReviewOfRate(){
        RatingHandle.getInstance().getRatingCount(
            this.props.idProduct,
            (isSuccess, dataResponse) => {
                if (isSuccess) {
                    if (dataResponse.data !== undefined) {
                        let mData=dataResponse.data
                        this.setState({statsCount:mData.data,ratingAvg:mData.rate_average,ratingCount:mData.sum},()=>this.getRatingProduct())
                    }
                }

            },
        );
    }
    render() {
        return (
            <View style={Styles.container}>
                <Toolbar backgroundColor={ColorStyle.tabActive} textColor={ColorStyle.tabWhite} title={strings('review')}/>
                <RateReview
                    ratingAvg={this.state.ratingAvg}
                    statsCount={this.state.statsCount}
                    ratingCount={this.state.ratingCount}
                    callback={(index)=>{
                        this.setState({selectRate:index,data:[]},()=>{
                            this.getReviewOfRate()
                        })
                        }
                }
                />
                {this.state.data?.length > 0 && (
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        horizontal={false}
                        data={this.state.data}
                        keyExtractor={this.keyExtractor}
                        renderItem={this.renderItemComment}
                        style={{flex: 1, height: '100%'}}
                        onEndReachedThreshold={0.5}
                        removeClippedSubviews={true} // Unmount components when outside of window
                        windowSize={10}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReached={() => this.handleLoadMore()}
                    />
                )}
                {this.state.data?.length === 0 && (
                    <EmptyView
                        containerStyle={Styles.loading.container}
                        text={strings('notReview')}
                    />
                )}
            </View>
        );
    }
    keyExtractor = (item, index) => `get_Rev_${index.toString()}`;
    renderItemComment = ({item, index}) => {
        return <ItemComment item={item} />;
    };
    renderFooter = () => {
        //it will show indicator at the bottom of the list when data is loading otherwise it returns null
        if (!this.state.loading) {
            return null;
        }
        return <ActivityIndicator style={{color: '#000'}} />;
    };
    handleLoadMore() {
        if (this.state.loading) {
            return;
        }
        if (this.state.canLoadData) {
            return;
        }
        this.setState({loading: true, page: this.state.page + 1}, () =>
            this.getRatingProduct,
        );
    }
}
