import Styles from '../../../resource/Styles';
import ColorStyle from '../../../resource/ColorStyle';

let X = Styles.constants.X;
export default {
  container: {
    paddingVertical:X*0.3,
    marginTop: X * 0.3,
    backgroundColor: ColorStyle.tabWhite,
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 1,
    elevation:2
  },
  titleItem: {
    paddingHorizontal: X * 0.75,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  itemRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: X * 0.3,
    marginTop: X * 0.38,
  },
  itemColumn: {
    marginHorizontal: X * 0.6,
    marginTop: X * 0.38,
  },
  text: {
    textName: [
      Styles.text.text18,
      {
        color: ColorStyle.tabBlack,
        fontWeight: '700',
        maxWidth: '80%',
        lineHeight: 21,
      },
    ],
    textNameShop: [
      Styles.text.text12,
      {
        color: ColorStyle.tabBlack,
        fontWeight: '700',
        maxWidth: '80%',
        lineHeight: 21,
      },
    ],
    textComponent: [
      Styles.text.text12,
      {
        color: ColorStyle.tabBlack,
        fontWeight: '400',
        alignItems: 'center',
        textAlign:'center'
      },
    ],
    textTitle:

      {
        ... Styles.text.text14,
        color: ColorStyle.tabBlack,
        fontWeight: '700',
        alignItems: 'center',
      },
    textViewAll: [
      Styles.text.text10,
      {
        color: ColorStyle.tabActive, fontWeight: '700',
      },
    ],
    textSold: [
      Styles.text.text12,
      {
        color: ColorStyle.tabBlack,
        fontWeight: '400',
        marginLeft: 5,
      },
    ],
    textDes: [
      Styles.text.text10,
      {
        color: ColorStyle.gray,
        lineHeight: 12,
        marginLeft: 5,
      },
    ],
    seeMore: [
      Styles.text.text10,
      {
        color: ColorStyle.tabActive,
        marginLeft: 5,
        fontStyle: 'italic',
      },
    ],
    textItemChat: [
      Styles.text.text10,
      {
        color: ColorStyle.tabWhite,
        fontWeight: '400',
      },
    ],
    textItemBuy: [
      Styles.text.text12,
      {
        color: ColorStyle.tabWhite,
        fontWeight: '700',
      },
    ],
  },
  postContentContainer: {
    // borderWidth: 1,
    // borderColor: 'red',
    flexDirection: 'column',
  },

  postMedia: {
    //borderWidth: 1,
    //borderColor: 'red',
    width: '100%',
    height: 280,
    resizeMode: 'cover',
  },

  postDescription: {
    paddingTop: 15,
    paddingHorizontal: 15,
  },

  storeInfo: {
    paddingHorizontal: X * 0.6,
    marginTop: X * 0.3,
    backgroundColor: ColorStyle.colorPersonal,
  },
  storeAvatar: {
    width: X * 0.7,
    height: X * 0.7,
    borderRadius: X * 0.7,
    alignSelf: 'center',
  },
  bottomNavigation: {
    width: '100%',
    height: X * 1.3,
    backgroundColor: 'transform',
    flexDirection: 'row',
  },
  itemBottom: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: ColorStyle.itemBottom,
  },
  itemBottomBuy: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: ColorStyle.tabActive,
  },
};
