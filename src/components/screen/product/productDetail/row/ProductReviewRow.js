import React from 'react';
import ProductStyle from "../../ProductStyle";
import {FlatList, Image, Text, TouchableOpacity, View} from "react-native";
import Styles from "../../../../../resource/Styles";
import ColorStyle from "../../../../../resource/ColorStyle";
import {strings} from "../../../../../resource/languages/i18n";
import ItemComment from "../../../../elements/viewItem/itemComment/ItemComment";
import RatingHandle from "../../../../../sagas/RatingHandle";
import EmptyView from "../../../../elements/reminder/EmptyView";
import NavigationUtils from "../../../../../Utils/NavigationUtils";
export default class ProductReviewRow extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            pageCount: 0,
            data:[]
        }
    }

    componentDidMount() {
        this.getRatingProduct()
    }
    keyExtractor = (item, index) => `getProductRev_${index.toString()}`;
    renderItemComment = ({item, index}) => {
        return <ItemComment item={item} />;
    };
    render() {
        return(
            <View
                style={[
                    ProductStyle.storeInfo,
                    {
                        marginTop: 0,
                        paddingVertical:10
                    },
                ]}>
                <TouchableOpacity
                    onPress={()=>{NavigationUtils.goToReview(this.props.item.id)}}
                    style={[
                        ProductStyle.itemRow,
                        {
                            marginHorizontal: 0,
                            marginBottom: Styles.constants.X * 0.38,
                        },
                    ]}>
                    <Text
                        style={[
                            Styles.text.text18,
                            {color: ColorStyle.tabBlack, fontWeight: '700'},
                        ]}>
                        {strings('productReviews')}
                    </Text>
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 15,
                            paddingBottom: 10,
                        }}>
                        <Text
                            style={[
                                Styles.text.text12,
                                {color: ColorStyle.tabActive, fontWeight: '700'},
                            ]}>
                            {strings('viewAll')}
                        </Text>
                    </View>
                </TouchableOpacity>
                {this.state.data.length > 0 && (
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        horizontal={false}
                        data={this.state.data}
                        keyExtractor={this.keyExtractor}
                        renderItem={this.renderItemComment}
                        style={{flex: 1, height: '100%'}}
                        onEndReachedThreshold={0.5}
                        removeClippedSubviews={true} // Unmount components when outside of window
                        windowSize={10}
                    />
                )}
                {this.state.data.length === 0 && (
                    <EmptyView
                    containerStyle={Styles.loading.container}
                    text={strings('notReview')}
                    />
                )}
            </View>
        )
    }
    getRatingProduct() {
      let params = {
        page_index:1,
        page_size:3
      };
      RatingHandle.getInstance().getProductRating(
        this.props.item?.id,
        params,
        (isSuccess, dataResponse) => {
          let commentCount;
          if (isSuccess) {
            if (dataResponse.data !== undefined) {
              commentCount = dataResponse.total_items;
                let pageCount =parseInt(commentCount)
                let mData=dataResponse.data
                this.setState({pageCount,data:mData.data});
            }
          }

        },
      );
    }
}
