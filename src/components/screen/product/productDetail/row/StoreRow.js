import React from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import ProductStyle from '../../ProductStyle';
import {Icon} from 'react-native-elements';
import ColorStyle from '../../../../../resource/ColorStyle';
import {strings} from '../../../../../resource/languages/i18n';
import Styles from '../../../../../resource/Styles';
import StoreHandle from '../../../../../sagas/StoreHandle';
import ProductHandle from '../../../../../sagas/ProductHandle';
import ViewStoreLoading from '../../../../elements/SkeletonPlaceholder/ViewStoreLoading';
import ImageHelper from '../../../../../resource/images/ImageHelper';
import NavigationUtils from '../../../../../Utils/NavigationUtils';
import constants from '../../../../../Api/constants';
import MediaUtils from "../../../../../Utils/MediaUtils";
export default class StoreRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            store: undefined,
            countRateStore: 0,
            productCount: 0,
            item: this.props.item,
            listProductStore: [],
            loading:true
        };
    }
    componentDidMount() {
        this.getStoreDetail(this.props.item);

    }

    render() {
        let store = this.state.store;
        if (
            store === undefined
        ) {
            return <ViewStoreLoading />;
        }
        return (
            <View style={[ProductStyle.storeInfo]}>
                <TouchableOpacity onPress={()=> NavigationUtils.goToShopDetail(store.id)} style={{flexDirection: 'row', paddingVertical: 14}}>
                    <View style={{flex: 1}}>
                        <Image
                            style={ProductStyle.storeAvatar}
                            source={
                                MediaUtils.getStoreAvatar(store?.avatar,store?.id)
                            }
                        />
                    </View>
                    <View style={{flex: 5}}>
                        <Text style={ProductStyle.text.textNameShop}>{store?.name}</Text>
                        <View style={{width: '100%', flexDirection: 'row'}}>
                            <Text style={ProductStyle.text.textComponent}>
                                {store?.product_quantity} {strings('product1')} |
                                <Icon
                                    name={'star'}
                                    type={'entypo'}
                                    size={13}
                                    color={'#FFC120'}
                                />
                                {store?.rate===null?0:store?.rate} | {strings('chatStore')}
                            </Text>
                        </View>
                        <View style={{width: '100%', flexDirection: 'row'}}>
                            <Text
                                numberOfLines={1}
                                style={[
                                    Styles.text.text10,
                                    {flex: 1, color: ColorStyle.secondary},
                                ]}>
                                {strings('address1')} :
                            </Text>
                            <Text numberOfLines={1} style={[Styles.text.text10, {flex: 4}]}>
                                {store?.address.address}
                            </Text>
                        </View>
                    </View>
                </TouchableOpacity>
                <View style={{marginLeft: 10}}>
                    <Text
                        style={[
                            Styles.text.text12,
                            {color: ColorStyle.secondary, fontWeight: '700'},
                        ]}>
                        {strings('shopProduct')}
                    </Text>
                    <FlatList
                        horizontal={true}
                        style={{paddingVertical: 10}}
                        data={this.state.listProductStore}
                        renderItem={this.renderItemProductStore}
                    />
                    <TouchableOpacity
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 15,
                            paddingBottom: 10,
                        }}
                        onPress={() =>
                            NavigationUtils.goToShopDetail(store.id)
                        }>
                        <Icon
                            name={'doubleright'}
                            type={'antdesign'}
                            size={13}
                            color={ColorStyle.tabActive}
                        />
                        <Text
                            style={[
                                Styles.text.text12,
                                {color: ColorStyle.tabActive, fontWeight: '700'},
                            ]}>
                            {strings('goShop')}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
    renderItemProductStore = ({item, index}) => {
        return (
            <TouchableOpacity onPress={()=>  NavigationUtils.goToProductDetail(item)} style={{marginHorizontal: 15}}>
                <Image
                    style={{width: 80, height: 80}}
                    source={{
                        uri: this.getImageUrl(item?.image_url,index),
                    }}
                />
                <View
                    style={{
                        width: 80,
                        height: 30,
                        backgroundColor: 'rgba(0, 0, 0, 0.6)',
                        alignItems: 'center',
                        justifyContent: 'center',
                        bottom: 0,
                        position: 'absolute',
                    }}>
                    <Text
                        numberOfLines={1}
                        style={{
                            maxWidth: '100%',
                            textAlign: 'center',
                            color: ColorStyle.tabWhite,
                        }}>
                        {item.name}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };
    getStoreDetail(storeId) {
        StoreHandle.getInstance().getStoreDetail(storeId, (isSuccess, store) => {
            if (isSuccess) {
                this.setState({store: store,loading:false});
                this.getProductListShop(store.id)
            }
        });
    }

    getProductListShop(id) {
        let param = {
            current_page: 1,
            page_size: 4,
            store_id:id
        };
        ProductHandle.getInstance().getProductList(
            param,
            (isSuccess, dataResponse) => {
                if (isSuccess) {
                    this.setState(
                        {
                            listProductStore: dataResponse.data.data,
                        },
                        () => {},);
                }
            },true
        );
    }
    getImageUrl(images,index) {
        if (images != null && images.length > 0) {
            return constants.host + images[0].url;
        } else {
            return 'https://picsum.photos/130/214?' + index;
        }
    }
}
