import React, {Component} from 'react';
import {
  FlatList,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../../../resource/Styles';
import {strings} from '../../../../../resource/languages/i18n';
import ViewProductItem from '../../../../elements/viewItem/ViewedProduct/ViewProductItem';
import {getCategoryAction} from '../../../../../actions';
import GlobalUtil from '../../../../../Utils/Common/GlobalUtil';
import NavigationUtils from '../../../../../Utils/NavigationUtils';
class SuggestRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.itemList,
    };
  }
  componentDidMount() {}
  render() {
    let isTablet = GlobalUtil.isTablet();
    let columnNumber = isTablet ? 3 : 2;
    return (
      <View style={[Styles.containerItemHome, {marginTop: 0}]}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={this.state.data}
          numColumns={columnNumber}
          style={{
            width: Styles.constants.widthScreenMg24,
            marginTop: 20,
            alignItems: 'center',
          }}
          keyExtractor={this.keyExtractor}
          renderItem={this._renderItemProduct}
          removeClippedSubviews={true} // Unmount components when outside of window
          windowSize={10}
        />
      </View>
    );
  }
  _renderItemProduct = ({item, index}) => (
    <ViewProductItem
      item={item}
      loading={this.state.loading}
      onClick={item => {
        NavigationUtils.goToProductDetail1(item);
      }}
    />
  );
}
const mapStateToProps = state => {
  return {
    category: state.categoryReducers,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onGetCategory: param => {
      dispatch(getCategoryAction(param));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SuggestRow);
