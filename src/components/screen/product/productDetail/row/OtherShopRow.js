import React from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import AppConstants from '../../../../../resource/AppConstants';
import ProductHandle from '../../../../../sagas/ProductHandle';
import OtherProductLoading from '../../../../elements/SkeletonPlaceholder/OtherProductLoading';
import ProductStyle from '../../ProductStyle';
import {strings} from '../../../../../resource/languages/i18n';
import {Icon} from 'react-native-elements';
import ColorStyle from '../../../../../resource/ColorStyle';
import ViewProductRow from "../../../../elements/viewItem/ViewedProduct/ViewProductRow";
import ViewProductItem from "../../../../elements/viewItem/ViewedProduct/ViewProductItem";
import NavigationUtils from "../../../../../Utils/NavigationUtils";
export default class OtherShopRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      otherProductOfStore: [],
    };
  }
  componentDidMount() {
    this.getOtherProductInSameStore(this.props.idCategory);
  }

  render() {
    let data = this.state.otherProductOfStore;
    if (data === undefined || data.length === 0) {
      return <OtherProductLoading />;
    }
    return (
      <View style={ProductStyle.container}>
        <TouchableOpacity style={ProductStyle.titleItem}>
          <Text style={ProductStyle.text.textTitle}>
            {strings('otherProductOfStore')}
          </Text>
        </TouchableOpacity>

        <FlatList
          data={data}
          horizontal={true}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItemProduct}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  }
  keyExtractor = (item, index) => `get_product_store_${index.toString()}`;
  renderItemProduct = ({item, index}) => (
    <ViewProductItem
      item={item}
      loading={this.state.loading}
      onClick={item => {
        NavigationUtils.goToProductDetail(item);
      }}
    />
  );
  getOtherProductInSameStore() {
    let param = {
      page_index: 1,
      page_size: AppConstants.isTablet ? 9 : 10,
      category_id:this.props.idCategory
    };
      ProductHandle.getInstance().getProductList(
          param,
      (isSuccess, dataResponse) => {
        if (isSuccess) {
          this.setState(
            {
              otherProductOfStore: dataResponse.data.data,
            }
          );
        }
      },true
    );
  }
}
