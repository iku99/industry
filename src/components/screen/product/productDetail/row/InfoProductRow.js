import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import ProductStyle from '../../ProductStyle';
import {Icon} from 'react-native-elements';
import ColorStyle from '../../../../../resource/ColorStyle';
import RatingStar from '../../../../RatingStar';
import {strings} from '../../../../../resource/languages/i18n';
import Styles from '../../../../../resource/Styles';
import CurrencyFormatter from '../../../../../Utils/CurrencyFormatter';
import FuncUtils from '../../../../../Utils/FuncUtils';
import ProductHandle from '../../../../../sagas/ProductHandle';
import RatingHandle from '../../../../../sagas/RatingHandle';
import HTMLView from "react-native-htmlview";
export default class InfoProductRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      likesCount: 0,
      liked: this.props.item?.is_like,
      ratingCount: 0,
      ratingAvg: 0,
    };
  }
  componentDidMount() {
    this.setState({liked: this.props.item?.is_like});
    //  this.getRatingStats();
  }
  getProperty(data) {
    if (data !== undefined) {
      return data?.map((item, index) => {
        return this.renderRow(item.name, item.value);
      });
    }
    return <View />;
  }
  render() {
    let data = this.props.item;
    let like =
      this.state.liked === undefined
        ? this.props.item?.is_like
        : this.state.liked;
    if (data === undefined) {
      return <View />;
    } else {
      return (
        <View>
          <View style={ProductStyle.itemRow}>
            <Text style={ProductStyle.text.textName} numberOfLines={2}>
              {data?.name}
            </Text>
            <TouchableOpacity onPress={() => this.likeProduct()}>
              <Icon
                name={like ? 'heart' : 'heart-o'}
                type="font-awesome"
                size={25}
                color={ColorStyle.tabActive}
              />
            </TouchableOpacity>
          </View>
          <View
            style={[
              ProductStyle.itemRow,
              {justifyContent: 'flex-start', marginTop: 5},
            ]}>
            <RatingStar size={15} rating={data?.rate} />
            <Text style={ProductStyle.text.textSold}>
              ({data?.total_sold === null ? 0 : data.total_sold}{' '}
              {strings('sold')})
            </Text>
          </View>
          <View
            style={{marginHorizontal: Styles.constants.X * 0.3, marginTop: 8}}>
            <Text
              style={[
                Styles.text.text24,
                {
                  color: ColorStyle.tabActive,
                  fontWeight: '700',
                  marginVertical: 10,
                },
              ]}>
              {CurrencyFormatter(data?.price)}
            </Text>
            <View style={{width: Styles.constants.widthScreenMg24}}>
              <View
                style={{
                  backgroundColor: ColorStyle.bgButtonFal,
                  width: Styles.constants.widthScreenMg24,
                  padding: 15,
                }}>
                <Text
                  style={{
                    fontWeight: '500',
                    fontSize: 16,
                    color: ColorStyle.tabWhite,
                  }}>
                  {strings('description')}
                </Text>
              </View>
              {this.renderRow(strings('nameProduct'), data.name)}
              {this.renderRow(strings('introduce'), data.des)}
              {this.renderRow(
                strings('stock'),
                data.quantity === 0
                  ? strings('outStock')
                  : data.quantity + ' ' + strings('product1'),
              )}
              {this.getProperty(data.property)}
              {this.getProperty(data.features)}
            </View>
          </View>
          <View
            style={{
              marginHorizontal: 20,
              display: data.content === undefined ? 'none' : 'flex',
            }}>
            <TouchableOpacity>
              <Text
                style={{
                  marginVertical: 10,
                  fontWeight: '500',
                  color: ColorStyle.tabActive,
                  fontSize: 16,
                }}>
                {strings('content')}
              </Text>
            </TouchableOpacity>
              {data.content !==undefined && (
                  <HTMLView
                      value={`<body>${data?.content}<body>`}
                      stylesheet={{
                          body: {
                              color: ColorStyle.tabBlack,
                          },
                          p: {
                              color: ColorStyle.tabBlack, // make links coloured pink
                          },
                          a: {
                              lineHeight: 20
                          },
                      }}
                  />
              )}
          </View>
        </View>
      );
    }
  }
  renderRow(title, des) {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          display: des === undefined ? 'none' : 'flex',
          borderWidth: 1,
          borderColor: ColorStyle.gray,
          justifyContent: 'space-between',
          width: Styles.constants.widthScreenMg24,
        }}>
        <Text
          style={{
            width: Styles.constants.widthScreenMg24 / 2 - 10,
            fontSize: 14,
            color: ColorStyle.tabBlack,
            padding: 5,
          }}>
          {title}
        </Text>
        <View
          style={{width: 1, height: '100%', backgroundColor: ColorStyle.gray}}
        />
        <Text
          style={{
            width: Styles.constants.widthScreenMg24 / 2 - 10,
            fontSize: 14,
            color: ColorStyle.tabBlack,
            padding: 5,
          }}>
          {des}
        </Text>
      </View>
    );
  }
  likeProduct() {
    FuncUtils.getInstance().callRequireLogin(() => {
      let param = {
        product_id: this.props.item.id,
      };
      ProductHandle.getInstance().likeProduct(
        param,
        (isSuccess, dataResponse) => {
          if (isSuccess) {
            this.setState({liked: !this.state.liked});
            return;
          }
          this.setState({liked: false});
        },
      );
    });
  }
}
