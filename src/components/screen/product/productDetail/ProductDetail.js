import {EventRegister} from 'react-native-event-listeners';
import React, {Component} from 'react';
import {
  Animated,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import Styles from '../../../../resource/Styles';
import ProductUtils from '../../../../Utils/ProductUtils';
import ColorStyle from '../../../../resource/ColorStyle';
import {strings} from '../../../../resource/languages/i18n';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import BadgeView from '../../../elements/BadgeView';
import ProductHandle from '../../../../sagas/ProductHandle';
import FuncUtils from '../../../../Utils/FuncUtils';
import AppConstants from '../../../../resource/AppConstants';
import CartHandle from '../../../../sagas/CartHandle';
import ProductStyle from '../ProductStyle';
import BannerProduct from '../../../elements/carousel/BannerProduct';
import {UIActivityIndicator} from 'react-native-indicators';
import SimpleToast from 'react-native-simple-toast';
import InfoProductRow from './row/InfoProductRow';
import SuggestRow from './row/SuggestRow';
import StoreHandle from '../../../../sagas/StoreHandle';
import ProductReviewRow from './row/ProductReviewRow';
import StoreRow from './row/StoreRow';
import CurrencyFormatter from '../../../../Utils/CurrencyFormatter';
import QuantityControl from '../../../elements/QuantityControl';
import OtherShopRow from './row/OtherShopRow';
const BUY_TYPE = {
  ADD_TO_CART: 0,
  BUY_NOW: 1,
};
const defaulData = [
  {
    type: AppConstants.detailItemType.BANNER,
  },
  {
    type: AppConstants.detailItemType.DES_PRODUCT,
  },
  {
    type: AppConstants.detailItemType.STORE_INFO,
  },
  {
    type: AppConstants.detailItemType.PRODUCT_REVIEWS,
  },
  {
    type: AppConstants.detailItemType.OTHER,
  },
  {
    type: AppConstants.detailItemType.TITLE_SUGGEST,
  },
  {
    type: AppConstants.detailItemType.END,
  },
];

export default class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: this.props.item,
      quantity: 1,
      loading: false,
      ratingCount: 0,
      ratingAvg: 0,
      showSelectOptionView: false,
      buyType: BUY_TYPE.ADD_TO_CART,
      imageUrl: '',
      userInfo: {},
      //animation
      showAnimView: false,
      suggestProduct: [],
      //Scale
      scaleStartValue: new Animated.Value(1),
      scaleEndValue: 0.5,
      //alpha
      alphaStartValue: new Animated.Value(1),
      alphaEndValue: 0,
      //Translate
      translateStartXValue: new Animated.Value(0),
      translateStartYValue: new Animated.Value(0),
      translateEndXValue: Styles.constants.widthScreen,
      translateEndYValue:
        Styles.constants.widthScreen / 4 - Styles.constants.heightScreen,
      duration: 500,
      //store
      store: [],
      //store
      showRatingBT: false,

      listProduct: [],
      statsCount: {1.0: 0, 2.0: 0, 4.0: 0, 5.0: 0, 3.0: 0},
      data: [],
      isDes: false,
      page: 1,
    };
  }
  componentDidMount() {
    this.getProductDetail();
    this.setState({data: defaulData}, () => {
      this.getProductSuggestList();
    });
    this.setState({
      imageUrl: ProductUtils.getImageUrl(
        this.state.item?.image_url,
        this.state.item?.id,
      ),
    });
  }
  render() {
    return (
      <View style={Styles.container}>
        {this.toolbar()}
        {this.renderView()}
        {this.bottomScreen()}
        {this.state.showAnimView && (
          <Animated.Image
            source={{
              uri: this.state.imageUrl,
            }}
            style={{
              opacity: this.state.alphaStartValue,
              width: Styles.constants.widthScreen / 3,
              height: Styles.constants.widthScreen / 3,
              position: 'absolute',
              top:
                (Styles.constants.heightScreen -
                  Styles.constants.widthScreen / 2) /
                2,
              left:
                (Styles.constants.widthScreen -
                  Styles.constants.widthScreen / 2) /
                2,
              transform: [
                {
                  scale: this.state.scaleStartValue,
                },
                {
                  translateX: this.state.translateStartXValue,
                },
                {
                  translateY: this.state.translateStartYValue,
                },
              ],
            }}
          />
        )}
        {this.renderBuyView()}
      </View>
    );
  }
  bottomScreen() {
    let store = this.state.store;
    return (
      <View style={ProductStyle.bottomNavigation}>
        <TouchableOpacity
          onPress={() => {
            SimpleToast.show(strings('test'), SimpleToast.SHORT);
            if (store?.user_id !== undefined) {
              // this.goToConversation(store?.user_id);
              SimpleToast.show(strings('test'), SimpleToast.SHORT);
            }
          }}
          style={[
            ProductStyle.itemBottom,
            {borderRightWidth: 1, borderRightColor: ColorStyle.tabWhite},
          ]}>
          <Icon
            name={'message'}
            style={'materialcommunityicons'}
            size={20}
            color={ColorStyle.tabWhite}
          />
          <Text style={ProductStyle.text.textItemChat}>
            {strings('chatNow')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.renderShowBuy(BUY_TYPE.ADD_TO_CART);
          }}
          style={ProductStyle.itemBottom}>
          <Icon
            name={'shopping-basket-add'}
            type={'fontisto'}
            size={20}
            color={ColorStyle.tabWhite}
          />
          <Text style={ProductStyle.text.textItemChat}>{strings('add')}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.renderShowBuy(BUY_TYPE.BUY_NOW)}
          style={ProductStyle.itemBottomBuy}>
          <Text style={ProductStyle.text.textItemBuy}>{strings('buyNow')}</Text>
        </TouchableOpacity>
      </View>
    );
  }
  goToConversation(userId) {
    NavigationUtils.goToConversation(userId);
  }
  toolbar() {
    return (
      <View
        style={{
          width: Styles.constants.widthScreen,
          paddingTop: Styles.constants.X,
          paddingHorizontal: Styles.constants.marginHorizontal20,
          alignSelf: 'center',
          backgroundColor: ColorStyle.tabActive,
          paddingBottom: 10,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={() => Actions.pop()}>
            <Icon
              name="left"
              type="antdesign"
              size={25}
              color={ColorStyle.tabWhite}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              ...Styles.search.searchInput,
              marginVertical: 0,
              width: '75%',
            }}
            onPress={() => Actions.jump('searchScreen')}>
            <Icon
              name={'search'}
              type={'evilicons'}
              size={25}
              color={'#a59999'}
            />
            <Text
              style={Styles.search.searchText}
              underlineColorAndroid="transparent">
              {strings('textSearch')}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{padding: 5}}
            onPress={() => NavigationUtils.goToCart()}>
            <Icon
              name="shopping-cart"
              style="entypo"
              size={25}
              color={ColorStyle.tabWhite}
            />
            <BadgeView />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  renderView() {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        horizontal={false}
        data={this.state.data}
        keyExtractor={this.keyExtractor}
        renderItem={({item, index}) => this._renderItem(item, index)}
        style={{flex: 1, height: '100%'}}
        ListFooterComponent={this.renderFooter.bind(this)}
        onEndReachedThreshold={0.5}
        onEndReached={() => this.handleLoadMore()}
        removeClippedSubviews={true} // Unmount components when outside of window
        windowSize={10}
      />
    );
  }
  keyExtractor = (item, index) => `getProductDetail_${index.toString()}`;
  _renderItem = item => {
    switch (item.type) {
      case AppConstants.detailItemType.BANNER:
        return <BannerProduct item={this.state.item} />;
      case AppConstants.detailItemType.DES_PRODUCT:
        return <InfoProductRow item={this.state.item} />;
      case AppConstants.detailItemType.STORE_INFO:
        return <StoreRow item={this.state.item?.store.id} />;
      case AppConstants.detailItemType.PRODUCT_REVIEWS:
        return <ProductReviewRow item={this.state.item} />;
      case AppConstants.detailItemType.OTHER:
        return <OtherShopRow idCategory={this.state.item?.category.id} />;
      case AppConstants.detailItemType.TITLE_SUGGEST:
        return (
          <Text
            style={{
              ...ProductStyle.text.textTitle,
              marginHorizontal: Styles.constants.marginHorizontalAll,
              marginTop: Styles.constants.marginTopAll,
            }}>
            {strings('suggested')}
          </Text>
        );
      case AppConstants.detailItemType.PRODUCT:
        return <SuggestRow itemList={item.data} />;
      case AppConstants.detailItemType.END:
        return <View style={{marginBottom: 10}} />;
      default:
        break;
    }
  };
  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <UIActivityIndicator color={ColorStyle.tabActive} size={20} />;
  };
  handleLoadMore() {
    if (this.state.loading) {
      return;
    }
    if (!this.state.canLoadData) {
      return;
    }
    this.setState(
      {loading: true, page: this.state.page + 1},
      this.getProductSuggestList,
    );
  }
  getProductDetail() {
    ProductHandle.getInstance().getProductDetail(
      this.props.item?.id,
      undefined,
      (isSuccess, dataResponse) => {
        if (isSuccess) {
          let data = dataResponse.data;
          console.log(data);
          this.setState({item: data});
          this.getStoreDetail(data.store.id);
        }
      },
    );
  }
  renderBuyView() {
    let data = this.state.item;
    return (
      <RBSheet
        customStyles={{
          container: {backgroundColor: '#F9F9F9', overflow: 'hidden'},
        }}
        keyboardAvoidingViewEnabled={false}
        height={Styles.constants.heightScreen / 3 + Styles.constants.X / 2}
        ref={ref => {
          this.sendBuyView = ref;
        }}
        openDuration={150}
        closeDuration={150}>
        <View
          style={{
            width: '100%',
            height: '100%',
            padding: Styles.constants.marginHorizontal20,
            backgroundColor: ColorStyle.tabWhite,
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image
              style={Styles.icon.img_product}
              source={{
                uri: ProductUtils.getImageUrl(data?.image_url, data?.index),
              }}
            />
            <View
              style={{
                marginLeft: 10,
                alignItems: 'flex-start',
                justifyContent: 'flex-end',
                height: Styles.constants.X * 1.9,
              }}>
              <Text
                style={{
                  ...Styles.text.text16,
                  color: ColorStyle.tabBlack,
                  width: '50%',
                  lineHeight: 25,
                }}
                numberOfLines={2}>
                {data?.name}
              </Text>
              <Text
                style={{
                  ...Styles.text.text10,
                  color: ColorStyle.tabActive,
                  marginTop: Styles.constants.X / 4,
                }}>
                {' '}
                {CurrencyFormatter(data?.price)}
              </Text>
              <Text
                style={{
                  ...Styles.text.text12,
                  color: ColorStyle.gray,
                  marginTop: Styles.constants.X / 4,
                }}>
                {strings('warehouse1')}:{data?.quantity}
              </Text>
            </View>
          </View>
          <QuantityControl
            minValue={1}
            showTitle={true}
            containerStyle={{
              backgroundColor: ColorStyle.tabWhite,
              height: 35,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
            maxValue={data?.quantity + 1}
            quantity={this.state.quantity}
            changeValue={quantity => {
              if (quantity === 0) {
              } else {
                this.updateProductInCart();
              }
            }}
          />
          <TouchableOpacity
            onPress={() => this.buyProduct()}
            style={{
              width: Styles.constants.widthScreenMg24,
              height: Styles.constants.X,
              alignSelf: 'center',
              marginTop: Styles.constants.X / 4,
              backgroundColor:
                this.state.buyType === BUY_TYPE.ADD_TO_CART
                  ? '#A7CDCC'
                  : ColorStyle.tabActive,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                ...Styles.text.text16,
                color: ColorStyle.tabWhite,
                fontWeight: '700',
              }}>
              {this.state.buyType === BUY_TYPE.ADD_TO_CART
                ? strings('addCart')
                : strings('buyNow')}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              position: 'absolute',
              right: 10,
              top: 10,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => this.sendBuyView.close()}>
            <Icon name={'close'} type={'evilicons'} size={20} />
          </TouchableOpacity>
        </View>
      </RBSheet>
    );
  }
  updateProductInCart() {
    this.setState({quantity: this.state.quantity + 1});
  }
  getProductSuggestList() {
    let param = {
      page_index: this.state.page,
      page_size: 10,
    };
    ProductHandle.getInstance().getProductList(
      param,
      (isSuccess, responseData) => {
        if (isSuccess) {
          if (responseData.data != null && responseData.data.data) {
            if (responseData.data.length < 10) {
              this.setState({canLoadData: false});
            }
            let listData = this.state.data;
            let abc = {
              data: responseData.data.data,
              type: AppConstants.detailItemType.PRODUCT,
            };
            listData = [...listData, ...[abc]];
            this.setState({
              data: listData,
              loading: false,
            });
          }
        } else {
          console.log('Failed');
        }
      },
      true,
    );
  }
  renderShowBuy(buyType) {
    FuncUtils.getInstance().callRequireLogin(() => {
      if (this.state.item.quantity === 0) {
        return SimpleToast.show(strings('notQuantity'), SimpleToast.SHORT);
      }
      this.setState({buyType, quantity: 1}, () => {
        this.sendBuyView.open();
      });
    });
  }
  buyProduct() {
    if (this.state.buyType === BUY_TYPE.ADD_TO_CART) {
      this.sendBuyView.open();
      this.addToCart(null, false);
    } else {
      let buyNow = [];
      let itemProduct = this.state.item;
      let product = {
        // ...this.state.item,
        name: itemProduct.name,
        id: itemProduct.id,
        image_url: itemProduct.image_url,
        price: itemProduct.price,
        ...itemProduct?.size,
        quantity: this.state.quantity,
        quantity_in_stock: itemProduct.total_sold,
      };
      let itemBuy = {
        store: {
          id: this.state.store.id,
          name: this.state.store.name,
          image_url: this.state.store.image_url,
          address: this.state.store.address,
          ship_unit_id: this.state.store.ship_unit_id,
        },
        products: [product],
        total: this.state.item.price * this.state.quantity,
      };
      buyNow.push(itemBuy);
      NavigationUtils.goToCheckout(buyNow, true);
      this.sendBuyView.close();
    }
  }
  getStoreDetail(storeId) {
    StoreHandle.getInstance().getStoreDetail(storeId, (isSuccess, store) => {
      if (isSuccess) {
        this.setState({store: store});
      }
    });
  }
  addToCart() {
    this.runAddCartAnim(() => {
      let params = {
        product_name: this.state.item.name,
        product_id: this.state.item.id,
        quantity: this.state.quantity,
        price: this.state.item.price,
        store_id: this.state.item.store.id,
      };
      CartHandle.getInstance().addToCart(params, (isSuccess, _) => {
        if (isSuccess) {
          SimpleToast.show(strings('addedToCart'));
          EventRegister.emit(AppConstants.EventName.LIST_CART);
          this.sendBuyView.close();
        } else {
          SimpleToast.show(strings('addedToCartFailed'));
        }
      });
    });
  }
  runAddCartAnim(callback) {
    this.state.scaleStartValue.addListener(({value}) => {
      if (value <= this.state.scaleEndValue) {
        this.setState(
          {
            scaleStartValue: new Animated.Value(1),
            translateStartXValue: new Animated.Value(0),
            translateStartYValue: new Animated.Value(0),
            showAnimView: false,
          },
          () => {
            this.setState({alphaStartValue: new Animated.Value(1)});
            callback();
          },
        );
      }
    });
    this.setState(
      {
        showAnimView: true,
      },
      () => {
        Animated.timing(this.state.scaleStartValue, {
          toValue: this.state.scaleEndValue,
          duration: this.state.duration,
        }).start();

        Animated.timing(this.state.translateStartXValue, {
          toValue: this.state.translateEndXValue,
          duration: this.state.duration,
        }).start();

        Animated.timing(this.state.translateStartYValue, {
          toValue: this.state.translateEndYValue,
          duration: this.state.duration,
        }).start();

        Animated.timing(this.state.alphaStartValue, {
          toValue: this.state.alphaEndValue,
          duration: this.state.duration,
        }).start();
      },
    );
  }
}
