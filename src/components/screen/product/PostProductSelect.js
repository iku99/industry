import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {CheckBox} from 'react-native-elements';
import ElevatedView from 'react-native-elevated-view';
import {strings} from '../../../resource/languages/i18n';
import ProductUtils from '../../../Utils/ProductUtils';
import CurrencyFormatter from '../../../Utils/CurrencyFormatter';
import MyFastImage from "../../elements/MyFastImage";
import Styles from "../../../resource/Styles";
import ColorStyle from "../../../resource/ColorStyle";

let checkPrice = -1;
let productPrice = 0;
export default class PostProductSelectItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: this.props.item,
      index: this.props.index,
      validPrice: false,
    };
  }

  render() {
    let item = this.state.item;

    let index = this.state.index;
    let checked = false;
    productPrice = item.original_price;
    if (item.checked) {
      checked = item.checked;
    }
    console.log(item)
    return (
      <ElevatedView elevation={2} style={{marginTop: 10, backgroundColor:ColorStyle.tabWhite,
        marginHorizontal:5,
        marginBottom: 5,
        paddingVertical: 10}}>
        <TouchableOpacity onPress={() => {}}>
          <TouchableOpacity
            style={{flexDirection: 'row', alignItems: 'center'}}
            onPress={() => this.onChangeSelect(!checked)}>
            <CheckBox
              checked={checked}
              containerStyle={{borderColor: 'transparent', padding: 0}}
              onPress={() => this.onChangeSelect(!checked)}
              iconType="ionicon"
              uncheckedIcon="ios-radio-button-off"
              checkedIcon="ios-radio-button-on"
              textStyle={{fontWeight: 'normal'}}
            />
            <Text>{strings('selectThis')}</Text>
          </TouchableOpacity>
          {/*<Text*/}
          {/*  style={{*/}
          {/*    paddingHorizontal: 10,*/}
          {/*    paddingVertical: 8,*/}
          {/*    // color: ProductUtils.getProductStatusColor(item),*/}
          {/*    fontSize: 17,*/}
          {/*  }}>*/}
          {/*  {`${strings('trangThaiSanPham')}${ProductUtils.getProductStatus(*/}
          {/*    item,*/}
          {/*  )}`}*/}
          {/*</Text>*/}

          <View onPress={() => {}}>
            <MyFastImage
                style={{ width:100,
                  height:100,
                  borderRadius:8,
                  overflow: 'hidden',
                  justifyContent: 'center',
                  alignItems: 'center',}}
              source={{
                uri: ProductUtils.getImages(item)[0],
                headers: {Authorization: 'someAuthToken'},
              }}
              resizeMode={'cover'}
            />
            <View
              style={{
                width:'80%',
                height: 100,
                justifyContent: 'center',
                marginHorizontal: 8,
              }}>
              <Text>{item.name}</Text>
              <Text>{`${strings('quantity')}: ${item.quantity}`}</Text>
              {/*<Text>{`${strings('SKU_Quantity', {quantity: 1})}`}</Text>*/}
            </View>
            <View style={{justifyContent: 'flex-end', alignItems: 'flex-end'}}>
              <Text>{CurrencyFormatter(item.price)}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </ElevatedView>
    );
  }

  onChangeSelect(checked) {
    let item = {
      ...this.state.item,
      checked,
    };
    this.onChangeDataCallback(item);
  }
  onChangeDataCallback(item) {
    this.setState({item}, () => {
      if (this.props.onChangeDataCallback !== undefined) {
        this.props.onChangeDataCallback(item, this.state.index);
      }
    });
  }
}
