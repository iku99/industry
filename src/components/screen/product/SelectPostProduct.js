import React, {Component} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Icon} from 'react-native-elements';
import SimpleToast from 'react-native-simple-toast';
import {strings} from '../../../resource/languages/i18n';
import Ripple from 'react-native-material-ripple';
import ViewUtils from '../../../Utils/ViewUtils';
import ProductHandle from '../../../sagas/ProductHandle';
import GlobalInfo from '../../../Utils/Common/GlobalInfo';
import GroupHandle from '../../../sagas/GroupHandle';

let LIMIT = 20;
export default class SelectPostProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
      page: 0,
      refreshing: false,
      canLoadData: true,
      posting: false,
    };
  }
  componentDidMount() {
    setTimeout(() => this.getProductList(), 300);
  }
  render() {
    return (
      <View>
        <View>
          <Text>{strings('selectProductToPost')}</Text>
          {/*<FlatList*/}
          {/*  contentContainerStyle={{width: '100%'}}*/}
          {/*  showsVerticalScrollIndicator={false}*/}
          {/*  data={this.state.data}*/}
          {/*  keyExtractor={this.keyExtractor}*/}
          {/*  renderItem={this._renderItem}*/}
          {/*  style={{flex: 1, backgroundColor: 'transparent', marginTop: 10}}*/}
          {/*  ListFooterComponent={this.renderFooter.bind(this)}*/}
          {/*  onEndReachedThreshold={0.4}*/}
          {/*  onEndReached={() => this.handleLoadMore()}*/}
          {/*  ref={ref => {*/}
          {/*    this.listRef = ref;*/}
          {/*  }}*/}
          {/*  refreshing={this.state.refreshing}*/}
          {/*/>*/}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
            }}>
            <View style={{flex: 1, marginHorizontal: 10}}>
              <Ripple
                onPress={() => {
                  this.closeView();
                }}
                style={{marginTop: 10}}
                animation="fadeInUpBig">
                <Text style={{fontSize: 14}}>
                  {strings('addProductCancel')}
                </Text>
              </Ripple>
            </View>
            <View style={{flex: 1, marginHorizontal: 10}}>
              <Ripple
                onPress={() => {
                  this.onPost();
                }}
                style={{marginTop: 10}}
                animation="fadeInUpBig">
                <Text style={{color: 'white', fontSize: 14}}>
                  {strings('addProductDone')}
                </Text>
              </Ripple>
            </View>
          </View>
        </View>

        <TouchableOpacity
          onPress={() => {
            this.closeView();
          }}>
          <Icon name="close" type="font-awesome" size={20} />
        </TouchableOpacity>
        {ViewUtils.renderLoading(this.state.posting, true)}
      </View>
    );
  }
  keyExtractor = (item, index) => `selectPostProduct_${index.toString()}`;
  _renderItem = ({item, index}) => <View>{this.renderItem(item, index)}</View>;
  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <ActivityIndicator style={{color: '#000'}} />;
  };
  renderItem(item, index) {
    return (
      <PostProductItem
        index={index}
        item={item}
        onChangeDataCallback={(item, index) => {
          let data = this.state.data;
          data[index] = item;
          this.setState({data});
        }}
      />
    );
  }
  handleLoadMore() {
    if (!this.state.canLoadData) {
      return;
    }
    if (this.state.loading) {
      return;
    }
    this.setState({loading: true, page: this.state.page + 1}, () =>
      this.getProductList(),
    );
  }
  getProductList() {
    let param = {
      current_page: this.state.page,
      page_size: LIMIT,
      store_id: GlobalInfo.userInfo.store_id,
    };
    ProductHandle.getInstance().getProductList(
      param,
      (isSuccess, responseData) => {
        if (isSuccess) {
          if (responseData.data != null && responseData.data.list) {
            let data = this.state.data;
            data = data.concat(responseData.data.list);
            let canLoadData = responseData.data.list.length === LIMIT;
            this.setState({data: data, canLoadData});
          }
        }
        this.setState({loading: false});
      },
    );
  }
  closeView() {
    if (this.props.onClose !== undefined) {
      this.props.onClose();
    }
  }
  onPost() {
    let data = this.state.data;
    let postProduct = [];
    let size = data.length;
    let valid = true;
    for (let i = 0; i < size; i++) {
      let item = data[i];
      if (item.checked) {
        if (item.validPrice === undefined || !item.validPrice) {
          SimpleToast.show(strings('dataInvalid'), SimpleToast.SHORT);
          valid = false;
          break;
        }
        postProduct.push({
          product_id: item.id,
          price: Number(item.sharePrice),
        });
      }
    }
    if (valid && postProduct.length > 0) {
      this.onPostProduct(postProduct);
    } else {
      SimpleToast.show(
        strings('selectAtLeast1ProductToPost'),
        SimpleToast.SHORT,
      );
    }
  }
  onPostProduct(postProduct) {
    this.setState({posting: true}, () => {
      GroupHandle.getInstance().postProductToGroup(
        this.props.groupId,
        postProduct,
        (isSuccess, dataResponse) => {
          this.setState({posting: false});
          if (isSuccess) {
            ViewUtils.showAlertDialog(strings('postProductSuccess'), () => {
              this.closeView();
              if (this.props.onPostProduct !== undefined) {
                this.props.onPostProduct(postProduct);
              }
            });
          } else {
            ViewUtils.showAlertDialog(strings('postProductFailed'), null);
          }
        },
      );
    });
  }
}
