import React, {Component} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ProductStyle from './ProductStyle';
import {Icon} from 'react-native-elements';
import Ripple from 'react-native-material-ripple';
import DataUtils from '../../../Utils/DataUtils';
import {strings} from '../../../resource/languages/i18n';
import ViewUtils from '../../../Utils/ViewUtils';
import PostProductSelectItem from './PostProductSelect';
import ProductHandle from '../../../sagas/ProductHandle';

let LIMIT = 20;
export default class SelectTagProductGroup extends Component {
  constructor(props) {
    super(props);
    let loadNew = true;
    let oldState = this.props.selectProductState;
    if (oldState === undefined || DataUtils.listNullOrEmpty(oldState.data)) {
      this.state = {
        data: [],
        loading: true,
        page: 0,
        refreshing: false,
        canLoadData: true,
        posting: false,
      };
    } else {
      loadNew = false;
      this.state = oldState;
    }
    if (loadNew) {
      setTimeout(() => this.getProductList(), 300);
    }
  }
  render() {
    return (
      <View>
        <View>
          <Text style={{fontWeight: '600', fontSize: 17}}>
            {strings('selectProductToTag')}
            <Text
              style={{fontWeight: 'normal', fontStyle: 'italic', fontSize: 14}}>
              {strings('selectProductToTagDes')}
              <Text
                onPress={() => {
                  if (this.props.onSelectProduct !== undefined) {
                    this.props.onSelectProduct();
                  }
                  this.closeView();
                }}
                style={ProductStyle.buttonText}>
                {strings('postProductNow')}
              </Text>
            </Text>
          </Text>
          {this.state.data.length > 0 && (
            <FlatList
              contentContainerStyle={{width: '100%'}}
              showsVerticalScrollIndicator={false}
              data={this.state.data}
              keyExtractor={this.keyExtractor}
              renderItem={this._renderItem}
              style={{flex: 1, backgroundColor: 'transparent', marginTop: 10}}
              ListFooterComponent={this.renderFooter.bind(this)}
              onEndReachedThreshold={0.4}
              onEndReached={() => this.handleLoadMore()}
              ref={ref => {
                this.listRef = ref;
              }}
              refreshing={this.state.refreshing}
            />
          )}
          {/*{this.state.data.length === 0 && (*/}
          {/*  <View style={ProductStyle.emptyView}>*/}
          {/*    <Image*/}
          {/*      source={ImageHelper.emptyBox}*/}
          {/*      style={RemindStyle.emptyImage}*/}
          {/*    />*/}
          {/*    <Text style={ProductStyle.emptyText}>*/}
          {/*      {strings('youNotYetPostAnyProduct')}*/}
          {/*      <Text*/}
          {/*        onPress={() => {*/}
          {/*          if (this.props.onSelectProduct !== undefined) {*/}
          {/*            this.props.onSelectProduct();*/}
          {/*          }*/}
          {/*          this.closeView();*/}
          {/*        }}*/}
          {/*        style={ProductStyle.buttonText}>*/}
          {/*        {strings('postNow')}*/}
          {/*      </Text>*/}
          {/*    </Text>*/}
          {/*  </View>*/}
          {/*)}*/}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              alignItems: 'center',
            }}>
            <View style={{flex: 1, marginHorizontal: 10}}>
              <Ripple
                onPress={() => {
                  this.closeView();
                }}
                style={{marginTop: 10}}
                animation="fadeInUpBig">
                <Text style={{fontSize: 14}}>
                  {strings('addProductCancel')}
                </Text>
              </Ripple>
            </View>
            <View style={{flex: 1, marginHorizontal: 10}}>
              <Ripple
                onPress={() => {
                  this.onTag();
                }}
                style={{marginTop: 10}}
                animation="fadeInUpBig">
                <Text style={{color: 'white', fontSize: 14}}>
                  {strings('addProductDone')}
                </Text>
              </Ripple>
            </View>
          </View>
        </View>

        <TouchableOpacity
          style={ProductStyle.closeButton}
          onPress={() => {
            this.closeView();
          }}>
          <Icon name="close" type="font-awesome" size={20} />
        </TouchableOpacity>
        {ViewUtils.renderLoading(this.state.posting, true)}
      </View>
    );
  }
  keyExtractor = (item, index) => `selectTagProduct_${index.toString()}`;
  _renderItem = ({item, index}) => <View>{this.renderItem(item, index)}</View>;
  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <ActivityIndicator style={{color: '#000'}} />;
  };
  renderItem(item, index) {
    return (
      <PostProductSelectItem
        index={index}
        item={item}
        onChangeDataCallback={(item, index) => {
          let data = this.state.data;
          data[index] = item;
          this.setState({data});
        }}
      />
    );
  }
  handleLoadMore() {
    if (!this.state.canLoadData) {
      return;
    }
    if (this.state.loading) {
      return;
    }
    this.setState({loading: true, page: this.state.page + 1}, () =>
      this.getProductList(),
    );
  }
  getProductList() {
    let param = {
      type: 'own',
      current_page: this.state.page,
      page_size: LIMIT,
      group_id: this.props.groupId,
    };
    ProductHandle.getInstance().getProductList(
      param,
      (isSuccess, responseData) => {
        if (isSuccess) {
          if (responseData.data != null && responseData.data.list) {
            let data = this.state.data;
            let newData = responseData.data.list;
            newData.forEach(product => {});
            data = data.concat(newData);
            let canLoadData = newData.length === LIMIT;
            this.setState({data: data, canLoadData});
          }
        }
        this.setState({loading: false});
      },
    );
  }
  closeView() {
    if (this.props.onClose !== undefined) {
      this.state = this.props.selectProductState;
      this.props.onClose();
    }
  }
  onTag() {
    let data = this.state.data;
    let selectedData = [];
    data.forEach(product => {
      if (product.checked) {
        selectedData.push(product);
      }
    });
    if (this.props.onTag !== undefined) {
      this.props.onTag(selectedData, this.state);
    }
  }
}
