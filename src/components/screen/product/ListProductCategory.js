import React, {Component} from 'react';
import {
    FlatList, Image,
    RefreshControl,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import ProductHandle from '../../../sagas/ProductHandle';
import Styles from '../../../resource/Styles';
import ToolbarMain from '../../elements/toolbar/ToolbarMain';
import {strings} from '../../../resource/languages/i18n';
import {Icon, SearchBar} from 'react-native-elements';
import ColorStyle from '../../../resource/ColorStyle';
import {UIActivityIndicator} from 'react-native-indicators';
import AppConstants from '../../../resource/AppConstants';
import SuggestRow from '../main/home/row/SuggestRow';
import NavigationUtils from "../../../Utils/NavigationUtils";
import {Actions} from "react-native-router-flux";
import EmptyView from "../../elements/reminder/EmptyView";
const defaulData = [
  {
    type: AppConstants.productListCategory.END,
  },
];
export default class ListProductCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      category: this.props.node,
      page: 1,
      loading: false,
      canLoadData: false,
      selectedFilter: 0,
      priceUp: true,
    };
  }
  componentDidMount() {
    this.setState({data: defaulData}, () => {
      this.getViewedProduct();
    });
  }

  render() {
    return (
      <View style={Styles.container}>
        <ToolbarMain title={this.state.category?.name} iconBack={true} iconShopping={true} callbackShopping={()=>
          NavigationUtils.goToCart()}/>
          {this.renderSearch()}
          {this.renderTabFilter()}
          <FlatList
              showsVerticalScrollIndicator={false}
              horizontal={false}
              data={this.state.data}
              keyExtractor={this.keyExtractor}
              renderItem={this._renderItem}
              style={{flex: 1, height: '100%'}}
              ListFooterComponent={this.renderFooter.bind(this)}
              onEndReachedThreshold={0.4}
              onEndReached={() => this.handleLoadMore()}
              removeClippedSubviews={true} // Unmount components when outside of window
              windowSize={10}
          />
      </View>
    );
  }
  keyExtractor = (item, index) => `listProductCategory_${index.toString()}`;
  _renderItem = ({item}) => {
      switch (item.type) {
          case AppConstants.productListCategory.PRODUCT:
              if(item.data.length===0){
                  return <EmptyView
                      containerStyle={{marginTop:Styles.constants.X}}
                      text={strings('notProduct')}
                  />
              }
              return (
                  <SuggestRow
                      itemList={item.data}
                      showLoading={loading => this.setState({loading})}
                  />
              );
          case AppConstants.productListCategory.END:
              return <View style={{marginBottom: 10}} />;
          default:
              break;
      }
  }
  renderSearch(){
        return(
            <TouchableOpacity
                style={{...Styles.search.searchInput,
                     shadowColor: ColorStyle.borderItemHome,
                    shadowOffset: {width: 0, height: 1},
                    shadowOpacity: 0.8,
                    shadowRadius: 1,
                    elevation: 2,
                    width:Styles.constants.widthScreenMg24,
                    alignSelf:'center'}}
                onPress={() => Actions.jump('searchScreen')}>
                <TouchableOpacity
                    onPress={() => {
                        // NavigationUtils.goToSearchProduct();
                    }}>
                    <Icon name={'search'} type={'evilicons'} size={25} color={'#a59999'}/>
                </TouchableOpacity>
                <Text
                    style={Styles.search.searchText}
                    underlineColorAndroid="transparent">
                    {strings('textSearch')}
                </Text>
            </TouchableOpacity>
        )
    }
  renderTabFilter() {
    let isSelected = this.state.selectedFilter;
    return (
      <View
        style={{
          width: Styles.constants.widthScreenMg24,
          alignSelf: 'center',
          height: 50,
          borderBottomWidth: 1,
          borderBottomColor: ColorStyle.borderItemHome,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => {
           setTimeout(() => {
               this.setState({selectedFilter: 0,page:1,data:[]},this.getViewedProduct)
            }, 1000)
          }}
          style={{
            flex: 1,
            alignItems: 'center',
            backgroundColor:
              isSelected === 0
                ? ColorStyle.tabActive
                : ColorStyle.backgroundScreen,
            paddingVertical: 5,
            paddingHorizontal: 2,
            borderRadius: 20,
          }}>
          <Text
            style={{textAlign:'center',
              color:
                isSelected === 0 ? ColorStyle.tabWhite : ColorStyle.tabBlack,
              fontWeight: isSelected === 0 ? '700' : '400',
            }}>
            {strings('all')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flex: 1,
            alignItems: 'center',
            backgroundColor:
              isSelected === 1
                ? ColorStyle.tabActive
                : ColorStyle.backgroundScreen,
            paddingVertical: 5,
            paddingHorizontal: 2,
            borderRadius: 20,
          }}
          onPress={() => {
            setTimeout(() => {
                this.setState({selectedFilter: 1,page:1,data:[]},this.getViewedProduct)
            }, 1000)
          }}>
          <Text
            style={{
              color:
                isSelected === 1 ? ColorStyle.tabWhite : ColorStyle.tabBlack,textAlign:'center',
              fontWeight: isSelected === 1 ? '700' : '400',
            }}>
            {strings('news')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flex: 1,
            alignItems: 'center',
            backgroundColor:
              isSelected === 2
                ? ColorStyle.tabActive
                : ColorStyle.backgroundScreen,
            paddingVertical: 5,
            paddingHorizontal: 2,
            borderRadius: 20,
          }}
          onPress={() => {
            setTimeout(() => {
                this.setState({selectedFilter: 2,page:1,data:[]},this.getViewedProduct);
            }, 1000);

          }}>
          <Text
            style={{textAlign:'center',
              color:
                isSelected === 2 ? ColorStyle.tabWhite : ColorStyle.tabBlack,
              fontWeight: isSelected === 2 ? '700' : '400',
            }}>
            {strings('selling')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            setTimeout(() => {
                this.setState({selectedFilter: 3,page:1, priceUp: !this.state.priceUp,data:[]},this.getViewedProduct);
            }, 1000);

          }}
          style={{
            flex: 1,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor:
              isSelected === 3
                ? ColorStyle.tabActive
                : ColorStyle.backgroundScreen,
            paddingVertical: 5,
            paddingHorizontal: 2,
            borderRadius: 20,
          }}>
          <Text
            style={{textAlign:'center',
              color:
                isSelected === 3 ? ColorStyle.tabWhite : ColorStyle.tabBlack,
              fontWeight: isSelected === 3 ? '700' : '400',
            }}>
            {strings('price')}
          </Text>
          <Icon
            name={this.state.priceUp ? 'arrowdown' : 'arrowup'}
            size={18}
            type={'antdesign'}
            color={isSelected === 3 ? ColorStyle.tabWhite : ColorStyle.tabBlack}
          />
        </TouchableOpacity>
      </View>
    );
  }
  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <UIActivityIndicator color={ColorStyle.tabActive} size={20} />;
  };
  handleLoadMore() {
    if (this.state.loading) return;
    if (!this.state.canLoadData) return;

    this.setState(
      {loading: true, page: this.state.page + 1},
      this.getViewedProduct,
    );
  }
  getViewedProduct() {
   // this.setState({data: []});
    let param = {
      page_size: 10, page_index: this.state.page,category_id:this.state.category?.id
    };
      if (this.state.selectedFilter === 1) {
          param = {
              ...param,
          };
      }
    if (this.state.selectedFilter === 2) {
      param = {
        ...param,
        type: 'best-seller',
      };
    }
    if (this.state.selectedFilter === 3) {
        param = {
            ...param,
            columnName:'Price',
            isDesc: this.state.priceUp ? true : false,
        };
    }
    ProductHandle.getInstance().getProductCategoryList(
      param,
      (isSuccess, responseData) => {
          console.log(responseData)
        if (isSuccess) {
          if (responseData.data != null) {

            if(responseData.data.length<10){
                this.setState({canLoadData:false})
            }
              let listData = this.state.data;
              let abc = {
                  data: responseData.data.data,
                  type: AppConstants.productListCategory.PRODUCT,
              };
            listData = [...listData, ...[abc]];

            this.setState({data: listData,loading:false});
          }
        } else {
          console.log('Failed');
        }
      },true
    );
  }
}
