import React, {Component} from 'react';
import {
  Animated,
  FlatList,
  Image,
  ScrollView,
  Share,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';
import AppConstants from '../../../resource/AppConstants';
import RatingHandle from '../../../sagas/RatingHandle';
export default class ProductRatingPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ratingList: [],
    };
  }
  componentDidMount() {
    this.getRatingProduct(0);
  }

  render() {
    // let ratingList =
    //   this.state.ratingList !== undefined &&
    //   this.state.ratingList.map((item, index) => {
    //     return (
    //       <View style={RatingStyle.ratingContainer}>
    //         <TouchableOpacity style={RatingStyle.contentComment}>
    //           <View style={RatingStyle.commentItem}>
    //             <Text style={RatingStyle.ratingName}>
    //               {RatingUtils.getRatingUserName(item)}
    //             </Text>
    //             <View
    //               style={{
    //                 flexDirection: 'row',
    //                 justifyContent: 'space-between',
    //                 width: '100%',
    //               }}>
    //               <RatingStar
    //                 size={12}
    //                 style={{marginTop: 5, marginBottom: 5}}
    //                 rating={RatingUtils.getRating(item)}
    //               />
    //               <Text style={RatingStyle.dateText}>{`${DateUtil.trimDate(
    //                 item.created_at,
    //               )}`}</Text>
    //             </View>
    //             <Text style={RatingStyle.ratingComment}>
    //               {RatingUtils.getRatingUserComment(item)}
    //             </Text>
    //           </View>
    //           <TouchableOpacity style={RatingStyle.thankBT}>
    //             <Text
    //               style={{paddingHorizontal: 10, color: RatingStyle.likeColor}}>
    //               {strings('thankComment')}
    //             </Text>
    //             <Icon
    //               name={'thumbs-up'}
    //               type={'feather'}
    //               size={15}
    //               color={RatingStyle.likeColor}
    //             />
    //           </TouchableOpacity>
    //         </TouchableOpacity>
    //         <MyFastImage
    //           style={RatingStyle.avatar}
    //           source={{
    //             uri: RatingUtils.getRatingUserAvatarUrl(item),
    //             headers: {Authorization: 'someAuthToken'},
    //           }}
    //           resizeMode={'cover'}
    //         />
    //       </View>
    //     );
    //   });
    return (
      <View style={{paddingTop: 10}}>
        {/*{ratingList}*/}
        {/*<PagenationControl*/}
        {/*  pageCount={this.props.pageCount}*/}
        {/*  onPageChange={page => {*/}
        {/*    this.getRatingProduct(page);*/}
        {/*  }}*/}
        {/*/>*/}
      </View>
    );
  }
  getRatingProduct(page) {
    let params = {
      current_page: page,
      page_size: AppConstants.LIMIT_COMMENT,
      sort_by: 'id',
      sort_order: 'desc',
    };
    RatingHandle.getInstance().getProductRating(
      this.props.id,
      params,
      (isSuccess, dataResponse) => {
        let ratingList = [];
        if (isSuccess) {
          console.log(dataResponse);
          this.setState({ratingList: dataResponse.data.list});
        }
      },
    );
  }
}
