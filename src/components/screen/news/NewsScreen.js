import React, {Component} from 'react';
import {AsyncStorage, FlatList, View} from 'react-native';
import Styles from '../../../resource/Styles';
import Toolbar from '../../elements/toolbar/Toolbar';
import {strings} from '../../../resource/languages/i18n';
import ColorStyle from '../../../resource/ColorStyle';
import EmptyView from '../../elements/reminder/EmptyView';
import NavigationUtils from '../../../Utils/NavigationUtils';
import {DotIndicator} from 'react-native-indicators';
import NewsHandle from "../../../sagas/NewsHandle";
import NewsItem from "../../elements/viewItem/NewsItem/NewsItem";
import AppConstants from "../../../resource/AppConstants";
import AppBannerRow from "../main/home/row/AppBannerRow";
let LIMIT = 1;
let groupPosts = [];
let groupPostRef = [];
const Data=[
  {
    type:AppConstants.listNewType.BANNER,
  },
]
export default class NewsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: Data,
      page: 1,
      refreshing: false,
      showFloatingButton: false,
      canLoadData: true,
      finishLoad: false,
      isLogged:false
    };
  }
  componentDidMount() {
    AsyncStorage.getItem(
      AppConstants.SharedPreferencesKey.IsLogin,
      (error, isLogin) => {
        this.setState({isLogged: isLogin === '1'});
      },
    );
    this.getGroupPostData();
  }
  getGroupPostData() {
    let param = {
      page_index: this.state.page,
      page_size: LIMIT,
      type:AppConstants.getNews.NEWS
    };
    NewsHandle.getInstance().getNews(
      param,
      (isSuccess, dataResponse) => {
        console.log(dataResponse)
        let canLoadData = false;
        if (
          isSuccess &&
          dataResponse.data !== undefined
        ) {
          let mData=this.state.data;
          if (dataResponse.data.data.length < LIMIT) {
            this.setState({canLoadData: false});
            return
          }
          let abc = {
            data: dataResponse.data.data,
            type: AppConstants.listNewType.PRODUCT,
          };
          mData = [...mData, ...[abc]];
          this.setState({data: mData, loading: false});
        }


        this.setState({loading: false, canLoadData}, () => {
          if (this.state.page === 0 && !canLoadData) {
          }
        });
      },this.state.isLogged
    );
  }
  keyExtractor = (item, index) => `news_list_${index.toString()}`;
  render() {
    return (
      <View style={Styles.container}>
        <Toolbar
          title={strings('news')}
          iconBack={true}
          backgroundColor={ColorStyle.tabActive}
          textColor={ColorStyle.tabWhite}
        />

        {this.state.data.length === 0 ? (
          <EmptyView text={strings('newEmptyPost')} />
        ) : (
          <FlatList

            showsVerticalScrollIndicator={false}
            data={this.state.data}
            keyExtractor={this.keyExtractor}
            renderItem={this._renderItem}
            style={{
              width: '100%',
              height: '95%',
            }}
            ListFooterComponent={this.renderFooter.bind(this)}
            onEndReachedThreshold={0.4}
            onEndReached={() => this.handleLoadMore()}
            ref={ref => {
              this.listRef = ref;
            }}
            refreshing={this.state.refreshing}
          />
        )}
      </View>
    );
  }
  renderItem = ({item, index}) => {
    return (
      <NewsItem
        index={index}
        item={item}
        groupName={'this.props.item.name'}
        ref={ref => (groupPostRef[index] = ref)}
        onGoToPostDetail={() => {
          NavigationUtils.goToNewsDetail(
            item,
            item.id,
            index,
            (item, index) => {groupPostRef[index].changeDataItem(item);
              // this.changeItem(item, index, true);
            },
          );
        }}
      />
    );
  };
  renderList(data){
    return(
      <FlatList
        showsHorizontalScrollIndicator={false}
        data={data}
        contentContainerStyle={{ width: '100%'}}
        style={{alignItems: 'center',width:Styles.constants.widthScreen,marginTop:10}}
        keyExtractor={this.keyExtractor}
        renderItem={this.renderItem}
      />
    )
  }
  _renderItem = ({item, index}) => {
    switch (item.type) {
      case AppConstants.listNewType.BANNER:
        return <AppBannerRow
          type={AppConstants.BANNER_POSITION.NEWS}
          containerStyle={{
            maxHeight: (Styles.constants.heightScreen ) / 7,
          }}
          backgroundStyle={{height:  (Styles.constants.heightScreen ) / 7}}
        />
      case AppConstants.listNewType.PRODUCT:
        return this.renderList(item.data)
      case AppConstants.listNewType.END:
        return <View style={{marginBottom: 10}} />;

      default:
        return null;
    }
  };
  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <DotIndicator color={ColorStyle.tabWhite} size={10} />;
  };
  handleLoadMore() {
    if (this.state.loading) {
      return;
    }
    if (this.state.canLoadData) {
      this.setState({loading: true, page: this.state.page + 1}, () =>
        this.getGroupPostData,
      );
    }
  }
}
