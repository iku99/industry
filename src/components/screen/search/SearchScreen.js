import React, {Component} from 'react';
import {
  FlatList,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Styles from '../../../resource/Styles';
import {strings} from '../../../resource/languages/i18n';
import ColorStyle from '../../../resource/ColorStyle';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import NavigationUtils from '../../../Utils/NavigationUtils';
import BadgeView from '../../elements/BadgeView';
import ProductHandle from '../../../sagas/ProductHandle';
import DataUtils from '../../../Utils/DataUtils';
import {DotIndicator} from 'react-native-indicators';
import EmptyView from '../../elements/reminder/EmptyView';
import AppConstants from '../../../resource/AppConstants';
import MyFastImage from "../../elements/MyFastImage";
import ViewProductStyle from "../../elements/viewItem/ViewedProduct/ViewProductStyle";
import constants from "../../../Api/constants";
import CurrencyFormatter from "../../../Utils/CurrencyFormatter";
import SearchStyles from "./SearchStyles";
import {TextInputMask} from "react-native-masked-text";
import CategoryHandle from "../../../sagas/CategoryHandle";
export default class SearchScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      sortSelectedIndex: undefined,
      resultCount: 0,
      finishSearch: false,
      data: [],
      loading: false,
      canLoadData: true,
      page: 0,
        showFilter:false,
        priceStart:undefined,
        priceLater:undefined,
        selectPrice:0,

        //
        categories:[],
        selectCategory:-1,
    };
  }

  componentDidMount() {
      this.getChildCategory()
  }

  render() {
    return (
      <View style={Styles.container}>
        {this.toolbar()}
        {this.renderTextInputSearch()}
        {this.renderContain()}
          {this.state.showFilter &&(this.renderFilter())}
      </View>
    );
  }
  toolbar() {
    return (
      <View
        style={{
          width: Styles.constants.widthScreen,
          paddingTop: Styles.constants.X,
          paddingHorizontal: Styles.constants.marginHorizontal20,
          alignSelf: 'center',
          backgroundColor: ColorStyle.tabActive,
          paddingBottom: 10,
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={() => Actions.pop()}>
            <Icon
              name="left"
              type="antdesign"
              size={25}
              color={ColorStyle.tabWhite}
            />
          </TouchableOpacity>
          <Text
            style={[
              Styles.text.text20,
              {
                color: ColorStyle.tabWhite,
                fontWeight: '700',
                top: 0,
                maxWidth: '60%',
                textAlign: 'center',
              },
            ]}
            numberOfLines={1}>
            {strings('search')}
          </Text>
          <TouchableOpacity
            onPress={() => NavigationUtils.goToCart()}>
              <Icon
                  name="shopping-cart"
                  style="entypo"
                  size={25}
                  color={ColorStyle.tabWhite}
              />
            <BadgeView />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  renderTextInputSearch() {
    return (
      <View
        style={{
          width: Styles.constants.widthScreenMg24,
          alignSelf: 'center',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
        }}>
        <View
          style={{
            width: Styles.constants.widthScreenMg24 - Styles.constants.X,
              paddingVertical:10,
            backgroundColor: ColorStyle.tabWhite,
                elevation: 2,
              borderRadius:5,
              shadowColor: '#a4a2a2',
              shadowOffset: { width: 2, height: 2 },
              shadowOpacity: 0.8,
              shadowRadius: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <TextInput
            style={{
              alignSelf: 'center',
              width: '85%',
              backgroundColor: ColorStyle.tabWhite,
              paddingLeft: 20,
            }}
            placeholder={strings('hintSearchProduct')}
            value={this.state.searchText}
            keyboardType={'email-address'}
            placeholderTextColor={ColorStyle.textInput}
            onChangeText={text => {
              this.setState({searchText: text});
            }}
          />
          <TouchableOpacity
            style={{width: '15%'}}
            onPress={() => this.searchProduct()}>
            <Icon
              name={'search'}
              type={'evilicons'}
              size={25}
              color={ColorStyle.tabBlack}
            />
          </TouchableOpacity>
        </View>

        <TouchableOpacity onPress={()=>{this.setState({showFilter:true})}} style={{width: Styles.constants.X}}>
          <Icon
            name={'filter'}
            type={'feather'}
            size={23}
            color={ColorStyle.tabBlack}
          />
        </TouchableOpacity>
      </View>
    );
  }
  renderFilter(){
      return(
         <View style={SearchStyles.containerFilter}>
             <TouchableOpacity style={SearchStyles.containerView_not}  onPress={()=>{this.setState({showFilter:false})}}></TouchableOpacity>
             <View style={SearchStyles.bodyFilter}>
                 <View style={SearchStyles.headerView}>
                    <Text style={SearchStyles.textTitleHeader}>{strings('filterTitle')}</Text>
                 </View>
                 <View >
                     {this.renderPrice()}
                     <View style={{width:'100%',height:1,backgroundColor:ColorStyle.gray}}/>
                     {this.renderCategory()}
                     <View style={{width:'100%',height:1,backgroundColor:ColorStyle.gray}}/>

                 </View>
                 <View style={{position:'absolute',bottom:10,flexDirection:'row',width:Styles.constants.widthScreen-Styles.constants.X*2}}>
                     <TouchableOpacity onPress={()=>this.setState({showFilter:false})} style={{...SearchStyles.btnCategory,backgroundColor:ColorStyle.tabWhite}}>
                         <Text style={{color:ColorStyle.tabBlack}}>{strings('cancel')}</Text>
                     </TouchableOpacity>
                     <TouchableOpacity onPress={()=>this.filterProduct()} style={{...SearchStyles.btnCategory,backgroundColor:ColorStyle.tabActive}}>
                         <Text style={{color:ColorStyle.tabWhite}}>{strings('filter')}</Text>
                     </TouchableOpacity>

                 </View>
             </View>
         </View>
      )
  }
    filterProduct(){
      let params={
          price_start:this.state.priceStart,
          price_end:this.state.priceLater,
      }
      if(this.state.selectCategory!==-1){
          params={
              ...params,
              category_id:this.state.selectCategory
          }
      }
        ProductHandle.getInstance().FilterProduct(
            params,
            (isSuccess, responseData, msg) => {
                console.log(responseData)
                let resultCount = responseData.data.page_info?.total_items;
                let canLoadData = responseData.data.length === 20;
                this.setState({
                    data: responseData.data.data,
                    canLoadData,
                    finishSearch: true,
                    showFilter:false,
                    resultCount,
                });
            })
    }
    renderPrice(){
      let selectPrice=this.state.selectPrice;
      return(
          <View style={SearchStyles.bodyList}>
              <Text style={SearchStyles.textTitle}>{strings('price_range')}:</Text>
              <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                  <TextInputMask
                      type={'money'}
                      placeholder={strings('priceStart').toUpperCase()}
                      style={{
                          paddingVertical: 10,
                          width:'45%',
                          paddingLeft: 10,
                          marginVertical: 8,
                          borderWidth: 1,
                          borderColor: ColorStyle.gray,
                      }}
                      value={this.state.priceStart}
                      onChangeText={text => {
                          this.setState({price: DataUtils.trimNumber(text)});
                      }}
                      options={{
                          precision: 0,
                          separator: '.',
                          delimiter: ',',
                          unit: '',
                          suffixUnit: '',
                      }}
                      ref={ref => (this.priceTxt = ref)}
                  />
                  <Icon name={'arrowright'} type={'antdesign'} size={20} color={ColorStyle.tabActive}/>
                  <TextInputMask
                      type={'money'}
                      placeholder={strings('priceEnd').toUpperCase()}
                      style={{
                          width:'45%',
                          paddingVertical: 10,
                          paddingLeft: 10,
                          marginVertical: 8,
                          borderWidth: 1,
                          borderColor: ColorStyle.gray,
                      }}
                      value={this.state.priceLater}
                      onChangeText={text => {
                          this.setState({price: DataUtils.trimNumber(text)});
                      }}
                      options={{
                          precision: 0,
                          separator: '.',
                          delimiter: ',',
                          unit: '',
                          suffixUnit: '',
                      }}
                      ref={ref => (this.priceTxt = ref)}
                  />
              </View>
              <View style={{flexDirection:'row',alignItems:'center',marginTop:10,justifyContent:'space-between'}}>
                  <TouchableOpacity onPress={()=>{this.clickPrice(0,500000,1) }} style={{...SearchStyles.btnPrice,backgroundColor:selectPrice!==1?ColorStyle.optionTextColor:ColorStyle.tabActive}}><Text style={{color:ColorStyle.tabWhite}}>0-500k</Text></TouchableOpacity>
                  <TouchableOpacity  onPress={()=>{this.clickPrice(500000,1000000,2)}} style={{...SearchStyles.btnPrice,backgroundColor:selectPrice!==2?ColorStyle.optionTextColor:ColorStyle.tabActive}}><Text style={{color:ColorStyle.tabWhite}}>500K-1tr</Text></TouchableOpacity>
                  <TouchableOpacity  onPress={()=>{this.clickPrice(1000000,10000000,3)}} style={{...SearchStyles.btnPrice,backgroundColor:selectPrice!==3?ColorStyle.optionTextColor:ColorStyle.tabActive}}><Text style={{color:ColorStyle.tabWhite}}>1tr -10tr</Text></TouchableOpacity>
              </View>
          </View>
      )
    }
    clickPrice(start,end,type){
      this.setState({priceStart:start,priceLater:end,selectPrice:type})
    }
    renderCategory(){
      return(
          <View style={SearchStyles.bodyList}>
              <Text style={SearchStyles.textTitle}>{strings('or_category')}:</Text>
              <FlatList
                  showsVerticalScrollIndicator={false}
                  data={DataUtils.formatCategory(this.state.categories)}
                  numColumns={2}
                  style={{width:'100%'}}
                  keyExtractor={this.keyExtractorCategory}
                  renderItem={this._renderItemCategory}
                  onEndReachedThreshold={0.4}
                  removeClippedSubviews={true} // Unmount components when outside of window
                  windowSize={10}
              />
          </View>
      )
    }
    getChildCategory() {
        let param = {
            parent_id: 0,
        };
        CategoryHandle.getInstance().getCategoryMenu(
            param,
            (isSuccess, responseData) => {
                if (isSuccess) {
                    if (responseData.data != null) {
                        let data = responseData.data;
                        this.setState({categories: data});
                    }
                } else {
                    console.log('Failed');
                }
            },
        );
    }
    renderContain() {
    if (this.state.data.length > 0) {
      return this.renderResult();
    } else if (this.state.finishSearch || this.state.data.length === 0) {
      return (
          <EmptyView
              containerStyle={{flex: 1}}
              text={strings('emptySearchProduct')}
          />
      )
    }
  }
    renderResult() {
    return (
      <View style={{marginTop: 10, flex: 1}}>
        <Text
          style={{
            paddingHorizontal: 16,
          }}>
          {strings('related').replace('%s', this.state.resultCount)}
        </Text>
        <FlatList
          // contentInset={{right: 0, top: 0, left: 0, bottom: 0}}
          showsVerticalScrollIndicator={false}
          data={this.state.data}
          keyExtractor={this.keyExtractor}
          renderItem={this._renderItem}
          style={{flex: 1, marginTop: 10}}
          onEndReachedThreshold={0.4}
          onEndReached={() => this.handleLoadMore()}
          removeClippedSubviews={true} // Unmount components when outside of window
          windowSize={10}
        />
      </View>
    );
  }
    keyExtractorCategory = (item, index) => `list_category_${index.toString()}`;
    _renderItemCategory = ({item, index}) => <View>{this.renderItemCategory(item, index)}</View>;

    keyExtractor = (item, index) => `search_${index.toString()}`;
    _renderItem = ({item, index}) => <View>{this.renderItem(item, index)}</View>;
    renderItemCategory(item, index) {
        let selectCategory=this.state.selectCategory===item.id
        return (
            <TouchableOpacity
                onPress={()=>this.setState({selectCategory:item.id})}
                style={{...SearchStyles.btnCategory,backgroundColor:!selectCategory?ColorStyle.optionTextColor:ColorStyle.tabActive}}>
                    <Text style={{...Styles.text.text16, textAlign:'center',color: ColorStyle.tabWhite}} numberOfLines={2}>
                        {item.name}
                    </Text>

            </TouchableOpacity>
        );
    }
    renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <DotIndicator color={ColorStyle.tabActive} size={10} />;
  };
  renderItem(item, index) {
    return (
      <TouchableOpacity
        style={{
          width: Styles.constants.widthScreenMg24,
          alignSelf: 'center',
          backgroundColor: 'transforms',
          flexDirection:'row',
          alignItems:'center',
          marginVertical:10
        }}>
        <MyFastImage
          style={[
            ViewProductStyle.imageProduct,{
              width:  Styles.constants.X*2,
              height: Styles.constants.X*2,
            }
          ]}
          source={{
            uri: this.getImageUrl(item.image_url),
          }}
          resizeMode={AppConstants.IMAGE_SCALE_TYPE.CONTAIN}
        />
      <View style={{justifyContent:'space-between',height:Styles.constants.X*2,marginLeft:10}}>
        <Text style={{...Styles.text.text16,fontWeight:'600', color: ColorStyle.tabBlack,maxWidth:'90%'}} numberOfLines={2}>
          {item.name}
        </Text>
        <Text style={{...Styles.text.text14,fontWeight:'600',color:ColorStyle.tabActive}}>
          {CurrencyFormatter(item.price)}
        </Text>
        <View style={{  flexDirection:'row',
          alignItems:'center'}}>
          <Icon name={'star'} type={'entypo'} color={'yellow'} size={15}/>
          <Text style={{...Styles.text.text12, color: ColorStyle.tabBlack}}>
            {item.rating}
          </Text>
          <Text style={{...Styles.text.text12, color: ColorStyle.tabBlack,marginLeft:10}}>
            {item.total_sold} {strings('sold')}
          </Text>
        </View>
      </View>
      </TouchableOpacity>
    );
  }
  handleLoadMore() {
    if (this.state.loading) {
      return;
    }
    if (!this.state.canLoadData) {
      return;
    }
    this.setState({loading: true, page: this.state.page + 1}, () =>
      this.searchProduct,
    );
  }
  searchProduct() {
    let searchText = this.state.searchText;
    if (DataUtils.stringNullOrEmpty(searchText)) {
      this.setState({
        finishSearch: false,
        data: [],
        canLoadData: true,
        loading: false,
        page: 0,
      });
      return;
    }
    let param = {
      page_index: this.state.page,
      page_size: 20,
      name: searchText,
    };
    ProductHandle.getInstance().searchProduct(
      param,
      (isSuccess, responseData, msg) => {
          console.log(responseData)
        if (isSuccess) {
          if (responseData.data != null && responseData.data) {
            let data = this.state.page === 0 ? [] : responseData.data;
            let resultCount = responseData.data.page_info?.total_items;
            let canLoadData = responseData.data.length === 20;
            this.setState({
              data: responseData.data.data,
              canLoadData,
              finishSearch: true,
              resultCount,
            });
          }
        }
        this.setState({loading: false});
      },
    );
  }
  getImageUrl(images) {
    if (images != null && images.length > 0) {
      return constants.host + images[0].url;
    } else {
      return 'https://picsum.photos/130/214?' + this.props.index;
    }
  }
}
