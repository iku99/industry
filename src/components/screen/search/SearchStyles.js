import Styles from "../../../resource/Styles";
import ColorStyle from "../../../resource/ColorStyle";
const X=Styles.constants.X
export default {
    containerFilter:{
        position:'absolute',top:0,right:0,bottom:0,width:Styles.constants.widthScreen,
        height:'100%',
        backgroundColor:'transparent',  elevation: 3,
    },
    containerView_not:{
        width:Styles.constants.X*2,height:'100%',backgroundColor:'rgba(191,191,191,0.35)'
    },
    bodyFilter:{
        position:'absolute',top:0,right:0,
        width:Styles.constants.widthScreen-Styles.constants.X*2,
        height:'100%',backgroundColor:ColorStyle.tabWhite,
        shadowColor: ColorStyle.borderItemHome,
        shadowOffset: {width: 1, height: 3},
        shadowOpacity: 0.8,
        shadowRadius: 1,
        elevation: 3,

    },
    headerView:{
        backgroundColor:ColorStyle.tabActive,width:'100%',paddingTop:X,paddingBottom:X/4,paddingLeft:10
    },
    bodyList:{
        paddingHorizontal:X/4,
        paddingVertical:X/4
    },
    textTitleHeader:{
        ...Styles.text.text20,fontWeight:'700',color:ColorStyle.tabWhite
    },
    textTitle:{
        ...Styles.text.text16,fontWeight:'500',color:ColorStyle.tabBlack
    },
    btnPrice:{
        paddingVertical:5,
        paddingHorizontal: 10
    },
    btnCategory:{
        height:X,
        shadowColor: 'rgba(0, 0, 0, 0.25)',
        shadowOffset: {width: 0, height: 1},
        shadowOpacity: 0.8,
        shadowRadius: 1,
        justifyContent:'center',
        margin:5,width:(Styles.constants.widthScreen-Styles.constants.X*3)/2,
        alignItems:'center'
    }
}
