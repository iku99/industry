import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import MyFastImage from '../../elements/MyFastImage';
import ProductUtils from '../../../Utils/ProductUtils';
import CurrencyFormatter from '../../../Utils/CurrencyFormatter';
import NavigationUtils from '../../../Utils/NavigationUtils';
import {strings} from '../../../resource/languages/i18n';
import CartUtils from '../../../Utils/CartUtils';
import ColorStyle from '../../../resource/ColorStyle';
import Styles from '../../../resource/Styles';
import StoreHandle from '../../../sagas/StoreHandle';
import OrderHandle from "../../../sagas/OrderHandle";
import ViewUtils from "../../../Utils/ViewUtils";
import MediaUtils from "../../../Utils/MediaUtils";
import OrderStyles from "./OrderStyles";
export default class OrderItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: this.props.item,
            store:undefined
        };
    }
    componentDidMount() {
        this.getStoreDetail(this.props.item.store?.id);
    }

    render() {
        let total = 0;
        let numProduct = 0;
        let order = this.state.item;
        let index = this.props.index
        if (order.details=== undefined) {return null;}
        let productList = order.details.map((item, _) => {
            let product = item.product;
            total = item.price;
            numProduct = numProduct + item.quantity;
            return(
                <TouchableOpacity
                    key={`orderItem_${_.toString()}`}
                    style={OrderStyles.itemProduct}
                    onPress={() => {
                    }}>
                    <MyFastImage
                        style={{flex: 1, height: 70}}
                        source={{
                            uri: ProductUtils.getImages(product)[0],
                            headers: {Authorization: 'someAuthToken'},
                        }}
                        resizeMode={'cover'}
                    />
                    <View style={{marginLeft: 10, flex: 3}}>
                        <Text
                            style={[
                                Styles.text.text14,
                                {
                                    fontWeight: '500',
                                },
                            ]}>
                            {product.name}
                        </Text>
                        <Text
                            style={[
                                Styles.text.text11,
                                {
                                    fontWeight: '400',
                                    color: ColorStyle.gray,
                                    textAlign: 'right',
                                },
                            ]}>
                            x{item.quantity}
                        </Text>
                        <Text
                            style={[
                                Styles.text.text11,
                                {
                                    fontWeight: '400',
                                    textAlign: 'right',
                                    color: ColorStyle.tabActive,
                                },
                            ]}>
                            {CurrencyFormatter(product.price)}
                        </Text>
                    </View>
                </TouchableOpacity>
            )
        });
        return (
            <TouchableOpacity
                style={OrderStyles.bodyItem}
                onPress={() =>{
                    OrderHandle.getInstance().getOrderDetail(
                        order.id,
                        (isSuccess, dataResponse) => {
                            if (isSuccess) {
                                NavigationUtils.goToOrderDetail(
                                    dataResponse.data,
                                    false,
                                    ()=> {if (this.props.cancelOrderFunc != null) {
                                        this.props.cancelOrderFunc(order, index);
                                    }},
                                    ()=>{if (this.props.completeOrder != null) {
                                        this.props.completeOrder(order, index);
                                    }},
                                );
                            } else {
                                ViewUtils.showAlertDialog(strings('cannotFoundOrder'));
                            }
                        },
                    );
                }}>

                <View
                    style={OrderStyles.viewStoreItem}>
                    <TouchableOpacity
                        style={{flexDirection: 'row',alignItems:'center'}}
                        onPress={() => {
                            this.goToStore(order.store);
                        }}>

                        <Image
                            source={ MediaUtils.getStoreAvatar(order.store?.avatar, order.store?.id)}
                            style={{...Styles.icon.iconOrder, borderRadius: 50}}
                        />
                        <Text
                            style={[
                                Styles.text.text14,
                                {
                                    fontWeight: '500',
                                    color: ColorStyle.tabBlack,
                                    marginLeft: 10,
                                },
                            ]}
                            numberOfLines={1}>
                            {order.store?.name}
                        </Text>
                    </TouchableOpacity>
                    <Text
                        style={[
                            Styles.text.text14,
                            {
                                fontWeight: '500',
                                color: CartUtils.getColorText(order.status),
                            },
                        ]}
                        numberOfLines={1}>{`${CartUtils.getCartStatus(
                        order.status,
                    )}`}</Text>
                </View>
                <View style={OrderStyles.bodyProduct}>
                    {productList}
                </View>
                <TouchableOpacity
                    onPress={() =>{
                        OrderHandle.getInstance().getOrderDetail(
                            order.id,
                            (isSuccess, dataResponse) => {
                                if (isSuccess) {
                                    NavigationUtils.goToOrderDetail(
                                        dataResponse.data,
                                        false,
                                        ()=> {if (this.props.cancelOrderFunc != null) {
                                            this.props.cancelOrderFunc(order, index);
                                        }},
                                        ()=>{if (this.props.completeOrder != null) {
                                            this.props.completeOrder(order, index);
                                        }},
                                    );
                                } else {
                                    ViewUtils.showAlertDialog(strings('cannotFoundOrder'));
                                }
                            },
                        );
                    }
                    }
                    style={{
                        paddingVertical: 8,
                    }}>
                    <Text
                        style={[
                            Styles.text.text11,
                            {
                                fontWeight: '400',
                                color: ColorStyle.gray,
                                textAlign: 'center',
                            },
                        ]}>
                        {strings('viewMoreProduct')}
                    </Text>
                </TouchableOpacity>
                <View style={{...OrderStyles.viewStoreItem,flexDirection: 'column',alignItems: 'flex-start',}}>
                    <View style={{width:'100%',flexDirection:'row',alignItems:'center',justifyContent: 'space-between',}}>
                        <Text
                            style={[
                                Styles.text.text11,
                                {fontWeight: '500', color: ColorStyle.optionTextColor},
                            ]}>
                            {numProduct} {strings('product1')}
                        </Text>
                        <Text
                            style={{...Styles.text.text12,fontWeight: '500', color: ColorStyle.tabBlack}}>
                            {strings('totalPrice')}:
                            <Text
                                style={{...Styles.text.text12,fontWeight: '500', color: ColorStyle.tabActive}}>
                                {CurrencyFormatter(order?.discount_detail?.total)}
                            </Text>
                        </Text>
                    </View>
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            paddingTop: 10,
                            width:'100%',
                            alignItems: 'center',
                        }}>
                        <Text style={{...Styles.text.text11, color: ColorStyle.tabBlack}}>
                            {strings('orderID')}
                        </Text>
                        <Text style={{...Styles.text.text12, color: ColorStyle.tabBlack}}>
                            {order.code}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
    goToStore(store) {
        NavigationUtils.goToShopDetail(store.id);
    }
    getStoreDetail(storeId) {
        StoreHandle.getInstance().getStoreDetail(storeId, (isSuccess, store) => {
            if (isSuccess) {
                this.setState({store: store});
            }
        });
    }
}
