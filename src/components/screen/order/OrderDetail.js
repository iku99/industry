import React, {Component} from 'react';
import {FlatList, Image, ScrollView, Text, TextInput, TouchableOpacity, View} from 'react-native';
import Ripple from 'react-native-material-ripple';
import CartUtils from '../../../Utils/CartUtils';
import {strings} from '../../../resource/languages/i18n';
import {Icon} from 'react-native-elements';
import DataUtils from '../../../Utils/DataUtils';
import MyFastImage from '../../elements/MyFastImage';
import ProductUtils from '../../../Utils/ProductUtils';
import CurrencyFormatter from '../../../Utils/CurrencyFormatter';
import ViewUtils from '../../../Utils/ViewUtils';
import AppConstants from '../../../resource/AppConstants';
import Styles from '../../../resource/Styles';
import ColorStyle from '../../../resource/ColorStyle';
import OrderStyles from './OrderStyles';
import RBSheet from 'react-native-raw-bottom-sheet';
import {Actions} from 'react-native-router-flux';
import DateUtil from '../../../Utils/DateUtil';
import StoreHandle from '../../../sagas/StoreHandle';
import NavigationUtils from '../../../Utils/NavigationUtils';
import constants from "../../../Api/constants";
import ImageHelper from "../../../resource/images/ImageHelper";
import SimpleToast from "react-native-simple-toast";
import RatingHandle from "../../../sagas/RatingHandle";
import {EventRegister} from "react-native-event-listeners";
import OrderUtils from "../../../Utils/OrderUtils";
import MediaUtils from "../../../Utils/MediaUtils";
let cartInfos = [];
let numberOfLines = 5;
export default class OrderDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            order: this.props.order,
            store: undefined,
            editComment:'',
            data: [
                {
                    image: ImageHelper.icon_star_disable,
                },
                {
                    image: ImageHelper.icon_star_disable,
                },
                {
                    image: ImageHelper.icon_star_disable,
                },
                {
                    image: ImageHelper.icon_star_disable,
                },
                {
                    image: ImageHelper.icon_star_disable,
                },
            ],
            productId:0,
            starCount: 0,
            text: '',
            loading: false,
            isChooesStar: false,
            statusProduct:5,
            order_detail_id:0,
            checkStatus:false,
            product:undefined,
            dataNotStatus:[]
        };
    }
    render() {
        let order = this.props.order;
        let hideActionBT = this.props.hideActionBT === true;
        return (
            <View style={Styles.container}>
                <ScrollView showsVerticalScrollIndicator={false} >
                    {this.renderStatusOrder(order)}
                    {this.renderAddressView(order)}
                    {this.renderProductView(order)}
                    {this.renderPaymentView(order)}
                    {this.renderShipView(order)}
                    {!hideActionBT && this.renderActionView(order)}
                </ScrollView>

                {order.status === AppConstants.STATUS_ORDER.SUCCESS &&
                    this.renderRequestHIACView()}
                {this.toolbar()}
            </View>
        );
    }
    toolbar() {
        return (
            <View
                style={[
                    {
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        backgroundColor: ColorStyle.tabActive,
                        paddingTop: Styles.constants.X  ,
                        paddingBottom: Styles.constants.X * 0.2,
                        paddingHorizontal: Styles.constants.X * 0.65,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20,
                    },
                ]}>
                <Text
                    style={[
                        Styles.text.text20,
                        {
                            color: ColorStyle.tabWhite,
                            fontWeight: '700',
                            width: '100%',
                            top: 0,
                            textAlign: 'center',
                        },
                    ]}
                    numberOfLines={1}>
                    {strings('orderDetail')}
                </Text>
                <TouchableOpacity
                    onPress={() => Actions.pop()}
                    style={{
                        borderRadius: 200,
                        position: 'absolute',
                        paddingVertical: Styles.constants.X  ,
                        paddingHorizontal: Styles.constants.X * 0.4,
                        top: 0,
                        left: 0,
                    }}>
                    <Icon
                        name="left"
                        type="antdesign"
                        size={25}
                        color={ColorStyle.tabWhite}
                    />
                </TouchableOpacity>
            </View>
        );
    }
    componentDidMount() {
        let order = this.state.order;

        cartInfos = order.details;
        let productSave=cartInfos[0].product;
        cartInfos=cartInfos.filter(ele=>{
            return ele.status===5;
        })
        this.setState({dataNotStatus:cartInfos})

        this.setState({product:productSave})
        this.setState({checkStatus:order.status!==5})
        this.getStoreDetail(this.props.order?.store_id);
    }
    renderStatusOrder() {
        let order = this.state.order;
        return (
            <View
                style={{
                    backgroundColor: '#546E7A',
                    marginTop: Styles.constants.X,
                    paddingTop: Styles.constants.X,
                    paddingBottom: Styles.constants.X * 0.4,
                    flexDirection: 'row',
                    paddingHorizontal: Styles.constants.marginHorizontal20,
                }}>
                <Icon
                    name={'message-text-outline'}
                    type={'material-community'}
                    size={25}
                    color={ColorStyle.tabWhite}
                />
                <View style={{paddingHorizontal: Styles.constants.marginHorizontal20}}>
                    <Text
                        style={{
                            ...Styles.text.text12,
                            color: ColorStyle.tabWhite,
                            fontWeight: '500',
                        }}>
                        {CartUtils.getNotificationsOrder(order.status)}
                    </Text>
                    <Text
                        style={{
                            ...Styles.text.text12,
                            color: ColorStyle.tabWhite,
                            fontWeight: '500',
                        }}>
                        {CartUtils.getDesNotiOrder(order.status, order.created_date)}
                    </Text>
                    <Text
                        style={{
                            ...Styles.text.text12,
                            color: ColorStyle.tabWhite,
                            fontWeight: '500',
                            marginTop: 10,
                        }}>
                        {CartUtils.getTimeNotiOrder(order.status, order.created_date)}
                    </Text>
                </View>
            </View>
        );
    }
    renderAddressView(order) {
        let address =order?.address;
        return (
            <View style={{...OrderStyles.viewBody,marginTop:0}}>
                <View
                    style={[
                        {
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingBottom: 10,
                        },
                    ]}>
                    <Icon name="location" type="octicon" size={15} />
                    <View style={{flex: 1, marginLeft: 10}}>
                        <Text
                            style={[
                                Styles.text.text16,
                                {
                                    fontWeight: '500',
                                    color: ColorStyle.tabBlack,
                                },
                            ]}>
                            {strings('deliveryAddress')}
                        </Text>
                    </View>
                    <Text
                        style={[
                            Styles.text.text14,
                            {
                                fontWeight: '500',
                                color: CartUtils.getColorText(order.status),
                            },
                        ]}
                        numberOfLines={1}>{`${CartUtils.getCartStatus(
                        order.status,
                    )}`}</Text>
                </View>
                <View >
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabBlack,
                            lineHeight: 20,
                        }}>
                        {address.name} | {address.phone}
                    </Text>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.gray,
                            marginTop: 5,
                            lineHeight: 22,
                        }}>
                        {address?.address_detail.address}
                    </Text>
                </View>
            </View>
        );
    }
    renderTextRow(title,value){
        return(
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text
                    style={{
                        flex: 3,
                        ...Styles.text.text11,
                        color: ColorStyle.tabBlack,
                    }}>
                    {title}
                </Text>
                <Text
                    style={{
                        flex: 1,
                        ...Styles.text.text11,
                        color: ColorStyle.tabBlack,
                        textAlign: 'right',
                    }}>
                    {CurrencyFormatter(value)}
                </Text>
            </View>
        )
    }
    renderProductView() {
        let order = this.state.order;
        cartInfos = order.details;
        let total = 0;
        let numProduct = 0;
        if (cartInfos === undefined) {
            cartInfos = [];
        }
        let productInfoView = cartInfos.map((cardInfo, i) => {
            if (cardInfo.product == null) {return;}
            let product = cardInfo.product;
            numProduct = numProduct + cardInfo.quantity;
            return (
                    <TouchableOpacity
                        key={`orderDetail_${i.toString()}`}
                        style={OrderStyles.itemProduct}
                        onPress={() => {}}>
                        <MyFastImage
                            style={{flex: 1, height: 70}}
                            source={{
                                uri: ProductUtils.getImages(product)[0],
                                headers: {Authorization: 'someAuthToken'},
                            }}
                            resizeMode={'cover'}
                        />
                        <View style={{flex: 3}}>
                            <Text

                                style={[
                                    Styles.text.text14,
                                    {
                                        fontWeight: '500',
                                        marginHorizontal: 20,
                                    },
                                ]}>
                                {product.name}
                            </Text>
                            <View
                                style={[{flexDirection: 'row', justifyContent: 'flex-end'}]}>
                                <Text
                                    style={[
                                        Styles.text.text11,
                                        {
                                            fontWeight: '400',
                                            color: ColorStyle.gray,
                                            textAlign: 'right',
                                        },
                                    ]}>{`x ${cardInfo.quantity}`}</Text>
                            </View>
                            <View
                                style={[{flexDirection: 'row', justifyContent: 'flex-end'}]}>
                                <Text
                                    style={[
                                        Styles.text.text11,
                                        {
                                            fontWeight: '400',
                                            textAlign: 'right',
                                            color: ColorStyle.tabActive,
                                        },
                                    ]}>
                                    {CurrencyFormatter(product.price)}
                                </Text>
                            </View>
                        </View>
                    </TouchableOpacity>
            );
        });

        return (
            <View style={{...OrderStyles.viewBody,padding:0}}>
                <View
                    style={OrderStyles.viewStoreItem}>
                    <TouchableOpacity style={{flexDirection: 'row',alignItems:'center'}}>
                        <Image
                            source={ MediaUtils.getStoreAvatar(order.store?.avatar, order.store.id)}
                            style={{...Styles.icon.iconOrder, borderRadius: 50}}
                        />
                        <Text
                            style={[
                                Styles.text.text14,
                                {
                                    fontWeight: '500',
                                    color: ColorStyle.tabBlack,
                                    marginLeft: 10,
                                },
                            ]}
                            numberOfLines={1}>
                            {order.store.name}
                        </Text>
                    </TouchableOpacity>
                </View>
                {productInfoView}
                <View style={{...OrderStyles.bodyProduct,borderTopWidth:1,borderTopColor:ColorStyle.gray,paddingTop:10}}>
                    {this.renderTextRow(strings('items'),order?.discount_detail?.product_price)}
                    {this.renderTextRow(strings('priceShipping'),order?.discount_detail?.shipping_fee)}
                    {this.renderTextRow(strings('voucher'),-(order?.discount_detail?.reduce_shipping_fee+order?.discount_detail?.reduce_product_price))}
                    <View
                        style={[
                            {
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                borderStyle: 'dashed',
                                borderTopWidth: 1,
                                borderColor: '#E2E2E2',
                                paddingVertical: 10,
                            },
                        ]}>
                        <Text
                            style={{
                                flex: 3,
                                ...Styles.text.text12,
                                color: ColorStyle.tabBlack,
                                fontWeight: '700',
                            }}>
                            {strings('totalPrice')}
                        </Text>
                        <Text
                            style={{
                                flex: 1,
                                ...Styles.text.text12,
                                color: ColorStyle.tabActive,
                                fontWeight: '700',
                                textAlign: 'right',
                            }}>
                            {CurrencyFormatter(order?.discount_detail?.total)}
                        </Text>
                    </View>
                </View>
            </View>
        );
    }
    renderShipView(order) {
        let dateOrder=order.delivery_progress;
        let viewDateOrder=dateOrder.map((item,index)=>{
            return(
                <View key={`listPay${index.toString()}`} style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginVertical:5}}>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabBlack,
                            fontWeight: '500',
                        }}
                    >{OrderUtils.getText(item.status)}</Text>
                    <Text style={{fontSize:14,color:ColorStyle.gray}}>{DateUtil.formatDate('DD/MM/YYYY HH:mm ', item.time)}</Text>
                </View>
            )
        });
        return <View style={OrderStyles.viewBody}>
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                }}>
                <Text
                    style={{
                        ...Styles.text.text14,
                        color: ColorStyle.tabBlack,
                        fontWeight: '500',
                    }}>
                    {strings('orderId')}
                </Text>
                <Text
                    style={{
                        ...Styles.text.text14,
                        color: ColorStyle.tabBlack,
                        fontWeight: '500',
                    }}>
                    {order.code}
                </Text>
            </View>
            {viewDateOrder}
        </View>
    }
    renderPaymentView(order) {
        return (
            <View style={OrderStyles.viewBody}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-start',
                    }}>
                    <Icon
                        name={'wallet'}
                        type={'antdesign'}
                        color={ColorStyle.tabActive}
                        size={20}
                    />
                    <Text
                        style={{
                            ...Styles.text.text16,
                            color: ColorStyle.tabBlack,
                            fontWeight: '500',
                            marginLeft: 5,
                        }}>
                        {strings('paymentM')}
                    </Text>
                </View>
                <Text style={{paddingTop: 10, color: ColorStyle.gray}}>
                    {CartUtils.getPayment(order.payment_method)}
                </Text>
            </View>
        );
    }
    renderButton() {
        return (
            <View style={{ alignSelf: 'center'}}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        paddingVertical:5
                    }}>
                    <TouchableOpacity style={OrderStyles.btnBuy}>
                        <Icon
                            name={'message1'}
                            type={'antdesign'}
                            size={16}
                            color={ColorStyle.tabActive}
                        />
                        <Text
                            style={{
                                ...Styles.text.text14,
                                color: ColorStyle.tabBlack,
                                marginLeft: 5,
                            }}>
                            {strings('contactShop')}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={OrderStyles.btnBuy} onPress={()=>{ NavigationUtils.goToProductDetail(this.state.product);}}>
                        <Icon
                            name={'shoppingcart'}
                            type={'antdesign'}
                            size={18}
                            color={ColorStyle.tabActive}
                        />
                        <Text
                            style={{
                                ...Styles.text.text16,
                                color: ColorStyle.tabBlack,
                                marginLeft: 5,
                            }}>
                            {strings('buyAgain')}
                        </Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={OrderStyles.btnRate}  onPress={() => {
                    if(this.state.dataNotStatus.length ===0){
                        return
                    }else {
                        this.requestHIACView.open();
                    }
                }}>
                    <Text style={{...Styles.text.text14, color: ColorStyle.tabWhite}}>
                        {this.state.dataNotStatus.length===0?strings('reviewed'):strings('rate')}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
    renderActionView(order) {
        return (
            <View
                style={{
                    width: Styles.constants.widthScreenMg24,
                    alignSelf: 'center',
                    alignItems: 'center',
                    marginVertical:20
                }}>
                {this.renderRightBT(order)}
            </View>
        );
    }
    showDialog(msg, callback) {
        ViewUtils.showAskAlertDialog(msg, callback, undefined);
    }
    renderRightBT(order) {
        switch (order.status) {
            case AppConstants.STATUS_ORDER.SUCCESS:
                return this.renderButton();
            case AppConstants.STATUS_ORDER.CONFIRMED:
            case AppConstants.STATUS_ORDER.DELIVERING:
                return (
                    <TouchableOpacity
                        onPress={()=>this.renderMess()}
                        style={OrderStyles.btnBuy}>
                        <Icon
                            name={'message1'}
                            type={'antdesign'}
                            size={18}
                            color={ColorStyle.tabActive}
                        />
                        <Text
                            style={{
                                ...Styles.text.text14,
                                color: ColorStyle.tabBlack,
                                marginLeft: 5,
                            }}>
                            {strings('contactShop')}
                        </Text>
                    </TouchableOpacity>
                );
            case AppConstants.STATUS_ORDER.UNCONFIRMED:
                return (
                    <TouchableOpacity
                        style={OrderStyles.btnBuy}
                        onPress={() => {
                            this.showDialog(strings('cancelOrderDes'), () => {
                                if (this.props.cancelOrderFunc != null) {
                                    this.props.cancelOrderFunc(order);
                                }
                                Actions.popTo('order');
                            });
                        }}>
                        <Icon
                            name="remove"
                            type="font-awesome"
                            size={15}
                            color={ColorStyle.tabActive}
                        />
                        <Text
                            style={{
                                ...Styles.text.text16,
                                color: ColorStyle.tabBlack,
                                marginLeft: 5,
                            }}>
                            {strings('cancelOrder')}
                        </Text>
                    </TouchableOpacity>
                );
            case AppConstants.STATUS_ORDER.DELIVERING:
                return (
                    <Ripple
                        style={[
                            {
                                marginRight: '4%',
                                marginLeft: '2%',
                            },
                        ]}
                        onPress={() => {
                            this.showDialog(strings('confirmOrderCompleted'), () => {
                                if (this.props.completeOrder != null) {
                                    this.props.completeOrder(order);
                                }
                            });
                        }}>
                        <Icon name="check" type="font-awesome" size={18} />
                        <Text>{strings('orderConfirmComplete')}</Text>
                    </Ripple>
                );
            case AppConstants.STATUS_ORDER.SHOP_CANCEL:
                return (
                    <View
                        style={{
                            flexDirection: 'row',
                            alignSelf: 'center',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                        }}>
                        <TouchableOpacity style={OrderStyles.btnBuy} onPress={() => {}}>
                            <Text
                                style={{
                                    ...Styles.text.text16,
                                    color: ColorStyle.tabBlack,
                                    marginLeft: 5,
                                }}>
                                {strings('setAgain')}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>this.renderMess()} style={{...OrderStyles.btnBuy, marginLeft: 10}}>
                            <Icon
                                name={'message1'}
                                type={'antdesign'}
                                size={18}
                                color={ColorStyle.tabActive}
                            />
                            <Text
                                style={{
                                    ...Styles.text.text16,
                                    color: ColorStyle.tabBlack,
                                    marginLeft: 5,
                                }}>
                                {strings('contactShop')}
                            </Text>
                        </TouchableOpacity>
                    </View>
                );
            case AppConstants.STATUS_ORDER.USER_CANCEL:
            case AppConstants.STATUS_ORDER.CANNOT_SHIP:
                return (
                    <View/>
                );
        }
    }
    renderMess(){
        NavigationUtils.goToConversation(this.props.order?.store_id);
    }
    getImageUrl(item, index) {
        let url = item.image_url;
        return DataUtils.stringNullOrEmpty(url)
            ? 'https://picsum.photos/50/50?avatar' + index
            : constants.host + url;
    }
    renderRequestHIACView() {
        cartInfos = this.state.dataNotStatus;
        if (cartInfos === undefined) {
            cartInfos = [];
        }
        let productInfoView = cartInfos.map((cardInfo, i) => {
            if (cardInfo.product == null) {return;}
            let product = cardInfo.product;
            if(this.state.checkStatus){
                return
            }
            return (
                <TouchableOpacity
                    key={`orderDetail_${i.toString()}`} style={{marginVertical: 16,paddingVertical:10,width: 101,marginHorizontal:10,borderColor:this.state.productId===cardInfo.product.id?'red':'white',borderWidth:this.state.productId===cardInfo.product.id?1:0,alignItems:'center'}}
                    onPress={() => {this.setState({productId:cardInfo.product.id,statusProduct:cardInfo.status,order_detail_id:cardInfo.id})}}>
                    <MyFastImage
                        style={{width:Styles.constants.X*2, height: Styles.constants.X*2}}
                        source={{
                            uri: ProductUtils.getImages(product)[0],
                            headers: {Authorization: 'someAuthToken'},
                        }}
                        resizeMode={'cover'}
                    />
                    <Text
                        numberOfLines={1}
                        style={{
                            ... Styles.text.text14,
                            fontWeight: '500',
                            marginTop:5
                        }}>
                        {product.name}
                    </Text>
                </TouchableOpacity>
            );
        });
        return (
            <RBSheet
                customStyles={{
                    container: {
                        backgroundColor: '#F9F9F9',
                        overflow: 'hidden',
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10,
                    },
                }}
                keyboardAvoidingViewEnabled={false}
                closeOnDragDown={false}
                height={500}
                ref={ref => {
                    this.requestHIACView = ref;
                }}
                openDuration={150}
                closeDuration={150}>
                <View style={{marginVertical:10,paddingHorizontal:20}}>
                    <Text style={{fontSize:20,fontWeight:'600',textAlign:'center'}}>{strings('ratingTitle')}</Text>
                    <View style={{flexDirection:'row'}}>
                        {productInfoView}
                    </View>
                    <FlatList
                        horizontal={true}
                        data={this.state.data}
                        style={{alignSelf:'center',marginVertical:10}}
                        renderItem={({item, index}) => (<TouchableOpacity onPress={() => this.setRating(index)}>
                            <Image
                                style={{
                                    height: Styles.constants.X,
                                    width: Styles.constants.X,
                                    alignItems: 'center',
                                    marginHorizontal: 4,
                                }}
                                source={item.image}
                            />
                        </TouchableOpacity>)}/>

                    <Text style={{...Styles.text.text14,color:ColorStyle.tabBlack,marginBottom:10}}>{strings('returnNote')}</Text>
                    <TextInput
                        style={{ borderColor:'black',borderWidth:1,paddingLeft:10,}}
                        value={this.state.editComment}
                        autoCorrect={false}
                        placeholderTextColor={'#000'}
                        onChangeText={note => {
                            this.setState({editComment:note});
                        }}
                        ref={ref => {
                            this.txtComment = ref;
                        }}
                        multiline={true}
                        numberOfLines={numberOfLines}
                        minHeight={20 * (numberOfLines + 2)}
                        underlineColorAndroid="transparent"
                    />
                    <TouchableOpacity onPress={()=>{this.sendRating()}} style={{...OrderStyles.btnBuy,width:'60%',marginVertical:10,backgroundColor:ColorStyle.tabActive,alignSelf:'center' }}>
                        <Text style={{...Styles.text.text16, color: ColorStyle.tabWhite}}>{strings('send')}</Text>
                    </TouchableOpacity>
                </View>
            </RBSheet>
        );
    }
    sendRating(){
        if(!this.state.productId>0){
            return   SimpleToast.show(strings('checkProduct'), SimpleToast.SHORT)
        }
        if(!this.state.starCount>0){
            return  SimpleToast.show(strings('checkRating'), SimpleToast.SHORT)
        }
        let params={
            des:this.state.editComment,
            rate:this.state.starCount,
            product_id:this.state.productId,
            status:this.state.statusProduct,
            store_id:this.state.order.store.id,
            order_detail_id:this.state.order_detail_id
        }
        RatingHandle.getInstance().sendRating(params,(isSuccess, dataResponse) => {
            if(dataResponse.code===0){
                Actions.pop();
                EventRegister.emit(AppConstants.EventName.ORDER_USER, '');
                this.requestHIACView.close()
            }else {
                SimpleToast.show(strings('notRating'), SimpleToast.SHORT)
            }
        })
    }
    setRating(position) {
        let newData = [];
        this.state.data.forEach((item, index) => {
            if (index <= position) {
                let newItem = {
                    image: ImageHelper.icon_star,
                };
                newData.push(newItem);
            } else {
                let newItem = {
                    image: ImageHelper.icon_star_disable,
                };
                newData.push(newItem);
            }
        });
        this.setState({data: []}, () =>
            this.setState({
                data: newData,
                starCount: position + 1,
                isChooesStar: true,
            }),
        );
    }
    getStoreDetail(storeId) {
        StoreHandle.getInstance().getStoreDetail(storeId, (isSuccess, store) => {
            if (isSuccess) {
                this.setState({store: store});
            }
        });
    }
    goToStore(order) {
        NavigationUtils.goToShopDetail(order.store_id);
    }
}
