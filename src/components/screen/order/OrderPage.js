import React, {Component} from 'react';
import {ActivityIndicator, FlatList, View} from 'react-native';
import EmptyView from '../../elements/reminder/EmptyView';
import {strings} from '../../../resource/languages/i18n';
import AppConstants from '../../../resource/AppConstants';
import {PacmanIndicator} from 'react-native-indicators';
import SimpleToast from 'react-native-simple-toast';
import OrderItem from './OrderItem';
import OrderHandle from '../../../sagas/OrderHandle';
import {EventRegister} from "react-native-event-listeners";
import RenderLoading from "../../elements/RenderLoading";
import ColorStyle from "../../../resource/ColorStyle";
import Styles from "../../../resource/Styles";
export default class OrderPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
      page: 1,
      canLoadData: true,
      isStore: this.props.isStore,
    };
  }
  componentDidMount() {

    EventRegister.addEventListener(AppConstants.EventName.ORDER_USER, total => {
      this.setState({data:[]},()=>this.getOrders())

    });
    this.getOrders();
  }
  render() {
    return (
      <View>
        {this.state.data.length > 0 && (
          <FlatList
            contentInset={{right: 0, top: 0, left: 0, bottom: 0}}
            showsVerticalScrollIndicator={false}
            data={this.state.data}
            keyExtractor={this.keyExtractor}
            renderItem={this._renderItem}
            style={{
              height: '100%',
              backgroundColor: ColorStyle.transparent,
              paddingVertical: Styles.constants.X/9,
            }}
            onEndReachedThreshold={0.4}
            onEndReached={() => this.handleLoadMore()}
          />
        )}
        {this.state.data.length === 0 && !this.state.loading && (
          <View
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <EmptyView text={strings('emptyOrder')} />

          </View>
        )}

        <RenderLoading loading={this.state.loading}/>
      </View>
    );
  }
  keyExtractor = (item, index) => `orderPage_${index.toString()}`;
  _renderItem = ({item, index}) => <View>{this.renderItem(item, index)}</View>;
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) {
      return null;
    }
    return <ActivityIndicator style={{color: '#000'}} />;
  };
  handleLoadMore() {
    if (this.state.loading) {
      return;
    }
    if (!this.state.canLoadData) {
      return;
    }
    this.setState({loading: true, page: this.state.page + 1}, () =>
      this.getOrders(),
    );
  }
  renderLoading() {
    if (this.state.loading) {
      return (
        <View>
          <PacmanIndicator color="orange" size={40} />
        </View>
      );
    } else {
      return null;
    }
  }
  getOrders(type) {
    let param = {
      page_index: this.state.page,
      page_size: AppConstants.LIMIT,
    };
    param = {
      ...param,
      status: this.props.type,
    };
    OrderHandle.getInstance().getOrderByUser(
      param,
      (isSuccess, dataResponse) => {
        let mData = this.state.data;
        let newData = [];
        if (
          isSuccess &&
          dataResponse.data !== undefined
        ) {
          newData = dataResponse.data;
        }
        let data = mData.concat(newData);
        let canLoadData = newData.length === AppConstants.LIMIT;
        this.setState({data: data, canLoadData, loading: false});
      },
    );
  }
  renderItem(item, index) {
return(
    <OrderItem
        item={item}
        index={index}
        cancelOrderFunc={(item, index) => {
          this.cancelOrder(item, index);
        }}
        completeOrder={(item, index) => {
          this.completeOrder(item, index);
        }}
    />
)
  }
  cancelOrder(item, index) {
    let params = {
      status: AppConstants.STATUS_ORDER.USER_CANCEL,
      reason: strings('userYeuCau'),
    };
      OrderHandle.getInstance().cancelOrder(
          item.id,
          params,
          (isSuccess, responseData) => {
            if (isSuccess) {
              if (this.onDataChange != null) {
                this.onDataChange(item, index, item.status, params.status);
              }
              SimpleToast.show(strings('completeOrderSuccessful'), SimpleToast.SHORT,)
              return
            }
            SimpleToast.show(strings('completeOrderFailed'), SimpleToast.SHORT,)
          },
      );
  }
  completeOrder(item, index) {
    let params = {
      status: AppConstants.STATUS_ORDER.SUCCESS,
      reason: strings('userMarkComplete'),
    };
    OrderHandle.getInstance().cancelOrder(
        item.id,
        params,
        (isSuccess, responseData) => {
          if (isSuccess) {
            if (this.onDataChange != null) {
              this.onDataChange(item, index, item.status, params.status);
            }
            SimpleToast.show(strings('orderCancelSuccessful'), SimpleToast.SHORT,)
          }
          SimpleToast.show(strings('orderCancelFailed'), SimpleToast.SHORT,)
        },
    );
  }
  onDataChange(item, index, preStatus, newStatus) {
    this.setState({data:[]},()=>{
      // this.getOrders()
      EventRegister.emit(AppConstants.EventName.ORDER_USER, '');
    })
    // let data = this.state.data;
    // if (index < 0 || index >= data.length) {
    //   return;
    // }
    // item = {
    //   ...item,
    //   status: newStatus,
    // };
    // if (this.props.type === AppConstants.STATUS_ORDER.TO_PAY) {
    //   //Neu tab Toan bo thi chi cap nhat lai trang thai
    //   data[index] = item;
    //   this.setState({data: []}, () => {
    //     this.setState({data});
    //   });
    // } else if (this.props.type === preStatus) {
    //   //Neu type = preStatus thi xoa item di
    //   for (let i = 0; i < data.length; i++) {
    //     let mItem = data[i];
    //     if (mItem.id === item.id) {
    //       data = DataUtils.removeItem(data, i);
    //       this.setState({data: []}, () => {
    //         this.setState({data});
    //       });
    //       return;
    //     }
    //   }
    // } else if (this.props.type === newStatus) {
    //   //Neu type = newStatus thi them item do
    //   data = [item, ...data];
    //   this.setState({data: []}, () => {
    //     this.setState({data});
    //   });
    // }
  }
}
