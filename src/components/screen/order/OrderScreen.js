import React, {Component} from 'react';
import { Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../resource/Styles';
import ColorStyle from '../../../resource/ColorStyle';
import {strings} from '../../../resource/languages/i18n';
import {SceneMap, TabView} from 'react-native-tab-view';
import AppConstants from '../../../resource/AppConstants';
import AppTab from '../../elements/AppTab';
import OrderPage from './OrderPage';
import PropTypes from 'prop-types';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';

class Order extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: this.props.index === undefined ? 0 : this.props.index,
        isStore:false,
      routes: [
        {key: 'tab1', title: strings('to_pay')},
        {key: 'tab2', title: strings('un_confirmed')},
        {key: 'tab3', title: strings('confirmed')},
        {key: 'tab4', title: strings('delivering')},
        {key: 'tab5', title: strings('userCancel')},
        {key: 'tab6', title: strings('shopCancel')},
        {key: 'tab7', title: strings('success')},
      ],
    };
  }
  componentDidMount() {
    setTimeout(() => this.appTabRef.onTabChanged(this.state.index), 100);
  }

  _handleIndexChange = index => {
    this.appTabRef.onTabChanged(index);
    this.setState({index});
  };

  Tab1 = () => (
    <OrderPage
      type={AppConstants.STATUS_ORDER.TO_PAY}
      isStore={this.state.isStore}
      ref={ref => (this.toPay = ref)}
      onDataChange={(item, index, preStatus, newStatus) => {
        this.onDataChange(item, index, preStatus, newStatus);
      }}
    />
  );
  Tab2 = () => (
    <OrderPage
      type={AppConstants.STATUS_ORDER.UNCONFIRMED}
      isStore={this.state.isStore}
      ref={ref => (this.unconfirmed = ref)}
      onDataChange={(item, index, preStatus, newStatus) => {
        this.onDataChange(item, index, preStatus, newStatus);
      }}
    />
  );
  Tab3 = () => (
    <OrderPage
      type={AppConstants.STATUS_ORDER.CONFIRMED}
      isStore={this.state.isStore}
      ref={ref => (this.confirmed = ref)}
      onDataChange={(item, index, preStatus, newStatus) => {
        this.onDataChange(item, index, preStatus, newStatus);
      }}
    />
  );
  Tab4 = () => (
    <OrderPage
      type={AppConstants.STATUS_ORDER.DELIVERING}
      isStore={this.state.isStore}
      ref={ref => (this.delivering = ref)}
      onDataChange={(item, index, preStatus, newStatus) => {
        this.onDataChange(item, index, preStatus, newStatus);
      }}
    />
  );
  Tab5 = () => (
    <OrderPage
      type={AppConstants.STATUS_ORDER.USER_CANCEL}
      isStore={this.state.isStore}
      ref={ref => (this.cancelled = ref)}
      onDataChange={(item, index, preStatus, newStatus) => {
        this.onDataChange(item, index, preStatus, newStatus);
      }}
    />
  );
  Tab6 = () => (
    <OrderPage
      type={AppConstants.STATUS_ORDER.SHOP_CANCEL}
      isStore={this.state.isStore}
      ref={ref => (this.shop_cancel = ref)}
      onDataChange={(item, index, preStatus, newStatus) => {
        this.onDataChange(item, index, preStatus, newStatus);
      }}
    />
  );
  Tab7 = () => (
    <OrderPage
      type={AppConstants.STATUS_ORDER.SUCCESS}
      isStore={this.state.isStore}
      ref={ref => (this.success = ref)}
      onDataChange={(item, index, preStatus, newStatus) => {
        this.onDataChange(item, index, preStatus, newStatus);
      }}
    />
  );
  _renderTabBar = props => {
    return (
      <AppTab
        data={props.navigationState.routes}
        index={this.state.index}
        onPress={i => {
          this.setState({index: i});
        }}
        ref={ref => {
          this.appTabRef = ref;
        }}
      />
    );
  };
  _renderScene = SceneMap({
    tab1: this.Tab1,
    tab2: this.Tab2,
    tab3: this.Tab3,
    tab4: this.Tab4,
    tab5: this.Tab5,
    tab6: this.Tab6,
    tab7: this.Tab7,
  });
  toolbar() {
    return (
      <View
        style={[
          {
            paddingTop: Styles.constants.X,
            paddingHorizontal: Styles.constants.X * 0.65,
            backgroundColor: ColorStyle.tabActive,
            paddingBottom: 10,
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20,
          },
        ]}>
        <Text
          style={[
            Styles.text.text20,
            {
              color: ColorStyle.tabWhite,
              fontWeight: '700',
              top: 0,
              textAlign: 'center',
            },
          ]}
          numberOfLines={1}>
          {strings('order')}
        </Text>
        <TouchableOpacity
          onPress={() => Actions.pop()}
          style={{
            position: 'absolute',
            paddingVertical: Styles.constants.X ,
            paddingHorizontal: Styles.constants.X * 0.4,
            top: 0,
            left: 0,
          }}>
          <Icon
            name="left"
            type="antdesign"
            size={25}
            color={ColorStyle.tabWhite}
          />
        </TouchableOpacity>
      </View>
    );
  }
  render() {
    return (
      <View
        style={[Styles.container, {backgroundColor: ColorStyle.colorPersonal}]}>
        {this.toolbar()}
        <TabView
          navigationState={this.state}
          renderScene={this._renderScene}
          renderTabBar={this._renderTabBar}
          onIndexChange={this._handleIndexChange}
        />
      </View>
    );
  }
  onDataChange(item, index, preStatus, newStatus) {
    if (this.toPay != null) {
      this.toPay.onDataChange(item, index, preStatus, newStatus);
    }
    if (this.unconfirmed != null) {
      this.unconfirmed.onDataChange(item, index, preStatus, newStatus);
    }
    if (this.confirmed != null) {
      this.confirmed.onDataChange(item, index, preStatus, newStatus);
    }
    if (this.delivering != null) {
      this.delivering.onDataChange(item, index, preStatus, newStatus);
    }
    if (this.cancelled != null) {
      this.cancelled.onDataChange(item, index, preStatus, newStatus);
    }
    if (this.shop_cancel != null) {
      this.shop_cancel.onDataChange(item, index, preStatus, newStatus);
    }
    if (this.success != null) {
      this.success.onDataChange(item, index, preStatus, newStatus);
    }
  }
}
//lấy dữ liệu về
const mapStateToProps = state => {
  return {};
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {
    onFetchMovies: () => {},
  };
};
const OrderScreen = connect(mapStateToProps, mapDispatchToProps)(Order);
export default OrderScreen;
OrderScreen.defaultProps = {
  index: 0,
};

OrderScreen.propTypes = {
  index: PropTypes.number.required,
};
