import ColorStyle from '../../../resource/ColorStyle';
import Styles from '../../../resource/Styles';
const X=Styles.constants.X
export default {
  viewBody: {
    backgroundColor: ColorStyle.tabWhite,
    marginTop: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    padding: X/4,
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    },
  btnBuy: {
    ...Styles.input.codeInput,
    height:Styles.constants.X*1.2,
    borderRadius: 25,
    width: (Styles.constants.widthScreenMg24/2)-10,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent:'center',
    flexDirection: 'row',
    marginBottom: 16,
  },
  btnRate: {
    ...Styles.input.codeInput,
    backgroundColor: ColorStyle.tabActive,
    borderRadius: 25,
    alignItems: 'center',
  },
  bodyItem:{
    backgroundColor: ColorStyle.tabWhite,
    marginTop: 10,
  },
  viewStoreItem:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    padding: X/4,
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    backgroundColor: ColorStyle.tabWhite,
    elevation: 3,
  },
  itemProduct:{
    flexDirection: 'row',
    justifyContent: 'flex-start',
    margin: 10,
    paddingVertical: 10,
  },
  bodyProduct:{
    paddingHorizontal: X/4,
  }

};
