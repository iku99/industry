import React, { Component } from "react";
import {FlatList, View} from "react-native";
import Toolbar from "../../elements/toolbar/Toolbar";
import { strings } from "../../../resource/languages/i18n";
import ColorStyle from "../../../resource/ColorStyle";
import Styles from "../../../resource/Styles";
import StoreHandle from "../../../sagas/StoreHandle";
import ItemSupplierView from "../../elements/viewItem/ItemSupplierView";
import AppBannerRow from "../main/home/row/AppBannerRow";
import AppConstants from "../../../resource/AppConstants";
import {UIActivityIndicator} from "react-native-indicators";

const Data=[
    {
        type:1
    }
]
const LIMIT=10
export default class SupplierScreen extends Component{
    constructor(props) {
        super(props);
        this.state = {
            data: Data,
            page: 1,
            canLoadData: true,
            loading: false,
        };
    }
    componentDidMount() {
        this.getSupplier()
    }
    getSupplier(){
        let params={
            page_index:this.state.page,
            page_size:LIMIT
        }
        StoreHandle.getInstance().getTopStore(params,(isSuccess,responseData)=>{
            if(isSuccess){
                if (responseData.data != null ) {
                    if (responseData.data.length < LIMIT) {
                        this.setState({canLoadData: false});
                    }
                    let listData = this.state.data;
                    let abc = {
                        data: responseData.data,
                        type: 2,
                    };
                    listData = [...listData, ...[abc]];
                    console.log("listData:",listData)
                    this.setState({data: listData, loading: false});
                }
            }
        })
    }
  render() {
    return(
      <View style={Styles.container}>
        <Toolbar
          title={strings('supplier')}
          iconBack={true}
          backgroundColor={ColorStyle.tabActive}
          textColor={ColorStyle.tabWhite}
        />

          <FlatList
              contentContainerStyle={{
                  alignSelf: 'center',
              }}
              showsHorizontalScrollIndicator={false}
              style={{paddingBottom: 20}}
              ListFooterComponent={this.renderFooter.bind(this)}
              onEndReachedThreshold={0.4}
              onEndReached={() => this.handleLoadMore()}
              data={this.state.data}
              keyExtractor={this.keyExtractor}
              renderItem={this.renderItem}
          />
      </View>
    )
  }
    renderItem=({item,index})=>{
     switch (item.type) {
         case 1:return <AppBannerRow
             showLoading={loading => this.setState({loading})}
             type={AppConstants.BANNER_POSITION.MAIN}
             statusBarAlpha={this.state.statusBarAlpha}
             containerStyle={{
                 maxHeight: (Styles.constants.widthScreen * 2.5) / 3,
             }}
             backgroundStyle={{height: (Styles.constants.widthScreen * 1.5) / 3}}
         />
         case 2:return this.renderList(item.data)
         case 3:
             return <View style={{marginBottom: 10}} />;
         default:
             return null;
     }
    }
    renderList(data){
        console.log(data)
        return(
            <FlatList
                contentContainerStyle={{
                    alignSelf: 'center',
                }}
                showsHorizontalScrollIndicator={false}
                style={{paddingBottom: 20}}
                numColumns={2}
                data={data}
                keyExtractor={this.keyExtractor1}
                renderItem={this._renderItem}
            />
        )
    }
    renderFooter = () => {
        if (!this.state.loading) {
            return null;
        }
        return <UIActivityIndicator color={ColorStyle.tabActive} size={20} />;
    };

    handleLoadMore() {
        if (this.state.loading) return ;
        if (!this.state.canLoadData) return;
        this.setState({loading: true, page: this.state.page + 1}, this.getSupplier);
    }
    keyExtractor1 = (item, index) => `supplierRow1_${index.toString()}`;

    keyExtractor = (item, index) => `supplierRow_${index.toString()}`;
    _renderItem = ({item, index}) => (
        <ItemSupplierView item={item} loading={this.state.loading} />
    );
}
