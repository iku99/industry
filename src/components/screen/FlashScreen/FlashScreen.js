import React, {Component} from 'react';
import {
  AsyncStorage,
  Image,
  ImageBackground,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ColorStyle from '../../../resource/ColorStyle';
import {Actions} from 'react-native-router-flux';
import {EventRegister} from 'react-native-event-listeners';
import AppConstants from '../../../resource/AppConstants';
import AccountHandle from '../../../sagas/AccountHandle';
import ImageHelper from "../../../resource/images/ImageHelper";
import Styles from "../../../resource/Styles";
import AppleAuth from "../../../sagas/auth/AppleAuth";
import FacebookAuth from "../../../sagas/auth/FacebookAuth";
import GoogleAuth from "../../../sagas/auth/GoogleAuthen";
export default class FlashScreen extends Component {
  componentDidMount() {
       // AccountHandle.getInstance().logout()
    AsyncStorage.getItem(
      AppConstants.SharedPreferencesKey.LOGIN_TYPE,
      (error, type) => {
          console.log(type)
        switch (type) {
          case AppConstants.LOGIN_TYPE.GOOGLE:
            GoogleAuth.getInstance().signInToApp(_ => {
              this.goToMainScreen();
            }, false);
            break;
          case AppConstants.LOGIN_TYPE.FACEBOOK:
          FacebookAuth.getInstance().signInWithFacebook(_=>{
              this.goToMainScreen();
          });
          break;
          case AppConstants.LOGIN_TYPE.APPLE:
          AppleAuth.getInstance().login(isSuccess => {
            this.goToMainScreen();
          }, false);
          break;
          default:
            this.loginUser();
            break;
        }
      },
    );
  }

  loginUser() {
    AsyncStorage.getItem(
      AppConstants.SharedPreferencesKey.user_name,
      (error, userName) => {

        if (error != null || userName === null) {
          this.goToMainScreen();
          return;
        }
        AsyncStorage.getItem(
          AppConstants.SharedPreferencesKey.userValidation,
          (error, pwd) => {
            if (error != null || pwd === undefined) {
              this.goToMainScreen();
              return;
            }
            //Thu hien dang nhap
            let params = {
                user_name: userName,
                password: pwd,
                ecommerce_id:AppConstants.EventName.ECOMMERCE
            };
            AccountHandle.getInstance().login(params, (error, dataResponse) => {
              if (error == null) {
                EventRegister.emit(AppConstants.EventName.LOGIN, true);
              }
              this.goToMainScreen();
            });
          },
        );
      },
    );
  }

  render() {
    return (
      <View
        style={{
          alignItems: 'center',
          backgroundColor: ColorStyle.tabActive,
          width: '100%',
          height: '100%',
          justifyContent:'center'
        }}>
        <View>
         <Image source={ImageHelper.logoApp} style={Styles.icon.iconLogoBig}/>
        </View>
      </View>
    );
  }
  goToMainScreen() {
 Actions.reset('drawer');
  }
}
