import React, {Component} from 'react';
import {
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../resource/Styles';
import {Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import TitleLogin from '../../elements/TitleLogin';
import {strings} from '../../../resource/languages/i18n';
import {EventRegister} from 'react-native-event-listeners';
import ColorStyle from '../../../resource/ColorStyle';
import OtherLogin from '../../elements/OtherLogin';
import AppConstants from '../../../resource/AppConstants';
import DataUtils from '../../../Utils/DataUtils';
import {postLogin} from '../../../actions';
import AccountHandle from '../../../sagas/AccountHandle';
import ViewUtils from '../../../Utils/ViewUtils';
import SimpleToast from 'react-native-simple-toast';
import LoadingView from '../../elements/LoadingView';
let checkPasswordId = -1;
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      password: '',
      validPass: false,
      validUser: false,
      loading: false,
      check_phone: false,
      showPassword: false,
    };
  }
  render() {
    return (
      <View style={Styles.container}>
        <TitleLogin title={strings('signIn')} />
        <View style={{alignItems: 'center'}}>
          {this.renderTextInputWithTitle(
            'userName',
            'validUser',
            strings('emailPhone'),
            'checkEmailId',
            () => this.checkUserName(),
            'email-address',
            'next',
            input => {
              this.storeMailRef = input;
            },
          )}
          {this.renderTextInputPassword(
            this.state.password,
            'password',
            strings('password'),
            this.state.showPassword,
            this.state.validPass,
            password => {
              clearTimeout(checkPasswordId);
              checkPasswordId = setTimeout(() => {
                this.checkPassword(password);
              }, 200);
            },
          )}
        </View>
        <View>
          <TouchableOpacity
            style={{
              width: Styles.constants.widthScreenMg24,
              flexDirection: 'row',
              justifyContent: 'flex-end',
              alignSelf: 'center',
              alignItems: 'center',
              paddingVertical: 10,
            }}
            onPress={() => {
              Actions.jump('forgot');
            }}>
            <Text
              style={[
                Styles.text.text14,
                {color: ColorStyle.tabActive},
              ]}>{`${strings('forgotPassword')}`}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            animation="fadeInUpBig"
            onPress={() => {
              this.login();
            }}
            style={[
              Styles.button.buttonLogin,
              {
                backgroundColor: ColorStyle.tabActive,
                borderColor: 'rgba(250, 111, 52, 0.5)',
                borderWidth: 1,
                marginBottom: Styles.constants.X * 2,
              },
            ]}>
            <Text style={[Styles.text.text18, {fontWeight: '500'}]}>
              {strings('signIn')}
            </Text>
          </TouchableOpacity>
          <OtherLogin
            loading={item => {
              this.setState({loading: item});
            }}
            title={strings('newUser')}
            titleBT={strings('signUp')}
            callback={() => {
              Actions.jump('register');
            }}
          />
        </View>
        <LoadingView loading={this.state.loading} />
      </View>
    );
  }
  renderTextInputWithTitle(
    stateName,
    validStateName,
    placeholder,
    checkId,
    checkFunc,
    keyboardType,
    returnKeyType,
    ref,
    onSubmitEditing,
  ) {
    let mValidStateName = this.state[validStateName];
    return (
      <View style={{marginTop: 10}}>
        <TextInput
          style={Styles.input.inputLogin}
          placeholder={placeholder}
          value={this.state[stateName]}
          placeholderTextColor={ColorStyle.textInput}
          onChangeText={text => {
            this.setState({[stateName]: text}, () => {
              if (!DataUtils.stringNullOrEmpty(checkId) && checkFunc != null) {
                clearTimeout(this[checkId]);
                this[checkId] = setTimeout(() => {
                  checkFunc();
                }, 500);
              }
            });
          }}
          ref={ref}
          onSubmitEditing={onSubmitEditing}
          returnKeyType={returnKeyType}
          keyboardType={
            !DataUtils.stringNullOrEmpty(keyboardType)
              ? keyboardType
              : 'ascii-capable'
          }
        />
        {mValidStateName != null && (
          <Icon
            name={mValidStateName ? '' : 'close'}
            type="antdesign"
            size={18}
            color={mValidStateName ? ColorStyle.tabActive : ColorStyle.red}
            containerStyle={{
              position: 'absolute',
              right: 10,
              alignItems: 'flex-end',
              display: this.state[stateName] === '' ? 'none' : 'flex',
              bottom: '30%',
            }}
          />
        )}
      </View>
    );
  }
  renderTextInputPassword(
    stateName,
    valueName,
    placeholder,
    secure,
    validState,
    callback,
  ) {
    return (
      <View style={{marginTop: 10}} animation="fadeInLeftBig">
        <TextInput
          style={[Styles.input.inputLogin]}
          placeholder={placeholder}
          value={this.state[stateName]}
          placeholderTextColor={ColorStyle.textInput}
          onChangeText={callback}
          secureTextEntry={!secure}
        />
        {secure !== null ? (
          <Icon
            name={secure ? 'eye' : 'eye-off'}
            type="feather"
            size={20}
            color={ColorStyle.tabActive}
            containerStyle={{position: 'absolute', right: 20, bottom: 15}}
            onPress={() => this.setState({showPassword: !secure})}
          />
        ) : null}
        {validState != null && (
          <Icon
            name={validState ? '' : 'close'}
            type="antdesign"
            size={20}
            color={ColorStyle.tabActive}
            containerStyle={{
              position: 'absolute',
              right: 50,
              bottom: 15,
              display: this.state.password === '' ? 'none' : 'flex',
            }}
          />
        )}
      </View>
    );
  }
  checkUserName() {
    if (DataUtils.validMail(this.state.userName)) {
      return this.setState({validUser: true, check_phone: false});
    } else if (DataUtils.validPhone(this.state.userName)) {
      return this.setState({validUser: true, check_phone: true});
    } else {
      return this.setState({validUser: false});
    }
  }

  checkPassword(password) {
    this.setState({
      password: password,
      validPass: this.state.password.length > 6,
    });
  }

  login() {
    if (!this.state.validUser && !this.state.validPass) {
      SimpleToast.show(strings('loginInfoIncorrect'));
      return;
    }
    let params = {
      user_name: this.state.userName,
      password: this.state.password,
    };
    this.setState({loading: true});
    AccountHandle.getInstance().login(params, (isSuccess, dataResponse) => {
      console.log(dataResponse);
      if (isSuccess) {
        this.setState({loading: false});
        EventRegister.emit(AppConstants.EventName.LOGIN, true);
        Actions.reset('drawer');
        return;
      }
      ViewUtils.showAlertDialog(strings('loginInfoIncorrect'), () =>
        this.setState({loading: false}),
      );
    });
  }
}

//lấy dữ liệu về
const mapStateToProps = state => {
  return {account: state.Accounts};
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {
    postLogin: param => {
      dispatch(postLogin(param));
    },
  };
};
const LoginScreen = connect(mapStateToProps, mapDispatchToProps)(Login);
export default LoginScreen;
