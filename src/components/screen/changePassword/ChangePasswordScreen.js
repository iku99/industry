import React, {Component} from 'react';
import {AsyncStorage, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../resource/Styles';
import Toolbar from '../../elements/toolbar/Toolbar';
import {strings} from '../../../resource/languages/i18n';
import ColorStyle from '../../../resource/ColorStyle';
import {HelperText, TextInput} from 'react-native-paper';
import AccountHandle from '../../../sagas/AccountHandle';
import AppConstants from '../../../resource/AppConstants';
import ViewUtils from '../../../Utils/ViewUtils';
import { Actions } from "react-native-router-flux";
import SimpleToast from "react-native-simple-toast";
let checkPasswordId = -1;
let checkNewPasswordId = -1;
let checkConfirmPasswordId = -1;
export default class ChangePasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      validPass: false,
      currentPassValid: false,
      newPassValid: false,
      confirmPassValid: false,
      showPasswordCurrent: false,
      showPasswordNew: false,
      showPasswordConfirm: false,
      newPass: '',
      confirmPass: '',
    };
  }

  render() {
    return (
      <View style={Styles.container}>
        <Toolbar
          title={strings('changePassword')}
          backgroundColor={ColorStyle.tabActive}
          textColor={ColorStyle.tabWhite}
        />
        <View
          style={{
            marginHorizontal: Styles.constants.marginHorizontalAll,
            marginVertical: 20,
          }}>
          <Text style={{...Styles.text.text14, color: ColorStyle.tabBlack}}>
            {strings('txtpass')}
          </Text>
        </View>
        {this.renderTextInputPassword(
          strings('place_pass'),
          this.state.password,
          'showPasswordCurrent',
          this.state.showPasswordCurrent,
          this.state.currentPassValid,
          text => {
            this.checkPassword(text);
          },
        )}
        <View
          style={{
            marginHorizontal: Styles.constants.marginHorizontalAll,
            marginVertical: 20,
          }}>
          <Text style={{...Styles.text.text14, color: ColorStyle.tabBlack}}>
            {strings('txtpass1')}
          </Text>
        </View>
        {this.renderTextInputPassword(
          strings('place_newpass'),
          this.state.newPass,
          'showPasswordNew',
          this.state.showPasswordNew,
          this.state.newPassValid,
          text => this.checkNewPassword(text),
        )}
        {this.renderTextInputPassword(
          strings('place_cofPass'),
          this.state.confirmPass,
          'showPasswordConfirm',
          this.state.showPasswordConfirm,
          this.state.confirmPassValid,
          text => this.checkConfirmPassword(text),
        )}
        <View style={{position: 'absolute', bottom: 20}}>
          <TouchableOpacity
            onPress={() => this.SavePassword()}
            style={[
              Styles.button.buttonLogin,
              {
                backgroundColor: ColorStyle.tabActive,
                borderRadius: 35,
                marginVertical: Styles.constants.X * 0.5,
                  borderColor: 'rgba(0, 0, 0, 0.2)',
                  shadowColor: 'rgba(0, 0, 0, 0.2)',
                  shadowOffset: { width: 0, height: 2 },
                  shadowOpacity: 1,
                  shadowRadius: 1,
                  elevation: 1,
              },
            ]}>
            <Text
              style={[
                Styles.text.text14,
                {color: ColorStyle.tabWhite, fontWeight: '500'},
              ]}>
              {strings('confirm').toUpperCase()}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.SavePassword()}
            style={[
              {
                alignItems: 'center',
                borderRadius: 35,
                  marginBottom:10,
              },
            ]}>
            <Text
              style={[
                Styles.text.text14,
                {color: ColorStyle.tabActive, fontWeight: '400'},
              ]}>
              {strings('forgotPassword')}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  renderTextInputPassword(
    label,
    value,
    showPassword,
    showPassword1,
    status,
    onChangeText,
  ) {
    return (
      <View
        style={{

          backgroundColor: ColorStyle.tabWhite,
          borderRadius: 5,
          marginTop: 8,
          width: Styles.constants.widthScreenMg24,
          marginHorizontal: Styles.constants.marginHorizontalAll,
        }}>
        <TextInput
          mode="flat "
          label={label}
          placeholderTextColor="white"
          value={value}
          editable={this.state.statusEdit}
          onChangeText={onChangeText}
          secureTextEntry={!showPassword1}
          right={
            <TextInput.Icon
              color={!status ? ColorStyle.tabActive : ColorStyle.tabBlack}
              name={showPassword1 ? 'eye' : 'eye-off'}
              onPress={() => this.setState({[showPassword]: !showPassword1})}
            />
          }
          underlineColor={'transform'}
          outlineColor={'green'}
          style={{
            color: !status ? ColorStyle.tabActive : ColorStyle.tabBlack,
            fontWeight: '500',
            borderRadius: 5,
            width: Styles.constants.widthScreenMg24,
            backgroundColor: ColorStyle.tabWhite,
            fontSize: 14,
              borderColor: 'rgba(0, 0, 0, 0.2)',
              shadowColor: 'rgba(0, 0, 0, 0.2)',
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 1,
              shadowRadius: 1,
              elevation: 1,
          }}
          theme={{
            colors: {
              text: ColorStyle.tabBlack,
              primary: !status ? ColorStyle.tabActive : ColorStyle.tabBlack,
            },
          }}
        />
      </View>
    );
  }
  SavePassword() {
   if(!this.changePassword()) return     SimpleToast.show(strings('registerPassword'));
    let params = {
      old_password: this.state.password,
      new_password: this.state.newPass,
    };
    AccountHandle.getInstance().changePassword(
      params,
      (isSuccess, dataResponse) => {
        console.log(dataResponse);
        if (dataResponse.code===0) {

             AsyncStorage.setItem(
                AppConstants.SharedPreferencesKey.userValidation,
                params.new_password,
                null,
            );
          ViewUtils.showAlertDialog(strings('changePasswordSuccess'));
          Actions.pop()
        } else {
          ViewUtils.showAlertDialog(strings('changePasswordFailed'), () => {

          });
        }
      },
    );
  }
  checkPassword(password) {
    this.setState({
      currentPassValid:
       password !== undefined && password.length > 6,
      password: password,
    });
  }
  checkNewPassword(newPass) {
    this.setState({
      newPassValid:
        newPass !== undefined && newPass.length > 5,
      newPass: newPass,
    });
  }
  checkConfirmPassword(confirmPass) {
    this.setState({
      confirmPassValid:
        confirmPass !== undefined &&
        confirmPass === this.state.newPass,
      confirmPass: confirmPass,
    });
  }
  changePassword() {
    if (
      !this.state.currentPassValid ||
      !this.state.newPassValid ||
      !this.state.confirmPassValid
    ) {
      ViewUtils.showAlertDialog(strings('dataInvalid'), null);
      return false;
    }
    return true
  }
}
