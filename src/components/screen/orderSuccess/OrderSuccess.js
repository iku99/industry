import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../resource/Styles';
import {EventRegister} from 'react-native-event-listeners';
import AppConstants from '../../../resource/AppConstants';
import GlobalInfo from '../../../Utils/Common/GlobalInfo';
import {strings} from '../../../resource/languages/i18n';
import ColorStyle from '../../../resource/ColorStyle';
import ImageHelper from '../../../resource/images/ImageHelper';
import {Actions} from 'react-native-router-flux';
export default class OrderSuccess extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
    };
  }
  componentDidMount() {}

  render() {
    let name = GlobalInfo.userInfo.full_name;
    return (
      <View
        style={{
          ...Styles.container,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text style={{...Styles.text.text18, color: ColorStyle.tabBlack}}>
          {strings('thank')},
          <Text style={{...Styles.text.text18, color: ColorStyle.tabActive}}>
              {' '}{name}
          </Text>
        </Text>
        <Text
          style={{
            ...Styles.text.text30,
            color: ColorStyle.tabActive,
            fontWeight: '700',
          }}>
          {strings('hello')}
        </Text>
        <Image
          source={ImageHelper.img_success}
          style={{
            width: Styles.constants.X * 5,
            height: Styles.constants.X * 5,
            marginVertical: Styles.constants.X,
          }}
        />
        <TouchableOpacity
          onPress={() => {
            Actions.reset('drawer');
          }}
          style={{
            ...Styles.button.buttonLogin,
            backgroundColor: ColorStyle.tabActive,
            borderColor: 'rgba(247, 108, 104, 0.431)',
            elevation: 2,
          }}>
          <Text
            style={{
              ...Styles.text.text15,
              fontWeight: '600',
              color: ColorStyle.tabWhite,
            }}>
            {strings('keep')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>{
             setTimeout(()=>{ Actions.jump('order')},5)
              Actions.reset('drawer')}
        }
          style={{
            ...Styles.button.buttonLogin,
            borderColor: ColorStyle.tabWhite,
            backgroundColor: ColorStyle.tabWhite,
            elevation: 2,
            marginTop: Styles.constants.marginTopAll,
          }}>
          <Text>{strings('checkOrder')}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
