import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import GroupStyle from '../GroupStyle';
import {Icon} from 'react-native-elements';
import {strings} from '../../../../resource/languages/i18n';
import Styles from '../../../../resource/Styles';
import DataUtils from '../../../../Utils/DataUtils';
import constants from '../../../../Api/constants';
import ColorStyle from '../../../../resource/ColorStyle';
import {Dropdown} from 'react-native-material-dropdown-v2';
import DateUtil from '../../../../Utils/DateUtil';
import GroupHandle from '../../../../sagas/GroupHandle';
import ViewUtils from '../../../../Utils/ViewUtils';
import AppConstants from '../../../../resource/AppConstants';
import {Actions} from 'react-native-router-flux';
let alreadyGetRole = false;
let role;
export default class DetailMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role: '',
      checkMember: true,
      roleList: [
        {
          id: AppConstants.ROLE.ADMIN,
          name: strings('admin'),
        },
        {
          id: AppConstants.ROLE.MEMBER,
          name: strings('member1'),
        },
      ],
      selectedRole: undefined,
      showRole: false,
    };
  }
  componentDidMount() {
    this.setState({
      role:
        this.props.item?.role === AppConstants.ROLE.ADMIN
          ? strings('admin')
          : strings('member1'),
    });
    // this.getRoles();
  }
  render() {
    let item = this.props.item;
    return (
      <View style={GroupStyle.modal.container}>
        <View style={GroupStyle.modal.selectProductContainerContent}>
          <View
            style={{
              justifyContent: 'space-around',
              alignItems: 'center',
            }}>
            <Image
              source={{uri: this.getImageUrl(item.customers)}}
              style={{...Styles.icon.img_comment, borderRadius: 100}}
            />
            <Text
              style={{
                ...Styles.text.text18,
                color: ColorStyle.tabBlack,
                fontWeight: '500',
                marginTop: 8,
              }}>
              {item.customers.full_name}
            </Text>
            <View style={{flexDirection: 'row', width: '90%'}}>
              <Dropdown
                valueExtractor={item => item.name}
                onChangeText={(v, i, data) => {
                  this.updateRole(data[i]);
                }}
                containerStyle={{flex: 3}}
                value={this.state.role}
                labelFontSize={13}
                fontSize={14}
                data={this.state.roleList}
              />

              <TouchableOpacity
                style={{
                  flex: 1,
                  backgroundColor: ColorStyle.colorPersonal,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 5,
                  marginVertical: 12,
                  borderColor: 'rgba(0, 0, 0, 0.1)',
                  elevation: 2,
                  marginHorizontal: 6,
                }}>
                <Icon name={'message1'} type={'antdesign'} size={20} />
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flex: 1,
                  backgroundColor: ColorStyle.colorPersonal,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 5,
                  marginVertical: 12,
                  borderColor: 'rgba(0, 0, 0, 0.1)',
                  elevation: 2,
                  marginHorizontal: 6,
                }}>
                <Icon name={'user'} type={'antdesign'} size={20} />
              </TouchableOpacity>
            </View>
            <View
              style={{
                width: '90%',
                borderWidth: 1,
                borderBottomColor: ColorStyle.gray,
              }}
            />

            <View
              style={{
                width: '90%',
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 18,
              }}>
              <Icon
                name="account-group-outline"
                type="material-community"
                size={30}
              />
              <Text
                style={{
                  ...Styles.text.text18,
                  color: ColorStyle.tabBlack,
                  fontWeight: '400',
                  marginLeft: 4,
                }}>
                {item.full_name} {strings('joinFrom')}{' '}
                {DateUtil.formatDate('DD/MM/YYYY', item.join_at)}
              </Text>
            </View>

            <TouchableOpacity
              onPress={() => {
                this.saveMember();
              }}
              style={{
                width: '45%',
                backgroundColor: ColorStyle.tabActive,
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                paddingVertical: 13,
                borderRadius: 5,
                marginVertical: 12,
              }}>
              <Text
                style={{
                  ...Styles.text.text18,
                  color: ColorStyle.tabWhite,
                  fontWeight: '700',
                }}>
                {strings('save')}
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <TouchableOpacity
          style={GroupStyle.modal.closeButton}
          onPress={() => {
            this.closeView();
          }}>
          <Icon
            name="closecircleo"
            type="antdesign"
            size={25}
            color={ColorStyle.tabActive}
          />
        </TouchableOpacity>
      </View>
    );
  }
  // getRoles() {
  //   let param = {
  //     current_page: 0,
  //     page_size: 100,
  //   };
  //   GroupHandle.getInstance().getRoleOfGroup(
  //     this.props.groupId,
  //     param,
  //     (isSuccess, dataResponse) => {
  //       // console.log(dataResponse);
  //       // if (this.state.showRole) {
  //       //   return;
  //       // }
  //       if (
  //         isSuccess &&
  //         dataResponse.data !== undefined &&
  //         dataResponse.data.list !== undefined
  //       ) {
  //         alreadyGetRole = true;
  //         let data = dataResponse.data.list;
  //         let roleList = [];
  //         for (let i = 0; i < data.length; i++) {
  //           let selectedRole = {
  //             value: data[i].id,
  //             label: data[i].name,
  //           };
  //           roleList.push(selectedRole);
  //         }
  //         this.setState({showRole: true, roleList});
  //       } else {
  //         ViewUtils.showAlertDialog(strings('cannotGetRoleOfGroup', null));
  //       }
  //     },
  //   );
  // }
  updateRole(newRole) {
    this.setState({selectedRole: newRole});
  }
  saveMember() {
    setTimeout(() => {
      let params = {
        new_role: this.state.selectedRole.id,
        team_id: this.props.item.team_id,
      };
      GroupHandle.getInstance().updateMemberRoleGroup(
        this.props.item.id,
        params,
        (isSuccess, dataResponse) => {
          console.log('dataResponse:', dataResponse);
          if (isSuccess && dataResponse.data) {
            this.closeView();
            Actions.pop();
            this.props.onPostProduct();
            return;
          }
          this.setState({committingData: false}, () =>
            ViewUtils.showAlertDialog(strings('cannotUpdateMemberRole'), null),
          );
        },
      );
    }, 300);
  }
  closeView() {
    if (this.props.onClose !== undefined) {
      this.props.onClose();
    }
  }
  getImageUrl(item, index) {
    let url = item.avatar;
    return DataUtils.stringNullOrEmpty(url)
      ? 'https://picsum.photos/50/50?avatar' + this.props.index
      : constants.host + url;
  }
}
