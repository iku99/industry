import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';

let X = Styles.constants.X;

export default {
  content: {
    flex: 1,
    height: X*3.5,
    borderRadius: 7,
    paddingHorizontal: 20,
    paddingBottom: 12,
    textAlignVertical: 'top',
    color: 'black',
    marginTop: 5,
    fontSize: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    backgroundColor: ColorStyle.tabWhite,
    elevation: 2,
    margin:X/5
  },
  titleView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    flex: 1,
  },
  title: {
    fontSize: 18,
    paddingLeft: 5,
    color: ColorStyle.selectedItemBg,
  },
  buttonPost:{
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingVertical:5,
    backgroundColor: ColorStyle.tabActive,
    borderRadius: 5
  }
};
