import React, {Component} from 'react';
import {PERMISSIONS} from 'react-native-permissions';
import GlobalInfo from '../../../../Utils/Common/GlobalInfo';
import {
  ActivityIndicator,
  CameraRoll,
  FlatList,
  Image,
  Modal,
  PermissionsAndroid,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {strings} from '../../../../resource/languages/i18n';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import ViewUtils from '../../../../Utils/ViewUtils';
import ImageGrid from '../../../elements/ImageGrid';
import ViewProductItem from '../../../elements/viewItem/ViewedProduct/ViewProductItem';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import GroupHandle from '../../../../sagas/GroupHandle';
import AppConstants from '../../../../resource/AppConstants';
import DataUtils from '../../../../Utils/DataUtils';
import PermissionUtils from '../../../../Utils/PermissionUtils';
import ImagePicker from 'react-native-image-crop-picker';
import MediaHandle from '../../../../sagas/MediaHandle';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
import PostStyle from './PostStyle';
import {EventRegister} from 'react-native-event-listeners';
import LoadingView from '../../../elements/LoadingView';
import MediaUtils from '../../../../Utils/MediaUtils';

const Data = [
  {
    type: AppConstants.AddPost.TITLE,
  },
  {
    type: AppConstants.AddPost.TEXT_INPUT,
  },
  {
    type: AppConstants.AddPost.BTN,
  },
  {
    type: AppConstants.AddPost.LIST_IMAGE,
  },
];
let numberOfLines = 7;
export default class AddPostToGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: '',
      images: [],
      avatar: undefined,
      showSelectProduct: false,
      products: [],
      userInfo: undefined,
      imageGalleries: [],
      userId: undefined,
      loading: false,
    };
  }
  async componentDidMount() {
    GlobalInfo.getUserInfo().then(userInfo => {
      this.setState({userId: userInfo.id});
      this.setState({userInfo: userInfo});
    });

    EventRegister.addEventListener(
      AppConstants.EventName.CHANGE_AVATAR,
      data => {
        this.setState({userInfo: {}}, () => {
          this.setState({userInfo: data});
        });
      },
    );
    if (Platform.OS === 'android') {
      const result = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: 'Permission Explanation',
          message: 'ReactNativeForYou would like to access your photos!',
        },
      );
      if (result !== 'granted') {
        console.log('Access to pictures was denied');
        return;
      }
    }
    CameraRoll.getPhotos({
      first: 50,
      assetType: 'Photos',
    })
      .then(r => {
        this.setState({imageGalleries: r.edges});
      })
      .catch(err => {
        //Error Loading Images
      });
  }
  render() {
    return (
      <View style={Styles.container}>
        {this.toolbar()}
        <FlatList
          data={Data}
          renderItem={this.renderItem}
          style={{width: '100%', height: '100%'}}
        />
        <LoadingView loading={this.state.loading} />
      </View>
    );
  }
  renderItem = ({item, index}) => {
    switch (item.type) {
      case AppConstants.AddPost.TITLE:
        return this.renderHeader();
      case AppConstants.AddPost.TEXT_INPUT:
        return this.renderTextInput();
      case AppConstants.AddPost.BTN:
        return this.renderBtn();
      case AppConstants.AddPost.LIST_IMAGE:
        return this.renderListImage();
    }
  };
  renderHeader() {
    let userInfo = this.state.userInfo;
    let canPost = this.state.content.length > 0;
    return (
      <View
        style={{
          flexDirection: 'row',
          padding: 20,
          alignItems: 'center',
        }}>
        <View
          style={{
            flex: 5,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Image
            source={MediaUtils.getAvatar(this.state.userInfo)}
            style={{
              ...Styles.icon.iconShow,
              borderRadius: 50,
            }}
          />
          <View style={{marginLeft: 10, justifyContent: 'space-between'}}>
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabBlack,
                fontWeight: '700',
              }}>
              {userInfo?.full_name}
            </Text>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
                marginTop: Styles.constants.X / 5,
              }}>
              (
              {userInfo?.id === this.props.item.owner
                ? strings('owner')
                : strings('member')}
              )
            </Text>
          </View>
        </View>
        <TouchableOpacity
          style={PostStyle.buttonPost}
          onPress={() => {
            if (canPost) {
              this.onPost();
            } else {
              ViewUtils.showAlertDialog(
                strings('pleaseEnterContent'),
                undefined,
              );
            }
          }}>
          <Text
            style={{
              ...Styles.text.text18,
              color: ColorStyle.tabWhite,
              fontWeight: '700',
            }}>
            {strings('postProductFunc')}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
  renderBtn() {
    return (
      <View
        style={{
          flexDirection: 'row',
          height: Styles.constants.X * 1.3,
          justifyContent: 'flex-end',
          backgroundColor: ColorStyle.tabWhite,
          paddingVertical: 10,
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => {
            this.onSelectCamera();
          }}>
          <Icon
            name="camera"
            type="feather"
            size={30}
            color={ColorStyle.tabBlack}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{marginHorizontal: 30}}
          onPress={() => {
            this.onSelectImage();
          }}>
          <Icon
            name={'image'}
            type={'evilicons'}
            size={30}
            color={ColorStyle.tabBlack}
          />
        </TouchableOpacity>
      </View>
    );
  }
  renderTextInput() {
    let images = this.state.images;
    let imagePaths = [];
    images.forEach(image => {
      if (
        image.item !== undefined &&
        image.item.node !== undefined &&
        image.item.node.image !== undefined &&
        image.item.node.image.uri !== undefined
      ) {
        imagePaths.push(image.item.node.image.uri);
      }
    });
    let lineNumber = numberOfLines;
    if (imagePaths.length > 0 || this.state.products.length > 0) {
      lineNumber = 1;
    }
    return (
      <View>
        <TextInput
          style={PostStyle.content}
          value={this.state.content}
          autoCorrect={false}
          placeholderTextColor={'#000'}
          onChangeText={content => {
            this.setState({content});
          }}
          ref={ref => {
            this.txtContent = ref;
          }}
          multiline={true}
          numberOfLines={lineNumber}
          minHeight={20 * lineNumber}
          underlineColorAndroid="transparent"
          placeholder={strings('enterContent')}
        />
        {imagePaths.length > 0 && (
          <Text style={{paddingHorizontal: 10}}>{strings('postImage')}</Text>
        )}
        <ImageGrid
          style={{
            width: Styles.constants.widthScreen / 2,
            aspectRatio: 1,
            marginTop: 10,
          }}
          images={imagePaths}
          onPress={(image, index) => {
            this.imageOnPress(imagePaths, index);
          }}
        />
      </View>
    );
  }
  renderListImage() {
    return (
      <FlatList
        style={{width: '50%', height: '40%'}}
        data={this.state.imageGalleries}
        numColumns={4}
        renderItem={({item, index}) => (
          <TouchableOpacity
            style={{
              flex: 1,
              aspectRatio: 1,
              margin: 0.5,
            }}
            onPress={() => {
              this.onSelectImageGallery(item, index);
            }}>
            <Image
              style={{
                width: '100%',
                height: '100%',
              }}
              source={{uri: item.node.image.uri}}
            />
            <Icon
              containerStyle={{
                position: 'absolute',
                top: 8,
                right: 8,
              }}
              name={item.check ? 'ios-radio-button-on' : 'ios-radio-button-off'}
              type="ionicon"
              size={25}
            />
          </TouchableOpacity>
        )}
      />
    );
  }
  renderFooter() {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (this.state.loading) {
      return null;
    }
    return (
      <View style={{position: 'absolute', width: '100%', height: '100%'}}>
        <ActivityIndicator style={{color: '#000'}} />
      </View>
    );
  }
  toolbar() {
    return (
      <View
        style={{
          backgroundColor: ColorStyle.tabActive,
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          paddingTop: Styles.constants.X,
          paddingHorizontal: Styles.constants.X * 0.65,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-start',
        }}>
        <TouchableOpacity
          onPress={() => Actions.pop()}
          style={{
            alignSelf: 'flex-start',
            paddingBottom: 10,
          }}>
          <Icon
            name="left"
            type="antdesign"
            size={25}
            color={ColorStyle.tabWhite}
          />
        </TouchableOpacity>
      </View>
    );
  }
  keyExtractor = (item, index) => `addPostToGroup_${index.toString()}`;
  _renderItem = ({item, index}) => (
    <ViewProductItem
      item={item}
      index={index}
      onClick={item => {
        NavigationUtils.goToProductDetail(item);
      }}
    />
  );
  onPost() {
    let msg = '';
    let image = this.state.images;
    this.setState({loading: true});
    if (image.length !== 0) {
      MediaHandle.getInstance().uploadImageFile(
        image,
        progress => {},
        (isSuccess, responseData) => {
          let params = {
            team_id: this.props.item.id,
            caption: '',
            content: this.state.content,
            image_url: JSON.parse(responseData).url,
          };
          GroupHandle.getInstance().postPostToGroup(
            params,
            (isSuccess, responseData) => {
              this.setState({loading: false});
              if (isSuccess) {
                ViewUtils.showAlertDialog(
                  strings(
                    this.props.item.owner === this.state.userId
                      ? 'postGroupPostSuccess'
                      : 'postGroupPostSuccess1',
                  ),
                  () => {
                    this.props.callBack();
                    Actions.pop();
                  },
                );
                return;
              } else if (
                responseData !== undefined &&
                AppConstants.ERROR_CODE.INVALID_DATA === responseData.code
              ) {
                msg = strings('productNotActive');
              } else {
                msg = strings('cannotPost');
              }
              ViewUtils.showAlertDialog(msg, undefined);
            },
          );
        },
      );
    } else {
      let params = {
        team_id: this.props.item.id,
        caption: '',
        content: this.state.content,
        image_url: null,
      };
      GroupHandle.getInstance().postPostToGroup(
        params,
        (isSuccess, responseData) => {
          this.setState({loading: false});
          if (isSuccess) {
            ViewUtils.showAlertDialog(
              strings(
                this.props.item.owner === this.state.userId
                  ? 'postGroupPostSuccess'
                  : 'postGroupPostSuccess1',
              ),
              () => {
                this.props.callBack();
                Actions.pop();
              },
            );
            return;
          } else if (
            responseData !== undefined &&
            AppConstants.ERROR_CODE.INVALID_DATA === responseData.code
          ) {
            msg = strings('productNotActive');
          } else {
            msg = strings('cannotPost');
          }
          ViewUtils.showAlertDialog(msg, undefined);
        },
      );
    }
  }
  uploadImg(image) {
    let mData = [];
    MediaHandle.getInstance().uploadImageFile(
      image,
      progress => {},
      (isSuccess, responseData) => {
        JSON.parse(responseData).url.filter(item => mData.push(item));
      },
    );
  }
  onSelectCamera() {
    if (PermissionUtils.checkPermission(PERMISSIONS.IOS.CAMERA)) {
      ImagePicker.openCamera({
        width: 500,
        height: 500,
        cropping: true,
      }).then(image => {
        let imageGalleries = this.state.imageGalleries;
        let item = {
          node: {
            image: {
              uri: image.path,
            },
          },
        };
        imageGalleries.push(item);
        this.setState({imageGalleries}, () =>
          this.onSelectImageGallery(item, imageGalleries.length - 1),
        );
      });
    }
  }
  onSelectImage() {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: true,
      // multiple: true
    }).then(image => {
      let imageGalleries = this.state.imageGalleries;
      let item = {
        node: {
          image: {
            uri: image.path,
          },
        },
      };

      imageGalleries.push(item);
      this.setState({imageGalleries}, () =>
        this.onSelectImageGallery(item, imageGalleries.length - 1),
      );
    });
  }
  imageOnPress(imagePaths, index) {
    NavigationUtils.goToFullViewImageList(imagePaths, index, currentIndex => {
      let images = this.state.images;
      images = DataUtils.removeItem(images, currentIndex);
      this.setState({images});
    });
  }
  onSelectImageGallery(item, index) {
    let imageGalleries = this.state.imageGalleries;
    let images = this.state.images;
    item = {
      ...item,
      check: item.check !== undefined ? !item.check : true,
    };
    if (item.check) {
      images.push({
        item,
        index,
      });
    } else {
      let size = images.length;
      for (let i = 0; i < size; i++) {
        let image = images[i];
        if (image.index === index) {
          images = DataUtils.removeItem(images, i);
          break;
        }
      }
    }
    imageGalleries[index] = item;
    this.setState({imageGalleries, images});
  }
  getAvatar(item, index) {
    let avatar = '';
    if (item !== undefined) {
      avatar = GlobalInfo.initApiEndpoint(item.avatar);
    } else {
      avatar = 'https://picsum.photos/84/84?' + index;
    }
    return avatar;
  }
}
