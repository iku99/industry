import React, {Component} from 'react';
import {
  FlatList,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Styles from '../../../../resource/Styles';
import Toolbar from '../../../elements/toolbar/Toolbar';
import ColorStyle from '../../../../resource/ColorStyle';
import {strings} from '../../../../resource/languages/i18n';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import constants from '../../../../Api/constants';
import MediaUtils from '../../../../Utils/MediaUtils';
import TimeUtils from '../../../../Utils/TimeUtils';
import DataUtils from '../../../../Utils/DataUtils';
import CategoryHandle from '../../../../sagas/CategoryHandle';
export default class MemberScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
    };
  }

  render() {
    return (
      <View style={Styles.container}>
        {this.renderToolbar()}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingHorizontal: 30,
            paddingVertical: 28,
            backgroundColor: ColorStyle.tabWhite,
            borderColor: ColorStyle.borderItemHome,
            elevation: 2,
          }}>
          <TextInput
            style={[
              Styles.input.inputLogin,
              {
                width: '90%',
                borderColor: ColorStyle.gray,
                borderWidth: 1,
                backgroundColor: 'transparent',
              },
            ]}
            placeholder={strings('textMember')}
            value={this.state.name}
            placeholderTextColor={ColorStyle.textInput}
            onChangeText={value => {
              this.queryCategory(value);
            }}
          />
          <TouchableOpacity>
            <Icon name={'search1'} type={'antdesign'} size={25} />
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 1,
            backgroundColor: ColorStyle.tabWhite,
            marginTop: 16,
            borderColor: ColorStyle.borderItemHome,
            elevation: 2,
            padding: 24,
          }}>
        </View>
      </View>
    );
  }
  renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginVertical: 12,
          paddingVertical: 5,
        }}>
        <View style={{flex: 1}}>
          <Image
            source={{uri: this.getImageUrl(item)}}
            style={{...Styles.icon.iconShow, borderRadius: 50}}
          />
        </View>
        <View style={{flex: 4, marginLeft: 12}}>
          <Text
            style={{
              ...Styles.text.text14,
              color: ColorStyle.tabBlack,
              fontWeight: '700',
            }}>
            {item.full_name}
          </Text>
          <Text style={{...Styles.text.text14, color: ColorStyle.gray}}>
            {strings('joinFrom')}
            {TimeUtils.formatDay(item.join_at)}
          </Text>
        </View>
        <Icon
          name={'user-cog'}
          type="font-awesome-5"
          style={{display: item.owner_group ? 'flex' : 'none'}}
        />
      </TouchableOpacity>
    );
  };

  renderToolbar() {
    return (
      <View
        style={[
          {
            backgroundColor: ColorStyle.tabActive,
            paddingBottom: 15,
            paddingTop: Styles.constants.X,
            paddingHorizontal: Styles.constants.X * 0.45,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          },
        ]}>
        <TouchableOpacity onPress={() => Actions.pop()}>
          <Icon
            name="left"
            type="antdesign"
            size={25}
            color={ColorStyle.tabWhite}
          />
        </TouchableOpacity>
        <Text
          style={[
            Styles.text.text20,
            {
              color: ColorStyle.tabWhite,
              fontWeight: '700',
              textAlign: 'center',
            },
          ]}
          numberOfLines={1}>
          {strings('members')}
        </Text>
        <TouchableOpacity onPress={this.props.onPress}>
          <Icon
            name={'adduser'}
            type="antdesign"
            size={25}
            color={ColorStyle.tabWhite}
          />
        </TouchableOpacity>
      </View>
    );
  }
  queryCategory(query) {
    let querys = this.props.data;
    setTimeout(() => {
      for (let i = 0; i < querys.length; i++) {
        if (querys[i].id === 260) {
        }
      }
    }, 300);
  }
  getImageUrl(item, index) {
    let url = item.avatar;
    return DataUtils.stringNullOrEmpty(url)
      ? 'https://picsum.photos/50/50?avatar' + this.props.index
      : constants.host + url;
  }
}
