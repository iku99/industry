import {Platform} from 'react-native';
import Styles from '../../../resource/Styles';
import ColorStyle from '../../../resource/ColorStyle';
let X = Styles.constants.X;
let isAndroid = Platform.OS === 'android';
export default {
  bannerGroup: {
    width: Styles.constants.widthScreenMg24,
    height: Styles.constants.heightScreen / 3,
  },
  bannerGroupDetail: {
    width: Styles.constants.widthScreen,
    height: Styles.constants.X * 6.5,
  },
  buttonCre: {
    width: Styles.constants.widthScreen / 2,
    height: 50,
    marginTop: 40,
    backgroundColor: ColorStyle.tabActive,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    elevation: 1,
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 1,
  },
  ViewImage:{
    alignItems: 'center',
    justifyContent:'center',
    paddingTop: 20,
  },
  buttonCam: {
    width: X * 2,
    height: X * 2,
    borderRadius: X * 3,
    shadowColor: "#000",
    backgroundColor:ColorStyle.tabWhite,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.4,
    shadowRadius: 2.22,
    elevation: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imagesAvatar: {
    width: X * 1.5,
    height: X * 1.5,
    borderRadius: X * 3,
  },
  optionRow: {
    width: '100%',
    height: X * 1.6,
    flexDirection: 'row',
    backgroundColor: ColorStyle.tabWhite,
    borderColor: ColorStyle.borderItemHome,
    elevation: 2,
    shadowColor: ColorStyle.borderItemHome,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 1,
  },
  groupPost: {
    width: '100%',
    height: X * 1.6,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems:'center',
    paddingHorizontal: 16,
    backgroundColor: ColorStyle.tabWhite,
    marginTop: 8,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    marginBottom: X/5,
    elevation: 3,
  },
  itemOption: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  itemComment: {
    flex: 4,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  viewComment: {
    width: '100%',
    height: X * 0.9,
    alignItems: 'flex-start',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    elevation: 2,
    backgroundColor:ColorStyle.tabWhite,
    borderRadius: 10,

  },
  groupPost1: {
    paddingTop: X /6,
    width: '100%',
    marginBottom: X/5,
    backgroundColor:ColorStyle.tabWhite,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  itemSelection: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',


  },
  itemTabManage: {
    button: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 24,
      backgroundColor: ColorStyle.tabWhite,
      borderRadius: 15,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,

      elevation: 5,
      marginTop: 12,

    },
    text: {
      ...Styles.text.text14,
      color: ColorStyle.tabBlack,
      fontWeight: '500',
      marginLeft: 5,
    },
  },
  modal: {
    container: {

      backgroundColor: 'white',
      borderRadius: 6,
      overflow: 'hidden',
    },
    selectProductContainerContent: {
      paddingTop: 35,
      paddingHorizontal: 10,
    },
    closeButton: {
      position: 'absolute',
      top: 10,
      right: 10,
    },
  },
  information: {
    container: {
      backgroundColor: ColorStyle.tabWhite,
      marginHorizontal: 10,
      marginVertical: 12,
      padding: 24,
      borderRadius:10,
      borderColor:ColorStyle.borderItemHome,
      elevation:5,
      shadowColor: ColorStyle.borderItemHome,
      shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.8,
      shadowRadius: 1,
    },
  },
  textInputName:{
    ...Styles.input.codeInput,
    width:Styles.constants.widthScreenMg24,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    backgroundColor: ColorStyle.tabWhite,
    elevation: 2,
  },
  textInputDes:{
    ...Styles.input.codeInput,
    paddingVertical: 10,
    paddingLeft: 10,
    width:Styles.constants.widthScreenMg24,
    height:Styles.constants.X*3,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    backgroundColor: ColorStyle.tabWhite,
    elevation: 2,
  },
  btnSearch:{
    width:'10%',
    backgroundColor:ColorStyle.tabActive,
    height:Styles.constants.X ,
    alignItems:'center',
    justifyContent:'center'
  },
  bodySearch :{
      width: '95%',
      height: X,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems:'center',
      backgroundColor: ColorStyle.tabWhite,
      overflow: 'hidden',
    borderRadius:10,
      marginTop: 8,
    borderColor:ColorStyle.borderItemHome,
    borderWidth:1
    },
    viewSelect:{
      flexDirection: 'row',
      backgroundColor:ColorStyle.tabWhite,
      borderTopColor:ColorStyle.borderItemHome,
      borderTopWidth:1,
       flex: 1, paddingHorizontal: X/6,
      paddingVertical: X/4
    }

};
