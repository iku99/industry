import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import AppConstants from '../../../../resource/AppConstants';
import {strings} from '../../../../resource/languages/i18n';
import NewGroupStyle from './NewGroupStyle';
import GroupStyle from '../GroupStyle';
import ImageHelper from '../../../../resource/images/ImageHelper';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
import {Actions} from 'react-native-router-flux';

export default class SuccessGroup extends Component {
  render() {
    return (
      <View
        style={{
          paddingHorizontal: 5,
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
          <Image
              source={ImageHelper.img_success}
              style={{
                  width: Styles.constants.X * 5,
                  height: Styles.constants.X * 5,
                  marginVertical: Styles.constants.X,
              }}
          />
        <Text
          style={[
            GroupStyle.titleText,
            {paddingHorizontal: 50, textAlign: 'center', paddingTop: 30},
          ]}>
          {strings('successfulGroup')}
        </Text>
        <TouchableOpacity
          style={{
            ...Styles.button.buttonLogin,
            position: 'absolute',
            bottom: 20,
            backgroundColor: ColorStyle.tabActive,
          }}
          onPress={() =>{
              setTimeout(()=>{   Actions.jump('groupDetail', {item: this.props.item, checkRole: 1})},5)
              Actions.reset('drawer')}
          }>
          <Text style={{color: 'white', fontSize: 14}}>
            {strings('goToGroup')}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
