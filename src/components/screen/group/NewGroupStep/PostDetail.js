import React, {Component} from 'react';
import {
  FlatList,
  Keyboard,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import AppConstants from '../../../../resource/AppConstants';
import {strings} from '../../../../resource/languages/i18n';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import ViewUtils from '../../../../Utils/ViewUtils';
import GroupPostRow from '../groupDetail/row/GroupPostRow';
import GroupPostHandle from '../../../../sagas/GroupPostHandle';
import DataUtils from '../../../../Utils/DataUtils';
import SimpleToast from 'react-native-simple-toast';
import CommentItem from '../../../elements/CommentItem';
import GlobalInfo from '../../../../Utils/Common/GlobalInfo';
import MDIcon from 'react-native-vector-icons/MaterialIcons';
import RBSheet from 'react-native-raw-bottom-sheet';
import Toolbar from '../../../elements/toolbar/Toolbar';
import ColorStyle from '../../../../resource/ColorStyle';
import Styles from '../../../../resource/Styles';
let currentPosition = 0;
let LIMIT = 20;

let infoItem = {
  itemType: AppConstants.POST_DETAIL_TYPE.INFO,
};
let loadMoreItem = {
  itemType: AppConstants.POST_DETAIL_TYPE.LOAD_MORE,
};

let commentItems = [];
let canloadMore = false;
let commentOptions = [
  {
    id: 1,
    icon: 'delete-forever',
    label: strings('deleteComment'),
  },
  {
    id: 2,
    icon: 'edit',
    label: strings('editComment'),
  },
];
let comment;
let size = 500;
let numberOfLines = 5;
export default class PostDetail extends Component {
  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow() {
    this.setState({keyboardShow: true, editViewHeight: 600, bottomMargin: 40});
  }

  _keyboardDidHide() {
    this.setState({keyboardShow: false, editViewHeight: 250, bottomMargin: 10});
  }
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
      page: 0,
      refreshing: false,
      showFloatingButton: false,
      canLoadData: true,
      finishLoad: false,
      post: this.props.post,
      commentText: '',
      keyboardShow: false,
      editComment: '',
      editViewHeight: 250,
      bottomMargin: 10,
    };
  }
  componentDidMount() {
    this.viewPost()
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardWillShow',
      () => this._keyboardDidShow(),
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardWillHide',
      () => this._keyboardDidHide(),
    );
    infoItem = {
      ...infoItem,
      ...this.props.post,
    };
    commentItems = [];
    this.getComments();
     this.getPostDetail()
  }
  render() {
    return (
      <View style={Styles.container}>
        <Toolbar
          title={strings('postDetail')}
          iconBack={true}
          backgroundColor={ColorStyle.tabActive}
          textColor={ColorStyle.tabWhite}
        />
        <View>
          {this.props.deleteFunc !== undefined && (
            <View>
              {!this.state.isGroupDes && (
                <TouchableOpacity
                  onPress={() => {
                    ViewUtils.showAskAlertDialog(
                      strings('areYouSureDeletePost'),
                      () => {
                        this.props.deleteFunc();
                        Actions.pop();
                      },
                      null,
                    );
                  }}>
                  <View>
                    <Icon name="trash" type="feather" size={25} />
                  </View>
                </TouchableOpacity>
              )}
            </View>
          )}
        </View>
          <FlatList
            onScrollBeginDrag={() => Keyboard.dismiss()}
            keyboardShouldPersistTaps="handled"
            nestedScrollEnabled={true}
            contentContainerStyle={{
              paddingBottom: 10,
              backgroundColor: 'transparent',
            }}
            showsVerticalScrollIndicator={false}
            data={this.state.data}
            keyExtractor={this.keyExtractor}
            renderItem={this._renderItem}
            style={{flex: 1, backgroundColor: 'transparent'}}
            ref={ref => {
              this.listRef = ref;
            }}
            refreshing={this.state.refreshing}
            onMomentumScrollEnd={() => {
              this.onScrollEnd();
            }}
            onViewableItemsChanged={this.onViewableItemsChanged}
          />

          {this.renderCommentView()}
        {this.state.showFloatingButton && (
          <TouchableOpacity
            style={{bottom: 90}}
            onPress={() => {
              this.scrollToTop();
            }}>
            <Icon name="ios-arrow-up" type="ionicon" size={30} />
          </TouchableOpacity>
        )}
        {this.renderEditComment()}
        {this.renderOption()}
      </View>
    );
  }
  keyExtractor = (item, index) => `post_Detail${index.toString()}`;
  _renderItem = ({item, index}) => <View>{this.renderItem(item, index)}</View>;
  onViewableItemsChanged = info => {
    let viewableItems = info.viewableItems;
    if (viewableItems.length > 0) {
      currentPosition = viewableItems[0].index;
    }
  };
  renderItem(item, index) {
    switch (item.itemType) {
      case AppConstants.POST_DETAIL_TYPE.INFO:
        return (
          <GroupPostRow
            index={index}
            item={item}
            ref={ref => {
              this.groupInfo = ref;
            }}
            lengthData={true}
            groupId={this.props.groupId}
            groupName={this.props.groupName}
            onChangeItemData={newItem => {
              if (this.props.onChangeItemData !== undefined) {
                this.props.onChangeItemData(newItem, this.props.index);
              }
            }}
          />
        );
      case AppConstants.POST_DETAIL_TYPE.LOAD_MORE:
        return (
          <TouchableOpacity
            onPress={() => {
              // this.getComments();
            }}>
            <Text>{strings('loadMoreComment')}</Text>
          </TouchableOpacity>
        );
      default:
        return (
          <CommentItem
            index={index}
            item={item}
            onOptionClick={() => {
              comment = item;

              if (item.customer_id === GlobalInfo.userInfo.id) {
                this.Standard.open();
              }
            }}
          />
        );
    }
  }
  renderOption() {
    return (
      <RBSheet
        customStyles={{
          container: {
            borderTopLeftRadius: 15,
            borderTopRightRadius: 15,
            overflow: 'hidden',
          },
        }}
        ref={ref => {
          this.Standard = ref;
        }}
        height={220}
        closeOnDragDown={true}
        closeDuration={150}
        onClose={() => {}}>
        <View style={{paddingHorizontal:20}}>
          <Text style={{...Styles.text.text20,color:ColorStyle.tabActive,fontWeight:'600',alignSelf:'center',marginVertical:10}}>{strings('selectOption')}</Text>
          {commentOptions.map((list, index) => (
            <TouchableOpacity
                style={{flexDirection:'row',alignItems:'center',justifyContent:'flex-start',paddingVertical:10}}
              key={list.icon}
              onPress={() => {
                this.Standard.close();
                setTimeout(() => {
                  this.optionCommentSelect(index);
                }, 200);
              }}>
              <MDIcon name={list.icon} size={25}/>
              <Text style={{...Styles.text.text16,color:ColorStyle.tabBlack,marginLeft:5}}>{list.label}</Text>
            </TouchableOpacity>
          ))}
        </View>
      </RBSheet>
    );
  }
  renderEditComment() {
    return (
      <RBSheet
        customStyles={{
          container: {
            backgroundColor: 'white',
            overflow: 'hidden',
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
          },
        }}
        keyboardAvoidingViewEnabled={false}
        height={200}
        closeOnDragDown={true}
        ref={ref => {
          this.EditComment = ref;
        }}
        openDuration={0}
        closeDuration={0}
        onOpen={() => {
          setTimeout(() => {
            this.txtComment.focus();
          }, 200);
        }}>
        <View
          behavior={'padding'}
          enabled
          style={{padding: 10, backgroundColor: 'white', marginHorizontal: 10}}>
          <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
            <TouchableOpacity
              onPress={() => {
                this.editComment();
                Keyboard.dismiss();
                this.EditComment.close();
              }}>
              <Icon name="check" type="font-awesome" size={20} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                Keyboard.dismiss();
                this.setState({editComment: ''});
                setTimeout(() => {
                  this.EditComment.close();
                }, 200);
              }}>
              <Icon name="close" type="font-awesome" size={20} />
            </TouchableOpacity>
          </View>
          <TextInput
            value={this.state.editComment}
            autoCorrect={false}
            placeholderTextColor={'#000'}
            onChangeText={editComment => {
              this.setState({editComment});
            }}
            ref={ref => {
              this.txtComment = ref;
            }}
            style={{backgroundColor:ColorStyle.tabWhite,marginTop:10,borderRadius:3,borderColor:'black',borderWidth:1,paddingLeft:10,color:ColorStyle.tabBlack}}
            multiline={true}
            numberOfLines={numberOfLines}
            minHeight={20 * numberOfLines}
            underlineColorAndroid="transparent"
            placeholder={strings('enterComment')}
          />
        </View>
      </RBSheet>
    );
  }
  scrollToTop() {
    this.listRef.scrollToOffset({offset: 0, animated: true});
  }
  onScrollEnd() {
    this.setState({showFloatingButton: currentPosition > 1});
  }
  getComments() {
    let params = {
      post_id:this.state.post.id,
      page_index: this.state.page,
      page_size: LIMIT,
    };
    this.setState({page: this.state.page + 1}, () => {
      GroupPostHandle.getInstance().getPostsComment(
        params,
        (isSuccess, dataResponse) => {
          console.log(dataResponse)
          if (isSuccess && dataResponse.data !== undefined) {

            if (this.state.page === 0) {
              commentItems = [];
            }
            let items = dataResponse.data.data;
            canloadMore = items.length === LIMIT;
            items.forEach(item => {
              let newItem = {
                itemType: AppConstants.POST_DETAIL_TYPE.COMMENT,
                ...item,
                isLoadUserInfo: false,
              };
              commentItems = [newItem, ...commentItems];
            });
          }
          this.combineData(undefined);
        },
      );
    });
  }
  viewPost() {
    let params = {
      post_id:this.state.post.id
    };
    GroupPostHandle.getInstance().ViewPost(
        params,
        (isSuccess, dataResponse) => {}
    );
  }
  commentPost() {
    if (this.state.commentText.length === 0) {return}
    let comment = this.state.commentText;
    let params = {
      content:comment,
      post_id:this.props.post.id,
    };
    this.setState({commentText: ''});
    GroupPostHandle.getInstance().commentToPost(
      params,
      (isSuccess, dataResponse) => {
        if (isSuccess) {
          infoItem = {
            ...infoItem,
            total_comment:infoItem.total_comment+1,
          };

          let userInfo = GlobalInfo.userInfo;
          let customer={
            full_name:userInfo.full_name,
            id:userInfo.id,
            avatar:userInfo.avatar
          }
          let data=dataResponse.data
          let newItem = {
            ...data,
            customer,
          };
          // let newData={
          //   ...newItem,
          //   total_comment:newItem.le
          // }
          commentItems.push(newItem);
          this.combineData(() => {
            setTimeout(() => {
              this.listRef.scrollToEnd();
            }, 200);
            this.changeInfoItemData();
          });
        } else {
          ViewUtils.showAlertDialog(strings('commentPostError'), null);
        }
      },
    );
  }
  changeInfoItemData() {
    if (this.groupInfo !== undefined) {
      this.groupInfo.changeDataItem(infoItem);
    }
    if (this.props.onChangeItemData !== undefined) {
      this.props.onChangeItemData(infoItem, this.props.index);
    }
  }
  optionCommentSelect(actionId) {
    switch (actionId) {
      case 0:
        this.deleteComment();
        break;
      case 1:
        this.setState({editComment: comment.comment});
        // ViewUtil.showAlertDialog("Comming soon",null)
        this.EditComment.open();
        break;
      default:
        break;
    }
  }
  combineData(callback) {
    this.setState({data: this.combineCommentItems()}, () => {
      if (callback !== undefined) {
        callback();
      }
    });
  }
  getPostDetail(){
    GroupPostHandle.getInstance().getPostsDetail(this.state.post.id,(isSuccess, dataResponse) => {
      if (isSuccess) {
        this.setState({post:dataResponse.data})
      }
    })
  }
  deleteComment() {
    let param={
      team_id:infoItem.team_id,
      post_id:this.state.post.id
    }
    let mComment = comment;
    GroupPostHandle.getInstance().deleteCommentOfPost(
        comment.id,
      param,
      (isSuccess, dataResponse) => {
        if (isSuccess) {
          SimpleToast.show(strings('deleteCommentSuccess'), SimpleToast.SHORT);
          let size = commentItems.length;
          for (let i = 0; i < size; i++) {
            let comment = commentItems[i];
            if (comment.id === mComment.id) {
              commentItems = DataUtils.removeItem(commentItems, i);
              break;
            }
          }
          infoItem = {
            ...infoItem,
            total_comment:infoItem.total_comment-1,
          };
          this.setState({data: []}, () => {
            this.setState({data: this.combineCommentItems()});
            if (this.props.onChangeItemData !== undefined) {
              this.props.onChangeItemData(infoItem, this.props.index);
            }
          });
        } else {
          ViewUtils.showAlertDialog(strings('cannotDeleteComment'), null);
        }
      },
    );
  }
  editComment() {
    let mComment = comment;
    let commentStr = this.state.editComment;
    let params = {
      content: commentStr,

    };
    GroupPostHandle.getInstance().editCommentOfPost(
        comment.id,
      params,
      (isSuccess, dataResponse) => {
        if (dataResponse.code===1) {return;}
        let size = commentItems.length;
        for (let i = 0; i < size; i++) {
          let item = commentItems[i];
          if (item.id === mComment.id) {
            item = {
              ...item,
              content: commentStr,
            };
            commentItems[i] = item;
            break;
          }
        }
        this.setState({data: []}, () =>
          this.setState({data: this.combineCommentItems()}),
        );
      },
    );
  }
  combineCommentItems() {
    let data = [infoItem];
    let newData={
      ...infoItem,
      total_comment:infoItem.total_comment
    }
    if (canloadMore) {
      data = [...data, loadMoreItem];
    }
    data = [...data, ...commentItems];
    return data;
  }
  renderCommentView() {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent:'space-between',
          padding: Styles.constants.X/3,
          backgroundColor:ColorStyle.tabWhite,
          shadowColor: "#000",
          shadowOffset: {
            width: 1,
            height: 0,
          },
          shadowOpacity: 0.5,
          shadowRadius: 2.22,
          elevation: 3,
        }}>
        <TextInput
          value={this.state.commentText}
          autoCorrect={false}
          onChangeText={commentText => {
            this.setState({commentText});
          }}
          style={{width:'85%',
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 0,
            },
            shadowOpacity: 0.4,
            shadowRadius: 3,
            backgroundColor: ColorStyle.tabWhite,
            elevation: 2,
            height: Styles.constants.X,
            color:ColorStyle.tabBlack,
            paddingLeft: 20,
            borderRadius: 10,}}
          underlineColorAndroid="transparent"
          placeholder={strings('enterComment')}
        />
        <TouchableOpacity
          style={{padding: 10,
            backgroundColor: ColorStyle.tabActive,
            borderRadius: 5,}}
          onPress={() => this.commentPost()}>
          <Icon
            name="send"
            color={ColorStyle.tabWhite}
            type="font-awesome"
            size={20}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
