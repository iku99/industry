import React, {Component} from 'react';
import {Image, Text, TextInput, TouchableOpacity, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {CheckBox, Icon} from 'react-native-elements';
import {PERMISSIONS} from 'react-native-permissions';
import AppConstants from '../../../../resource/AppConstants';
import MediaUtils from '../../../../Utils/MediaUtils';
import MyFastImage from '../../../elements/MyFastImage';
import FloatLabelTextField from '../../../elements/FloatLabelTextInput';
import Styles from '../../../../resource/Styles';
import {strings} from '../../../../resource/languages/i18n';
import PermissionUtils from '../../../../Utils/PermissionUtils';
import ImagePicker from 'react-native-image-crop-picker';
import ColorStyle from '../../../../resource/ColorStyle';
import DataUtils from '../../../../Utils/DataUtils';
import GroupStyle from '../GroupStyle';

export default class EditGroupContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      private:
        this.props.item == null ||
        this.props.item.privacy === AppConstants.GROUP_PRIVACY.PRIVATE,
      des: this.props.item.description,
      groupName: this.props.item.name,
      validGroupName: true,
      imageAvatar: '',
      imageCover: '',
    };
  }
  componentDidMount() {
    this.getAvatar();
    this.imageCover();
  }

  getAvatar() {
    this.setState({imageAvatar: MediaUtils.getGroupAvatar(this.props.item, 0)});
    this.setState({des: 'ssss'});
  }
  imageCover() {
    this.setState({imageCover: MediaUtils.getGroupCover(this.props.item, 0)});
  }
  render() {
    return (
      <View style={Styles.container}>
        <View>
          <View
            style={{
              alignItems: 'center',
              backgroundColor: ColorStyle.tabWhite,
              paddingTop: 20,
              borderColor: ColorStyle.borderItemHome,
              elevation: 2,
            }}>
            <TouchableOpacity
              style={GroupStyle.buttonCam}
              onPress={() => this.selectAvatar()}>
              <Icon
                name={'camerao'}
                type={'antdesign'}
                style={{
                  display:
                    this.state.imageAvatar ===
                    MediaUtils.getGroupAvatar(this.props.item, 0)
                      ? 'flex'
                      : 'none',
                }}
                size={35}
                color={ColorStyle.gray}
              />
              <Image
                source={{uri: this.state.imageAvatar}}
                style={{
                  ...GroupStyle.imagesAvatar,
                  display:
                    this.state.imageAvatar !==
                    MediaUtils.getGroupAvatar(this.props.item, 0)
                      ? 'flex'
                      : 'none',
                }}
              />
            </TouchableOpacity>
            <TextInput
              style={[
                Styles.input.inputLogin,
                {
                  borderColor: ColorStyle.gray,
                  borderWidth: 1,
                  backgroundColor: 'transparent',
                  marginTop: 20,
                  marginBottom: 30,
                },
              ]}
              placeholder={'Name your group'}
              value={this.state.groupName}
              placeholderTextColor={ColorStyle.textInput}
              onChangeText={value => {
                this.validateGroupName(value);
              }}
            />
            {this.state.validGroupName != null && (
              <Icon
                name={this.state.validGroupName ? 'check' : 'close'}
                type="antdesign"
                size={18}
                color={
                  this.state.validGroupName
                    ? ColorStyle.tabActive
                    : ColorStyle.red
                }
                containerStyle={{
                  position: 'absolute',
                  right: 40,
                  alignItems: 'flex-end',
                  bottom: '29%',
                }}
              />
            )}
          </View>
        </View>
        <View
          style={{
            paddingHorizontal: 20,
            paddingVertical: 10,
            backgroundColor: ColorStyle.tabWhite,
            marginTop: 20,
            borderColor: ColorStyle.borderItemHome,
            elevation: 2,
          }}>
          <Text
            style={{
              ...Styles.text.text18,
              color: ColorStyle.tabBlack,
              fontWeight: '700',
            }}>
            {strings('privacy')}
          </Text>
          <Text
            style={{
              ...Styles.text.text11,
              color: ColorStyle.gray,
              maxWidth: '90%',
            }}>
            {strings('privacyDes')}
          </Text>
          <View style={{flexDirection: 'row', marginBottom: 20, marginTop: 10}}>
            <CheckBox
              title={
                <View style={{marginLeft: 5}}>
                  <Text
                    style={{
                      ...Styles.text.text16,
                      color: this.state.private
                        ? ColorStyle.gray
                        : ColorStyle.tabBlack,
                      fontWeight: '700',
                    }}>
                    {strings('public')}
                  </Text>
                  <Text
                    style={{
                      ...Styles.text.text11,
                      color: this.state.private
                        ? ColorStyle.gray
                        : ColorStyle.tabBlack,
                      maxWidth: '90%',
                    }}>
                    {strings('publicDes')}
                  </Text>
                </View>
              }
              checked={!this.state.private}
              containerStyle={{
                borderColor: 'transparent',
                padding: 0,
                backgroundColor: 'transparent',
                marginTop: 10,
              }}
              onPress={() => {
                this.setState({private: !this.state.private}, () =>
                  this.genData(),
                );
              }}
              checkedColor={ColorStyle.tabActive}
              iconType="ionicon"
              uncheckedIcon="ios-radio-button-off"
              checkedIcon="ios-radio-button-on"
              textStyle={{fontWeight: 'normal'}}
            />
          </View>
          <View style={{flexDirection: 'row'}}>
            <CheckBox
              title={
                <View style={{marginLeft: 5}}>
                  <Text
                    style={{
                      ...Styles.text.text16,
                      color: this.state.private
                        ? ColorStyle.tabBlack
                        : ColorStyle.gray,
                      fontWeight: '700',
                    }}>
                    {strings('private')}
                  </Text>
                  <Text
                    style={{
                      ...Styles.text.text11,
                      color: this.state.private
                        ? ColorStyle.tabBlack
                        : ColorStyle.gray,
                      maxWidth: '90%',
                    }}>
                    {strings('privateDes')}
                  </Text>
                </View>
              }
              checked={this.state.private}
              containerStyle={{
                borderColor: 'transparent',
                backgroundColor: 'transparent',
                padding: 0,
              }}
              onPress={() => {
                this.setState({private: !this.state.private}, () =>
                  this.genData(),
                );
              }}
              iconType="ionicon"
              checkedColor={ColorStyle.tabActive}
              uncheckedIcon="ios-radio-button-off"
              checkedIcon="ios-radio-button-on"
              textStyle={{fontWeight: 'normal'}}
            />
          </View>
        </View>
      </View>
    );
  }

  selectAvatar() {
    if (PermissionUtils.checkPermission(PERMISSIONS.IOS.PHOTO_LIBRARY)) {
      ImagePicker.openPicker({
        width: 400,
        height: 400,
        compressImageMaxWidth: 800,
        compressImageMaxHeight: 800,
        cropping: true,
      }).then(image => {
        this.setState({imageAvatar: image.path}, () => {
          if (this.props.updataAvatar != null) {
            this.props.updataAvatar(image);
          }
        });
      });
    }
  }
  selectCover() {
    if (PermissionUtils.checkPermission(PERMISSIONS.IOS.PHOTO_LIBRARY)) {
      ImagePicker.openPicker({
        cropping: true,
        freeStyleCropEnabled: true,
        compressImageMaxWidth: 800,
        compressImageMaxHeight: 800,
      }).then(image => {
        this.setState({imageCover: image.path}, () => {
          if (this.props.updataCover != null) {
            this.props.updataCover(image);
          }
        });
      });
    }
  }
  genData() {
    let data = {
      private: this.state.private
        ? AppConstants.GROUP_PRIVACY.PRIVATE
        : AppConstants.GROUP_PRIVACY.PUBLIC,
      des: this.state.des,
      groupName: this.state.groupName,
      validGroupName: this.state.validGroupName,
      imageAvatar: this.state.imageAvatar,
      imageCover: this.state.imageCover,
    };
      console.log(data)
    // this.props.onChangeData(data);
  }
  validateGroupName(groupName) {
    if (!DataUtils.stringNullOrEmpty(groupName)) {
      this.setState(
        {groupName: groupName, validGroupName: groupName.length > 5},
        () => this.genData(),
      );
    }
  }
}
