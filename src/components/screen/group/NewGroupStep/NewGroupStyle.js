export default {
  nextBT: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    borderWidth: 0,
    padding: 12,
    marginTop: 30,
    elevation: 5,
  },
  previousBT: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    borderWidth: 0,
    padding: 12,
    marginTop: 30,
    elevation: 5,
  },
  percentRow: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
  },
  title: {
    fontSize: 17,
  },
  percentInput: {
    width: 100,
    borderRadius: 8,
    borderWidth: 1,
    padding: 7,
    textAlign: 'right',
    alignSelf: 'stretch',
  },
  titleRow: {
    padding: 10,
    fontWeight: '500',
    fontSize: 14,
    marginTop: 10,
  },
};
