import React, {Component} from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import NewGroupStyle from './NewGroupStyle';
import ColorStyle from '../../../../resource/ColorStyle';
import {Icon} from 'react-native-elements';
import {strings} from '../../../../resource/languages/i18n';

export default class PercentRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      percent: 0,
    };
  }
  componentDidMount() {
    this.setState({percent: `${this.props.item.percent}`});
  }

  render() {
    return (
      <View style={NewGroupStyle.percentRow}>
        <Text style={{fontSize: 14}}>{`${this.props.item.name}`}</Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {this.props.item.canRemove && (
            <TouchableOpacity
              style={{flexDirection: 'row', alignItems: 'center', padding: 10}}
              onPress={() => {
                this.props.removeRow(
                  this.props.item.subType,
                  this.props.item.index,
                );
              }}>
              <Icon name="minus-circle" type="feather" size={20} />
            </TouchableOpacity>
          )}
          <TextInput
            style={NewGroupStyle.percentInput}
            keyboardType={'numeric'}
            value={this.state.percent}
            onChangeText={value => {
              this.validInput(value);
            }}
          />
          <Text style={{marginLeft: 10}}>{strings('percentCharacter')}</Text>
        </View>
      </View>
    );
  }
  validInput(value) {
    value = value.replace(/\D/g, '');
    if (value <= 100 && value >= 0) {
      this.setState({percent: value});
      this.props.onTextChange(value);
    }
  }
}
