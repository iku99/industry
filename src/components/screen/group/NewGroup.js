import React, {Component} from 'react';
import {
    Alert,
    Image, ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import GroupHandle from '../../../sagas/GroupHandle';
import AppConstants from '../../../resource/AppConstants';
import {strings} from '../../../resource/languages/i18n';
import MediaHandle from '../../../sagas/MediaHandle';
import ToolbarMain from '../../elements/toolbar/ToolbarMain';
import Styles from '../../../resource/Styles';
import ColorStyle from '../../../resource/ColorStyle';
import {Actions} from 'react-native-router-flux';
import GroupStyle from "./GroupStyle";
import {CheckBox, Icon} from "react-native-elements";
import MediaUtils from "../../../Utils/MediaUtils";
import PermissionUtils from "../../../Utils/PermissionUtils";
import {PERMISSIONS} from "react-native-permissions";
import ImagePicker from "react-native-image-crop-picker";
import constants from "../../../Api/constants";
import SimpleToast from "react-native-simple-toast";
import TextInputTitle from "../../elements/TextInput/TextInputTitle";
import TextInputContent from "../../elements/TextInput/TextInputContent";
import DialogSuccess from "../../elements/Dialog/DialogSuccess";
export default class NewGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      group: undefined,
      validGroupName: false,
        validGroupDes:false,
      isCreating: false,
      id: undefined,
      imageAvatar:'',
      groupName:'',
        desGroup:'',
        showDialog:false,
      private:
          this.props.item == null ||
          this.props.item.c === AppConstants.GROUP_PRIVACY.PRIVATE,
    };
  }

  renderAvatar(){
      if(this.state.imageAvatar ===''){
          return(
              <Icon
                  name={'camerao'}
                  type={'antdesign'}
                  containerStyle={{ display:
                          this.state.imageAvatar ==='' ? 'flex' : 'none'}}
                  size={35}
                  color={ColorStyle.gray}
              />
          )
      }else {
          return (
              <Image
                  source={{uri:this.state.imageAvatar.path}}
                  style={{
                      ...GroupStyle.buttonCam,
                  }}
              />
          )
      }
  }
  renderPrivate(){
      return(
          <View
              style={{
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                  backgroundColor: ColorStyle.tabWhite,
                  borderColor: ColorStyle.borderItemHome,
                  elevation: 2,
              }}>
              <Text
                  style={{
                      ...Styles.text.text18,
                      color: ColorStyle.tabBlack,
                      fontWeight: '700',
                  }}>
                  {strings('privacy')}
              </Text>
              <Text
                  style={{
                      ...Styles.text.text11,
                      color: ColorStyle.gray,
                      maxWidth: '90%',
                  }}>
                  {strings('privacyDes')}
              </Text>
              <View style={{flexDirection: 'row', marginBottom: 20, marginTop: 10}}>
                  <CheckBox
                      title={<View style={{marginLeft: 5}}>
                          <Text
                              style={{
                                  ...Styles.text.text16,
                                  color: this.state.private
                                      ? ColorStyle.gray
                                      : ColorStyle.tabBlack,
                                  fontWeight: '700',
                              }}>
                              {strings('public')}
                          </Text>
                          <Text
                              style={{
                                  ...Styles.text.text11,
                                  color: this.state.private
                                      ? ColorStyle.gray
                                      : ColorStyle.tabBlack,
                                  maxWidth: '90%',
                              }}>
                              {strings('publicDes')}
                          </Text>
                      </View>}
                      checked={!this.state.private}
                      containerStyle={{borderColor: 'transparent', padding: 0, backgroundColor: 'transparent',marginTop: 10}}
                      onPress={() => {
                          this.setState({private: !this.state.private});
                      }}
                      checkedColor={ColorStyle.tabActive}
                      iconType="ionicon"
                      uncheckedIcon="ios-radio-button-off"
                      checkedIcon="ios-radio-button-on"
                      textStyle={{fontWeight: 'normal'}}
                  />
              </View>
              <View style={{flexDirection: 'row'}}>
                  <CheckBox
                      title={
                          <View style={{marginLeft: 5}}>
                              <Text
                                  style={{
                                      ...Styles.text.text16,
                                      color: this.state.private
                                          ? ColorStyle.tabBlack
                                          : ColorStyle.gray,
                                      fontWeight: '700',
                                  }}>
                                  {strings('private')}
                              </Text>
                              <Text
                                  style={{
                                      ...Styles.text.text11,
                                      color: this.state.private
                                          ? ColorStyle.tabBlack
                                          : ColorStyle.gray,
                                      maxWidth: '90%',
                                  }}>
                                  {strings('privateDes')}
                              </Text>
                          </View>
                      }
                      checked={this.state.private}
                      containerStyle={{
                          borderColor: 'transparent',
                          backgroundColor: 'transparent',
                          padding: 0,
                      }}
                      onPress={() => {
                          this.setState({private: !this.state.private});
                      }}
                      iconType="ionicon"
                      checkedColor={ColorStyle.tabActive}
                      uncheckedIcon="ios-radio-button-off"
                      checkedIcon="ios-radio-button-on"
                      textStyle={{fontWeight: 'normal'}}
                  />
              </View>
          </View>
      )
  }
  render() {
    return (
        <View style={Styles.container}>

          <ToolbarMain
              title={strings('creatGroup')}
              iconBack={true}
              iconSearch={true}
          />
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={GroupStyle.ViewImage}>
              <TouchableOpacity
                  style={GroupStyle.buttonCam}
                  onPress={() => this.selectAvatar()}>
                  {this.renderAvatar()}
              </TouchableOpacity>

             <View style={{alignSelf:'center', width:Styles.constants.widthScreenMg24 ,marginVertical: Styles.constants.X/2}}>
                 <TextInputTitle title={strings('plhNameGroup')}
                                 onChange={(text)=>this.validateGroupName(text)}
                                 placeholder={strings('plhNameGroup')}
                                 value={this.state.groupName}/>
                 <TextInputContent title={strings('description')}
                                   onChange={(text)=>this.validateGroupDes(text)}
                                   placeholder={strings('placeholderDes')}
                                   value={this.state.desGroup}/>
             </View>
            </View>
              {this.renderPrivate()}
              <TouchableOpacity
                  style={{
                      ...GroupStyle.buttonCre,
                      width: Styles.constants.widthScreenMg24,
                      alignSelf: 'center',
                      marginTop: Styles.constants.X ,
                  }}
                  onPress={() => {
                      this.onRegister();
                  }}>
                  <Text
                      style={{
                          ...Styles.text.text18,
                          color: ColorStyle.tabWhite,
                          fontWeight: '700',
                      }}>
                      {strings('creatGroup')}
                  </Text>
              </TouchableOpacity>
          </ScrollView>
            {this.state.showDialog && <DialogSuccess
                                        onSuccess={()=>{
                                            setTimeout(()=>{   Actions.jump('groupDetail', {item: this.state.group, checkRole: 1})},5)
                                            Actions.reset('drawer')
                                        }}
                                        item={this.state.group}/>}
        </View>
    );
  }
  validateGroupName(groupName) {
      this.setState(
          {groupName: groupName, validGroupName: groupName.length > 5});
  }
    validateGroupDes(groupNameDes) {

            this.setState(
                {desGroup: groupNameDes});
    }
  selectAvatar() {
    if (PermissionUtils.checkPermission(PERMISSIONS.IOS.PHOTO_LIBRARY)) {
      ImagePicker.openPicker({
        width: 400,
        height: 400,
        compressImageMaxWidth: 800,
        compressImageMaxHeight: 800,
        cropping: true,
      }).then(image => {this.setState({imageAvatar: image})

      });
    }
  }
  onRegister() {
      if(!this.state.validGroupName){
          SimpleToast.show(strings('checkName'),SimpleToast.SHORT)
          return
      }
      MediaHandle.getInstance().uploadProfileAvatar(this.state.imageAvatar,(_)=>{
      },(isSuccess, responseData)=>{
          console.log(responseData)
        if(isSuccess && JSON.parse(responseData._response).code ===0){
          let data=responseData._response;
            console.log('img:',data)
            let params = {
                name:this.state.groupName,
                des:this.state.desGroup,
                phone:"",
                image_url:JSON.parse(data).url,
                email:"",
                status:this.state.private?AppConstants.GROUP_PRIVACY.PRIVATE:AppConstants.GROUP_PRIVACY.PUBLIC
            };
            GroupHandle.getInstance().registerGroup(
                params,
                (isSuccess, responseData) => {
                    if (
                        isSuccess &&
                        responseData !== undefined &&
                        responseData.data !== undefined
                    ) {
                        this.setState({group:responseData.data,showDialog:true})
                        return
                    } else {
                        Alert.alert(
                            '',
                            'Có lỗi xảy ra. Vui lòng thử lại',
                            [{text: strings('close'), onPress: () => {}}],
                            {cancelable: false},
                        );
                    }
                },
            );
      }
      })
  }
}
