import React, {Component} from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../../resource/Styles';
import {DotIndicator} from 'react-native-indicators';
import ColorStyle from '../../../../resource/ColorStyle';
import GlobalInfo from '../../../../Utils/Common/GlobalInfo';
import GroupHandle from '../../../../sagas/GroupHandle';
import ImageHelper from '../../../../resource/images/ImageHelper';
import GroupStyle from '../GroupStyle';
import {strings} from '../../../../resource/languages/i18n';
import {Actions} from 'react-native-router-flux';

export default class TabCreatGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      groups: [],
      data: [],
      loading: true,
      page: 0,
      userId: undefined,
      isGroupDes: true,
    };
    this.allGroup = null;
    this.myGroup = null;
  }
  componentDidMount() {
    GlobalInfo.getUserInfo().then(userInfo => {
      this.setState({userId: userInfo.id}, () => this.getBestGroupList());
    });
  }
  render() {
    return (
      <View
        style={{
          ...Styles.container,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Image
          source={ImageHelper.bannerGroup}
          style={GroupStyle.bannerGroup}
        />
        <Text
          style={{
            ...Styles.text.text18,
            color: ColorStyle.textTitle24,
            fontWeight: '700',
          }}>
          {strings('textGroups')}
        </Text>
        <Text
          style={{
            ...Styles.text.text14,
            color: ColorStyle.gray,
            fontWeight: '400',
            maxWidth: '80%',
            textAlign: 'center',
          }}>
          {strings('textGroups1')}
        </Text>
        <TouchableOpacity
          style={GroupStyle.buttonCre}
          onPress={() => {
            Actions.jump('newGroup');
          }}>
          <Text
            style={{
              ...Styles.text.text18,
              color: ColorStyle.tabWhite,
              fontWeight: '700',
            }}>
            {strings('creatGroup')}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
  _renderItem = ({item, index}) => {
    return (
      <View>
        <Text>{item.name}</Text>
      </View>
    );
  };
  renderLoading() {
    if (this.state.loading) {
      return (
        <View>
          <DotIndicator color={ColorStyle.tabWhite} size={10} />
        </View>
      );
    } else {
      return null;
    }
  }
  getBestGroupList() {
    let params = {
      current_page: 0,
      page_size: 30,
    };
    GroupHandle.getInstance().getSuggestionGroup(
      params,
      (isSuccess, responseData) => {
        if (isSuccess) {
          if (responseData.data != null && responseData.data.list) {
            let data = responseData.data.list;
            let canLoadData = responseData.data.list.length === 30;
            this.setState({data, canLoadData});
          }
        }
      },
    );
  }
}
