import React, {Component} from 'react';
import {FlatList, RefreshControl, View} from 'react-native';
import ItemGroups from '../../../../elements/viewItem/itemGroup/ItemGroups';
import {Actions} from 'react-native-router-flux';
import Styles from "../../../../../resource/Styles";

export default class ListGroupRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.item,
    };
  }
  render() {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={this.state.data}
        renderItem={this._renderItem}
        style={{flex: 1, height: '100%'}}
        onEndReachedThreshold={0.4}
        removeClippedSubviews={true} // Unmount components when outside of window
        windowSize={10}
      />
    );
  }
  _renderItem = ({item}) => {
    return (
      <ItemGroups
        item={item}
        onPress={item => {
          if (this.props.onItemPress !== undefined) {
            this.props.onItemPress();
          }
          Actions.jump('groupDetail', {item: item, checkRole: 3});
        }}
      />
    );
  };
}
