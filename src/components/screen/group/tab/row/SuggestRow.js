import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../../../resource/Styles';
import ColorStyle from '../../../../../resource/ColorStyle';
import {strings} from '../../../../../resource/languages/i18n';
import TimeUtils from '../../../../../Utils/TimeUtils';
import constants from '../../../../../Api/constants';
import AppConstants from '../../../../../resource/AppConstants';
import GroupHandle from '../../../../../sagas/GroupHandle';
import {Actions} from 'react-native-router-flux';

export default class SuggestRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      canSendRequest: false,
      id: this.props.item === undefined ? 1 : this.props?.item[0]?.id,
      members: 0,
      requestState: AppConstants.GROUP_APPROVE_STATUS.NOTHING,
    };
  }

  componentDidMount() {
      this.getButtonText(this.props.item.is_request);
  }
  render() {
    if (this.props.item[0] === undefined) {
      return <View />;
    }
    let dataHot = this.props.item[0];
    return (
      <View style={{height:Styles.constants.heightScreen / 3,marginBottom:Styles.constants.X}}>
        <Text
          style={{
            ...Styles.text.text18,
            color: ColorStyle.textTitle24,
            fontWeight: '700',
          }}>
          {strings('suggestedGroup')}
        </Text>
        <Text
          style={{
            ...Styles.text.text14,
            color: ColorStyle.tabInactive,
            fontWeight: '400',
            marginBottom: 10,
          }}>
          {strings('desTitleGroup')}
        </Text>
        <TouchableOpacity   onPress={() => Actions.jump('groupDetail', {item: dataHot, checkRole:3})}
          style={{
            width: '100%',
            // height: Styles.constants.heightScreen / 3,
            backgroundColor: ColorStyle.tabWhite,
            borderBottomColor: 'rgba(0, 0, 0, 0.05)',
              shadowColor: 'black',
              shadowOffset: {width: 1, height: 1},
              shadowOpacity: 0.26, elevation: 2,
            borderRadius: 10,
          }}>
          <Image
            source={{uri: this.getImageUrl(dataHot)}}
            style={{
              width: '100%',
              height: '40%',
              borderTopRightRadius: 10,
              borderTopLeftRadius: 10,
            }}
          />
          <View
            style={{marginHorizontal: Styles.constants.X * 0.7, marginTop: 15}}>
            <Text
              style={{
                ...Styles.text.text16,
                color: ColorStyle.tabBlack,
                fontWeight: '700',
              }}>
              {dataHot.name}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                marginTop: 5,
              }}>
              <Text
                style={[
                  Styles.text.text10,
                  {color: ColorStyle.gray, fontWeight: '400'},
                ]}
                numberOfLines={1}
                ellipsizeMode="tail">
                {dataHot.total_member} {strings('members')}
              </Text>
              <Text
                style={[
                  Styles.text.text10,
                  {color: ColorStyle.gray, fontWeight: '400', marginLeft: 5},
                ]}
                numberOfLines={1}
                ellipsizeMode="tail">
                + {TimeUtils.timeDiff(dataHot.created_date)}
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => this.joinGroup()}
              style={{
                width: '100%',
                height: Styles.constants.X/1.3,
                alignSelf: 'center',
                alignItems: 'center',
                backgroundColor: '#F6F6F6',
                justifyContent: 'center',
                borderRadius: 10,
                marginTop: 10,
              }}>
              <Text style={{...Styles.text.text11, fontWeight: '700'}}>
                  {this.state.text}
              </Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
  getImageUrl(image,index) {
    let cover = image.image_url;
    if (cover !== undefined ) {
      return constants.host + cover;
    }
    return 'https://picsum.photos/130/214?' +index;
  }
    joinGroup() {
        // FuncUtils.getInstance().callRequireLogin(() => {
        let param = {
            team_id:this.props.item.id,
            team_name:this.props.item.name
        };
        GroupHandle.getInstance().requestJoinGroup(
            param,
            (isSuccess, responseData) => {
                if(isSuccess){
                    this.getButtonText(true)
                }
                },
        );
        // });
    }
  getButtonText(state) {
    // if (!state) {
    //     return  strings('join')
    // } else {
    //     return  strings('sentRequest')
    // }
      if (!state) {
          this.setState({canSendRequest: true, text: strings('join')});
      } else {
          this.setState({canSendRequest: false, text: strings('sentRequest')});
      }
  }
}
