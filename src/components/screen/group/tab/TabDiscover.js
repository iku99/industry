import React, {Component} from 'react';
import {FlatList, View} from 'react-native';
import Styles from '../../../../resource/Styles';
import {DotIndicator, UIActivityIndicator} from 'react-native-indicators';
import ColorStyle from '../../../../resource/ColorStyle';
import GlobalInfo from '../../../../Utils/Common/GlobalInfo';
import GroupHandle from '../../../../sagas/GroupHandle';
import AppConstants from '../../../../resource/AppConstants';
import SuggestRow from './row/SuggestRow';
import ListGroupRow from './row/ListGroupRow';
import DataUtils from "../../../../Utils/DataUtils";
const defaulData = [
  {type: AppConstants.listGroups.SUGGEST},
  {type: AppConstants.listGroups.END},
];
export default class TabDiscover extends Component {
  constructor(props) {
    super(props);
    this.state = {
      groups: [],
      data: [],
      loading: true,
      page: 1,
      userId: undefined,
      canLoading: true,
      dataHot: [],
    };
  }
  componentDidMount() {
    this.setState({data: defaulData}, () => {
      GlobalInfo.getUserInfo().then(userInfo => {
        this.setState(
          {userId: userInfo.id},
          () => this.getBestGroupList(),
          this.getBestGroup(),
        );
      });
    });
  }
  keyExtractor = (item, index) => `tab_discover_${index.toString()}`;
  render() {
    console.log(this.state.data)
    if (this.state.dataHot === undefined) {
      return <View />;
    } else {
      return (
        <View style={{...Styles.container}}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.data}
            extraData={this.state}
            keyExtractor={this.keyExtractor}
            renderItem={this._renderItem}
            style={{
              width: Styles.constants.widthScreenMg24,
              height: '100%',
              marginTop: 10,
              marginHorizontal: Styles.constants.marginHorizontalAll,
            }}
            ListFooterComponent={this.renderFooter.bind(this)}
            onEndReachedThreshold={0.4}
            onEndReached={() => this.handleLoadMore()}
            ref={ref => {
              this.listRef = ref;
            }}
          />
          {this.renderLoading()}
        </View>
      );
    }
  }
  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <UIActivityIndicator color={ColorStyle.tabActive} size={20} />;
  };
  handleLoadMore() {
    if (this.state.loading) {
      return;
    }
    if (this.state.canLoading) {
      return;
    }
    this.setState(
      {loading: true, page: this.state.page + 1},
      this.getBestGroupList,
    );
  }
  _renderItem = ({item, index}) => {
    switch (item.type) {
      case AppConstants.listGroups.SUGGEST:
        return <SuggestRow item={this.state.dataHot} />;
      case AppConstants.listGroups.GROUPS:
        return <ListGroupRow item={item.data} />;

      case AppConstants.listGroups.END:
        return <View style={{marginBottom: 10}} />;
      default:
        return null;
    }
  };
  renderLoading() {
    if (!this.state.loading) {
      return null;
    } else {
      return (
        <View>
          <DotIndicator color={ColorStyle.tabWhite} size={10} />
        </View>
      );
    }
  }
  getBestGroupList() {
    let params = {
      page_index: this.state.page,
      page_size:AppConstants.LIMIT,
      type:AppConstants.GetGroups.SUGGEST_GROUP
    };
    GroupHandle.getInstance().getGroups(
      params,
      (isSuccess, responseData) => {
        if (isSuccess) {

          if (responseData.data != null) {
            if (responseData.data.length < AppConstants.LIMIT) {
              this.setState({canLoadData: false});
            }
            let listData = this.state.data;
            let abc = {
              data: DataUtils.removeItem(responseData.data.data,0),
              type: AppConstants.listGroups.GROUPS,
            };
            listData = [...listData, ...[abc]];
            this.setState({data: listData, loading: false});
          }
        }
      },true
    );
  }
  getBestGroup() {
    let params = {
      page_index:1,
      page_size:1,
      type:AppConstants.GetGroups.SUGGEST_GROUP
    };
    GroupHandle.getInstance().getGroups(
      params,
      (isSuccess, responseData) => {

        if (isSuccess) {
          if (responseData.data != null) {
            let data = responseData.data.data;
            this.setState({dataHot: data});
          }
        }
      },true
    );
  }
}
