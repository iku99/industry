import React, {Component} from 'react';
import {
    ActivityIndicator,
    FlatList,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import Styles from '../../../../resource/Styles';
import GroupHandle from '../../../../sagas/GroupHandle';
import GlobalInfo from '../../../../Utils/Common/GlobalInfo';
import {strings} from '../../../../resource/languages/i18n';
import ColorStyle from '../../../../resource/ColorStyle';
import ItemGroups from '../../../elements/viewItem/itemGroup/ItemGroups';
import {Actions} from 'react-native-router-flux';
import {SceneMap, TabView} from 'react-native-tab-view';
import TabDiscover from './TabDiscover';
import TabCreatGroup from './TabCreatGroup';
import ViewUtils from '../../../../Utils/ViewUtils';
import AppConstants from '../../../../resource/AppConstants';
import {DotIndicator} from 'react-native-indicators';
import NavigationUtils from "../../../../Utils/NavigationUtils";
import {EventRegister} from "react-native-event-listeners";
import EmptyView from "../../../elements/reminder/EmptyView";

export default class TabGroups extends Component {
    constructor(props) {
        super(props);
        this.state = {
            groups: [],
            data: [],
            loading: true,
            page: 1,
            userId: undefined,
            index: 0,
            isSelect: AppConstants.GetGroups.MY_GROUP,
        };
    }
    componentDidMount() {
        EventRegister.addEventListener(AppConstants.EventName.UPDATE_GROUP, total => {
            this.getGroupData()

        });
        GlobalInfo.getUserInfo().then(userInfo => {
            this.setState({userId: userInfo.id}, () => this.getGroupData());
        });
    }
    render() {
        return (
            <View style={{...Styles.container, marginHorizontal: 24, marginTop: 20}}>
                {this._renderTabContent()}
                {this.state.groups.length===0 && (
                    <EmptyView text={strings(this.state.isSelect===AppConstants.GetGroups.MY_GROUP?'notGroup':'notGroup1')}/>
                )}
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.groups}
                    extraData={this.state}
                    keyExtractor={this.keyExtractor}
                    renderItem={this._renderItem}
                    style={{
                        flex: 1,
                        height: '100%',
                        backgroundColor: 'transparent',
                        paddingHorizontal: '2%',
                    }}
                    ListFooterComponent={this.renderFooter.bind(this)}
                    onEndReachedThreshold={0.4}
                    onEndReached={() => this.handleLoadMore()}
                    ref={ref => {
                        this.listRef = ref;
                    }}
                />
            </View>
        );
    }
    _renderItem = ({item}) => {
        return (
            <ItemGroups
                item={item}
                showJoin={true}
                onPress={item => {
                    NavigationUtils.goToGroupDetail(item,this.state.isSelect === AppConstants.GetGroups.MY_GROUP?1: 2,()=>{
                        this.getGroupData()
                    })
                }}
            />
        );
    };
    _renderTabContent() {
        let i = this.state.isSelect;
        return (
            <View
                style={{
                    flexDirection: 'row',
                    height: 50,
                    marginTop: 6,
                    backgroundColor: ColorStyle.tabWhite,
                    borderRadius: 20,
                }}>
                <TouchableOpacity
                    style={[
                        {
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderBottomLeftRadius: 20,
                            borderTopLeftRadius: 20,
                            backgroundColor:
                                i === AppConstants.GetGroups.MY_GROUP ? ColorStyle.tabActive : ColorStyle.tabWhite,
                        },
                    ]}
                    onPress={() => this.getGroups(AppConstants.GetGroups.MY_GROUP)}>
                    <Text
                        style={[
                            Styles.text.text14,
                            {
                                color: i === AppConstants.GetGroups.MY_GROUP ? ColorStyle.tabWhite : ColorStyle.tabBlack,
                                fontWeight: i === AppConstants.GetGroups.MY_GROUP ? '700' : '400',
                            },
                        ]}>
                        {strings('yourGroup')}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[
                        {
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderBottomRightRadius: 20,
                            borderTopRightRadius: 20,
                            backgroundColor:
                                i === 1 ? ColorStyle.tabActive : ColorStyle.tabWhite,
                        },
                    ]}
                    onPress={() => this.getGroups(AppConstants.GetGroups.JOIN_GROUP)}>
                    <Text
                        style={[
                            Styles.text.text14,
                            {
                                color: i === 1 ? ColorStyle.tabWhite : ColorStyle.tabBlack,
                                fontWeight: i === 1 ? '700' : '400',
                            },
                        ]}>
                        {strings('joinGroup')}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
    renderFooter = () => {
        //it will show indicator at the bottom of the list when data is loading otherwise it returns null
        if (!this.state.loading) {
            return null;
        }
        return <ActivityIndicator style={{color: '#000'}} />;
    };
    renderLoading() {
        if (this.state.loading) {
            return (
                <View>
                    <DotIndicator color={ColorStyle.tabWhite} size={10} />
                </View>
            );
        } else {
            return null;
        }
    }
    handleLoadMore() {
        if (this.state.loading) {
            return;
        }
        if (!this.state.canLoadData) {
            return;
        }
        this.setState({loading: true, page: this.state.page + 1}, () =>
            this.getGroupData(),
        );
    }
    getGroups(i) {
        this.setState({isSelect: i}, this.getGroupData);
    }
    getGroupData() {
        let param = {
            page_index: this.state.page,
            page_size:10,
            type:this.state.isSelect
        };
        this.setState({groups: []});
        GroupHandle.getInstance().getGroups(param, (isSuccess, responseData) => {
            console.log('responseData:',responseData)
            this.handleData(isSuccess, responseData);
        },true);
    }
    handleData(isSuccess, responseData) {
        if (isSuccess) {

            if (responseData.data != null && responseData.data) {
                let canLoadData = responseData.data.length === 20;
                this.setState({
                    groups: responseData.data.data,
                    canLoadData,
                    loading: false,
                });
            }
        } else {
            console.log('err');
        }
    }
}
