import React, {Component} from 'react';
import {AsyncStorage, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import {strings} from '../../../resource/languages/i18n';
import {EventRegister} from 'react-native-event-listeners';
import {SceneMap, TabView} from 'react-native-tab-view';
import TabDiscover from './tab/TabDiscover';
import TabGroups from './tab/TabGroups';
import TabCreatGroup from './tab/TabCreatGroup';
import ViewUtils from '../../../Utils/ViewUtils';
import Styles from '../../../resource/Styles';
import ColorStyle from '../../../resource/ColorStyle';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';

class GroupScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isGroupDes: false,
      index: this.props.index === undefined ? 0 : this.props.index,

      routes: [
        {key: 'TabDiscover', title: strings('discover')},
        {key: 'TabGroups', title: strings('group')},
        {key: 'TabCreatGroup', title: strings('creatGroup')},
      ],
      // isLogged:FuncUtils.getInstance().isLogged
    };
    this.allGroup = null;
    this.myGroup = null;
  }
  componentWillUnmount() {
    EventRegister.removeEventListener(this.listener);
  }
  _handleIndexChange = index => this.setState({index});

  _renderScene = SceneMap({
    TabDiscover: () => (
      <TabDiscover/>
    ),
    TabGroups: () => (
      <TabGroups/>
    ),
    TabCreatGroup: () => <TabCreatGroup />,
  });
  _renderTabBar = props => {
    return ViewUtils.renderTab(
      props,
      ColorStyle.tabWhite,
      this.state.index,
      i => {
        this.setState({index: i});
      },
    );
  };
  render() {
    return (
      <View style={Styles.container}>
        {this.renderToolbar()}
        {this._renderTabContent()}
      </View>
    );
  }
  renderToolbar() {
    return (
      <View
        style={[
          Styles.toolbar.toolbar,
          {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingBottom: Styles.constants.X * 0.4,
          },
        ]}>
        <TouchableOpacity onPress={() => Actions.jump('main')}>
          <Icon
            name="arrow-back-ios"
            type="materialicons"
            size={20}
            color={ColorStyle.tabWhite}
          />
        </TouchableOpacity>
        <Text
          style={[
            Styles.text.text20,
            {
              color: ColorStyle.tabWhite,
              fontWeight: '700',
              textAlign: 'center',
            },
          ]}>
          {strings('group')}
        </Text>
        <TouchableOpacity onPress={() => Actions.jump('searchScreen')}>
          <Icon
            name="search1"
            type="antdesign"
            size={20}
            color={ColorStyle.tabWhite}
          />
        </TouchableOpacity>
      </View>
    );
  }
  _renderTabContent() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
      />
    );
  }
}
const mapStateToProps = state => {
  return {};
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(GroupScreen);
