import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import MyFastImage from '../../../../elements/MyFastImage';
import MediaUtils from '../../../../../Utils/MediaUtils';
import GroupStyle from '../../GroupStyle';
import AppConstants from '../../../../../resource/AppConstants';
import {strings} from '../../../../../resource/languages/i18n';
import Styles from '../../../../../resource/Styles';
import ColorStyle from '../../../../../resource/ColorStyle';
import GroupUtils from '../../../../../Utils/GroupUtils';
import FuncUtils from '../../../../../Utils/FuncUtils';
import GroupHandle from '../../../../../sagas/GroupHandle';
import {Icon} from 'react-native-elements';
import ViewUtils from '../../../../../Utils/ViewUtils';
import NavigationUtils from "../../../../../Utils/NavigationUtils";

export default class OptionRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      canSendRequest: false,
      requestState: AppConstants.GROUP_APPROVE_STATUS.NOTHING,
      item: this.props.item,
    };
  }
  componentDidMount() {
    this.getButtonText(this.props.item.state);
    this.getGroupDetail();
  }

  render() {
    let item = this.props.item;
    return (
      <View>
        <View style={GroupStyle.optionRow}>
          <TouchableOpacity
            onPress={() => {
              if (this.state.canSendRequest) {
                this.requestJoinGroup();
              }
            }}
            style={{...GroupStyle.itemOption,display:this.props.checkJoin?'none':'flex'}}>
            <Icon
              name={'check'}
              type={'antdesign'}
              size={20}
              color={
                this.state.canSendRequest
                  ? ColorStyle.tabBlack
                  : ColorStyle.tabActive
              }
            />
            <Text
              style={{
                ...Styles.text.text14,
                color: this.state.canSendRequest
                  ? ColorStyle.tabBlack
                  : ColorStyle.tabActive,
              }}>
              {this.state.text}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {}} style={GroupStyle.itemOption}>
            <Icon name={'adduser'} type={'antdesign'} size={20} />
            <Text style={{...Styles.text.text14, color: ColorStyle.tabBlack}}>
              {strings('invite')}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => NavigationUtils.goToInformationGroup(item)} style={GroupStyle.itemOption}>
            <Icon name={'menu'} type={'feather'} size={20} />
            <Text style={{...Styles.text.text14, color: ColorStyle.tabBlack}}>
              {strings('more')}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={GroupStyle.groupPost}>
          <TouchableOpacity style={GroupStyle.itemOption}>
            <Image
              source={{uri: this.getAvatarUrl(item)}}
              style={{...Styles.icon.iconShow, borderRadius: 50}}
            />
          </TouchableOpacity>
          <TouchableOpacity style={GroupStyle.itemComment}>
            <View style={GroupStyle.viewComment}>
              <Text
                style={{
                  ...Styles.text.text14,
                  color: ColorStyle.gray,
                  marginLeft: 10,
                }}>
                {strings('textComment')}
              </Text>
            </View>
          </TouchableOpacity>

        </View>
      </View>
    );
  }
  getGroupDetail() {
    GroupHandle.getInstance().getDetailGroup(
      this.props.item.id,
      (isSuccess, responseData) => {
        if (isSuccess) {
          let item = responseData.data;
          this.setState({item});
          this.getButtonText(item.approved_status);
        }
      },
    );
  }
  getButtonText(state) {
    GroupUtils.getButtonText(state, (canSend, text) => {
      this.setState({canSendRequest: canSend, text});
    });
  }
  requestJoinGroup() {
    FuncUtils.getInstance().callRequireLogin(() => {
      let param = {};
      GroupHandle.getInstance().requestJoinGroup(
        this.props.item.id,
        param,
        (isSuccess, responseData) => {
          if (isSuccess) {
            this.getButtonText(AppConstants.GROUP_APPROVE_STATUS.PENDING);
            ViewUtils.showAlertDialog(strings('requestJoinSuccess'));
          } else {
            ViewUtils.showAlertDialog('Error');
          }
        },
      );
    });
  }
  // getButtonText(state) {
  //   GroupUtils.getButtonText(state, (canSend, text) => {
  //     this.setState({canSendRequest: canSend, text});
  //   });
  // }
  // requestJoinGroup() {
  //   FuncUtils.getInstance().callRequireLogin(() => {
  //     let param = {};
  //     GroupHandle.getInstance().requestJoinGroup(
  //       this.props.item.id,
  //       param,
  //       (isSuccess, responseData) => {
  //         if (isSuccess) {
  //           this.getButtonText(AppConstants.GROUP_APPROVE_STATUS.PENDING);
  //           ViewUtils.showAlertDialog(strings('requestJoinSuccess'));
  //         } else {
  //           ViewUtils.showAlertDialog('Error');
  //         }
  //       },
  //     );
  //   });
  // }
  getAvatarUrl(item) {
    return MediaUtils.getGroupAvatar(item, 0);
  }
}
