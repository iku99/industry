import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import MyFastImage from '../../../../elements/MyFastImage';
import MediaUtils from '../../../../../Utils/MediaUtils';
import GroupStyle from '../../GroupStyle';
import AppConstants from '../../../../../resource/AppConstants';
import {strings} from '../../../../../resource/languages/i18n';
import Styles from '../../../../../resource/Styles';
import ColorStyle from '../../../../../resource/ColorStyle';

export default class BannerRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      canEdit: this.props.canEdit,
    };
  }
  componentDidMount() {}

  render() {
    let item = this.props.item;
    return (
      <View style={{...GroupStyle.bannerGroupDetail}}>
        <Image
          style={{...GroupStyle.bannerGroupDetail}}
          source={{
            uri: this.getGroupImageUrl(item),
            headers: {Authorization: 'someAuthToken'},
          }}
          resizeMode={'cover'}
        />
        <View style={{...GroupStyle.bannerGroupDetail,position:'absolute',backgroundColor:'rgba(0, 0, 0, 0.7)'}}/>
        <View style={{position: 'absolute', bottom: 10, left: 20, right: 20}}>
          <Text
            style={{
              ...Styles.text.text24,
              color: ColorStyle.tabWhite,
              fontWeight: '700',
            }}>
            {item.name}
          </Text>
          <Text
            style={{
              ...Styles.text.text14,
                color: ColorStyle.tabWhite,
              fontWeight: '700',
            }}>
            {this.checkPrivacy()}
          </Text>
        </View>
      </View>
    );
  }
  getGroupName(item) {
    return item.name !== undefined ? item.name : 'Nhóm mua chung Hà Nội';
  }
  checkPrivacy() {
    let privacy = this.props.item.status;
    if (privacy === AppConstants.GROUP_PRIVACY.PRIVATE) {
      return strings('privateGroup');
    } else {
      return strings('publicGroup');
    }
  }
  getGroupMember() {
    return '+2K Thành viên';
  }
  getGroupImageUrl(item) {
    return MediaUtils.getGroupCover(item, 0);
  }
  getAvatarUrl(item) {
    return MediaUtils.getGroupAvatar(item, 0);
  }
}
