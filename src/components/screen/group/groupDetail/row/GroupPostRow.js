import React, {Component} from 'react';
import {
  FlatList,
  Image,
  Share,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {strings} from '../../../../../resource/languages/i18n';
import constants from '../../../../../Api/constants';
import ElevatedView from 'react-native-elevated-view';
import GroupStyle from '../../GroupStyle';
import HTMLView from 'react-native-htmlview';
import {Icon} from 'react-native-elements';
import GroupHandle from '../../../../../sagas/GroupHandle';
import {Actions} from 'react-native-router-flux';
import GroupPostHandle from '../../../../../sagas/GroupPostHandle';
import Styles from '../../../../../resource/Styles';
import ColorStyle from '../../../../../resource/ColorStyle';
import ImageGrid from '../../../../elements/ImageGrid';
import DateUtil from "../../../../../Utils/DateUtil";
import GlobalInfo from "../../../../../Utils/Common/GlobalInfo";
import MediaUtils from "../../../../../Utils/MediaUtils";
let isPostingLike = false;
let onChangeItemData;
export default class GroupPostRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: this.props.item,
    };
  }
  render() {
    let item = this.state.item;
    return (
      <ElevatedView
        elevation={2}
        style={GroupStyle.groupPost1}>
        <View style={{flexDirection: 'row', paddingHorizontal: Styles.constants.X/6}}>
          <Image
            style={{...Styles.icon.iconShow, borderRadius: 50}}
            source={MediaUtils.getAvatarUrl(item)}
          />
          <View
            style={{
              flex: 1,
              justifyContent: 'space-between',
              marginLeft: 10,
              paddingHorizontal: 5,
            }}>
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabBlack,
                fontWeight: '700',
              }}>
              {item.customer.full_name}
            </Text>
            <Text
              style={{
                ...Styles.text.text10,
                color: ColorStyle.gray,
                fontWeight: '400',
              }}>
              {DateUtil.formatDate('HH:MM DD/MM/YYYY',item.created_date)}
            </Text>
          </View>
        </View>
          <Text style={{...Styles.text.text15,fontWeight:'600',color:ColorStyle.tabBlack,marginVertical: 10,}} numberOfLines={4}>{item.des}</Text>

          <HTMLView
            value={`<body>${item.content}<body>`}
            stylesheet={{
                body: {
                    color:ColorStyle.tabBlack,
                 paddingHorizontal: Styles.constants.X/6
                },}}
        />
          {this.state.item.image_url !==null && ( <ImageGrid
              style={{width: '100%', aspectRatio: 1, marginTop: 10}}
              images={this.getImageList(item)}
              onPress={(image, index) => {
                  this.imageOnPress(index);
              }}
          />)}

        <View
          style={GroupStyle.viewSelect}>
          <View
            style={{
              flex: 3,
              flexDirection: 'row',
              justifyContent: 'flex-start',
            }}>
            <TouchableOpacity
              onPress={() => this.likePost()}
              style={GroupStyle.itemSelection}>
              <Icon
                name={'hearto'}
                type={'antdesign'}
                size={20}
                color={item.is_like ? ColorStyle.tabActive : ColorStyle.tabBlack}
              />
              <Text
                style={{
                  ...Styles.text.text12,
                  color: item.is_like ? ColorStyle.tabActive : ColorStyle.tabBlack,
                  fontWeight: '700',
                }}>
                {item.total_like}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={GroupStyle.itemSelection}
              onPress={() => {
                this.goToPostDetail();
              }}>
              <Icon
                name={'comment'}
                type={'evilicon'}
                size={20}
                color={'#A7CDCC'}
              />
              <Text style={{...Styles.text.text12, color: '#A7CDCC'}}>
                {item.total_comment}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={GroupStyle.itemSelection}>
              <Icon
                name={'eyeo'}
                type={'antdesign'}
                size={20}
                color={'#014B56'}
              />
              <Text style={{...Styles.text.text12, color: '#014B56'}}>
                {item.total_seen}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{flex: 2}}>
            <TouchableOpacity
              onPress={() => {
                let shareUrl = GroupHandle.getInstance().getGroupPostUrl(
                  this.state.item,
                );
                Share.share({
                  title: strings('eplazaShare'),
                  message: shareUrl,
                }).then(r => {});
              }}
              style={{
                ...GroupStyle.itemSelection,
                justifyContent: 'flex-end',
              }}>
              <Icon name={'share'} type={'entypo'} size={20} />
            </TouchableOpacity>
          </View>
        </View>
      </ElevatedView>
    );
  }

  getImageList() {
    let item = this.state.item;
    let imgList = [];
    let images = item.image_url;
    if (images === undefined) {
      images = [];
    }
    images.forEach((image, index) => {
      imgList.push(constants.host + image.url);
    });
    return imgList;
  }
  imageOnPress(initIndex) {
    Actions.jump('fullViewImage', {images: this.getImageList(), initIndex});
  }
  onChangeData() {
    if (onChangeItemData !== undefined) {
      onChangeItemData(this.state.item, this.props.index);
    }
  }
  likePost() {
    let item = this.state.item;
    let params = {
        post_id: item.id,
    };
    GroupPostHandle.getInstance().likePost(params,(isSuccess, dataResponse) => {
        if (isSuccess ) {
          item = {
            ...this.state.item,
              is_like: !item.is_like,
              total_like: !item.is_like ? item.total_like + 1 : item.total_like - 1,
          };
          this.setState({item}, () => {
            isPostingLike = false;
            this.onChangeData();
          });
        }
      });
  }
  getImage(item,index){
      let avatar = '';
      if (
           item !== null
      ) {
              avatar = GlobalInfo.initApiEndpoint(item)
      } else {
          avatar = 'https://picsum.photos/84/84?' + index;
      }
      return avatar;
  }
  goToPostDetail() {
    if (this.props.onGoToPostDetail !== undefined) {
      this.props.onGoToPostDetail();
    }
  }
  keyExtractor = (item, index) => `groupPostRow_${index.toString()}`;
  changeDataItem(item) {
    this.setState({item});
  }
}
