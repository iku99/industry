import React, {Component} from 'react';
import {
    Alert,
    FlatList,
    Image, ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import {strings} from '../../../../../resource/languages/i18n';
import AppConstants from '../../../../../resource/AppConstants';
import Styles from '../../../../../resource/Styles';
import Toolbar from '../../../../elements/toolbar/Toolbar';
import ColorStyle from '../../../../../resource/ColorStyle';
import {CheckBox, Icon} from 'react-native-elements';
import DataUtils from '../../../../../Utils/DataUtils';
import GroupStyle from '../../GroupStyle';
import StoreStyle from "../../../store/StoreStyle";
import SellerStyles from "../../../seller/SellerStyles";
import constants from "../../../../../Api/constants";
import PermissionUtils from "../../../../../Utils/PermissionUtils";
import {PERMISSIONS} from "react-native-permissions";
import ImagePicker from "react-native-image-crop-picker";
import MediaHandle from "../../../../../sagas/MediaHandle";
import {EventRegister} from "react-native-event-listeners";
import GroupHandle from "../../../../../sagas/GroupHandle";
import {Actions} from "react-native-router-flux";
import SimpleToast from "react-native-simple-toast";
import AppCheckBox from "../../../../elements/AppCheckBox";
export default class InformationGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      groupName: this.props.data.name,
      des: this.props.data.des,
      cover:'',
      privacy: this.props.data.status===AppConstants.GROUP_PRIVACY.PRIVATE?true:false
    };
  }
 componentDidMount() {
     this.setState({cover:this.props.data.image_url})
     this.validateGroupName(this.props.data.name)}
  render() {
    return (
      <View style={Styles.container}>
        <Toolbar
          iconBack={true}
          title={strings('information')}
          textColor={ColorStyle.tabWhite}
          backgroundColor={ColorStyle.tabActive}
        />
         <ScrollView>
             {this.renderImageCover()}
             <View style={GroupStyle.information.container}>
                 <View style={{flexDirection: 'row'}}>
                     <Icon
                         name={'setting'}
                         type={'antdesign'}
                         size={20}
                         color={ColorStyle.gray}
                     />
                     <Text
                         style={{
                             ...Styles.text.text14,
                             color: ColorStyle.loading,
                         }}>
                         {strings('imGroupBtn')}
                     </Text>
                 </View>
                 <View
                     style={{
                         flexDirection: 'row',
                         alignItems: 'center',
                         justifyContent: 'space-between',
                     }}>
                     <Text
                         style={{
                             ...Styles.text.text14,
                             color: ColorStyle.loading,
                         }}>
                         {strings('name')}:
                     </Text>
                     <TextInput
                         style={[
                             Styles.input.inputLogin,
                             {
                                 width: '70%',
                                 borderColor: ColorStyle.gray,
                                 borderWidth: 1,
                                 backgroundColor: 'transparent',
                                 marginTop: 20,
                                 marginBottom: 30,
                             },
                         ]}
                         placeholder={'Name your group'}
                         value={this.state.groupName}
                         placeholderTextColor={ColorStyle.textInput}
                         onChangeText={value => {
                             this.validateGroupName(value);
                         }}
                     />

                     {this.state.validGroupName != null && (
                         <Icon
                             name={this.state.validGroupName ? 'check' : 'close'}
                             type="antdesign"
                             size={18}
                             color={
                                 this.state.validGroupName
                                     ? ColorStyle.tabActive
                                     : ColorStyle.red
                             }
                             containerStyle={{
                                 position: 'absolute',
                                 right: 1,
                                 alignItems: 'flex-end',
                             }}
                         />
                     )}
                 </View>
                 {this.renderCheckBox()}
             </View>
             <View style={GroupStyle.information.container}>
                 <View style={{flexDirection: 'row',alignItems:'center'}}>
                     <Icon
                         name={'menu'}
                         type={'entypo'}
                         size={20}
                         color={ColorStyle.gray}
                     />
                     <Text
                         style={{
                             ...Styles.text.text14,
                             color: ColorStyle.loading,
                         }}>
                         {strings('inchoduce')}
                     </Text>
                 </View>
                 <TextInput
                     placeholder={strings('addComment')}
                     value={this.state.des}
                     style={{
                         borderWidth: 1,
                         borderColor: ColorStyle.borderItemHome,
                         height: 100,
                         marginTop: 12,
                         paddingLeft:10,
                         paddingVertical:5
                     }}
                     onChangeText={text => this.setState({des: text})}
                     multiline={true}
                     underlineColorAndroid="transparent"
                 />
             </View>
             <TouchableOpacity
                 onPress={() => this.saveGroup()}
                 style={{
                     width: '40%',
                     backgroundColor: ColorStyle.tabActive,
                     alignSelf: 'center',
                     marginVertical: 20,
                     alignItems: 'center',
                     justifyContent: 'center',
                     paddingVertical: 12,
                     borderRadius: 10,
                     elevation: 2,

                 }}>
                 <Text
                     style={{
                         ...Styles.text.text18,
                         color: ColorStyle.tabWhite,
                         fontWeight: '700',
                     }}>
                     {strings('save')}
                 </Text>
             </TouchableOpacity>
         </ScrollView>
      </View>
    );
  }
  renderCheckBox(){
      return(
          <View
              style={{
                  paddingVertical: 10,
                  marginTop: 20,
              }}>
              <Text
                  style={{
                      ...Styles.text.text14,
                      color: ColorStyle.loading,
                  }}>
                  {strings('privacy')}
              </Text>

              <TouchableOpacity onPress={()=>{this.setState({privacy:false})}} style={{marginHorizontal: 5,flexDirection:'row',alignItems:'center',marginTop:10}}>
                  <Icon name={!this.state.privacy?'checkbox-active':'checkbox-passive'} type={'fontisto'} size={20} color={!this.state.privacy?ColorStyle.tabBlack:ColorStyle.gray} containerStyle={{}}/>
                  <View style={{   marginLeft:10,}}>
                  <Text
                      style={{
                          ...Styles.text.text16,
                          color: !this.state.privacy
                              ? ColorStyle.tabBlack
                              : ColorStyle.gray,

                          fontWeight: '700',
                      }}>
                      {strings('public')}
                  </Text>
                  <Text
                      style={{
                          ...Styles.text.text11,
                          color: !this.state.privacy
                              ? ColorStyle.tabBlack
                              : ColorStyle.gray,
                          maxWidth: '90%',
                      }}>
                      {strings('publicDes')}
                  </Text>
                  </View>
              </TouchableOpacity>
              <TouchableOpacity  onPress={()=>{this.setState({privacy:true})}} style={{marginHorizontal: 5,flexDirection:'row',alignItems:'center',marginTop:10}}>
                  <Icon name={this.state.privacy?'checkbox-active':'checkbox-passive'} type={'fontisto'} size={20} color={this.state.privacy?ColorStyle.tabBlack:ColorStyle.gray} containerStyle={{}}/>
                  <View style={{   marginLeft:10,}}>
                      <Text
                          style={{
                              ...Styles.text.text16,
                              color: this.state.privacy
                                  ? ColorStyle.tabBlack
                                  : ColorStyle.gray,

                              fontWeight: '700',
                          }}>
                          {strings('privacy')}
                      </Text>
                      <Text
                          style={{
                              ...Styles.text.text11,
                              color: this.state.privacy
                                  ? ColorStyle.tabBlack
                                  : ColorStyle.gray,
                              maxWidth: '90%',
                          }}>
                          {strings('privateDes')}
                      </Text>
                  </View>
              </TouchableOpacity>
          </View>
      )
  }
  renderImageCover(){
        if(this.state.cover===undefined || this.state.cover===null){
            return(
                <View style={{ ...StoreStyle.bannerStore}}>
                    <View style={{backgroundColor:ColorStyle.borderButton,width:'100%',height:'100%'}}/>
                    <TouchableOpacity onPress={()=>this.getImageCover('cover')} style={SellerStyles.btnSettingCover}>
                        <Icon name={'setting'} type={'antdesign'} size={25} color={ColorStyle.tabActive}/>
                    </TouchableOpacity>
                </View>
            )
        }else
            return(
                <View style={{ ...StoreStyle.bannerStore}}>
                    <Image source={{uri:constants.host+this.state.cover}} style={{width:'100%',height:'100%'}}/>
                    <TouchableOpacity onPress={()=>this.getImageCover('cover')} style={SellerStyles.btnSettingCover}>
                        <Icon name={'setting'} type={'antdesign'} size={25} color={ColorStyle.tabActive}/>
                    </TouchableOpacity>
                </View>
            )
    }
  getImageCover(item){
        if (
            PermissionUtils.checkPermission(PERMISSIONS.IOS.PHOTO_LIBRARY)
        ) {
            ImagePicker.openPicker({
                width: 400,
                height: 400,
                compressImageMaxWidth: 800,
                compressImageMaxHeight: 800,
                cropping: true,
            }).then(image => {
                MediaHandle.getInstance().uploadProfileAvatar(image,(_)=>{
                },(isSuccess, responseData)=>{
                    if(isSuccess){
                        let data=responseData._response;
                        this.setState({[item]: JSON.parse(data).url});
                    }
                })
            });
        }
    }
  validateGroupName(groupName) {
    if (!DataUtils.stringNullOrEmpty(groupName)) {
      this.setState({
        groupName: groupName,
        validGroupName: groupName.length > 5,
      });
    }
  }
  saveGroup() {
      if(!this.state.validGroupName){
          SimpleToast.show(strings('checkName'),SimpleToast.SHORT)
          return
      }
      let params={
          name:this.state.groupName,
          status:this.state.privacy?AppConstants.GROUP_PRIVACY.PRIVATE:AppConstants.GROUP_PRIVACY.PUBLIC,
          des:this.state.des,
          image_url:this.state.cover,
      }
      GroupHandle.getInstance().updateGroup(this.props.data.id,params,(isSuccess, responseData)=>{
      if (isSuccess) {
          SimpleToast.show(strings('successOjo'))
          EventRegister.emit(AppConstants.EventName.UPDATE_GROUP)
          Actions.pop()
      } else {
          Alert.alert(
              '',
              strings('failedOjo'),
              [{text: strings('close'), onPress: () => {}}],
              {cancelable: false},
          );
      }

  })
  }
}
