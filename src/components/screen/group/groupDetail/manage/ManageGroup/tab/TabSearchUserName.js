import React, {useState} from "react";
import AppConstants from "../../../../../../../resource/AppConstants";
import {FlatList, Image, Text, TextInput, TouchableOpacity, View} from "react-native";
import GroupStyle from "../../../../GroupStyle";
import MediaUtils from "../../../../../../../Utils/MediaUtils";
import Styles from "../../../../../../../resource/Styles";
import NavigationUtils from "../../../../../../../Utils/NavigationUtils";
import ColorStyle from "../../../../../../../resource/ColorStyle";
import {strings} from "../../../../../../../resource/languages/i18n";
import EmptyView from "../../../../../../elements/reminder/EmptyView";
import {Icon} from "react-native-elements";
import GroupHandle from "../../../../../../../sagas/GroupHandle";
import SimpleToast from "react-native-simple-toast";
import ViewUtils from "../../../../../../../Utils/ViewUtils";
export default function TabSearchUserName({items,group,onSearch,onSend}){
    const [userName,setUserName]=useState('')
    const _renderItem=({item,index})=>{
        return(
                                <TouchableOpacity key={`listMember_${index.toString()}`}
                                    onPress={() => sendInvite(item)}
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        paddingVertical: 10,
                                        paddingHorizontal: Styles.constants.X/2,
                                        borderColor: ColorStyle.borderItemHome,
                                        borderWidth: 1,
                                    }}>
                                    <View style={{flex: 1}}>
                                        <Image
                                            source={ MediaUtils.getAvatar(item,index)}
                                            style={{...Styles.icon.iconShow, borderRadius: 50}}
                                        />
                                    </View>
                                    <View style={{flex: 4}}>
                                        <Text
                                            style={{
                                                ...Styles.text.text14,
                                                color: ColorStyle.tabBlack,
                                                fontWeight: '700',
                                            }}>
                                            {item.user_name}
                                        </Text>
                                    </View>
                                    <View  style={{flex: 1}}>
                                        <Icon name={'add'} type={'materialicons'} size={15} color={'#BABABA'}/>
                                    </View>
                                </TouchableOpacity>
        )
    }
    function sendInvite(username){
        console.log(group)
        let param={
            "team_name":group.name,
            "team_id": group.id,
            "customer_id":username.id
        }
        GroupHandle.getInstance().inviteUserName(param,(isSuccess,dataSuccess)=>{
           if(dataSuccess.code===0){
               ViewUtils.showAlertDialog(strings('inviteUserNameTrue').replace('{name}',username.name), null);
return
           }
            ViewUtils.showAlertDialog(strings('inviteUserNameFa').replace('{name}',username.name), null);

        })
    }
    const keyExtractor = (item, index) =>`list_member ${index.toString()}` ;
    if(items===undefined) return null
    if(items.type===AppConstants.POST_GROUPS.BUTTON_ADD){
        return(
                <View
                    style={GroupStyle.groupPost}>
                  <View style={GroupStyle.bodySearch}>
                      <TextInput
                          style={{
                                width:'90%',
                              height:Styles.constants.X * 1.2,
                              paddingLeft:10
                          }}
                          placeholder={strings('textMember')}
                          value={userName}
                          placeholderTextColor={ColorStyle.textInput}
                          onChangeText={value => {
                              setUserName(value)

                          }}
                      />
                      <TouchableOpacity style={GroupStyle.btnSearch}  onPress={()=>{ onSearch(userName);}}>
                          <Icon name={'search1'} type={'antdesign'} size={20} color={ColorStyle.tabWhite} />
                      </TouchableOpacity>
                  </View>
                </View>
        )
    }else {
        if(items.data.length===0){
            return  <EmptyView text={strings('groupEmptyUser')} styles={{marginTop:50}}/>
        }else {
            return(
                <FlatList
                    data={items.data}
                    renderItem={_renderItem}
                    keyExtractor={keyExtractor}
                    style={{width:'100%', height:'100%'}}/>
            );
        }
    }
}
