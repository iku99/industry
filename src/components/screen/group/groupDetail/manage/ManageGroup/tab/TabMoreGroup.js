import React from "react";
import {Image, Text, TouchableOpacity, View} from "react-native";
import Styles from "../../../../../../../resource/Styles";
import ColorStyle from "../../../../../../../resource/ColorStyle";
import {strings} from "../../../../../../../resource/languages/i18n";
import {Icon} from "react-native-elements";
import TimeUtils from "../../../../../../../Utils/TimeUtils";
import GroupStyle from "../../../../GroupStyle";
import DateUtil from "../../../../../../../Utils/DateUtil";
import MediaUtils from "../../../../../../../Utils/MediaUtils";
export default function TabMoreGroup({item,onOutGroup,admins,countMember}){


    function renderMember() {
        return (
            <View
                style={{
                    width: Styles.constants.widthScreenMg24,
                    alignSelf:'center',
                    marginVertical:Styles.constants.X/6
                }}>
                <Text
                    style={{
                        ...Styles.text.text20,
                        color: ColorStyle.tabBlack,
                        fontWeight: '700',
                    }}>
                    {strings('members')}
                </Text>
                <View style={{marginHorizontal: 30}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between',marginTop:10,width:Styles.constants.widthScreenMg24-Styles.constants.X}}>
                        <Text style={{...Styles.text.text14, color: ColorStyle.gray}}>
                            {countMember} {strings('member1').toLowerCase()}
                        </Text>
                    </View>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabBlack,
                            fontWeight: '700',
                            marginTop: 10,
                        }}>
                        {strings('admin')}
                    </Text>
                    <View style={{flexDirection: 'row',marginTop:10}}>
                        {admins.map((item,index) => (
                            <View  style={{alignItems:'center'}}   key={index.toString()}>
                                <Image
                                    source={ MediaUtils.getAvatar(item.customers)}
                                    style={{width: 24, height: 24, borderRadius: 24,borderWidth:0.5,borderColor:'black'}}
                                />
                                <Text style={{fontSize:11,color:ColorStyle.tabBlack,marginTop:5}}>{item.customers.full_name}</Text>
                            </View>
                        ))}
                    </View>
                </View>
            </View>
        );
    }
    return(
        <View  style={{
            alignSelf: 'center',
            alignItems: 'flex-start',
        }}>
            <View
                style={{
                    width: Styles.constants.widthScreenMg24,
                    alignSelf:'center',
                    marginVertical:Styles.constants.X/6
                }}>
                <Text
                    style={{
                        ...Styles.text.text20,
                        color: ColorStyle.tabBlack,
                        fontWeight: '700',

                    }}>
                    {strings('introduce')}
                </Text>
                <Text
                    style={{
                        ...Styles.text.text14,
                        color: ColorStyle.tabBlack,
                        fontWeight: '300',
                        lineHeight: 20,
                    }}>
                    {item.description}
                </Text>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                    }}>
                    <Icon
                        name={'clockcircle'}
                        type={'antdesign'}
                        size={25}
                        color={ColorStyle.tabBlack}
                    />
                    <View style={{flex: 4, marginLeft: 10}}>
                        <Text
                            style={{
                                ...Styles.text.text14,
                                color: ColorStyle.tabBlack,
                                fontWeight: '700',
                            }}>
                            {strings('history')}
                        </Text>
                        <Text
                            style={{
                                ...Styles.text.text14,
                                color: ColorStyle.tabBlack,
                                fontWeight: '400',
                            }}>
                            {strings('desGroup')} {DateUtil.formatDate('DD/MM/YYYY',item.created_date)}
                        </Text>
                    </View>
                </View>
            </View>
            <View style={{height:1,backgroundColor:ColorStyle.borderItemHome,width:Styles.constants.widthScreen}}/>
            {renderMember()}
            <View style={{height:1,backgroundColor:ColorStyle.borderItemHome,width:Styles.constants.widthScreen}}/>
        </View>
    )
}
