import React from "react";
import {FlatList, Image, Text, TextInput, TouchableOpacity, View} from "react-native";
import Styles from "../../../../../../../resource/Styles";
import ColorStyle from "../../../../../../../resource/ColorStyle";
import {strings} from "../../../../../../../resource/languages/i18n";
import {Icon} from "react-native-elements";
import GroupStyle from "../../../../GroupStyle";
import MediaUtils from "../../../../../../../Utils/MediaUtils";
import NavigationUtils from "../../../../../../../Utils/NavigationUtils";
import AppConstants from "../../../../../../../resource/AppConstants";
import EmptyView from "../../../../../../elements/reminder/EmptyView";
import GroupPostRow from "../../../row/GroupPostRow";
export default function TabHomeGroup({items,group,callback,userInfo,goToDetail,onChangeItemData}){
   const _renderItem=({item,index})=>{
        return(
            <GroupPostRow
                index={index}
                item={item}
                groupId={group.id}
                groupName={group.name}
                // ref={ref => (groupPostRef[index] = ref)}
                onChangeItemData={(item, index) => {
                    onChangeItemData(item, index, false);
                }}
                onGoToPostDetail={() => goToDetail(item,index)}
            />
        )
    }
   const keyExtractor = (item, index) =>`list_commen ${index.toString()}` ;
 if(items===undefined) return null
    if(items.type===AppConstants.POST_GROUPS.BUTTON_ADD){
        return(
            <View style={GroupStyle.groupPost}>
                <TouchableOpacity style={GroupStyle.itemOption}>
                    <Image
                        source={MediaUtils.getAvatar(userInfo)}
                        style={{...Styles.icon.iconShow, borderRadius: 50}}
                    />
                </TouchableOpacity>
                <TouchableOpacity style={GroupStyle.itemComment}

                                  onPress={()=>NavigationUtils.goToAddPost(group,()=>{callback()})}>
                    <View style={GroupStyle.viewComment}>
                        <Text
                            style={{
                                ...Styles.text.text14,
                                color: ColorStyle.gray,
                                marginLeft: 10,
                            }}>
                            {strings('textComment')}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }else {
        if(items.data.length===0){
            return  <EmptyView text={strings('groupEmptyPost')} styles={{marginTop:50}}/>
        }else {
            return(
                <FlatList
                    data={items.data}
                    renderItem={_renderItem}
                    keyExtractor={keyExtractor}
                    style={{width:'100%', height:'100%'}}/>
            );
        }
    }
}
