import React from "react";
import {Alert, Text, TouchableOpacity, View} from "react-native";
import Styles from "../../../../../../../resource/Styles";
import ColorStyle from "../../../../../../../resource/ColorStyle";
import {strings} from "../../../../../../../resource/languages/i18n";
import GroupStyle from "../../../../GroupStyle";
import NavigationUtils from "../../../../../../../Utils/NavigationUtils";
import {Icon} from "react-native-elements";
export default function TabMember({item,callback}){
    return(
        <View style={{marginHorizontal: Styles.constants.marginHorizontalAll}}>
            <View style={{marginTop: 20}}>
                <Text
                    style={{
                        ...Styles.text.text20,
                        color: ColorStyle.loading,
                        fontWeight: '700',
                    }}>
                    {strings('manage')}
                </Text>
                <TouchableOpacity
                    style={GroupStyle.itemTabManage.button}
                    onPress={() => {
                        NavigationUtils.goToPostManager(item.id);
                    }}>
                    <View
                        style={{
                            flex: 3,
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                        }}>
                        <Icon
                            name={'post-outline'}
                            type="material-community"
                            size={25}
                            color={ColorStyle.tabBlack}
                        />
                        <Text style={GroupStyle.itemTabManage.text} numberOfLines={1}>
                            {strings('textPostBtn')}
                        </Text>
                    </View>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'flex-end',
                            alignItems: 'center',
                        }}>
                        {/*<Text*/}
                        {/*    style={{*/}
                        {/*        ...Styles.text.text11,*/}
                        {/*        color: ColorStyle.tabWhite,*/}
                        {/*        paddingVertical: 5,*/}
                        {/*        backgroundColor: ColorStyle.tabActive,*/}
                        {/*        paddingHorizontal: 17,*/}
                        {/*        borderRadius: 5,*/}
                        {/*        fontWeight: '700',*/}
                        {/*    }}>*/}

                        {/*</Text>*/}
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    style={GroupStyle.itemTabManage.button}
                    onPress={() => {
                        NavigationUtils.goToMemberManager(item.id);
                    }}>
                    <View
                        style={{
                            flex: 3,
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                        }}>
                        <Icon
                            name={'shield'}
                            type="feather"
                            size={25}
                            color={ColorStyle.tabBlack}
                        />
                        <Text style={GroupStyle.itemTabManage.text} numberOfLines={1}>
                            {strings('textAddBtn')}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{marginTop: 20}}>
                <Text
                    style={{
                        ...Styles.text.text20,
                        color: ColorStyle.loading,
                        fontWeight: '700',
                    }}>
                    {strings('setting')}
                </Text>
                <TouchableOpacity
                    style={GroupStyle.itemTabManage.button}
                    onPress={() =>
                        NavigationUtils.goToInformationGroupManage(item)
                    }>
                    <View
                        style={{
                            flex: 3,
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                        }}>
                        <Icon
                            name={'information-circle-outline'}
                            type="ionicon"
                            size={25}
                            color={ColorStyle.tabBlack}
                        />
                        <Text style={GroupStyle.itemTabManage.text} numberOfLines={1}>
                            {strings('imGroupBtn')}
                        </Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    style={GroupStyle.itemTabManage.button}
                    onPress={() =>
                        Alert.alert(
                            '',
                            strings('deleteGroup'),
                            [
                                {text: strings('cancel'), onPress: () => {}},
                                {
                                    text: strings('delete'),
                                    onPress: () => {
                                        callback()
                                    },
                                },
                            ],
                            {cancelable: false},
                        )
                    }>
                    <View
                        style={{
                            flex: 3,
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                        }}>
                        <Icon
                            name={'delete-outline'}
                            type="material-community"
                            size={25}
                            color={ColorStyle.tabBlack}
                        />
                        <Text style={GroupStyle.itemTabManage.text} numberOfLines={1}>
                            {strings('deleteGroup')}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>

    )
}
