import React, {Component} from "react";
import {
    ActivityIndicator, Alert,
    FlatList,
    Image,
    ImageBackground,
    ScrollView,
    Text, TextInput,
    TouchableOpacity,
    View
} from "react-native";
import Styles from "../../../../../../resource/Styles";
import HeaderGroupAnimation from "../../../../../elements/animation/headerGroup/HeaderGroupAnimation";
import {Actions} from "react-native-router-flux";
import {Icon} from "react-native-elements";
import ColorStyle from "../../../../../../resource/ColorStyle";
import {strings} from "../../../../../../resource/languages/i18n";
import MediaUtils from "../../../../../../Utils/MediaUtils";
import AppConstants from "../../../../../../resource/AppConstants";
import GroupStyle from "../../../GroupStyle";
import {SceneMap, TabView} from 'react-native-tab-view';
import TabComment from "../tab/TabComment";
import TabInvite from "../tab/TabInvite";
import TabManage from "../tab/TabManage";
import ViewUtils from "../../../../../../Utils/ViewUtils";
import GroupPostHandle from "../../../../../../sagas/GroupPostHandle";
import NavigationUtils from "../../../../../../Utils/NavigationUtils";
import GroupPostRow from "../../row/GroupPostRow";
import GroupHandle from "../../../../../../sagas/GroupHandle";
import DateUtil from "../../../../../../Utils/DateUtil";
import DataUtils from "../../../../../../Utils/DataUtils";
import constants from "../../../../../../Api/constants";
import EmptyView from "../../../../../elements/reminder/EmptyView";
import SimpleToast from "react-native-simple-toast";
import AccountHandle from "../../../../../../sagas/AccountHandle";
import GlobalInfo from "../../../../../../Utils/Common/GlobalInfo";
import {EventRegister} from "react-native-event-listeners";
import TabHomeGroup from "./tab/TabHomeGroup";
import TabSearchUserName from "./tab/TabSearchUserName";
import TabMember from "./tab/TabMember";
let LIMIT = 15;
let addedSuggestionGroup = false;
let groupPosts = [];
let groupPostRef = [];
const Data=[
    {
        type:AppConstants.POST_GROUPS.BUTTON_ADD
    }
]
export default class ManageGroup extends Component {
    constructor(props) {
        super(props);
        this.state={
            data:[],
            page:1,
            name:'',
            finishLoad: false,
            userInfo:undefined,
            loading:true,
            canLoadData:true,
            routes: [
                {
                    key: 'tabComment',
                    title: strings('group'),
                    icon: 'user-friends',
                    type: 'font-awesome-5',
                },
                {
                    key: 'tabInvite',
                    title: strings('invite'),
                    icon: 'user-plus',
                    type: 'font-awesome-5',
                },
                {
                    key: 'tabManage',
                    title: strings('manage'),
                    icon: 'user-cog',
                    type: 'font-awesome-5',
                },
            ],
}
    }
    async componentDidMount() {
        let userInfo = await GlobalInfo.getUserInfo();
        EventRegister.addEventListener(
            AppConstants.EventName.CHANGE_AVATAR,
            data => {
                this.setState({data:{}},()=>{this.setState({userInfo:data})});
            },
        );
        this.setState({userInfo});
        this.onPressTab(0)
    }
    render() {
        return(
            <View style={Styles.container}>
                <HeaderGroupAnimation
                    style={{flex: 1}}
                    bodyText="Back"
                    backTextStyle={{fontSize: 14, color: '#000'}}
                    titleStyle={{fontSize: 22, left: 20, bottom: 20, color: '#000'}}
                    headerMaxHeight={Styles.constants.X * 8.5}
                    toolbarColor={ColorStyle.tabActive}
                    disabled={false}
                    renderLeft={() => (
                        <TouchableOpacity style={{marginLeft:Styles.constants.marginHorizontal20}} onPress={() => Actions.pop()}>
                            <Icon
                                name="left"
                                type="antdesign"
                                size={25}
                                color={ColorStyle.tabWhite}
                            />
                        </TouchableOpacity>
                    )}
                    renderRight={() => (
                        <TouchableOpacity onPress={() => Actions.jump('profile')}>
                            <Icon
                                name="ios-notifications-outline"
                                type="ionicon"
                                size={25}
                                color={ColorStyle.tabWhite}
                                style={{marginRight: Styles.constants.marginHorizontal20}}
                            />
                        </TouchableOpacity>
                    )}
                    renderBackground={{
                        uri: this.getGroupImageUrl(this.props.item),
                        headers: {Authorization: 'someAuthToken'},
                    }}
                    renderInfo={() => (
                        <View style={{width:Styles.constants.widthScreenMg24}}>
                            <Text
                                style={{
                                    ...Styles.text.text24,
                                    color: ColorStyle.tabWhite,
                                    fontWeight: '700',
                                }}>
                                {this.props.item.name}
                            </Text>

                            <Text
                                style={{
                                    ...Styles.text.text14,
                                    color: ColorStyle.tabWhite,
                                    fontWeight: '700',
                                }}>
                                {this.checkPrivacy()}
                            </Text>
                        </View>
                    )}
                    renderBody={() =>
                         <View>
                             <Image
                                 style={{...GroupStyle.bannerGroupDetail}}
                                 source={{
                                     uri: MediaUtils.getGroupCover(this.props.item, 0),
                                     headers: {Authorization: 'someAuthToken'},
                                 }}
                                 resizeMode={'cover'}
                             />
                             <View style={{...GroupStyle.bannerGroupDetail,position:'absolute',backgroundColor:'rgba(0, 0, 0, 0.7)'}}/>
                         </View>
                }
                    tabView={()=>
                        <View style={{ flexDirection: 'row',
                            height:'100%',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 0,
                            },
                            shadowOpacity: 0.4,
                            shadowRadius: 3,
                            elevation: 2,
                            backgroundColor:ColorStyle.tabWhite,}}>
                            {this.state.routes.map((route, i) => {
                                return (
                                    <TouchableOpacity
                                        style={{
                                            flex: 1,
                                            alignItems: 'center',
                                            justifyContent: 'center',

                                            borderBottomColor:i === this.state.index ? ColorStyle.tabActive : ColorStyle.tabBlack,
                                            borderBottomWidth:i === this.state.index ?2:0
                                        }}
                                        onPress={() => this.onPressTab(i)}>
                                        <Icon
                                            name={route.icon}
                                            type={route.type}
                                            size={20}
                                            color={i === this.state.index ? ColorStyle.tabActive : ColorStyle.tabBlack}
                                        />
                                        <Text
                                            style={{
                                                ...Styles.text.text14,
                                                color:
                                                    i === this.state.index ? ColorStyle.tabActive : ColorStyle.tabBlack,
                                                fontWeight: i === this.state.index ? '700' : '400',
                                            }}>
                                            {route.title}
                                        </Text>
                                    </TouchableOpacity>
                                );
                            })}
                        </View>
                    }
                >
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.state.data}
                        keyExtractor={this.keyExtractor}
                        renderItem={this.renderItem}
                        style={{flex:1, height:'100%',backgroundColor:'transparent'}}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={() => this.handleLoadMore()}
                        removeClippedSubviews={true} // Unmount components when outside of window
                        windowSize={10}/>
                </HeaderGroupAnimation>
            </View>
        )
    }

    renderFooter = () => {
        //it will show indicator at the bottom of the list when data is loading otherwise it returns null
        if (!this.state.loading) return null;
        return (
            <ActivityIndicator
                style={{ color: '#000' }}
            />
        );
    };
    handleLoadMore() {
        if (this.state.loading) return ;
        if (!this.state.canLoadData) return;
        this.setState({loading: true, page: this.state.page + 1}, this.getGroupPostData);
    }
    keyExtractor = (item, index) => `list_post ${index.toString()}`;
    renderItem=({item,index})=>{
        switch (this.state.index) {
            case 0:  return(
                <TabHomeGroup items={item}
                              group={this.props.item}
                              userInfo={this.state.userInfo}
                              onChangeItemData={(elm,num)=>{
                                  this.changeItem(elm, num, true);
                              }}
                              callback={()=>{
                                  this.setState({page:1},()=>{
                                      this.onPressTab(0)
                                      // this.getGroupPostData()
                                  })
                              }}
                              goToDetail={(post,num)=>{
                                  console.log("this.props.item:",post)
                                  NavigationUtils.goToPostDetail(
                                      post,
                                      this.props.item.id,
                                      this.props.item.name,
                                      num,
                                      (item, num) => {
                                          groupPostRef[num].changeDataItem(item);
                                          this.changeItem(item, num, true);
                                      },
                                  );
                              }
                              }/>
            )
            case 1:
                return (
                    <TabSearchUserName group={this.props.item}
                                       items={item}
                                       onSend={(text)=>{
                    }} onSearch={(text)=>{
                        this.queryMember(text)
                    }}/>
                )
            case 2:return(
                <TabMember item={this.props.item}

                           callback={()=>{
                               this.deleteGroup(item.id);
                           }}/>
            );break
        }
    }
    onPressTab(i){
            switch (i) {
                case 0:return this.setState({index:i,data:[]},()=>this.getGroupPostData())
                case 1:return this.setState({index:i,data:[]},()=>this.getMember())
                case 2:return this.setState({index:i,data:[{}]})
            }

    }
    queryMember(text){
        this.setState({data:Data})
        let param={
            user_name:text,
            page_index:1,
            page_size: 20
        }
        AccountHandle.getInstance().searchUser(param,(isSuccess,res)=>{
            console.log(res)
            if(isSuccess){
                let listData=this.state.data;
                let abc = {
                    data: res.data,
                    type:AppConstants.POST_GROUPS.LIST_ITEM,
                };
                listData = [...listData, ...[abc]];
                this.setState({data:listData})
            }
        })
    }
    deleteGroup(id, index) {
        GroupHandle.getInstance().deleteGroup(id, isSuccess => {
            if (isSuccess) {
                SimpleToast.show(strings('deleteGroupSuccess',SimpleToast.SHORT))
                this.props.callback()
                Actions.pop()
            } else {
                ViewUtils.showAlertDialog(strings('deleteGroupFa'));
            }
        });
    }
    changeItem(item, index, forceRefresh) {
         let listData=this.state.data
         listData[1].data[index]=item;
         this.setState({data:listData})
    }
    getGroupImageUrl(item) {
        return MediaUtils.getGroupCover(item, 0);
    }
    addSuggestionGroup() {
        addedSuggestionGroup = true;
        this.setState({finishLoad: true});
    }
    checkPrivacy() {
        let privacy = this.props.item.status;
        if (privacy === AppConstants.GROUP_PRIVACY.PRIVATE) {
            return strings('privateGroup');
        } else {
            return strings('publicGroup');
        }
    }
    getGroupPostData() {
        this.setState({data:Data})
        let param = {
            status:AppConstants.Status_Post.ALL,
            page_index: this.state.page,
            page_size: LIMIT,
        };
        GroupPostHandle.getInstance().getPostsOfGroup(
            this.props.item.id,
            param,
            (isSuccess, dataResponse) => {
                let canLoadData = false;
                if (isSuccess && dataResponse.data !== undefined) {

                    if (dataResponse.data.length < LIMIT) {
                        this.setState({canLoadData: false});
                    }
                    let listData = this.state.data;
                    let data={
                        type:AppConstants.POST_GROUPS.LIST_ITEM,
                        data: dataResponse.data
                    }
                    listData = [...listData,...[data]];
                    this.setState({data: listData});
                    this.setState({loading: false}, () => {
                        if (this.state.page === 0 && !canLoadData) {
                            this.addSuggestionGroup();
                        }
                    });
                }
            },
        );
    }
    getMember() {this.setState({data:Data})}

}
