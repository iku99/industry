import React, {Component} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {strings} from '../../../../../resource/languages/i18n';
import ViewUtils from '../../../../../Utils/ViewUtils';
import ColorStyle from '../../../../../resource/ColorStyle';
import Styles from '../../../../../resource/Styles';
import Toolbar from '../../../../elements/toolbar/Toolbar';
import {SceneMap, TabView} from 'react-native-tab-view';
import TabMember from './tab/TabMember';
import TabPost from './tab/TabPost';

let LIMIT = 20;
let shouldRefresh = false;
export default class PostManager extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: false,
      page: 0,
      canLoadData: true,
      index: 0,

      routes: [
        {key: 'tabMember', title: strings('members2')},
        {key: 'tabPost', title: strings('addPostToGroup')},
      ],
    };
  }
  // componentDidMount() {
  //   this.getData();
  // }
  // componentWillUnmount() {
  //   if (shouldRefresh && this.props.onRefresh !== undefined) {
  //     this.props.onRefresh();
  //   }
  // }
  _handleIndexChange = index => this.setState({index});

  _renderScene = SceneMap({
    tabMember: () => <TabMember groupId={this.props.groupId}/>,
    tabPost: () => <TabPost groupId={this.props.groupId}/>,
  });
  _renderTabBar = props => {
    return ViewUtils.renderTabPostApproval(props, this.state.index, i => {
      this.setState({index: i});
    });
  };
  render() {
    return (
      <View style={Styles.container}>
        <Toolbar
          iconBack={true}
          title={strings('approve')}
          textColor={ColorStyle.tabWhite}
          backgroundColor={ColorStyle.tabActive}
        />
        <TabView
          navigationState={this.state}
          renderScene={this._renderScene}
          renderTabBar={this._renderTabBar}
          onIndexChange={this._handleIndexChange}
        />
      </View>
    );
  }
}
