import React, {Component} from 'react';
import GroupPostHandle from '../../../../../sagas/GroupPostHandle';
import ViewUtils from '../../../../../Utils/ViewUtils';
import DataUtils from '../../../../../Utils/DataUtils';
import {
  FlatList,
  Image,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Styles from '../../../../../resource/Styles';
import Toolbar from '../../../../elements/toolbar/Toolbar';
import ColorStyle from '../../../../../resource/ColorStyle';
import {strings} from '../../../../../resource/languages/i18n';
import {Icon} from 'react-native-elements';
import GroupHandle from '../../../../../sagas/GroupHandle';
import constants from '../../../../../Api/constants';
import DetailMember from '../../modal/DetailMember';
import DateUtil from '../../../../../Utils/DateUtil';
import AppConstants from "../../../../../resource/AppConstants";
import {UIActivityIndicator} from "react-native-indicators";
import MediaUtils from "../../../../../Utils/MediaUtils";
import EmptyView from "../../../../elements/reminder/EmptyView";

let LIMIT = 20;
const Data=[
    {
        type:AppConstants.LIST_MEMBER_TYPE.BTN,
    },
    {
        type:AppConstants.LIST_MEMBER_TYPE.LIST_ADMIN,
    },
]
export default class MemberManage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: Data,
      loading: false,
      page: 1,
      canLoadData: true,
      showRole: false,
      committingData: false,
      showSelectProduct: false,
      dataItem: [],
        listAdmin:[]
    };
  }
  componentDidMount() {
    this.getDataMember();
    this.getDataAdmin()
  }

  render() {
    return (
      <View style={Styles.container}>
        <Toolbar
          backgroundColor={ColorStyle.tabActive}
          textColor={ColorStyle.tabWhite}
          title={strings('textAddBtn')}
        />
          <FlatList
              showsVerticalScrollIndicator={false}
              horizontal={false}
              data={this.state.data}
              keyExtractor={this.keyExtractor}
              renderItem={this._renderItem}
              ListFooterComponent={this.renderFooter.bind(this)}
              onEndReachedThreshold={0.4}
              onEndReached={() => this.handleLoadMore()}
              removeClippedSubviews={true} // Unmount components when outside of window
              windowSize={10}
          />

        {this.state.showSelectProduct && (
          <View
            style={{
              backgroundColor: 'rgba(0,0,0,0.5)',
              position: 'absolute',
              top: 0,
              length: 0,
              width: '100%',
              height: '100%',
              paddingHorizontal: '3%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <DetailMember
              onClose={() => this.setState({showSelectProduct: false})}
              item={this.state.dataItem}
              groupId={this.props.groupId}
              onPostProduct={()=>{
                  this.setState({data:[]},()=>{
                      this.getDataMember();
                      this.getDataAdmin()
                  })
              }}
            />
          </View>
        )}
      </View>
    );
  }
  handleLoadMore() {
        if (this.state.loading) {
            return;
        }
        if(this.state.canLoadData) return;
        this.setState(
            {loading: true, page: this.state.page + 1},
            // this.getNewData,
        );
    }
  keyExtractor = (item, index) => `memberManage_${index.toString()}`;
  renderFooter = () => {
        if (!this.state.loading) {
            return null;
        }
        if(!this.state.canLoadData){return null}
        return <UIActivityIndicator color={ColorStyle.tabActive} size={20} />;
    };
  _renderItem=({item,index})=>{
        switch (item.type) {
            case AppConstants.LIST_MEMBER_TYPE.BTN:
                return this.renderBtn()
            case AppConstants.LIST_MEMBER_TYPE.LIST_ADMIN:
                return this.renderListAdmin()
            case AppConstants.LIST_MEMBER_TYPE.LIST_MEMBER:
                return this.renderListMember(item)
            default:
                return null;
        }
    }
  renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => this.setState({showSelectProduct: true, dataItem: item})}
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingVertical: 5,
          paddingHorizontal: 24,
          borderColor: ColorStyle.borderItemHome,
          borderWidth: 1,
        }}>
        <View style={{flex: 1}}>
          <Image
            source={MediaUtils.getAvatar(item.customers)}
            style={{...Styles.icon.iconShow, borderRadius: 50}}
          />
        </View>
        <View style={{flex: 4}}>
          <Text
            style={{
              ...Styles.text.text14,
              color: ColorStyle.tabBlack,
              fontWeight: '700',
            }}>
            {item.customers.full_name}
          </Text>
          <Text style={{...Styles.text.text14, color: ColorStyle.gray}}>
            {strings('joinFrom')}
            {DateUtil.formatDate('DD/MM/YYYY', item.join_at)}
          </Text>
        </View>
        <Icon
          name={'user-cog'}
          type="font-awesome-5"
          style={{display: item.owner_group ? 'flex' : 'none'}}
        />
      </TouchableOpacity>
    );
  };
  renderBtn(){
      return(
          <View
              style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingHorizontal: 30,
                  paddingVertical: 28,
                  backgroundColor: ColorStyle.tabWhite,
                  borderColor: ColorStyle.borderItemHome,
                  elevation: 2,
              }}>
              <TextInput
                  style={[
                      Styles.input.inputLogin,
                      {
                          width: '90%',
                          borderColor: ColorStyle.gray,
                          borderWidth: 1,
                          backgroundColor: 'transparent',
                      },
                  ]}
                  placeholder={strings('textMember')}
                  value={this.state.name}
                  placeholderTextColor={ColorStyle.textInput}
                  onChangeText={value => {
                      this.queryCategory(value);
                  }}
              />
              <TouchableOpacity>
                  <Icon name={'search1'} type={'antdesign'} size={25} />
              </TouchableOpacity>
          </View>
      )
  }
  renderListMember(item){
     if(item.data.length===0){
         return(
             <View
                 style={{marginTop: 16, flex: 1}}>
                 <Text
                     style={{
                         ...Styles.text.text20,
                         color: ColorStyle.loading,
                         fontWeight: '700',
                         padding: 24,
                     }}>
                     {strings('members')}
                 </Text>

                <EmptyView text={strings('notMember')}/>
             </View>
         )
     }else {
         return(
             <View
                 style={{marginTop: 16, flex: 1}}>
                 <Text
                     style={{
                         ...Styles.text.text20,
                         color: ColorStyle.loading,
                         fontWeight: '700',
                         padding: 24,
                     }}>
                     {strings('members')}
                 </Text>

                 <FlatList
                     data={item.data}
                     renderItem={this.renderItem}
                     style={{
                         backgroundColor: ColorStyle.tabWhite,
                         width: Styles.constants.widthScreen,
                         height: '100%',
                     }}
                     showsVerticalScrollIndicator={false}
                 />
             </View>
         )
     }
  }
  renderListAdmin(){
      return(
          <View
              style={{
                  flex: 1,
                  marginTop: 16,
              }}>
              <Text
                  style={{
                      ...Styles.text.text20,
                      color: ColorStyle.loading,
                      fontWeight: '700',
                      padding: 24,
                  }}>
                  {strings('admin')}
              </Text>

              <FlatList
                  data={this.state.listAdmin}
                  style={{
                      backgroundColor: ColorStyle.tabWhite,
                      width: '100%',
                      height: '100%',
                  }}
                  renderItem={this.renderItem}
                  showsVerticalScrollIndicator={false}
              />
          </View>
      )
  }
  getDataMember() {
    let params = {
        role:2,
        page_index:this.state.page,
        page_size:10
    };
    GroupHandle.getInstance().getMemberOfGroup(
      this.props.groupId,
      params,
      (isSuccess, dataResponse) => {
        if (isSuccess && dataResponse.data !== undefined) {
            let listData = this.state.data;
            if(dataResponse.data.memberInfo.length<10){
                this.setState({canLoadData:true})
            }
            let abc = {
                data: dataResponse.data.memberInfo,
                type: AppConstants.LIST_MEMBER_TYPE.LIST_MEMBER,
            };
            listData = [...listData, ...[abc]];
            this.setState({
                data: listData,
                loading: false,
            });
        }
      },
    );
  }
  getDataAdmin() {
        let params = {
            role:0,
            page_index:this.state.page,
            page_size:10
        };
        GroupHandle.getInstance().getMemberOfGroup(
            this.props.groupId,
            params,
            (isSuccess, dataResponse) => {
                let newData = [];
                if (isSuccess && dataResponse.data !== undefined) {
                    let data=dataResponse.data;
                    this.setState({
                        loading: false,
                        listAdmin:data.memberInfo,
                    });
                }

            },
        );
    }
}
