import React, {Component} from 'react';
import {Alert, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {strings} from '../../../../../../resource/languages/i18n';
import Styles from '../../../../../../resource/Styles';
import ColorStyle from '../../../../../../resource/ColorStyle';
import {Icon} from 'react-native-elements';
import GroupStyle from '../../../GroupStyle';
import NavigationUtils from '../../../../../../Utils/NavigationUtils';
import GroupHandle from '../../../../../../sagas/GroupHandle';
import ViewUtils from '../../../../../../Utils/ViewUtils';
import {Actions} from 'react-native-router-flux';
export default class TabManage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.allGroup = null;
    this.myGroup = null;
  }

  render() {
    return (
      <View style={{marginHorizontal: Styles.constants.marginHorizontalAll}}>
        <View style={{marginTop: 20}}>
          <Text
            style={{
              ...Styles.text.text20,
              color: ColorStyle.loading,
              fontWeight: '700',
            }}>
            {strings('manage')}
          </Text>
          <TouchableOpacity
            style={GroupStyle.itemTabManage.button}
            onPress={() => {
              NavigationUtils.goToPostManager(this.props.groupId);
            }}>
            <View
              style={{
                flex: 3,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <Icon
                name={'post-outline'}
                type="material-community"
                size={25}
                color={ColorStyle.tabBlack}
              />
              <Text style={GroupStyle.itemTabManage.text} numberOfLines={1}>
                {strings('textPostBtn')}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  ...Styles.text.text11,
                  color: ColorStyle.tabWhite,
                  paddingVertical: 5,
                  backgroundColor: ColorStyle.tabActive,
                  paddingHorizontal: 17,
                  borderRadius: 5,
                  fontWeight: '700',
                }}>
                29
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={GroupStyle.itemTabManage.button}
            onPress={() => {
              NavigationUtils.goToMemberManager(this.props.groupId);
            }}>
            <View
              style={{
                flex: 3,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <Icon
                name={'shield'}
                type="feather"
                size={25}
                color={ColorStyle.tabBlack}
              />
              <Text style={GroupStyle.itemTabManage.text} numberOfLines={1}>
                {strings('textAddBtn')}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={{marginTop: 20}}>
          <Text
            style={{
              ...Styles.text.text20,
              color: ColorStyle.loading,
              fontWeight: '700',
            }}>
            {strings('setting')}
          </Text>
          <TouchableOpacity
            style={GroupStyle.itemTabManage.button}
            onPress={() =>
              NavigationUtils.goToInformationGroupManage(this.props.item)
            }>
            <View
              style={{
                flex: 3,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <Icon
                name={'information-circle-outline'}
                type="ionicon"
                size={25}
                color={ColorStyle.tabBlack}
              />
              <Text style={GroupStyle.itemTabManage.text} numberOfLines={1}>
                {strings('imGroupBtn')}
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={GroupStyle.itemTabManage.button}
            onPress={() =>
              Alert.alert(
                '',
                strings('deleteGroup'),
                [
                  {text: strings('cancel'), onPress: () => {}},
                  {
                    text: strings('delete'),
                    onPress: () => {
                      this.deleteGroup(this.props.groupId);
                    },
                  },
                ],
                {cancelable: false},
              )
            }>
            <View
              style={{
                flex: 3,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <Icon
                name={'delete-outline'}
                type="material-community"
                size={25}
                color={ColorStyle.tabBlack}
              />
              <Text style={GroupStyle.itemTabManage.text} numberOfLines={1}>
                {strings('deleteGroup')}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  deleteGroup(id, index) {
    GroupHandle.getInstance().deleteGroup([id], isSuccess => {
      if (isSuccess) {
        // this.allGroup.updateDeleteGroup(index);
        // this.myGroup.updateDeleteGroup(index)
        ViewUtils.showAlertDialog('xoas thanh cong');
        Actions.reset('groups');
      } else {
        ViewUtils.showAlertDialog(strings('deleteGroupError'));
      }
    });
  }
}
