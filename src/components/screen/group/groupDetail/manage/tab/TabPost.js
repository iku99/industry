import React, {Component} from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {strings} from '../../../../../../resource/languages/i18n';
import Styles from '../../../../../../resource/Styles';
import ColorStyle from '../../../../../../resource/ColorStyle';
import GroupPostHandle from '../../../../../../sagas/GroupPostHandle';
import DateUtil from '../../../../../../Utils/DateUtil';
import TimeUtils from '../../../../../../Utils/TimeUtils';
import {Icon} from 'react-native-elements';
import DataUtils from '../../../../../../Utils/DataUtils';
import constants from '../../../../../../Api/constants';
import GroupHandle from '../../../../../../sagas/GroupHandle';
import GlobalInfo from '../../../../../../Utils/Common/GlobalInfo';
import {EventRegister} from 'react-native-event-listeners';
import AppConstants from '../../../../../../resource/AppConstants';
import ViewUtils from '../../../../../../Utils/ViewUtils';
import SimpleToast from "react-native-simple-toast";
import MediaUtils from "../../../../../../Utils/MediaUtils";
import EmptyView from "../../../../../elements/reminder/EmptyView";
let LIMIT = 20;
let shouldRefresh = false;
export default class TabPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: false,
      page: 1,
      canLoadData: true,
      item: [],
      avatar: undefined,
      userInfo: {},
        idPost:undefined
    };
  }
  async componentDidMount() {
    this.getData();
    // let userInfo = await GlobalInfo.getUserInfo();
    // EventRegister.addEventListener(
    //   AppConstants.EventName.CHANGE_AVATAR,
    //     userInfo => {
    //       this.setState({userInfo});
    //   },
    // );
    // this.setState({userInfo});
  }
  render() {
 if(this.state.data.length===0){
     return(
         <EmptyView text={strings('notReq')}/>
     )
 }else
      return (
          <FlatList
              data={this.state.data}
              style={{
                  flex: 1,
                  height:'100%',
                  marginTop: 12,
                  paddingVertical: 28,
                  backgroundColor: ColorStyle.tabWhite,
                  borderColor: ColorStyle.borderItemHome,
                  elevation: 3,
              }}
              renderItem={this.renderItem}
          />
      );
  }
  renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        style={{
          paddingBottom: 16,
          paddingHorizontal:12,
          paddingTop: 8,
          borderBottomWidth: 1,
          borderBottomColor: ColorStyle.borderItemHome,
        }}>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 1}}>
            <Image
              source={MediaUtils.getAvatar(item.customer)}
              style={{...Styles.icon.iconShow, borderRadius: 50}}
            />
          </View>
          <View style={{flex: 4, marginLeft: 12, justifyContent: 'flex-start'}}>
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabBlack,
                fontWeight: '700',
              }}>
              {item.customer.full_name}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 4,
              }}>
              <Icon name={'clock'} type={'evilicon'} size={15} />
              <Text
                style={{
                  ...Styles.text.text10,
                  color: ColorStyle.gray,
                  alignItems: 'center',
                }}>
                {DateUtil.formatDate('HH:mm DD/MM/YYYY', item.created_at)}
              </Text>
            </View>
            <Text
              style={{...Styles.text.text14, marginTop: 8}}
              numberOfLines={4}>
              {item.content}
            </Text>
          </View>
        </View>

        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <TouchableOpacity
              onPress={()=>{this.updateStatusPost(item.id)}}
            style={{
              width: '45%',
              backgroundColor: ColorStyle.tabActive,
              alignItems: 'center',
              justifyContent: 'center',
              paddingVertical: 7,
              borderRadius: 5,
              marginVertical: 12,
            }}>
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabWhite,
                fontWeight: '700',
              }}>
              {strings('approve')}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              ViewUtils.showAskAlertDialog(
                strings('areYouSureDeletePost'),
                () => {
                  this.deleteGroupPost(item.id);
                },
                null,
              );
            }}
            style={{
              width: '45%',
              backgroundColor: 'transform',
              borderColor: 'rgba(0, 0, 0, 0.1)',
              borderWidth: 1,
              alignItems: 'center',
              justifyContent: 'center',
              paddingVertical: 7,
              borderRadius: 5,
              marginVertical: 12,
            }}>
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabBlack,
                fontWeight: '700',
              }}>
              {strings('refuse')}
            </Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  };
  getData() {
    let param = {
        status:0,
        page_index:this.state.page,
        page_size:LIMIT
    };
    GroupPostHandle.getInstance().getPostsOfGroup(
      this.props.groupId,
      param,
      (isSuccess, dataResponse) => {
        let data = [];

        if (this.state.page !== 1) {
          data = this.state.data;
        }
        let canLoadData = false;

        if (
          isSuccess &&
          dataResponse.data !== undefined
        ) {
          let newData = dataResponse.data;
          canLoadData = newData.length === LIMIT;
          data = data.concat(newData);
        }
        this.setState({loading: false, data, canLoadData});
      },
    );
  }
  getImageUrl(item, index) {
    let url = item.avatar;
    return DataUtils.stringNullOrEmpty(url)
      ? 'https://picsum.photos/50/50?avatar' + this.props.index
      : constants.host + url;
  }
  updateStatusPost(id){
      let params={
          status:1,
          team_id:this.props.groupId
      }
      GroupPostHandle.getInstance().updateStatusPost(id,params,(isSuccess, dataResponse)=>{
          if (isSuccess) {
              SimpleToast.show(strings('updateSuccess'),SimpleToast.SHORT)
              this.setState({data:[]},()=>{
                  this.getData();
              })
              return;
          }
          SimpleToast.show(strings('updateFailed'),SimpleToast.SHORT)
      })
  }
  deleteGroupPost(postId) {
    GroupPostHandle.getInstance().deletePost(
      postId,
      (isSuccess, dataResponse) => {
          console.log(dataResponse)
        if (isSuccess) {
          shouldRefresh = true;
          let data = this.state.data;
          // for (let index = 0; index < size; index++) {
          //   let item = data[index];
          //   if (postId === item.id) {
          //     data = DataUtils.removeItem(data, index);
          //     this.setState({data: []}, () => this.setState({data}));
          //
          //   }
          // }
            this.setState({data:[]},()=>{
                this.getData();
            })
            return;
        } else {
          ViewUtils.showAlertDialog(strings('cannotDeletePost'), null);
        }
      },
    );
  }
}
