import React, {Component} from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import GroupStyle from '../../../GroupStyle';
import Styles from '../../../../../../resource/Styles';
import ColorStyle from '../../../../../../resource/ColorStyle';
import {strings} from '../../../../../../resource/languages/i18n';
import MediaUtils from '../../../../../../Utils/MediaUtils';
import GroupPostHandle from '../../../../../../sagas/GroupPostHandle';
import AppConstants from '../../../../../../resource/AppConstants';
import {DotIndicator} from 'react-native-indicators';
import EmptyView from '../../../../../elements/reminder/EmptyView';
import GroupPostRow from '../../row/GroupPostRow';
import NavigationUtils from '../../../../../../Utils/NavigationUtils';
import { Actions } from "react-native-router-flux";
let currentPosition = 0;
let LIMIT = 5;
let addedSuggestionGroup = false;
let groupPosts = [];
let groupPostRef = [];
export default class TabComment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: [],
      page: 0,
      refreshing: false,
      showFloatingButton: false,
      canLoadData: true,
      finishLoad: false,
    };
  }

  componentDidMount() {
      console.log('hieu')
    // this.getGroupPostData();
  }

  render() {
    let item = this.props.item;
      console.log('data:',this.state.data)
    return (
      <View style={{flex:1,backgroundColor:'red'}}>
        <View style={{...GroupStyle.groupPost}}>
          <TouchableOpacity style={GroupStyle.itemOption}>
            <Image
              source={{uri: this.getAvatarUrl(item)}}
              style={{...Styles.icon.iconShow, borderRadius: 50}}
            />
          </TouchableOpacity>
          <TouchableOpacity style={GroupStyle.itemComment} onPress={()=>Actions.jump('addPostToGroup',{item:item})}>
            <View style={GroupStyle.viewComment}>
              <Text
                style={{
                  ...Styles.text.text14,
                  color: ColorStyle.gray,
                  marginLeft: 10,
                }}>
                {strings('textComment')}
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <View
          style={{
            marginTop: 12,
            backgroundColor: ColorStyle.tabWhite,
            borderColor: ColorStyle.borderItemHome,
            elevation: 3,
            width: '100%',
            height: '100%',
            justifyContent: 'center',
          }}>
          {this.state.data.length === 0 ? (
            <EmptyView text={strings('groupEmptyPost')} />
          ) : (
            <FlatList
              contentContainerStyle={{paddingBottom: 170, width: '100%'}}
              showsVerticalScrollIndicator={false}
              data={this.state.data}
              keyExtractor={this.keyExtractor}
              renderItem={this._renderItem}
              style={{
                width: '100%',
                height: '95%',
                backgroundColor: 'transform',
              }}
              ListFooterComponent={this.renderFooter.bind(this)}
              onEndReachedThreshold={0.4}
              onEndReached={() => this.handleLoadMore()}
              ref={ref => {
                this.listRef = ref;
              }}
              refreshing={this.state.refreshing}
            />
          )}
        </View>
      </View>
    );
  }
  _renderItem = ({item, index}) => {
    return (
      <GroupPostRow
        index={index}
        item={item}
        groupId={this.props.item.id}
        groupName={this.props.item.name}
        ref={ref => (groupPostRef[index] = ref)}
        onChangeItemData={(item, index) => {
          this.changeItem(item, index, false);
        }}
        onGoToPostDetail={() => {
          NavigationUtils.goToPostDetail(
            groupPosts[index],
            this.props.item.id,
            this.props.item.name,
            index,
            (item, index) => {
              groupPostRef[index].changeDataItem(item);
              this.changeItem(item, index, true);
            },
          );
        }}
      />
    );
  };
  changeItem(item, index, forceRefresh) {
    groupPosts[index] = item;
  }
  getAvatarUrl(item) {
    return MediaUtils.getGroupAvatar(item, 0);
  }
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) {
      return null;
    }
    return <DotIndicator color={ColorStyle.tabWhite} size={10} />;
  };
  handleLoadMore() {
    if (this.state.loading) {
      return;
    }
    if (this.state.canLoadData) {
      this.setState({loading: true, page: this.state.page + 1}, () =>
        this.getGroupPostData(),
      );
    }
  }
  addSuggestionGroup() {
    addedSuggestionGroup = true;
    this.setState({finishLoad: true});
  }
  getGroupPostData() {
    let param = {
      current_page: this.state.page,
      page_size: LIMIT,
    };
    GroupPostHandle.getInstance().getPostsOfGroup(
      this.props.item.id,
      param,
      (isSuccess, dataResponse) => {
        let canLoadData = false;

        if (
          isSuccess &&
          dataResponse.data !== undefined &&
          dataResponse.data.list !== undefined
        ) {
          let newData = [];
          let data=this.state.data;
          dataResponse.data.list.forEach((item, index) => {
            newData.push({
              ...item,
              isLoadDetail: false,
              isLoadComment: false,
              isLoadLike: false,
              isLoadProducts: false,
              isLoadOwnerUser: false,
            });
          });

          canLoadData = newData.length === LIMIT;
          groupPosts = data.concat(newData);
        }
        this.setState({data: groupPosts});

        this.setState({loading: false, canLoadData}, () => {
          if (this.state.page === 0 && !canLoadData) {
            this.addSuggestionGroup();
          }
        });
      },
    );
  }
}
