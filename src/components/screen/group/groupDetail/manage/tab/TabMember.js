import React, {Component} from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {strings} from '../../../../../../resource/languages/i18n';
import ColorStyle from '../../../../../../resource/ColorStyle';
import Styles from '../../../../../../resource/Styles';
import {Icon} from 'react-native-elements';
import DataUtils from '../../../../../../Utils/DataUtils';
import constants from '../../../../../../Api/constants';
import GroupHandle from '../../../../../../sagas/GroupHandle';
import DateUtil from "../../../../../../Utils/DateUtil";
import SimpleToast from "react-native-simple-toast";
import AppConstants from "../../../../../../resource/AppConstants";
import EmptyView from "../../../../../elements/reminder/EmptyView";
const LIMIT = 20;
export default class TabMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: false,
      page: 1,
      canLoadData: true,
      showRole: false,
      committingData: false,
      roleList: [],
      selectedRole: undefined,

    };
  }
  componentDidMount() {
    this.getData();
  }

  render() {
      if(this.state.data.length===0){
          return(
              <EmptyView text={strings('notReq')}/>
          )
      }else
    return (
      <View style={{flex: 1, marginVertical: 12}}>
        {/*<View*/}
        {/*  style={{*/}
        {/*    flexDirection: 'tab',*/}
        {/*    alignItems: 'center',*/}
        {/*    justifyContent: 'space-between',*/}
        {/*    paddingHorizontal: 40,*/}
        {/*    paddingVertical: 28,*/}
        {/*    backgroundColor: ColorStyle.tabWhite,*/}
        {/*    borderColor: ColorStyle.borderItemHome,*/}
        {/*    elevation: 2,*/}
        {/*  }}>*/}
        {/*  <TextInput*/}
        {/*    style={[*/}
        {/*      Styles.input.inputLogin,*/}
        {/*      {*/}
        {/*        width: '80%',*/}
        {/*      },*/}
        {/*    ]}*/}
        {/*    placeholder={strings('textMember')}*/}
        {/*    value={this.state.name}*/}
        {/*    placeholderTextColor={ColorStyle.textInput}*/}
        {/*    onChangeText={value => {*/}
        {/*      this.queryCategory(value);*/}
        {/*    }}*/}
        {/*  />*/}
        {/*  <TouchableOpacity>*/}
        {/*    <Icon name={'search1'} type={'antdesign'} size={25} />*/}
        {/*  </TouchableOpacity>*/}
        {/*</View>*/}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            height:'100%',
            marginTop: 12,
            paddingVertical: 28,
            backgroundColor: ColorStyle.tabWhite,
            borderColor: ColorStyle.borderItemHome,
            elevation: 2,
          }}>
          <FlatList
            data={this.state.data}
            renderItem={this.renderItem}
            style={{width: '100%', height: '100%'}}
            showsVerticalScrollIndicator={false}
          />
        </View>
      </View>
    );
  }
  renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={{
          borderBottomWidth: 1,
          borderBottomColor: ColorStyle.borderItemHome,
          alignItems: 'center',
          paddingHorizontal: 30,
          marginVertical: 12,
          paddingVertical: 5,
        }}>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 1}}>
            <Image
              source={{uri: this.getImageUrl(item.customers)}}
              style={{...Styles.icon.iconShow, borderRadius: 50}}
            />
          </View>
          <View style={{flex: 4, marginLeft: 12}}>
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabBlack,
                fontWeight: '700',
              }}>
              {item.customers.full_name}
            </Text>
            <Text style={{...Styles.text.text14, color: ColorStyle.gray,marginTop:10}}>
              {strings('joinFrom')}
              {DateUtil.formatDate("DD/MM/YYYY",item.join_at)}
            </Text>
          </View>
          <Icon
            name={'user-cog'}
            type="font-awesome-5"
            style={{display: item.owner_group ? 'flex' : 'none'}}
          />
        </View>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <TouchableOpacity
              onPress={()=>this.approveUser(item)}
            style={{
              width: '45%',
              backgroundColor: ColorStyle.tabActive,
              alignItems: 'center',
              justifyContent: 'center',
              paddingVertical: 7,
              borderRadius: 5,
              marginVertical: 12,
            }}>
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabWhite,
                fontWeight: '700',
              }}>
              {strings('approve')}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
              onPress={()=>this.refuseUser(item)}
            style={{
              width: '45%',
              backgroundColor: 'transform',
              borderColor: 'rgba(0, 0, 0, 0.1)',
              borderWidth: 1,
              alignItems: 'center',
              justifyContent: 'center',
              paddingVertical: 7,
              borderRadius: 5,
              marginVertical: 12,
            }}>
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabBlack,
                fontWeight: '700',
              }}>
              {strings('refuse')}
            </Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  };
    refuseUser(item){
        let params={
            team_id:this.props.groupId
        }

        GroupHandle.getInstance().deleteMemberOfGroup(item.id,params,(isSuccess, dataResponse)=>{
            if(isSuccess){
                this.setState({data:[]},()=>{
                    this.getData();
                })

            }
        })
    }
    approveUser(item){
        let params={
            new_role:2,
            team_id:this.props.groupId
        }
        GroupHandle.getInstance().updateMemberGroupStatus(item.id,params,(isSuccess, dataResponse)=>{
            if(isSuccess){
                SimpleToast.show(strings('updateSuccess'),SimpleToast.SHORT)
                this.setState({data:[]},()=>{
                    this.getData();
                })
                return
            }
            SimpleToast.show(strings('updateFailed'),SimpleToast.SHORT)
        })
    }
  getImageUrl(item, index) {
    let url = item?.avatar;
    return DataUtils.stringNullOrEmpty(url)
      ? 'https://picsum.photos/50/50?avatar' + this.props.index
      : constants.host + url;
  }
  getData() {
    let params = {
        role:AppConstants.ROLE.NOT_MEMBER,
        page_index:this.state.page,
        page_size:10
    };
    GroupHandle.getInstance().getMemberOfGroup(
      this.props.groupId,
      params,
      (isSuccess, dataResponse) => {
        let data = this.state.page === 0 ? [] : this.state.data;
        let newData = [];
        if (isSuccess && dataResponse.data !== undefined) {
          newData = dataResponse.data.memberInfo;
          data = data.concat(newData);
        }
        this.setState({
          loading: false,
          canLoadData: newData.length === LIMIT,
          data,
        });
      },
    );
  }
}
