import React,{Component} from "react";
import { FlatList, Image, ScrollView, Text, TextInput, TouchableOpacity, View } from "react-native";
import ColorStyle from "../../../../../../resource/ColorStyle";
import Styles from "../../../../../../resource/Styles";
import { strings } from "../../../../../../resource/languages/i18n";
import { Icon } from "react-native-elements";
import DataUtils from "../../../../../../Utils/DataUtils";
import constants from "../../../../../../Api/constants";
import DateUtil from "../../../../../../Utils/DateUtil";
const Data = [
  {
    id: 294,
    full_name: 'Eplaza',
    approved_status: 0,
    user_id: 259,
    group_id: 70,
    tree_path: '143',
    join_at: '2021-11-13T03:25:34.000+00:00',
    role: {
      id: 140,
      name: 'Thành viên',
      description: 'Thành viên mặc đinh khi tham gia nhóm',
    },
    valid: false,
    self_tree_path: '143:294',
    owner_group: false,
  },
  {
    id: 265,
    full_name: 'Vũ Tiến Chiến',
    avatar: '/upload/user/ecd9ef21-916d-4f6c-b57a-78ad466aac8d.jpg',
    approved_status: 20,
    user_id: 269,
    group_id: 70,
    tree_path: '143',
    join_at: '2021-09-06T14:09:06.000+00:00',
    role: {
      id: 140,
      name: 'Thành viên',
      description: 'Thành viên mặc đinh khi tham gia nhóm',
    },
    valid: false,
    self_tree_path: '143:265',
    owner_group: false,
  },
  {
    id: 235,
    full_name: 'Nguyễn Đức Chính',
    approved_status: 0,
    user_id: 242,
    group_id: 70,
    tree_path: '143',
    join_at: '2021-04-03T09:02:02.000+00:00',
    role: {
      id: 140,
      name: 'Thành viên',
      description: 'Thành viên mặc đinh khi tham gia nhóm',
    },
    valid: false,
    self_tree_path: '143:235',
    owner_group: false,
  },
  {
    id: 227,
    full_name: 'Nguyen Van Thang',
    avatar:
      '[LOCAL]file:///storage/emulated/0/Pictures/14da985e-25d7-486f-b305-9bdd32279427.jpg',
    approved_status: 0,
    user_id: 48,
    group_id: 70,
    tree_path: '143',
    join_at: '2021-02-22T10:23:18.000+00:00',
    role: {
      id: 140,
      name: 'Thành viên',
      description: 'Thành viên mặc đinh khi tham gia nhóm',
    },
    valid: false,
    self_tree_path: '143:227',
    owner_group: false,
  },
  {
    id: 186,
    full_name: 'Sàn TMDT Eplaza',
    approved_status: 20,
    user_id: 111,
    group_id: 70,
    tree_path: '143',
    join_at: '2020-10-02T09:33:02.000+00:00',
    role: {
      id: 140,
      name: 'Thành viên',
      description: 'Thành viên mặc đinh khi tham gia nhóm',
    },
    valid: false,
    self_tree_path: '143:186',
    owner_group: false,
  },
  {
    id: 169,
    full_name: 'Trần Anh Huy',
    avatar: '/upload/user/7cae154f-50f5-47e5-bf99-ff05960d7386.jpg',
    approved_status: 20,
    user_id: 8,
    group_id: 70,
    tree_path: '143',
    join_at: '2020-09-14T16:11:37.000+00:00',
    role: {
      id: 142,
      name: 'Shipper',
      description: 'Người vận chuyển hàng',
    },
    valid: false,
    self_tree_path: '143:169',
    owner_group: false,
  },
  {
    id: 158,
    full_name: 'L.H',
    avatar: '/upload/user/b3a7f269-5644-4905-9a79-72dec9ed73df.jpg',
    approved_status: 20,
    user_id: 100,
    group_id: 70,
    tree_path: '143',
    join_at: '2020-09-10T10:18:49.000+00:00',
    role: {
      id: 140,
      name: 'Thành viên',
      description: 'Thành viên mặc đinh khi tham gia nhóm',
    },
    valid: false,
    self_tree_path: '143:158',
    owner_group: false,
  },
  {
    id: 148,
    full_name: 'muahangeplaza',
    approved_status: 20,
    user_id: 88,
    group_id: 70,
    tree_path: '143',
    join_at: '2020-09-07T04:11:51.000+00:00',
    role: {
      id: 140,
      name: 'Thành viên',
      description: 'Thành viên mặc đinh khi tham gia nhóm',
    },
    valid: false,
    self_tree_path: '143:148',
    owner_group: false,
  },
  {
    id: 143,
    full_name: 'EPLAZA MALL',
    avatar: '/upload/user/e3127b0f-1367-46d1-b434-d19ef3c8a177.jpg',
    approved_status: 20,
    user_id: 4,
    group_id: 70,
    tree_path: '',
    join_at: '2020-09-07T04:03:46.000+00:00',
    role: {
      id: 140,
      name: 'Thành viên',
      description: 'Thành viên mặc đinh khi tham gia nhóm',
    },
    valid: false,
    self_tree_path: '143',
    owner_group: false,
  },
];
export default class TabInvite extends Component{
  constructor(props) {
    super(props);
    this.state={
      data:Data,
      checkInvited:false
    }
  }

  render() {

    return (
      <View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingHorizontal: 30,
            paddingVertical: 28,
            backgroundColor: ColorStyle.tabWhite,
            borderColor: ColorStyle.borderItemHome,
            elevation: 2,
          }}>
          <TextInput
            style={[
              Styles.input.inputLogin,
              {
                width: '90%',
                borderColor: ColorStyle.gray,
                borderWidth: 1,
                backgroundColor: 'transparent',
              },
            ]}
            placeholder={strings('textMember')}
            value={this.state.name}
            placeholderTextColor={ColorStyle.textInput}
            onChangeText={value => {
              this.queryCategory(value);
            }}
          />
          <TouchableOpacity>
            <Icon name={'search1'} type={'antdesign'} size={25} />
          </TouchableOpacity>
        </View>
        <Text style={{...Styles.text.text14,color:ColorStyle.loading,fontWeight:'700',marginHorizontal:Styles.constants.marginHorizontalAll,marginVertical:16}}>
          {strings('sugMember')}
        </Text>
          <FlatList
            data={this.state.data}
            renderItem={this.renderItem}
            style={{backgroundColor:ColorStyle.tabWhite,borderColor:ColorStyle.borderItemHome,elevation:2}}
            showsVerticalScrollIndicator={false}
          />

      </View>
    );
  }
  renderItem=({item,index})=>{
    return(
      <TouchableOpacity
        onPress={() => this.setState({showSelectProduct: true, dataItem: item})}
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingVertical: 5,
          paddingHorizontal: 24,
          borderColor: ColorStyle.borderItemHome,
          borderWidth: 1,
        }}>
        <View style={{flex: 1}}>
          <Image
            source={{uri: this.getImageUrl(item,index)}}
            style={{...Styles.icon.iconShow, borderRadius: 50}}
          />
        </View>
        <View style={{flex: 2}}>
          <Text
            style={{
              ...Styles.text.text14,
              color: ColorStyle.tabBlack,
              fontWeight: '700',
            }}>
            {item.full_name}
          </Text>
        </View>
        <TouchableOpacity onPress={()=>this.setState({checkInvited:!this.state.checkInvited})} style={{flex: 1,flexDirection:'row',alignItems:'center',borderWidth:1,borderColor:ColorStyle.borderItemHome,justifyContent:'center',paddingVertical:4,borderRadius:5}}>
          <Icon name={this.state.checkInvited?'check':'add'} type={'materialicons'} size={15} color={'#BABABA'}/>
          <Text
            style={{
              ...Styles.text.text10,
              color: '#BABABA',
              fontWeight: '700',
            }}>
            {strings('invited')}
          </Text>
        </TouchableOpacity>
      </TouchableOpacity>
    )
  }
  getImageUrl(item, index) {
    let url = item.avatar;
    return DataUtils.stringNullOrEmpty(url)
      ? 'https://picsum.photos/50/50?avatar' + index
      : constants.host + url;
  }
}
