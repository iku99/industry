import React, {Component} from 'react';
import {
    ActivityIndicator,
    Alert,
    FlatList,
    Image,
    ScrollView,
    Text, TextInput,
    TouchableOpacity,
    View,
} from 'react-native';

import Styles from '../../../../resource/Styles';
import {strings} from '../../../../resource/languages/i18n';
import {Icon} from 'react-native-elements';
import AppConstants from '../../../../resource/AppConstants';
import TimeUtils from '../../../../Utils/TimeUtils';
import GroupHandle from '../../../../sagas/GroupHandle';
import DataUtils from '../../../../Utils/DataUtils';
import constants from '../../../../Api/constants';
import {Actions} from 'react-native-router-flux';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import Toolbar from '../../../elements/toolbar/Toolbar';
import ColorStyle from '../../../../resource/ColorStyle';
import GroupStyle from '../GroupStyle';
import GroupUtils from '../../../../Utils/GroupUtils';
import FuncUtils from '../../../../Utils/FuncUtils';
import ViewUtils from '../../../../Utils/ViewUtils';
import BannerRow from './row/BannerRow';
import {SceneMap, TabView} from 'react-native-tab-view';
import TabComment from './manage/tab/TabComment';
import TabInvite from './manage/tab/TabInvite';
import TabManage from './manage/tab/TabManage';
import InformationGroupOther from "./InformationGroup";
import HeaderGroupAnimation from "../../../elements/animation/headerGroup/HeaderGroupAnimation";
import MediaUtils from "../../../../Utils/MediaUtils";
import GroupPostHandle from "../../../../sagas/GroupPostHandle";
import EmptyView from "../../../elements/reminder/EmptyView";
import GroupPostRow from "./row/GroupPostRow";
import AccountHandle from "../../../../sagas/AccountHandle";
import SimpleToast from "react-native-simple-toast";
import GlobalInfo from "../../../../Utils/Common/GlobalInfo";
import {EventRegister} from "react-native-event-listeners";
import TabHomeGroup from "./manage/ManageGroup/tab/TabHomeGroup";
import TabSearchUserName from "./manage/ManageGroup/tab/TabSearchUserName";
import TabMoreGroup from "./manage/ManageGroup/tab/TabMoreGroup";
let LIMIT = 15;
let addedSuggestionGroup = false;
let groupPosts = [];
let groupPostRef = [];
const Data=[
  {
    type:AppConstants.POST_GROUPS.BUTTON_ADD
  }
]
export default class GroupDetailMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      page: 0,
        userInfo:undefined,
      refreshing: false,
      showFloatingButton: false,
      canLoadData: true,
      finishLoad: false,
      data:Data,
      members: [],
      index: 0,
        admins: [],
        countMember:0,
        countAdmin:0,
      routes: [
        {
          key: 'tabComment',
          title: strings('group'),
          icon: 'user-friends',
          type: 'font-awesome-5',
        },
        {
          key: 'tabInvite',
          title: strings('invite'),
          icon: 'user-plus',
          type: 'font-awesome-5',
        },
        {
          key: 'tabMore',
          title: strings('more'),
          icon: 'menu',
          type: 'feather',
        },
      ],
    };
  }

    async componentDidMount() {
        let userInfo = await GlobalInfo.getUserInfo();
        EventRegister.addEventListener(
            AppConstants.EventName.CHANGE_AVATAR,
            data => {
                this.setState({data:{}},()=>{this.setState({userInfo:data})});
            },
        );
        this.setState({userInfo});
        this.getMembers(AppConstants.ROLE.MEMBER,'members','countMember')
        this.getMembers(AppConstants.ROLE.ADMIN,'admins','countAdmin')
        this.onPressTab(0)
  }
    getMembers(type,state,count) {
        let param = {
            role:type,
            page_index:1,
            page_size:4
        };
        GroupHandle.getInstance().getMemberOfGroup(
            this.props.item.id,
            param,
            (isSuccess, dataResponse) => {
                if (isSuccess && dataResponse.data.memberInfo !== undefined) {
                    this.setState({[state]: dataResponse.data.memberInfo,[count]:dataResponse.data.count});
                }
            },
        );
    }
    render() {
        return(
            <View style={Styles.container}>
                <HeaderGroupAnimation
                    style={{flex: 1}}
                    bodyText="Back"
                    backTextStyle={{fontSize: 14, color: '#000'}}
                    titleStyle={{fontSize: 22, left: 20, bottom: 20, color: '#000'}}
                    headerMaxHeight={Styles.constants.X * 8.5}
                    toolbarColor={ColorStyle.tabActive}
                    disabled={false}
                    renderLeft={() => (
                        <TouchableOpacity style={{marginLeft:Styles.constants.marginHorizontal20}} onPress={() => Actions.pop()}>
                            <Icon
                                name="left"
                                type="antdesign"
                                size={25}
                                color={ColorStyle.tabWhite}
                            />
                        </TouchableOpacity>
                    )}
                    renderRight={() => (
                        <TouchableOpacity onPress={() => this.outGroup()}>
                            <Icon
                                name="logout"
                                type="antdesign"
                                size={25}
                                color={ColorStyle.tabWhite}
                                style={{marginRight: Styles.constants.marginHorizontal20}}
                            />
                        </TouchableOpacity>
                    )}
                    renderBackground={{
                        uri: this.getGroupImageUrl(this.props.item),
                        headers: {Authorization: 'someAuthToken'},
                    }}
                    renderInfo={() => (
                        <View style={{width:Styles.constants.widthScreenMg24}}>
                            <Text
                                style={{
                                    ...Styles.text.text24,
                                    color: ColorStyle.tabWhite,
                                    fontWeight: '700',
                                }}>
                                {this.props.item.name}
                            </Text>

                            <Text
                                style={{
                                    ...Styles.text.text14,
                                    color: ColorStyle.tabWhite,
                                    fontWeight: '700',
                                }}>
                                {this.checkPrivacy()}
                            </Text>
                        </View>
                    )}
                    renderBody={() =>
                        <View>
                            <Image
                                style={{...GroupStyle.bannerGroupDetail}}
                                source={{
                                    uri: MediaUtils.getGroupCover(this.props.item, 0),
                                    headers: {Authorization: 'someAuthToken'},
                                }}
                                resizeMode={'cover'}
                            />
                            <View style={{...GroupStyle.bannerGroupDetail,position:'absolute',backgroundColor:'rgba(0, 0, 0, 0.7)'}}/>

                        </View>
                    }
                    tabView={()=>
                        <View style={{ flexDirection: 'row',elevation: 1, height:'100%',backgroundColor:ColorStyle.tabWhite,  alignSelf:'center', borderBottomColor: ColorStyle.borderItemHome,}}>
                            {this.state.routes.map((route, i) => {
                                return (
                                    <TouchableOpacity
                                        style={{
                                            flex: 1,
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            borderBottomColor:i === this.state.index ? ColorStyle.tabActive : ColorStyle.tabBlack,
                                            borderBottomWidth:i === this.state.index ?1:0
                                        }}
                                        onPress={() => this.onPressTab(i)}>
                                        <Icon
                                            name={route.icon}
                                            type={route.type}
                                            size={20}
                                            color={i === this.state.index ? ColorStyle.tabActive : ColorStyle.tabBlack}
                                        />
                                        <Text
                                            style={{
                                                ...Styles.text.text14,
                                                color:
                                                    i === this.state.index ? ColorStyle.tabActive : ColorStyle.tabBlack,
                                                fontWeight: i === this.state.index ? '700' : '400',
                                            }}>
                                            {route.title}
                                        </Text>
                                    </TouchableOpacity>
                                );
                            })}
                        </View>
                    }
                >
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.state.data}
                        keyExtractor={this.keyExtractor}
                        renderItem={this.renderItem}
                        style={{flex:1, height:'100%',backgroundColor:'transparent'}}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        onEndReachedThreshold={0.4}
                        onEndReached={() => this.handleLoadMore()}
                        removeClippedSubviews={true} // Unmount components when outside of window
                        windowSize={10}
                    />
                </HeaderGroupAnimation>
            </View>
        )
    }
    handleLoadMore() {
        if (this.state.loading) return ;
        if (!this.state.canLoadData) return;
        this.setState({loading: true, page: this.state.page + 1}, this.getGroupPostData);
    }
    renderFooter = () => {
        //it will show indicator at the bottom of the list when data is loading otherwise it returns null
        if (!this.state.loading) return null;
        return (
            <ActivityIndicator
                style={{ color: '#000' }}
            />
        );
    };
    keyExtractor = (item, index) => `list_post ${index.toString()}`;
    renderItem=({item,index})=>{
    switch (this.state.index) {
      case 0:  return (
          <TabHomeGroup group={item}
                        userInfo={this.state.userInfo}
                        onChangeItemData={(elm,num)=>{
                            this.changeItem(elm, num, true);
                        }}
                        goToDetail={(post,num)=>{
                            NavigationUtils.goToPostDetail(
                                post,
                                this.props.item.id,
                                this.props.item.name,
                                num,
                                (item, num) => {
                                    groupPostRef[num].changeDataItem(item);
                                    this.changeItem(item, num, true);
                                },
                            );
                        }
                        }/>
      )
        ;break
      case 1:
           return (
               <TabSearchUserName group={this.props.item}
                                  items={item}
                                  onSend={(text)=>{
                                  }} onSearch={(text)=>{
                   this.queryMember(text)
               }}/>
           )
            ;break
      case 2:return(
          <TabMoreGroup item={this.props.item} onOutGroup={()=>this.outGroup()}
                        countMember={this.state.countMember}
                        admins={this.state.admins}
                        />
      );break
    }
  }
    _renderItem=({item,index})=>{
        return(
            <GroupPostRow
                index={index}
                item={item}
                groupId={this.props.item.id}
                groupName={this.props.item.name}
                ref={ref => (groupPostRef[index] = ref)}
                onChangeItemData={(item, index) => {
                    this.changeItem(item, index, false);
                }}
                onGoToPostDetail={() => {
                    NavigationUtils.goToPostDetail(
                        item,
                        this.props.item.id,
                        this.props.item.name,
                        index,
                        (item, index) => {
                            groupPostRef[index].changeDataItem(item);
                            this.changeItem(item, index, true);
                        },
                    );
                }}
            />
        )
    }
    onPressTab(i){
        switch (i) {
            case 0:return this.setState({index:i,data:[]},()=>this.getGroupPostData())
            case 1:return this.setState({index:i,data:[]},()=>this.getMember())
            case 2:return this.setState({index:i,data:[{}]})

        }

    }

    getAvatarUrl(item) {
        let url = item.avatar;

        return DataUtils.stringNullOrEmpty(url) ? '' : constants.host + url;
    }

    outGroup(){
ViewUtils.showAskAlertDialog(strings('outGroup').replace('{name}',this.props.item.name),()=>{

},()=>{})
    }
    queryMember(){
        let param={
            user_name:this.state.name,
            page_index:1,
            page_size: 20
        }
        AccountHandle.getInstance().searchUser(param,(isSuccess,res)=>{
            if(isSuccess){
                let listData=this.state.data;
                let abc = {
                    data: res.data,
                    type:AppConstants.POST_GROUPS.LIST_ITEM,
                };
                listData = [...listData, ...[abc]];
                this.setState({data:listData})
            }
        })
    }
    deleteGroup(id, index) {
        GroupHandle.getInstance().deleteGroup(id, isSuccess => {
            if (isSuccess) {
                SimpleToast.show(strings('deleteGroupSuccess',SimpleToast.SHORT))
                this.props.callback()
                Actions.pop()
            } else {
                ViewUtils.showAlertDialog(strings('deleteGroupFa'));
            }
        });
    }
    changeItem(item, index, forceRefresh) {
        let listData=this.state.data
        listData[1].data[index]=item;
        this.setState({data:listData})
    }
    getGroupImageUrl(item) {
        return MediaUtils.getGroupCover(item, 0);
    }
    getImageUrl(item, index) {
        let url = item.avatar;
        return DataUtils.stringNullOrEmpty(url)
            ? 'https://picsum.photos/50/50?avatar' + this.props.index
            : constants.host + url;
    }
    addSuggestionGroup() {
        addedSuggestionGroup = true;
        this.setState({finishLoad: true});
    }
    checkPrivacy() {
        let privacy = this.props.item.status;
        if (privacy === AppConstants.GROUP_PRIVACY.PRIVATE) {
            return strings('privateGroup');
        } else {
            return strings('publicGroup');
        }
    }

    getGroupPostData() {
        this.setState({data:Data})
        let param = {
            status:AppConstants.Status_Post.ALL,
            page_index: this.state.page,
            page_size: LIMIT,
        };
        GroupPostHandle.getInstance().getPostsOfGroup(
            this.props.item.id,
            param,
            (isSuccess, dataResponse) => {
                let canLoadData = false;
                if (isSuccess && dataResponse.data !== undefined) {
                    let data = this.state.data;
                    let data1={
                        type:AppConstants.POST_GROUPS.LIST_ITEM,
                        data: dataResponse.data
                    }
                    groupPosts = data.concat(data1);
                }
                this.setState({data: groupPosts});
                this.setState({loading: false, canLoadData}, () => {
                    if (this.state.page === 0 && !canLoadData) {
                        this.addSuggestionGroup();
                    }
                });
            },
        );
    }
    getMember() {
        this.setState({data:Data})
        //         let newData = [];
        //         let data = this.state.data;
        //             let data1={
        //                 type:AppConstants.POST_GROUPS.LIST_ITEM,
        //                 data:[]
        //             }
        //             data.push(data1)
        //             this.setState({data:data})
    }
}
