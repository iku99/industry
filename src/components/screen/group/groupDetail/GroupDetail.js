import React, {Component} from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../../resource/Styles';
import {strings} from '../../../../resource/languages/i18n';
import AppConstants from '../../../../resource/AppConstants';
import GroupPostHandle from '../../../../sagas/GroupPostHandle';
import ColorStyle from '../../../../resource/ColorStyle';
import {DotIndicator} from 'react-native-indicators';
import InformationGroupOther from './InformationGroup';
import GroupDetailMember from './GroupDetailMember';
import ManageGroup from "./manage/ManageGroup/ManageGroup";
import GroupHandle from "../../../../sagas/GroupHandle";
import {EventRegister} from "react-native-event-listeners";
const fixData = [
  {
    type: AppConstants.MyGroup.BANNER,
  },
  {
    type: AppConstants.MyGroup.OPTION,
  },
];
const suggestionItem = {
  type: AppConstants.MyGroup.SUGGESTION_GROUP,
};
let currentPosition = 0;
let LIMIT = 5;
let addedSuggestionGroup = false;
let groupPosts = fixData;
class GroupDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      page: 0,
      refreshing: false,
      showFloatingButton: false,
      canLoadData: true,
      finishLoad: false,
      group:undefined,
      index: 0,
      routes: [
        {
          key: 'tabComment',
          title: strings('group'),
          icon: 'user-friends',
          type: 'font-awesome-5',
        },
        {
          key: 'tabInvite',
          title: strings('invite'),
          icon: 'user-plus',
          type: 'font-awesome-5',
        },
        {
          key: 'tabManage',
          title: strings('manage'),
          icon: 'user-cog',
          type: 'font-awesome-5',
        },
      ],
    };
  }
  componentDidMount() {
    this.getGroupDetail()
    this.getGroupPostData();
    EventRegister.addEventListener(AppConstants.EventName.UPDATE_GROUP, total => {
      this.setState({group:undefined},()=>{
        this.getGroupDetail()
      })

    });

  }
  getGroupDetail(){
      GroupHandle.getInstance().getDetailGroup(
          this.props.item.id,
          (isSuccess, responseData) => {
            console.log("responseData:",responseData)
            if (isSuccess) {
              let data = responseData.data;
              console.log("data:",data)
              this.setState({group:data});
            }
          },
      );
  }
  render() {
    return (
      <View style={[Styles.container]}>
        {this.renderOption()}
      </View>
    );
  }
  renderOption() {
    switch (this.props.checkRole) {
      //1 là admin
      //2 là thành viên
      //3 là khách
      case 1:
        return  <ManageGroup item={this.state.group===undefined?this.props.item:this.state.group} callback={()=>this.props.onRefresh()}/>
      case 2:
        return <GroupDetailMember item={this.state.group===undefined?this.props.item:this.state.group} />;
      case 3:
        return (
          <InformationGroupOther data={this.state.group===undefined?this.props.item:this.state.group} outGroup={false} />
        );
    }
  }

  addSuggestionGroup() {
    addedSuggestionGroup = true;
    groupPosts.push(suggestionItem);
    this.setState({finishLoad: true});
  }
  renderLoading() {
    if (this.state.loading) {
      return (
        <View>
          <DotIndicator color={ColorStyle.tabActive} size={10} />
        </View>
      );
    } else {
      return null;
    }
  }
  getGroupPostData() {
    let param = {
      current_page: this.state.page,
      page_size: LIMIT,
    };
    GroupPostHandle.getInstance().getPostsOfGroup(
      this.props.item.id,
      param,
      (isSuccess, dataResponse) => {
        let data = fixData;
        if (this.state.page !== 0) {
          data = groupPosts;
        } else {
          groupPosts = [fixData];
        }
        let canLoadData = false;
        if (
          isSuccess &&
          dataResponse.data !== undefined &&
          dataResponse.data.list !== undefined
        ) {
          let newData = [];
          dataResponse.data.list.forEach((item, index) => {
            newData.push({
              ...item,
              isLoadDetail: false,
              isLoadComment: false,
              isLoadLike: false,
              isLoadProducts: false,
              isLoadOwnerUser: false,
            });
          });

          canLoadData = newData.length === LIMIT;
          data = data.concat(newData);
          groupPosts = data;
        }
        this.setState({loading: false, canLoadData}, () => {
          if (this.state.page === 0 && !canLoadData && !addedSuggestionGroup) {
            this.addSuggestionGroup();
          }
        });
      },
    );
  }
}
const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    onFetchMovies: () => {},
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(GroupDetail);
