import React, {Component} from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import Styles from '../../../../resource/Styles';
import {strings} from '../../../../resource/languages/i18n';
import {Icon} from 'react-native-elements';
import AppConstants from '../../../../resource/AppConstants';
import TimeUtils from '../../../../Utils/TimeUtils';
import GroupHandle from '../../../../sagas/GroupHandle';
import DataUtils from '../../../../Utils/DataUtils';
import constants from '../../../../Api/constants';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import ColorStyle from '../../../../resource/ColorStyle';
import GroupStyle from '../GroupStyle';
import GroupUtils from '../../../../Utils/GroupUtils';
import FuncUtils from '../../../../Utils/FuncUtils';
import ViewUtils from '../../../../Utils/ViewUtils';
import BannerRow from "./row/BannerRow";
import Toolbar from "../../../elements/toolbar/Toolbar";
import ToolbarMain from "../../../elements/toolbar/ToolbarMain";
import {Actions} from "react-native-router-flux";
import MediaUtils from "../../../../Utils/MediaUtils";
export default class InformationGroupOther extends Component {
  constructor(props) {
    super(props);
    this.state = {
      members: [],
        admins: [],
        countMember:0,
        countAdmin:0,
      text: '',
      canSendRequest: false,
      requestState: AppConstants.GROUP_APPROVE_STATUS.NOTHING,
      data: this.props.data,
    };
  }
  componentDidMount() {
      this.getGroupDetail();
    this.getMembers(AppConstants.ROLE.MEMBER,'members','countMember')
       this.getMembers(AppConstants.ROLE.ADMIN,'admins','countAdmin')

  }
  toolbar(){
      return(
          <View style={{position:'absolute',top:Styles.constants.X}}>
              <TouchableOpacity style={{marginLeft:Styles.constants.marginHorizontal20}} onPress={() => Actions.pop()}>
                  <Icon
                      name="left"
                      type="antdesign"
                      size={25}
                      color={ColorStyle.tabWhite}
                  />
              </TouchableOpacity>
          </View>
      )
    }
  render() {
    let data = this.props.data;
    return (
        <View
          style={{
            alignSelf: 'center',
            alignItems: 'flex-start',
          }}>
            <BannerRow canEdit={false} item={ data} />
            {this.toolbar()}
        <ScrollView showsVerticalScrollIndicator={false} style={{
            width: '100%',
            alignSelf: 'center',
        }}>
            <View
                style={{
                    width: Styles.constants.widthScreenMg24,
                    borderBottomWidth: 1,
                    borderColor: ColorStyle.gray,
                }}>
                <Text
                    style={{
                        ...Styles.text.text20,
                        color: ColorStyle.tabBlack,
                        fontWeight: '700',
                    }}>
                    {strings('introduce')}
                </Text>
                <Text
                    style={{
                        ...Styles.text.text14,
                        color: ColorStyle.tabBlack,
                        fontWeight: '300',
                        lineHeight: 20,
                        marginTop: 12,
                    }}>
                    {data.description}
                </Text>

                {this.renderStatus()}
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        marginVertical: 12,
                    }}>
                    <Icon
                        name={'clockcircle'}
                        type={'antdesign'}
                        size={25}
                        color={ColorStyle.tabBlack}
                    />
                    <View style={{flex: 4, marginLeft: 10}}>
                        <Text
                            style={{
                                ...Styles.text.text14,
                                color: ColorStyle.tabBlack,
                                fontWeight: '700',
                            }}>
                            {strings('history')}
                        </Text>
                        <Text
                            style={{
                                ...Styles.text.text14,
                                color: ColorStyle.tabBlack,
                                fontWeight: '400',
                            }}>
                            {strings('desGroup')} {TimeUtils.formatDay(data.created_date)}
                        </Text>
                    </View>
                </View>
            </View>
            {this.renderMember()}
            {this.checkOutGroup()}
        </ScrollView>
        </View>
    );
  }
  checkOutGroup() {
    if (this.props.outGroup) {
      return (
        <TouchableOpacity
          style={{...GroupStyle.buttonCre,marginBottom:Styles.constants.X, alignSelf: 'center'}}
          onPress={() => {}}>
          <Text
            style={{
              ...Styles.text.text18,
              color: ColorStyle.tabWhite,
              fontWeight: '700',
            }}>
            {strings('leaveGroup')}
          </Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          style={{...GroupStyle.buttonCre,marginBottom:Styles.constants.X, alignSelf: 'center'}}
          onPress={() => {
            if (this.state.canSendRequest) {
              this.requestJoinGroup();
            }
          }}>
          <Text
            style={{
              ...Styles.text.text18,
              color: ColorStyle.tabWhite,
              fontWeight: '700',
            }}>
            {this.state.text}
          </Text>
        </TouchableOpacity>
      );
    }
  }
  getButtonText(state) {
      if (!state
      ) {
          this.setState({canSendRequest: true, text: strings('join')});
      } else {
          this.setState({canSendRequest: false, text: strings('sentRequest')});
      }
  }
  renderStatus() {
    let data = this.props.data;
    if (data.status === AppConstants.GROUP_PRIVACY.PUBLIC) {
      return (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            marginTop: 12,
          }}>
          <Icon
            name={'md-globe-outline'}
            type={'ionicon'}
            size={25}
            color={ColorStyle.tabBlack}
          />
          <View style={{flex: 4, marginLeft: 10}}>
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabBlack,
                fontWeight: '700',
              }}>
              {strings('public')}
            </Text>
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabBlack,
                fontWeight: '400',
              }}>
              {strings('publicDes')}
            </Text>
          </View>
        </View>
      );
    } else {
      return (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            marginTop: 12,
          }}>
          <Icon
            name={'lock'}
            type={'evilicon'}
            size={25}
            color={ColorStyle.tabBlack}
          />

          <View style={{flex: 4, marginLeft: 10}}>
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabBlack,
                fontWeight: '700',
              }}>
              {strings('private')}
            </Text>
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabBlack,
                fontWeight: '400',
              }}>
              {strings('privateDes')}
            </Text>
          </View>
        </View>
      );
    }
  }
  getGroupDetail() {
    GroupHandle.getInstance().getDetailGroup(
      this.props.data.id,
      (isSuccess, responseData) => {
        if (isSuccess) {
          let data = responseData.data;
          this.setState({data,countMember:data.total_member});
            this.getButtonText(data.is_request);
        }
      },
    );
  }
  requestJoinGroup() {
    FuncUtils.getInstance().callRequireLogin(() => {
      let param = {
          team_id:this.props.data.id,
          team_name:this.props.data.name
      };
      GroupHandle.getInstance().requestJoinGroup(
        param,
        (isSuccess, responseData) => {
            console.log(responseData)
          if (isSuccess) {
            this.getButtonText(true);
            ViewUtils.showAlertDialog(strings('requestJoinSuccess'));
          } else {
            ViewUtils.showAlertDialog('Error');
          }
        },
      );
    });
  }
  renderMember() {
      let admins = this.state.admins;
    return (
      <View
        style={{
          width: Styles.constants.widthScreenMg24,
          borderBottomWidth: 1,
          borderColor: ColorStyle.gray,
          paddingVertical: 16,
        }}>
        <Text
          style={{
            ...Styles.text.text20,
            color: ColorStyle.tabBlack,
            fontWeight: '700',
          }}>
          {strings('members')}
        </Text>
        <View style={{marginHorizontal: 30}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between',marginTop:10,width:Styles.constants.widthScreenMg24-Styles.constants.X}}>
            <Text style={{...Styles.text.text14, color: ColorStyle.gray}}>
              {this.state.countMember} {strings('member1').toLowerCase()}
            </Text>
          </View>
          <Text
            style={{
              ...Styles.text.text14,
              color: ColorStyle.tabBlack,
              fontWeight: '700',
              marginTop: 10,
            }}>
            {strings('admin')}
          </Text>
          <View style={{flexDirection: 'row',marginTop:10}}>
            {admins.map((item,index) => (
                <View  style={{alignItems:'center'}}   key={index.toString()}>
                    <Image

                        source={ MediaUtils.getAvatar(item.customers)}
                        style={{width: 24, height: 24, borderRadius: 24,borderWidth:0.5,borderColor:'black'}}
                    />
                    <Text style={{fontSize:11,color:ColorStyle.tabBlack,marginTop:5}}>{item.customers.full_name}</Text>
                </View>
            ))}
          </View>
        </View>
      </View>
    );
  }
  getAvatarUrl(item) {
    let url = item.avatar;

    return DataUtils.stringNullOrEmpty(url) ? '' : constants.host + url;
  }
  getMembers(type,state,count) {
    let param = {
        role:type,
        page_index:1,
        page_size:4
    };
    GroupHandle.getInstance().getMemberOfGroup(
      this.props.data.id,
      param,
      (isSuccess, dataResponse) => {
        if (isSuccess && dataResponse.data.memberInfo !== undefined) {
          this.setState({[state]: dataResponse.data.memberInfo});
        }
      },
    );
  }
}
