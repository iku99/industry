import {Platform} from 'react-native';
import Styles from '../../../resource/Styles';
import ColorStyle from "../../../resource/ColorStyle";
let X = Styles.constants.X;
let isAndroid = Platform.OS === 'android';
export default {
  container: {
    flex:1,
  },
  bannerStore: {
    borderBottomColor:ColorStyle.borderItemHome,
    borderBottomWidth:1,
    width: '100%',
    height: Styles.constants.heightScreen / 3.5,
  },
  avatarStore: {
    width: X * 2.35,
    height: X * 2.35,
    borderRadius: X * 4,
    borderWidth:1,
    backgroundColor:ColorStyle.tabWhite,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 2,
  },
  containerStoreInfo: {
    marginTop:- (X * 2.35) / 2,
    width: '100%',
    paddingHorizontal: X * 0.5,
  },
  storeInfo: {
    flexDirection: 'row',
    width: '100%',
  },
  storeAddress: {
    flexDirection: 'row',
    width: '100%',
  },
  containerText: {
    width: '100%',
    height: X * 2.35,
    marginLeft: 10,
    justifyContent: 'space-around',
  },
};
