import React, {Component} from 'react';
import {Dimensions, Image, Text, TouchableOpacity, View} from 'react-native';
import StoreStyle from '../StoreStyle';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
import ImageHelper from '../../../../resource/images/ImageHelper';
import {strings} from '../../../../resource/languages/i18n';
import {SceneMap, TabView} from 'react-native-tab-view';
import TabDiscover from '../../group/tab/TabDiscover';
import TabGroups from '../../group/tab/TabGroups';
import TabCreatGroup from '../../group/tab/TabCreatGroup';
import ViewUtils from '../../../../Utils/ViewUtils';
import TabInfo from '../tab/TabInfo';
import TabProduct from '../tab/TabProduct';
import TabCategory from '../tab/TabCategory';
import StoreHandle from '../../../../sagas/StoreHandle';
import AppConstants from '../../../../resource/AppConstants';
import AccountHandle from '../../../../sagas/AccountHandle';
export default class StoreTabRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isGroupDes: false,
      index: 0,
    };
  }
  render() {
    let i = this.state.index;
    return (
      <View style={{paddingTop: 20,backgroundColor:ColorStyle.tabWhite}}>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 6,
            borderColor: 'gray',
            paddingHorizontal: 10,
            paddingVertical: 5,
            borderBottomWidth: 0.5,
            borderTopWidth: 0.5,
          }}>
          <TouchableOpacity
            onPress={() => this.setState({index: 0})}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              paddingVertical: 6,
              borderRadius: 20,
              marginLeft: 10,
              paddingHorizontal: 3,
              backgroundColor:
                i === 0 ? ColorStyle.tabActive : ColorStyle.tabWhite,
            }}>
            <Text
              style={{
                ...Styles.text.text14,
                color: i === 0 ? ColorStyle.tabWhite : ColorStyle.tabBlack,
                fontWeight: i === 0 ? '700' : '400',
              }}>
              {strings('introduce')}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.setState({index: 1})}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              paddingVertical: 6,
              borderRadius: 20,
              marginLeft: 10,
              paddingHorizontal: 3,
              backgroundColor:
                i === 1 ? ColorStyle.tabActive : ColorStyle.tabWhite,
            }}>
            <Text
              style={{
                ...Styles.text.text14,
                color: i === 1 ? ColorStyle.tabWhite : ColorStyle.tabBlack,
                fontWeight: i === 1 ? '700' : '400',
              }}>
              {strings('product')}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.setState({index: 2})}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              paddingVertical: 6,
              borderRadius: 20,
              marginLeft: 10,
              paddingHorizontal: 3,
              backgroundColor:
                i === 2 ? ColorStyle.tabActive : ColorStyle.tabWhite,
            }}>
            <Text
              style={{
                ...Styles.text.text14,
                color: i === 2 ? ColorStyle.tabWhite : ColorStyle.tabBlack,
                fontWeight: i === 2 ? '700' : '400',
              }}>
              {strings('category')}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ paddingVertical: 20}}>
          {this.renderTab(i)}
        </View>
      </View>
    );
  }
  renderTab(i) {
    let store = this.props.item;
    if (i === 0) {
      return <TabInfo item={store} />;
    } else if (i === 1) {
      return <TabProduct idStore={store.id}/>;
    } else {
      return <TabCategory item={store} />;
    }
  }
}
