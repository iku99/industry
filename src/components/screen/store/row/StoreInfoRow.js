import React, {Component} from 'react';
import {Image, ImageBackground, Text, View} from 'react-native';
import StoreStyle from '../StoreStyle';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
import ImageHelper from '../../../../resource/images/ImageHelper';
import {strings} from '../../../../resource/languages/i18n';
import StoreHandle from '../../../../sagas/StoreHandle';
import constants from '../../../../Api/constants';
import GroupStyle from "../../group/GroupStyle";
import MediaUtils from "../../../../Utils/MediaUtils";
import {Icon} from "react-native-elements";
export default class StoreInfoRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productCount: 0,
      coverList: [],
    };
  }
  componentDidMount() {
  }

  render() {
    let store = this.props.item;
    return (
      <View style={{flex:1,backgroundColor:ColorStyle.tabWhite}}>
        <ImageBackground
          source={{uri: store.image_cover_url===undefined ? '' : ''}}
          style={{overflow: 'hidden', backgroundColor: 'rgba(0,0,0,0.2)'}}>
          <Image source={MediaUtils.getStoreCover(store.image_url,store?.id)} style={StoreStyle.bannerStore}/>
          <View style={{...GroupStyle.bannerGroupDetail,position:'absolute',backgroundColor:'rgba(0, 0, 0, 0.7)'}}/>
        </ImageBackground>

        <View style={StoreStyle.containerStoreInfo}>
          <View style={StoreStyle.storeInfo}>
            <Image
              style={StoreStyle.avatarStore}
              source={{uri: this.getStoreAvatar('avatar')}}
            />
            <View style={StoreStyle.containerText}>
              <Text
                style={{
                  ...Styles.text.text18,
                  color: ColorStyle.tabWhite,
                  fontWeight: '700',
                }}>
                {this.getStoreName()}
              </Text>
              <View style={{flexDirection:'row',alignItems:'center'}}>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                  <Icon name={'eye'} type={'entypo'} size={20} color={ColorStyle.tabActive}/>
                  <Text style={{...Styles.text.text14 ,marginLeft:5}}>
                    {store.views===null?0:store.views} |</Text>
                </View>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                  <Icon name={'star-rate'} type={'material-icons'} size={20} color={ColorStyle.vang}/>
                  <Text style={{...Styles.text.text14 ,marginLeft:5}}>
                    {store.rate===null?0:store.rate} |</Text>
                </View>
                <Text style={{...Styles.text.text14 ,marginLeft:5}}>
                    {store.product_quantity}{' '}
                    {strings('product')}</Text>
              </View>
            </View>
          </View>

        </View>
      </View>
    );
  }
  getStoreName() {
    let store = this.props.item;
    if (store !== undefined && store.name !== undefined) {
      return store?.name;
    }
    return '';
  }
  getStoreAvatar(type,) {
    let store = this.props.item;
    if (store !== undefined && store[type] !== null) {
      return `${constants.host}${store[type]}`;
    } else {
      return 'https://picsum.photos/300/300?avatar='+store.id;
    }
  }

}
