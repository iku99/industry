import React, {Component} from 'react';
import {FlatList, Image, Text, View} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../resource/Styles';
import {strings} from '../../../resource/languages/i18n';
import AppConstants from '../../../resource/AppConstants';
import ToolbarSearch from '../../elements/toolbar/ToolbarSearch';
import StoreInfoRow from './row/StoreInfoRow';
import StoreTabRow from './row/StoreTabRow';
import StoreHandle from '../../../sagas/StoreHandle';
import ProductHandle from '../../../sagas/ProductHandle';
const defaulData = [
  {type: AppConstants.listStoreDetail.STORE_INFO},
  {type: AppConstants.listStoreDetail.TAB_INFORMATION},
  {type: AppConstants.listStoreDetail.BANNER_ACTION},
  {type: AppConstants.listStoreDetail.TEXT},
  {type: AppConstants.listStoreDetail.PRODUCT},
];
let products = [];
class StoreDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [...defaulData],
      loading: false,
      page: 0,
      canLoadData: true,
      store: {},
      storeId: this.props.storeId,
      userInfo: {},
      coverList: [],
      countRate: 5,
      countAvg: 0,
      productCount: 0,
      tabOptionIndex: 1,
      orderText: strings('storeOrder'),
      sortValue: undefined,
      showSort: false,
      showCategory: false,
      categories: [],
      category: undefined,
      region: {
        latitude: AppConstants.MAP.LATITUDE,
        longitude: AppConstants.MAP.LONGITUDE,
        latitudeDelta: AppConstants.MAP.COORDINATE_DELTA,
        longitudeDelta: AppConstants.MAP.COORDINATE_DELTA,
      },
    };
  }
  componentDidMount() {
    this.getStoreDetail(this.state.storeId);
    this.onPressView(this.state.storeId)
    this.getProductList();

  }
  onPressView(id){
      StoreHandle.getInstance().viewStore(id,(isSuccess,res)=>{

      })
  }
  render() {
    return (
      <View style={Styles.container}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={this.state.data}
          extraData={this.state}
          keyExtractor={this.keyExtractor}
          renderItem={({item,index})=>this._renderItem(item)}
          style={{
            position: 'absolute',
            top: 20,
            width: '100%',
            height: '100%',
          }}
          ref={ref => {
            this.listRef = ref;
          }}
        />
        <ToolbarSearch />
      </View>
    );
  }

  _renderItem = (item) => {
    let store = this.state.store;
    switch (item.type) {
      case AppConstants.listStoreDetail.STORE_INFO:
        return <StoreInfoRow item={store} />;
      case AppConstants.listStoreDetail.TAB_INFORMATION:
        return <StoreTabRow item={store} />;
      case AppConstants.listStoreDetail.END:
        return <View style={{marginBottom: 10}} />;
    }
  };
  getStoreDetail(storeId) {
    StoreHandle.getInstance().getStoreDetail(storeId, (isSuccess, store) => {
      if (isSuccess) {
        console.log(store)
        this.setState({store});
      }
    });
  }
  getProductList() {
    let param = {
      current_page: this.state.page,
      page_size: 5,
    };
    ProductHandle.getInstance().getProductStore(
        param,this.state.storeId,
        (isSuccess, responseData) => {
          if (isSuccess) {
            if (responseData.data.length < 5) {
              this.setState({canLoadData: false});
            }
            let listData = this.state.data;
            let abc = {
              data: responseData.data,
              type: AppConstants.listStoreDetail.PRODUCT,
            };
            listData = [...listData, ...[abc]];
            this.setState({
              loading: false,
              loaded: true,
              data: listData,
            });
          }
        },
    );
  }
}
//lấy dữ liệu về
const mapStateToProps = state => {
  return {};
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {
    onFetchMovies: () => {},
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(StoreDetail);
