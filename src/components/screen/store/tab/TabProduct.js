import React, {Component} from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
import {strings} from '../../../../resource/languages/i18n';
import {Icon} from 'react-native-elements';
import ViewProductItem from '../../../elements/viewItem/ViewedProduct/ViewProductItem';
import StoreHandle from '../../../../sagas/StoreHandle';
import AppConstants from '../../../../resource/AppConstants';
import AccountHandle from '../../../../sagas/AccountHandle';
import ProductHandle from '../../../../sagas/ProductHandle';
import constants from '../../../../Api/constants';
import {UIActivityIndicator} from 'react-native-indicators';
import EmptyView from "../../../elements/reminder/EmptyView";
import NavigationUtils from "../../../../Utils/NavigationUtils";
export default class TabProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      selectedFilter: 0,
      sortValue: undefined,
      priceUp: false,
      loading: false,
      page: 1,
      canLoadData: true,
    };
  }
  componentDidMount() {
    this.getProductList();
  }

  render() {
    return (
      <View style={{flex: 1}}>
        {this.renderTabFilter()}
        {this.renderProduct()}
      </View>
    );
  }
  renderProduct() {
  if (this.state.data.length === 0) {
      return <EmptyView styles={{marginTop:Styles.constants.X}} text={strings('notProduct')}/>
    }else {
        return (
            <FlatList
                data={this.state.data}
                style={{alignSelf:'center'}}
                numColumns={2}
                renderItem={this.renderItem}
                keyExtractor={this.keyExtractor}
                ListFooterComponent={this.renderFooter.bind(this)}
                onEndReachedThreshold={0.4}
                onEndReached={() => this.handleLoadMore()}
            />
        );
    }
  }
    keyExtractor = (item, index) => `list_product_store_${index.toString()}`;
  renderItem = ({item, index}) => {
    return <ViewProductItem item={item}  onClick={item => {
        NavigationUtils.goToProductDetail(item);
    }}/>;
  };
  renderFooter = () => {
      console.log(this.state.loading,this.state.canLoadData)
    if (!this.state.loading) {
      return null;
    }
    return <UIActivityIndicator color={ColorStyle.tabActive} size={20} />;
  };
  handleLoadMore() {

    if (this.state.loading) {
      return;
    }
    if (!this.state.canLoadData) {
      return;
    }
    this.setState(
      {loading: true, page: this.state.page + 1},
      this.getProductList,
    );
  }
  renderTabFilter() {
    let isSelected = this.state.selectedFilter;
    return (
      <View
        style={{
          alignSelf: 'center',
          flexDirection: 'row',
          alignItems: 'center',
            paddingHorizontal:10
        }}>
        <TouchableOpacity
          onPress={() => {
              this.setState({selectedFilter: 0});
            setTimeout(() => {
              this.selectSortOption(undefined);
            }, 1000);

          }}
          style={{
            flex: 1,
            alignItems: 'center',
            backgroundColor:
              isSelected === 0
                ? ColorStyle.tabActive
                : ColorStyle.tabWhite,
            paddingVertical: 5,
            paddingHorizontal: 2,
            borderRadius: 20,
          }}>
          <Text
            style={{
              color:
                isSelected === 0 ? ColorStyle.tabWhite : ColorStyle.tabBlack,textAlign:'center',
              fontWeight: isSelected === 0 ? '700' : '400',
            }}>
            {strings('popular')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flex: 1,
            alignItems: 'center',
            backgroundColor:
              isSelected === 1
                ? ColorStyle.tabActive
                : ColorStyle.tabWhite,
            paddingVertical: 5,
            paddingHorizontal: 2,
            borderRadius: 20,
          }}
          onPress={() => {
              this.setState({selectedFilter: 1});
            setTimeout(() => {
              this.selectSortOption({sort_order: 'desc'});
            }, 1000);

          }}>
          <Text
            style={{
              color:
                isSelected === 1 ? ColorStyle.tabWhite : ColorStyle.tabBlack,textAlign:'center',
              fontWeight: isSelected === 1 ? '700' : '400',
            }}>
            {strings('newest')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flex: 1,
            alignItems: 'center',
            backgroundColor:
              isSelected === 2
                ? ColorStyle.tabActive
                : ColorStyle.tabWhite,
            paddingVertical: 5,
            paddingHorizontal: 2,
            borderRadius: 20,
          }}
          onPress={() => {
              this.setState({selectedFilter: 2});
            setTimeout(() => {
              this.selectSortOption({sort_by: 'sell', sort_order: 'desc'});
            }, 1000);

          }}>
          <Text
            style={{
              color:
                isSelected === 2 ? ColorStyle.tabWhite : ColorStyle.tabBlack,textAlign:'center',
              fontWeight: isSelected === 2 ? '700' : '400',
            }}>
            {strings('selling')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
              this.setState({selectedFilter: 3, priceUp: !this.state.priceUp});
            setTimeout(() => {
              this.selectSortOption({sort_by: 'price', sort_order: 'desc'});
            }, 1000);

          }}
          style={{
            flex: 1,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor:
              isSelected === 3
                ? ColorStyle.tabActive
                : ColorStyle.tabWhite,
            paddingVertical: 5,
            paddingHorizontal: 2,
            borderRadius: 20,
          }}>
          <Text
            style={{
              color:
                isSelected === 3 ? ColorStyle.tabWhite : ColorStyle.tabBlack,
              fontWeight: isSelected === 3 ? '700' : '400',
            }}>
            {strings('price')}
          </Text>
          <Icon
            name={this.state.priceUp ? 'arrowdown' : 'arrowup'}
            size={18}
            type={'antdesign'}
            color={isSelected === 3 ? ColorStyle.tabWhite : ColorStyle.tabBlack}
          />
        </TouchableOpacity>
      </View>
    );
  }
  getProductList() {
    let param = {
        page_size: 20,
        page_index: this.state.page,
        store_id:this.props.idStore
    };
      // if (this.state.selectedFilter === 0) {
      //     param = {
      //         ...param,
      //         type: 'popular',
      //     };
      // }
      if (this.state.selectedFilter === 1) {
          param = {
              ...param,
          };
      }
      if (this.state.selectedFilter === 2) {
          param = {
              ...param,
              type: 'best-seller',
          };
      }
      if (this.state.selectedFilter === 3) {
          param = {
              ...param,
              columnName:'Price',
              isDesc: this.state.priceUp ? true : false,
          };
      }
      ProductHandle.getInstance().getProductStore(
      param,
      (isSuccess, responseData, _) => {
        if (isSuccess) {
          if (responseData.data != null && responseData.data.data) {
              let listData = this.state.data;
            if(responseData.data.data.length<20){this.setState({canLoadData:false}) }
              listData=listData.concat(responseData.data.data)
              this.setState({
                  loading: false,
                  data: listData,
              });
          }
        }
      },
    );
  }
  selectSortOption(sort) {
    this.setState({data: []});
    this.setState(
      {sortValue: sort, loading: false, page: 0, canLoadData: true},
      () => this.getProductList(),
    );
  }
}
