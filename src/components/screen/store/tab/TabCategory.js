import React, {Component} from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../../resource/Styles';
import StoreHandle from '../../../../sagas/StoreHandle';
import CategoryHandle from '../../../../sagas/CategoryHandle';
import constants from '../../../../Api/constants';
import {Icon} from 'react-native-elements';
import ColorStyle from '../../../../resource/ColorStyle';
import {strings} from '../../../../resource/languages/i18n';
export default class TabCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      idStore: this.props.item.id,
      images: [],
    };
  }
  componentDidMount() {
    this.getStoreCategory();
  }

  render() {
    return (
      <FlatList
        data={this.state.categories}

        renderItem={this.renderItem}
      />
    );
  }
  renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'center',
          paddingVertical: 11,
          paddingHorizontal: 30,
          borderBottomWidth: 0.5,
          borderTopWidth: 0.5,
          borderColor: '#BABABA',
        }}>
        <View style={{flex: 1,}}>
          <Image
              source={{uri:constants.host+item.image_url}}
            style={{...Styles.icon.iconGroup}}
          />
        </View>
        <View style={{flex: 4, alignItems: 'flex-start'}}>
          <Text
            style={{
              ...Styles.text.text14,
              color: ColorStyle.tabBlack,
              fontWeight: '500',
            }}>
            {item.name}
          </Text>
          <Text>
            {item.total_products} {strings('product1')}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  getStoreCategory() {
    let params = {
      page_index:1,
        page_size:100
    };
    StoreHandle.getInstance().getStoreCategory(
      this.state.idStore,
      params,
      (isSuccess, dataResponse) => {
        if (isSuccess && dataResponse.data !== undefined) {
          let categories = dataResponse.data;
          if (categories === undefined) {
            categories = [];
          }
          this.setState({categories: categories});
        }
      },
    );
  }
  getDetailCategory(id) {
    CategoryHandle.getInstance().getCategoryDetail(
      id,
      (isSuccess, responseData) => {
        if(responseData.data != null) { this.setState({images: responseData.data.avatar_url});}

      },
    );
  }
}
