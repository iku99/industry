import React, {Component} from 'react';
import {Text, View} from 'react-native';
import Styles from '../../../../resource/Styles';
import HTMLView from "react-native-htmlview";
import ColorStyle from "../../../../resource/ColorStyle";
import {strings} from "../../../../resource/languages/i18n";
export default class TabInfo extends Component {
  render() {
      console.log(this.props.item)
    return (
      <View style={{flex: 1,paddingHorizontal:10,height:Styles.constants.heightScreen/2}}>
          <View style={{width: '100%', flexDirection: 'row'}}>
              <Text
                  numberOfLines={2}
                  style={[
                      Styles.text.text16,
                      {flex: 1, color: ColorStyle.secondary},
                  ]}>
                  {strings('address1')} :
              </Text>
              <Text
                  numberOfLines={2}
                  style={[
                      Styles.text.text16,
                      {flex: 4, color: ColorStyle.tabBlack},
                  ]}>
                  {this.props.item?.address?.address}
              </Text>
          </View>
          <View style={{width: '100%', flexDirection: 'row',marginTop:10}}>
              <Text
                  numberOfLines={2}
                  style={[
                      Styles.text.text16,
                      { color: ColorStyle.secondary},
                  ]}>
                  {strings('phone')}:
              </Text>
              <Text
                  style={{...Styles.text.text16,textAlign:'left',color: ColorStyle.tabBlack,lineHeight:Styles.constants.X/2}}>
                  {this.props.item.phone}
              </Text>
          </View>
          <View style={{width: '100%', flexDirection: 'row',marginTop:10}}>
              <Text
                  numberOfLines={2}
                  style={[
                      Styles.text.text16,
                      { color: ColorStyle.secondary},
                  ]}>
                  {strings('email')}:
              </Text>
              <Text
                  style={{...Styles.text.text16,textAlign:'left',color: ColorStyle.tabBlack,lineHeight:Styles.constants.X/2}}>
                  {this.props.item.email}
              </Text>
          </View>
          <View style={{width: '100%', flexDirection: 'row',marginTop:10}}>
              <Text
                  numberOfLines={2}
                  style={[
                      Styles.text.text16,
                      {flex: 1, color: ColorStyle.secondary},
                  ]}>
                  {strings('des2')}
              </Text>
              <Text
                  style={{...Styles.text.text16,flex: 4,textAlign:'left',color: ColorStyle.tabBlack,lineHeight:Styles.constants.X/2}}>
                  {this.props.item.des}
              </Text>
          </View>
          <HTMLView
              style={{marginTop: 20,paddingHorizontal: 5,display:this.props.item.content===null?'none':'flex'}}
              value={this.props.item.content}
          />
      </View>
    );
  }
}
