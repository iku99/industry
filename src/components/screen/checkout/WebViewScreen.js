import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../resource/Styles';
import WebView from 'react-native-webview';
import {Actions} from 'react-native-router-flux';
import Toolbar from '../../elements/toolbar/Toolbar';
import {strings} from '../../../resource/languages/i18n';
import ColorStyle from "../../../resource/ColorStyle";
import {Icon} from "react-native-elements";
export default class WebviewScreen extends Component {
  onNavigationStateChanged(navState) {
    let url = navState.url;
    if (this.props.onRedirect !== undefined) {
      if (this.props.onRedirect(url)) {
        Actions.pop();
      }
    }
  }
  renderToolbar(){
      return(
          <View
              style={styles.container}>
              <Text
                  style={[
                      Styles.text.text20,
                      {
                          color: ColorStyle.tabWhite,
                          fontWeight: '700',
                          top: 0,
                          textAlign: 'center',
                      },
                  ]}
                  numberOfLines={1}>
                  {strings('checkOut')}
              </Text>
              <TouchableOpacity
                  onPress={() => Actions.reset('drawer')}
                  style={{
                      position: 'absolute',
                      paddingVertical: Styles.constants.X ,
                      paddingHorizontal: Styles.constants.X * 0.4,
                      top: 0,
                      left: 0,
                  }}>
                  <Icon
                      name="left"
                      type="antdesign"
                      size={25}
                      color={ColorStyle.tabWhite}
                  />
              </TouchableOpacity>
          </View>
      )
  }
  render() {
    return (
        <View
            style={{
              flex: 1,
              flexDirection: 'column',
              width: Styles.constants.widthScreen,
            }}>
            {this.renderToolbar()}
          <WebView
              source={{uri: this.props.url}}
              onNavigationStateChange={this.onNavigationStateChanged.bind(this)}
          />
        </View>
    );
  }
}
const styles = StyleSheet.create({
    container: {
        paddingTop: Styles.constants.X ,
        paddingHorizontal: Styles.constants.X * 0.65,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: ColorStyle.tabActive,
        paddingBottom: 10,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
});
