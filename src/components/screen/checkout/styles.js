import Styles from "../../../resource/Styles";
import ColorStyle from "../../../resource/ColorStyle";

const X=Styles.constants.X
export default {
    bodyView:{
        backgroundColor:ColorStyle.tabWhite,
        marginBottom:X/5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
        padding:X/4
    },
    textTitle:{
        ...Styles.text.text14,
        color: ColorStyle.tabBlack,
        fontWeight:'500'
    },
    textTitleShipping:{
        ...Styles.text.text16,
        color: ColorStyle.tabBlack,
        fontWeight:'500'
    },
    textPriceShipping:{
        ...Styles.text.text14,
        color: ColorStyle.red,
        fontWeight:'500'
    },
    textDesShipping:{
        ...Styles.text.text12,
        color: ColorStyle.gray,
        marginBottom:X/10,
        fontWeight:'400'
    },
    textValue:{
        ...Styles.text.text12,
        color: ColorStyle.tabBlack,
    },
    bodyVoucher:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent: 'space-between',
        padding:X/4,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        backgroundColor:ColorStyle.tabWhite,
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
    },
    textDes:{
        ...Styles.text.text12,
        color: ColorStyle.tabBlack,
        fontWeight: '500',
    },
    viewAccept:{
        height:'10%',
        backgroundColor:ColorStyle.tabWhite,
        marginBottom:X/5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
        padding:X/4,
        alignItems:'center'
    },
    btnAccept:{
        width:'80%',
        height:X,
        backgroundColor:ColorStyle.tabActive,
        alignItems: 'center',
        justifyContent: 'center'
    },

    textTitleVoucher:{
        ...Styles.text.text16,
        color: ColorStyle.tabActive,
        fontWeight:'500'
    },
}
