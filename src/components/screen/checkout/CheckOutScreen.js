import React, {Component} from 'react';
import {
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../resource/Styles';
import ToolbarMain from '../../elements/toolbar/ToolbarMain';
import {strings} from '../../../resource/languages/i18n';
import NavigationUtils from '../../../Utils/NavigationUtils';
import ColorStyle from '../../../resource/ColorStyle';
import CurrencyFormatter from '../../../Utils/CurrencyFormatter';
import AddressHandle from '../../../sagas/AddressHandle';
import ViewUtils from '../../../Utils/ViewUtils';
import OrderHandle from '../../../sagas/OrderHandle';
import AppConstants from '../../../resource/AppConstants';
import constants from '../../../Api/constants';
import CartHandle from '../../../sagas/CartHandle';
import {Actions} from 'react-native-router-flux';
import CartItemCheckout from '../../elements/viewItem/cartItem/CartItemCheckout';
import ImageHelper from '../../../resource/images/ImageHelper';
import GlobalInfo from "../../../Utils/Common/GlobalInfo";
import {Icon} from "react-native-elements";
import styles from "./styles";
import RenderLoading from "../../elements/RenderLoading";
import LoadingClick from "../../elements/LoadingClick";
import RBSheet from "react-native-raw-bottom-sheet";
import VoucherHandle from "../../../sagas/VoucherHandle";
import EmptyView from "../../elements/reminder/EmptyView";
import VoucherStoreItem from "../../elements/viewItem/ItemVoucher/VoucherStoreItem";
import VoucherStoreNotCheckBox from "../../elements/viewItem/ItemVoucher/VoucherStoreNotCheckBox";
import VoucherUtils from "../../../Utils/VoucherUtils";
import CheckoutUtils from "../../../Utils/CheckoutUtils";
import I18n from "react-native-i18n";
let listShipping=[]
let dataCard = [
  {
    id: AppConstants.PAYMENT_TYPE.COD,
    name: strings('shipCod'),
    canSelect: true,
    icon: ImageHelper.iconMony,
  },
  {
    id: AppConstants.PAYMENT_TYPE.CREDIT_CARD,
    name: strings('shipCreditCard'),
    canSelect: true,
    icon: ImageHelper.iconMatter,
  },
];

const DataDef=[
  {
    type:AppConstants.CheckoutType.ADDRESS,
  },
  {
    type:AppConstants.CheckoutType.LIST_PRODUCT,
  },
  // {
  //     type:AppConstants.CheckoutType.SHIP_FEE,
  // },

  {
    type:AppConstants.CheckoutType.PAYMENT,
  },
  {
    type:AppConstants.CheckoutType.VOUCHER,
  },
  {
    type:AppConstants.CheckoutType.INFO_PAYMENT,
  }
]
class CheckOutScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: this.props.products,
      useEpoint: false,
      addressList: [],
      saveAddress: false,
      shipType: AppConstants.SHIP_TYPE.GIAO_HANG_TIET_KIEM,
      paymentType: AppConstants.PAYMENT_TYPE.COD,
      usingReceiveInfo: true,
      exportInvoice: false,
      slider1ActiveSlide: 0,
      showingAddressEditView: false,
      addressEdit: undefined,
      validName: false,
      validPhone: false,
      validProvince: false,
      validDistrict: false,
      validWard: false,
      validAddress: false,
      receiver: undefined,
      activeSlide: 0,
      shipping: 0,
      total: 0,
      voucher:0,
      loading:true,
      loadingCheckOut:false,
      listShipping:[],
      voucherStore:[],
      voucherStoreItem:undefined,
      voucherEItem:undefined,
      discount_detail:undefined,
      data:undefined,
      idVoucherSelect:undefined
    };
  }
  componentDidMount() {
    this.setState({data:CheckoutUtils.formatData(this.props.products)},()=>{


      this.getAddressList();
    })

  }
  getVoucherStore(id,product){
    let dataId=product.map(elm=>{
      return  {
        id:elm.id,
        quantity:elm.quantity,
        total_price:elm.price*elm.quantity
      }
    })
    let params={
      products:dataId,
      store_id:id,
    }
    VoucherHandle.getInstance().getListVoucherApply(params,(isSuccess,responseData)=>{
      if(isSuccess && responseData.code===0){
        let mData=responseData.data;
        if(this.state.idVoucherSelect !==undefined){
          mData=mData.map(item=>{
            if(item.store_id ===this.state.idVoucherSelect){
              return {
                ...item,
                checked:true
              }
            }
          })
        }
        this.setState({voucherStore:mData})
      }
    })
  }
  render() {
    return (
      <View style={Styles.container}>
        <ToolbarMain
          title={strings('checkOut')}
          iconProfile={true}
          iconBack={true}
        />
        {!this.state.loading && (
          <FlatList
            showsVerticalScrollIndicator={false}
            data={DataDef}
            renderItem={({item})=>this._renderItem(item)}
            keyExtractor={this.keyExtractor}
            onEndReachedThreshold={0.5}
            removeClippedSubviews={true}
            windowSize={10}/>
        )}
        {this.state.loading && ( <RenderLoading loading={this.state.loading} styles={{width:'100%',height:'80%'}}/>)}
        {this.state.loadingCheckOut && ( <LoadingClick loading={this.state.loadingCheckOut} styles={{width:'100%',height:'80%'}}/>)}
        {this.renderVoucherBottom()}
      </View>
    );
  }
  keyExtractor1 = (item, index) => `renderVoucherStore_${index.toString()}`;
  renderVoucherBottom(){
    return(
      <RBSheet
        customStyles = {{container:{backgroundColor:'#F9F9F9', overflow:'hidden'}}} keyboardAvoidingViewEnabled = {false}
        height={(Styles.constants.heightScreen/2)}
        ref={ref => {
          this.viewVoucher = ref;
        }}
        openDuration ={150}
        closeDuration = {150}>
        <View style={{width:'100%',height:'100%',padding:Styles.constants.marginHorizontal20,backgroundColor:ColorStyle.tabWhite}}>
          <View style={{flexDirection:'row',alignItems:'center',justifyContent: 'space-between',}}>
            <Text style={{...styles.textTitleVoucher}}>{strings('voucherStore')}</Text>
            <TouchableOpacity style={{alignItems:'center',justifyContent:'center'}} onPress={()=>this.viewVoucher.close()}>
              <Icon name={'close'} type={'evilicons'} size={20}/>
            </TouchableOpacity>
          </View>
          <View>
            <FlatList
              data={this.state.voucherStore}
              style={{marginVertical:Styles.constants.X/5}}
              showsHorizontalScrollIndicator={false}
              keyExtractor={this.keyExtractor1}
              renderItem={this.renderItemStore} />
            {this.state.voucherStore.length===0 &&(
              <View
                style={{
                  width:'100%',height:'100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <EmptyView text={strings('notVoucher')} />

              </View>
            )}
          </View>
        </View>
      </RBSheet>
    )
  }
  renderItemStore = ({item, index}) => (
    <VoucherStoreNotCheckBox item={item} onChange={(item)=>{
      let data=CheckoutUtils.convertVoucherStore(this.state.data,item)
      this.setState({idVoucherSelect:item.store_id})
      this.viewVoucher.close()
      this.setState({data:data},()=>{
        this.getTotal()
      })
    }}/>)
  _renderItem(item) {
    switch (item.type) {
      case AppConstants.CheckoutType.ADDRESS:
        return this.renderAddress();
      case AppConstants.CheckoutType.LIST_PRODUCT:
        return this.renderListProduct();
      case AppConstants.CheckoutType.PAYMENT:
        return this.renderPayment();
      case AppConstants.CheckoutType.VOUCHER:
        return this.renderVoucher();
      case AppConstants.CheckoutType.INFO_PAYMENT:
        return this.renderBuyerInfo();
      default:
        break;
    }
  }
  keyExtractor = (item, index) => `checkout_${index.toString()}`;
  renderItem  (item, index) {
    return <CartItemCheckout  item={item}
                              index={index}
                              dataVoucher={this.state.voucherStoreItem}
                              callback={(value,loading)=>{
                                let data=CheckoutUtils.addShipping(this.state.data,value,index)
                                console.log('bbb:',value)
                                this.setState({loading:loading,data:data},()=>{this.getTotal()})}}
                              onVoucher={()=>{
                                this.getVoucherStore(item.store.id,item.products)
                                this.viewVoucher.open();
                              }}
                              onChange={(item)=>{
                                let data=CheckoutUtils.addShipping(this.state.data,item,index)
                                this.setState({data:data},()=>{
                                  this.getTotal()
                                })
                              }}
                              address={this.state.receiver}/>;
  };
  onVoucher = () => {
    if (this.state.receiver === undefined) {
      ViewUtils.showAlertDialog(
        strings('pleaseSelectOrCreateNewReceiverAdd'),
        undefined,
      );
      return;
    }
    NavigationUtils.goToListVoucher(this.state.data,(item,quantity,voucher)=>{
      let data=CheckoutUtils.convertVoucher(this.state.data,item.data,voucher);
      this.setState({data:data,voucherEItem:quantity},()=>{
        this.getTotal()
      })
    })
  }
  renderVoucher(){
    return(
      <TouchableOpacity onPress={this.onVoucher}  style={styles.bodyVoucher}>
        <View style={{flexDirection:'row',alignItems:'center',justifyContent: 'space-between',}}>
          <Image source={this.state.voucherEItem===undefined?ImageHelper.voucher:ImageHelper.selectVoucher} style={{width:Styles.constants.X/1.5,height:Styles.constants.X/2,marginRight:10}}/>
          <Text style={{...styles.textTitle}}>{strings('voucher')}</Text>
        </View>
        <View style={{flexDirection:'row',alignItems:'center',justifyContent: 'space-between',}}>
          <Text style={styles.textDes}>{this.state.voucherEItem===undefined?strings('selectVoucher'):this.state.voucherEItem+' ' +strings('voucher')}</Text>
          <Icon name={'right'} type={'antdesign'} color={ColorStyle.tabBlack} size={15}/>
        </View>
      </TouchableOpacity>
    )
  }
  renderAddress() {
    let data = this.state.receiver;
    if(data===undefined){
      return(
        <TouchableOpacity
          onPress={() => {
            NavigationUtils.goToAddressList(item => {
              this.setState({receiver: item,loading:true},()=>{   this.getTotal()});
            }, true);
          }}    style={styles.bodyView}>

          <Text
            style={{
              ...Styles.text.text12,
              color: ColorStyle.tabBlack,
              fontWeight: '600',
              fontStyle:'italic'
            }}>
            {strings('desNotAddress')}:
          </Text>
        </TouchableOpacity>
      )
    }else
      return(
        <View
          style={styles.bodyView}>
          {/*<Text>{I18n.t('dog')} hiuy</Text>*/}
          <Text
            style={{
              ...Styles.text.text16,
              color: ColorStyle.tabBlack,
              fontWeight: '700',
            }}>
            {strings('shippingAddress')}:
          </Text>

          <TouchableOpacity
            onPress={() => {
              NavigationUtils.goToAddressList(item => {
                this.setState({receiver: item},()=>{   this.getTotal()});

              }, true);
            }}
            style={{
              backgroundColor: ColorStyle.tabWhite,
              paddingHorizontal: 24,
              paddingVertical: 16,
              borderRadius: 12,
              borderColor: ColorStyle.borderItemHome,
              elevation: 2,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 6,
              }}>
              <Text
                style={{
                  ...Styles.text.text15,
                  color: ColorStyle.tabBlack,
                  fontWeight: '600',
                }}>
                {data?.address_category===AppConstants.ADDRESS_CATEGORY.COMPANY ? strings('company') : strings('house')}
              </Text>
              <Text
                style={{
                  ...Styles.text.text12,
                  color: ColorStyle.tabActive,
                  fontWeight: '500',
                }}>
                {strings('change')}
              </Text>
            </View>
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabBlack,
                fontWeight: '500',
                marginBottom: 6,
              }}>
              {data?.name} | {data?.phone}
            </Text>
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.gray,
              }}>
              {data?.address_detail?.address}
            </Text>
          </TouchableOpacity>
        </View>
      )
  }
  renderListProduct() {
    return (
      <FlatList
        style={{...styles.bodyView,padding:0}}
        showsVerticalScrollIndicator={false}
        data={this.state.data.list_product}
        renderItem={({item,index})=>this.renderItem(item,index)}
      />
    );
  }
  renderPayment() {
    return (
      <View
        style={styles.bodyView}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text
            style={{
              ...Styles.text.text16,
              color: ColorStyle.tabBlack,
              fontWeight: '700',
            }}>
            {strings('titlePayment')}
          </Text>
        </View>
        <FlatList
          data={dataCard}
          horizontal={true}
          style={{marginTop: 10}}
          showsHorizontalScrollIndicator={false}
          renderItem={this.renderPay}
        />
      </View>
    );
  }
  renderPay = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({paymentType: item.id});
        }}
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          borderColor:
            this.state.paymentType === item.id ? '#FA6F34' : '#DADADA',
          backgroundColor:
            this.state.paymentType === item.id
              ? 'rgba(255, 129, 50, 0.3)'
              : '#F5F5F5',
          borderWidth: 1,
          paddingVertical: 17,
          marginRight: 15,
          paddingHorizontal: 15,
          borderRadius: 5,
        }}>
        <Image source={item.icon} style={Styles.icon.iconOrder} />
        <Text style={{marginLeft: 20, ...Styles.text.text11}}>{item.name}</Text>
      </TouchableOpacity>
    );
  };
  renderViewText(title,value,voucher){
    return(
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 10,
        }}>
        <Text
          style={styles.textTitle}>
          {title}
        </Text>

        {voucher && (
          <Text
            style={styles.textValue}>
            -{CurrencyFormatter(value)}
          </Text>
        )||(
          <Text
            style={styles.textValue}>
            {CurrencyFormatter(value)}
          </Text>
        )}
      </View>
    )
  }
  renderBuyerInfo() {
    return (
      <View
        style={styles.bodyView}>
        {this.renderViewText(strings('ordered'),this.state.discount_detail.product_price)}
        {this.renderViewText(strings('delivery'),this.state.discount_detail.shipping_fee)}
        {this.renderViewText(strings('voucher'),this.state.discount_detail.reduce_shipping_fee+this.state.discount_detail.reduce_product_price,true)}

        <View style={{flexDirection: 'row', justifyContent: 'space-between',borderTopWidth:1,borderTopColor:ColorStyle.tabActive,paddingVertical:10}}>
          <Text
            style={{
              ...Styles.text.text16,
              color: ColorStyle.tabActive,
            }}>
            {strings('summary')}
          </Text>
          <Text
            style={{
              ...Styles.text.text16,
              color: ColorStyle.tabActive,
              fontWeight: '700',
            }}>
            {CurrencyFormatter(this.state.total)}
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            // this.setState({loadingCheckOut:true})
            this.onOrder()}}
          style={{
            flexDirection: 'row',
            height: 50,
            marginVertical: 20,
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 25,
            marginHorizontal: 16,
            backgroundColor: ColorStyle.tabActive,
          }}>
          <Text
            style={{
              ...Styles.text.text14,
              color: ColorStyle.tabWhite,
              fontWeight: '700',
            }}>
            {strings('checkOut')}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
  getTotal() {
    let data=this.state.data;
    let discount_detail=data.discount_detail
    this.setState({total: discount_detail.total,shipping:discount_detail.shipping_fee,voucher:discount_detail.reduce_shipping_fee+discount_detail.reduce_product_price,discount_detail,loading:false});
  }
  getAddressList() {
    AddressHandle.getInstance().getAddressList((isSuccess, dataResponse) => {
      let data = [];
      let receiver;
      if (
        isSuccess &&
        dataResponse.data !== undefined
      ) {
        data = dataResponse.data;

        for (let i = 0; i < data.length; i++) {
          let item = data[i];
          if (item.default_address===AppConstants.ADDRESS_DEFAULT.DEFAULT) {
            receiver = item;
            break;
          }
        }
        this.setState({
          loading: false,
          receiver:receiver,
        });
      }
    });
    this.getTotal()
  }
  onOrder() {
    console.log(JSON.stringify(this.state.data))
    if (this.state.receiver === undefined) {
      // this.setState({loadingCheckOut:false})
      ViewUtils.showAlertDialog(
        strings('pleaseSelectOrCreateNewReceiverAdd'),
        undefined,
      );
      return;
    }
    let product=CheckoutUtils.convertProduct(this.state.data)

    let paymentType = this.state.paymentType;
    let param = {
      address: this.state.receiver,
      payment_method: paymentType,
      ...product,
    };
    console.log('product:',JSON.stringify(param))
    OrderHandle.getInstance().orderNew(param, (isSuccess, dataResponse) => {
      console.log('cam:',dataResponse)
      if (isSuccess && dataResponse.code===0) {
        this.setState({loading:false})
        switch (paymentType) {
          case AppConstants.PAYMENT_TYPE.CREDIT_CARD:
          case AppConstants.PAYMENT_TYPE.ATM:
            let params={
              "order":
                {
                  "amount": this.state.total ,
                  "customerId":GlobalInfo?.userInfo.id.toString()
                },
              "transactionType": "domestic"
            }
            OrderHandle.getInstance().updateOnePayStatus(params,(isSuccess,dataResponse)=>{
              console.log('dataResponse:',dataResponse)
              NavigationUtils.goToWebviewScreen(dataResponse.url, url => {
                console.log('url:',url)
                if(url.startsWith(constants.host)){
                  OrderHandle.getInstance().getResultOrder(url,(res)=>{
                    console.log('bb:',res)
                    if(res){
                      let params = {
                        status: AppConstants.ORDER_STATUS.UNCONFIRMED,
                        reason: strings('userYeuCau'),
                      };
                      OrderHandle.getInstance().cancelOrder(dataResponse.data[0],
                        params,
                        (isSuccess, dataResponse) => {
                          console.log('res:',dataResponse)
                          if(isSuccess){
                            ViewUtils.showAlertDialog(strings('paymentSuccess'), null);
                            this.onOrderSuccess();
                            return
                          }
                          ViewUtils.showAlertDialog(
                            strings( 'paymentFailed'),
                            null,
                          );
                        },
                      );
                      return
                    }else {
                      this.showErrorMsg(strings('orderFailed'));
                    }
                  })
                }


              });
            })
            break;
          case AppConstants.PAYMENT_TYPE.COD:
            this.onOrderSuccess();
            break;
        }
      } else {
        this.showErrorMsg(strings('orderFailed'));
      }
    });
  }
  showErrorMsg(msg) {
    ViewUtils.showAlertDialog(msg, null);
  }
  onOrderSuccess() {
    CartHandle.getInstance().getCart(undefined);
    Actions.jump('orderSuccess');
  }
}
const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    onFetchMovies: () => {},
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CheckOutScreen);
