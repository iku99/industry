import React, {Component} from "react";
import {FlatList, Text, TouchableOpacity, View} from "react-native";
import ToolbarMain from "../../elements/toolbar/ToolbarMain";
import {strings} from "../../../resource/languages/i18n";
import ShipHandle from "../../../sagas/ShipHandle";
import Styles from "../../../resource/Styles";
import styles from "./styles";
import ShippingUtils from "../../../Utils/ShippingUtils";
import CurrencyFormatter from "../../../Utils/CurrencyFormatter";
import {Icon} from "react-native-elements";
import ColorStyle from "../../../resource/ColorStyle";
import {Actions} from "react-native-router-flux";
import RenderLoading from "../../elements/RenderLoading";
import ViewUtils from "../../../Utils/ViewUtils";
export default class ListShippingScreen extends Component{
    constructor(props) {
        super(props);
        this.state={
            data:[],
            loading:true
        }
    }

    componentDidMount() {
        this.getData()
    }

    getData(){
        ShipHandle.getInstance().billShip(this.props.item,(isSuccess,res)=>{
            if(isSuccess){
                let mData=res.data
               let newArray  =mData.map(item=>{
                        return {
                            ...item,
                            checked:false
                        }
                })
                let data = mData.map((item,index)=>{
                    if(index===0){
                        return {
                            ...item,
                            checked:true
                        }
                    }
                })
                this.setState({data:data,loading:false})
            }

        })
    }
    onChange(item) {
        let newArray = this.state.data.map((e,index) => {
            if (item.id === e.id) {
                return {
                    ...e,
                    checked: !e.checked,
                };
            }
            return {
                ...e,
                checked: false,
            };
        });
        this.setState({data: newArray});
    }
    _renderItem =({item,index})=>{
        return(
            <TouchableOpacity
                onPress={()=>this.onChange(item)}
                style={{...styles.bodyView,flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginHorizontal:Styles.constants.X/4,marginTop:Styles.constants.X/5}}>
               <View>
                   <Text style={{...styles.textTitleShipping,marginVertical:Styles.constants.X/10}}>{ShippingUtils.checkNameShipping(item.service_type_id)}</Text>
                   <Text  style={styles.textDesShipping}>{strings('desShipping')}</Text>
                   <Text style={styles.textDesShipping}>{strings('desShipping2')}</Text>
                   <Text style={styles.textPriceShipping}>{strings('price')}:{CurrencyFormatter(item.total)}</Text>
               </View>
                <TouchableOpacity>
                    <Icon name={'check'} type={'font-awesome-5'} size={20} color={ColorStyle.tabActive} containerStyle={{display:item.checked?'flex':'none'}}/>
                </TouchableOpacity>
            </TouchableOpacity>
        )
    }
    onAccept(){
        let item
     this.state.data.map((e,index) => {
            if (e.checked) {
                return item=e
            }
            return item={
                ...e,
                "service_fee": 0,
               "short_name": "Đi bộ",
                "total": 0}
        });
        let checkShipping=this.state.data.filter(eml=>eml.checked===true)

        if(checkShipping.length !==0){
            this.props.callback(item)
            Actions.pop()
            return
        }
        ViewUtils.showAlertDialog(
            strings('pleaseSelectShipping'),
            undefined,
        );
    }
    render() {
        return (
            <View>
                <ToolbarMain
                    title={strings('selectShip')}
                    iconProfile={true}
                    iconBack={true}
                />


                {!this.state.loading && ( <FlatList
                    data={this.state.data}
                    renderItem={this._renderItem}
                    style={{width:'100%',height:'80%'}}
                />)}
                {this.state.loading && ( <RenderLoading loading={this.state.loading} styles={{width:'100%',height:'80%'}}/>)}
                <View style={styles.viewAccept}>
               <TouchableOpacity onPress={()=>this.onAccept()} style={styles.btnAccept}>
                   <Text style={{...styles.textTitleShipping,color:ColorStyle.tabWhite}}>{strings('accept')}</Text>
               </TouchableOpacity>
                </View>

            </View>
        );
    }
}
