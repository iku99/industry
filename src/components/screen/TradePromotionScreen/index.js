import React,{Component} from "react";
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import AppConstants from "../../../resource/AppConstants";
import NewsHandle from "../../../sagas/NewsHandle";
import ColorStyle from "../../../resource/ColorStyle";
import Styles from "../../../resource/Styles";
import GlobalUtil from "../../../Utils/Common/GlobalUtil";
import TradePromotionItem from "../../elements/viewItem/TradePromotionItem";
import Toolbar from "../../elements/toolbar/Toolbar";
import {strings} from "../../../resource/languages/i18n";
let selectedIndex = 0;
export default class TradePromotionScreen extends Component{
    constructor(props) {
        super(props);
        this.state={
            page:1,
            data:undefined,
            contents: [],
            loading:false,
            index:0
        }
    }

    componentDidMount() {
        this.getData()
    }
    getData(){
        NewsHandle.getInstance().getNewsCate({}, (isSuccess, res) => {
            if (isSuccess) {
                this.setState({data: res.data, loading: false},()=>{
                    this.getContent(res.data[0].id)
                });
            }
        });
    }
    getContent(id) {
        this.setState({loading: true});
        let params = {
            page_size:10,
            page_index:this.state.page,
            type:AppConstants.getNews.PROMOTION,
            category_id: id,
        };
        NewsHandle.getInstance().getNews(params, (isSuccess, res) => {
            if (isSuccess) {
                this.setState({contents: res?.data.data, loading: false});
            }
        });
    }
    onHandle(item, index) {
        this.setState({index:index, contents: []},()=>this.getContent(item?.id));

    }
    _renderItem = ({item, index}) => (
        <TouchableOpacity
            style={[index === selectedIndex ? styles.tabSelected : styles.tabNormal]}
            onPress={() => {
                this.onHandle(item, index);
            }}>
            <Text
                style={
                    index === selectedIndex
                        ? styles.itemSelectedText
                        : styles.itemNormalText
                }>
                {item.name}
            </Text>
        </TouchableOpacity>
    );
    renderTab() {
        selectedIndex = this.state.index;
        return (
            <View
                style={{
                    height: Styles.constants.X,
                    marginTop:10,
                    alignItems: 'center',
                    overflow: 'hidden',
                }}>
                <FlatList
                    ref={ref => {
                        this.flatListRef = ref;
                    }}
                    showsHorizontalScrollIndicator={false}
                    horizontal={true}
                    data={this.state.data}
                    scroll
                    renderItem={this._renderItem}
                    style={{
                        borderRadius: 15,
                        width: Styles.constants.widthScreen,
                    }}
                />
            </View>
        );
    }
    _renderContentItem = ({item}) => {
        return (
            <TradePromotionItem item = {item}/>
        )
    }
    renderContent() {
        return (
            <FlatList
                style={{
                    width: Styles.constants.widthScreen,
                }}
                numColumns={GlobalUtil.isTablet() ? 3 : 2}
                data={this.state.contents}
                renderItem={this._renderContentItem}
            />
        );
    }
    render() {
        return(
            <View style={Styles.container}>
                <Toolbar title={strings('tradePromotion')} backgroundColor={ColorStyle.tabActive} textColor={ColorStyle.tabWhite}/>
                {this.renderTab()}
                {this.renderContent()}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    tabSelected: {
        backgroundColor: ColorStyle.tabActive,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 27,
        paddingHorizontal: 16,
        borderRadius: 15,
    },
    tabNormal: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 27,
        paddingHorizontal: 16,
    },
    itemNormalText: {
        ...Styles.text.text15,
        color:ColorStyle.tabBlack
    },
    itemSelectedText: {
        ...Styles.text.text15,
        fontWeight:'700',
        backgroundColor:ColorStyle.tabActive,
        color:ColorStyle.tabWhite
    },
});
