import React ,{Component} from "react";
import NewsHandle from "../../../sagas/NewsHandle";
import {ScrollView, Text, View} from "react-native";
import {Actions} from "react-native-router-flux";
import Styles from "../../../resource/Styles";
import Toolbar from "../../elements/toolbar/Toolbar";
import ColorStyle from "../../../resource/ColorStyle";
import HTMLView from "react-native-htmlview";
import {strings} from "../../../resource/languages/i18n";

export default class TradePromotionDetail extends Component{
    constructor(props) {
        super(props);
        this.state={
            data:undefined,
            loading: true,
        }
    }
    componentDidMount() {
        this.getDta();
    }
    getDta() {
        NewsHandle.getInstance().getNewsDetail(
            this.props.id,
            (isSuccess, res) => {
                if (isSuccess) {
                    this.setState({loading: false, data: res.data});
                }
            },
        );
    }
    render() {
        return (
            <View style={{flex: 1, width: Styles.constants.widthScreen, ...Styles.container}}>
                <Toolbar title={strings('detail')} backgroundColor={ColorStyle.tabActive} textColor={ColorStyle.tabWhite}/>

                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{
                        width: Styles.constants.widthScreen,
                        alignSelf: 'center',
                    }}>
                        <Text style={{marginVertical:10,paddingHorizontal: 20,...Styles.text.text18,color:ColorStyle.tabBlack,fontWeight:'600'}}>{this.state.data?.name}</Text>
                        <HTMLView
                            value={`<body>${this.state.data?.content}<body>`}
                            stylesheet={{
                                body: {
                                    paddingHorizontal: 20,
                                    color: ColorStyle.tabBlack,
                                },
                                p: {
                                    color: ColorStyle.tabBlack, // make links coloured pink
                                },
                                a: {
                                    lineHeight: 20
                                },
                            }}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}
