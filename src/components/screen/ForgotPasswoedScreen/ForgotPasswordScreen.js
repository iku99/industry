import React, {Component} from 'react';
import {
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import {Icon} from 'react-native-elements';
import TitleLogin from '../../elements/TitleLogin';
import Styles from '../../../resource/Styles';
import {strings} from '../../../resource/languages/i18n';
import ColorStyle from '../../../resource/ColorStyle';
import DataUtils from '../../../Utils/DataUtils';
import AccountHandle from '../../../sagas/AccountHandle';
import ConfirmDialog from '../../elements/ConfirmDialog';
import {Actions} from 'react-native-router-flux';
import OtpInputs from 'react-native-otp-inputs';
import SimpleToast from "react-native-simple-toast";
import ViewUtils from "../../../Utils/ViewUtils";
let TimeReSenOTP = 120;
class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      validUser: false,
      check_phone: false,
      dialogMsg: '',
      showConfirmDialog: false,
      requestGetPassSuccess: false,
        countSelect:0,

        //
        validPass: false,
        currentPassValid: false,
        newPassValid: false,
        confirmPassValid: false,
        showPasswordNew: false,
        showPasswordConfirm: false,
        newPass: '',
        confirmPass: '',
        otp:'',
        validOtp: false,
        minute: 2,
        second: 0,
    };
  }
    timeSendOtp = () => {
        setInterval(() => {
            TimeReSenOTP = TimeReSenOTP - 1;
            if (TimeReSenOTP >= 0) {
                this.setState({
                    minute: Math.floor(TimeReSenOTP / 60 === 60 ? 0 : TimeReSenOTP / 60),
                });
                let second = TimeReSenOTP % 60;
                this.setState({second});
            }
        }, 1000);
        if (TimeReSenOTP === 0) {
            clearInterval(this.timeSendOtp());
            return;
        }
    };
    renderBody(){
      let minute = '0' + this.state.minute;
      let second =
          this.state.second < 10 ? '0' + this.state.second : this.state.second;
      let time = minute + ':' + second;
        console.log(minute === '00',second===0)
      if(this.state.countSelect===0){
          return(
              <View style={{alignItems: 'center',marginTop:20}}>
                  {this.renderTextInputWithTitle(
                      'userName',
                      'validUser',
                      strings('emailPhone'),
                      'checkEmailId',
                      () => this.checkUserName(),
                      'email-address',
                      'next',
                      input => {
                          this.storeMailRef = input;
                      },
                  )}
                  <Text
                      style={{
                          display: this.state.dialogMsg === '' ? 'none' : 'flex',
                          color: 'red',
                          textAlign: 'left',
                          marginTop: 10,
                          marginHorizontal:10,
                          width: Styles.constants.widthScreen - 70,
                      }}>
                      {this.state.dialogMsg}
                  </Text>
                  <TouchableOpacity
                      onPress={()=>this.sendOtp()}
                      disabled={!this.state.check_phone && !this.state.validUser}
                      style={[
                          Styles.button.buttonLogin,
                          {
                              backgroundColor: !this.state.check_phone && !this.state.validUser?ColorStyle.gray:ColorStyle.tabActive,
                              borderColor: !this.state.check_phone && !this.state.validUser?ColorStyle.gray:'rgba(250, 111, 52, 0.5)',
                              borderWidth: 1,
                              marginTop: 20,
                          },
                      ]}
                      animation="fadeInUpBig">
                      <Text style={[Styles.text.text18, {fontWeight: '500'}]}>
                          {strings('send')}
                      </Text>
                  </TouchableOpacity>
              </View>
          )
      }else if(this.state.countSelect===1){
          return(
              <View style={{alignItems: 'center',marginTop:20}}>

                  <Text style={{fontSize:16,color:ColorStyle.tabActive,fontWeight:'600',marginVertical:20}}>{strings('txtUserName')} {this.state.userName}</Text>
                  <OtpInputs
                      style={{
                          height: 50,
                          width: '90%',
                          flexDirection: 'row',
                      }}
                      inputStyles={{
                          borderRadius: 8,
                          borderColor: ColorStyle.tabActive,
                          borderWidth: 1.5,
                          marginRight: 5.15,
                          height: 50,
                          width: (Styles.constants.widthScreen - 40) / 6 - 3.5,
                          textAlign: 'center',
                          color:ColorStyle.tabActive
                      }}
                      handleChange={code => this.setState({otp:code})}
                      numberOfInputs={6}
                  />

                  <Text
                      style={{
                          display: this.state.dialogMsg === '' ? 'none' : 'flex',
                          color: 'red',
                          textAlign: 'left',
                          marginTop: 5,
                          width: Styles.constants.widthScreen - 40,
                      }}>
                      {this.state.dialogMsg}
                  </Text>
                  <Text style={{textAlign: 'center', marginTop: 16}}>{time}</Text>
                  <TouchableOpacity
disabled={minute === '00' && second==='00'}
                      onPress={()=>this.checkOtp()}
                      style={[
                          Styles.button.buttonLogin,
                          {
                              backgroundColor: ColorStyle.tabActive,
                              borderColor: 'rgba(250, 111, 52, 0.5)',
                              borderWidth: 1,
                              marginTop: 20,
                          },
                      ]}
                      animation="fadeInUpBig">
                      <Text style={[Styles.text.text18, {fontWeight: '500'}]}>
                          {strings('xd')}
                      </Text>
                  </TouchableOpacity>

                      <View style={{flexDirection:'row',alignItems:'center',justifyContent:'flex-start',marginTop:20}}>
                          <Text
                              style={{color: ColorStyle.tabBlack }}>
                              {strings('sendLai')}
                          </Text>
                          <TouchableOpacity onPress={()=>this.sendOtp()}>
                              <Text
                              style={{
                                  color: ColorStyle.tabActive,
                                  textAlign: 'left',
                                  fontWeight:'700',
                                  marginLeft:10
                              }}>
                              {strings('sendLai1')}
                          </Text></TouchableOpacity>
                      </View>
              </View>
          )
      }else
           return(
          <View style={{alignItems: 'center',marginTop:20}}>
              {this.renderTextInputPassword(
                  strings('place_newpass'),
                  this.state.newPass,
                  'showPasswordNew',
                  this.state.showPasswordNew,
                  text => this.checkNewPassword(text),
                  ()=>{this.setState({showPasswordNew:!this.state.showPasswordNew})}
              )}
              {this.renderTextInputPassword(
                  strings('place_cofPass'),
                  this.state.confirmPass,
                  'showPasswordConfirm',
                  this.state.showPasswordConfirm,
                  text => this.checkConfirmPassword(text),
                  ()=>{this.setState({showPasswordConfirm:!this.state.showPasswordConfirm})}
              )}
              <TouchableOpacity
                  onPress={()=>this.forgotPassword()}
                  style={[
                      Styles.button.buttonLogin,
                      {
                          backgroundColor: ColorStyle.tabActive,
                          borderColor: 'rgba(250, 111, 52, 0.5)',
                          borderWidth: 1,
                          marginTop: 20,
                      },
                  ]}
                  animation="fadeInUpBig">
                  <Text style={[Styles.text.text18, {fontWeight: '500'}]}>
                      {strings('changePassword')}
                  </Text>
              </TouchableOpacity>
          </View>
      )
  }
    checkNewPassword(newPass) {
        this.setState({
            newPassValid:
            newPass !== undefined && newPass.length > 5,
            newPass: newPass,
        });
    }
    checkConfirmPassword(confirmPass) {
        this.setState({
            confirmPassValid:
                confirmPass !== undefined &&
                confirmPass === this.state.newPass,
            confirmPass: confirmPass,
        });
    }
    sendOtp(){
      if(!this.state.check_phone && !this.state.validUser) return this.setState({dialogMsg:strings('errUserName')})
        let params={
          user_name: this.state.userName
        }
        AccountHandle.getInstance().verifyUserName(
            params,
            (isSuccess, dataResponse) => {
              console.log(dataResponse);
                if (isSuccess) {
                        this.setState({countSelect:1,dialogMsg:''},()=>{
                            this.timeSendOtp()
                        })
                        return;

                }else
                    this.setState({dialogMsg:strings('errUserNameNotServer')})
            },
        );
    }
    checkOtp(){
        if(this.state.otp.length<6) return this.setState({dialogMsg:strings('errOTP')})
        let params={
            otp: this.state.otp
        }
        AccountHandle.getInstance().checkOTP(
            params,
            (isSuccess, dataResponse) => {
                if (isSuccess) {
                    this.setState({countSelect:2,dialogMsg:''})
                    return;
                }else
                    this.setState({dialogMsg:strings('errOTP1')})
            },
        );
    }
    renderTextInputPassword(
        label,
        value,
        showPassword,
        status,
        onChangeText,
        changeStatus
    ) {
        return (
            <View
                style={{
                    backgroundColor: ColorStyle.tabWhite,
                    borderRadius: 5,
                    marginTop: 8,
                    width: Styles.constants.widthScreenMg24,
                    marginHorizontal: Styles.constants.marginHorizontalAll,
                }}>
                <TextInput
                    style={[Styles.input.inputLogin]}
                    placeholder={label}
                    value={this.state[showPassword]}
                    placeholderTextColor={ColorStyle.textInput}
                    secureTextEntry={!status}
                    onChangeText={text => {
                        onChangeText(text)
                    }}
                />
                    <TouchableOpacity style={{
                        position: 'absolute',
                        right: 10,
                        alignItems: 'flex-end',
                        bottom: '30%',}} onPress={()=>changeStatus()}>
                        <Icon
                            name={status ? 'eye' : 'eye-with-line'}
                            type="entypo"
                            size={18}
                            color={ColorStyle.tabActive}
                        />
                    </TouchableOpacity>
            </View>
        );
    }
  render() {
      let count=this.state.countSelect
    return (
      <ScrollView style={Styles.container}>
        <TitleLogin title={strings('forgotPassword')} />
          <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
              <View style={{backgroundColor:count===0?ColorStyle.tabActive:ColorStyle.tabWhite,borderColor:count!==0?ColorStyle.tabActive:ColorStyle.tabWhite,borderWidth:count!==0?1:0,width:30,height:30,borderRadius:30,alignItems:'center',justifyContent:'center'}}>
                  <Text style={{fontSize:15,fontWeight:'500',color:count===0?ColorStyle.tabWhite:ColorStyle.tabActive}}>1</Text>
              </View>
              <View style={{height:1,width:50,backgroundColor:ColorStyle.tabActive}}/>
              <View style={{backgroundColor:count===1?ColorStyle.tabActive:ColorStyle.tabWhite,borderColor:count!==1?ColorStyle.tabActive:ColorStyle.tabWhite,borderWidth:count!==1?1:0,width:30,height:30,borderRadius:30,alignItems:'center',justifyContent:'center'}}>
                  <Text style={{fontSize:15,fontWeight:'500',color:count===1?ColorStyle.tabWhite:ColorStyle.tabActive}}>2</Text>
              </View>
              <View style={{height:1,width:50,backgroundColor:ColorStyle.tabActive}}/>
              <View style={{backgroundColor:count===2?ColorStyle.tabActive:ColorStyle.tabWhite,borderColor:count!==2?ColorStyle.tabActive:ColorStyle.tabWhite,borderWidth:count!==2?1:0,width:30,height:30,borderRadius:30,alignItems:'center',justifyContent:'center'}}>
                  <Text style={{fontSize:15,fontWeight:'500',color:count===2?ColorStyle.tabWhite:ColorStyle.tabActive}}>3</Text>
              </View>
          </View>
          {this.renderBody()}
        <ConfirmDialog
          okFunc={() => {
            this.setState({showConfirmDialog: false}, () => {
              if (this.state.requestGetPassSuccess) {
                setTimeout(() => Actions.jump('main'), 100);
              }
            });
          }}
          bodyContent={this.renderBodyDialog()}
          isShowing={this.state.showConfirmDialog}
        />
      </ScrollView>
    );
  }
  renderTextInputWithTitle(
    stateName,
    validStateName,
    placeholder,
    checkId,
    checkFunc,
    keyboradType,
    returnKeyType,
    ref,
    onSubmitEditing,
  ) {
    let mValidStateName = this.state[validStateName];
    return (
      <View style={{marginTop: 10}}>
        <TextInput
          style={[Styles.input.inputLogin]}
          placeholder={placeholder}
          value={this.state[stateName]}
          placeholderTextColor={ColorStyle.textInput}
          onChangeText={text => {
            this.setState({[stateName]: text}, () => {
              if (!DataUtils.stringNullOrEmpty(checkId) && checkFunc != null) {
                clearTimeout(this[checkId]);
                this[checkId] = setTimeout(() => {
                  checkFunc();
                }, 500);
              }
            });
          }}
          ref={ref}
          onSubmitEditing={onSubmitEditing}
          returnKeyType={returnKeyType}
          // keyboardType={
          //   !DataUtils.stringNullOrEmpty(keyboradType)
          //     ? keyboradType
          //     : 'ascii-capable'
          // }
        />
        {mValidStateName != null && (
          <Icon
            name={mValidStateName ? '' : 'close'}
            type="antdesign"
            size={18}
            color={mValidStateName ? ColorStyle.tabActive : ColorStyle.red}
            containerStyle={{
              position: 'absolute',
              right: 10,
              alignItems: 'flex-end',
              display: this.state[stateName] === '' ? 'none' : 'flex',
              bottom: '30%',
            }}
          />
        )}
      </View>
    );
  }
  renderBodyDialog(){
    return(
      <Text style={{marginTop:15,marginHorizontal:10}}>{this.state.dialogMsg}</Text>
    )
  }
  checkUserName() {
    if (DataUtils.validMail(this.state.userName)) {
      return this.setState({validUser: true, check_phone: true});
    } else if (DataUtils.validPhone(this.state.userName)) {
      return this.setState({validUser: true, check_phone: true});
    } else {
      return this.setState({validUser: false});
    }
  }
  changePassword() {
        if (
            !this.state.newPassValid ||
            !this.state.confirmPassValid
        ) {
            return false;
        }
        return true
    }
  forgotPassword() {
      if(!this.changePassword()) return  SimpleToast.show(strings('registerPassword'));
    let params = {
        token: this.state.token,
        password: this.state.newPass
    };
    AccountHandle.getInstance().changePassword(
      params,
      (isSuccess, dataResponse) => {
        if (isSuccess) {
            SimpleToast.show(strings('changePasswordSuccess'))
            Actions.pop()
return
        }
        this.setState({
          requestGetPassSuccess: false,
          dialogMsg: strings('changePasswordFalse'),
          showConfirmDialog: true,
        });
      },
    );
  }
}

//lấy dữ liệu về
const mapStateToProps = state => {
  return {};
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {};
};
const ForgotPasswordScreen = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ForgotPassword);
export default ForgotPassword;
