import React, {Component} from "react";
import {FlatList, View} from "react-native";
import Styles from "../../../resource/Styles";
import ToolbarMain from "../../elements/toolbar/ToolbarMain";
import EmptyView from "../../elements/reminder/EmptyView";
import {strings} from "../../../resource/languages/i18n";
import {UIActivityIndicator} from "react-native-indicators";
import ColorStyle from "../../../resource/ColorStyle";
import NotificationHandle from "../../../sagas/NotificationHandle";
import AppConstants from "../../../resource/AppConstants";
import ItemNotidication from "../../elements/viewItem/itemNotification/ItemNotidication";
import OrderHandle from "../../../sagas/OrderHandle";
import NavigationUtils from "../../../Utils/NavigationUtils";
import ViewUtils from "../../../Utils/ViewUtils";
let LIMIT=1
export default class NotificationStore extends Component{
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            page: 0,
            canLoadData: false,
            loading: true,
        };
    }
    componentDidMount() {
        this.getNotification();
    }

    render() {
        return (
            <View style={{...Styles.container}}>
                <ToolbarMain title={this.getTitle()} iconBack={true} readAll={true} callBackReadAll={()=>this.readAllNotification()}/>
                <FlatList
                    data={this.state.data}
                    renderItem={this.renderItem}
                    onEndReachedThreshold={0.01}
                    onEndReached={() => this.handleLoadMore()}
                    keyboardShouldPersistTaps={'handle'}
                    ListFooterComponent={this.renderFooter.bind(this)}
                    showsVerticalScrollIndicator={false}
                />
                {this.state.data.length === 0 && !this.state.loading && (
                    <EmptyView
                        containerStyle={{flex: 1}}
                        text={strings('emptyNotification')}
                    />
                )}
            </View>
        );
    }
    renderFooter = () => {
        if (!this.state.loading) {
            return null;
        }
        if (this.state.canLoadData) {
            return null;
        }
        return <UIActivityIndicator color={ColorStyle.tabActive} size={20} />;
    };
    handleLoadMore() {
        if (this.state.loading) {
            return;
        }
        if (!this.state.canLoadData) {
            return;
        }
        this.setState({loading: true, page: this.state.page + 1}, () =>
            this.getNotification,
        );
    }
    readAllNotification(){
        NotificationHandle.getInstance().updateNotifications(undefined,
            (isSuccess, _) => {
                if (isSuccess) {
                    this.getCountNotification(AppConstants.typeNotification.Order,'countOrder')
                    this.setState({data:[],page:1},()=>{
                        this.getNotification();
                    });
                }
            },
        );
    }
    getTitle() {
        let type = this.props.type;
        if (type === AppConstants.typeNotification.Active) {
            return strings('active');
        } else if (type === AppConstants.typeNotification.Promotion) {
            return strings('promotion');
        } else if(type === AppConstants.typeNotification.Rate) {
            return  strings('rate');
        }
    }
    renderItem = ({item, index}) => {
        return (
            <ItemNotidication
                onNotificationClick={(item, index) =>
                    this.onNotificationClick(item, index)
                }
                index={index}
                item={item}
            />
        );
    };
    onNotificationClick(item, index) {
        if (item.status===1) {
            this.updateNotificationStatus(item, index);
        }
        this.goToOrderDetail(item.type_id);
    }
    updateNotificationStatus(item, index) {
        let param={
            id:item.id
        }
        NotificationHandle.getInstance().updateNotifications(
            param,
            (isSuccess, _) => {
                if (isSuccess) {
                    let data = this.state.data;
                    item = {
                        ...item,
                        status: true,
                    };
                    if (index >= 0 && index < data.length) {
                        data[index] = item;
                    }
                    this.props.callback()
                    this.setState({data});
                }
            },
        );
    }
    goToOrderDetail(item) {
        if (item!== undefined) {
            OrderHandle.getInstance().getOrderDetail(
                item,
                (isSuccess, dataResponse) => {
                    console.log(dataResponse)
                    if (isSuccess) {
                        NavigationUtils.goToOrderDetail(
                            dataResponse.data,
                            false,
                            null,
                            null,
                        );
                    } else {
                        ViewUtils.showAlertDialog(strings('cannotFoundOrder'));
                    }
                },
            );
        }
    }
    getNotification() {
        let params = {
            page_index: this.state.page,
            page_size: LIMIT,
            type: AppConstants.typeNotification.Order,
        };
        NotificationHandle.getInstance().getNotifications(
            params,
            (isSuccess, responseData) => {
                this.handelResponse(isSuccess, responseData);
            },
        );
    }
    handelResponse(isSuccess, responseData) {
        let data = this.state.data;
        let newData = [];
        if (
            isSuccess &&
            responseData.data !== undefined
        ) {
            newData = responseData.data;
            if (newData.length < LIMIT) {
                this.setState({canLoadData: true});
                return
            }
            data = data.concat(newData);
            this.setState({data, loading: false});
        }
    }
}
