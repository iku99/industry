import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../resource/Styles';
import {connect} from 'react-redux';
import {getCategoryAction} from '../../../actions';
import SellerStyles from './SellerStyles';
import {strings} from '../../../resource/languages/i18n';
import ColorStyle from '../../../resource/ColorStyle';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import ImageHelper from '../../../resource/images/ImageHelper';
import ToolbarSeller from "../../elements/toolbar/ToolbarSeller";
import { SceneMap, TabView } from "react-native-tab-view";
import TabDiscover from "../group/tab/TabDiscover";
import TabGroups from "../group/tab/TabGroups";
import TabCreatGroup from "../group/tab/TabCreatGroup";
import TabRelease from "./tab/TabRelease";
import ViewUtils from "../../../Utils/ViewUtils";
import AppConstants from "../../../resource/AppConstants";
import DatePicker from "react-native-date-picker";
import DateUtil from "../../../Utils/DateUtil";
class IncomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      index:  0 ,

      routes: [
        {key: 'TabToRelease', title: strings('toRelease')},
        {key: 'TabRelease', title: strings('released')},
      ],
    }
  }
  _handleIndexChange = index => this.setState({index});

  _renderScene = SceneMap({
    TabToRelease: () => (<TabRelease type={AppConstants.SELLER_INCOME.TO_RELEASE}/>),
    TabRelease: () => (<TabRelease type={AppConstants.SELLER_INCOME.RELEASE} />),
  });
  _renderTabBar = props => {
    return ViewUtils.renderTab(props,ColorStyle.tabWhite, this.state.index, i => {
      this.setState({index: i});
    });
  };
  render() {
    return (
      <View style={Styles.container}>

      <ToolbarSeller title={strings('myIncome')} message={true} onPressMess={()=>{}}/>
        {this._renderTabContent()}

      </View>
    );
  }
  _renderTabContent() {
    return (
      <TabView
        navigationState={this.state}

        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
      />
    );
  }

}
const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    onGetCategory: param => {},
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(IncomeScreen);
