import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../resource/Styles';
import {connect} from 'react-redux';
import {getCategoryAction} from '../../../actions';
import SellerStyles from './SellerStyles';
import {strings} from '../../../resource/languages/i18n';
import ColorStyle from '../../../resource/ColorStyle';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import ImageHelper from '../../../resource/images/ImageHelper';
import ToolbarSeller from '../../elements/toolbar/ToolbarSeller';
import {SceneMap, TabView} from 'react-native-tab-view';
import TabDiscover from '../group/tab/TabDiscover';
import TabGroups from '../group/tab/TabGroups';
import TabCreatGroup from '../group/tab/TabCreatGroup';
import TabRelease from './tab/TabRelease';
import ViewUtils from '../../../Utils/ViewUtils';
import AppConstants from '../../../resource/AppConstants';
import DatePicker from 'react-native-date-picker';
import DateUtil from '../../../Utils/DateUtil';
import TabRating from './tab/TabRating';
import RatingStar from '../../RatingStar';
class RatingShop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      countRating: '5.0',
      index: 0,
      routes: [
        {key: 'TabShop', title: strings('ratingShop')},
        {key: 'TabBuyer', title: strings('ratingBuyer')},
      ],
    };
  }
  _handleIndexChange = index => this.setState({index});

  _renderScene = SceneMap({
    TabShop: () => <TabRating type={AppConstants.typeRating.SHOP} callback={(text)=>{this.setState({countRating:text})}}/>,
    TabBuyer: () => <TabRating type={AppConstants.typeRating.USER} callback={(text)=>{this.setState({countRating:text})}}/>,
  });
  _renderTabBar = props => {
    return ViewUtils.renderTab(
      props,
      ColorStyle.tabWhite,
      this.state.index,
      i => {
        this.setState({index: i});
      },
    );
  };
  render() {
    return (
      <View style={Styles.container}>
        {this.renderBanner()}
        <ToolbarSeller
          title={strings('ratingShop')}
          message={true}
          onPressMess={() => {}}
        />
      </View>
    );
  }
  renderBanner() {
    return (
      <View
        style={{
          position: 'absolute',
          top: Styles.constants.X * 1.5,
          width: Styles.constants.widthScreen,
          height: Styles.constants.heightScreen + 20,
        }}>
        <View
          style={{
            backgroundColor: '#A7CDCC',
            alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 20,
            borderColor: ColorStyle.borderItemHome,
            elevation: 2,
          }}>
          <View
            style={{
              ...Styles.icon.avatarGroup,
              borderWidth: 2,
              borderColor: ColorStyle.tabWhite,
              alignItems: 'center',
              justifyContent: 'center',
              marginVertical: 10,
            }}>
            <Text style={{...Styles.text.text18}}>
              {this.state.countRating}
            </Text>
          </View>
          <RatingStar size={15} rating={this.state.countRating} />
        </View>
        {this._renderTabContent()}
      </View>
    );
  }
  _renderTabContent() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
      />
    );
  }
}
const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    onGetCategory: param => {},
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(RatingShop);
