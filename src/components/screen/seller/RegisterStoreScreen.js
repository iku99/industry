import React from 'react';
import {
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import Styles from '../../../resource/Styles';
import {connect} from 'react-redux';
import { getSeller} from '../../../actions';
import {strings} from '../../../resource/languages/i18n';
import ColorStyle from '../../../resource/ColorStyle';
import NavigationUtils from '../../../Utils/NavigationUtils';
import Toolbar from '../../elements/toolbar/Toolbar';
import SellerStyles from './SellerStyles';
import ViewUtils from '../../../Utils/ViewUtils';
import SimpleToast from 'react-native-simple-toast';
import DataUtils from '../../../Utils/DataUtils';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import GroupStyle from "../group/GroupStyle";
import StoreHandle from "../../../sagas/StoreHandle";
import {EventRegister} from "react-native-event-listeners";
import AppConstants from "../../../resource/AppConstants";
import GlobalInfo from "../../../Utils/Common/GlobalInfo";
import ShipHandle from "../../../sagas/ShipHandle";
class RegisterStoreScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            storeName: '',
            storeAdd: '',
            storeEmail: '',
            storePhone: '',
            shortDes: '',
            validName: false,
            validAdd: false,
            validEmail: false,
            validPhone: false,
            businessTypeIndex: 0,
            showDialog: false,
        };
    }
    componentDidMount() {
        let data=GlobalInfo.userInfo
        this.setState({storeEmail:data.email},()=>this.checkEmail())
    }

    render() {
        return (
            <View style={Styles.container}>
                <Toolbar
                    title={strings('shopInfo')}
                    backgroundColor={ColorStyle.tabActive}
                    textColor={ColorStyle.tabWhite}
                />
                <ScrollView>
                    {this.renderViewName()}
                    {this.renderViewOther()}
                </ScrollView>
                {this.renderBottom()}
                {this.state.showDialog && this.renderDialog()}
            </View>
        );
    }
    renderDialog() {
        return (
            <View
                style={{
                    backgroundColor: 'rgba(0,0,0,0.5)',
                    position: 'absolute',
                    top: 0,
                    length: 0,
                    width: '100%',
                    height: '100%',
                    paddingHorizontal: '3%',
                    justifyContent: 'center',
                    alignItems: 'center',elevation: 3,
                    zIndex:2
                }} >
                <View style={{...GroupStyle.modal.container,paddingHorizontal:Styles.constants.marginHorizontalAll,paddingVertical:30}} >
                    <Text style={{...Styles.text.text20,color:ColorStyle.tabBlack,fontWeight:'700',textAlign:'center'}}>{strings('finishShop').replace('{name}',this.state.storeName)}</Text>
                    <TouchableOpacity onPress={()=>{
                        setTimeout(()=>{ Actions.jump('sellerDetail')},5)
                        Actions.reset('drawer')
                    }} style={{paddingHorizontal:16,paddingVertical:10,alignSelf:'center',borderColor:ColorStyle.tabActive,borderWidth:1,borderRadius:5,marginTop:20}}>
                        <Text style={{...Styles.text.text16,color:ColorStyle.tabActive}}>{strings('goShop')}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
    renderBottom() {
        return (
            <View
                style={{
                    ...SellerStyles.renderView,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: 0,
                }}>
                <TouchableOpacity
                    style={{
                        ...SellerStyles.btnNext,
                        backgroundColor: ColorStyle.tabWhite,
                        borderColor: 'rgba(151, 151, 151, 0.3)',
                        elevation: 2,
                    }}>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabActive,
                            fontWeight: '500',
                        }}>
                        {strings('back').toUpperCase()}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => this.registerStore()}
                    style={{
                        ...SellerStyles.btnNext,
                        backgroundColor: ColorStyle.tabActive,
                        borderColor: 'rgba(211, 38, 38, 0.25)',
                        elevation: 2,
                    }}>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabWhite,
                            fontWeight: '500',
                        }}>
                        {strings('next').toUpperCase()}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
    renderViewName() {
        return (
            <View style={SellerStyles.renderView}>
                <Text
                    style={{
                        ...Styles.text.text18,
                        color: ColorStyle.tabBlack,
                        fontWeight: '500',
                    }}>
                    {strings('nameStore')}
                </Text>
                {this.renderTextInputWithName(
                    strings('name'),
                    'storeName',
                    'validName',
                    'checkAddId',
                    () => this.checkName(),
                    '',
                    'next',
                    input => {
                        this.addressRef = input;
                    },
                    () => {},
                )}
                {this.renderDesProduct(strings('introduce'),'shortDes','validDes','checkAddId',)}
            </View>
        );
    }
    renderDesProduct( title,
                      stateName,
                      validStateName,
                      checkId,
                      checkFunc,) {
        return (
            <View>
                <Text style={{...Styles.text.text18,color:ColorStyle.tabBlack,fontWeight:'500',marginVertical:10}}>
                    {title}*
                </Text>
                <TextInput
                    style={{
                        ...Styles.input.codeInput,
                        width:Styles.constants.widthScreenMg24,
                        height:Styles.constants.X*2,
                        paddingVertical: 10,
                        borderWidth: 0.5,
                        paddingLeft: 10,
                        borderColor: '#BABABA',
                        color:ColorStyle.tabBlack
                    }}
                    textAlignVertical={'top'}
                    value={this.state[stateName]}
                    placeholder={title}
                    // maxLength={3000}
                    multiline
                    numberOfLines={5}
                    editable
                    onChangeText={text => {
                        this.setState({[stateName]: text}, () => {
                            if (!DataUtils.stringNullOrEmpty(checkId) && checkFunc != null) {
                                clearTimeout(this[checkId]);
                                this[checkId] = setTimeout(() => {
                                    checkFunc();
                                }, 500);
                            }
                        });
                    }}
                />
            </View>
        );
    }
    renderViewOther() {

        return (
            <View style={SellerStyles.renderView}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}>
                    <Text
                        style={{
                            flex: 1,
                            ...Styles.text.text14,
                            color: ColorStyle.tabBlack,
                            fontWeight: '500',
                        }}>
                        {strings('address1')}
                        <Text
                            style={{
                                ...Styles.text.text14,
                                color: ColorStyle.tabActive,
                                fontWeight: '500',
                                paddingLeft: 10,
                            }}>
                            *
                        </Text>
                    </Text>
                    <TouchableOpacity
                        onPress={() =>
                            NavigationUtils.goToAddressList(item => {
                                this.setState({storeAdd: item.address_detail,storePhone:item.phone},()=>this.checkPhone());
                            },true)
                        }
                        style={{
                            ...Styles.input.codeInput,
                            flex: 2,
                            borderWidth: 0.5,
                            elevation: 2,
                            borderColor: '#BABABA',
                            flexDirection: 'row',
                        }}>
                        <Text
                            style={{
                                ...Styles.text.text11,
                                color: ColorStyle.gray,
                                flex: 3,
                            }}
                            numberOfLines={1}>
                            {this.state.storeAdd?.address}
                        </Text>
                        <Icon
                            name={'right'}
                            type="antdesign"
                            size={15}
                            color={ColorStyle.gray}
                            style={{marginRight: 10}}
                        />
                    </TouchableOpacity>
                </View>

                {this.renderTextInputWithTitle(
                    strings('email'),
                    'storeEmail',
                    'validEmail',
                    'checkAddId',
                    () => this.checkEmail(),
                    undefined
                )}
                {this.renderTextInputWithTitle(
                    strings('phone'),
                    'storePhone',
                    'validPhone',
                    'checkAddId',
                    () => this.checkPhone(),
                    'number-pad'
                )}
            </View>
        );
    }
    renderTextInputWithName(
        title,
        stateName,
        validStateName,
        checkId,
        checkFunc,
    ) {
        return (
            <View
                style={{
                    ...Styles.input.codeInput,
                    paddingRight: 30,
                    paddingVertical: 0,
                    marginTop: 15,
                    borderWidth: 0.5,
                    elevation: 2,
                    borderColor: '#BABABA',
                }}>
                <TextInput
                    style={{}}
                    value={this.state[stateName]}
                    placeholder={title}
                    maxLength={30}
                    onChangeText={text => {
                        this.setState({[stateName]: text}, () => {
                            if (!DataUtils.stringNullOrEmpty(checkId) && checkFunc != null) {
                                clearTimeout(this[checkId]);
                                this[checkId] = setTimeout(() => {
                                    checkFunc();
                                }, 500);
                            }
                        });
                    }}
                />
                <Text style={{position: 'absolute', right: 5, bottom: 15}}>
                    {this.state[stateName] === undefined
                        ? 0
                        : this.state[stateName].length}
                    /30
                </Text>
            </View>
        );
    }
    renderTextInputWithTitle(
        title,
        stateName,
        validStateName,
        checkId,
        checkFunc,
        keyboardType
    ) {
        let mValidStateName = this.state[validStateName];
        return (
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 16,
                }}>
                <Text
                    style={{
                        flex: 1,
                        ...Styles.text.text14,
                        color: ColorStyle.tabBlack,
                        fontWeight: '500',
                    }}>
                    {title}
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabActive,
                            fontWeight: '500',
                        }}>
                        *
                    </Text>
                </Text>
                <TextInput
                    style={{
                        ...Styles.input.codeInput,
                        flex: 2,
                        borderWidth: 0.5,
                        elevation: 2,
                        paddingLeft: 10,
                        borderColor: '#BABABA',
                    }}
                    value={this.state[stateName]}
                    placeholder={title}
                    maxLength={30}
                    keyboardType={keyboardType===undefined?'email-address':keyboardType}
                    onChangeText={text => {
                        this.setState({[stateName]: text}, () => {
                            if (!DataUtils.stringNullOrEmpty(checkId) && checkFunc != null) {
                                clearTimeout(this[checkId]);
                                this[checkId] = setTimeout(() => {
                                    checkFunc();
                                }, 500);
                            }
                        });
                    }}
                />
            </View>
        );
    }
    registerStore() {
        console.log(this.state.storeAdd)
        if (this.state.storeAdd === '') {
            ViewUtils.showAlertDialog(strings('valAddress'));
            return;
        }
        if(!this.state.validName){
            SimpleToast.show(strings('dataInvalid'), SimpleToast.SHORT);
            return;
        }
        if(!this.state.validPhone){
            SimpleToast.show(strings('errPhone'), SimpleToast.SHORT);
            return;
        }
        if(!this.state.validEmail){
            SimpleToast.show(strings('errEmail'), SimpleToast.SHORT);
            return;
        }
        let param={
            name: this.state.storeName,
            des: this.state.shortDes,
            address: this.state.storeAdd,
            email: this.state.storeEmail,
            phone: this.state.storePhone,
        }

        StoreHandle.getInstance().createStore(param,(isSuccess, responseData) => {
            console.log("responseData:",responseData)
            if(isSuccess && responseData.code ===0){
                let params={
                    district_id: this.state.storeAdd.district.ghn_id,
                    ward_code: this.state.storeAdd.ward.ghn_id,
                    name: this.state.storeName,
                    phone: this.state.storePhone,
                    address: this.state.storeAdd.address
                }
                ShipHandle.getInstance().getShip(responseData.data.id,params,
                    (isSuccess, responseData) => {
                        console.log(responseData,params)
                        if (isSuccess) {
                            this.setState({showDialog: true});
                            EventRegister.emit(AppConstants.EventName.ADD_NEW_STORE, true);
                        }})

            }else {
                SimpleToast.show(strings('createStoreFailed'), SimpleToast.SHORT)
            }
        })
        // this.props.onGetSeller({
        //   name: this.state.storeName,
        //   des: this.state.shortDes,
        //   address: this.state.storeAdd,
        //   email: this.state.storeEmail,
        //   phone: this.state.storePhone,
        // });
        // Actions.jump('transportList');
    }

    registerShip(){

    }
    checkAddress() {
        return this.setState(
            {validAdd: this.state.storeAdd !==undefined},
            () => {
                // this.searchOnMap()
            },
        );
    }
    checkName() {
        return this.setState({validName: this.state.storeName.trim().length > 5});
    }
    checkEmail() {
        return this.setState({
            validEmail: DataUtils.validMail(this.state.storeEmail),
        });
    }
    checkPhone() {
        return this.setState({
            validPhone: DataUtils.validPhone(this.state.storePhone),
        });
    }
}
const mapStateToProps = state => {
    return { seller: state.SellerReducers,};
};
const mapDispatchToProps = dispatch => {
    return {
        onGetSeller: param => {
            dispatch(getSeller(param));
        },
    };
};
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(RegisterStoreScreen);
