import Styles from '../../../resource/Styles';
import ColorStyle from '../../../resource/ColorStyle';
import Colors from "../../../resource/ColorStyle";

const X = Styles.constants.X;

export default {
  toolbar: {
    container: {
      height: Styles.constants.heightScreen / 5,
      backgroundColor: ColorStyle.tabActive,
      borderBottomLeftRadius: 40,
      borderBottomRightRadius: 40,
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
  codeInput: {
    height:Styles.constants.X,
    borderRadius: 3,
    marginHorizontal: 10,
    ...Styles.input.codeInput,
    paddingVertical: 10,
    paddingLeft: 10,
    borderWidth: 1,
  },
  itemRow:{
    flexDirection:'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  bg: {
    width: Styles.constants.widthScreenMg24,
    height: Styles.constants.heightScreen / 3,
    alignSelf: 'center',
    marginTop: X,
  },
  textWelcom: {
    ...Styles.text.text18,
    color: ColorStyle.tabBlack,
    fontWeight: '700',
    maxWidth: Styles.constants.widthScreenMg24,
    alignSelf: 'center',
    textAlign: 'center',
    marginTop: Styles.constants.marginHorizontalAll,
  },
  buttonRe: {
    ...Styles.input.codeInput,
    backgroundColor: ColorStyle.tabActive,
    marginHorizontal: Styles.constants.marginHorizontalAll,
    alignItems: 'center',
    justifyContent:'center',
    paddingVertical:0,
    marginTop: X * 1.9,
  },
  renderView: {
    backgroundColor: ColorStyle.tabWhite,
    paddingHorizontal:X/4,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,

    elevation: 2,
    marginVertical: 5,
    paddingVertical: X/6,
  },
  btnNext: {
    width: '45%',
    paddingVertical: 14,
    alignItems: 'center',
    borderRadius: 25,

  },
  renderViewSellerDetail: {
    backgroundColor: ColorStyle.tabWhite,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    marginVertical: X/4,
  },
  heartView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: X/5,
    paddingHorizontal: Styles.constants.marginHorizontalAll,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
backgroundColor:ColorStyle.tabWhite,
  },
  viewItem: {
    paddingVertical: 12,
    paddingHorizontal: Styles.constants.marginHorizontalAll,
    backgroundColor: ColorStyle.tabWhite,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: ColorStyle.ba,
    alignItems: 'center',
  },
  imageItem: {
    width: X * 2,
    height: X * 2,
    alignItems: 'center',
    justifyContent:'center',
    borderRadius: 10,
  },
  btnImage: {
    width: X * 2,
    height: X * 2,
    alignItems: 'center',
    justifyContent:'center',
    borderStyle: 'dashed',
    borderWidth: 1,
    borderRadius: 10,
    borderColor: ColorStyle.tabActive,
  },
  text:{
    textTitle:{
      ...Styles.text.text16,
      color:ColorStyle.tabBlack,
      fontWeight: '500',
    },
    textParameter:{
      ...Styles.text.text16,
      color:ColorStyle.gray,
      fontWeight: '500',
    }
  },
  itemClass:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 0,
    alignItems:'center',
    borderBottomColor:ColorStyle.borderItemHome,
    borderBottomWidth:1,paddingVertical:10
  },
  itemWeight:{
    width:Styles.constants.widthScreenMg24/2-10,
    flexDirection:'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  viewAvatar:{
    position:'absolute',
    top: Styles.constants.heightScreen / 4.5,
    alignSelf:'center',
    borderRadius: 500,
    borderColor:ColorStyle.borderItemHome,
    borderWidth:1,
  overflow:'hidden'
  },
  img_avatar: {
    ...Styles.icon.img_avatar,
    backgroundColor:ColorStyle.tabWhite,

    borderColor: ColorStyle.borderItemHome,
    elevation: 2,
    shadowColor: ColorStyle.borderItemHome,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 1,
  },
  btnSettingCover:{
    position:'absolute',
    bottom:0,
    right:0,
    backgroundColor:ColorStyle.tabWhite,
    padding:10,
    borderTopLeftRadius:Styles.constants.X,
    elevation: 2,
    shadowColor: ColorStyle.borderItemHome,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 1,
  },
  renderBody:{
    paddingHorizontal:Styles.constants.marginHorizontal20,marginTop:Styles.constants.X*2
  },
  title:{
      ...Styles.text.text14,
    color:ColorStyle.tabBlack,
    marginBottom: X/8
  },
  viewOptions:{
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'center',
    borderStyle: 'dashed',
    borderWidth:1,
    borderColor:ColorStyle.tabBlack,
    paddingVertical:X/5,
    marginTop:X/2
  },
  bodyOption:{
    backgroundColor:ColorStyle.tabWhite,
    padding: X/5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    marginVertical:X/3,
    elevation: 3,
  },
  btnClose:{
    position:'absolute',
    top:X/7,
    right: X/7
  },
  itemOptions:{
    marginVertical:X/3,
  },
  btnOptions:{
    backgroundColor:ColorStyle.tabActive,
    paddingVertical:X/5,
    alignSelf:'flex-end',
    alignItems:'center',
    paddingRight:10,
    marginTop:10,
    width:'40%',
    borderRadius:X/5
  },
  viewStoreItem:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    padding: X/4,
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    backgroundColor: ColorStyle.tabWhite,
    elevation: 3,
  },
  itemProduct:{
    flexDirection: 'row',
    justifyContent: 'flex-start',
    margin: 10,
    paddingVertical: 10,
  },
  bodyProduct:{
    paddingHorizontal: X/4,
  },
  bodyItemDetail:{
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    paddingHorizontal: Styles.constants.marginHorizontalAll,
    paddingVertical: Styles.constants.X / 2,
    backgroundColor: ColorStyle.tabWhite,
    flex: 1,
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    flexDirection: 'row',
    alignItems: 'center',
    elevation: 2,
  },

  logoStore:{
    ...Styles.icon.img_comment,
    borderRadius: 500,
    marginRight: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  }
};
