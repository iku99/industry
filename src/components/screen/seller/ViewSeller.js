import React from 'react';
import {FlatList, Text, TouchableOpacity, View} from "react-native";
import AppConstants from "../../../resource/AppConstants";
import StoreInfoRow from "../store/row/StoreInfoRow";
import StoreTabRow from "../store/row/StoreTabRow";
import StoreHandle from "../../../sagas/StoreHandle";
import Styles from "../../../resource/Styles";
import ToolbarSearch from "../../elements/toolbar/ToolbarSearch";
import {Actions} from "react-native-router-flux";
import {Icon} from "react-native-elements";
import {strings} from "../../../resource/languages/i18n";
import ColorStyle from "../../../resource/ColorStyle";
import ProductHandle from "../../../sagas/ProductHandle";
const defaulData = [
    {type: AppConstants.listStoreDetail.STORE_INFO},
    {type: AppConstants.listStoreDetail.TAB_INFORMATION},
];
export default class ViewSeller extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            data: [...defaulData],
            loading: false,
            page: 0,
            canLoadData: true,
            store: {},
            storeId: this.props.storeId,
            userInfo: {},
            coverList: [],
            countRate: 5,
            countAvg: 0,
            productCount: 0,
            tabOptionIndex: 1,
            orderText: strings('storeOrder'),
            sortValue: undefined,
            showSort: false,
            showCategory: false,
            categories: [],
            category: undefined,
            region: {
                latitude: AppConstants.MAP.LATITUDE,
                longitude: AppConstants.MAP.LONGITUDE,
                latitudeDelta: AppConstants.MAP.COORDINATE_DELTA,
                longitudeDelta: AppConstants.MAP.COORDINATE_DELTA,
            },
        }
    }
    componentDidMount() {
        this.getStoreDetail(this.props.storeId);
        this.getProductList();
    }
    render() {
        return(
            <View style={Styles.container}>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.data}
                    extraData={this.state}
                    style={{
                        position: 'absolute',
                        top: 20,
                        width: '100%',
                        height: '100%',
                    }}
                    keyExtractor={this.keyExtractor}
                    renderItem={this._renderItem}/>
                {this.toolbar()}
            </View>
        )
    }
    toolbar(){
        return(
            <View
                style={[
                    {
                        paddingTop: Styles.constants.X,
                        paddingBottom: Styles.constants.X * 0.2,
                        paddingHorizontal: Styles.constants.X * 0.65,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: ColorStyle.tabWhite,
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20,
                        shadowColor: ColorStyle.borderButton,
                        shadowOffset: { width: 0, height: 1 },
                        shadowOpacity: 0.8,
                        shadowRadius: 1,
                    },
                ]}>
                <Text
                    style={
                        {
                            ...  Styles.text.text20, color: ColorStyle.tabBlack,
                            fontWeight: '700',
                            maxWidth: '60%',
                            textAlign: 'center',
                            alignSelf:'center',
                        }
                    }
                    numberOfLines={1}>
                    {strings('view')}
                </Text>
                <TouchableOpacity
                    onPress={() => Actions.pop()}
                    style={{
                        borderRadius: 200,
                        position: 'absolute',
                        paddingVertical: Styles.constants.X ,
                        paddingLeft: Styles.constants.X * 0.4,
                        top: 0,
                        left: 0,
                    }}>
                    <Icon
                        name="left"
                        type="antdesign"
                        size={25}
                        color={ColorStyle.tabActive}
                    />
                </TouchableOpacity>
            </View>
        )
    }

    _renderItem=({item,index})=>{
            let store = this.state.store;
            switch (item.type) {
                case AppConstants.listStoreDetail.STORE_INFO:
                    return <StoreInfoRow item={store}/>;
                case AppConstants.listStoreDetail.TAB_INFORMATION:
                    return <StoreTabRow item={store} />;
                case AppConstants.listStoreDetail.END:
                    return <View style={{marginBottom: 10}} />;
            }
    }
    getStoreDetail(storeId) {
        StoreHandle.getInstance().getStoreDetail(storeId, (isSuccess, store) => {
            if (isSuccess) {
                if (store === undefined) {
                    store = {};
                }
                this.setState({store});

            }
        });
    }
    getProductList() {
        let param = {
            current_page: this.state.page,
            page_size: 5,
        };
        ProductHandle.getInstance().getProductStore(
            param,this.state.storeId,
            (isSuccess, responseData) => {
                if (isSuccess) {
                    if (responseData.data.length < 5) {
                        this.setState({canLoadData: false});
                    }
                    let listData = this.state.data;
                    let abc = {
                        data: responseData.data,
                        type: AppConstants.listStoreDetail.PRODUCT,
                    };
                    listData = [...listData, ...[abc]];
                    this.setState({
                        loading: false,
                        loaded: true,
                        data: listData,
                    });
                }
            },
        );

    }
}
