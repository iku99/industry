import React from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../resource/Styles';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import ColorStyle from '../../../resource/ColorStyle';
import {strings} from '../../../resource/languages/i18n';
import CartUtils from '../../../Utils/CartUtils';
import OrderStyles from '../order/OrderStyles';
import MyFastImage from '../../elements/MyFastImage';
import ProductUtils from '../../../Utils/ProductUtils';
import CurrencyFormatter from '../../../Utils/CurrencyFormatter';
import DateUtil from '../../../Utils/DateUtil';
import RatingStar from '../../RatingStar';
import AppConstants from '../../../resource/AppConstants';
import NavigationUtils from '../../../Utils/NavigationUtils';
import ViewUtils from "../../../Utils/ViewUtils";
import DataUtils from "../../../Utils/DataUtils";
import constants from "../../../Api/constants";
import MediaUtils from "../../../Utils/MediaUtils";
import OrderUtils from "../../../Utils/OrderUtils";
let cartInfos = [];
export default class OrderDetailSeller extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            order: this.props.order,
            store: undefined,
        };
    }

    componentDidMount() {
    }
    toolbar() {
        return (
            <View
                style={[
                    {
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        backgroundColor: ColorStyle.tabActive,
                        paddingTop: Styles.constants.X  ,
                        paddingBottom: Styles.constants.X * 0.2,
                        paddingHorizontal: Styles.constants.X * 0.65,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20,
                    },
                ]}>
                <Text
                    style={[
                        Styles.text.text20,
                        {
                            color: ColorStyle.tabWhite,
                            fontWeight: '700',
                            width: '100%',
                            top: 0,
                            textAlign: 'center',
                        },
                    ]}
                    numberOfLines={1}>
                    {strings('orderDetail')}
                </Text>
                <TouchableOpacity
                    onPress={() => Actions.pop()}
                    style={{
                        borderRadius: 200,
                        position: 'absolute',
                        paddingVertical: Styles.constants.X  ,
                        paddingHorizontal: Styles.constants.X * 0.4,
                        top: 0,
                        left: 0,
                    }}>
                    <Icon
                        name="left"
                        type="antdesign"
                        size={25}
                        color={ColorStyle.tabWhite}
                    />
                </TouchableOpacity>
            </View>
        );
    }
    renderTextRow(title,value){
        return(
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text
                    style={{
                        flex: 3,
                        ...Styles.text.text11,
                        color: ColorStyle.tabBlack,
                    }}>
                    {title}
                </Text>
                <Text
                    style={{
                        flex: 1,
                        ...Styles.text.text11,
                        color: ColorStyle.tabBlack,
                        textAlign: 'right',
                    }}>
                    {CurrencyFormatter(value)}
                </Text>
            </View>
        )
    }
    render() {
        return (
            <View style={Styles.container}>
                <ScrollView showsVerticalScrollIndicator={false} >
                    {this.renderStatusOrder()}
                    {this.renderTrustUser()}
                    {this.renderAddressView(this.state.order)}
                    {this.renderProductView()}
                    {this.renderPaymentView()}
                    {this.renderShipView()}
                    <View style={{marginBottom:20}}>
                        {this.renderButton()}
                    </View>
                </ScrollView>
                {this.toolbar()}
            </View>
        );
    }
    renderStatusOrder() {
        let order = this.state.order;
        return (
            <View
                style={{
                    backgroundColor: '#546E7A',
                    marginTop: Styles.constants.X,
                    paddingTop: Styles.constants.X  ,
                    paddingBottom: Styles.constants.X * 0.4,
                    flexDirection: 'row',
                    paddingHorizontal: Styles.constants.marginHorizontal20,
                }}>
                <Icon
                    name={'message-text-outline'}
                    type={'material-community'}
                    size={25}
                    color={ColorStyle.tabWhite}
                />
                <View style={{paddingHorizontal: Styles.constants.marginHorizontal20}}>
                    <Text
                        style={{
                            ...Styles.text.text12,
                            color: ColorStyle.tabWhite,
                            fontWeight: '500',
                        }}>
                        {CartUtils.getNotificationsOrder(order.status)}
                    </Text>
                    <Text
                        style={{
                            ...Styles.text.text12,
                            color: ColorStyle.tabWhite,
                            fontWeight: '500',
                        }}>
                        {CartUtils.getDesNotiOrder(order.status, order.created_date)}
                    </Text>
                    <Text
                        style={{
                            ...Styles.text.text12,
                            color: ColorStyle.tabWhite,
                            fontWeight: '500',
                            marginTop: 10,
                        }}>
                        {CartUtils.getTimeNotiOrder(order.status , order.created_date)}
                    </Text>
                </View>
            </View>
        );
    }
    renderAddressView(order) {
        let address =order.address;
        return (
            <View style={OrderStyles.viewBody}>
                <View
                    style={[
                        {
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingBottom: 10,
                        },
                    ]}>
                    <Icon name="location" type="octicon" size={15} />
                    <View style={{flex: 1, marginLeft: 10}}>
                        <Text
                            style={[
                                Styles.text.text16,
                                {
                                    fontWeight: '500',
                                    color: ColorStyle.tabBlack,
                                },
                            ]}>
                            {strings('deliveryAddress')}
                        </Text>
                    </View>
                </View>
                <View >
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabBlack,
                            lineHeight: 20,
                        }}>
                        {address.name} | {address.phone}
                    </Text>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.gray,
                            marginTop: 5,
                            lineHeight: 22,
                        }}>
                        {address?.address_detail.address}
                    </Text>
                </View>
            </View>
        );
    }
    renderShipView() {
        let dateOrder=this.state.order.delivery_progress;

        let viewDateOrder=dateOrder.map((item,index)=>{
            return(
                <View key={`listPay${index.toString()}`} style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginVertical:5}}>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabBlack,
                            fontWeight: '500',
                        }}
                    >{OrderUtils.getText(item.status)}</Text>
                    <Text style={{fontSize:14,color:ColorStyle.gray}}>{DateUtil.formatDate('DD/MM/YYYY HH:mm ', item.time)}</Text>
                </View>
            )
        });
        return (
            <View style={OrderStyles.viewBody}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                    }}>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabBlack,
                            fontWeight: '500',
                        }}>
                        {strings('orderId')}
                    </Text>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabBlack,
                            fontWeight: '500',
                        }}>
                        {this.state.order.code}
                    </Text>
                </View>
                {viewDateOrder}
            </View>
        )
    }
    renderProductView() {
        let order = this.state.order;
        cartInfos = order.details;
        let total = 0;
        let numProduct = 0;
        if (cartInfos === undefined) {
            cartInfos = [];
        }
        let productInfoView = cartInfos.map((cardInfo, i) => {
            if (cardInfo.product == null) {
                return;
            }
            let product = cardInfo.product;
            total = total + product.price * cardInfo.quantity;
            numProduct = numProduct + cardInfo.quantity;

            return (
                <View key={`ItemOrderSeller_${i.toString()}`} style={{marginVertical: 16}}>
                    <TouchableOpacity
                        style={OrderStyles.itemProduct}
                        onPress={() => {}}>
                        <MyFastImage
                            style={{flex: 1, height: 70}}
                            source={{
                                uri: ProductUtils.getImages(product)[0],
                                headers: {Authorization: 'someAuthToken'},
                            }}
                            resizeMode={'cover'}
                        />
                        <View style={{flex: 3}}>
                            <Text
                                style={[
                                    Styles.text.text14,
                                    {
                                        fontWeight: '500',
                                        marginHorizontal: 20,
                                    },
                                ]}>
                                {product.name}
                            </Text>
                            <View
                                style={[{flexDirection: 'row', justifyContent: 'flex-end'}]}>
                                <Text
                                    style={[
                                        Styles.text.text11,
                                        {
                                            fontWeight: '400',
                                            color: ColorStyle.gray,
                                            textAlign: 'right',
                                        },
                                    ]}>{`x ${cardInfo.quantity}`}</Text>
                            </View>
                            <View
                                style={[{flexDirection: 'row', justifyContent: 'flex-end'}]}>
                                <Text
                                    style={[
                                        Styles.text.text11,
                                        {
                                            fontWeight: '400',
                                            textAlign: 'right',
                                            color: ColorStyle.tabActive,
                                        },
                                    ]}>
                                    {CurrencyFormatter(product.price)}
                                </Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            );
        });
        return (
            <View style={{...OrderStyles.viewBody,padding:0}}>
                <View
                    style={OrderStyles.viewStoreItem}>
                    <TouchableOpacity style={{flexDirection: 'row',alignItems:'center'}}>
                        <Image
                            source={ MediaUtils.getAvatar(order.customer)}
                            style={{...Styles.icon.iconOrder, borderRadius: 50}}
                        />
                        <Text
                            style={[
                                Styles.text.text14,
                                {
                                    fontWeight: '500',
                                    color: ColorStyle.tabBlack,
                                    marginLeft: 10,
                                },
                            ]}
                            numberOfLines={1}>
                            {order.customer.full_name}
                        </Text>
                    </TouchableOpacity>
                </View>
                {productInfoView}
                <View style={{...OrderStyles.bodyProduct,borderTopWidth:1,borderTopColor:ColorStyle.gray,paddingTop:10}}>
                    {this.renderTextRow(strings('items'),order?.discount_detail.product_price)}
                    {this.renderTextRow(strings('priceShipping'),order?.discount_detail.shipping_fee)}
                    {this.renderTextRow(strings('voucher'),-(order?.discount_detail.reduce_shipping_fee+order?.discount_detail.reduce_product_price))}
                    <View
                        style={[
                            {
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                borderStyle: 'dashed',
                                borderTopWidth: 1,
                                borderColor: '#E2E2E2',
                                paddingVertical: 10,
                            },
                        ]}>
                        <Text
                            style={{
                                flex: 3,
                                ...Styles.text.text12,
                                color: ColorStyle.tabBlack,
                                fontWeight: '700',
                            }}>
                            {strings('totalPrice')}
                        </Text>
                        <Text
                            style={{
                                flex: 1,
                                ...Styles.text.text12,
                                color: ColorStyle.tabActive,
                                fontWeight: '700',
                                textAlign: 'right',
                            }}>
                            {CurrencyFormatter(order?.discount_detail.total)}
                        </Text>
                    </View>
                </View>

            </View>
        );
    }
    getImageUrl(item, index) {
        let url = item.avatar;
        return DataUtils.stringNullOrEmpty(url)
            ? 'https://picsum.photos/50/50?avatar' + index
            : constants.host + url;
    }
    renderPaymentView() {
        let order = this.state.order;
        return (
            <View style={OrderStyles.viewBody}>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-start',
                    }}>
                    <Icon
                        name={'wallet'}
                        type={'antdesign'}
                        color={ColorStyle.tabActive}
                        size={20}
                    />
                    <Text
                        style={{
                            ...Styles.text.text16,
                            color: ColorStyle.tabBlack,
                            fontWeight: '500',
                            marginLeft: 5,
                        }}>
                        {strings('paymentM')}
                    </Text>
                </View>
                <Text style={{paddingTop: 10, color: ColorStyle.gray}}>
                    {CartUtils.getPayment(order.payment_method)}
                </Text>
            </View>
        );
    }
    renderTrustUser() {
        return (
            <View style={OrderStyles.viewBody}>
                <Text style={{...Styles.text.text16, color: ColorStyle.tabBlack}}>
                    {strings('trustUserTitle')}
                </Text>
                <Text
                    style={{
                        ...Styles.text.text12,
                        color: ColorStyle.gray,
                        lineHeight: 20,
                        marginVertical: 10,
                    }}>
                    {strings('trustUserDes')}
                </Text>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                    }}>
                    <Text style={{...Styles.text.text11, color: ColorStyle.gray}}>
                        {strings('buyerRate')}
                        <RatingStar size={10} paddingStar={1} rating={5} />
                    </Text>
                    <Text style={{...Styles.text.text11, color: ColorStyle.gray}}>
                        8 {strings('rate1')}
                    </Text>
                </View>
            </View>
        );
    }
    renderButton() {
        switch (this.state.order.status) {
            case AppConstants.STATUS_ORDER.UNCONFIRMED:
                return (
                    <View style={{marginTop: 10}}>
                        {this.renderMess()}
                        {this.renderStatus()}
                    </View>
                );
            case AppConstants.STATUS_ORDER.CONFIRMED:
                return this.renderConfirmed();
            case AppConstants.STATUS_ORDER.DELIVERING:
                return this.renderDelivering();
            case AppConstants.STATUS_ORDER.DA_GIAO:
                return this.renderMessColor();
            case AppConstants.STATUS_ORDER.USER_CANCEL:
                return this.renderMessColor();
            case AppConstants.STATUS_ORDER.SUCCESS:
                return (
                    <View style={{marginTop: 10}}>
                        {/*{this.renderMess()}*/}
                        {/*{this.renderRate()}*/}
                    </View>
                );
        }
    }
    renderDelivering(){
        return(
            <View>
                <TouchableOpacity
                    onPress={() => {
                        this.showDialog(strings('deviOrderDes'), () => {
                            if (this.props.orderFunc != null) {
                                this.props.orderFunc(AppConstants.STATUS_ORDER.SUCCESS);
                            }
                            Actions.pop();
                        });
                    }}
                    style={{
                        marginHorizontal: Styles.constants.marginHorizontal20,
                        backgroundColor: ColorStyle.tabActive,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 20,
                        paddingVertical: 10,


                    }}>
                    <Text
                        style={{
                            ...Styles.text.text16,
                            color: ColorStyle.tabWhite,
                        }}>
                        {strings('completed')}
                    </Text>
                </TouchableOpacity>
                <View style={{
                    backgroundColor: ColorStyle.tabWhite,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginHorizontal: Styles.constants.marginHorizontal20,
                    paddingVertical: 10,
                }}>
                    <TouchableOpacity
                        onPress={() => {
                            NavigationUtils.goToConversation(this.state.order.customer.id);
                        }}
                        style={{
                            width:'45%',
                            backgroundColor: ColorStyle.tabWhite,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderColor: '#DADADA',
                            borderWidth: 1,
                            borderRadius: 20,
                            paddingVertical: 10,
                        }}>
                        <Icon
                            name={'chatbubble-ellipses-outline'}
                            type={'ionicon'}
                            size={25}
                            color={ColorStyle.tabActive}
                        />
                        <Text
                            style={{
                                ...Styles.text.text14,
                                color: ColorStyle.tabBlack,
                                marginLeft: 10,
                            }}>
                            {strings('contactBuyer')}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {}}
                        style={{
                            width:'45%',
                            backgroundColor: ColorStyle.tabWhite,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderColor: '#DADADA',
                            borderWidth: 1,
                            borderRadius: 20,
                            paddingVertical: 10,
                        }}>
                        <Icon
                            name={'chatbubble-ellipses-outline'}
                            type={'ionicon'}
                            size={25}
                            color={ColorStyle.tabActive}
                        />
                        <Text
                            style={{
                                ...Styles.text.text14,
                                color: ColorStyle.tabBlack,
                                marginLeft: 10,
                            }}>
                            {strings('contactShip')}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    renderConfirmed(){
        return(
            <View style={{
                backgroundColor: ColorStyle.tabWhite,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginHorizontal: Styles.constants.marginHorizontal20,
                paddingVertical: 10,
            }}>
                <TouchableOpacity
                    onPress={() => {
                        NavigationUtils.goToConversation(this.state.order.customer.id);
                    }}
                    style={{
                        width:'45%',
                        backgroundColor: ColorStyle.tabWhite,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderColor: '#DADADA',
                        borderWidth: 1,
                        borderRadius: 20,
                        paddingVertical: 10,
                    }}>
                    <Icon
                        name={'chatbubble-ellipses-outline'}
                        type={'ionicon'}
                        size={20}
                        color={ColorStyle.tabActive}
                    />
                    <Text
                        style={{
                            ...Styles.text.text12,
                            color: ColorStyle.tabBlack,
                            marginLeft: 10,
                        }}>
                        {strings('contactBuyer')}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.showDialog(strings('deviOrderDes'), () => {
                            if (this.props.orderFunc != null) {
                                this.props.orderFunc(AppConstants.STATUS_ORDER.DELIVERING);
                            }
                            Actions.pop();
                        });
                    }}
                    style={{
                        width:'45%',
                        backgroundColor: ColorStyle.tabActive,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 20,
                        paddingVertical: 10,

                    }}>
                    <Text
                        style={{
                            ...Styles.text.text16,
                            color: ColorStyle.tabWhite,
                        }}>
                        {strings('giaochoShip')}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
    renderMess() {
        return (
            <TouchableOpacity
                onPress={() => {
                    NavigationUtils.goToConversation(this.state.order.customer.id);
                }}
                style={{
                    backgroundColor: ColorStyle.tabWhite,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginHorizontal: Styles.constants.marginHorizontal20,
                    borderColor: '#DADADA',
                    borderWidth: 1,
                    borderRadius: 20,
                    paddingVertical: 10,
                }}>
                <Icon
                    name={'chatbubble-ellipses-outline'}
                    type={'ionicon'}
                    size={25}
                    color={ColorStyle.tabActive}
                />
                <Text
                    style={{
                        ...Styles.text.text14,
                        color: ColorStyle.tabBlack,
                        marginLeft: 10,
                    }}>
                    {strings('contactBuyer')}
                </Text>
            </TouchableOpacity>
        );
    }
    renderMessColor() {
        return (
            <TouchableOpacity
                style={{
                    backgroundColor: ColorStyle.tabActive,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginHorizontal: Styles.constants.marginHorizontal20,
                    borderRadius: 20,
                    paddingVertical: 10,
                }}>
                <Text
                    style={{
                        ...Styles.text.text14,
                        color: ColorStyle.tabWhite,
                    }}>
                    {strings('contactBuyer')}
                </Text>
            </TouchableOpacity>
        );
    }
    renderStatus() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginHorizontal: Styles.constants.marginHorizontal20,
                    marginTop: 10,
                }}>
                <TouchableOpacity
                    onPress={() => {
                        this.showDialog(strings('cancelOrderDes'), () => {
                            if (this.props.orderFunc != null) {
                                this.props.orderFunc(AppConstants.STATUS_ORDER.SHOP_CANCEL);
                            }
                            Actions.pop();
                        });
                    }}
                    style={{
                        width: '49%',
                        backgroundColor: '#C4C4C4',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderColor: '#DADADA',
                        borderWidth: 1,
                        borderRadius: 20,
                        paddingVertical: 15,
                    }}>
                    <Text
                        style={{
                            ...Styles.text.text16,
                            color: ColorStyle.tabWhite,
                            marginLeft: 10,
                        }}>
                        {strings('cancelOrder1')}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() =>
                        this.showDialog(strings('confirmOrderCompleted'), () => {
                            if (this.props.orderFunc != null) {
                                this.props.orderFunc(AppConstants.STATUS_ORDER.CONFIRMED);
                            }
                            Actions.pop();
                        })
                    }
                    style={{
                        width: '49%',
                        backgroundColor: ColorStyle.tabActive,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 20,
                        paddingVertical: 15,
                        marginLeft: '2%',
                    }}>
                    <Text
                        style={{
                            ...Styles.text.text16,
                            color: ColorStyle.tabWhite,
                            marginLeft: 10,
                        }}>
                        {strings('prepare')}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
    showDialog(msg, callback) {
        ViewUtils.showAskAlertDialog(msg, callback, undefined);
    }
    renderRate() {
        return (
            <TouchableOpacity
                style={{
                    backgroundColor: ColorStyle.tabActive,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginHorizontal: Styles.constants.marginHorizontal20,
                    borderRadius: 20,
                    paddingVertical: 10,
                    marginTop: 10,
                }}>
                <Text
                    style={{
                        ...Styles.text.text16,
                        color: ColorStyle.tabWhite,
                        marginLeft: 10,
                    }}>
                    {strings('rate')}
                </Text>
            </TouchableOpacity>
        );
    }
}
