import React, {Component} from "react";
import {Text, TextInput, TouchableOpacity, View} from "react-native";
import DataUtils from "../../../Utils/DataUtils";
import SellerStyles from "./SellerStyles";
import {strings} from "../../../resource/languages/i18n";
import {Icon} from "react-native-elements";
import ColorStyle from "../../../resource/ColorStyle";
import ElevatedView from "react-native-elevated-view";
import Ripple from "react-native-material-ripple";
import Styles from "../../../resource/Styles";
import {TextInputMask} from "react-native-masked-text";
import {Actions} from "react-native-router-flux";
let validGroupName = "validGroupName";
let validGroupValue = "validGroupValue";
let validOptionPrice = "validOptionPrice";
let validOptionFinalPrice = "validOptionFinalPrice";
let validOptionSku = "validOptionSku";
let validOptionQty = "validOptionQty";
let validProductFeature = "validProductFeature";
let finalPriceFinalOptionTxt = "finalPriceFinalOptionTxt";
let priceOptionTxt = "priceOptionTxt";
export default class TestAdd extends Component{
    constructor(props) {
        super(props);
        this.state={
            groupOptions: [], //Max la 2
            options: [],
        }
    }

    render() {
        return (
            <View style={{marginTop:50,width:'90%',alignSelf:'center'}}>
                {this.renderGroupOptions()}
                {this.renderBottom()}
            </View>
        );
    }
    renderGroupOptions(){
        let groupOptions =  this.state.groupOptions;
        let groupOptionView = groupOptions.map((item, index)=>{
            let values = item.product_details;
            if(DataUtils.listNullOrEmpty(values)) values = [];
            let valuesView = values.map((value,i) => {
                return(
                    <View style={SellerStyles.itemOptions}>
                        <Text style={SellerStyles.title}>{`${strings('groupOptionValue')}${i+ 1}`}</Text>
                        <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center',height:50}}>
                            <View style={{flex:1}}>
                                <TextInput value={value.name}
                                           onChangeText={(text => {
                                               this.state[`${validGroupValue}${index}${i}`] = !DataUtils.stringNullOrEmpty(text);
                                               values[i] = {
                                                   ...values[i],
                                                   name:text
                                               };
                                               item.product_details = values;
                                               groupOptions[index] = item;
                                               this.compileOptions(groupOptions)
                                           })}
                                           style={{...SellerStyles.codeInput, flex:1}}
                                           placeholder={strings('groupOptionValueHint')} />
                                <Icon name={this.state[`${validGroupValue}${index}${i}`] ? 'check' :'alert-circle'} type='feather' size={15}
                                      color={this.state[`${validGroupName}${index}${i}`] ? 'green' :'red'}
                                      containerStyle={{position:'absolute', right:20, bottom:12}}/>
                            </View>
                            {values.length > 1 &&
                                <TouchableOpacity   onPress={()=>{
                                    values = DataUtils.removeItem(values, i);
                                    item.product_details = values;
                                    groupOptions[index] = item;
                                    this.compileOptions(groupOptions)
                                }}>
                                    <Icon name='close' type='evil-icons' size={20} color={ColorStyle.inActiveText}/>
                                </TouchableOpacity>
                            }
                        </View>
                        <View style={{flexDirection:'row', justifyContent:'center', marginTop:10,alignItems:'center',height:50}}>
                            <View style={{flex:1}}>
                                <TextInput
                                    value={value.quantity}
                                    keyboardType={'number-pad'}
                                   onChangeText={(text => {
                                       this.state[`${validOptionQty}${index}${i}`] = !DataUtils.stringNullOrEmpty(text);
                                       values[i] = {
                                           ...values[i],
                                           quantity:text
                                       };
                                       item.product_details = values
                                       groupOptions[index] = item;
                                       this.compileOptions(groupOptions)
                                   })}
                                           style={{...SellerStyles.codeInput, flex:1}}
                                           placeholder={strings('quantity')}
                                />
                                <Icon name={this.state[`${validOptionQty}${index}${i}`] ? 'check' :'alert-circle'} type='feather' size={15}
                                      color={this.state[`${validOptionQty}${index}${i}`] ? 'green' :'red'}
                                      containerStyle={{position:'absolute', right:20, bottom:12}}/>
                            </View>
                        </View>
                    </View>
                )
            });
            return(
                <ElevatedView elevation={2} style={SellerStyles.bodyOption} >
                    <Text style={SellerStyles.title}>{strings('optionName')}</Text>
                    <View>
                        <TextInput value={item.name}
                                   onChangeText={(text => {
                                       this.state[`${validGroupName}${index}`] = !DataUtils.stringNullOrEmpty(text);
                                       item.name = text;
                                       groupOptions[index] = item;
                                       this.compileOptions(groupOptions)
                                   })}
                                   style={SellerStyles.codeInput}
                                   placeholder={strings('groupOptionNameHint')}/>
                        <Icon name={this.state[`${validGroupName}${index}`] ? 'check' :'alert-circle'} type='feather' size={15}
                              color={this.state[`${validGroupName}${index}`] ? 'green' :'red'}
                              containerStyle={{position:'absolute', right:20, bottom:12}}/>
                    </View>
                    <TouchableOpacity style={SellerStyles.btnClose} onPress={()=>{
                        let groupOptions = DataUtils.removeItem(this.state.groupOptions, index);
                        this.compileOptions(groupOptions)
                    }}>
                        <Icon name='close' type='evil-icons' size={20} color={ColorStyle.inActiveText}/>
                    </TouchableOpacity>
                    {valuesView}
                    <View style={SellerStyles.btnOptions}>
                        <Ripple onPress ={()=>{
                            let values = item.product_details;
                            values.push("");
                            item = {
                                ...item,
                                product_details:values
                            };
                            groupOptions[index] = item;
                            this.compileOptions(groupOptions)
                        }}>
                            <Text style={{...Styles.text.text14,color:ColorStyle.tabWhite}}>{strings('addGroupOptionValue')}</Text>
                        </Ripple>
                    </View>
                </ElevatedView>
            )
        });
        return(
            <View>
                <Text style={{...SellerStyles.title, fontSize:16, fontWeight:'600',marginTop:10}}>{strings('phanLoaiSanPhamList')}</Text>
                {groupOptionView}
                {this.state.groupOptions.length <= 1 &&
                    <TouchableOpacity style={SellerStyles.viewOptions} onPress={()=>{
                        this.addGroupOption()
                    }}>
                        <Icon name='plus' type='evilicon' size={25}/>
                        <Text>{strings('addGroupOption')}</Text>
                    </TouchableOpacity>
                }
            </View>
        )
    }
    addGroupOption(){
        let groupOptions =  this.state.groupOptions;
        let groupOption = {
            name:"",
            product_details:[""]
        };
        groupOptions.push(groupOption);
        this.compileOptions(groupOptions)
    }

    compileOptions(groupOptions){
        let options = [];
        if(groupOptions.length === 1){
            groupOptions[0].product_details.forEach((item, i)=>{
                let option = {
                    name:item,
                    sku:"",
                    qty:0,
                    price:0,
                    original_price:0,
                    groups_index : [i]
                };
                options.push(option)
            })
        }else if(groupOptions.length === 2) {
            let valueOptions1 = groupOptions[0].product_details;
            let valueOption2 = groupOptions[1].product_details;
            valueOptions1.forEach((item, i1) =>{
                valueOption2.forEach((item2, i2)=>{
                    let option = {
                        name:item +" - " + item2,
                        sku:"",
                        qty:0,
                        price:0,
                        original_price:0,
                        groups_index:[i1, i2]
                    };
                    options.push(option)
                })
            })
        }
        this.setState({groupOptions,options})
    }
    renderBottom() {
        return (
            <TouchableOpacity
                onPress={() => this.onSave()}
                style={{
                    ...SellerStyles.btnNext,
                    backgroundColor: ColorStyle.tabActive,
                    borderColor: 'rgba(211, 38, 38, 0.25)',
                    elevation: 2,
                    marginTop:50
                }}>
                <Text
                    style={{
                        ...Styles.text.text14,
                        color: ColorStyle.tabWhite,
                        fontWeight: '500',
                    }}>
                    {strings('save').toUpperCase()}
                </Text>
            </TouchableOpacity>
        );
    }
    onSave(){
        console.log(JSON.stringify(this.state.groupOptions))
    }
}
