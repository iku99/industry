import React from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Styles from '../../../../resource/Styles';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import ColorStyle from '../../../../resource/ColorStyle';
import StoreHandle from '../../../../sagas/StoreHandle';
import {strings} from '../../../../resource/languages/i18n';
import AppConstants from '../../../../resource/AppConstants';
import InfoSellerRow from './row/InfoSellerRow';
import MySalesRow from './row/MySalesRow';
import ListItemRow from './row/ListItemRow';
import NavigationUtils from "../../../../Utils/NavigationUtils";

const defaulData = [
  {
    type: AppConstants.SELLER_DETAIL_TYPE.STORE_INFO,
  },
  {
    type: AppConstants.SELLER_DETAIL_TYPE.NOTIFICATION,
  },
  {
    type: AppConstants.SELLER_DETAIL_TYPE.MY_SALE,
  },
  {
    type: AppConstants.SELLER_DETAIL_TYPE.LIST_ITEM,
  },
  {
    type: AppConstants.SELLER_DETAIL_TYPE.TITLE,
  },
];
class SellerDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      storeId: undefined,
      store: undefined,
      productCount: 0,
      loading: false,
      page: 0,
      canLoadData: true,
    };
  }
  componentDidMount() {
    this.getMyStoreInfo();
    this.setState({data: defaulData});
  }
  toolbar() {
    return (
      <View
        style={{
          paddingTop: Styles.constants.X,
          paddingBottom: Styles.constants.X * 0.2,
          paddingHorizontal: Styles.constants.X * 0.65,
          flexDirection: 'row',
          backgroundColor: ColorStyle.tabActive,
          alignItems: 'center',
          justifyContent: 'space-between',
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
        }}>
        <TouchableOpacity onPress={() => Actions.pop()}>
          <Icon
            name="left"
            type="antdesign"
            size={25}
            color={ColorStyle.tabWhite}
          />
        </TouchableOpacity>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity style={{marginRight: 10}} onPress={()=>{NavigationUtils.goToCartScreen()}}>
            <Icon
              name="message-processing-outline"
              type="material-community"
              size={25}
              color={ColorStyle.tabWhite}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{
                Actions.jump('notificationStore',{})
          }}>
            <Icon
              name="bell-o"
              type="font-awesome"
              size={25}
              color={ColorStyle.tabWhite}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  render() {
    return (
      <View style={Styles.container}>
        {this.toolbar()}
        <FlatList
          showsVerticalScrollIndicator={false}
          horizontal={false}
          data={this.state.data}
          keyExtractor={this.keyExtractor}
          renderItem={({item,index})=>this._renderItem(item,index)}
          onEndReachedThreshold={0.4}
        />
      </View>
    );
  }
  keyExtractor = (item, index) => `sellerDetail${index.toString()}`;
  _renderItem(item,index){
    switch (item.type) {
      case AppConstants.SELLER_DETAIL_TYPE.STORE_INFO:return <InfoSellerRow store={this.state.store} callback={()=>{this.setState({store:{}},()=>this.getMyStoreInfo())}}/>;
      // case AppConstants.SELLER_DETAIL_TYPE.NOTIFICATION:return <NotificationSellerRow storeId={this.state.store?.id} />;
      case AppConstants.SELLER_DETAIL_TYPE.MY_SALE:return <MySalesRow store={this.state.store} />;
      case AppConstants.SELLER_DETAIL_TYPE.LIST_ITEM:return (<ListItemRow store={this.state.store} callback={() => this.setState({data: defaulData})}/>);
      case AppConstants.SELLER_DETAIL_TYPE.END:return <View style={{marginBottom: 10}} />;
      default:
        break;
    }
  };
  renderTitle() {
    return (
      <TouchableOpacity
        onPress={() => Actions.jump('listProduct',{idStore:this.state.store.id})}
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginHorizontal: Styles.constants.marginHorizontalAll,
          marginVertical: 12,
        }}>
        <Text
          style={{
            ...Styles.text.text18,
            fontWeight: '700',
            color: ColorStyle.gray,
          }}>
          {strings('all') + ' ' + strings('product')}
        </Text>
        <Icon
          name="chevron-right"
          type="feather"
          size={25}
          color={ColorStyle.gray}
        />
      </TouchableOpacity>
    );
  }
  async getMyStoreInfo() {
    this.setState({loading: true});
    StoreHandle.getInstance().getMyStore(
      (isSuccess, responseData) => {
        if (isSuccess && responseData !== undefined) {
          if (responseData.data != null) {
            this.setState({store:responseData.data, hasStore: true,loading:false})
            return;
          }
        }
        this.setState({hasStore: false});
      },
    );
  }
}
const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    onGetCategory: param => {},
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SellerDetail);
