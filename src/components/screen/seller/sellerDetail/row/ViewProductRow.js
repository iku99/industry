import React from 'react';
import {FlatList} from 'react-native';
import EmptyView from '../../../../elements/reminder/EmptyView';
import ViewItemProductSeller from '../../../../elements/viewItem/ViewedProduct/ViewItemProductSeller';
import NavigationUtils from '../../../../../Utils/NavigationUtils';
export default class ViewProductRow extends React.Component {
  render() {
    let item = this.props.item;
    if (item.length !== 0) {
      return (
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={item}
          style={{
            alignItems: 'center',
          }}
          keyExtractor={this.keyExtractor}
          renderItem={this._renderItemProduct}
          removeClippedSubviews={true} // Unmount components when outside of window
          windowSize={10}
        />
      );
    } else {
      return <EmptyView />;
    }
  }
  _renderItemProduct = ({item, index}) => (
    <ViewItemProductSeller
      item={item}
      index={index}
      onClick={item => {
        NavigationUtils.goToProductDetail(item);
      }}
      onUpdate={(productId, index) => {
        this.props.onUpdateProduct(productId, index);
      }}
    />
  );
}
