import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../../../resource/Styles';
import ColorStyle from '../../../../../resource/ColorStyle';
import ImageHelper from '../../../../../resource/images/ImageHelper';
import {strings} from '../../../../../resource/languages/i18n';
import StoreHandle from '../../../../../sagas/StoreHandle';
import constants from "../../../../../Api/constants";
import {Actions} from "react-native-router-flux";
import NavigationUtils from "../../../../../Utils/NavigationUtils";
import SellerStyles from "../../SellerStyles";
export default class InfoSellerRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
  }

  render() {
    let store = this.props.store;
    return (
      <TouchableOpacity
          onPress={()=>NavigationUtils.goToUpdateSeller(store?.id,()=>{
              this.props.callback();
          })}
        style={SellerStyles.bodyItemDetail}>
        <Image
          source={this.getImageAvatar(store?.avatar)}
          style={SellerStyles.logoStore}
        />
        <View
          style={{
            flex: 3,
            justifyContent: 'space-between',
            height: Styles.constants.X * 1.9,
          }}>
          <Text
            style={{
              ...Styles.text.text18,
              color: ColorStyle.tabBlack,
              fontWeight: '700',
            }}>
            {store?.name}
          </Text>
          <Text
            style={{
              ...Styles.text.text14,
              color: 'rgba(0, 0, 0, 0.6)',
              fontWeight: '500',
            }}>
            {/*54 {strings('follows')} | 45 {strings('followers')}*/}
              {store?.follow_quantity} {strings('follows')}
          </Text>
          <Text
            style={{
              ...Styles.text.text14,
              color: ColorStyle.tabBlack,
              fontWeight: '500',
            }}>
            {strings('product') + ' : ' + store?.product_quantity}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
  getImageAvatar(image){
      if(image!==null){
          return {uri:constants.host+image}
      }else
          return ImageHelper.avtSeller
  }
}
