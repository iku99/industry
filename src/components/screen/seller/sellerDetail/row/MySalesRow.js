import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import SellerStyles from '../../SellerStyles';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import Styles from '../../../../../resource/Styles';
import ColorStyle from '../../../../../resource/ColorStyle';
import {strings} from '../../../../../resource/languages/i18n';
export default class MySalesRow extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {}
  render() {
    return (
      <View style={SellerStyles.renderViewSellerDetail}>
        <TouchableOpacity
          style={SellerStyles.heartView}
          onPress={() =>
            Actions.jump('mySaleScreen', {storeId: this.props.store.id})
          }>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon name={'sound'} type={'antdesign'} size={20} />
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabBlack,
                marginLeft: 10,
                fontWeight: '500',
              }}>
              {strings('sales')}
            </Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.gray,
                marginLeft: 10,
              }}>
              {strings('viewHistory')}
            </Text>
            <Icon
              name={'right'}
              type={'antdesign'}
              size={20}
              color={ColorStyle.gray}
            />
          </View>
        </TouchableOpacity>
        <View style={{flexDirection: 'row', marginVertical: 16}}>
          {this.renderDeliveryStatus(
            strings('delivery'),
            'package',
            'feather',
            () => {
              Actions.jump('mySaleScreen', {index: 2,storeId: this.props.store.id});
            },
          )}
          {this.renderDeliveryStatus(
            strings('cancel'),
            'calendar-times-o',
            'font-awesome',
            () => {
              Actions.jump('mySaleScreen', {index: 4,storeId: this.props.store.id});
            },
          )}
          {this.renderDeliveryStatus(
            strings('return'),
            'package',
            'feather',
            () => {
              Actions.jump('mySaleScreen', {index: 3,storeId: this.props.store.id});
            },
          )}
          {this.renderDeliveryStatus(
            strings('more'),
            'more-horizontal',
            'feather',
            () => {
              Actions.jump('mySaleScreen', {index: 0,storeId: this.props.store.id});
            },
          )}
        </View>
      </View>
    );
  }
  renderDeliveryStatus(title, icon, type, onPress) {
    return (
      <TouchableOpacity
        style={{
          alignItems: 'center',
          width: Styles.constants.widthScreenMg24 / 4,
          justifyContent: 'flex-start',
          marginHorizontal: Styles.constants.X * 0.1,
        }}
        onPress={onPress}>
        <Icon name={icon} type={type} size={25} />
        <Text style={[Styles.text.text14, {marginTop: 10}]}>{title}</Text>
      </TouchableOpacity>
    );
  }
}
