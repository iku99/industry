import React from 'react';
import { FlatList, Image, Text, TouchableOpacity, View } from "react-native";
import SellerStyles from '../../SellerStyles';
import {Icon} from 'react-native-elements';
import Styles from '../../../../../resource/Styles';
import ColorStyle from '../../../../../resource/ColorStyle';
import {strings} from '../../../../../resource/languages/i18n';
import SimpleToast from "react-native-simple-toast";
const ListImg = [
  {
    url: 'https://png.pngtree.com/png-clipart/20201209/original/pngtree-big-sale-2021-now-purple-png-image_5677289.jpg',
  },
  {
    url: 'https://media.istockphoto.com/vectors/one-day-big-sale-banner-vector-id635869990',
  },
  {
    url: 'https://media.istockphoto.com/photos/big-sale-picture-id187046307?s=612x612',
  },
];

export default class NotificationSellerRow extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={SellerStyles.renderViewSellerDetail}>
        <TouchableOpacity style={SellerStyles.heartView} onPress={()=>SimpleToast.show(strings('developing'))}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon name={'sound'} type={'antdesign'} size={20} />
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabBlack,
                marginLeft: 10,
              }}>
              {strings('notification')}
            </Text>
          </View>
          <View>
            <Icon name={'right'} type={'antdesign'} size={25} />
          </View>
        </TouchableOpacity>
        <FlatList
          data={ListImg}
          renderItem={this.renderImage}
          style={{
            marginHorizontal: Styles.constants.marginHorizontalAll,
            marginTop: 8,
          }}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  }
  renderImage = ({item, index}) => {
    return (
      <TouchableOpacity style={{marginRight: 10}}>
        <Image source={{uri: item.url}} style={Styles.icon.img_avatar} />
      </TouchableOpacity>
    );
  };
}
