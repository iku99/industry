import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import ColorStyle from '../../../../../resource/ColorStyle';
import {strings} from '../../../../../resource/languages/i18n';
import ImageHelper from '../../../../../resource/images/ImageHelper';
import {Actions} from 'react-native-router-flux';
import SellerStyles from '../../SellerStyles';
import Styles from '../../../../../resource/Styles';
import {Icon} from 'react-native-elements';
import NavigationUtils from '../../../../../Utils/NavigationUtils';
export default class ListItemRow extends React.Component {
    componentDidMount() {

    }

    render() {
    return (
      <View>
        <View style={{borderColor: ColorStyle.borderItemHome, elevation: 2}}>
          {this.renderOptionItem(
            strings('myProduct'),
            '#FE9870',
            ImageHelper.myProduct,
            () => {
              Actions.jump('listProduct', {
                storeId: this.props.store.id,
                callBack: this.props.callBack,
              });
            },
          )}
          {this.renderOptionItem(
            strings('addNewProduct'),
            '#FE9870',
            ImageHelper.addProduct,
            () => {
              NavigationUtils.goToUploadProduct(undefined,this.props.store.id, undefined, () => {
                this.props.callback();
              });
            },
          )}
        </View>
        <View
          style={{
            marginTop: 16,
            borderColor: ColorStyle.borderItemHome,
            elevation: 2
          }}>
          {this.renderOptionItem(
            strings('myIncome'),
            '#FE9870',
            ImageHelper.myIncome,
            () => {
              Actions.jump('incomeScreen', {storeId: this.props.store.id});
            },
          )}
          { this.renderOptionItem(
            strings('marke'),
            '#FE9870',
            ImageHelper.makg,
            () => {  Actions.jump('voucherStore', {storeId: this.props.store?.id});}
          )}
          {this.renderOptionItem(
            strings('myShipping'),
            '#FE9870',
            ImageHelper.myShipping,
            () => {
              Actions.jump('transportList', {store: this.props.store});
            },
          )}
          {this.renderOptionItem(
            strings('rating'),
            '#FE9870',
            ImageHelper.rating,
            () => {
              Actions.jump('ratingShop', {storeId: this.props.store?.id});
            },
          )}
          {this.renderOptionItem(
            strings('business'),
            '#FE9870',
            ImageHelper.business,
            () => {
                Actions.jump('statisticalScreen', {storeId: this.props.store?.id});
            },
          )}
          {/*{ this.renderOptionItem(*/}
          {/*  strings('ads'),*/}
          {/*  '#FE9870',*/}
          {/*  ImageHelper.ads,*/}
          {/*  () => {}*/}
          {/*)}*/}
          {/*{this.renderOptionItem(*/}
          {/*  strings('support'),*/}
          {/*  '#FE9870',*/}
          {/*  ImageHelper.support,*/}
          {/*  () => {},*/}
          {/*)}*/}
          <TouchableOpacity
              onPress={()=>Actions.jump('viewSeller',{storeId: this.props.store?.id})}
            style={{
              ...SellerStyles.viewItem,
              marginTop: 12,
              paddingVertical: 20,
            }}>
            <View
              style={{
                flex: 2,
                flexDirection: 'row',
                alignItems: 'center',
                paddingLeft: 10,
              }}>
              <Text
                style={[
                  Styles.text.text14,
                  {
                    fontWeight: '500',
                    maxWidth: '70%',
                  },
                ]}>
                {strings('viewShop')}
              </Text>
            </View>
            <View style={{flex: 2, flexDirection: 'row', alignItems: 'center'}}>
              <Text>{strings('http')}</Text>
              <Icon
                name="chevron-right"
                type="feather"
                size={25}
                color={ColorStyle.gray}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  renderOptionItem(title, color, icon, onPress) {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={{
          ...SellerStyles.viewItem,
        }}>
        <View
          style={{
            flex: 2,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View
            style={{
              padding: 10,
              borderRadius: 100,
              alignItems: 'center',
            }}>
            <Image source={icon} style={Styles.icon.iconOrder} />
          </View>
          <Text
            style={[
              Styles.text.text14,
              {
                fontWeight: '500',
                marginLeft: Styles.constants.X * 0.5,
                maxWidth: '70%',
              },
            ]}>
            {title}
          </Text>
        </View>
        <View style={{flex: 1, alignItems: 'flex-end'}}>
          <Icon
            name="chevron-right"
            type="feather"
            size={25}
            color={ColorStyle.gray}
          />
        </View>
      </TouchableOpacity>
    );
  }
}
