import React from 'react';
import {
    Dimensions, FlatList,
    ScrollView,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import ToolbarSeller from '../../elements/toolbar/ToolbarSeller';
import {strings} from '../../../resource/languages/i18n';
import {connect} from 'react-redux';
import Styles from '../../../resource/Styles';
import ColorStyle from '../../../resource/ColorStyle';
import DateUtil from '../../../Utils/DateUtil';
import SellerStyles from './SellerStyles';
import {LineChart} from 'react-native-line-chart';
import {SceneMap, TabView} from 'react-native-tab-view';
import ViewUtils from '../../../Utils/ViewUtils';
import TabStatistical from "./tab/TabStatistical";
import { Icon } from "react-native-elements";
import AppConstants from "../../../resource/AppConstants";
import InfoSellerRow from "./sellerDetail/row/InfoSellerRow";
import NotificationSellerRow from "./sellerDetail/row/NotificationSellerRow";
import MySalesRow from "./sellerDetail/row/MySalesRow";
import ListItemRow from "./sellerDetail/row/ListItemRow";
let created_at = '2019-10-19T05:54:03.000+00:00';
let created_up = '2019-10-23T05:54:03.000+00:00';
const Data = [
  {
    url: '/upload/product/2d6fa716-71df-42e7-a369-35156040100f.JPG',
    title: 'Light',
    viewUser: '24.5',
    count: '2k',
  },
  {
    url: '/upload/product/2d6fa716-71df-42e7-a369-35156040100f.JPG',
    title: 'Light',
    viewUser: '24.5',
    count: '2k',
  },
  {
    url: '/upload/product/2d6fa716-71df-42e7-a369-35156040100f.JPG',
    title: 'Light',
    viewUser: '24.5',
    count: '2k',
  },
  {
    url: '/upload/product/2d6fa716-71df-42e7-a369-35156040100f.JPG',
    title: 'Light',
    viewUser: '24.5',
    count: '2k',
  },
];
const DataList=[
    {type:1},{type:2},{type:3},{type:4},
]
class StatisticalScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectControl: 0,
      index: 0,
      routes: [
        {key: 'TabSale', title: strings('sale')},
        {key: 'TabUnits', title: strings('unitsSold')},
        {key: 'TabPage', title: strings('pageViews')},
      ],
    };
  }
  componentDidMount() {

  }

    _handleIndexChange = index => this.setState({index});

  _renderScene = SceneMap({
    TabSale: () => <TabStatistical item={Data}/>,
    TabUnits: () => <TabStatistical item={Data}/>,
    TabPage: () => <TabStatistical item={Data}/>,
  });
  _renderTabBar = props => {
    return ViewUtils.renderTab(
      props,
      ColorStyle.tabWhite,
      this.state.index,
      i => {
        this.setState({index: i});
      },
    );
  };
  render() {
    return (
      <View style={Styles.container}>
        {this.renderBody()}
        <ToolbarSeller title={strings('statistical')} />
      </View>
    );
  }
    keyExtractor1 = (item, index) => `statistical_${index.toString()}`;
    _renderItem = ({item}) => {
        switch (item.type) {
            case 1:
                return this.renderListRow()
            case 2:
                return this.renderItemData()
            case 3:
                return this.renderControl();
        }
    };
  renderBody() {
    return (
        <FlatList
            showsVerticalScrollIndicator={false}
            horizontal={false}
              style={{
                position: 'absolute',
                top: Styles.constants.X * 1.3,
                width: Styles.constants.widthScreen,
                height: Styles.constants.heightScreen + 20,
              }}
            data={DataList}
            onEndReachedThreshold={0.4}
            keyExtractor={this.keyExtractor1}
            renderItem={this._renderItem}
            ListFooterComponent={() => (
                // this.renderProduct()
                this._renderTabContent()
            )}
            removeClippedSubviews={true}
            windowSize={10}
        />
    );
  }
  renderListRow(){
      return(
          <View
              style={{
                  backgroundColor: ColorStyle.tabWhite,
                  paddingTop: 50,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingHorizontal: 10,
                  paddingBottom: 12,
              }}>
              <TouchableOpacity
                  style={{
                      backgroundColor: ColorStyle.buttonDay,
                      paddingVertical: 8,
                      paddingHorizontal: 12,
                  }}>
                  <Text style={{...Styles.text.text14, color: ColorStyle.tabBlack}}>
                      {strings('Real_time')}
                  </Text>
              </TouchableOpacity>
              <TouchableOpacity
                  style={{
                      backgroundColor: ColorStyle.buttonDay,
                      paddingVertical: 8,
                      paddingHorizontal: 12,
                  }}>
                  <Text style={{...Styles.text.text14, color: ColorStyle.tabBlack}}>
                      {strings('yesterday')}
                  </Text>
              </TouchableOpacity>
              <TouchableOpacity
                  style={{
                      backgroundColor: ColorStyle.buttonDay,
                      paddingVertical: 8,
                      paddingHorizontal: 12,
                  }}>
                  <Text style={{...Styles.text.text14, color: ColorStyle.tabBlack}}>
                      {strings('week')}
                  </Text>
              </TouchableOpacity>
              <TouchableOpacity
                  style={{
                      backgroundColor: ColorStyle.buttonDay,
                      paddingVertical: 8,
                      paddingHorizontal: 12,
                  }}>
                  <Text style={{...Styles.text.text14, color: ColorStyle.tabBlack}}>
                      {strings('month')}
                  </Text>
              </TouchableOpacity>
          </View>
      )
  }
  renderItemData() {
    return (
      <View style={{backgroundColor: ColorStyle.tabWhite, marginTop: 8}}>
        <View
          style={{
            paddingHorizontal: Styles.constants.marginHorizontalAll,
            marginVertical: 12,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            borderBottomWidth: 1,
            borderColor: ColorStyle.loading,
            paddingBottom: 12,
          }}>
          <Text
            style={{
              ...Styles.text.text14,
              color: ColorStyle.tabBlack,
              fontWeight: '500',
            }}>
            {strings('keyMetrics')}
          </Text>
          <Text style={{...Styles.text.text12, color: ColorStyle.gray}}>
            {DateUtil.formatDate('DD.MM.YYYY', created_at)}-->
            {DateUtil.formatDate('DD.MM.YYYY', created_up)}
          </Text>
        </View>

      </View>
    );
  }
  renderControl() {
    return (
      <View>
        <View
          style={{
            paddingHorizontal: Styles.constants.marginHorizontalAll,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <TouchableOpacity
            onPress={() => this.selectControl(1)}
            style={{
              width: '30%',
              backgroundColor: ColorStyle.buttonControl,
              padding: 8,
              borderWidth: 1,
              borderColor:
                this.state.selectControl === 0
                  ? ColorStyle.tabActive
                  : ColorStyle.loading,
            }}>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              {strings('ordered')}
            </Text>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              40
            </Text>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              10%
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.selectControl(1)}
            style={{
              width: '30%',
              backgroundColor: ColorStyle.buttonControl,
              padding: 8,
              borderWidth: 1,
              borderColor:
                this.state.selectControl === 1
                  ? ColorStyle.tabActive
                  : ColorStyle.loading,
            }}>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              {strings('sales1')}
            </Text>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              40
            </Text>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              10%
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.selectControl(2)}
            style={{
              width: '30%',
              backgroundColor: ColorStyle.buttonControl,
              padding: 8,
              borderWidth: 1,
              borderColor:
                this.state.selectControl === 2
                  ? ColorStyle.tabActive
                  : ColorStyle.loading,
            }}>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              {strings('conversion')}
            </Text>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              40
            </Text>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              <Icon name={'caretdown'} type={'antdesign'} color={'red'} size={15}/>
              10%
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            paddingHorizontal: Styles.constants.marginHorizontalAll,
            marginTop: 12,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <TouchableOpacity
            onPress={() => this.selectControl(3)}
            style={{
              width: '30%',
              backgroundColor: ColorStyle.buttonControl,
              padding: 8,
              borderWidth: 1,
              borderColor:
                this.state.selectControl === 3
                  ? ColorStyle.tabActive
                  : ColorStyle.loading,
            }}>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              {strings('salesOrder')}
            </Text>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              40
            </Text>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              10%
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.selectControl(4)}
            style={{
              width: '30%',
              backgroundColor: ColorStyle.buttonControl,
              padding: 8,
              borderWidth: 1,
              borderColor:
                this.state.selectControl === 4
                  ? ColorStyle.tabActive
                  : ColorStyle.loading,
            }}>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              {strings('visitor')}
            </Text>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              40
            </Text>
            <Icon name={'caretup'} type={'antdesign'} size={15} style={{alignSelf:'flex-start'}} color={'#0BD27D'}/>

          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.selectControl(5)}
            style={{
              width: '30%',
              backgroundColor: ColorStyle.buttonControl,
              padding: 8,
              borderWidth: 1,
              borderColor:
                this.state.selectControl === 5
                  ? ColorStyle.tabActive
                  : ColorStyle.loading,
            }}>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              {strings('pageViews')}
            </Text>
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
              }}>
              40
            </Text>
            <Icon name={'caretup'} type={'antdesign'} size={15} style={{alignSelf:'flex-start'}} color={'#0BD27D'}/>

          </TouchableOpacity>
        </View>
        {this.renderLineChart()}

      </View>
    );
  }
  renderLineChart() {
    return (
      <View
        style={{
          width: Styles.constants.widthScreen,
          height: Styles.constants.heightScreen / 3,
        }}>
        <View
          style={{
            ...SellerStyles.itemRow,
            backgroundColor: 'rgba(196, 196, 196, 0.3)',
            paddingHorizontal: Styles.constants.marginHorizontalAll,
            paddingVertical: 8,
            marginVertical: 10,
          }}>
          <Text>23 Nov, Tuesday</Text>
          <Text>1k (30%)</Text>
        </View>
        {/*<LineChart*/}
        {/*  data={{*/}
        {/*    // labels: ['22/11', '23/11', '24/11', '25/11', '26/11', '27/11'],*/}
        {/*    labels: ['January', 'February', 'March', 'April', 'May', 'June'],*/}
        {/*    datasets: [*/}
        {/*      {*/}
        {/*        data: [20, 45, 28, 80, 99, 43],*/}
        {/*      },*/}
        {/*    ],*/}
        {/*  }}*/}
        {/*  width={Styles.constants.widthScreenMg24} // from react-native*/}
        {/*  height={Styles.constants.heightScreen / 3}*/}
        {/*  chartConfig={{*/}
        {/*    backgroundColor: 'green',*/}
        {/*    backgroundGradientFrom: 'white',*/}
        {/*    backgroundGradientTo: ColorStyle.tabWhite,*/}
        {/*    decimalPlaces: 2,*/}
        {/*    color: (opacity = 1) => 'red',*/}
        {/*  }}*/}
        {/*  style={{*/}
        {/*    marginVertical: 8,*/}
        {/*        */}
        {/*  }}*/}
        {/*/>*/}
        <LineChart
          data={{
            labels: ['22/11', '23/11', '24/11', '25/11', '26/11', '27/11'],
            datasets: [
              {
                data: [
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                ],
              },
            ],
          }}
          width={Dimensions.get('window').width} // from react-native
          height={220}
          chartConfig={{
            backgroundColor: 'green',
            backgroundGradientFrom: 'white',
            backgroundGradientTo: ColorStyle.tabWhite,
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = 1) => 'red',
            style: {
              borderRadius: 16,
            },
          }}
          style={{
            marginVertical: 8,
            borderRadius: 16,
          }}
        />
      </View>
    );
  }
  renderProduct(){
      return(
          <View
              style={{
                  marginTop: 20,
                  backgroundColor: ColorStyle.tabWhite,
                  paddingHorizontal: Styles.constants.marginHorizontalAll,
                  paddingVertical: 16,
              }}>
              <Text style={{...Styles.text.text12, color: ColorStyle.tabBlack}}>
                  {strings('productRanking')}
              </Text>


          </View>
      )
  }
  selectControl(index) {
    this.setState({selectControl: index});
  }
  _renderTabContent() {
    return (
      <TabView
        style={{
          height: Styles.constants.heightScreen / 2,
        }}
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
      />
    );
  }
}
const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    onGetCategory: param => {},
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(StatisticalScreen);
