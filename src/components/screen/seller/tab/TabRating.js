import React from 'react';
import {FlatList, View} from 'react-native';
import Styles from '../../../../resource/Styles';
import {strings} from '../../../../resource/languages/i18n';
import ItemShopRating from "../../../elements/viewItem/itemSeller/ItemShopRating";
import RatingHandle from "../../../../sagas/RatingHandle";
import EmptyView from "../../../elements/reminder/EmptyView";
export default class TabRating extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data:[]
    };
  }
  componentDidMount() {this.getData()}
  getData(){
    let param={
      type:this.props.type
    }
    RatingHandle.getInstance().getRatingStore(param,(isSuccess,res)=>{
        this.props.callback(res.average_rate)
     this.setState({data:res.data})
    })
  }
  render() {
    return (
      <View style={{flex: 1}}>
        {this.state.data.length===0 &&(
            this.renderEmptyView()
        )}
        <FlatList
            horizontal={false}
            data={this.state.data}
            style={{
              paddingHorizontal: Styles.constants.marginHorizontalAll,
            }}
            contentContainerStyle={{flexWrap: 'wrap'}}
            renderItem={this.renderItem}
        />
      </View>
    );
  }
  renderEmptyView() {
    return (
        <View style={{alignItems:'center',flex:1}}>
          <EmptyView text={strings('notRating')}/>
        </View>
    );
  }
  renderItem = ({item}) => {
    return <ItemShopRating item={item} type={this.props.type}/>;
  };
}
