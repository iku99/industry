import React from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../../resource/Styles';
import constants from '../../../../Api/constants';
import { Icon } from "react-native-elements";
import ColorStyle from "../../../../resource/ColorStyle";
import { strings } from "../../../../resource/languages/i18n";
import SellerStyles from "../SellerStyles";
export default class TabStatistical extends React.Component {
  render() {
    let data = this.props.item;
    return (
      <View>
        <FlatList data={data} renderItem={this.renderItem} />
      </View>
    );
  }
  renderItem = ({item, index}) => (
    <TouchableOpacity style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginVertical:10}}>
     <View style={SellerStyles.itemRow}>
       <Image
         source={{uri: this.getImageUrl(item)}}
         style={{...Styles.icon.icon_notification}}
       />
       <Text  style={{...Styles.text.text12,color:ColorStyle.tabBlack,marginLeft:10}}>{item.title}</Text>

     </View>
       <View style={{alignItems:'center'}}>
        <Text style={{...Styles.text.text12,color:ColorStyle.tabBlack}}>{item.count}</Text>
        <Icon name={'caretup'} type={'antdesign'} size={15} color={'#0BD27D'}/>
        <Text style={{...Styles.text.text12,color:ColorStyle.tabBlack}}>{item.viewUser} {strings('viewAndUser')}</Text>
      </View>
    </TouchableOpacity>
  );
  getImageUrl(item) {
    return constants.host +item.url;
  }
}
