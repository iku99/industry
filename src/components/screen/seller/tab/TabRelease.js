import React from 'react';
import {FlatList, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import ColorStyle from '../../../../resource/ColorStyle';
import Styles from '../../../../resource/Styles';
import {Icon} from 'react-native-elements';
import {strings} from '../../../../resource/languages/i18n';
import SellerStyles from '../SellerStyles';
import ItemNotificationOrder from '../../../elements/viewItem/itemNotification/ItemNotificationOrder';
import ItemNotificationSeller from '../../../elements/viewItem/itemSeller/ItemNotificationSeller';
import AppConstants from '../../../../resource/AppConstants';
import AppDatePicker from '../../../elements/DatePicker/AppDatePicker';
import DateUtil from '../../../../Utils/DateUtil';
import DatePicker from 'react-native-date-picker';
const listNotificationOrder = [
  {
    url: '/upload/product/2d6fa716-71df-42e7-a369-35156040100f.JPG',
    title: 'Delivery successful',
    description:
      'The package XXXXXX00 of order 123AB456 has been delivered successfully',
    created_at: '2019-10-19T05:54:03.000+00:00',
    readed: true,
  },
  {
    url: '/upload/product/2d6fa716-71df-42e7-a369-35156040100f.JPG',
    title: 'Cancellation successful',
    description:
      'Cancellation request 123ABC24 is accepted. Order 123ABC24 has been canceled successfully',
    created_at: '2019-10-19T05:54:03.000+00:00',
    readed: false,
  },
  {
    url: '/upload/product/2d6fa716-71df-42e7-a369-35156040100f.JPG',
    title: 'Delivery successful',
    description:
      'The package XXXXXX00 of order 123AB456 has been delivered successfully',
    created_at: '2019-10-19T05:54:03.000+00:00',
    readed: true,
  },
  {
    url: '/upload/product/2d6fa716-71df-42e7-a369-35156040100f.JPG',
    title: 'Cancellation successful',
    description:
      'Cancellation request 123ABC24 is accepted. Order 123ABC24 has been canceled successfully',
    created_at: '2019-10-19T05:54:03.000+00:00',
    readed: false,
  },
  {
    url: '/upload/product/2d6fa716-71df-42e7-a369-35156040100f.JPG',
    title: 'Delivery successful',
    description:
      'The package XXXXXX00 of order 123AB456 has been delivered successfully',
    created_at: '2019-10-19T05:54:03.000+00:00',
    readed: false,
  },
  {
    url: '/upload/product/2d6fa716-71df-42e7-a369-35156040100f.JPG',
    title: 'Cancellation successful',
    description:
      'Cancellation request 123ABC24 is accepted. Order 123ABC24 has been canceled successfully',
    created_at: '2019-10-19T05:54:03.000+00:00',
    readed: false,
  },
  {
    url: '/upload/product/2d6fa716-71df-42e7-a369-35156040100f.JPG',
    title: 'Delivery successful',
    description:
      'Cancellation request 123ABC24 is accepted. Order 123ABC24 has been canceled successfullyCancellation request 123ABC24 is accepted. Order 123ABC24 has been canceled successfully',
    created_at: '2019-10-19T05:54:03.000+00:00',
    readed: false,
  },
  {
    url: '/upload/product/2d6fa716-71df-42e7-a369-35156040100f.JPG',
    title: 'Cancellation successful',
    description:
      'Cancellation request 123ABC24 is accepted. Order 123ABC24 has been canceled successfully',
    created_at: '2019-10-19T05:54:03.000+00:00',
    readed: true,
  },
];
export default class TabRelease extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showDate: false,
      showDateStart:false,
      timeStart: new Date(),
      timeEnd: new Date(),
    };
  }
  componentDidMount() {
    // this.setState({timeStart:new Date()})
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <View
          style={{
            backgroundColor: ColorStyle.tabWhite,
            alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 20,
            borderColor: ColorStyle.borderItemHome,
            elevation: 2,
          }}>
          <Text
            style={{
              ...Styles.text.text24,
              color: ColorStyle.tabActive,
              fontWeight: '500',
            }}>
            120$
          </Text>
          <View style={{...SellerStyles.itemRow, marginTop: 16}}>
            <Icon
              name={'shield-checkmark-outline'}
              type={'ionicon'}
              size={24}
              color={ColorStyle.tabActive}
            />
            <Text
              style={{
                ...Styles.text.text14,
                color: ColorStyle.tabBlack,
                marginLeft: 10,
              }}>
              {strings('guarantee')}
            </Text>
          </View>
          <Text
            style={{
              ...Styles.text.text14,
              display:
                this.props.type === AppConstants.SELLER_INCOME.RELEASE
                  ? 'flex'
                  : 'none',
              color: ColorStyle.tabBlack,
              marginLeft: 10,
            }}>
            {strings('amoutReleased')}
          </Text>
          <View
            style={{
              ...SellerStyles.itemRow,
              justifyContent: 'space-between',
              paddingHorizontal: Styles.constants.marginHorizontalAll,
              marginTop:20,
              display:
                this.props.type === AppConstants.SELLER_INCOME.RELEASE
                  ? 'flex'
                  : 'none',
            }}>
            <TouchableOpacity
              style={{
                ...SellerStyles.itemRow,
                flex: 1,
                borderColor: ColorStyle.borderItemHome,
                borderWidth: 1,
                marginRight: 10,
                padding: 8,
              }}
              onPress={() => this.setState({showDateStart: true})}>
              <Text style={{...Styles.text.text12, color: ColorStyle.loading}}>
                {DateUtil.formatDate('YYYY-MM-DD', this.state.timeStart)}
              </Text>
              <Icon
                name="down"
                type="antdesign"
                size={15}
                color={ColorStyle.borderItemHome}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                ...SellerStyles.itemRow,
                flex: 1,
                borderColor: ColorStyle.borderItemHome,
                borderWidth: 1,
                marginLeft: 10,
                padding: 8,
              }}
              onPress={() => this.setState({showDate: true})}>
              <Text style={{...Styles.text.text12, color: ColorStyle.loading}}>
                {DateUtil.formatDate('YYYY-MM-DD', this.state.timeEnd)}
              </Text>
              <Icon
                name="down"
                type="antdesign"
                size={15}
                color={ColorStyle.borderItemHome}
              />
            </TouchableOpacity>
          </View>
        </View>
        {this.renderList()}
        {this.state.showDate && (
          <AppDatePicker
            date={this.state.timeEnd}
            onFinish={(isSelect, date) => {
              let preDate = this.state.timeEnd;
              if (!isSelect) {
                this.setState({showDate: false});
              } else {
                preDate = date;
                this.setState({
                  showDate: false,
                  timeEnd: preDate,
                });
              }
            }}
            maximumDate={new Date()}
            minimumDate={DateUtil.addYear(new Date(), -100)}
          />
        )}
        {this.state.showDateStart && (
          <AppDatePicker
            date={this.state.timeStart}
            onFinish={(isSelect, date) => {
              let preDate = this.state.timeStart;
              if (!isSelect) {
                this.setState({showDateStart: false});
              } else {
                preDate = date;
                this.setState({
                  showDateStart: false,
                  timeStart: preDate,
                });
              }
            }}
            maximumDate={new Date()}
            minimumDate={DateUtil.addYear(new Date(), -100)}
          />
        )}
      </View>
    );
  }
  renderList() {
    return (
      <FlatList
        horizontal={false}
        data={listNotificationOrder}
        style={{
          paddingHorizontal: Styles.constants.marginHorizontalAll,
        }}
        contentContainerStyle={{flexWrap: 'wrap'}}
        renderItem={this.renderItem}
      />
    );
  }
  renderItem = ({item}) => {
    return <ItemNotificationSeller item={item} />;
  };
}
