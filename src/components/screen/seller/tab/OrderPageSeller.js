import React, {Component} from 'react';
import {ActivityIndicator, FlatList, View} from 'react-native';
import EmptyView from '../../../elements/reminder/EmptyView';
import {strings} from '../../../../resource/languages/i18n';
import {PacmanIndicator} from 'react-native-indicators';
import AppConstants from '../../../../resource/AppConstants';
import OrderHandle from '../../../../sagas/OrderHandle';
import SimpleToast from 'react-native-simple-toast';
import ItemOrderSeller from '../../../elements/viewItem/itemOrderSeller/ItemOrderSeller';
import {EventRegister} from "react-native-event-listeners";
import RenderLoading from "../../../elements/RenderLoading";

export default class OrderPageSeller extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
      page: 0,
      canLoadData: true,
    };
  }

  componentDidMount() {

    EventRegister.addEventListener(AppConstants.EventName.ORDER_SELLER, total => {
      this.setState({data:[]},()=>this.getOrders())
    });
    this.getOrders();
  }

  render() {
    return (
      <View>
        {this.state.data.length > 0 && (
          <FlatList
            contentInset={{right: 0, top: 0, left: 0, bottom: 0}}
            showsVerticalScrollIndicator={false}
            data={this.state.data}
            keyExtractor={this.keyExtractor}
            renderItem={this._renderItem}
            style={{
              height: '100%',
              backgroundColor: 'transparent',
            }}
            onEndReachedThreshold={0.4}
            onEndReached={() => this.handleLoadMore()}
          />
        )}
        {this.state.data.length === 0 && !this.state.loading && (
          <View
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <EmptyView text={strings('emptyOrder')} />
          </View>
        )}
        <RenderLoading loading={this.state.loading}/>
      </View>
    );
  }

  keyExtractor = (item, index) => `orderPageSeller_${index.toString()}`;
  _renderItem = ({item, index}) => <View>{this.renderItem(item, index)}</View>;
  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <ActivityIndicator style={{color: '#000'}} />;
  };

  handleLoadMore() {
    if (this.state.loading) {
      return;
    }
    if (!this.state.canLoadData) {
      return;
    }
    this.setState({loading: true, page: this.state.page + 1}, () =>
      this.getOrders,
    );
  }

  renderLoading() {
    if (this.state.loading) {
      return (
        <View>
          <PacmanIndicator color="orange" size={40} />
        </View>
      );
    } else {
      return null;
    }
  }
  getOrders() {
    let param = {
      page_index: this.state.page,
      page_size: AppConstants.LIMIT,
      status: this.props.type,
      store_id:this.props.storeId,
    };
    OrderHandle.getInstance().getOrderByStore(
      param,
      (isSuccess, dataResponse) => {
        let mData = this.state.data;
        console.log('data:',dataResponse)
        let newData = [];
        if (
          isSuccess &&
          dataResponse.data !== undefined &&
          dataResponse.data
        ) {
          newData = dataResponse.data;
        }
        let data = mData.concat(newData);
        let canLoadData = newData.length === AppConstants.LIMIT;
        this.setState({data: data, canLoadData, loading: false});
      },
    );
  }

  renderItem(item, index) {
        return (
          <ItemOrderSeller
            item={item}
            index={index}
            orderFunc={(item, type) => {
              this.updateOrderStatusFunc(item, type);
            }}
            hideActionBT={this.state.isStore}
          />
        )
    }
  updateOrderStatusFunc(item,type) {
    let params = {
      status: type,
      reason: strings('userYeuCau'),
    };
    OrderHandle.getInstance().cancelOrder(
      item.id,
      params,
      (isSuccess, responseData) => {
        if (isSuccess) {
          SimpleToast.show(strings( 'completeOrderSuccessful'), SimpleToast.SHORT),
          EventRegister.emit(AppConstants.EventName.ORDER_SELLER, '');
          return
        }
        SimpleToast.show(strings(  'completeOrderFailed'), SimpleToast.SHORT)
      },

    );
  }
}
