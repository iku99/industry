import React from 'react';
import {
  FlatList,
  ScrollView,
  Switch,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Styles from '../../../resource/Styles';
import {connect} from 'react-redux';
import {getSeller} from '../../../actions';
import Toolbar from '../../elements/toolbar/Toolbar';
import {strings} from '../../../resource/languages/i18n';
import ColorStyle from '../../../resource/ColorStyle';
import SellerStyles from './SellerStyles';
import {CheckBox, Icon} from 'react-native-elements';
import NavigationUtils from '../../../Utils/NavigationUtils';
import {Actions} from 'react-native-router-flux';
import SimpleToast from 'react-native-simple-toast';
import GroupStyle from '../group/GroupStyle';
import StoreHandle from "../../../sagas/StoreHandle";
import {EventRegister} from "react-native-event-listeners";
import AppConstants from "../../../resource/AppConstants";
import ProductHandle from "../../../sagas/ProductHandle";
import ShipHandle from "../../../sagas/ShipHandle";
const Data = [
            {
                id: 1,
                name: 'Giao hàng nhanh',
            },

];
class TransportList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      storeAdd: this.props.store.address,
      listDelivery: [],
      isPage: false,
      showDialog: false,
    };
  }
  componentDidMount() {
      console.log(this.props.store)
    this.getListDelivery();
  }

  render() {
    return (
      <View style={Styles.container}>
        <Toolbar
          title={strings('transport')}
          backgroundColor={ColorStyle.tabActive}
          textColor={ColorStyle.tabWhite}
        />
        {this.getBody()}
        {this.renderBottom()}
        {this.state.showDialog && this.renderDialog()}
      </View>
    );
  }
  renderDialog() {
    return (
      <View
        style={{
          backgroundColor: 'rgba(0,0,0,0.5)',
          position: 'absolute',
          top: 0,
          length: 0,
          width: '100%',
          height: '100%',
          paddingHorizontal: '3%',
          justifyContent: 'center',
          alignItems: 'center',elevation: 3,zIndex:2
        }} >
        <View style={{...GroupStyle.modal.container, height: '30%',paddingHorizontal:Styles.constants.marginHorizontalAll,paddingVertical:30}} >
          <Text style={{...Styles.text.text24,color:ColorStyle.tabBlack,fontWeight:'700'}}>{strings('finishShop')}</Text>
          <Text style={{...Styles.text.text14,color:ColorStyle.tabBlack,lineHeight:20,textAlign:'center'}}>{strings('desFinishShop')}</Text>
          <TouchableOpacity onPress={()=>{setTimeout(()=>{Actions.jump('addProduct')},5)
              Actions.reset('drawer')}} style={{paddingHorizontal:16,paddingVertical:10,alignSelf:'center',borderColor:ColorStyle.tabActive,borderWidth:1,borderRadius:5,marginTop:20}}>
            <Text style={{...Styles.text.text18,color:ColorStyle.tabActive}}>{strings('addProduct')}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{position:'absolute',bottom:20,right:20}} onPress={()=>
          {
              setTimeout(()=>{ Actions.jump('sellerDetail')},5)
              Actions.reset('drawer')}}>
            <Text style={{...Styles.text.text18,color:ColorStyle.tabActive,fontWeight:'500'}}>{strings('skip')}</Text>
          </TouchableOpacity>

        </View>
      </View>
    );
  }
  getBody() {
      return (
        <View>
          {this.renderAddress()}
          {this.renderTitleShip()}
          {this.renderListShip()}
        </View>
      );
  }
  getListDelivery() {
    let newArray = Data.map(e => {
      return {
        ...e,
          checked: true,
      };
    });
    this.setState({listDelivery: newArray});
  }
  renderBottom() {
    return (
      <View
        style={{
          position: 'absolute',
          bottom: 0,
          ...SellerStyles.renderView,
          flexDirection: 'row',
          left: 0,
          right: 0,
          justifyContent: 'space-between',
          marginBottom: 0,
        }}>
        <TouchableOpacity
          onPress={() => Actions.pop()}
          style={{
            ...SellerStyles.btnNext,
            backgroundColor: ColorStyle.tabWhite,
            borderColor: 'rgba(151, 151, 151, 0.3)',
            elevation: 2,
          }}>
          <Text
            style={{
              ...Styles.text.text14,
              color: ColorStyle.tabActive,
              fontWeight: '500',
            }}>
            {strings('back').toUpperCase()}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.butNext()}
          style={{
            ...SellerStyles.btnNext,
            backgroundColor: ColorStyle.tabActive,
            borderColor: 'rgba(211, 38, 38, 0.25)',
            elevation: 2,
          }}>
          <Text
            style={{
              ...Styles.text.text14,
              color: ColorStyle.tabWhite,
              fontWeight: '500',
            }}>
            {strings('save').toUpperCase()}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
  renderAddress() {
    let item = this.state.storeAdd;
    return (
      <View
        style={{
          ...SellerStyles.renderView,
          paddingHorizontal: Styles.constants.X,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text
            style={{
              ...Styles.text.text18,
              color: ColorStyle.tabBlack,
              fontWeight: '500',
            }}>
              {item.address_category ? strings('company') : strings('home')}
          </Text>
          <TouchableOpacity
            onPress={() =>
              NavigationUtils.goToAddressList(item => {
                this.setState({storeAdd: item},true);
              })
            }>
            <Icon
              name={'pencil'}
              type="foundation"
              size={15}
              color={ColorStyle.tabActive}
            />
          </TouchableOpacity>
        </View>

        <Text style={{...Styles.text.text14, color: ColorStyle.tabBlack}}>
          {item.name} | {item.phone}
        </Text>
        <Text
          style={{...Styles.text.text14, color: ColorStyle.gray, marginTop: 5}}>
            {item?.address}
        </Text>
      </View>
    );
  }
  renderTitleShip() {
    return (
      <View
        style={{
          ...SellerStyles.renderView,
          paddingHorizontal: Styles.constants.marginHorizontalAll / 2,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Icon
          name={'fire'}
          type={'font-awesome-5'}
          size={15}
          color={ColorStyle.tabActive}
        />
        <Text
          style={{
            ...Styles.text.text16,
            color: ColorStyle.tabBlack,
            fontWeight: '500',
            marginLeft: 10,
          }}>
          {strings('pttt')}
        </Text>
      </View>
    );
  }
  renderListShip() {
    return (
      <FlatList
        data={this.state.listDelivery}
        renderItem={this._renderItem}
        style={{marginBottom: 100}}
      />
    );
  }
  _renderItem = ({item, index}) => {
    return (
        <TouchableOpacity
            style={{
                paddingHorizontal: Styles.constants.marginHorizontalAll,
                paddingVertical: 10,
                alignItems: 'center',
                flexDirection: 'row',
                justifyContent: 'space-between',
            }}>
            <Text
                style={{
                    ...Styles.text.text16,
                    color: ColorStyle.tabBlack,
                    fontWeight: '500',
                }}>
                {item.name}
            </Text>
            <CheckBox
                checked={item.checked}
                containerStyle={{padding: 0, borderColor: 'transparent'}}
                onPress={() => {
                    this.onChange(item);
                }}
            />
        </TouchableOpacity>
    );
  };
  onChange(item) {
    let newArray = this.state.listDelivery.map(e => {
        if (item.id === e.id) {
            return {
                ...e,
                checked: !e.checked,
            };
        }
        return {
            ...e,
            checked: false,
        };
    });
      console.log('er:',newArray)
    this.setState({listDelivery: newArray});
  }
  butNext() {
      let item=this.getListChecked()
    if (item.length===0){
        SimpleToast.show(strings('shippingInvalid'), SimpleToast.SHORT);
        return;
    }
      let address=this.props.store.address
    let params={
        district_id: address.district.ghn_id,
        ward_code: address.ward.ghn_id,
        name: this.props.store.name,
        phone: this.props.store.phone,
        address: address.address
    }
      ShipHandle.getInstance().getShip(this.props.store.id,params,
          (isSuccess, responseData) => {
              console.log(responseData)
              if (isSuccess) {

              }})
  }

  getListChecked() {
    let newData = [];
    let listDelivery = this.state.listDelivery;
    listDelivery.map(item => {
        if (item.checked === true) {
            newData.push(item);
        }
    });
    return newData;
  }
}
const mapStateToProps = state => {
  return {
    seller: state.SellerReducers,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onGetSeller: param => {
      dispatch(getSeller(param));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(TransportList);
