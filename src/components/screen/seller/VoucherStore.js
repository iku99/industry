import React, {Component} from 'react';
import {FlatList, View} from "react-native";
import Styles from "../../../resource/Styles";
import Toolbar from "../../elements/toolbar/Toolbar";
import ColorStyle from "../../../resource/ColorStyle";
import {strings} from "../../../resource/languages/i18n";
import EmptyView from "../../elements/reminder/EmptyView";
import {UIActivityIndicator} from "react-native-indicators";
import {Actions} from "react-native-router-flux";
import VoucherHandle from "../../../sagas/VoucherHandle";
import AppConstants from "../../../resource/AppConstants";
import VoucherManageItem from "../../elements/viewItem/ItemVoucher/VoucherManageItem";
import DataUtils from "../../../Utils/DataUtils";
import ViewUtils from "../../../Utils/ViewUtils";

export default class VoucherStore extends Component{
    constructor(props) {
        super(props);
        this.state={
            data:[],
            canLoadData: true,
            loading: false,
            page:1,
            total_page:0
        }
    }
    componentDidMount() {
        this.getData()
    }
    getData(){
        let params={
            page_index:this.state.page,
            page_size:10,
            type:AppConstants.GetVoucher.GET_VOUCHER_STORE,
            store_id:this.props.storeId
        }
        VoucherHandle.getInstance().getListVoucher(params,undefined,(isSuccess,responseData)=>{
            if(isSuccess && responseData.code===0){
                let data=this.state.data;
                let mData=responseData.data.data;
                // if(mData.length <5){
                //     this.setState({canLoadData:true})
                //     return
                // }
                data=data.concat(mData)

               this.setState({data:data,loading:false})
            }
        })
    }
    _renderItem =({item,index})=>{
        return(<VoucherManageItem item={item} onDelete={()=>this.onDeleteVoucher(item,index)} onChange={()=>Actions.jump('uploadVoucher',{voucher:item,callback:()=>{
                this.setState({data:[],page:1},()=>{this.getData()})
            }})}/>)
    }
    onDeleteVoucher(item,index){
        ViewUtils.showAskAlertDialog(
            strings('deleteVoucherNoti').replace('{name}',item.name),
            () => {
        VoucherHandle.getInstance().deleteVoucher(item.id,(isSuccess,responseData)=>{
            console.log(responseData)
            if(isSuccess){
                let data=this.state.data;
                let mDate=DataUtils.removeItem(data,index)
                this.setState({data:[]},()=>{
                    this.setState({data:mDate})
                })
                return
            }
            ViewUtils.showAlertDialog(strings('deleteVoucherFailed'),()=>{})
        })
            },
            null,
        );
    }

    render() {
        return (
            <View style={Styles.container}>
                <Toolbar
                    backgroundColor={ColorStyle.tabActive}
                    textColor={ColorStyle.tabWhite}
                    title={strings('voucher')}
                    add={true}
                    onPressAdd={()=>{
                        Actions.jump('uploadVoucher',{storeId:this.props.storeId,callback:(item)=>{

                            }})
                    }}
                />
                {this.state.data.length === 0 && this.renderEmptyView()}
                {this.state.data.length > 0 && (
                    <FlatList
                        contentInset={{right: 0, top: 0, left: 0, bottom: 0}}
                        showsVerticalScrollIndicator={false}
                        data={this.state.data}
                        keyExtractor={this.keyExtractor}
                        renderItem={this._renderItem}
                        ListFooterComponent={this.renderFooter.bind(this)}
                        style={{flex: 1, height: '100%', backgroundColor: 'transparent'}}
                        onEndReachedThreshold={0.4}
                        onEndReached={() => this.handleLoadMore()}
                    />
                )}
           </View>
        );
    }
    renderFooter = () => {
        //it will show indicator at the bottom of the list when data is loading otherwise it returns null
        if (!this.state.loading) {
            return null;
        }
        return <UIActivityIndicator color={ColorStyle.tabActive} size={20} />;
    };
    handleLoadMore() {
        if (this.state.loading) {
            return;
        }
        if (this.state.canLoadData) {
            return;
        }
        this.setState({loading: true, page: this.state.page + 1}, () =>
            this.getData(),
        );
    }
    renderEmptyView() {
        return (
            <View style={{flex:1,alignItems:'center'}}>
                <EmptyView text={strings('notVoucher')}/>
            </View>
        );
    }
}
