import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../resource/Styles';
import ToolbarSeller from '../../elements/toolbar/ToolbarSeller';
import {strings} from '../../../resource/languages/i18n';
import {connect} from 'react-redux';
import ColorStyle from '../../../resource/ColorStyle';
import AppConstants from '../../../resource/AppConstants';
import {SceneMap, TabView} from 'react-native-tab-view';
import AppTab from '../../elements/AppTab';
import OrderPageSeller from './tab/OrderPageSeller';
import NavigationUtils from "../../../Utils/NavigationUtils";
class MySaleScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: this.props.index === undefined ? 0 : this.props.index,
      routes: [
        {key: 'tab1', title: strings('xn')},
        {key: 'tab2', title: strings('toShip')},
        {key: 'tab3', title: strings('shipping')},
        {key: 'tab4', title: strings('completed')},
        {key: 'tab5', title: strings('cancellation')},
      ],
      isStore: true,
    };
  }

  _renderScene = SceneMap({
    tab1: () => (
      <OrderPageSeller
        type={AppConstants.STATUS_ORDER.UNCONFIRMED}
        storeId={this.props.storeId}
      />
    ),
    tab2: () => (
      <OrderPageSeller
        type={AppConstants.STATUS_ORDER.CONFIRMED}
        storeId={this.props.storeId}
      />
    ),
    tab3: () => (
      <OrderPageSeller
        type={AppConstants.STATUS_ORDER.DELIVERING}
        storeId={this.props.storeId}
        callback={()=>{

        }}
      />
    ),
    tab4: () => (
      <OrderPageSeller
        type={AppConstants.STATUS_ORDER.SUCCESS}
        storeId={this.props.storeId}
      />
    ),
    tab5: () => (
      <OrderPageSeller
        type={AppConstants.STATUS_ORDER.SHOP_CANCEL}
        storeId={this.props.storeId}
      />
    ),
  });
  _handleIndexChange = index => this.setState({index});
  _renderTabBar = props => {
    return (
      <AppTab
        data={props.navigationState.routes}
        index={this.state.index}
        onPress={i => {
          this.setState({index: i});
        }}
        ref={ref => {
          this.appTabRef = ref;
        }}
      />
    );
  };
  _renderTabContent() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
      />
    );
  }
  render() {
    return (
      <View style={Styles.container}>
        <ToolbarSeller
          title={strings('sales')}
          onPressMess={() => {NavigationUtils.goToCartScreen()}}
          message={true}
          backgroundColor={ColorStyle.tabActive}
        />
        {this._renderTabContent()}
      </View>
    );
  }
  onDataChange(item, index, preStatus, newStatus) {
    // if (this.toPay != null) {
    //   this.toPay.onDataChange(item, index, preStatus, newStatus);
    // }
    // if (this.unconfirmed != null) {
    //   this.unconfirmed.onDataChange(item, index, preStatus, newStatus);
    // }
    // if (this.confirmed != null) {
    //   this.confirmed.onDataChange(item, index, preStatus, newStatus);
    // }
    // if (this.delivering != null) {
    //   this.delivering.onDataChange(item, index, preStatus, newStatus);
    // }
    // if (this.cancelled != null) {
    //   this.cancelled.onDataChange(item, index, preStatus, newStatus);
    // }
    // if (this.shop_cancel != null) {
    //   this.shop_cancel.onDataChange(item, index, preStatus, newStatus);
    // }
    // if (this.success != null) {
    //   this.success.onDataChange(item, index, preStatus, newStatus);
    // }
  }
}
const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    onGetCategory: param => {},
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(MySaleScreen);
