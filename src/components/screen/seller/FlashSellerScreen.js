import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../resource/Styles';
import {connect} from 'react-redux';
import {getCategoryAction} from '../../../actions';
import SellerStyles from './SellerStyles';
import {strings} from '../../../resource/languages/i18n';
import ColorStyle from '../../../resource/ColorStyle';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import ImageHelper from '../../../resource/images/ImageHelper';
import GlobalInfo from '../../../Utils/Common/GlobalInfo';
import StoreHandle from '../../../sagas/StoreHandle';
class FlashSellerScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasStore: false,
    };
  }
  componentDidMount() {
  }

  render() {
    return (
      <View style={Styles.container}>
        {this.toolbar()}
        <Image source={ImageHelper.bgSeller} style={SellerStyles.bg} />
        <Text style={SellerStyles.textWelcom}>{strings('textWelcome')}</Text>
        <TouchableOpacity
          onPress={() => Actions.jump('registerStore')}
          style={SellerStyles.buttonRe}>
          <Text
            style={{
              ...Styles.text.text20,
              color: ColorStyle.tabWhite,
              fontWeight: '700',
            }}>
            {strings('register').toUpperCase()}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
  toolbar() {
    return (
      <View style={SellerStyles.toolbar.container}>
        <Text
          style={{
            ...Styles.text.text24,
            color: ColorStyle.tabWhite,
            fontWeight: '700',
          }}>
          {strings('welcome')}
        </Text>
        <TouchableOpacity
          onPress={() => Actions.pop()}
          style={{
            borderRadius: 200,
            position: 'absolute',
            paddingVertical: Styles.constants.X * 0.8,
            paddingHorizontal: Styles.constants.X * 0.4,
            left: 0,
            alignSelf: 'center',
          }}>
          <Icon
            name="left"
            type="antdesign"
            size={25}
            color={ColorStyle.tabWhite}
          />
        </TouchableOpacity>
      </View>
    );
  }

}
const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    onGetCategory: param => {},
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(FlashSellerScreen);
