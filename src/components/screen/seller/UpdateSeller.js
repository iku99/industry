import React from 'react';
import {Image, ScrollView, Text, TextInput, TouchableOpacity, View} from "react-native";
import StoreHandle from "../../../sagas/StoreHandle";
import Styles from "../../../resource/Styles";
import Toolbar from "../../elements/toolbar/Toolbar";
import {strings} from "../../../resource/languages/i18n";
import ColorStyle from "../../../resource/ColorStyle";
import StoreStyle from "../store/StoreStyle";
import constants from "../../../Api/constants";
import {Icon} from "react-native-elements";
import PermissionUtils from "../../../Utils/PermissionUtils";
import {PERMISSIONS} from "react-native-permissions";
import ImagePicker from "react-native-image-crop-picker";
import MediaHandle from "../../../sagas/MediaHandle";
import SellerStyles from "./SellerStyles";
import DataUtils from "../../../Utils/DataUtils";
import NavigationUtils from "../../../Utils/NavigationUtils";
import {Actions} from "react-native-router-flux";
import SimpleToast from "react-native-simple-toast";
import ViewUtils from "../../../Utils/ViewUtils";
export default class UpdateSeller extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            store:{},
            name:'',
            phone:'',
            email:'',
            des:'',
            content:'',
            avatar:undefined,
            cover:undefined,
            address:undefined,
            businessTypeIndex: 0,
            //
            validName:false,
            validPhone:false,
            validEmail:false,
            validAddress:false,
            validDes:false,
            //
        }
    }
    componentDidMount() {
        this.getStoreDetail()
    }
    render() {
        return(
            <View style={Styles.container}>
                <ScrollView  style={{position:'absolute',width:'100%',height:"100%", top:Styles.constants.X}}>
                    {this.renderImageCover()}
                    {this.renderImageAvatar()}
                    {this.renderBody()}
                    {this.renderBottom()}
                    <View style={{marginBottom:Styles.constants.X*1.5}}/>
                </ScrollView>
                <Toolbar title={strings('updateSeller')} textColor={ColorStyle.tabWhite} backgroundColor={ColorStyle.tabActive}/>
            </View>
        )
    }
    renderImageCover(){
        if(this.state.cover===undefined || this.state.cover===null){
            return(
                <View style={{ ...StoreStyle.bannerStore}}>
                    <View style={{backgroundColor:ColorStyle.borderButton,width:'100%',height:'100%'}}/>
                    <TouchableOpacity onPress={()=>this.getImageCover('cover')} style={SellerStyles.btnSettingCover}>
                        <Icon name={'setting'} type={'antdesign'} size={25} color={ColorStyle.tabActive}/>
                    </TouchableOpacity>
                </View>
            )
        }else
            return(
                <View style={{ ...StoreStyle.bannerStore}}>
                    <Image source={{uri:constants.host+this.state.cover}} style={{width:'100%',height:'100%'}}/>
                    <TouchableOpacity onPress={()=>this.getImageCover('cover')} style={SellerStyles.btnSettingCover}>
                        <Icon name={'setting'} type={'antdesign'} size={25} color={ColorStyle.tabActive}/>
                    </TouchableOpacity>
                </View>
            )
    }
    renderImageAvatar(){
        if(this.state.avatar===undefined || this.state.avatar===null){
            return(
                <View style={SellerStyles.viewAvatar}>
                    <View  style={SellerStyles.img_avatar}/>
                    <TouchableOpacity onPress={()=>this.getImageCover('avatar')} style={{ position:'absolute',bottom:0,alignSelf:'center',backgroundColor:ColorStyle.buttonControl,width:'100%',paddingVertical:5}}>
                        <Icon name={'setting'} type={'antdesign'} size={25} color={ColorStyle.tabActive}/>
                    </TouchableOpacity>
                </View>
            )
        }else
            return(
                <View style={{ ...SellerStyles.viewAvatar}}>
                    <Image source={{uri:constants.host+this.state.avatar}} style={SellerStyles.img_avatar}/>
                    <TouchableOpacity onPress={()=>this.getImageCover('avatar')} style={{  position:'absolute',bottom:0,alignSelf:'center',backgroundColor:ColorStyle.buttonControl,width:'100%',paddingVertical:5}}>
                        <Icon name={'setting'} type={'antdesign'} size={25} color={ColorStyle.tabActive}/>
                    </TouchableOpacity>
                </View>
            )
    }
    renderBody(){
        return(
            <View style={SellerStyles.renderBody}>
                {this.renderTextInputWithTitle(
                    strings('name'),
                    'name',
                    'validName',
                    'checkAddId',
                    () => this.checkName(),
                )}
                {this.renderTextInputWithTitle(
                    strings('email'),
                    'email',
                    'validEmail',
                    'checkAddId',
                    () => this.checkEmail(),
                )}
                {this.renderTextInputWithTitle(
                    strings('phone'),
                    'phone',
                    'validPhone',
                    'checkAddId',
                    () => this.checkPhone(),)}
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 16,
                    }}>
                    <Text
                        style={{
                            flex: 1,
                            ...Styles.text.text14,
                            color: ColorStyle.tabBlack,
                            fontWeight: '500',
                        }}>
                        {strings('address1')}
                        <Text
                            style={{
                                ...Styles.text.text14,
                                color: ColorStyle.tabActive,
                                fontWeight: '500',
                                paddingLeft: 10,
                            }}>
                            *
                        </Text>
                    </Text>
                    <TouchableOpacity
                        onPress={() =>
                            NavigationUtils.goToAddressList(item => {
                                this.setState({address: item.address_detail});
                            },true)
                        }
                        style={{
                            ...Styles.input.codeInput,
                            flex: 2,
                            borderWidth: 0.5,
                            elevation: 2,
                            borderColor: '#BABABA',
                            flexDirection: 'row',
                        }}>
                        <Text
                            style={{
                                ...Styles.text.text11,
                                color: ColorStyle.gray,
                                flex: 3,
                            }}
                            numberOfLines={1}>
                            {this.state.address?.address}
                        </Text>
                        <Icon
                            name={'right'}
                            type="antdesign"
                            size={15}
                            color={ColorStyle.gray}
                            style={{marginRight: 10}}
                        />
                    </TouchableOpacity>
                </View>
                {this.renderDesProduct(strings('description'),'des','validDes','checkAddId',)}
            </View>
        )
    }
    renderBottom(){
        return(
            <View style={{
                marginTop:16,
                paddingHorizontal:Styles.constants.marginHorizontal20,
                flexDirection: 'row',justifyContent:'space-between'}}>
                <TouchableOpacity   onPress={() => Actions.pop()}
                                    style={{
                                        ...SellerStyles.btnNext,
                                        backgroundColor: ColorStyle.tabWhite,
                                        borderColor: 'rgba(151, 151, 151, 0.3)',
                                        elevation: 2,
                                        shadowColor: 'rgba(151, 151, 151, 0.3)',
                                        shadowOffset: { width: 0, height: 1 },
                                        shadowOpacity: 0.8,
                                        shadowRadius: 1,
                                    }}>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabActive,
                            fontWeight: '500',
                        }}>
                        {strings('back').toUpperCase()}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.updateSeller()}
                                  style={{
                                      ...SellerStyles.btnNext,
                                      backgroundColor: ColorStyle.tabActive,
                                      borderColor: 'rgba(211, 38, 38, 0.25)',
                                      elevation: 2,
                                      shadowColor:'rgba(211, 38, 38, 0.25)',
                                      shadowOffset: { width: 0, height: 1 },
                                      shadowOpacity: 0.8,
                                      shadowRadius: 1,
                                  }}>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabWhite,
                            fontWeight: '500',
                        }}>{strings('save').toUpperCase()}</Text>
                </TouchableOpacity>
            </View>
        )
    }
    renderDesProduct( title,
                      stateName,
                      validStateName,
                      checkId,
                      checkFunc,) {
        return (
            <View style={{  flexDirection: 'row',
                alignItems: 'center',
                marginTop: 16}}>
                <Text style={{...Styles.text.text14,fontWeight:'500',flex:1}}>
                    {title}*
                </Text>
                <TextInput
                    style={{
                        ...Styles.input.codeInput,
                        flex:2,
                        height:Styles.constants.X*2,
                        paddingVertical: 10,
                        borderWidth: 0.5,
                        paddingLeft: 10,
                        borderColor: '#BABABA',
                        color:ColorStyle.tabBlack
                    }}
                    textAlignVertical={'top'}
                    value={this.state[stateName]}
                    placeholder={title}
                    // maxLength={3000}
                    multiline
                    numberOfLines={5}
                    editable
                    onChangeText={text => {
                        this.setState({[stateName]: text}, () => {
                            if (!DataUtils.stringNullOrEmpty(checkId) && checkFunc != null) {
                                clearTimeout(this[checkId]);
                                this[checkId] = setTimeout(() => {
                                    checkFunc();
                                }, 500);
                            }
                        });
                    }}
                />
            </View>
        );
    }
    renderTextInputWithTitle(
        title,
        stateName,
        validStateName,
        checkId,
        checkFunc,
    ) {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 16,
                }}>
                <Text
                    style={{
                        flex: 1,
                        ...Styles.text.text14,
                        color: ColorStyle.tabBlack,
                        fontWeight: '500',
                    }}>
                    {title}
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabActive,
                            fontWeight: '500',
                        }}>
                        *
                    </Text>
                </Text>
                <TextInput
                    style={{
                        ...Styles.input.codeInput,
                        flex: 2,
                        borderWidth: 0.5,
                        elevation: 2,
                        paddingLeft: 10,
                        borderColor: '#BABABA',
                        color:ColorStyle.tabBlack
                    }}
                    value={this.state[stateName]}
                    placeholder={title}
                    maxLength={30}
                    onChangeText={text => {
                        this.setState({[stateName]: text}, () => {
                            if (!DataUtils.stringNullOrEmpty(checkId) && checkFunc != null) {
                                clearTimeout(this[checkId]);
                                this[checkId] = setTimeout(() => {
                                    checkFunc();
                                }, 500);
                            }
                        });
                    }}
                />
            </View>
        );
    }
    getImageCover(item){
        if (
            PermissionUtils.checkPermission(PERMISSIONS.IOS.PHOTO_LIBRARY)
        ) {
            ImagePicker.openPicker({
                width: 400,
                height: 400,
                compressImageMaxWidth: 800,
                compressImageMaxHeight: 800,
                cropping: true,
            }).then(image => {
                MediaHandle.getInstance().uploadProfileAvatar(image,(_)=>{
                },(isSuccess, responseData)=>{
                    if(isSuccess){
                        let data=responseData._response;
                        this.setState({[item]: JSON.parse(data).url});
                    }
                })
            });
        }
    }
    checkName() {
        return this.setState({validName: this.state.name?.trim().length > 5});
    }
    checkEmail() {
        return this.setState({
            validEmail: DataUtils.validMail(this.state.email),
        });
    }
    checkPhone() {
        return this.setState({
            validPhone: DataUtils.validPhone(this.state.phone),
        });
    }
    checkDes() {
        return this.setState({validDes: this.state.des.length > 10});
    }
    getStoreDetail() {
        StoreHandle.getInstance().getMyStore((isSuccess, res) => {
            let store=res.data;
            if (isSuccess) {
                if (store === undefined) {
                    store = {};
                }
                this.setState({store,
                        name:store.name,
                        phone:store.phone,
                        des:store.des===null?'':store.des,
                        email:store.email,
                        cover :store.image_url.length===0?'':store.image_url[0].url,
                        avatar:store.avatar===null?'':store.avatar,
                        address:store.address},()=>{
                        this.checkName(),
                            this.checkPhone(),
                            this.checkEmail(),
                            this.checkDes()
                    }
                );
            }
        });
    }
    updateSeller(){
        let params={
            name:this.state.name,
            phone:this.state.phone,
            email:this.state.email,
            des:this.state.des,
            image_url:[this.state.cover],
            avatar:this.state.avatar,
            address:this.state.address
        }
        if (!this.state.validName || !this.state.validPhone || !this.state.validEmail  || !this.state.des.length > 10) {
            SimpleToast.show(strings('dataInvalid'), SimpleToast.SHORT);
            return;
        }
        if (!this.state.address.length > 10) {
            SimpleToast.show(strings('dataInvalid'), SimpleToast.SHORT);
            return;
        }
        StoreHandle.getInstance().updateStore(params, (isSuccess, store) => {
            console.log(store)
            if (isSuccess) {
                ViewUtils.showAlertDialog(strings('updateSeller'),()=>{
                    this.props.callBackFunc(),
                        Actions.pop()
                });
            }
            else ViewUtils.showAlertDialog(strings('updateSellerFailed'));
        });
    }
}
