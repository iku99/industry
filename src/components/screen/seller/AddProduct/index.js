import AppConstants from "../../../../resource/AppConstants";
import React from "react";
import ProductHandle from "../../../../sagas/ProductHandle";
import constants from "../../../../Api/constants";
import CategoryHandle from "../../../../sagas/CategoryHandle";
import {FlatList, Image, Text, TextInput, TouchableOpacity, View} from "react-native";
import Styles from "../../../../resource/Styles";
import Toolbar from "../../../elements/toolbar/Toolbar";
import ColorStyle from "../../../../resource/ColorStyle";
import {strings} from "../../../../resource/languages/i18n";
import SellerStyles from "../SellerStyles";
import {CheckBox, Icon} from "react-native-elements";
import DataUtils from "../../../../Utils/DataUtils";
import {Actions} from "react-native-router-flux";
import ViewUtils from "../../../../Utils/ViewUtils";
import SimpleToast from "react-native-simple-toast";
import ListImageProduct from "./row/ListImageProduct";
import InfoProduct from "./row/InfoProduct";
import CategoryProduct from "./row/CategoryProduct";
import ListItemProduct from "./row/ListItemProduct";
import AddProductUtils from "../../../../Utils/AddProductUtils";
import MediaHandle from "../../../../sagas/MediaHandle";

let validGroupName = "validGroupName";
let validGroupValue = "validGroupValue";
let validOptionPrice = "validOptionPrice";
let validOptionFinalPrice = "validOptionFinalPrice";
let validOptionSku = "validOptionSku";
let validOptionQty = "validOptionQty";
let validProductFeature = "validProductFeature";
let finalPriceFinalOptionTxt = "finalPriceFinalOptionTxt";
let priceOptionTxt = "priceOptionTxt";

const DataDefau=[
    {
        type:AppConstants.AddProductType.LIST_IMAGE
    },
    {
        type:AppConstants.AddProductType.INFO_PRODUCT
    },
    {
        type:AppConstants.AddProductType.LIST_CATEGORY
    },
    {
        type:AppConstants.AddProductType.LIST_COM
    },
    {
        type:AppConstants.AddProductType.GROUP_OPTION
    },
    {
        type:AppConstants.AddProductType.END
    },
]
export default class AddProduct extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data:DataDefau,
            productId: undefined,
            products: [],
            productName: '',
            validProductName: false,
            validProductDes: false,
            validProductContent:false,
            validPrice: false,
            validFinalPrice: false,
            validSku: false,
            validQty: false,
            validWeight: false,
            validLength: false,
            validWidth: false,
            validHeight: false,
            menuList: [],
            menu: undefined,
            shortDes: '',
            content:'',
            price: 0,
            finalPrice: 0,
            sku: '',
            quantity: 0,
            weight: 0,
            width: 0,
            height: 0,
            length: 0,
            categories: {},
            groupOptions: [], //Max la 2
            options: [],
            categoryId: 0,
            imageList: [],
            features: [],
            images: [],
            storeId:this.props.storeId,
            categoryCha:undefined,
            categoryCon:undefined,
            listFeatures:[]
        };
    }
    componentDidMount() {
        this.getCategories(0);
        let productId = this.props.productId;
        if (productId !== undefined) {
            this.setState({productId}, () => {
                this.getProductInfo();
            });
        }
    }
    getProductInfo() {
        ProductHandle.getInstance().getProductDetail(
            this.props.productId,undefined,
            (isSuccess, dataResponse) => {
                if (isSuccess) {
                    let product = dataResponse.data;
                    let images =  this.state.imageList;
                    for (let i=0;i<product.image_url.length;i++){
                        let item=  {
                            node: {
                                image: {
                                    uri: constants.host+product.image_url[i].url,
                                },
                            },
                        };
                        images.push(item)
                    }
                    let categoryId = product.category.id;
                    let categories = this.state.categories;
                    let category = {
                        id: categoryId,
                        features:product.features,
                        property:product.property
                    };
                    categories[categoryId] = category;
                    this.setState(
                        {
                            categoryCon:product.category.id,
                            productName: product.name,
                            shortDes: product.des===null?'':product.des,
                            price: product.price,
                            finalPrice:product.original_price,
                            storeId:product.store_id,
                            quantity: product.quantity,
                            content:product.content===null?'':product.content,
                            categoryId: categoryId,
                            imageList: images,
                            sku:product?.code===undefined?'':product.code,
                            features:product.features,
                            categories,
                            "height": product.size.height,
                            "length": product.size.length,
                            "weight":product.size.weight,
                            "width": product.size.width,
                        },
                        () => {
                            this.checkProductName();
                            this.checkProductDes();
                            this.checkProductContent();
                            this.getCategoryDetail(categoryId);
                            this.checkSku(product?.code===undefined?'':product.code);
                            this.checkPriceValid();
                            this.checkQty();
                            // this.checkOptionList();
                        },
                    );
                }
            },
        );
    }
    getCategoryDetail(id){
        CategoryHandle.getInstance().getCategoryDetail(id, (isSuccess, responseData)=>{
            if(isSuccess){
                if(responseData.data != null) {
                    let mCategory = responseData.data;
                    let parentId = mCategory.parent_id;
                    let param = {
                        parent_id:parentId
                    };
                    console.log('category:',id,responseData.data)
                    CategoryHandle.getInstance().getCategoryMenu(param,(isSuccess, responseData) =>{
                        let data = [];
                        let categories = this.state.categories;
                        if(isSuccess){
                            if(responseData.data != null){
                                data = responseData.data;
                                let category ={
                                    list:data,
                                    id:parentId,
                                    selected:mCategory
                                };
                                categories[parentId] = category;
                                if(parentId===0){
                                    this.getProductFeatures(this.state.categoryId)
                                    this.getProductProperty(this.state.categoryId)
                                }
                                this.setState({categories},()=>{
                                    if(parentId !== 0){
                                        this.getCategoryDetail(parentId)
                                    }
                                })
                            }
                        }else {
                            // ViewUtil.showAlertDialog(strings('cannotGetCategoryList') + id,null)
                        }
                    })
                }
            }else {
                // ViewUtil.showAlertDialog(strings('cannotGetCategoryDetail') + id,null)
            }
        })

    }
    setCategoryInfo(categoryId) {
        this.setState({categoryId});
    }
    render() {
        return (
            <View style={Styles.container}>
                <Toolbar
                    backgroundColor={ColorStyle.tabActive}
                    textColor={ColorStyle.tabWhite}
                    title={
                        this.props.productId !== undefined
                            ? strings('editProduct')
                            : strings('addProduct')
                    }
                />
                <FlatList style={{flex:1}}
                          data={this.state.data}
                          showsVerticalScrollIndicator={false}
                          renderItem={({item,index})=>this.renderItem(item,index)}/>
            </View>
        );
    }
    renderItem=(item,index)=>{
        switch (item.type) {
            case AppConstants.AddProductType.LIST_IMAGE:
                return <ListImageProduct image={this.state.imageList} callback={(image)=>this.setState({listImage:image})}/>;
            case AppConstants.AddProductType.INFO_PRODUCT:
                return <InfoProduct productName={this.state.productName}
                                    content={this.state.content}
                                    des={this.state.shortDes}
                                    onChangeName={text=> this.setState({productName:text,validProductName: text.length > 5})}
                                    onChangeDes={text=>this.setState({shortDes:text,validProductDes: text.length > 5})}
                                    onChangeContent={text=>this.setState({content:text,validProductContent:text.length > 5})}
                />;
            case AppConstants.AddProductType.LIST_CATEGORY:
                return <CategoryProduct item={this.state.categories}
                                        callbackId={(id)=>this.setState({categoryId:id})}
                                        callback={(data)=>this.setState({categories:data})}
                />;
            case AppConstants.AddProductType.LIST_COM:
                return <ListItemProduct code={this.state.sku}
                                        onChangeCode={(text)=>this.setState({sku:text,validSku:text.length>7})}
                                        price={this.state.price}
                                        onChangePrice={text=>this.setState({price:text},()=>{
                                            this.checkPriceValid()
                                        })}
                                        finalPrice={this.state.finalPrice}
                                        onChangeFinalPrice={text=>this.setState({finalPrice:text},()=>{
                                            this.checkPriceValid()
                                        })}
                                        width={this.state.width}
                                        onChangeWidth={text=>this.setState({width:parseInt(text),validWidth:text===0})}
                                        length={this.state.length}
                                        onChangeLength={text=>this.setState({length:parseInt(text),validLength:text===0})}
                                        height={this.state.height}
                                        onChangeHeight={text=>this.setState({height:parseInt(text),validHeight:text===0})}
                                        weight={this.state.weight}
                                        onChangeWeight={text=>this.setState({weight:parseInt(text),validWeight:text===0})}
                                        quantity={this.state.quantity }
                                        onChangeQuantity={text=>this.setState({quantity:text,validQty: (text + '').length > 0 && !isNaN(text)})}
                />;
            // case AppConstants.AddProductType.GROUP_OPTION:
            //     return <GroupOption item={this.state.groupOptions}
            //                         callback={(groupOptions)=> this.setState({groupOptions})}/>;
            case AppConstants.AddProductType.END:
                return this.renderBottom();
            default:
                break;
        }
    }
    checkOptionList() {
        let options = this.state.options;
        if (options === undefined) {
            options = [];
        }
        options.forEach((option, index) => {
            this.checkOptionPriceValid(index);
            this[`${validOptionSku}${index}`] = !DataUtils.stringNullOrEmpty(
                option.sku,
            );
            this[`${validOptionQty}${index}`] = !isNaN(option.qty);
        });
    }
    checkPriceValid() {
        let validPrice = false;
        let validFinalPrice = false;
        let price = 0;
        let finalPrice = 0;
        try {
            price = parseInt(this.state.price) ;
            finalPrice = parseInt(this.state.finalPrice);
        } catch (e) {}
        console.log(price <= finalPrice)
        if (!isNaN(finalPrice)) {
            validFinalPrice= true;
        }
        if (!isNaN(price) && price <= finalPrice ) {
            validPrice= true;
        }
        this.setState({validPrice, validFinalPrice});
    }
    checkOptionPriceValid(index) {
        let validPrice = false;
        let validFinalPrice = false;
        let price = 0;
        let finalPrice = 0;
        let option = this.state.options[index];
        if (option === undefined) {
            return;
        }
        try {
            price = option.original_price;
            finalPrice = option.price;
        } catch (e) {
            console.log(e);
        }
        if (!isNaN(price)) {
            validPrice = true;
        } else {
            validPrice = false;
        }
        if (!isNaN(finalPrice) && price > finalPrice) {
            validFinalPrice = true;
        } else {
            validFinalPrice = false;
        }
        this.setState({
            [`${validOptionFinalPrice}${index}`]: validFinalPrice,
            [`${validOptionPrice}${index}`]: validPrice,
        });
    }
    checkQty() {
        let qty = this.state.quantity;
        this.setState({validQty: (qty + '').length > 0 && !isNaN(qty)});
    }
    checkProductName() {
        this.setState({validProductName: this.state.productName.length > 5});
    }
    checkProductDes() {
        this.setState({validProductDes: this.state.shortDes.length > 5});
    }
    checkProductContent() {
        this.setState({validProductContent: this.state.content.length > 5});
    }
    checkSku(code){
        this.setState({validSku:code.length>7})
    }
    //
    renderProductFeatures(id) {
        let categories = this.state.categories;
        if(categories === undefined || categories[id] === undefined) return null;
        let category = categories[id];
        let features = category.features;
        if(DataUtils.listNullOrEmpty(features)) return null;
        let items = features.map((item, index)=>{
            return(
                <View style={{marginVertical:10}}>
                    <Text style={{fontSize:12,color:ColorStyle.tabBlack}}>{item.name}</Text>
                    <TextInput   style={{
                        ...Styles.input.codeInput,
                        flex: 2,
                        paddingVertical: 10,
                        borderWidth: 0.5,
                        marginTop: 16,
                        paddingLeft: 10,
                        borderColor: '#BABABA',

                    }} value={item.value}
                                 onChangeText={(text => {
                                     item = {
                                         ...item,
                                         value:text,
                                     };
                                     features[index] = item;
                                     category= {
                                         ...category,
                                         features:features
                                     };
                                     categories[id] = category;
                                     this.setState({categories},)})}
                    />
                </View>
            )
        });
        return (
            <View>
                <View style={SellerStyles.itemRow}>
                    <Text
                        style={{
                            ...Styles.text.text16,
                            color: ColorStyle.tabBlack,
                            fontWeight:'600'
                        }}>
                        {strings('features')}
                    </Text>
                </View>
                {items}
            </View>
        )
    }
    renderBottom() {
        let productId = this.props.productId;
        if (productId !== undefined) {
            return (
                <View
                    style={{
                        ...SellerStyles.renderView,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        paddingVertical:Styles.constants.X/2

                    }}>
                    <TouchableOpacity
                        onPress={() => this.deleteProduct(productId)}
                        style={{
                            ...SellerStyles.btnNext,
                            backgroundColor: ColorStyle.tabWhite,
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 2,
                                height: 2,
                            },
                            shadowOpacity: 0.3,
                            shadowRadius: 2.62,

                            elevation: 4,
                        }}>
                        <Text
                            style={{
                                ...Styles.text.text14,
                                color: ColorStyle.tabActive,
                                fontWeight: '500',
                            }}>
                            {strings('delete').toUpperCase()}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.onSave()}
                        style={{
                            ...SellerStyles.btnNext,
                            backgroundColor: ColorStyle.tabActive,
                            borderColor: 'rgba(211, 38, 38, 0.25)',
                            elevation: 2,
                        }}>
                        <Text
                            style={{
                                ...Styles.text.text14,
                                color: ColorStyle.tabWhite,
                                fontWeight: '500',
                            }}>
                            {strings('save').toUpperCase()}
                        </Text>
                    </TouchableOpacity>
                </View>
            );
        }
        return (
            <View
                style={{
                     ...SellerStyles.renderView,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginVertical: Styles.constants.X/2,

                }}>
                <TouchableOpacity
                    onPress={() => Actions.pop()}
                    style={{
                        ...SellerStyles.btnNext,
                        backgroundColor: ColorStyle.tabWhite,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 1,
                        },
                        shadowOpacity: 0.20,
                        shadowRadius: 1.41,

                        elevation: 2,
                    }}>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabActive,
                            fontWeight: '500',
                        }}>
                        {strings('back').toUpperCase()}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => this.onSave()}
                    style={{
                        ...SellerStyles.btnNext,
                        backgroundColor: ColorStyle.tabActive,
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 1,
                        },
                        shadowOpacity: 0.20,
                        shadowRadius: 1.41,
                        elevation: 2,
                    }}>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabWhite,
                            fontWeight: '500',
                        }}>
                        {strings('save').toUpperCase()}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
    //get category
    getCategories(parentId) {
        this.setCategoryInfo(parentId);
        let param = {
            parent_id: parentId,
        };
        CategoryHandle.getInstance().getCategoryChildren(
            param,
            (isSuccess, responseData) => {
                let data = [];
                let categories = this.state.categories;
                if (isSuccess) {
                    if (responseData.data != null) {
                        data = responseData.data;
                        let category = {
                            list: data,
                            id: parentId,
                        };
                        categories[parentId] = category;
                        this.setState({categories, categoryId: parentId});
                    }
                }
                if(data.length ===0 && DataUtils.listNullOrEmpty(categories[parentId].features)){
                    // this.getProductFeatures(this.state.categoryCha,this.state.categoryCon)
                }

            },
        );
    }
    getProductFeatures(id) {
        let params = {
            category_id:id,
        };
        ProductHandle.getInstance().getProductFeature(
            params,
            (isSuccess, responseData) => {
                if (isSuccess) {
                    if (responseData.data != null ) {
                        let features = responseData.data;
                        let mCategories = this.state.categories;
                        let category = mCategories[id].features;

                        if(this.props.productId !== undefined){
                            let data=AddProductUtils.convertProperty(features,category)
                            category ={
                                ...mCategories[id],
                                features:data,
                            };
                            mCategories[id] = category;
                            this.setState({categories:mCategories})
                            return
                        }
                        category ={
                            ...mCategories[id],
                            features:features,
                        };
                        mCategories[id] = category;
                        this.setState({categories:mCategories})
                    }
                }
            },
        );
    }
    getProductProperty(id) {
        let params = {
            category_id:id,
        };
        ProductHandle.getInstance().getProductProperty(
            params,
            (isSuccess, responseData) => {
                if (isSuccess) {
                    if (responseData.data != null ) {
                        let property = responseData.data;
                        let mCategories = this.state.categories;
                        let category = mCategories[id].property;
                        if(this.props.productId !== undefined){
                            let data=AddProductUtils.convertProperty(property,category)
                            category ={
                                ...mCategories[id],
                                property:data,
                            };

                            mCategories[id] = category;
                            this.setState({categories:mCategories})
                            return
                        }

                         category ={
                            ...mCategories[id],
                            property:property,
                        };
                        mCategories[id] = category;
                        this.setState({categories:mCategories})
                    }
                }
            },
        );
    }
    onPostUpload(params, images) {
        ProductHandle.getInstance().uploadProducts(
            params,
            (isSuccess, responseData) => {
                console.log( responseData)
                if (isSuccess && responseData.code ===0) {
                    ViewUtils.showAlertDialog(strings('createProductSuccess'), () => {
                        this.props.onCallback();
                        Actions.pop();
                    });
                    return
                }
                ViewUtils.showAlertDialog(strings('createProductFailed'), () => {
                });
            },
        );
    }
    onUpdateProduct(productId, params, images) {
        ProductHandle.getInstance().updateProducts(
            productId,
            params,
            (isSuccess, responseData) => {
                if (isSuccess && responseData.code === 0) {
                    ViewUtils.showAlertDialog(strings('updateProductSuccess'), () => {
                        this.props.onCallback();
                        Actions.pop();
                    });
                    return
                }else ViewUtils.showAlertDialog(strings('updateProductSuccess'),)
            },
        );
    }
    deleteProduct(product, index) {
        ViewUtils.showAskAlertDialog(
            strings('deleteProductNoti').replace('...',this.state.productName),
            () => {
                let ids = this.props.productId;
                ProductHandle.getInstance().deleteProducts(
                    ids,
                    (isSuccess, responseData) => {
                        console.log(responseData)
                        if (isSuccess) {
                            SimpleToast.show(
                                strings('deleteProductSuccess'),
                                SimpleToast.SHORT,
                            );
                            this.props.onCallback(false, '', this.props.index);
                            Actions.pop()
                        } else {
                            SimpleToast.show(
                                strings('deleteProductFailed'),
                                SimpleToast.SHORT,
                            );
                        }
                    },
                );
            },
            null,
        );
    }
    onSave() {
        // validate
        if (DataUtils.listNullOrEmpty(this.state.imageList)) {
            SimpleToast.show(strings('productImageEmpty'), SimpleToast.SHORT);
            return;
        }
        else if (
            !this.state.validProductName
        ) {
            SimpleToast.show(strings('validProductName'), SimpleToast.SHORT);
            return;
        }
        else if(this.state.categoryId === undefined){
            SimpleToast.show(strings('validateCategoryId'), SimpleToast.SHORT);
            return;
        }else if(!this.state.validProductDes){
            SimpleToast.show(strings('validProductDes'), SimpleToast.SHORT);
            return;
        }else if(!this.state.validProductContent){
            SimpleToast.show(strings('validProductContent'), SimpleToast.SHORT);
            return;
        }
        else if (!this.state.validFinalPrice) {
            SimpleToast.show(strings('validProductPrice'), SimpleToast.SHORT);
            return;
        }
        else if (!this.state.validPrice) {
            SimpleToast.show(strings('validProductFiPrice'), SimpleToast.SHORT);
            return;
        }
        if( this.state.validHeight ||
            this.state.validWeight ||
            this.state.validLength ||
            this.state.validWidth  ){
            SimpleToast.show(strings('vldSize'), SimpleToast.SHORT);
            return;
        }
        if( !this.state.validSku){
            SimpleToast.show(strings('vldCode'), SimpleToast.SHORT);
            return;
        }
        let image=this.state.imageList;
        MediaHandle.getInstance().uploadImageProduct(
            image,
            progress => {},
            (isSuccess, responseData) => {
            if(isSuccess){
                let params = {
                    name: this.state.productName,
                    category_id: this.state.categoryId,
                    des: this.state.shortDes,
                    price: parseInt(this.state.price),
                    content:this.state.content,
                    store_id:this.props.idStore,
                    original_price:parseInt(this.state.finalPrice),
                    quantity: this.state.quantity,
                    code:this.state.sku,
                    image_url: JSON.parse(responseData).url,
                    property:AddProductUtils.filterPropertyProduct(this.state.categories[this.state.categoryId].property),
                    features:AddProductUtils.filterPropertyProduct(this.state.categories[this.state.categoryId].features),
                    size:{
                        "height": this.state.height,
                        "length": this.state.length,
                        "weight":this.state.weight,
                        "width": this.state.width,
                    },
                    product_details:this.state.groupOptions,
                };
                let productId = this.state.productId;
                if (productId == null) {
                    this.onPostUpload(params);
                } else {
                    this.onUpdateProduct(productId, params);
                }
                return
            }else {

            }

            })
    }
}
