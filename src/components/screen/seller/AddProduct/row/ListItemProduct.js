import React from "react";
import {Text, TextInput, View} from "react-native";
import TextInputTitle from "../../../../elements/TextInput/TextInputTitle";
import {strings} from "../../../../../resource/languages/i18n";
import styles from "../styles";
import TextInputNumber from "../../../../elements/TextInput/TextInputNumber";
import SellerStyles from "../../SellerStyles";
import {Icon} from "react-native-elements";
import Styles from "../../../../../resource/Styles";
import ColorStyle from "../../../../../resource/ColorStyle";
import TextInputUnit from "../../../../elements/TextInput/TextInputUnit";
export default function ListItemProduct({code,onChangeCode,price,onChangePrice,
                                            finalPrice,onChangeFinalPrice,
                                            quantity,onChangeQuantity,
                                            width,onChangeWidth,
                                            length,onChangeLength,
                                            height,onChangeHeight,
                                            weight,onChangeWeight
                                        }){

    function renderPrice(){
        return(
            <View style={{...styles.itemRow,}}>
                <TextInputUnit value={finalPrice}
                               unit={'đ'}
                                placeholder={strings('set')}
                                title={strings('price')}
                                onChange={(text)=>onChangeFinalPrice(text.replace(',',''))}
                />
                <TextInputUnit value={price}
                               v={true}
                                unit={'đ'}
                                placeholder={strings('set')}
                                title={strings('salePrice')}
                                onChange={(text)=>onChangePrice(text.replace(',',''))}
                />
            </View>
        )
    }
    function renderInfoShipping(){
        return(
            <View style={{marginTop:Styles.constants.X/5}}>
                <View
                    style={{
                        ...SellerStyles.itemRow,
                        flex: 1,
                        justifyContent: 'flex-start',
                    }}>
                    <Icon name={'shipping-fast'} type={'font-awesome-5'}  size={20}/>
                    <Text
                        style={{
                            ...Styles.text.text16,
                            color: ColorStyle.tabBlack,
                            marginLeft: 10,
                        }}>
                        {strings('buyShipping')}*
                    </Text>
                </View>
                <View style={{...styles.itemRow,}}>
                    <TextInputUnit value={width}
                                     placeholder={strings('width')}
                                     title={strings('width')}
                                   unit={strings('laWidth')}
                                     onChange={(text)=>onChangeWidth(text.replace(',',''))}
                    />
                    <TextInputUnit value={length}
                                     placeholder={strings('set')}
                                     title={strings('length')}
                                   unit={strings('laWidth')}
                                     onChange={(text)=>onChangeLength(text.replace(',',''))}
                    />
                </View>
                <View style={{...styles.itemRow,}}>
                    <TextInputUnit value={height}
                                     placeholder={strings('height')}
                                     title={strings('height')}
                                   unit={strings('laWidth')}
                                     onChange={(text)=>onChangeHeight(text.replace(',',''))}
                    />
                    <TextInputUnit value={weight}
                                     placeholder={strings('weight')}
                                     title={strings('weight')}
                                    unit={strings('laWeight')}
                                     onChange={(text)=>onChangeWeight(text.replace(',',''))}
                    />
                </View>
            </View>
        )
    }
    return(
        <View style={styles.renderView}>
            <TextInputTitle title={strings('code')}
                            onChange={(text)=>onChangeCode(text)}
                            placeholder={strings('setCode')}
                            value={code}/>
            {renderPrice()}
            <TextInputNumber title={strings('quantity')}
                            onChange={(text)=>onChangeQuantity(text)}
                            placeholder={strings('quantity')}
                            value={quantity}/>
            {renderInfoShipping()}
        </View>
    )
}
