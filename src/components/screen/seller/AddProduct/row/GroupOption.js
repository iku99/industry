import React, {useEffect, useState} from "react";
import DataUtils from "../../../../../Utils/DataUtils";
import {Text, TextInput, TouchableOpacity, View} from "react-native";
import SellerStyles from "../../SellerStyles";
import {strings} from "../../../../../resource/languages/i18n";
import {Icon} from "react-native-elements";
import ColorStyle from "../../../../../resource/ColorStyle";
import Styles from "../../../../../resource/Styles";
import ElevatedView from "react-native-elevated-view";
import Ripple from "react-native-material-ripple";
let validGroupName = "validGroupName";
let validGroupValue = "validGroupValue";
let validOptionQty = "validOptionQty";
export default class GroupOption extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            groupOptions: [], //Max la 2
            options: [],
        }
    }

componentDidMount() {
        this.setState({groupOptions:this.props.item})
}
    addGroupOption(){
        let groupOptions =  this.state.groupOptions;
        let groupOption = {
            size:"",
            color:[""]
        };
        groupOptions.push(groupOption);
        this.compileOptions(groupOptions)
    }
    compileOptions(groupOptions){
        let options = [];
        if(groupOptions.length === 1){
            groupOptions[0].color.forEach((item, i)=>{
                let option = {
                    name:item,
                    sku:"",
                    qty:0,
                    price:0,
                    original_price:0,
                    groups_index : [i]
                };
                options.push(option)
            })
        }else if(groupOptions.length === 2) {
            let valueOptions1 = groupOptions[0].color;
            let valueOption2 = groupOptions[1].color;
            valueOptions1.forEach((item, i1) =>{
                valueOption2.forEach((item2, i2)=>{
                    let option = {
                        name:item +" - " + item2,
                        sku:"",
                        qty:0,
                        price:0,
                        original_price:0,
                        groups_index:[i1, i2]
                    };
                    options.push(option)
                })
            })
        }
        this.setState({groupOptions,options},()=>{this.props.callback(groupOptions)})

    }
    render(){
         let groupOptions =  this.state.groupOptions;
        let groupOptionView = groupOptions.map((item, index)=>{
            let values = item.color;
            if(DataUtils.listNullOrEmpty(values)) values = [];
            let valuesView = values.map((value,i) => {
                return(
                    <View style={SellerStyles.itemOptions}>
                        <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center',}}>
                            <Text style={SellerStyles.title}>{`${strings('groupOptionValue')}${i+ 1}`}</Text>
                            {values.length > 1 &&
                                <TouchableOpacity   onPress={()=>{
                                    values = DataUtils.removeItem(values, i);
                                    item.color = values;
                                    groupOptions[index] = item;
                                    this.compileOptions(groupOptions)
                                }}>
                                    <Icon name='close' type='evil-icons' size={20} color={ColorStyle.tabActive}/>
                                </TouchableOpacity>
                            }
                        </View>
                        <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center',height:Styles.constants.X}}>
                            <View style={{flex:1}}>
                                <TextInput value={value.name}
                                           onChangeText={(text => {
                                               this.state[`${validGroupValue}${index}${i}`] = !DataUtils.stringNullOrEmpty(text);
                                               values[i] = {
                                                   ...values[i],
                                                   name:text
                                               };
                                               item.color = values;
                                               groupOptions[index] = item;
                                               this.compileOptions(groupOptions)
                                           })}
                                           style={{...SellerStyles.codeInput, flex:1}}
                                           placeholder={strings('groupOptionValueHint')} />
                                <Icon name={this.state[`${validGroupValue}${index}${i}`] ? 'check' :'alert-circle'} type='feather' size={15}
                                      color={this.state[`${validGroupName}${index}${i}`] ? 'green' :'red'}
                                      containerStyle={{position:'absolute', right:20, bottom:12}}/>
                            </View>

                        </View>
                        <View style={{flexDirection:'row', justifyContent:'center', marginTop:10,alignItems:'center',height:Styles.constants.X}}>
                            <View style={{flex:1}}>
                                <TextInput
                                    value={value.quantity}
                                    keyboardType={'number-pad'}
                                    onChangeText={(text => {
                                        this.state[`${validOptionQty}${index}${i}`] = !DataUtils.stringNullOrEmpty(text);
                                        values[i] = {
                                            ...values[i],
                                            quantity:text
                                        };
                                        item.color = values
                                        groupOptions[index] = item;
                                        this.compileOptions(groupOptions)
                                    })}
                                    style={{...SellerStyles.codeInput, flex:1}}
                                    placeholder={strings('quantity')}
                                />
                                <Icon name={this.state[`${validOptionQty}${index}${i}`] ? 'check' :'alert-circle'} type='feather' size={15}
                                      color={this.state[`${validOptionQty}${index}${i}`] ? 'green' :'red'}
                                      containerStyle={{position:'absolute', right:20, bottom:12}}/>
                            </View>
                        </View>
                    </View>
                )
            });
            return(<ElevatedView elevation={2} style={SellerStyles.bodyOption} >
                <Text style={SellerStyles.title}>{strings('optionName')}</Text>
                <View>
                    <TextInput value={item.size}
                               onChangeText={(text => {
                                   this.state[`${validGroupName}${index}`] = !DataUtils.stringNullOrEmpty(text);
                                   item.size = text;
                                   groupOptions[index] = item;
                                   this.compileOptions(groupOptions)
                               })}
                               style={SellerStyles.codeInput}
                               placeholder={strings('groupOptionNameHint')}/>
                    <Icon name={this.state[`${validGroupName}${index}`] ? 'check' :'alert-circle'} type='feather' size={15}
                          color={this.state[`${validGroupName}${index}`] ? 'green' :'red'}
                          containerStyle={{position:'absolute', right:20, bottom:12}}/>
                </View>
                <TouchableOpacity style={SellerStyles.btnClose} onPress={()=>{
                    let groupOptions = DataUtils.removeItem(this.state.groupOptions, index);
                    this.compileOptions(groupOptions)
                }}>
                    <Icon name='close' type='evil-icons' size={20} color={ColorStyle.tabActive}/>
                </TouchableOpacity>
                {valuesView}
                <View style={SellerStyles.btnOptions}>
                    <Ripple onPress ={()=>{
                        let values = item.color;
                        values.push("");
                        item = {
                            ...item,
                            color:values
                        };
                        groupOptions[index] = item;
                        this.compileOptions(groupOptions)
                    }}>
                        <Text style={{...Styles.text.text14,color:ColorStyle.tabWhite}}>{strings('addGroupOptionValue')}</Text>
                    </Ripple>
                </View>
            </ElevatedView>)
        });
        return(
            <View>
                <Text style={{...SellerStyles.title, fontSize:16, fontWeight:'600',marginTop:10}}>{strings('phanLoaiSanPhamList')}</Text>
                {groupOptionView}
                {this.state.groupOptions.length <= 1 &&
                    <TouchableOpacity style={SellerStyles.viewOptions} onPress={()=>{
                        this.addGroupOption()
                    }}>
                        <Icon name='plus' type='evilicon' size={25}/>
                        <Text>{strings('addGroupOption')}</Text>
                    </TouchableOpacity>
                }
            </View>
        )
    }

}
