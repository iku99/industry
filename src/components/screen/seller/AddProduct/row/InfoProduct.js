import React, {useState} from "react";
import {FlatList, Image, Text, TextInput, TouchableOpacity, View} from "react-native";
import {strings} from "../../../../../resource/languages/i18n";
import Styles from "../../../../../resource/Styles";
import styles from "../styles";
import TextInputTitle from "../../../../elements/TextInput/TextInputTitle";
import TextInputContent from "../../../../elements/TextInput/TextInputContent";
export default function InfoProduct({productName,onChangeName,des,onChangeDes,content,onChangeContent}){
    return(
        <View style={styles.renderView}>
            <TextInputTitle title={strings('titleProduct')}
                            onChange={(text)=>onChangeName(text)}
                            placeholder={strings('placeholderNameProduct')}
                            value={productName}/>
            <TextInputContent title={strings('description')}
                            onChange={(text)=>onChangeDes(text)}
                            placeholder={strings('placeholderDes')}
                            value={des}/>
            <TextInputContent title={strings('content')}
                              onChange={(text)=>onChangeContent(text)}
                              placeholder={strings('contentProduct')}
                              value={content}/>
        </View>
    )
}
