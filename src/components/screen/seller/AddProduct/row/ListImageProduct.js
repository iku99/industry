import React, {useEffect, useState} from "react";
import {FlatList, Image, Text, TouchableOpacity, View} from "react-native";
import SellerStyles from "../../SellerStyles";
import Styles from "../../../../../resource/Styles";
import ColorStyle from "../../../../../resource/ColorStyle";
import {strings} from "../../../../../resource/languages/i18n";
import {Icon} from "react-native-elements";
import ItemImageProduct from "../../../../elements/viewItem/ItemImageProduct";
import DataUtils from "../../../../../Utils/DataUtils";
import PermissionUtils from "../../../../../Utils/PermissionUtils";
import {PERMISSIONS} from "react-native-permissions";
import ImagePicker from "react-native-image-crop-picker";
export default function ListImageProduct({item,image,callback}){

    const [images,setImages]=useState([])

    useEffect(()=>{
        setImages(image)
    },[])
    const renderFooter = () => {
        return (
            <TouchableOpacity
                style={SellerStyles.btnImage}
                onPress={() => selectImage()}>
                <Text
                    style={{
                        ...Styles.text.text12,
                        color: ColorStyle.tabActive,
                        fontWeight: '500',
                    }}>
                    {strings('addPhoto')}
                </Text>
            </TouchableOpacity>
        );
    };
    function selectImage() {
        if (PermissionUtils.checkPermission(PERMISSIONS.IOS.PHOTO_LIBRARY)) {
            ImagePicker.openPicker({
                cropping: true,
                freeStyleCropEnabled: true,
                compressImageMaxWidth: 800,
                compressImageMaxHeight: 800,
                multiple: true,
                maxFiles: 8,
            }).then(image => {
                let imageGalleries = images;
                image.map(elm=>{
                    let item={
                        node: {
                            image: {
                                uri: elm.path,
                            },
                        },
                    }
                    imageGalleries.push(item);
                })
                setImages(imageGalleries)
                callback(imageGalleries)
            })
        }
    }
    const _renderItem = ({item, index}) => {
        return (
            <ItemImageProduct item={item} index={index} callback={()=>deleteImage(index)}/>
        );
    };
    const deleteImage = index => {
        let list = images;
        list= DataUtils.removeItem(list, index)
        setImages(list)
    };
    function renderListImage() {
        if (
            images.length <= 0 ||
            images.length === undefined
        ) {
            return <View style={SellerStyles.renderView}>{renderFooter()}</View>;
        } else {
            return (
                <View style={{
                    ...SellerStyles.renderView,
                    flexDirection:'row',
                    alignItems: 'center',
                    justifyContent: 'flex-start'
                }}>
                    {renderFooter()}
                    <FlatList
                        data={images}
                        horizontal={true}
                        renderItem={_renderItem}
                        showsHorizontalScrollIndicator={false}
                        removeClippedSubviews={true} // Unmount components when outside of window
                        windowSize={10}
                    />
                </View>
            );
        }
    }
    return renderListImage()
}
