import React, {useEffect, useState} from "react";
import {Text, TextInput, TouchableOpacity, View} from "react-native";
import SellerStyles from "../../SellerStyles";
import {Icon} from "react-native-elements";
import Styles from "../../../../../resource/Styles";
import ColorStyle from "../../../../../resource/ColorStyle";
import {strings} from "../../../../../resource/languages/i18n";
import DataUtils from "../../../../../Utils/DataUtils";
import {Dropdown} from "react-native-material-dropdown-v2";
import CategoryHandle from "../../../../../sagas/CategoryHandle";
import ProductHandle from "../../../../../sagas/ProductHandle";
import styles from "../styles";
import AddProductUtils from "../../../../../Utils/AddProductUtils";
import TextInputInfoItem from "../../../../elements/TextInput/TextInputInfoItem";
export default class CategoryProduct extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            categories:{},
            statusFeatures:false
        }
    }
    componentDidMount() {
        this.setState({categories:this.props.item})
    }

     onChangeCategory(parentId, item) {
        let category = this.state.categories[parentId];
        category = {
            ...category,
            selected: item,
        };
        let mCategories = this.state.categories;
        mCategories[parentId] = category;
        this.setState({categories:mCategories})
        if(DataUtils.listNullOrEmpty(mCategories[item.id])){
            this.getCategories(item.id);
        }
        else {
            let categoryId = undefined;
            item = this.state.categories[item.id];
            do{
                categoryId = !DataUtils.listNullOrEmpty(item.list) ? undefined : item.id;
                item = item.selected !== undefined ? this.state.categories[item.selected.id] : undefined
            }while (item !== undefined);
            this.props.callbackId(categoryId)
        }
    }
     getCategories(parentId) {
         this.props.callbackId(parentId)
        let param = {
            parent_id: parentId,
        };
        CategoryHandle.getInstance().getCategoryChildren(
            param,
            (isSuccess, responseData) => {
                let data = [];
                let mCategories = this.state.categories;
                if (isSuccess) {
                    if (responseData.data != null) {
                        data = responseData.data;
                        let category = {
                            list: data,
                            id: parentId,
                        };
                        mCategories[parentId] = category;
                        this.setState({categories:mCategories})
                        this.props.callbackId(parentId)}
                }
                if(data.length ===0){
                    this.getProductFeatures(parentId)
                    this.getProductProperty(parentId)
                }
            },
        );
    }
     getProductFeatures(id) {
        let params = {
            category_id:id,
        };
        ProductHandle.getInstance().getProductFeature(
            params,
            (isSuccess, responseData) => {
                if (isSuccess) {
                    if (responseData.data != null ) {
                        let features = responseData.data;
                        let mCategories = this.state.categories;

                        // let category = mCategories[id].features;
                        //     features=features.map((item,index)=>{
                        //     let checked=category.filter((elm)=> elm.name===item.name)
                        //     if(checked.length>0){
                        //         item.value= checked[0].value;
                        //         return item
                        //     }
                        // })
                        let category ={
                            ...mCategories[id],
                            features:features,
                        };
                        mCategories[id] = category;
                        this.setState({categories:mCategories,statusFeatures:true})
                        this.props.callbackId(id)
                    }
                }
            },
        );
    }
     getProductProperty(id) {
        let params = {
            category_id:id,
        };
        ProductHandle.getInstance().getProductProperty(
            params,
            (isSuccess, responseData) => {
                if (isSuccess) {
                    if (responseData.data != null ) {
                        let property = responseData.data;
                        let mCategories = this.state.categories;
                        // let category = mCategories[id].features;
                        //     features=features.map((item,index)=>{
                        //     let checked=category.filter((elm)=> elm.name===item.name)
                        //     if(checked.length>0){
                        //         item.value= checked[0].value;
                        //         return item
                        //     }
                        // })
                        let category ={
                            ...mCategories[id],
                            property:property,
                        };
                        mCategories[id] = category;
                        this.setState({categories:mCategories})
                        this.props.callbackId(id)
                    }
                }
            },
        );
    }
     renderCategory(id) {
        if (
            this.state.categories[id] !== undefined &&
            !DataUtils.listNullOrEmpty(this.state.categories[id].list)
        ) {
            let childItem = this.state.categories[id].selected;
            return (
                <View>
                    <Dropdown
                        valueExtractor={item => item.name}
                        onChangeText={(v, i, data) => {
                            this.onChangeCategory(id, data[i]);
                        }}
                        value={
                            this.state.categories[id].selected !== undefined
                                ? this.state.categories[id].selected.name
                                : ''
                        }
                        data={this.state.categories[id].list}
                        error={
                            this.state.categories[id].selected !== undefined
                                ? ''
                                : strings('cannotEmpty')
                        }
                        label={
                            id === 0
                                ? strings('productMenuCreate')
                                : strings('childProductCategories')
                        }
                    />
                    {childItem !== undefined && this.renderCategory(this.state.categories[id].selected.id)}
                    {childItem !== undefined  && this.renderProductFeatures(childItem.id)}
                    {childItem !== undefined  && this.renderProductProperty(childItem.id)}
                </View>
            );
        }
    }
    renderProductFeatures(id) {
        let mCategories = this.state.categories;
        if(mCategories === undefined || mCategories[id] === undefined) return null;
        let category = mCategories[id];
        let features = category.features;
        if(DataUtils.listNullOrEmpty(features)) return null;
        let items = features?.map((item, index)=>{
                return(
                    <View style={{marginVertical:10}}>
                        <Text style={{fontSize:12,color:ColorStyle.tabBlack}}>{item?.name}</Text>
                        <TextInput   style={styles.textInput} value={item?.value}
                                     placeholder={strings('value')}
                                     onChangeText={(text => {
                                         item = {
                                             ...item,
                                             value:text,
                                         };
                                         features[index] = item;
                                         category= {
                                             ...category,
                                             features:features
                                         };
                                         mCategories[id] = category;
                                         this.props.callback(mCategories)

                                     })}
                        />
                    </View>
                )
        });
        return (
            <View style={{width:'100%'}}>
                <View style={SellerStyles.itemRow}>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabBlack,
                            fontWeight:'600'
                        }}>
                        {strings('features')}
                    </Text>
                </View>
                {items}
            </View>
        )
    }
    renderProductProperty(id) {
        let mCategories = this.state.categories;
        if(mCategories === undefined || mCategories[id] === undefined) return null;
        let category = mCategories[id];
        let property = category.property;
        if(DataUtils.listNullOrEmpty(property)) return null;
        let items = property?.map((item, index)=>{
            return(
                <View style={{marginVertical:10}}>
                    <Text style={{fontSize:12,color:ColorStyle.tabBlack}}>{item?.name}</Text>
                    <TextInput   style={styles.textInput} value={item?.value}
                                 placeholder={strings('value')}
                                 onChangeText={(text => {
                                     item = {
                                         ...item,
                                         value:text,
                                     };
                                     property[index] = item;
                                     category= {
                                         ...category,
                                         property:property
                                     };
                                     mCategories[id] = category;
                                     this.props.callback(mCategories)

                                 })}
                    />
                </View>
            )
        });
        return (
            <View style={{width:'100%'}}>
                <View style={SellerStyles.itemRow}>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabBlack,
                            fontWeight:'600'
                        }}>
                        {strings('property')}
                    </Text>
                </View>
                {items}
            </View>
        )
    }
    render() {
        return(
            <View
                style={{
                    ...SellerStyles.renderView,
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginBottom: 10,
                        alignItems: 'center',
                    }}>
                    <View style={SellerStyles.itemRow}>
                        <Icon name={'view-grid-outline'} type={'material-community'} />
                        <Text
                            style={{
                                ...Styles.text.text16,
                                color: ColorStyle.tabBlack,
                                marginLeft: 10,
                            }}>
                            {strings('category')}
                        </Text>
                    </View>
                </View>
                {this.renderCategory(0)}
            </View>
        )
    }


}
