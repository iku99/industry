import Styles from "../../../../resource/Styles";
import ColorStyle from "../../../../resource/ColorStyle";

const X = Styles.constants.X;

export default {
    renderView: {
        backgroundColor: ColorStyle.tabWhite,
        padding:X/4,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        marginVertical: X/5,
    },
    text: {
        textTitle: {
            ...Styles.text.text15,
            color: ColorStyle.tabBlack,
            fontWeight: '500',
        },
        textParameter: {
            ...Styles.text.text15,
            color: ColorStyle.gray,
            fontWeight: '500',
        },
    },
    itemRow:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    textInputLength:{
        ...Styles.input.codeInput,
        height:Styles.constants.X*3,
        paddingVertical: 10,
        borderWidth: 0.5,
        marginTop: X/5,
        paddingLeft: 10,
        borderColor: '#BABABA',
    },
    textInputName:{
        ...Styles.input.codeInput,
        flex: 2,
        paddingVertical: 10,
        borderWidth: 0.5,
        marginTop: X/5,
        paddingLeft: 10,
        borderColor: '#BABABA',
    },
    dropDown:{
        ...Styles.input.codeInput,
        paddingVertical: 10,
        paddingLeft: 10,
        marginVertical: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        backgroundColor: ColorStyle.tabWhite,
        elevation: 2,
    },
    textInput:{
        ...Styles.input.codeInput,
        paddingVertical: 10,
        paddingLeft: 10,
        marginVertical: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        backgroundColor: ColorStyle.tabWhite,
        elevation: 2,
    },
    viewItem:{
        marginBottom: X/4,
    },
    btnAdd:{

        backgroundColor: ColorStyle.tabActive,
        borderRadius:5
    },
    btnSave:{

    },
    btnHuy:{

    }
}
