import React, {Component} from "react";
import {FlatList, ScrollView, Text, TextInput, TouchableOpacity, View} from "react-native";
import Styles from "../../../../resource/Styles";
import Toolbar from "../../../elements/toolbar/Toolbar";
import ColorStyle from "../../../../resource/ColorStyle";
import {strings} from "../../../../resource/languages/i18n";
import styles from "./styles";
import DataUtils from "../../../../Utils/DataUtils";
import AppConstants from "../../../../resource/AppConstants";
import {CheckBox, Icon} from "react-native-elements";
import ElevatedView from 'react-native-elevated-view';
import {TextInputMask} from 'react-native-masked-text';
import Modal from 'react-native-modal';
import RNPickerSelect from 'react-native-picker-select';
import DateUtil from "../../../../Utils/DateUtil";
import AppDatePicker from "../../../elements/DatePicker/AppDatePicker";
import SelectMyProduct from "../../../elements/SelectMyProduct";
import {Actions} from "react-native-router-flux";
import FlashSaleProductItem from "../../../elements/viewItem/flashSaleItem/FlashSaleProductItem";
import NavigationUtils from "../../../../Utils/NavigationUtils";
import ViewUtils from "../../../../Utils/ViewUtils";
import VoucherHandle from "../../../../sagas/VoucherHandle";
import ProductHandle from "../../../../sagas/ProductHandle";
let checkNameId = -1;
let checkCodeId = -1;
let checkQuantityId = -1;
let checkVoucherMaxUseId = -1;
let numberOfLines = 7;
let checkStartDateId = -1;
let checkEdnDateId = -1;
let ADD_PRODUCT_TYPE = 1;
let addProduct = {
    itemType: ADD_PRODUCT_TYPE,
};
let voucherId;
let mSelectProductState;
export default class UploadVoucher extends Component{
    constructor(props) {
        super(props);
        let startDate = new Date();
        let endDate = DateUtil.addDay(startDate, 30);
        this.state = {
            name: '',
            validName: false,
            code: '',
            quantity: '100',
            voucherMaxUse: '1',
            validCode: false,
            validQuantity: true,
            voucherMaxUseValid: true,
            des: '',
            startDate: startDate,
            endDate: endDate,
            startDateStr: DateUtil.formatDate('DD/MM/YYYY', startDate),
            endDateStr: DateUtil.formatDate('DD/MM/YYYY', endDate),
            showStartDate: false,
            showEndDate: false,
            validStartDate: true,
            validEndDate: true,
            disCountCondition: AppConstants.VOUCHER.CONDITION_TYPE[0].value,
            discountType: AppConstants.VOUCHER.DISCOUNT_TYPE[0].value,
            discountLevel: [this.renderNewLevel()],
            voucherScope: AppConstants.VOUCHER.VOUCHER_SCOPE[0].value,
            products: [addProduct],
            showSelectProduct: false,
            voucherDisplay: AppConstants.VOUCHER.DISPLAY.public,
            voucher_type:AppConstants.VOUCHER.VOUCHER_TYPE[0].value
        };
    }
    componentDidMount() {
        if (this.props.voucher !== undefined) {
            this.initData(this.props.voucher);
        }
    }
    initData(voucher){
        voucherId = voucher.id;
        let startDate = new Date(voucher.valid_from);
        let endDate = new Date(voucher.valid_to);
        let productIds = voucher?.product_ids;
        let conditions = [];
        let disCountCondition = voucher.condition_discount;
        let discountType = voucher.type_discount;
        voucher.detail_discount.forEach((condition)=>{
                let voucherMinValue = condition.min_value_to_apply ;
                let voucherMinQuantity = condition.min_value_to_apply ;
                let voucherDiscountPercent = discountType !== AppConstants.VOUCHER.CONDITION_TYPE[0].value ? condition.value : 0;
                let voucherDiscountMaxValue = condition.max_decrease_price
                let voucherByValue = condition.value
            conditions.push({
                voucherMinValue:`${voucherMinValue}`,
                voucherMinQuantity:`${voucherMinQuantity}`,
                voucherDiscountPercent:`${voucherDiscountPercent}`,
                voucherDiscountMaxValue:`${voucherDiscountMaxValue}`,
                voucherByValue:`${voucherByValue}`,
                voucherMinValueValid:Number(voucherMinValue) > 0,
                voucherMinQuantityValid:Number(voucherMinQuantity) > 0,
                voucherDiscountPercentValid:Number(voucherDiscountPercent) > 0  && Number(voucherDiscountPercent) <= 100,
                voucherDiscountMaxValueValid:Number(voucherDiscountMaxValue) > 0,
                voucherByValueValid:Number(voucherByValue) > 0
            })
        });
        console.log('disCountCondition:',voucher)
        this.setState({
            name:voucher.name,
            code:voucher.code,
            quantity:`${voucher.total_available}`,
            voucherMaxUse:`${voucher.max_amount_per_user}`,
            des:voucher.des,
            startDate:startDate,
            endDate:endDate,
            startDateStr:DateUtil.formatDate('DD/MM/YYYY', startDate),
            endDateStr: DateUtil.formatDate('DD/MM/YYYY', endDate),
            disCountCondition:discountType,
            discountType:discountType,
            voucher_type:voucher.voucher_type,
            voucherScope:voucher.apply_for === 0 ?AppConstants.VOUCHER.VOUCHER_SCOPE[0].value:AppConstants.VOUCHER.VOUCHER_SCOPE[1].value,
            voucherDisplay:voucher.is_public ? AppConstants.VOUCHER.DISPLAY.public : AppConstants.VOUCHER.DISPLAY.private,
            discountLevel:conditions
        },()=>{
            this.checkName();
            this.checkCode();
            this.checkQuantity();
            this.checkDate()
        });
        this.getProductList(voucher.apply_for)
    }
    render() {
        let initProducts = this.state.products;
        if (
            initProducts.length > 0 &&
            initProducts[initProducts.length - 1].itemType === ADD_PRODUCT_TYPE
        ) {
            initProducts = DataUtils.removeItem(
                initProducts,
                initProducts.length - 1,
            );
        }
        return (
            <View style={Styles.container}>
                <Toolbar
                    backgroundColor={ColorStyle.tabActive}
                    textColor={ColorStyle.tabWhite}
                    title={strings('voucher')}
                />
                <ScrollView keyboardShouldPersistTaps={'handled'}>
                    <Text style={styles.optionTitle}>{strings('voucherInfo')}</Text>
                    {this.renderTextInputWithTitle(
                        `${strings('voucherName')}`,
                        'name',
                        'validName',
                        'checkNameId',
                        () => this.checkName(),
                        '',
                        'next',
                        input => {
                            this.nameRef = input;
                        },
                        () => {
                            this.codeRef.focus();
                        },
                    )}
                    <View style={{flexDirection: 'row'}}>
                        <View style={{flex: 2}}>
                            {this.renderTextInputWithTitle(
                                `${strings('voucherCode')}`,
                                'code',
                                'validCode',
                                'checkCodeId',
                                () => this.checkCode(),
                                'numeric',
                                'next',
                                input => {
                                    this.codeRef = input;
                                },
                                () => {
                                    this.quantityRef.focus();
                                },
                            )}
                            <Text
                                style={{
                                    paddingHorizontal: 10,
                                    paddingTop: 10,
                                    color: ColorStyle.red,
                                }}>
                                {strings('voucherCodeHint')}
                            </Text>
                        </View>
                        <View style={{flex: 1}}>
                            {this.renderTextInputWithTitle(
                                `${strings('voucherQuantity')}`,
                                'quantity',
                                'validQuantity',
                                'validQuantityId',
                                () => this.checkQuantity(),
                                'numeric',
                                'next',
                                input => {
                                    this.quantityRef = input;
                                },
                                () => {},
                            )}
                        </View>
                    </View>
                    <View>
                        <Text style={styles.title}>{strings('voucherDes')}</Text>
                        <TextInput
                            style={styles.des}
                            value={this.state.des}
                            autoCorrect={false}
                            onChangeText={des => {
                                this.setState({des});
                            }}
                            multiline={true}
                            numberOfLines={numberOfLines}
                            minHeight={20 * numberOfLines}
                            textAlignVertical={'top'}
                            underlineColorAndroid="transparent"
                        />
                    </View>
                    <View
                        style={{
                            width: Styles.constants.widthScreen,
                            alignItems: 'center',
                            flexDirection: 'row',
                        }}>
                        <View style={{flex: 1}}>
                            {this.renderTextInputWithTitle(
                                `${strings('voucherStartDate')}`,
                                'startDateStr',
                                'validStartDate',
                                'checkStartDateId',
                                () => this.checkStartDate(),
                                '',
                                'next',
                                input => {
                                    this.startDateRef = input;
                                },
                                () => {},
                            )}

                            <TouchableOpacity
                                style={{
                                    width: '100%',
                                    height: '100%',
                                    position: 'absolute',
                                    top: 0,
                                    left: 0,
                                }}
                                onPress={() => {
                                    this.setState({showStartDate: true});
                                }}
                            />
                        </View>
                        <View style={{flex: 1}}>
                            {this.renderTextInputWithTitle(
                                `${strings('voucherEndDate')}`,
                                'endDateStr',
                                'validEndDate',
                                'checkEdnDateId',
                                () => this.checkStartDate(),
                                '',
                                'next',
                                input => {
                                    this.endDateRef = input;
                                },
                                () => {},
                            )}

                            <TouchableOpacity
                                style={{
                                    width: '100%',
                                    height: '100%',
                                    position: 'absolute',
                                    top: 0,
                                    left: 0,
                                }}
                                onPress={() => {
                                    this.setState({showEndDate: true});
                                }}
                            />
                        </View>
                    </View>
                    {/*<View>*/}
                    {/*    <Text style={styles.title}>{strings('voucherType')}</Text>*/}
                    {/*    <RNPickerSelect*/}
                    {/*        placeholder={{}}*/}
                    {/*        style={styles.pickerSelectStyles}*/}
                    {/*        onValueChange={voucherType => this.setState({voucher_type:voucherType})}*/}
                    {/*        items={AppConstants.VOUCHER.VOUCHER_TYPE}*/}
                    {/*        Icon={() => {*/}
                    {/*            return <Icon name="chevron-down" type="feather" size={18} containerStyle={{display:AppConstants.IS_IOS?'flex':'none'}}/>;*/}
                    {/*        }}*/}
                    {/*        value={this.state.voucher_type}*/}
                    {/*    />*/}
                    {/*</View>*/}

                    <View>
                        <Text style={styles.title}>{strings('voucherDiscountType')}</Text>

                        <RNPickerSelect
                            placeholder={{}}
                            style={styles.pickerSelectStyles}
                            onValueChange={discountType => this.setState({discountType})}
                            items={AppConstants.VOUCHER.DISCOUNT_TYPE}
                            Icon={() => {
                                return <Icon name="chevron-down" type="feather" size={18} containerStyle={{display:AppConstants.IS_IOS?'flex':'none'}}/>;
                            }}
                            value={this.state.discountType}
                        />
                    </View>


                    <View>
                        <Text style={styles.title}>
                            {strings('voucherDiscountCondition')}
                        </Text>
                        <RNPickerSelect
                            placeholder={{}}
                            style={styles.pickerSelectStyles}
                            onValueChange={value => this.setState({disCountCondition: value})}
                            items={AppConstants.VOUCHER.CONDITION_TYPE}
                            Icon={() => {
                                return <Icon name="chevron-down" type="feather" size={18} containerStyle={{display:AppConstants.IS_IOS?'flex':'none'}}/>;
                            }}
                            value={this.state.disCountCondition}
                        />
                    </View>
                    <Text style={styles.optionTitle}>{strings('mucGiamGia')}</Text>
                    {this.renderMucGiamGia()}
                    <TouchableOpacity
                        style={styles.addGroupOption}
                        onPress={() => {
                            let discountLevel = this.state.discountLevel;
                            discountLevel = [...discountLevel, this.renderNewLevel()];
                            this.setState({discountLevel});
                        }}>
                        <Icon
                            name="plus"
                            type="evilicon"
                            size={25}
                            color={ColorStyle.tabInactive}
                        />
                        <Text style={{color: ColorStyle.tabInactive}}>
                            {strings('addLevel')}
                        </Text>
                    </TouchableOpacity>
                    <Text style={styles.optionTitle}>{strings('applyProduct')}</Text>
                    <RNPickerSelect
                        placeholder={{}}
                        style={styles.pickerSelectStyles}
                        onValueChange={value => this.setState({voucherScope: value})}
                        items={AppConstants.VOUCHER.VOUCHER_SCOPE}
                        Icon={() => {
                            return <Icon name="chevron-down" type="feather" size={18} containerStyle={{display:AppConstants.IS_IOS?'flex':'none'}}/>;
                        }}
                        value={this.state.voucherScope}
                    />
                    {this.renderProductScope()}
                    <View
                        style={{
                            width: '100%',
                            justifyContent: 'space-between',
                            flexDirection: 'row',
                            alignItems: 'center',
                            paddingTop: 10,
                        }}>
                        <Text style={styles.optionTitle}>{strings('otherInfo')}</Text>
                    </View>
                    {this.renderTextInputWithTitle(
                        `${strings('voucherMaxUse')}`,
                        'voucherMaxUse',
                        'voucherMaxUseValid',
                        'voucherMaxUseId',
                        () => this.checkVoucherMaxUse(),
                        'numeric',
                        'next',
                        input => {
                            this.quantityRef = input;
                        },
                        () => {},
                    )}
                    <Text style={styles.title}>{strings('voucherDisplay')}</Text>
                    <View
                        style={{
                            width: AppConstants.width,
                            alignItems: 'center',
                            flexDirection: 'row',
                        }}>
                        <CheckBox
                            checked={
                                this.state.voucherDisplay ===
                                AppConstants.VOUCHER.DISPLAY.public
                            }
                            containerStyle={{borderColor: 'transparent', padding: 0, flex: 1}}
                            onPress={() => {
                                this.setState({
                                    voucherDisplay: AppConstants.VOUCHER.DISPLAY.public,
                                });
                            }}
                            iconType="ionicon"
                            uncheckedIcon="ios-radio-button-off"
                            checkedIcon="ios-radio-button-on"
                            textStyle={{fontWeight: 'normal'}}
                            title={strings('voucherPublic')}
                        />
                        <CheckBox
                            checked={
                                this.state.voucherDisplay ===
                                AppConstants.VOUCHER.DISPLAY.private
                            }
                            containerStyle={{borderColor: 'transparent', padding: 0, flex: 1}}
                            onPress={() => {
                                this.setState({
                                    voucherDisplay: AppConstants.VOUCHER.DISPLAY.private,
                                });
                            }}
                            iconType="ionicon"
                            uncheckedIcon="ios-radio-button-off"
                            checkedIcon="ios-radio-button-on"
                            textStyle={{fontWeight: 'normal'}}
                            title={strings('voucherPrivate')}
                        />
                    </View>
                    {this.renderActionButton()}
                </ScrollView>
                {this.state.showStartDate && (
                    <AppDatePicker
                        onFinish={(isSelect, date) => {
                            let preDate = this.state.startDate;
                            if (isSelect) {
                                preDate = date;
                            }
                            this.setState(
                                {
                                    startDate: preDate,
                                    showStartDate: false,
                                    startDateStr: DateUtil.formatDate('DD/MM/YYYY', preDate),
                                },
                                () => this.checkDate(),
                            );
                        }}
                        minimumDate={new Date()}
                        maximumDate={DateUtil.addYear(new Date(), 100)}
                        date={this.state.startDate}
                    />
                )}
                {this.state.showEndDate && (
                    <AppDatePicker
                        onFinish={(isSelect, date) => {
                            let preDate = this.state.endDate;
                            if (isSelect) {
                                preDate = date;
                            }
                            this.setState(
                                {
                                    endDate: preDate,
                                    showEndDate: false,
                                    endDateStr: DateUtil.formatDate('DD/MM/YYYY', preDate),
                                },
                                () => this.checkDate(),
                            );
                        }}
                        minimumDate={new Date()}
                        maximumDate={DateUtil.addYear(new Date(), 130)}
                        date={this.state.endDate}
                    />
                )}
                <Modal isVisible={this.state.showSelectProduct}>
                    <SelectMyProduct
                        onClose={() => this.setState({showSelectProduct: false})}
                        selectProductState={mSelectProductState}
                        initProducts={initProducts}
                        idStore={this.props.storeId}
                        onTag={(products, selectProductState) => {
                            mSelectProductState = selectProductState;
                            products.push(addProduct);
                            this.setState({showSelectProduct: false, products: products});
                        }}
                        onSelectProduct={() => {
                            Actions.pop();
                            if (this.props.onSelectProduct !== undefined) {
                                this.props.onSelectProduct();
                            }
                        }}
                    />
                </Modal>
            </View>
        );
    }
    getProductList(productIds){
        if(productIds.length > 0){
            ProductHandle.getInstance().getProductByIds(productIds,(isSuccess,dataResponse)=>{
                let products = [];

                if(isSuccess){
                    products = dataResponse["data"] !== undefined ? dataResponse["data"] : [];
                    console.log(dataResponse["data"])
                    products.push(addProduct)
                }
                this.setState({products})
            })
        }
    }
    renderProductScope() {
        if (
            this.state.voucherScope === AppConstants.VOUCHER.VOUCHER_SCOPE[0].value
        ) {
            return null;
        } else {
            return (
                <FlatList
                    showsHorizontalScrollIndicator={false}
                    horizontal={true}
                    data={this.state.products}
                    keyExtractor={this.keyExtractor}
                    renderItem={this._renderItem}
                    style={{margin: 10}}
                />
            );
        }
    }
    _renderItem = ({item, index}) => this.renderItem(item, index);
    renderItem(item, index) {
        if (item.itemType === ADD_PRODUCT_TYPE) {
            return (
                <View
                    style={{
                        height: '100%',
                        minHeight: AppConstants.defaultViewedProductItemWidth + 26,
                        paddingVertical: 13,
                        paddingHorizontal: 2,
                    }}>
                    <TouchableOpacity
                        style={styles.addProduct}
                        onPress={() => {
                            this.setState({showSelectProduct: true});
                        }}>
                        <Icon
                            name="ios-add"
                            type="ionicon"
                            size={35}
                            color={ColorStyle.tabInactive}
                        />
                    </TouchableOpacity>
                </View>
            );
        } else {
            return (
                <FlashSaleProductItem
                    item={item}
                    index={index}
                    onClick={item => {
                        NavigationUtils.goToProductDetail(item);
                    }}
                />
            );
        }
    }
    renderActionButton() {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    marginBottom: 20,
                }}>
                <View style={{flex: 1, marginHorizontal: 10}}>
                    <TouchableOpacity style={styles.previousBT} animation="fadeInUpBig" onPress={()=>Actions.pop()}>
                        <Text style={{color: ColorStyle.red, fontSize: 14}}>
                            {strings('voucherCancel')}
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={{flex: 1, marginHorizontal: 10}}>
                    <TouchableOpacity
                        onPress={() => {
                            this.onPost();
                        }}
                        style={styles.nextBT}
                        animation="fadeInUpBig">
                        <Text style={{color: 'white', fontSize: 14}}>
                            {this.props.voucher ===undefined?strings('voucherCreate'):strings('voucherUpdate')}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
    renderMucGiamGia() {
        let discountLevel = this.state.discountLevel;
        console.log('dis:',this.state.disCountCondition,this.state.discountType)
        return this.state.discountLevel.map((level, index) => (
            <ElevatedView elevation={2}>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingHorizontal: 10,
                    }}>
                    <Text
                        style={{
                            fontSize: 15,
                            fontWeight: '500',
                            paddingTop: 10,
                            color: 'blue',
                        }}>
                        {`${strings('levelValue')}${index + 1}`}
                    </Text>
                    {discountLevel.length > 1 && (
                        <TouchableOpacity
                            style={{padding: 10}}
                            onPress={() => {
                                discountLevel = DataUtils.removeItem(discountLevel, index);
                                this.setState({discountLevel});
                            }}>
                            <Text style={{fontSize: 16, color: ColorStyle.red}}>
                                {strings('delete')}
                            </Text>
                        </TouchableOpacity>
                    )}
                </View>
                {/*Condition type*/}
                {this.state.disCountCondition ===
                    AppConstants.VOUCHER.CONDITION_TYPE[0].value && (
                        <View style={{width: '100%'}}>
                            <Text style={styles.title}>{strings('voucherMinValue')}</Text>
                            <TextInputMask
                                type={'money'}
                                value={level.voucherMinValue}
                                onChangeText={price => {
                                    let finalValue = DataUtils.trimNumber(price);
                                    this.checkVoucherMinValue(
                                        DataUtils.remove0Number(finalValue),
                                        index,
                                    );
                                }}
                                style={{...styles.codeInput, paddingRight: 30}}
                                options={{
                                    precision: 0,
                                    separator: '.',
                                    delimiter: ',',
                                    unit: '',
                                    suffixUnit: '',
                                }}
                            />
                            <Icon
                                name={level.voucherMinValueValid ? 'check' : 'alert-circle'}
                                type="feather"
                                size={15}
                                color={level.voucherMinValueValid ? 'green' : 'red'}
                                containerStyle={{position: 'absolute', right: 20, bottom: 12}}
                            />
                        </View>
                    )}
                {this.state.disCountCondition ===
                    AppConstants.VOUCHER.CONDITION_TYPE[1].value && (
                        <View style={{width: '100%'}}>
                            <Text style={styles.title}>{strings('voucherMinQuantity')}</Text>
                            <TextInput
                                style={{...styles.codeInput, paddingRight: 30}}
                                value={level.voucherMinQuantity}
                                onChangeText={text => {
                                    this.checkVoucherMinQuantity(
                                        DataUtils.remove0Number(text),
                                        index,
                                    );
                                }}
                                keyboardType="numeric"
                            />
                            <Icon
                                name={level.voucherMinQuantityValid ? 'check' : 'alert-circle'}
                                type="feather"
                                size={15}
                                color={level.voucherMinQuantityValid ? 'green' : 'red'}
                                containerStyle={{position: 'absolute', right: 20, bottom: 12}}
                            />
                        </View>
                    )}

                {/*Discount type*/}
                {this.state.discountType ===
                    AppConstants.VOUCHER.DISCOUNT_TYPE[0].value && (
                        <View style={{flex: 1}}>
                            <Text style={styles.title}>{strings('voucherByValue')}</Text>
                            <TextInputMask
                                type={'money'}
                                value={level.voucherByValue}
                                onChangeText={price => {
                                    let finalValue = DataUtils.trimNumber(price);
                                    this.checkVoucherByValue(finalValue, index);
                                }}
                                style={{...styles.codeInput, paddingRight: 30}}
                                options={{
                                    precision: 0,
                                    separator: '.',
                                    delimiter: ',',
                                    unit: '',
                                    suffixUnit: '',
                                }}
                            />
                            <Icon
                                name={level.voucherByValueValid ? 'check' : 'alert-circle'}
                                type="feather"
                                size={15}
                                color={level.voucherByValueValid ? 'green' : 'red'}
                                containerStyle={{position: 'absolute', right: 20, bottom: 12}}
                            />
                        </View>
                    )}

                {this.state.discountType ===
                    AppConstants.VOUCHER.DISCOUNT_TYPE[1].value && (
                        <View style={{flexDirection: 'row'}}>
                            <View style={{flex: 1}}>
                                <Text style={styles.title}>
                                    {strings('voucherDiscountPercent')}
                                </Text>
                                <TextInput
                                    style={{...styles.codeInput, paddingRight: 30}}
                                    value={level.voucherDiscountPercent}
                                    onChangeText={text => {
                                        this.checkVoucherDiscountPercent(
                                            DataUtils.remove0Number(text),
                                            index,
                                        );
                                    }}
                                    keyboardType="numeric"
                                />
                                <Icon
                                    name={
                                        level.voucherDiscountPercentValid ? 'check' : 'alert-circle'
                                    }
                                    type="feather"
                                    size={15}
                                    color={level.voucherDiscountPercentValid ? 'green' : 'red'}
                                    containerStyle={{position: 'absolute', right: 20, bottom: 12}}
                                />
                            </View>
                            <View style={{flex: 1}}>
                                <Text style={styles.title}>
                                    {strings('voucherDiscountMaxValue')}
                                </Text>
                                <TextInputMask
                                    type={'money'}
                                    value={level.voucherDiscountMaxValue}
                                    onChangeText={price => {
                                        let finalValue = DataUtils.trimNumber(price);
                                        this.checkVoucherDiscountMaxValue(finalValue, index);
                                    }}
                                    style={{...styles.codeInput, paddingRight: 30}}
                                    options={{
                                        precision: 0,
                                        separator: '.',
                                        delimiter: ',',
                                        unit: '',
                                        suffixUnit: '',
                                    }}
                                />
                                <Icon
                                    name={
                                        level.voucherDiscountMaxValueValid ? 'check' : 'alert-circle'
                                    }
                                    type="feather"
                                    size={15}
                                    color={level.voucherDiscountMaxValueValid ? 'green' : 'red'}
                                    containerStyle={{position: 'absolute', right: 20, bottom: 12}}
                                />
                            </View>
                        </View>
                    )}
            </ElevatedView>
        ));
    }
    renderProductScope() {
        if (
            this.state.voucherScope === AppConstants.VOUCHER.VOUCHER_SCOPE[0].value
        ) {
            return null;
        } else {
            return (
                <FlatList
                    showsHorizontalScrollIndicator={false}
                    horizontal={true}
                    data={this.state.products}
                    keyExtractor={this.keyExtractor}
                    renderItem={this._renderItem}
                    style={{margin: 10}}
                />
            );
        }
    }
    keyExtractor = (item, index) =>`voucherModify`+ index.toString();
    renderNewLevel() {
        return {
            voucherMinValue: '0',
            voucherMinQuantity: '0',
            voucherDiscountPercent: '0',
            voucherDiscountMaxValue: '0',
            voucherByValue: '0',
            voucherMinValueValid: false,
            voucherMinQuantityValid: false,
            voucherDiscountPercentValid: false,
            voucherDiscountMaxValueValid: false,
            voucherByValueValid: false,
        };
    }
    checkStartDate() {}
    checkName() {
        clearTimeout(checkNameId);
        checkNameId = setTimeout(() => {
            this.setState({validName: this.state.name.length > 5});
        }, 300);
    }
    checkCode() {
        clearTimeout(checkCodeId);
        checkCodeId = setTimeout(() => {
            this.setState({validCode: this.state.code.length >= 5});
        }, 300);
    }
    checkQuantity() {
        clearTimeout(checkQuantityId);
        checkQuantityId = setTimeout(() => {
            let quantity = this.state.quantity;
            this.setState({validQuantity: !isNaN(quantity) && Number(quantity) > 0});
        }, 300);
    }
    checkVoucherMaxUse() {
        clearTimeout(checkVoucherMaxUseId);
        checkVoucherMaxUseId = setTimeout(() => {
            let quantity = this.state.voucherMaxUse;
            this.setState({
                voucherMaxUseValid: !isNaN(quantity) && Number(quantity) > 0,
            });
        }, 300);
    }
    checkDate() {
        let valid =
            DateUtil.compareDate(this.state.startDate, this.state.endDate) < 0;
        this.setState({validEndDate: valid});
    }
    checkVoucherMinValue(voucherMinValue, index) {
        let discountLevel = this.state.discountLevel;
        let level = discountLevel[index];
        level = {
            ...level,
            voucherMinValueValid:
                !isNaN(voucherMinValue) && Number(voucherMinValue) > 0,
            voucherMinValue,
        };
        discountLevel[index] = level;
        this.setState({discountLevel});
    }
    checkVoucherMinQuantity(voucherMinQuantity, index) {
        let discountLevel = this.state.discountLevel;
        let level = discountLevel[index];
        level = {
            ...level,
            voucherMinQuantityValid:
                !isNaN(voucherMinQuantity) && Number(voucherMinQuantity) > 0,
            voucherMinQuantity,
        };
        discountLevel[index] = level;
        this.setState({discountLevel});
    }
    checkVoucherByValue(voucherByValue, index) {
        let discountLevel = this.state.discountLevel;
        let level = discountLevel[index];
        level = {
            ...level,
            voucherByValueValid: !isNaN(voucherByValue) && Number(voucherByValue) > 0,
            voucherByValue,
        };
        discountLevel[index] = level;
        this.setState({discountLevel});
    }
    checkVoucherDiscountPercent(voucherDiscountPercent, index) {
        let discountLevel = this.state.discountLevel;
        let level = discountLevel[index];
        level = {
            ...level,
            voucherDiscountPercentValid:
                !isNaN(voucherDiscountPercent) &&
                Number(voucherDiscountPercent) > 0 &&
                Number(voucherDiscountPercent) <= 100,
            voucherDiscountPercent,
        };
        discountLevel[index] = level;
        this.setState({discountLevel});
    }
    checkVoucherDiscountMaxValue(voucherDiscountMaxValue, index) {
        let discountLevel = this.state.discountLevel;
        let level = discountLevel[index];
        level = {
            ...level,
            voucherDiscountMaxValueValid:
                !isNaN(voucherDiscountMaxValue) && Number(voucherDiscountMaxValue) > 0,
            voucherDiscountMaxValue,
        };
        discountLevel[index] = level;
        this.setState({discountLevel});
    }
    renderTextInputWithTitle(
        title,
        stateName,
        validStateName,
        checkId,
        checkFunc,
        keyboardType,
        returnKeyType,
        ref,
        onSubmitEditing,
    ) {
        let mValidStateName = this.state[validStateName];
        return (
            <View style={{width: '100%',}}>
                <Text style={styles.title}>{title}</Text>
                <TextInput
                    style={{...styles.codeInput, paddingRight: 30}}
                    value={this.state[stateName]}
                    onChangeText={text => {
                        if (keyboardType === 'numeric') {
                            if (text.length > 1 && text.startsWith('0')) {
                                text = text.substr(1, text.length);
                            }
                            if (text.length === 0) {
                                text = '0';
                            }
                        }
                        this.setState({[stateName]: text}, () => {
                            if (!DataUtils.stringNullOrEmpty(checkId) && checkFunc != null) {
                                clearTimeout(this[checkId]);
                                this[checkId] = setTimeout(() => {
                                    checkFunc();
                                }, 500);
                            }
                        });
                    }}
                    ref={ref}
                    onSubmitEditing={onSubmitEditing}
                    returnKeyType={returnKeyType}
                    keyboardType={
                        !DataUtils.stringNullOrEmpty(keyboardType)
                            ? keyboardType
                            : 'ascii-capable'
                    }
                />

                {!DataUtils.stringNullOrEmpty(validStateName) && (
                    <Icon
                        name={mValidStateName ? 'check' : 'alert-circle'}
                        type="feather"
                        size={15}
                        color={mValidStateName ? 'green' : 'red'}
                        containerStyle={{position: 'absolute', right: 20, bottom: 12}}
                    />
                )}
            </View>
        );
    }
    showInvalidMsg() {
        ViewUtils.showAlertDialog(strings('dataInvalid'), null);
    }
    onPost() {
        let validDes = this.state.des.length > 0;
        if (
            !this.state.validName ||
            !this.state.validCode ||
            !this.state.validQuantity ||
            !validDes ||
            !this.state.validStartDate ||
            !this.state.validEndDate
        ) {
            this.showInvalidMsg();
            return;
        }
        let params = {
            name: this.state.name,
            code: this.state.code,
            des: this.state.des,
            scope: this.state.voucherScope,
            type_discount: this.state.discountType,
            voucher_type:this.state.voucher_type,
            condition_discount: this.state.disCountCondition,
            // additional_conditions: [],
            is_public: this.state.voucherDisplay ,
            total_available: Number(this.state.quantity),
            valid_from: this.state.startDate,
            valid_to: this.state.endDate,
            user_id: [14]
        };
        let discountLevel = this.state.discountLevel;
        let detail_discount = [];
        let conditionValid = true;
        discountLevel.forEach((level, index) => {
            let condition = {};
            if (
                this.state.discountType === AppConstants.VOUCHER.DISCOUNT_TYPE[0].value
            ) {
                //Giam theo so tien
                if (!level.voucherByValueValid) {
                    conditionValid = false;
                    return;
                }

                condition = {
                    ...condition,
                    value: Number(level.voucherByValue),
                };
            } else {
                //Giam theo %
                if (
                    !level.voucherDiscountPercentValid ||
                    !level.voucherDiscountMaxValueValid
                ) {
                    conditionValid = false;
                    return;
                }
                condition = {
                    ...condition,
                    value: Number(level.voucherDiscountPercent),
                    max_decrease_price: Number(level.voucherDiscountMaxValue),
                };
            }
            if (
                this.state.disCountCondition ===
                AppConstants.VOUCHER.CONDITION_TYPE[0].value
            ) {
                //Gia tri don hang toi thieu
                if (!level.voucherMinValueValid) {
                    conditionValid = false;
                    return;
                }
                condition = {
                    ...condition,
                    min_value_to_apply: Number(level.voucherMinValue),
                };
            } else {
                //So luong toi thieu
                if (!level.voucherMinQuantityValid) {
                    conditionValid = false;
                    return;
                }
                condition = {
                    ...condition,
                    min_value_to_apply: Number(level.voucherMinQuantity),
                };
            }
            detail_discount.push(condition);
        });
        if (!conditionValid) {
            this.showInvalidMsg();
            return;
        }
        params = {
            ...params,
            detail_discount,
        };
        if (
            this.state.voucherScope === AppConstants.VOUCHER.VOUCHER_SCOPE[1].value
        ) {
            if (this.state.products.length <= 1) {
                // showAlertDialog(strings('voucherEmptyProduct'), null);
                return;
            }
            let apply_for = [];
            this.state.products.forEach((product, index) => {
                if (product.itemType !== ADD_PRODUCT_TYPE) {
                    apply_for.push(product.id);
                }
            });
            params = {
                ...params,
                apply_for,
            };
        }
        if (!this.state.voucherMaxUseValid) {
            this.showInvalidMsg();
            return;
        }
        params = {
            ...params,
            max_amount_per_user: this.state.voucherMaxUse,
        };
        if (this.props.voucher  !== undefined) {
            VoucherHandle.getInstance().updateVoucher(params,this.props.voucher.id,(isSuccess,responseData)=>{
                console.log('responseData:',responseData)
                if(isSuccess && responseData.code===0){
                    this.props.callback()
                    Actions.pop()
                    return
                }
                ViewUtils.showAlertDialog(strings('completeOrderFailed'), null);
            })
        } else {
            VoucherHandle.getInstance().createVoucher(params,(isSuccess,responseData)=>{
                console.log(responseData,params)
                if(isSuccess && responseData.code===0){
                    this.props.callback()
                    Actions.pop()
                    return
                }
                ViewUtils.showAlertDialog(strings('cannotCreateVoucher'), null);
            })
        }
    }
}
