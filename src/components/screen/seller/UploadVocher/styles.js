import Styles from "../../../../resource/Styles";
import Colors from "../../../../resource/ColorStyle";
import {Platform} from "react-native";
import AppConstants from "../../../../resource/AppConstants";

const X = Styles.constants.X;
export default {
    container: {
        ...Styles.container,
    },
    previousBT:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:Colors.tabWhite,
        borderRadius:20,
        borderWidth:0,
        padding: 12,
        marginTop:30,
        elevation:5,
    },
    optionTitle: {
        flexDirection: 'row',
        paddingVertical: 8,
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
        fontSize: 17,
        fontWeight: '600',
    },
    codeInput: {
        height:Styles.constants.X,
        borderRadius: 3,
        borderWidth: 1,
        paddingLeft: 5,
        marginHorizontal: 10,
        color:Colors.tabBlack,
    },
    addGroupOption: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderStyle: 'dashed',
        borderColor: Colors.borderButton,
        borderRadius: 5,
        padding: 10,
        margin: 10,
    },
    nextBT:{

        justifyContent:'center',
        alignItems:'center',
        backgroundColor:Colors.tabActive,
        borderRadius:20,
        borderWidth:0,
        padding: 12,
        marginTop:30,
        elevation:5
    },
    title: {
        padding: 10,
        marginTop: 10,
    },
    des: {
        flex: 1,
        backgroundColor: '#e9ebee',
        borderRadius: 10,
        overflow: 'hidden',
        paddingHorizontal: 15,
        paddingTop: Platform.OS === 'android' ? 8 : 12,
        paddingBottom: Platform.OS === 'android' ? 8 : 12,
        maxHeight: 150,
        marginHorizontal: 10,
        textAlignVertical: 'center',
        justifyContent: 'center',
    },
    pickerSelectStyles: {
        inputIOS: {
            borderRadius: 3,
            borderWidth: 1,
            borderColor: Colors.gray,
            paddingVertical: AppConstants.IS_IOS ? 8 : 5,
            paddingLeft: 5,
            marginHorizontal: 10,
            paddingRight: 30,
        },
        inputAndroid: {
            width:Styles.constants.widthScreenMg24,
            height:X,
            borderRadius: 3,
            borderWidth: 1,
            borderColor: Colors.gray,
            paddingVertical: AppConstants.IS_IOS ? 8 : 5,
            paddingLeft: 5,
            marginHorizontal: 10,
            paddingRight: 30,

        },
        iconContainer: {
            top: 10,
            right: 15,
        },
    },
    addProduct: {
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderStyle: 'dashed',
        borderRadius: 5,
        borderColor: Colors.gray,
        // width: AppConstants.defaultProductItemWidth,
        height: '100%',
    },
};
