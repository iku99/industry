import React from 'react';
import {
  ActivityIndicator,
  FlatList,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Styles from '../../../resource/Styles';
import {connect} from 'react-redux';
import {Icon, SearchBar} from 'react-native-elements';
import {strings} from '../../../resource/languages/i18n';
import {Actions} from 'react-native-router-flux';
import ColorStyle from '../../../resource/ColorStyle';
import ProductHandle from '../../../sagas/ProductHandle';
import {UIActivityIndicator} from 'react-native-indicators';
import NavigationUtils from '../../../Utils/NavigationUtils';
import ViewItemProductSeller from '../../elements/viewItem/ViewedProduct/ViewItemProductSeller';
import EmptyView from '../../elements/reminder/EmptyView';
const LIMIT = 10;
class ListProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      page: 1,
      canLoadData: true,
      loading: false,
      searching: false,
      searchText: '',
      idStore:this.props.storeId
    };
  }
  componentDidMount() {
    this.getMyProducts();
  }

  render() {
    return (
      <View style={Styles.container}>
        {this.renderHeader()}
        {this.state.products.length === 0 && (<EmptyView
            containerStyle={{flex: 1}}
            text={strings('notProduct')}
        />)}
        {this.state.products.length > 0 && (
          <FlatList
            contentInset={{right: 0, top: 0, left: 0, bottom: 0}}
            showsVerticalScrollIndicator={false}
            data={this.state.products}
            keyExtractor={this.keyExtractor}
            renderItem={this._renderItem}
            ListFooterComponent={this.renderFooter.bind(this)}
            style={{flex: 1, height: '100%', backgroundColor: 'transparent'}}
            onEndReachedThreshold={0.4}
            onEndReached={() => this.handleLoadMore()}
          />
        )}
      </View>
    );
  }
  renderHeader() {
    if (!this.state.searching) {
      return (
        <View
          style={{
            paddingTop: Styles.constants.X*1.4,
            flexDirection: 'row',
            backgroundColor: ColorStyle.tabWhite,
            alignItems: 'center',
            justifyContent: 'center',
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20,
              elevation: 4,
              shadowColor: 'rgba(63,63,63,0.1)',
              shadowOffset: {width: 0, height: 3},
              shadowOpacity: 0.8,
              shadowRadius: 1,
          }}>
          <Text
            style={{
              ...Styles.text.text20,
              color: ColorStyle.tabBlack,
              paddingBottom: 10,
            }}>
            {strings('productList')}
          </Text>
          <TouchableOpacity
            style={{
              position: 'absolute',
              left: Styles.constants.X * 0.65,
              top: Styles.constants.X*1.4,
            }}
            onPress={() => Actions.pop()}>
            <Icon
              name="left"
              type="antdesign"
              size={25}
              color={ColorStyle.tabActive}
            />
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              position: 'absolute',
              right: Styles.constants.X * 0.65,
              top: Styles.constants.X*1.4,
            }}>
            <TouchableOpacity
              style={{marginRight: 10}}
              onPress={() =>
                this.setState({searching: true}, () => this.searchRef.focus())
              }>
              <Icon
                name="search"
                type="feather"
                size={25}
                color={ColorStyle.tabActive}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.goToUploadProduct()}>
              <Icon
                name="md-add-circle-outline"
                type="ionicon"
                size={25}
                color={ColorStyle.tabActive}
              />
            </TouchableOpacity>
          </View>
        </View>
      );
    } else {
      return (
        <View style={{
           paddingTop: Styles.constants.X/1.5,
            backgroundColor: ColorStyle.tabWhite,
            alignItems: 'center',
            justifyContent: 'center',
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20,
            elevation: 4,
            shadowColor: 'rgba(63,63,63,0.1)',
            shadowOffset: {width: 0, height: 3},
            shadowOpacity: 0.8,
            shadowRadius: 1,
        }}>
          <SearchBar
            ref={input => {
              this.searchRef = input;
            }}
            platform={'ios'}
            placeholder={strings('hintSearchProduct')}
            onChangeText={text => {}}
            value={this.state.searchText}
            containerStyle={{width: '80%', backgroundColor: 'transparent'}}
            inputStyle={{fontSize: 14}}
            onCancel={() => this.setState({searching: false})}
          />
        </View>
      );
    }
  }

  keyExtractor = (item, index) => `listProduct_${index.toString()}`;
  _renderItem = ({item, index}) => (
    <ViewItemProductSeller
      item={item}
      loading={this.state.loading}
      onClick={item => {
          NavigationUtils.goToProductDetail1(item);
      }}
      onUpdate={(productId, index) => {
        this.goToUploadProduct(productId, index);
      }}
    />
  );
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) {
      return null;
    }
    return <UIActivityIndicator color={ColorStyle.tabActive} size={20} />;
  };
  handleLoadMore() {
    if (this.state.loading) {
      return;
    }
    if (!this.state.canLoadData) {
      return;
    }
    this.setState({loading: true, page: this.state.page + 1}, () =>
      this.getMyProducts(),
    );
  }
  goToUploadProduct(productId, index) {
    NavigationUtils.goToUploadProduct(productId, this.state.idStore, index, () => {
      this.setState({products: [],page:0}, () => {
        this.getMyProducts();
      });
    });
  }
  getMyProducts() {
      let params = {
          page_index: this.state.page,
          page_size: 10,
          store_id:this.state.idStore
      };
      ProductHandle.getInstance().getProductStore(
          params,
      (isSuccess, responseData) => {
        let products = this.state.products;
        let canLoadData = true;
        let newData = [];
        if (
          isSuccess &&
          responseData !== undefined &&
          responseData.data !== undefined
        ) {
          newData = responseData.data.data;
          products = products.concat(newData);
        }
        canLoadData = newData.length === LIMIT;
        this.setState({canLoadData, products, loading: false});
      },
    );
  }
}
const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => {
  return {
    onGetCategory: param => {},
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ListProduct);
