import {connect} from 'react-redux';
import {getCategoryAction} from '../../../../../actions';
import {
  FlatList,
  RefreshControl,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {strings} from '../../../../../resource/languages/i18n';
import Styles from '../../../../../resource/Styles';
import React, {Component} from 'react';
import ColorStyle from '../../../../../resource/ColorStyle';
import ProductHandle from "../../../../../sagas/ProductHandle";
import NavigationUtils from "../../../../../Utils/NavigationUtils";
class CommonSearchRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }
  componentDidMount() {
      this.getProductSuggestList()
  }
    getProductSuggestList() {
        let param = {
            page_index:1,
            page_size: 4,
            type:'trendy'
        };
        ProductHandle.getInstance().getProductList(
            param,
            (isSuccess, responseData) => {
                if (isSuccess) {
                    if (responseData.data != null ) {
                       let mData=responseData.data
                       this.setState({data:mData.data});
                    }
                } else {
                    console.log('Failed');
                }
            },
            true,
        );
    }
  render() {
   if(this.state.data.length===0){
       return <View/>
   }else
       return (
           <View style={Styles.containerItemHome}>
               <View
                   style={{
                       width: Styles.constants.widthScreenMg24,
                       flexDirection: 'row',
                       justifyContent: 'space-between',
                   }}>
                   <Text style={Styles.text.textTitleHome}>
                       {strings('commonSearchTitle')}
                   </Text>
                   <TouchableOpacity
                       style={{
                           flexDirection: 'row',
                           alignItems: 'center',
                       }}>
                       <Icon
                           name="refresh"
                           type="font-awesome"
                           size={15}
                           color={ColorStyle.tabActive}
                       />
                       <Text
                           style={[
                               Styles.text.text12,
                               {marginLeft: 10, color: ColorStyle.tabActive},
                           ]}>
                           {strings('refresh')}
                       </Text>
                   </TouchableOpacity>
               </View>
               <FlatList
                   numColumns={2}
                   showsHorizontalScrollIndicator={false}
                   data={this.state.data}
                   style={{width: Styles.constants.widthScreenMg24, marginTop: 20}}
                   keyExtractor={this.keyExtractor}
                   renderItem={this._renderItem}
                   removeClippedSubviews={true} // Unmount components when outside of window
                   windowSize={10}
               />
           </View>
       );
  }
  keyExtractor = (item, index) => `commonSearch_${index.toString()}`;
  _renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
          onPress={()=>  NavigationUtils.goToProductDetail(item)}
        style={{width:'47%',
          backgroundColor: '#EEEEEE',
          paddingHorizontal: 15,
          paddingVertical: 8,
          margin: 4,
        }}>
        <Text style={{...Styles.text.text12,color:ColorStyle.tabBlack,textAlign:"center"}}>{item.name}</Text>
      </TouchableOpacity>
    );
  };
}

const mapStateToProps = state => {
  return {
    category: state.categoryReducers,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onGetCategory: param => {
      dispatch(getCategoryAction(param));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CommonSearchRow);
