import React, {Component} from "react";
import {FlatList, Text, TouchableOpacity, View} from "react-native";
import NewsHandle from "../../../../../sagas/NewsHandle";
import AppConstants from "../../../../../resource/AppConstants";
import Styles from "../../../../../resource/Styles";
import ColorStyle from "../../../../../resource/ColorStyle";
import {strings} from "../../../../../resource/languages/i18n";
import {Actions} from "react-native-router-flux";
import TradePromotionItem from "../../../../elements/viewItem/TradePromotionItem";
const X = Styles.constants.X;
export default class TradePromotionRow extends Component{
    constructor(props) {
        super(props);
        this.state={
            data:[]
        }
    }
componentDidMount() {
this.getData()
}
getData(){
        let param={
            page_size:5,
            page_index:1,
            type:AppConstants.getNews.PROMOTION
        }
        NewsHandle.getInstance().getTradePromotion(param,(isSuccess,data)=>{
            console.log('data:',data)
            if(isSuccess){
                this.setState({data:data.data.data})
                return
            }
        })
}
    keyExtractor = (item, index) => `trade_Row${index.toString()}`;
    _renderItemRow = ({item, index}) => {
        return (
            <TradePromotionItem
                item={item}
                index={index}
            />
        );
    };
render() {
      if(this.state.data.length===0){
          return <View/>
      }else
          return(
              <View style={[Styles.containerItemHomeFullWidth, {paddingHorizontal: 0}]}>
                  <View
                      style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          marginTop: 10,
                          marginHorizontal: Styles.constants.marginHorizontalAll,
                      }}>
                      <Text
                          style={[
                              Styles.text.text24,
                              {
                                  color: ColorStyle.textTitle24,
                                  left: 0,
                                  fontWeight: '700',

                              },
                          ]}
                          numberOfLines={2}>
                          {strings('tradePromotion')}
                      </Text>
                      <TouchableOpacity onPress={() => Actions.jump('TradePromotionScreen')}>
                          <Text
                              style={[
                                  Styles.text.text11,
                                  {fontWeight: '700', color: ColorStyle.tabActive},
                              ]}>
                              {strings('viewAll')}
                          </Text>
                      </TouchableOpacity>
                  </View>
                  <FlatList
                      data={this.state.data}
                      horizontal={true}
                      keyExtractor={this.keyExtractor}
                      showsHorizontalScrollIndicator={false}
                      renderItem={this._renderItemRow}
                  />

              </View>
          )
}
}
