import React, {Component} from 'react';
import {
  Animated,
  Dimensions,
  Image as EImage,
  ImageBackground,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import {connect} from 'react-redux';
import Styles from '../../../../../resource/Styles';
import Carousel from 'react-native-banner-carousel-updated';
import ColorStyle from '../../../../../resource/ColorStyle';
import AppConstants from '../../../../../resource/AppConstants';
import MyFastImage from '../../../../elements/MyFastImage';
import constants from '../../../../../Api/constants';
import BannerBigSaleLoading from '../../../../elements/SkeletonPlaceholder/BannerBigSaleLoading';
import {strings} from '../../../../../resource/languages/i18n';
import BannerUtils from '../../../../../Utils/BannerUtils';
import DateUtil from '../../../../../Utils/DateUtil';
const BannerWidth = Styles.constants.widthScreen;
let heights = {};
let currentIndex = 0;
let defaultHeight = (Styles.constants.heightScreen ) / 3;

class BannerBigSaleRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: defaultHeight,
      banners: BannerUtils.getData(undefined),
    };
  }
  componentDidMount() {
    BannerUtils.registerListener(banners => {
      if (banners === undefined) {
        this.props.showLoading(true);
      }
      this.props.showLoading(false);
      this.setState({banners});
    });
  }

  render() {
    let listBanner = [];
    let category = {};
    for (let i = 1; i < 10; i++) {
      category = {
        ...category,
        children: this.getBannerAtPosition(i),
      };
      if (category.children !== undefined) {
        listBanner.push(category);
      }
    }
    if (listBanner === undefined) {
      return <BannerBigSaleLoading />;
    } else {
      return (
        <View
          style={[
            {
                flex:1,
              alignItems: 'flex-start',
              marginTop:Styles.constants.marginTopAll,
            },
          ]}>
          <Text
            style={[
              Styles.text.text24,
              {
                display: this.props.title ? 'flex' : 'none',
                color: ColorStyle.textTitle24,
                fontWeight: '700',
                maxWidth: Styles.constants.X * 4.6,
                marginBottom: Styles.constants.paddingVeAll,
                marginHorizontal: Styles.constants.marginHorizontalAll,
              },
            ]}
            numberOfLines={2}>
            {strings('news')}:
          </Text>
          <View style={{width: Styles.constants.widthScreen}}>
            <Carousel
              style={{
                height: defaultHeight,
              }}
              autoplay
              autoplayTimeout={4000}
              loop
              showsPageIndicator={true}
              index={0}
              pageSize={BannerWidth}>
              {listBanner.map((item, index) => this.renderPage(item, index))}
            </Carousel>
          </View>
        </View>
      );
    }
  }
  renderPage(item, index) {
    let data = item.children;
    let url = data !== undefined ? constants.host + data.info.url : '';
    if (data === undefined) {
      data = {};
    }
    let backgroundColor = data.background_color;
    if (backgroundColor === undefined) {
      backgroundColor = '#D7F2DE|#48B765';
    }
    let colors = backgroundColor.split('|');
    if (colors.length === 0) {
      colors = ['#D7F2DE'];
    }
    return (
      <TouchableOpacity
        key={index}
        style={{
          backgroundColor: colors[0],
          flexDirection: 'row',
          justifyContent: 'space-between',
          width: '100%',
          paddingHorizontal: Styles.constants.marginHorizontal20,
          paddingVertical: Styles.constants.paddingVeAll,
        }}>
        <View>
          <Text
            numberOfLines={3}
            style={[
              Styles.text.text14,
              {color: ColorStyle.tabBlack, fontWeight: '700'},
            ]}>
            {data.name !== undefined ? data.name.toUpperCase() : ''}
          </Text>
          <Text style={[Styles.text.text10, {color: ColorStyle.tabBlack,marginTop:10}]}>
            {DateUtil.formatDate('DD/MM/YYYY', item.created_at)}
          </Text>
          <Text style={[Styles.text.text10, {color: ColorStyle.tabBlack}]}>
            {data.title}
          </Text>
        </View>
        <MyFastImage
          style={{
            width: '40%',
            height: '80%',
            overflow: 'hidden',
            borderRadius: 10,
          }}
          source={{
            uri: url,
          }}
          resizeMode={'contain'}
        />
      </TouchableOpacity>
    );
  }
  onSelect(index) {
    this.setState({selectedIndex: index});
  }
  getBannerAtPosition(position) {
    let banners =
      this.state.banners[
        `${AppConstants.BANNER_POSITION.SUGGEST_SEARCH}${position}`
      ];
    if (banners !== undefined && banners.length > 0) {
      return banners[0];
    }
    return undefined;
  }
}

const mapStateToProps = state => {
  return {banner: state.bannerReducers};
};
export default connect(mapStateToProps)(BannerBigSaleRow);
