import React, {Component} from 'react';
import {
  Dimensions,
  FlatList,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../../../resource/Styles';
const isTablet = GlobalUtil.isTablet();
import {getBannerInfoAction, getCategoryAction} from '../../../../../actions';
import categoryReducers from '../../../../../reducers/categoryRendecers';
import CategoryItem from '../../../../elements/viewItem/CategoryItem/CategoryItem';
import GlobalUtil from '../../../../../Utils/Common/GlobalUtil';
import CategoryLoading from '../../../../elements/SkeletonPlaceholder/CategoryLoading';
import DataUtils from '../../../../../Utils/DataUtils';
import ColorStyle from '../../../../../resource/ColorStyle';
import {strings} from '../../../../../resource/languages/i18n';
import CategoryHandle from '../../../../../sagas/CategoryHandle';
import {EventRegister} from 'react-native-event-listeners';
import AppConstants from '../../../../../resource/AppConstants';
const X = Styles.constants.X;
class ProductMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: this.props.loading,
    };
  }
  componentDidMount() {
    this.getCategory();
  }
  render() {
    if (this.state.data == null || this.state.data.length === 0) {
      return <CategoryLoading />;
    }
    // let itemCountTablet = Math.round(Dimensions.get('window').width / 100);
    // let itemCount = Math.round(this.props.category.length / 2);
    // let columnNumber = isTablet ? itemCountTablet : itemCount;
    return (
      <View style={[Styles.containerItemHomeFullWidth]}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginVertical: 10,
          }}>
          <Text
            style={[
              Styles.text.text24,
              {
                color: ColorStyle.textTitle24,
                fontWeight: '700',
              },
            ]}
            numberOfLines={2}>
            {strings('topCategory')}
          </Text>
          <TouchableOpacity
            onPress={() =>
            {
              EventRegister.emit(AppConstants.EventName.GO_TO_TAB, 2)
            }
            }>
            <Text
              style={[
                Styles.text.text11,
                {fontWeight: '700', color: ColorStyle.tabActive},
              ]}>
              {strings('viewAll')}
            </Text>
          </TouchableOpacity>
        </View>
        <FlatList
          contentContainerStyle={{
            alignSelf: 'center',
          }}
          numColumns={4}
          data={DataUtils.formatCategory(this.state.data)}
          keyExtractor={this.keyExtractor}
          renderItem={this._renderItem}
        />
      </View>
    );
  }
  keyExtractor = (item, index) => `productMenu${index.toString()}`;
  _renderItem = ({item, index}) => (
    <CategoryItem item={item} index={index} loading={this.state.loading} />
  );
  getCategory() {
      CategoryHandle.getInstance().getAllCategory((data)=> this.setState({data},()=>{
          this.setState({data})
      }));
  }
}
const mapStateToProps = state => {
  return {
    category: state.categoryReducers,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onGetCategory: param => {
      dispatch(getCategoryAction(param));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductMenu);
