import React, {Component} from 'react';
import {
  Animated,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  View,
} from 'react-native';
import Carousel from 'react-native-banner-carousel-updated';
import Styles from '../../../../../resource/Styles';
import constants from '../../../../../Api/constants';
import AutoHeightImage from '../../../../elements/AutoHeightImage';
import ImageHelper from '../../../../../resource/images/ImageHelper';
import {connect} from 'react-redux';
import BannerLoading from '../../../../elements/SkeletonPlaceholder/BannerLoading';
import BannerHandle from "../../../../../sagas/BannerHandle";
import CategoryHandle from "../../../../../sagas/CategoryHandle";
import AppConstants from "../../../../../resource/AppConstants";
import NavigationUtils from "../../../../../Utils/NavigationUtils";
import ProductHandle from "../../../../../sagas/ProductHandle";
import {Actions} from "react-native-router-flux";
const BannerWidth = Dimensions.get('window').width ;
class AppBannerRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      banners: [],
      type: this.props.type,
      height: this.props.backgroundStyle.height,
        loading:false
    };
  }
  componentDidMount() {
    this.getBanners()
  }
  getBanners() {
      let params={
        type:this.props.type
      }
    BannerHandle.getInstance().getBannerList(params,(isSuccess, dataResponse) => {
      if (isSuccess) {
          console.log(dataResponse.data)
        this.setState({banners:dataResponse.data,loading:true})
      }
    });
  }
  render() {
    let banners = this.state.banners;
    let containerStyle =
      this.props.containerStyle !== undefined ? this.props.containerStyle : {};
    if (!this.state.loading) {
          return <BannerLoading />;
      }
    else  if (banners.length===0){
          return <View/>;
      }else {
          return (
              <View style={{...containerStyle}}>{this.renderBanner(banners)}</View>
          );
      }
  }
  renderBanner(banners) {
      return (
        <View
          style={{
            height: this.props.backgroundStyle.height,
            width: Styles.constants.widthScreen-10,
            alignSelf: 'center',
            marginTop: 10,
            overflow: 'hidden',
            alignItems: 'center',
          }}>
          <Carousel
            style={{
              width: Dimensions.get('window').width,
              height: this.props.backgroundStyle.height,
            }}
            autoplay={true}
            autoplayTimeout={5000}
            loop={true}
            index={0}
            pageSize={BannerWidth}>
            {banners.map((banner, index) => this.renderPage(banner, index))}
          </Carousel>
        </View>
      );
  }

  renderPage(banner, index) {
    let url = '';
    if (
      banner === undefined ||
      banner.image_url === undefined
    ) {
      url = 'https://picsum.photos/300/500?productId=' + index;
    } else {
      url =  constants.host+ banner.image_url[0].url;
    }
    return (
      <ImageBackground key={index} source={ImageHelper.imgBlur} style={{}}>
        <TouchableOpacity
          onPress={() => {
            // this.goToBanner(banner);
          }}>
          <AutoHeightImage width={BannerWidth} source={{uri: url}} />
        </TouchableOpacity>
      </ImageBackground>
    );
  }

goToBanner(banner) {
  if(banner.type ===null){
      return
  }
    if (banner.type.name === AppConstants.BANNER_POSITION.NEWS) {
        NavigationUtils.goToNewsDetail(
            undefined,
            banner.id,
            0,
            undefined
        );      return;
    }else if (banner.type.name === AppConstants.BANNER_POSITION.PRODUCT) {
        ProductHandle.getInstance().getProductDetail(
            banner.id,undefined, (isSuccess, dataResponse) => {
                if (isSuccess) {
                    let data=dataResponse.data;
                    Actions.jump('productDetail', {
                        item: data
                    });
                }
            })
        return;
    }else if (banner.type.name === AppConstants.BANNER_POSITION.CATEGORY) {
        CategoryHandle.getInstance().goToProductCategoryId(banner.id);
        return;
    }
  }
}
const mapStateToProps = state => {
  return {
    banner: state.bannerReducers,
  };
};
export default connect(mapStateToProps)(AppBannerRow);
