import React, {Component} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  Text,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../../../resource/Styles';
import ColorStyle from '../../../../../resource/ColorStyle';
import ImageHelper from '../../../../../resource/images/ImageHelper';
import ViewProductItem from '../../../../elements/viewItem/ViewedProduct/ViewProductItem';
import ProductRowLoading from '../../../../elements/SkeletonPlaceholder/ProductRowLoading';
import ProductHandle from '../../../../../sagas/ProductHandle';
import NavigationUtils from '../../../../../Utils/NavigationUtils';
const X = Styles.constants.X;
const LIMIT = 20;
class ProductRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: this.props.loading,
    };
  }
  componentDidMount() {
    this.bestSelling();
  }
  render() {
    if (this.state.loading) {
      return <ProductRowLoading />;
    } else if(this.state.data.length===0) return <View/>
    return (
      <View style={{marginTop:Styles.constants.marginTopAll}}>
        <View
          style={{
            ...Styles.border.borderItemHome,
            width: Styles.constants.widthScreenMg24,
            height: X * 3.4,
            alignSelf:'center',
            alignItems: 'flex-start',
            backgroundColor: ColorStyle.tabWhite,
            paddingHorizontal: 20,
            justifyContent: 'center',
          }}>
          <Text
            style={[
              Styles.text.text24,
              {
                color: ColorStyle.textTitle24,
                left: 0,
                fontWeight: '700',
                maxWidth: X * 4.6,
              },
            ]}
            numberOfLines={2}>
            {this.props.title.toUpperCase()}
          </Text>
          <Image
            source={ImageHelper.product}
            style={{
              height: X * 6,
              width: X * 5,
              position: 'absolute',
              bottom: -30,
              right: -20,
            }}
          />
        </View>
        <FlatList
          data={this.state.data}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          keyExtractor={this.keyExtractor}
          style={{marginTop: 10}}
          renderItem={this._renderItem}
        />
      </View>
    );
  }
  bestSelling() {
    let param = {
      page_index: 1,
      page_size: this.props.size,
      type: this.props.type,
    };
    this.props.showLoading(true);
      this.setState({loading:true})
    ProductHandle.getInstance().getProductList(
      param,
      (isSuccess, responeData) => {
        this.props.showLoading(false);
        this.setState({loading:false})
        if (isSuccess) {
          if (responeData.data != null && responeData.data.data) {
            if (responeData.data.length < LIMIT) {
              this.setState({canLoadData: false});
            }

            this.setState({data: responeData.data.data});
          }
        } else {
          console.log('getListBestSellProduct = Failed');
        }
      },
      true,
    );
  }
  keyExtractor = (item, index) => `productRow_${index.toString()}`;
  _renderItem = ({item, index}) => (
    <ViewProductItem
      item={item}
      loading={this.state.loading}
      onClick={item => {
        NavigationUtils.goToProductDetail(item);
      }}
    />
  );
}
const mapStateToProps = state => {
  return {
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onGetProduct: param => {
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductRow);
