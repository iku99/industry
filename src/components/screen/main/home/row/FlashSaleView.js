import React, {Component, useEffect} from 'react';
import {
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
} from 'react-native';
import ImageHelper from '../../../../../resource/images/ImageHelper';
import AppConstants from '../../../../../resource/AppConstants';
import Styles from '../../../../../resource/Styles';
import ColorStyle from '../../../../../resource/ColorStyle';
import {Actions} from 'react-native-router-flux';
import {strings} from '../../../../../resource/languages/i18n';
import FlashSaleProductItem from '../../../../elements/viewItem/flashSaleItem/FlashSaleProductItem';
import TimeUtils from "../../../../../Utils/TimeUtils";

let flashSaleTime = 36000;
const DATA = [
  {
    priceId: 48,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 64,
        thumbnail_url:
          '/upload/product/88201b7d-203b-463b-8deb-e1b614757345.jpg',
        url: '/upload/product/88201b7d-203b-463b-8deb-e1b614757345.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 65,
        thumbnail_url:
          '/upload/product/094cb8c2-33a2-4e55-960c-8c4dec972480.jpg',
        url: '/upload/product/094cb8c2-33a2-4e55-960c-8c4dec972480.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 66,
        thumbnail_url:
          '/upload/product/1e3a1bf2-5da4-4150-bbb5-0377ca5d00ad.jpg',
        url: '/upload/product/1e3a1bf2-5da4-4150-bbb5-0377ca5d00ad.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 67,
        thumbnail_url:
          '/upload/product/58b213ca-3538-4c25-b1de-3de3505dbaf6.jpg',
        url: '/upload/product/58b213ca-3538-4c25-b1de-3de3505dbaf6.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 68,
        thumbnail_url:
          '/upload/product/cc107ba7-1ce8-4d70-a10c-7cbffb19544a.jpg',
        url: '/upload/product/cc107ba7-1ce8-4d70-a10c-7cbffb19544a.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 69,
        thumbnail_url:
          '/upload/product/5c7f7699-0a3b-427e-84a2-1c0d262d62d1.jpg',
        url: '/upload/product/5c7f7699-0a3b-427e-84a2-1c0d262d62d1.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 22,
    name: 'Smart Tivi QLED Samsung 4K 43 inch QA43Q65R ',
    category_id: 8,
    store_id: 1,
    short_description:
      'Độ phân giải 4K sắc nét, tối ưu độ tương phản giữa các khung hình nhờ công nghệ Quantum HDR.\nTối ưu và nâng cấp độ nét hình ảnh, tự động điều chỉnh ánh sáng và âm thanh nhờ bộ xử Lý Quantum 4K.\nGóc nhìn rộng hơn với công nghệ Wide Viewing Angle.\nHệ điều hành TizenOS nhiều ứng dụng phong phú, One remote điều khiển, tìm kiếm thông tin linh hoạt.\nHỗ trợ điều khiển tivi bằng điện thoại thông qua ứng dụng SmartThings.\nChiếu màn hình điện thoại lên tivi tiện lợi bằng Screen Mirroring, Airplay 2.',
    full_description:
      '<p><img alt="Thông số kỹ thuật Smart Tivi QLED Samsung 4K 43 inch QA43Q65R" src="https://cdn.tgdd.vn/Products/Images/1942/200398/Kit/tivi-samsung-qa43q65r.jpg" style="height:585px; width:780px" /></p>\n\n<p>Thiết kế sang trọng, hiện đại</p>\n\n<p><a href="https://www.dienmayxanh.com/tivi/samsung-qa43q65r" target="_blank">Smart Tivi QLED Samsung 4K 43 inch QA43Q65R</a>&nbsp;thuộc d&ograve;ng tivi QLED cao cấp của h&atilde;ng Samsung. Thiết kế đơn giản nhưng hiện đại, l&agrave;m nổi bật vẻ đẹp của chiếc&nbsp;<a href="https://www.dienmayxanh.com/tivi" target="_blank">tivi</a>&nbsp;khi đặt trong ph&ograve;ng kh&aacute;ch nh&agrave; bạn.</p>\n\n<p>Chiếc&nbsp;<a href="https://www.dienmayxanh.com/tivi?g=tu-32-43-inch" target="_blank">tivi 43 inch</a>&nbsp;n&agrave;y c&oacute; ch&acirc;n đế dạng chữ V ngược chắc chắn, đứng vững tr&ecirc;n mọi bề mặt phẳng. Ngo&agrave;i ra viền m&agrave;n h&igrave;nh&nbsp;<a href="https://www.dienmayxanh.com/tivi-samsung" target="_blank">tivi Samsung</a>&nbsp;cực mỏng c&ograve;n gi&uacute;p bạn mở rộng khung h&igrave;nh.</p>\n\n<p><img alt="Smart Tivi QLED Samsung 4K 43 inch QA43Q65R - thiết kế" src="https://cdn.tgdd.vn/Products/Images/1942/200398/samsung-qa43q65r-16.jpg" /></p>\n\n<p>Độ ph&acirc;n giải&nbsp;<a href="https://www.dienmayxanh.com/kinh-nghiem-hay/tivi-uhd-tivi-4k-la-gi-co-khac-gi-so-voi-tivi-full-578555" target="_blank">4K</a>&nbsp;cực kỳ sắc n&eacute;t</p>\n\n<p><img alt="Smart Tivi QLED Samsung 4K 43 inch QA43Q65R - độ phân giải" src="https://cdn.tgdd.vn/Products/Images/1942/200398/samsung-qa43q65r-7.jpg" /></p>\n\n<p>C&ocirc;ng nghệ Quantum HDR ti&ecirc;n tiến gi&uacute;p tối ưu độ tương phản giữa c&aacute;c khung h&igrave;nh</p>\n\n<p>Độ sâu và độ sáng của màu trắng và màu đen được thể hiện chi tiết hơn.</p>\n\n<p><img alt="Smart Tivi QLED Samsung 4K 43 inch QA43Q65R - Quantum HDR" src="https://cdn.tgdd.vn/Products/Images/1942/200398/samsung-qa43q65r-8.jpg" /></p>\n\n<p>Bộ xử l&yacute; Quantum 4K mạnh mẽ</p>\n\n<p>Samsung t&iacute;ch hợp bộ xử l&yacute; c&ocirc;ng nghệ mới gi&uacute;p chiếc tivi c&oacute; thể tối ưu v&agrave; n&acirc;ng cấp độ n&eacute;t h&igrave;nh ảnh, tự động điều chỉnh &aacute;nh s&aacute;ng v&agrave; &acirc;m thanh.</p>\n\n<p><img alt="Smart Tivi QLED Samsung 4K 43 inch QA43Q65R - Quantum 4K" src="https://cdn.tgdd.vn/Products/Images/1942/200398/samsung-qa43q65r-18.jpg" /></p>\n\n<p>G&oacute;c nh&igrave;n rộng hơn với c&ocirc;ng nghệ Wide Viewing Angle</p>\n\n<p><img alt="Smart Tivi QLED Samsung 4K 43 inch QA43Q65R - Wide viewing angle" src="https://cdn.tgdd.vn/Products/Images/1942/200398/samsung-qa43q65r-9.jpg" /></p>\n\n<p>C&ocirc;ng nghệ &acirc;m thanh Dolby cho &acirc;m thanh sống động, b&ugrave;ng nổ</p>\n\n<p><img alt="Smart Tivi QLED Samsung 4K 43 inch QA43Q65R - Âm thanh" src="https://cdn.tgdd.vn/Products/Images/1942/200398/samsung-qa43q65r-15.jpg" /></p>\n\n<p><a href="https://www.dienmayxanh.com/kinh-nghiem-hay/he-dieu-hanh-tizen-cua-tivi-samsung-co-gi-dac-sac-796409" target="_blank">Hệ điều h&agrave;nh Tizen</a>&nbsp;với kho ứng dụng phong ph&uacute;</p>\n\n<p>Hệ điều Tizen được t&iacute;ch hợp tr&ecirc;n&nbsp;<a href="https://www.dienmayxanh.com/tivi-samsung?g=smart-tivi" target="_blank">smart tivi Samsung</a>&nbsp;dễ d&agrave;ng sử dụng, giao diện đơn giản, đi k&egrave;m rất nhiều ứng dụng giải tr&iacute; quen thuộc như FPT Play, Netflix, Youtube,... Ngo&agrave;i ra c&ograve;n t&iacute;ch hợp iTunes Movies với h&agrave;ng ng&agrave;n phim bản quyền chất lượng cao.</p>\n\n<p><img alt="hệ điều hành" src="https://cdn.tgdd.vn/Products/Images/1942/200398/Slider/vi-vn-8.jpg" /></p>\n\n<p>One remote thế hệ mới c&oacute; hỗ trợ t&igrave;m kiếm giọng n&oacute;i tiếng Việt tr&ecirc;n ứng dụng Youtube tiện lợi, điều khiển nhiều thiết bị c&ugrave;ng 1 l&uacute;c</p>\n\n<p><img alt="tìm kiếmg giọng nói" src="https://cdn.tgdd.vn/Products/Images/1942/200398/Slider/-qled.jpg" /></p>\n\n<p>Điều khiển tivi bằng điện thoại th&ocirc;ng qua ứng dụng&nbsp;<a href="https://www.dienmayxanh.com/kinh-nghiem-hay/huong-dan-su-dung-ung-dung-smartthings-dieu-khien-1126015" target="_blank">SmartThings</a></p>\n\n<p><img alt="Smart Tivi QLED Samsung 4K 43 inch QA43Q65R - Điều khiển tivi" src="https://cdn.tgdd.vn/Products/Images/1942/200398/samsung-qa43q65r-23.jpg" /></p>\n\n<p>Chiếu m&agrave;n h&igrave;nh điện thoại l&ecirc;n tivi tiện lợi bằng&nbsp;<a href="https://www.dienmayxanh.com/kinh-nghiem-hay/cach-trinh-chieu-hinh-anh-tu-dien-thoai-len-tivi-s-647680" target="_blank">Screen Mirroring</a></p>\n\n<p>Lưu &yacute;: Ứng dụng chỉ hoạt động được tr&ecirc;n 1 số điện thoại sử dụng hệ điều h&agrave;nh Android.</p>\n\n<p><img alt="Smart Tivi QLED Samsung 4K 43 inch QA43Q65R - Chiếu màn hình" src="https://cdn.tgdd.vn/Products/Images/1942/200398/samsung-qa43q65r-12.jpg" /></p>\n\n<p>Chiếu m&agrave;n h&igrave;nh c&aacute;c thiết bị Apple (iPhone, iPad, Macbook) l&ecirc;n m&agrave;n ảnh&nbsp;<a href="https://www.dienmayxanh.com/tivi?g=tivi-qled" target="_blank">tivi QLED</a>&nbsp;nhờ t&iacute;nh năng AirPlay 2</p>\n\n<p><img alt="Smart Tivi QLED Samsung 4K 43 inch QA43Q65R - Airplay 2" src="https://cdn.tgdd.vn/Products/Images/1942/200398/samsung-qa43q65r-21.jpg" /></p>\n\n<p>Đa dạng cổng kết nối</p>\n\n<p><img alt="Smart Tivi QLED Samsung 4K 43 inch QA43Q65R - Kết nối" src="https://cdn.tgdd.vn/Products/Images/1942/200398/samsung-qa43q65r-14.jpg" /></p>\n',
    price: 1.2729e7,
    original_price: 1.669e7,
    sku: '43Q65R',
    qty: 2,
    rank_total: 0,
    features: [
      {
        global_feature_id: 1,
        value: 'Việt Nam',
        name: 'Xuất xứ',
      },
      {
        global_feature_id: 2,
        value: 'Samsung',
        name: 'Thương Hiệu',
      },
      {
        global_feature_id: 6,
        value: '220V/50Hz',
        name: 'Điện Áp',
      },
    ],
    option_groups: [],
    options: [],
    created_at: '2019-10-20T00:00:00.000+00:00',
    sell_count: 2,
  },
  {
    priceId: 46,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 48,
        thumbnail_url:
          '/upload/product/5e1f2836-d1d9-4299-94da-598793bba11b.jpg',
        url: '/upload/product/5e1f2836-d1d9-4299-94da-598793bba11b.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 49,
        thumbnail_url:
          '/upload/product/006e1249-8854-4ff5-998b-0c0d5e4ee6a8.jpg',
        url: '/upload/product/006e1249-8854-4ff5-998b-0c0d5e4ee6a8.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 50,
        thumbnail_url:
          '/upload/product/94ef06ac-0318-4fa5-9e47-afe35f5891fe.jpg',
        url: '/upload/product/94ef06ac-0318-4fa5-9e47-afe35f5891fe.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 51,
        thumbnail_url:
          '/upload/product/b95fd210-bda5-4047-a027-3917b80ddf98.jpg',
        url: '/upload/product/b95fd210-bda5-4047-a027-3917b80ddf98.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 52,
        thumbnail_url:
          '/upload/product/1ade9f2d-2bd8-4cf6-a42f-699725e13490.jpg',
        url: '/upload/product/1ade9f2d-2bd8-4cf6-a42f-699725e13490.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 53,
        thumbnail_url:
          '/upload/product/d4d6e330-32cb-40bc-82d6-b0bd7f3ebdff.jpg',
        url: '/upload/product/d4d6e330-32cb-40bc-82d6-b0bd7f3ebdff.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 54,
        thumbnail_url:
          '/upload/product/12e8f431-2e3b-4ee4-a7ca-d6e055de7c35.jpg',
        url: '/upload/product/12e8f431-2e3b-4ee4-a7ca-d6e055de7c35.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 55,
        thumbnail_url:
          '/upload/product/d99f7169-db84-4304-9a66-ffc8a389b888.jpg',
        url: '/upload/product/d99f7169-db84-4304-9a66-ffc8a389b888.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 20,
    name: 'Tủ lạnh LG Inverter 393 lít GN-D422BL',
    category_id: 27,
    store_id: 1,
    short_description:
      'Tiết kiệm đến 50% lượng điện năng tiêu thụ nhờ công nghệ Inverter\nBảo quản thực phẩm toàn diện với công nghệ làm lạnh đa chiều\nCông nghệ Nano Carbon khử mùi mạnh mẽ\nDoor Cooling+ cải thiện tối ưu hiệu quả làm lạnh thực phẩm\nChuẩn đoán và khắc phục lỗi nhanh nhờ chức năng thông minh Smart Diagnosis\nGiúp rau củ tươi lâu trong ngăn cân bằng độ ẩm lưới mắt cáo',
    full_description:
      '<p><img alt="Thông số kỹ thuật Tủ lạnh LG Inverter 393 lít GN-D422BL" src="https://cdn.tgdd.vn/Products/Images/1943/202826/Kit/lg-gn-d422bl.jpg" style="height:585px; width:780px" /></p>\n\n<p>M&agrave;u sắc sang trọng, kiểu d&aacute;ng hiện đại</p>\n\n<p>Sở hữu gam m&agrave;u đen sang trọng,&nbsp;<a href="https://www.dienmayxanh.com/tu-lanh/lg-gn-d422bl" target="_blank">LG Inverter 393 l&iacute;t GN-D422BL</a>&nbsp;thuộc mẫu tủ lạnh&nbsp;<a href="https://www.dienmayxanh.com/tu-lanh?g=ngan-da-tren" target="_blank">ngăn đ&aacute; tr&ecirc;n</a>&nbsp;- kh&ocirc;ng chỉ mang lại cảm gi&aacute;c th&acirc;n thuộc về kiểu tủ truyền thống để sử dụng m&agrave; c&ograve;n trở th&agrave;nh nội thất hiện đại, bắt mắt trong kh&ocirc;ng gian nh&agrave; bạn.</p>\n\n<p><img alt="Tủ lạnh LG Inverter 393 lít GN-D422BL - Màu sắc sang trọng, kiểu dáng hiện đại" src="https://cdn.tgdd.vn/Products/Images/1943/202826/Slider/vi-vn-lg-gn-d422bl-1.jpg" /></p>\n\n<p>Dung t&iacute;ch 393 l&iacute;t, ph&ugrave; hợp gia đ&igrave;nh 3 &ndash; 4 người</p>\n\n<p>Chiếc&nbsp;<a href="https://www.dienmayxanh.com/tu-lanh-lg" target="_blank">tủ lạnh LG</a>&nbsp;n&agrave;y c&oacute; dung t&iacute;ch sử dụng trung b&igrave;nh l&agrave; 393 l&iacute;t, trong đ&oacute; dung t&iacute;ch ngăn đ&aacute; l&agrave; 110 l&iacute;t v&agrave; dung t&iacute;ch ngăn lạnh l&agrave; 283 l&iacute;t, ho&agrave;n to&agrave;n ph&ugrave; hợp cho những hộ gia đ&igrave;nh gồm 3 &ndash; 4 th&agrave;nh vi&ecirc;n thoải m&aacute;i sử dụng.</p>\n\n<p><img alt="Tủ lạnh LG Inverter 393 lít GN-D422BL - Dung tích 393 lít, phù hợp gia đình 3 – 4 người" src="https://cdn.tgdd.vn/Products/Images/1943/202826/Slider/vi-vn-lg-gn-d422bl-2.jpg" /></p>\n\n<p>Tiết kiệm điện, tủ chạy &ecirc;m, bền bỉ nhờ&nbsp;<a href="https://www.dienmayxanh.com/kinh-nghiem-hay/tu-lanh-inverter-la-gi-585937" target="_blank">c&ocirc;ng nghệ Inverter</a></p>\n\n<p>Tủ lạnh LG model GN-D422BL được t&iacute;ch hợp c&ocirc;ng nghệ Inverter mới nhất nhằm mang lại hiệu quả tiết kiệm điện l&ecirc;n đến 50%. Bạn c&oacute; thể y&ecirc;n t&acirc;m về lượng điện năng ti&ecirc;u thụ mỗi th&aacute;ng, b&ecirc;n cạnh việc h&agrave;i l&ograve;ng về m&aacute;y n&eacute;n của tủ lạnh hoạt động &ecirc;m &aacute;i v&agrave; nhiệt độ l&uacute;c n&agrave;o cũng được duy tr&igrave; ổn định.</p>\n\n<p><img alt="Tủ lạnh LG Inverter 393 lít GN-D422BL - Tiết kiệm điện lên 50% nhờ công nghệ Inverter" src="https://cdn.tgdd.vn/Products/Images/1943/202826/Slider/vi-vn-lg-gn-d422bl-3.jpg" /></p>\n\n<p>L&agrave;m lạnh thực phẩm to&agrave;n diện với&nbsp;<a href="https://www.dienmayxanh.com/kinh-nghiem-hay/he-thong-lam-lanh-da-chieu-tren-tu-lanh-la-gi-796059" target="_blank">c&ocirc;ng nghệ đa chiều</a></p>\n\n<p>Nhờ sự hoạt động của c&ocirc;ng nghệ l&agrave;m lạnh đa chiều, thực phẩm chứa b&ecirc;n trong tủ lạnh sẽ được bảo quản một c&aacute;ch to&agrave;n diện bởi sự ph&acirc;n bổ đồng đều của luồng kh&iacute; lạnh.</p>\n\n<p>Đ&acirc;y l&agrave; hệ thống l&agrave;m lạnh đ&atilde; được cải tiến rất nhiều tr&ecirc;n những mẫu&nbsp;<a href="https://www.dienmayxanh.com/tu-lanh-inverter" target="_blank">tủ lạnh Inverter</a>&nbsp;mới hiện nay.</p>\n\n<p><img alt="Tủ lạnh LG Inverter 393 lít GN-D422BL-Làm lạnh thực phẩm toàn diện với công nghệ đa chiều" src="https://cdn.tgdd.vn/Products/Images/1943/202826/Slider/vi-vn-lg-gn-d422bl-4.jpg" /></p>\n\n<p>Khửi m&ugrave;i to&agrave;n diện với&nbsp;<a href="https://www.dienmayxanh.com/kinh-nghiem-hay/cong-nghe-khang-khuan-tren-tu-lanh-lg-585629#nano-cacbon" target="_blank">c&ocirc;ng nghệ Nano Carbon</a></p>\n\n<p>V&ocirc; hiệu h&oacute;a sự hoạt động của vi khuẩn v&agrave; loại bỏ m&ugrave;i h&ocirc;i to&agrave;n diện, c&ocirc;ng Nano Cacbon mang đến sự h&agrave;i l&ograve;ng cho bạn mỗi khi mở cửa tủ lạnh ra m&agrave; kh&ocirc;ng c&ograve;n cảm thấy kh&oacute; chịu v&igrave; m&ugrave;i thực phẩm.</p>\n\n<p><img alt="Tủ lạnh LG Inverter 393 lít GN-D422BL - Khửi mùi toàn diện với công nghệ Nano Carbon" src="https://cdn.tgdd.vn/Products/Images/1943/202826/Slider/vi-vn-lg-gn-d422bl-7.jpg" /></p>\n\n<p>Cải thiện hiệu quả l&agrave;m lạnh nhờ&nbsp;<a href="https://www.dienmayxanh.com/kinh-nghiem-hay/cong-nghe-door-cooling-tren-tu-lanh-lg-la-gi-1000960" target="_blank">c&ocirc;ng nghệ Door Cooling+</a></p>\n\n<p>C&ocirc;ng nghệ Door Cooling+ ch&iacute;nh l&agrave; luồng kh&iacute; lạnh được tạo ra từ c&aacute;nh cửa tủ, nhằm cải thiện t&igrave;nh trạng mất thời gian để chờ đợi thực phẩm được l&agrave;m lạnh ở vị tr&iacute; b&ecirc;n cửa tủ.</p>\n\n<p>N&oacute;i một c&aacute;ch kh&aacute;c, nhờ c&oacute; c&ocirc;ng nghệ n&agrave;y, luồng kh&iacute; lạnh sẽ kh&ocirc;ng bị cản trở bởi số lượng thực phẩm chứa b&ecirc;n trong tủ v&agrave; gi&uacute;p thực phẩm được l&agrave;m lạnh nhanh ch&oacute;ng hơn, nhất l&agrave; ở vị tr&iacute; c&aacute;nh cửa tủ lạnh.</p>\n\n<p><img alt="Tủ lạnh LG Inverter 393 lít GN-D422BL - Cải thiện hiệu quả làm lạnh nhờ công nghệ Door Cooling+" src="https://cdn.tgdd.vn/Products/Images/1943/202826/Slider/vi-vn-lg-gn-d422bl-6.jpg" /></p>\n\n<p>Bảo quản rau củ tươi l&acirc;u trong&nbsp;<a href="https://www.dienmayxanh.com/kinh-nghiem-hay/ngan-can-bang-do-am-tren-tu-lanh-lg-681390" target="_blank">ngăn c&acirc;n bằng độ ẩm lưới mắt c&aacute;o</a></p>\n\n<p>H&atilde;ng LG thiết kế đặc biệt khi trang bị th&ecirc;m lưới mắt cao trong ngăn c&acirc;n bằng độ ẩm, nhằm cải thiện tối đa trong việc duy tr&igrave; độ ẩm cho rau củ quả. V&igrave; nhiệm vụ của lưới mắt c&aacute;o ch&iacute;nh l&agrave; giữ lại hơi nước &ndash; bốc ra từ rau củ, để duy tr&igrave; v&agrave; ổn định độ ẩm vốn c&oacute; b&ecirc;n trong ngăn.</p>\n\n<p><img alt="Tủ lạnh LG Inverter 393 lít GN-D422BL - Bảo quản rau củ tươi lâu trong ngăn cân bằng độ ẩm lưới mắt cáo" src="https://cdn.tgdd.vn/Products/Images/1943/202826/Slider/vi-vn-lg-gn-d422bl-8.jpg" /></p>\n\n<p>Khắc phục lỗi nhanh với t&iacute;nh năng th&ocirc;ng minh&nbsp;<a href="https://www.dienmayxanh.com/kinh-nghiem-hay/cong-nghe-moi-tren-tu-lanh-lg-2017-1001842#hmenuid5" target="_blank">Smart Diagnosis</a></p>\n\n<p>T&iacute;nh năng Smart Diagnosis được kết nối với chiếc điện thoại th&ocirc;ng minh, sẽ gi&uacute;p bạn chuẩn đo&aacute;n v&agrave; khắc phục nhanh c&aacute;c lỗi m&agrave; tủ lạnh LG Inverter GN-D422BL đang xảy ra.</p>\n\n<p>Ngo&agrave;i ra, t&iacute;nh năng n&agrave;y c&ograve;n gi&uacute;p bạn tiết kiệm thời gian khi c&oacute; thể nhờ ngay đến trung t&acirc;m bảo h&agrave;nh xử l&yacute; c&aacute;c lỗi tủ lạnh chỉ qua điện thoại.</p>\n\n<p><img alt="Tủ lạnh LG Inverter 393 lít GN-D422BL - Khắc phục lỗi nhanh với tính năng thông minh Smart Diagnosis" src="https://cdn.tgdd.vn/Products/Images/1943/202826/Slider/vi-vn-lg-gn-d422bl-9.jpg" /></p>\n\n<p>C&oacute; thể n&oacute;i, chiếc tủ lạnh LG Inverter 393 l&iacute;t GN-D422BL dường như đ&aacute;p ứng đầy đủ về mặt sử dụng c&ocirc;ng nghệ v&agrave; cũng sở hữu cho m&igrave;nh dung t&iacute;ch tương đối vừa phải, ph&ugrave; hợp cho những hộ gia đ&igrave;nh kh&ocirc;ng qu&aacute; đ&ocirc;ng người.</p>\n',
    price: 1.0489e7,
    original_price: 1.349e7,
    sku: 'TULANHLG2',
    qty: 3,
    rank_total: 0,
    features: [
      {
        global_feature_id: 1,
        value: 'Indonesia',
        name: 'Xuất xứ',
      },
      {
        global_feature_id: 2,
        value: 'LG',
        name: 'Thương Hiệu',
      },
      {
        global_feature_id: 6,
        value: '220V/50Hz',
        name: 'Điện Áp',
      },
      {
        global_feature_id: 12,
        value: '427L',
        name: 'Dung Tích',
      },
    ],
    option_groups: [],
    options: [],
    created_at: '2019-10-20T00:00:00.000+00:00',
    sell_count: 1,
  },
  {
    priceId: 1247,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 35,
        thumbnail_url:
          '/upload/product/8b2ab17d-5958-46c9-89a9-3af9a9b528f9.jpg',
        url: '/upload/product/8b2ab17d-5958-46c9-89a9-3af9a9b528f9.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 36,
        thumbnail_url:
          '/upload/product/6ca4fb2c-e373-47c7-94a8-ab301ba2d4b5.jpg',
        url: '/upload/product/6ca4fb2c-e373-47c7-94a8-ab301ba2d4b5.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 37,
        thumbnail_url:
          '/upload/product/1f709a02-8a68-4d26-8ec7-bffa4a60573a.jpg',
        url: '/upload/product/1f709a02-8a68-4d26-8ec7-bffa4a60573a.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 38,
        thumbnail_url:
          '/upload/product/9c359645-7db7-49ea-a462-552091bfba28.jpg',
        url: '/upload/product/9c359645-7db7-49ea-a462-552091bfba28.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 39,
        thumbnail_url:
          '/upload/product/c222e5e3-413c-40df-933e-6bdd14e83732.jpg',
        url: '/upload/product/c222e5e3-413c-40df-933e-6bdd14e83732.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 15,
    name: 'Tủ Lạnh Mini Electrolux EUM0900SA (90L) - Hàng Chính Hãng',
    category_id: 411,
    store_id: 1,
    short_description:
      'Dung tích: 90L\nKiểu tủ: 1 Cửa\nCông nghệ: Non-Inverter\nLàm lạnh: Hệ thống làm lạnh trực tiếp\nKhay: Kính chịu lực',
    full_description:
      '<p>Tủ lạnh mini 1 cửa nhỏ gọn</p>\n\n<p><strong>Tủ Lạnh Mini Electrolux EUM0900SA</strong>&nbsp;thuộc d&ograve;ng tủ lạnh mini 1 cửa nhỏ gọn, ph&ugrave; hợp lắp đặt sử dụng trong kh&ocirc;ng gian nhỏ, hay gia đ&igrave;nh &iacute;t người (dưới 3 th&agrave;nh vi&ecirc;n). Tủ c&oacute; kiểu d&aacute;ng đơn giản, phần cửa tủ được l&agrave;m bằng th&eacute;p kh&ocirc;ng gỉ s&aacute;ng b&oacute;ng gi&uacute;p bạn dễ d&agrave;ng ph&aacute;t hiện ra vết bẩn l&agrave; lau ch&ugrave;i thuận tiện. Th&ecirc;m v&agrave;o đ&oacute;, tủ c&oacute; dung t&iacute;ch sử dụng 90L, cho ph&eacute;p bạn thoải m&aacute;i chứa nhiều loại thực phẩm kh&aacute;c nhau.</p>\n\n<p><img alt="Tủ Lạnh Mini Electrolux EUM0900SA (90L)" src="https://vcdn.tikicdn.com/ts/tmp/a4/2e/6e/12d4a579f2728c3105c684844a696ac6.jpg" style="height:500px; width:750px" /></p>\n\n<p>Khay k&iacute;nh chịu lực an to&agrave;n v&agrave; bền</p>\n\n<p>Hệ thống khay ngăn l&agrave;m bằng k&iacute;nh chịu lực bền bỉ, cho khả năng chịu lực tốt, kh&ocirc;ng lo bị vỡ nứt trong qu&aacute; tr&igrave;nh bảo quản. Hơn nữa, tủ lạnh c&ograve;n cho ph&eacute;p người d&ugrave;ng linh hoạt thay đổi khay kệ cho ph&ugrave; hợp với nhu cầu bảo quản của c&aacute; nh&acirc;n.</p>\n\n<p><img alt="Tủ Lạnh Mini Electrolux EUM0900SA (90L)" src="https://vcdn.tikicdn.com/ts/tmp/01/e3/27/3edd81a05f28722ef8cc683bf2133995.jpg" style="height:500px; width:750px" /></p>\n\n<p>Hệ thống l&agrave;m lạnh trực tiếp</p>\n\n<p>Tủ lạnh&nbsp;EUM0900SA&nbsp;sử dụng hệ thống l&agrave;m lạnh trực tiếp kh&aacute; phổ biến, gi&uacute;p tủ c&oacute; thể l&agrave;m lạnh nhanh,&nbsp;đồng thời vận h&agrave;nh cực kỳ &ecirc;m &aacute;i do kh&ocirc;ng c&oacute; tiếng ồn của quạt v&agrave; gi&uacute;p bạn&nbsp;tiết kiệm điện. Chiếc tủ lạnh mini n&agrave;y thực tế chỉ ti&ecirc;u thụ xấp xỉ 0.55kW/ng&agrave;y, do đ&oacute; bạn ho&agrave;n to&agrave;n c&oacute; thể y&ecirc;n t&acirc;m sử dụng, kh&ocirc;ng qu&aacute; lo lắng về h&oacute;a đơn tiền điện cuối th&aacute;ng.</p>\n\n<p><img alt="Tủ Lạnh Mini Electrolux EUM0900SA (90L)" src="https://vcdn.tikicdn.com/ts/tmp/e9/22/0f/b41e54a94b11386b33676b4ecf096b17.jpg" style="height:500px; width:750px" /></p>\n\n<p>T&ugrave;y chỉnh nhiệt độ theo nhu cầu</p>\n\n<p>Tủ lạnh Electrolux EUM0900SA&nbsp;trang bị n&uacute;m vặn điều chỉnh nhiệt độ b&ecirc;n trong tủ, gi&uacute;p bạn c&oacute; thể thiết lập nhiệt độ l&agrave;m m&aacute;t th&iacute;ch hợp với khối lượng thực phẩm bảo quản một c&aacute;ch nhanh ch&oacute;ng.&nbsp;Nhờ đ&oacute;, tối ưu hiệu quả l&agrave;m lạnh để thực phẩm được bảo quản trong thời gian l&acirc;u hơn.</p>\n\n<p><img alt="Tủ Lạnh Mini Electrolux EUM0900SA (90L)" src="https://vcdn.tikicdn.com/ts/tmp/82/d3/b3/053009c43333372ce2580c0b4a03bd2b.jpg" style="height:500px; width:750px" /></p>\n\n<p>* Gi&aacute; sản phẩm tr&ecirc;n Tiki đ&atilde; bao gồm thuế theo luật hiện h&agrave;nh. Tuy nhi&ecirc;n tuỳ v&agrave;o từng loại sản phẩm hoặc phương thức, địa chỉ giao h&agrave;ng m&agrave; c&oacute; thể ph&aacute;t sinh th&ecirc;m chi ph&iacute; kh&aacute;c như ph&iacute; vận chuyển, phụ ph&iacute; h&agrave;ng cồng kềnh, ..</p>\n',
    price: 2519000.0,
    original_price: 2519000.0,
    sku: '1889417067979',
    qty: 5,
    rank_total: 0,
    features: [
      {
        global_feature_id: 1,
        value: 'Trung Quốc',
        name: 'Xuất xứ',
      },
      {
        global_feature_id: 2,
        value: 'Electrolux',
        name: 'Thương Hiệu',
      },
      {
        global_feature_id: 6,
        value: '220V/50Hz',
        name: 'Điện Áp',
      },
      {
        global_feature_id: 17,
        value: '',
        name: 'Bảo Hành ',
      },
      {
        global_feature_id: 12,
        value: '',
        name: 'Dung Tích',
      },
      {
        global_feature_id: 13,
        value: '',
        name: 'Loại tủ',
      },
      {
        global_feature_id: 18,
        value: '',
        name: 'Công nghệ',
      },
      {
        global_feature_id: 19,
        value: '',
        name: 'Kích Thước ',
      },
      {
        global_feature_id: 40,
        value: '',
        name: 'màu ',
      },
      {
        global_feature_id: 41,
        value: '',
        name: 'Model',
      },
    ],
    option_groups: [],
    options: [],
    created_at: '2019-10-19T00:00:00.000+00:00',
    sell_count: 4,
  },
  {
    priceId: 33,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 18,
        thumbnail_url:
          '/upload/product/7745ead1-6326-4f58-9949-9e78c2e9868d.JPG',
        url: '/upload/product/7745ead1-6326-4f58-9949-9e78c2e9868d.JPG',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 19,
        thumbnail_url:
          '/upload/product/dcb516a3-2fd6-4ec5-9544-506ae44143e7.JPG',
        url: '/upload/product/dcb516a3-2fd6-4ec5-9544-506ae44143e7.JPG',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 20,
        thumbnail_url:
          '/upload/product/4fd1e323-fac7-4c64-bac5-db7d15fac79a.JPG',
        url: '/upload/product/4fd1e323-fac7-4c64-bac5-db7d15fac79a.JPG',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 8,
    name: 'Camera Hikvision HD720 DS-2CE56C0T-IRP - HÀNG CHÍNH HÃNG',
    category_id: 18,
    store_id: 1,
    short_description:
      'Camera TVI HIKVISION DS-2CE56C0T-IRP dome hồng ngoại 1.0 Megapixel Full HD 720P',
    full_description:
      '<p><strong>Camera TVI HIKVISION DS-2CE56C0T-IRP&nbsp;dome</strong><strong>&nbsp;hồng ngoại 1.0 Megapixel Full HD 720P</strong></p>\n\n<p>&middot; Cảm biến h&igrave;nh ảnh&nbsp;1/3&quot;&nbsp;Progressive&nbsp;Scan&nbsp;CMOS</p>\n\n<p>&middot; Độ ph&acirc;n giải cao&nbsp;<strong>1.0 Megapixel HD 720P</strong>&nbsp;Color cho h&igrave;nh ảnh si&ecirc;u n&eacute;t</p>\n\n<p>. Tốc&nbsp;độ truyền h&igrave;nh&nbsp;&atilde;nh 720P@30fps&nbsp;/ 720P@25fps</p>\n\n<p><a href="https://www.sendo.vn/">&middot;&nbsp;</a>Đ&egrave;n led hồng ngoại tầm nh&igrave;n xa ban&nbsp;đ&ecirc;m l&ecirc;n&nbsp;đến&nbsp;<strong>20m</strong></p>\n\n<p>. Độ nhạy s&aacute;ng<strong>&nbsp;0.1 Lux</strong>@F1.2</p>\n\n<p>&middot; Sử dụng ống k&iacute;nh ti&ecirc;u cự&nbsp;<strong>3.6mm</strong>@F1.8 &nbsp;(t&ugrave;y chọn: 6mm)</p>\n\n<p>&middot; Truyền t&iacute;n hiệu h&igrave;nh ảnh HD khoảng c&aacute;ch xa 300-500M kh&ocirc;ng bị delay giật h&igrave;nh</p>\n\n<p>►&nbsp;<strong>HIKVISION DS-2CE56C0T-IRP</strong><strong>&nbsp;</strong>được sử dụng<strong>&nbsp;c</strong>&ocirc;ng ngệ mới nhất cho một camera cao cấp.</p>\n\n<p>. Giảm nhiễu&nbsp;<strong>DNR</strong>,&nbsp;True Day/Night</p>\n\n<p>. Dải nhi&ecirc;̣t hoạt đ&ocirc;̣ng r&ocirc;̣ng (-20&deg;~45&deg;).&nbsp;</p>\n\n<p>&middot; Nguồn điện cung cấp cho camera 12VDC 1000mA .</p>\n\n<p>.&nbsp;M&agrave;u trắng h&igrave;nh thức trang nh&atilde; dễ lắp đặt v&agrave; điều chỉnh, Chưa c&oacute; adaptor</p>\n\n<p>.&nbsp;<strong>HIKVISION DS-2CE56C0T-IRP</strong><strong>&nbsp;</strong><strong>&nbsp;</strong>c&ocirc;ng nghệ&nbsp;<strong>HDTVI&nbsp;</strong>&nbsp;truyền h&igrave;nh ảnh HD với khoảng c&aacute;ch rất xa 300-500M, dễ d&agrave;ng c&agrave;i đặt kh&ocirc;ng bị giật h&igrave;nh, kh&ocirc;ng bị độ trễ so với camera IP, hơn hẳn c&ocirc;ng nghệ HD-SDI về tốc độ truyền, c&ocirc;ng ngệ HDTVI l&agrave; c&ocirc;ng nghệ analog sau c&ugrave;ng cho h&igrave;nh&nbsp;ảnh sắc n&eacute;t rất trong v&agrave; mịn&nbsp;tốt nhất d&ograve;ng analog.</p>\n\n<p>&middot;&nbsp;<strong>HIKVISION DS-2CE56C0T-IRP</strong><strong>&nbsp;</strong><strong>&nbsp;</strong>camera quan s&aacute;t<strong>&nbsp;</strong>dome&nbsp;hồng ngoại&nbsp;h&igrave;nh ảnh HD 720P si&ecirc;u n&eacute;t,&nbsp;M&agrave;u trắng h&igrave;nh thức trang nh&atilde; dễ lắp đặt v&agrave; điều chỉnh, h&igrave;nh dạng nhỏ gọn&nbsp;th&iacute;ch hợp sử dụng lắp camera cho văn ph&ograve;ng c&ocirc;ng ty,camera cho shop thời trang quần &aacute;o, camera cho qu&aacute;n caf&eacute;&hellip;Sản phẩm chất lượng si&ecirc;u n&eacute;t đảm bảo h&agrave;i l&ograve;ng kh&aacute;ch h&agrave;ng kh&oacute; t&iacute;nh nhất.</p>\n',
    price: 400000.0,
    original_price: 500000.0,
    sku: 'CAM01',
    qty: 100,
    rank_total: 0,
    features: [
      {
        global_feature_id: 1,
        value: 'VietNam',
        name: 'Xuất xứ',
      },
      {
        global_feature_id: 2,
        value: 'HIKVISION ',
        name: 'Thương Hiệu',
      },
    ],
    option_groups: [
      {
        name: '',
        values: [''],
      },
    ],
    options: [],
    created_at: '2019-10-19T00:00:00.000+00:00',
    sell_count: 35,
  },
  {
    priceId: 1752,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 2712,
        thumbnail_url:
          '/upload/product/240a16cd-6a26-45a8-8620-f95450d583e7.jpg',
        url: '/upload/product/240a16cd-6a26-45a8-8620-f95450d583e7.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 2713,
        thumbnail_url:
          '/upload/product/59b82ddf-cc64-4fb5-81e8-c3407ba3b154.jpg',
        url: '/upload/product/59b82ddf-cc64-4fb5-81e8-c3407ba3b154.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 2714,
        thumbnail_url:
          '/upload/product/f7dddd0a-47e0-4512-babf-ef538e515b20.jpg',
        url: '/upload/product/f7dddd0a-47e0-4512-babf-ef538e515b20.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 1491,
    name: '   Bánh Pigeon vị súp lơ 13379 cho bé trên 7 tháng tuổi',
    category_id: 472,
    store_id: 1,
    short_description: 'Bánh Pigeon vị súp lơ 13379 cho bé trên 7 tháng tuổi',
    full_description:
      '<p><strong>Nguy&ecirc;n liệu</strong></p>\n\n<ul>\n\t<li>\n\t<p>B&aacute;nh được l&agrave;m từ: gạo non, ng&ocirc;, bột rau (b&ocirc;ng cải xanh, rau bina), đường, muối, canxi cacbonat, v&agrave; rất nhiều c&aacute;c<strong>&nbsp;chất dinh dưỡng</strong>&nbsp;kh&aacute;c</p>\n\t</li>\n\t<li>\n\t<p>Nguy&ecirc;n liệu tự nhi&ecirc;n v&ocirc; c&ugrave;ng bổ dưỡng v&agrave;&nbsp;<strong>an to&agrave;n tuyệt đối</strong>&nbsp;cho trẻ</p>\n\t</li>\n</ul>\n\n<p><strong>Đặc điểm v&agrave; c&ocirc;ng dụng</strong></p>\n\n<ul>\n\t<li>\n\t<p>Gi&uacute;p cung cấp canxi, c&aacute;c vi chất cần thiết,&nbsp;c&aacute;c loại vitamin v&agrave; chất kho&aacute;ng, c&oacute; vai tr&ograve; rất quan trọng với việc ph&aacute;t triển thể chất của b&eacute;</p>\n\t</li>\n\t<li>\n\t<p>C&aacute;c b&agrave; mẹ n&ecirc;n lựa chọn bột ăn dặm chứa những th&agrave;nh phần dinh dưỡng cần thiết để hỗ trợ sự ph&aacute;t triển n&atilde;o của b&eacute; đặc biệt l&agrave; DHA (Docosahexaenoic Acid), calci, sắt, Iốt, kẽm, c&aacute;c vitamin như A, D, C, acid folic...&nbsp;</p>\n\t</li>\n\t<li>\n\t<p>B&aacute;nh gi&ograve;n, dễ nhai v&agrave; tan nhanh trong nước,&nbsp;b&eacute; c&oacute; thể cầm v&agrave; nhai m&agrave;&nbsp;kh&ocirc;ng phải lo vấn đề nghẹn hay sặc, an to&agrave;n cho b&eacute;</p>\n\t</li>\n\t<li>\n\t<p>Sản phẩm&nbsp;<strong>b&aacute;nh ăn dặm</strong>&nbsp;kh&ocirc;ng sử dụng hương liệu, chất tạo m&agrave;u hay chất bảo quản.</p>\n\t</li>\n\t<li>\n\t<p>H&igrave;nh thức đ&oacute;ng g&oacute;i gi&uacute;p dễ d&agrave;ng bảo quản v&agrave; mang theo khi ra ngo&agrave;i</p>\n\t</li>\n\t<li>\n\t<p>Sản phẩm d&agrave;nh cho b&eacute; từ<strong>&nbsp;7 th&aacute;ng tuổi trở l&ecirc;n</strong>.</p>\n\t</li>\n\t<li>\n\t<p><strong>Lưu &yacute; khi sử dụng</strong></p>\n\t</li>\n\t<li>\n\t<p>Bảo quản: bảo quản ở nhiệt độ thường, tr&aacute;nh &aacute;nh s&aacute;ng trực tiếp, tr&aacute;nh những nơi c&oacute; độ ẩm cao.</p>\n\t</li>\n\t<li>\n\t<p>Ng&agrave;y sản xuất: trước ng&agrave;y hết hạn&nbsp;<strong>18 th&aacute;ng.</strong></p>\n\t</li>\n\t<li>\n\t<p>Khối lượng tịnh:&nbsp;<strong>20 g/hộp (7g x 2 g&oacute;i)</strong>.</p>\n\t</li>\n\t<li>\n\t<p>Nhập khẩu từ h&atilde;ng&nbsp;<strong>Pigeon - Nhật Bản.</strong></p>\n\t</li>\n</ul>\n',
    price: 70000.0,
    original_price: 75000.0,
    sku: 'BAD000025',
    qty: 10,
    rank_total: 0,
    features: [],
    option_groups: [],
    options: [],
    created_at: '2020-05-29T00:00:00.000+00:00',
    sell_count: 5,
  },
  {
    priceId: 50,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 73,
        thumbnail_url:
          '/upload/product/685bbd35-86cb-4595-8bf1-cb73f91a5be4.jpg',
        url: '/upload/product/685bbd35-86cb-4595-8bf1-cb73f91a5be4.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 74,
        thumbnail_url:
          '/upload/product/5fd807af-a1a7-4c7c-b320-7a88e12f800b.jpg',
        url: '/upload/product/5fd807af-a1a7-4c7c-b320-7a88e12f800b.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 75,
        thumbnail_url:
          '/upload/product/3534c853-91cc-4ba2-b04f-d7487e6a939f.jpg',
        url: '/upload/product/3534c853-91cc-4ba2-b04f-d7487e6a939f.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 76,
        thumbnail_url:
          '/upload/product/2180cfa6-5d74-4e93-9361-cd5ad82ee889.jpg',
        url: '/upload/product/2180cfa6-5d74-4e93-9361-cd5ad82ee889.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 24,
    name: 'Máy giặt sấy lồng ngang LG F2514DTGW, 14/8kg, Inverter',
    category_id: 26,
    store_id: 1,
    short_description:
      '- Thiết kế hiện đại, lồng ngang tiện lợi\n- Giặt nhanh và sạch với tính năng Turbowash\n- Giặt tiết kiệm với tính năng Turbowash\n- Công nghệ giặt 6 motions hiện đại\n- Inverter Direct Drive™-Tiết kiệm với động cơ dẫn động trực tiếp biến tần\n- Sử dụng thông minh cùng với NFC\n- Khối lượng giặt 14kg và sấy khô 8kg tiện dụng',
    full_description:
      '<p><strong>Thiết kế hiện đại, lồng ngang tiện lợi</strong><br />\nM&aacute;y giặt sấy lồng ngang LG F2514DTGW, 14/8kg, Inverter&nbsp;c&oacute; thiết kế hiện đại mang đến vẻ sang trọng cho kh&ocirc;ng gian của bạn khi sử dụng. M&aacute;y c&oacute; kiểu d&aacute;ng lồng ngang tiện lợi hơn cho bạn khi cho quần &aacute;o v&agrave;o giặt v&agrave; lấy ra phơi.&nbsp;M&aacute;y giặt sấy&nbsp;lồng ngang F2514DTGW<strong>&nbsp;</strong>c&oacute; lồng giặt được l&agrave;m bằng chất liệu th&eacute;p kh&ocirc;ng gỉ bền tốt, được&nbsp;trang bị&nbsp;bảng điều khiển với n&uacute;t dễ d&agrave;ng điều chỉnh chương tr&igrave;nh giặt v&agrave; c&oacute; m&agrave;n h&igrave;nh hiển thị gi&uacute;p người d&ugrave;ng chọn ch&iacute;nh x&aacute;c hơn. Khi mua chiếc m&aacute;y giặt sấy&nbsp;lồng ngang LG F2514DTGW, 14/8kg, bạn kh&ocirc;ng chỉ c&oacute; một chiếc m&aacute;y giặt tốt m&agrave; c&ograve;n c&oacute; th&ecirc;m một vật dụng gi&uacute;p tăng th&ecirc;m n&eacute;t hiện đại cho ng&ocirc;i nh&agrave; của m&igrave;nh.&nbsp;</p>\n\n<p><img src="https://cdn02.static-adayroi.com/0/2016/08/03/147021506552_7903060.jpg" /><br />\n<em>Thiết kế hiện đại, lồng ngang tiện lợi</em></p>\n\n<p><strong>Giặt nhanh v&agrave; sạch với t&iacute;nh năng Turbowash</strong><br />\nM&aacute;y giặt sấy&nbsp;lồng ngang LG&nbsp;F2514DTGW được trang bị t&iacute;nh năng Turbowash kết hợp c&ugrave;ng đầu giũ phun Jet spray gi&uacute;p giảm đ&aacute;ng kể thời gian giặt v&agrave; mang tới khả năng tiết kiệm năng lượng nhưng vẫn đảm bảo &aacute;o quần của bạn&nbsp;lu&ocirc;n sạch sẽ. Nhờ việc phun nước trực tiếp l&ecirc;n &aacute;o quần trong khoảng 120 gi&acirc;y, đầu giũ phun Jet spray n&acirc;ng cao hơn hiệu năng giặt &amp; giũ vốn đ&atilde; hiệu quả nhờ động cơ dẫn động trực tiếp &amp; 6 chuyển động giặt. Từ đ&oacute;, cho ph&eacute;p bạn tua nhanh thời gian giặt giũ h&agrave;ng ng&agrave;y.&nbsp;</p>\n\n<p><img src="https://cdn02.static-adayroi.com/0/2016/05/28/1464407157269_2060750.jpg" /><br />\n<em>Giặt nhanh v&agrave; sạch với t&iacute;nh năng Turbowash</em></p>\n\n<p><strong>Giặt tiết kiệm với t&iacute;nh năng Turbowash</strong><br />\nT&iacute;nh năng Turbowash kh&ocirc;ng chỉ cho ph&eacute;p bạn tua nhanh thời gian giặt, m&agrave; c&ograve;n mang tới sự tiết kiệm hiệu quả về năng lượng ti&ecirc;u thụ v&agrave; giữ nguy&ecirc;n hiệu năng giặt. M&aacute;y giặt&nbsp;LG&nbsp;F2514DTGW&nbsp;Turbowash giảm tới 15% điện năng v&agrave; 40% lượng nước sử dụng trong mỗi chu tr&igrave;nh. Nhanh hơn, tiết kiệm hơn m&agrave; vẫn sạch đồng đều.&nbsp;</p>\n\n<p><img alt="Giặt tiết kiệm với tính năng Turbowash" src="https://cdn02.static-adayroi.com/0/2016/05/28/1464407164486_4935535.jpg" /><br />\n<em>Giặt tiết kiệm với t&iacute;nh năng Turbowash</em></p>\n\n<p><strong>C&ocirc;ng nghệ giặt 6 motions hiện đại</strong><br />\nĐộng cơ dẫn động trực tiếp &amp; 6 chuyển động giặt mang tới c&aacute;c bước giặt tối ưu, nhẹ nh&agrave;ng chăm s&oacute;c sợi vải.&nbsp;C&ocirc;ng nghệ giặt&nbsp;6 motions&nbsp;hiện đại&nbsp;mới nhất của LG cho ph&eacute;p m&aacute;y giặt chăm s&oacute;c quần &aacute;o như đ&ocirc;i tay của bạn với 6 bước giặt: Ch&agrave; x&aacute;t - Quay - Nh&agrave;o trộn - Đảo - N&eacute;n - Đập. 6 motions l&agrave; sự cải tiến trong chu tr&igrave;nh giặt, m&ocirc; phỏng c&aacute;c bước giặt tay của con người (đập, v&ograve;,...).&nbsp;Nhờ sử dụng động cơ dẫn động trực tiếp thay thế d&acirc;y cua-roa n&ecirc;n m&aacute;y giặt hoạt động hiệu quả hơn, kh&ocirc;ng g&acirc;y tiếng ồn, giảm sự rung lắc&nbsp;v&igrave; vậy m&aacute;y giặt khi hoạt động sẽ&nbsp;&ecirc;m &aacute;i, bền bỉ hơn.&nbsp;6 chuyển động giặt bắt nguồn từ động cơ dẫn động trực tiếp Inverter l&agrave; sự kết hợp của c&aacute;c chuyển động giặt đa dạng th&iacute;ch hợp với từng loại vải. Nhẹ nh&agrave;ng chăm s&oacute;c &aacute;o quần như khi giặt tay m&agrave; vẫn đem lại hiệu quả giặt tối ưu.</p>\n\n<p><img alt="Công nghệ giặt 6 motions hiện đại" src="https://cdn02.static-adayroi.com/0/2016/05/28/146440718211_9662583.jpg" /><br />\n<em>C&ocirc;ng nghệ giặt 6 motions hiện đại</em></p>\n\n<p><strong>Chu tr&igrave;nh Cotton/ Sợi tinh xảo/ Vết bẩn cứng đầu</strong><br />\nChu tr&igrave;nh COTTON N&eacute;n - Quay - Nh&agrave;o trộn - Đập. Chu tr&igrave;nh Sợi tinh xảo Nh&agrave;o trộn - Đảo - Đảo - Đập. Chu tr&igrave;nh Vết bẩn kh&oacute; giặt N&eacute;n - Nh&agrave;o trộn - Đảo.</p>\n\n<p><img alt="Chu trình Cotton/ Sợi tinh xảo/ Vết bẩn cứng đầu" src="https://cdn02.static-adayroi.com/0/2016/05/28/1464403923878_7308572.jpg" /><br />\n<em>Chu tr&igrave;nh Cotton/ Sợi tinh xảo/ Vết bẩn cứng đầu</em></p>\n\n<p><strong>Inverter Direct Drive&trade;-Tiết kiệm với động cơ dẫn động trực tiếp biến tần</strong><br />\nM&aacute;y giặt sấy lồng ngang LG F2514DTGW, 14/8kg, Inverter&nbsp;với động cơ dẫn động trực tiếp hiện đại mạnh mẽ . C&ocirc;ng nghệ dẫn động trực tiếp cho m&aacute;y giặt hoạt động &ecirc;m &aacute;i, tiết kiệm điện v&agrave; bền bỉ đến kh&oacute; tin, LG đảm bảo kh&ocirc;ng l&agrave;m cho bạn thất vọng.&nbsp;V&agrave; thực tế l&agrave;, ch&uacute;ng t&ocirc;i tự tin bảo h&agrave;nh tới 10 năm cho động cơ v&agrave; tất cả c&aacute;c bộ phận của n&oacute;.&nbsp;M&aacute;y giặt với thiết kế m&agrave;u trắng sang trọng, hệ thống chức năng giặt th&ocirc;ng minh c&ugrave;ng những c&ocirc;ng nghệ ti&ecirc;n tiến sẽ gi&uacute;p bạn tận hưởng một cuộc sống tốt hơn với mức chi ph&iacute; tối ưu. M&aacute;y giặt được trang bị động cơ dẫn động trực tiếp gi&uacute;p tiết kiệm điện v&agrave;&nbsp;nước, tăng độ bền của động cơ v&agrave; giảm chi ph&iacute; bảo tr&igrave;. Ngo&agrave;i ra c&ograve;n giảm thiểu độ rung v&agrave; tiếng ồn do &iacute;t bộ phận chuyển động hơn, cho bạn một qu&aacute; tr&igrave;nh giặt giũ &ecirc;m &aacute;i thoải m&aacute;i.&nbsp;</p>\n\n<p><img alt="Inverter Direct Drive™-Tiết kiệm với động cơ dẫn động trực tiếp biến tần" src="https://cdn02.static-adayroi.com/0/2016/05/28/1464403908404_3415087.jpg" /><br />\n<em>Inverter Direct Drive&trade;-Tiết kiệm với động cơ dẫn động trực tiếp biến tần</em></p>\n\n<p><strong>Sử dụng&nbsp;th&ocirc;ng minh c&ugrave;ng&nbsp;với&nbsp;NFC</strong><br />\nM&aacute;y giặt&nbsp;LG&nbsp;F2514DTGW&nbsp;sử dụng c&ocirc;ng nghệ chạm&nbsp;NFC, người sử dụng c&oacute; thể tải về nhiều chương tr&igrave;nh giặt mới như:&nbsp;đồ len, đồ trẻ sơ sinh, giặt nước lạnh v.v&hellip;Sau đ&oacute;, c&agrave;i đặt c&aacute;c chu tr&igrave;nh vừa tải về đơn giản chỉ bằng c&aacute;ch chạm nhẹ Smartphone v&agrave;o biểu tượng Tag on tr&ecirc;n m&aacute;y giặt.&nbsp;Hơn thế nữa, c&ocirc;ng nghệ NFC c&ograve;n được &aacute;p dụng v&agrave;o t&iacute;nh năng chẩn đo&aacute;n th&ocirc;ng minh, gi&uacute;p kh&aacute;ch h&agrave;ng chẩn đo&aacute;n lỗi nhanh ch&oacute;ng v&agrave;&nbsp;hiệu quả hơn nữa,&nbsp;gi&uacute;p bạn nhanh ch&oacute;ng tự sửa được m&aacute;y m&agrave; kh&ocirc;ng cần phải đem m&aacute;y đi hoặc gọi nh&acirc;n vi&ecirc;n đến bảo h&agrave;nh rất tiện lợi.</p>\n\n<p><img alt="Sử dụng thông minh cùng với NFC" src="https://cdn02.static-adayroi.com/0/2016/05/28/1464407727396_6782395.jpg" /><br />\n<em>Sử dụng th&ocirc;ng minh c&ugrave;ng với NFC</em></p>\n\n<p><strong>Khối lượng giặt 14kg v&agrave; sấy kh&ocirc; 8kg tiện dụng</strong><br />\nM&aacute;y giặt sấy lồng ngang LG F2514DTGW&nbsp;c&oacute; khối lượng giặt giũ l&ecirc;n đến 14kg, rất&nbsp;ph&ugrave; hợp với những gia đ&igrave;nh đ&ocirc;ng&nbsp;th&agrave;nh vi&ecirc;n, gi&uacute;p người nội trợ tiết kiệm thời gian khi c&oacute; thể giặt sạch quần &aacute;o của cả gia đ&igrave;nh chỉ với một mẻ giặt. Khối lượng giặt lớn cũng ph&ugrave; hợp với những ai bận rộn, &iacute;t c&oacute; thời gian giặt giũ mỗi ng&agrave;y, gi&uacute;p bạn giặt nhanh v&agrave; sạch lượng quần &aacute;o trong nhiều ng&agrave;y m&agrave; kh&ocirc;ng tốn nhiều c&ocirc;ng sức.&nbsp;Ngo&agrave;i&nbsp;t&iacute;nh năng giặt quần &aacute;o th&igrave; m&aacute;y giặt LG c&ograve;n t&iacute;ch hợp th&ecirc;m c&ocirc;ng nghệ sấy với khối lượng l&ecirc;n đến 8 kg cho ph&eacute;p bạn l&agrave;m kh&ocirc; quần &aacute;o chỉ trong thời gian ngắn l&agrave; c&oacute; thể sử dụng được ngay.&nbsp;V&igrave; thế, m&aacute;y giặt LG &nbsp;F2514DTGW&nbsp;l&agrave; lựa chọn th&iacute;ch hợp cho cả nh&agrave;, nhanh ch&oacute;ng giặt sạch c&aacute;c loại quần &aacute;o bẩn của c&aacute;c th&agrave;nh vi&ecirc;n trong gia đ&igrave;nh.&nbsp;</p>\n\n<p><img alt="Khối lượng giặt 14kg và sấy khô 8kg tiện dụng" src="https://cdn02.static-adayroi.com/0/2016/08/03/1470215604579_3145225.jpg" /><br />\n<em>Khối lượng giặt 14kg v&agrave; sấy kh&ocirc; 8kg tiện dụng</em></p>\n\n<p><strong>Tốc độ quay vắt mạnh mẽ 1200 v&ograve;ng/ph&uacute;t, r&uacute;t ngắn thời gian phơi quần &aacute;o cho gia đ&igrave;nh bạn</strong><br />\nVới tốc độ quay vắt l&ecirc;n đến 1200 v&ograve;ng/ph&uacute;t, sản phẩm&nbsp;sẽ gi&uacute;p r&uacute;t ngắn thời gian phơi quần &aacute;o hiệu quả cho gia đ&igrave;nh bạn. Điều n&agrave;y cũng gi&uacute;p gia đ&igrave;nh bạn tiết kiệm kh&ocirc;ng &iacute;t thời gian v&agrave; điện năng cho m&igrave;nh.</p>\n\n<p><img alt="Tốc độ quay vắt mạnh mẽ 1200 vòng/phút, rút ngắn thời gian phơi quần áo cho gia đình bạn" src="https://cdn02.static-adayroi.com/0/2016/08/03/1470215597318_2414823.jpg" /><br />\n<em>Tốc độ quay vắt mạnh mẽ 1200 v&ograve;ng/ph&uacute;t, r&uacute;t ngắn thời gian phơi quần &aacute;o cho gia đ&igrave;nh bạn</em></p>\n',
    price: 2.0009e7,
    original_price: 2.0009e7,
    sku: 'F2514DTGW',
    qty: 7,
    rank_total: 0,
    features: [
      {
        global_feature_id: 1,
        value: 'Hàn Quốc',
        name: 'Xuất xứ',
      },
      {
        global_feature_id: 2,
        value: 'LG',
        name: 'Thương Hiệu',
      },
      {
        global_feature_id: 3,
        value: 'F2514DTGW',
        name: 'Model',
      },
      {
        global_feature_id: 4,
        value: 'Trắng',
        name: 'Màu sắc',
      },
      {
        global_feature_id: 6,
        value: '220V/5Hz',
        name: 'Điện Áp',
      },
      {
        global_feature_id: 7,
        value: 'Cửa Trước - Lồng Ngang',
        name: 'Loại Máy',
      },
      {
        global_feature_id: 8,
        value: '14kg',
        name: 'Khối lượng giặt',
      },
      {
        global_feature_id: 9,
        value: '8kg',
        name: 'Khối lượng sấy',
      },
      {
        global_feature_id: 10,
        value: '',
        name: 'Công nghệ Inverter',
      },
      {
        global_feature_id: 11,
        value: '1200 v/p',
        name: 'Tốc độ quay vắt',
      },
    ],
    option_groups: [],
    options: [],
    created_at: '2019-10-21T00:00:00.000+00:00',
    sell_count: 7,
  },
  {
    priceId: 39,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 31,
        thumbnail_url:
          '/upload/product/6cf9dbbe-5613-4f6a-852c-3364ec8120b3.jpg',
        url: '/upload/product/6cf9dbbe-5613-4f6a-852c-3364ec8120b3.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 32,
        thumbnail_url:
          '/upload/product/f90eee67-74fa-4a1d-8024-97c3ce5b0681.jpg',
        url: '/upload/product/f90eee67-74fa-4a1d-8024-97c3ce5b0681.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 33,
        thumbnail_url:
          '/upload/product/7ac86dd9-9649-4cd9-8418-edf6823a9f5d.jpg',
        url: '/upload/product/7ac86dd9-9649-4cd9-8418-edf6823a9f5d.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 34,
        thumbnail_url:
          '/upload/product/b64d7e86-e89e-43c3-807c-a4177ec0e48c.jpg',
        url: '/upload/product/b64d7e86-e89e-43c3-807c-a4177ec0e48c.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 14,
    name: 'MÁY GIẶT ELECTROLUX 8KG EWW12853',
    category_id: 26,
    store_id: 1,
    short_description:
      'Khối lượng giặt: 8 kg - Sấy: 5 kg\nSấy: Thông hơi\nĐộng cơ Eco-Inverter\nChức năng giặt hơi nước\nXuất xứ: Thái Lan\nBảo hành: 24 tháng (10 năm bảo hành cho motor máy và lồng giặt)',
    full_description:
      '<p>L&agrave;m mới quần &aacute;o Vapour Refresh</p>\n\n<p>M&aacute;y giặt Electrolux 8kg EWW12853 gi&uacute;p l&agrave;m mới v&agrave; gi&uacute;p quần &aacute;o đỡ nhăn hơn trong v&ograve;ng 30 ph&uacute;t, mang đến sự mềm mại v&agrave; thơm m&aacute;t cho quần &aacute;o cất l&acirc;u ng&agrave;y trong tủ.</p>\n\n<p><img alt="Máy giặt Electrolux 8kg EWW12853 giảm nhăn cho quần áo hiệu quả" src="https://cdn.nguyenkimmall.com/images/companies/_1/Content/dien-lanh/may-giat/may-giat-electrolux-8kg-eww12853-mt1.jpg" /></p>\n\n<p><img alt="Máy giặt Electrolux 8kg EWW12853 cho phép bỏ thêm quần áo" src="https://cdn.nguyenkimmall.com/images/companies/_1/Content/dien-lanh/may-giat/may-giat-electrolux-8kg-eww12853-mt2.jpg" /></p>\n\n<p>Cho ph&eacute;p bỏ th&ecirc;m quần &aacute;o</p>\n\n<p>M&aacute;y giặt Electrolux 8kg EWW12853 cho ph&eacute;p bạn th&ecirc;m quần &aacute;o ngay trong chu tr&igrave;nh giặt, từ 10-15 ph&uacute;t sau khi chu kỳ giặt bắt đầu, tuỳ theo từng chương tr&igrave;nh giặt.</p>\n\n<p>An to&agrave;n với vải len&nbsp;</p>\n\n<p>N&acirc;ng niu chiếc &aacute;o len bạn y&ecirc;u th&iacute;ch với M&aacute;y giặt Electrolux&nbsp;8kg EWW12853 với c&ocirc;ng nghệ giặt ph&ugrave; hợp v&agrave; an to&agrave;n với vải len, được chứng nhận bởi Woolmark (Chứng nhận ti&ecirc;u chuẩn đồ len quốc tế)</p>\n\n<p><img alt="Máy giặt Electrolux 8kg EWW12853 an toàn với vải len" src="https://cdn.nguyenkimmall.com/images/companies/_1/Content/dien-lanh/may-giat/may-giat-electrolux-8kg-eww12853-mt3.jpg" /></p>\n\n<p><img alt="Máy giặt Electrolux 8kg EWW12853 công nghệ Eco Inverter" src="https://cdn.nguyenkimmall.com/images/companies/_1/Content/dien-lanh/may-giat/may-giat-electrolux-8kg-eww12853-mt4.jpg" /></p>\n\n<p>C&ocirc;ng nghệ Eco Inverter</p>\n\n<p>M&aacute;y giặt Electrolux 8kg EWW12853 gi&uacute;p tối ưu h&oacute;a c&ocirc;ng suất v&agrave; hiệu quả giặt sạch, đồng thời giảm thiểu rung chấn v&agrave; sờn r&aacute;ch. Đặc biệt với c&ocirc;ng nghệ Ecoinverter gi&uacute;p giảm lượng điện năng ti&ecirc;u thụ tới 75%.</p>\n',
    price: 1.29e7,
    original_price: 1.909e7,
    sku: '9505463013884',
    qty: 5,
    rank_total: 0,
    features: [
      {
        global_feature_id: 1,
        value: 'Thái Lan',
        name: 'Xuất xứ',
      },
      {
        global_feature_id: 2,
        value: 'Electrolux',
        name: 'Thương Hiệu',
      },
      {
        global_feature_id: 3,
        value: 'EWW12853',
        name: 'Model',
      },
      {
        global_feature_id: 4,
        value: 'Trắng',
        name: 'Màu sắc',
      },
      {
        global_feature_id: 6,
        value: '220V/50Hz',
        name: 'Điện Áp',
      },
      {
        global_feature_id: 7,
        value: 'Cửa trước - Lồng Ngang',
        name: 'Loại Máy',
      },
      {
        global_feature_id: 8,
        value: '8kg',
        name: 'Khối lượng giặt',
      },
      {
        global_feature_id: 9,
        value: '5kg',
        name: 'Khối lượng sấy',
      },
      {
        global_feature_id: 10,
        value: 'có',
        name: 'Công nghệ Inverter',
      },
      {
        global_feature_id: 11,
        value: '1200 vòng/phút',
        name: 'Tốc độ quay vắt',
      },
    ],
    option_groups: [],
    options: [],
    created_at: '2019-10-19T00:00:00.000+00:00',
    sell_count: 3,
  },
  {
    priceId: 2398,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 4264,
        thumbnail_url:
          '/upload/product/c135b82a-7750-4a2e-9b58-7cdfd774ce52.jpg',
        url: '/upload/product/c135b82a-7750-4a2e-9b58-7cdfd774ce52.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 1917,
    name: 'Lan Ý',
    category_id: 778,
    store_id: 1,
    short_description:
      'Lá xanh mướt điểm xuyết bằng những bông hoa trắng tinh khôi – Đó là Lan Ý. Đây là 1 trong 10 loại cây có thể loại bỏ được độc tố giúp thanh lọc không khí cho gia chủ. Vì vậy, Lan Ý được rất nhiều người yêu cây cảnh lựa chọn.\n\nGiá đã bao gồm chậu.\n\nGiá chưa bao gồm VAT',
    full_description:
      '<p>M&ocirc; tả</p>\n\n<p>Nguồn gốc v&agrave; t&ecirc;n gọi</p>\n\n<p>C&acirc;y Lan &Yacute; c&oacute; t&ecirc;n tiếng Anh l&agrave; Peace Lily, White Sails plant, Spathe flower. T&ecirc;n c&acirc;y trong từ điển khoa học l&agrave; Spathiphyllum Wallisii, thuộc họ Araceae (R&aacute;y). Lan &Yacute; xuất xứ từ v&ugrave;ng nhiệt đới Nam Mỹ, v&agrave; một số quốc gia Đ&ocirc;ng Nam &Aacute;. Ở Việt Nam, ngo&agrave;i c&aacute;i t&ecirc;n Lan &Yacute;, c&acirc;y thường được gọi l&agrave; Bạch M&ocirc;n, Vỹ Hoa Trắng hay Huệ H&ograve;a B&igrave;nh.</p>\n\n<p>Đặc điểm của c&acirc;y Lan &Yacute;</p>\n\n<p>C&acirc;y mọc th&agrave;nh bụi, cao từ 40cm đến 1m. Cuống l&aacute; mọc từ gốc, nhỏ v&agrave; mảnh nhưng vươn cao, m&agrave;u xanh đậm. L&aacute; c&acirc;y h&igrave;nh bầu dục, thu&ocirc;n nhọn ở đầu như mũi m&aacute;c, bề mặt l&aacute; hơi nổi g&acirc;n. Lan &Yacute; c&oacute; l&aacute; m&agrave;u xanh đậm tiệp m&agrave;u th&acirc;n nhưng b&oacute;ng mượt.</p>\n\n<p>Cuống hoa Lan &Yacute; kh&aacute; d&agrave;i, c&oacute; thể m&agrave;u xanh hoặc trắng xanh, nhỏ mảnh như th&acirc;n, đầu cuống chứa một hoa tự thu&ocirc;n d&agrave;i m&agrave;u trắng. Bao bọc b&ecirc;n ngo&agrave;i hoa tự l&agrave; l&aacute; bắc của hoa (hay c&ograve;n gọi l&agrave; mo hoa), &ocirc;m v&agrave;o hoa như vỏ s&ograve;, m&agrave;u trắng hoặc trắng xanh.</p>\n\n<p>Lan &Yacute; c&oacute; đặc t&iacute;nh nổi bật l&agrave; sinh trưởng mạnh mẽ, lan bụi nhanh, c&oacute; thể nh&acirc;n giống bằng c&aacute;ch t&aacute;ch bụi. Hoa Lan &Yacute; thuộc loại nở l&acirc;u t&agrave;n (tầm 3-4 th&aacute;ng). C&acirc;y sống được ở nhiều m&ocirc;i trường, &aacute;nh s&aacute;ng hoặc b&oacute;ng r&acirc;m đều được, trồng đất hay trồng nước cũng sinh trưởng tốt.</p>\n\n<p>C&ocirc;ng dụng của c&acirc;y Lan &Yacute;</p>\n\n<p>Nhắc đến c&ocirc;ng dụng của c&acirc;y Lan &Yacute;, đầu ti&ecirc;n phải n&oacute;i ngay l&agrave; khả năng lọc kh&ocirc;ng kh&iacute; cực tốt. C&acirc;y được NASA khuyến c&aacute;o n&ecirc;n trồng trong nh&agrave; để hấp thụ bớt những kh&iacute; độc hại như benzen, formaldehyde, trichloroethylene, xylene v&agrave; toluene.</p>\n\n<p>Một số nghi&ecirc;n cứu c&ograve;n chỉ ra rằng Lan &Yacute; c&oacute; thể lọc bớt s&oacute;ng điện từ ph&aacute;t ra từ wifi, ti vi, m&aacute;y t&iacute;nh, laptop, đồ điện tử,&hellip;hay tia tử ngoại, hồng ngoại. Ch&iacute;nh nhờ giảm t&aacute;c hại của bức xạ n&ecirc;n những người điều trị ung thư bằng xạ trị, h&oacute;a trị thường đặt Lan &Yacute; trong ph&ograve;ng. C&aacute;c bệnh viện cũng hay trồng c&acirc;y n&agrave;y để mang lại kh&ocirc;ng gian trong l&agrave;nh, khỏe mạnh hơn cho c&aacute;c bệnh nh&acirc;n.</p>\n\n<p>Lưu &yacute; khi lọc kh&oacute;i bụi như vậy, Lan &Yacute; hay b&aacute;m bẩn tr&ecirc;n l&aacute;. Người trồng cần d&ugrave;ng khăn ướt lau sạch bề mặt l&aacute; để chừa lỗ khổng cho c&acirc;y thở, gi&uacute;p c&acirc;y sinh trưởng tốt hơn v&agrave; tiếp tục h&uacute;t bụi hiệu quả.</p>\n\n<p>Ngo&agrave;i ra, với vẻ đẹp tinh tế, thanh lịch của m&igrave;nh, Lan &Yacute; hay được chọn trồng trang tr&iacute; ở nhiều kh&ocirc;ng gian như văn ph&ograve;ng l&agrave;m việc, kh&aacute;ch sạn, nh&agrave; ở, &hellip; C&acirc;y cũng l&agrave; m&oacute;n qu&agrave; tặng đặc biệt &yacute; nghĩa cho những dịp như t&acirc;n gia, khai trương, thăm đau ốm, &hellip;</p>\n\n<p>&Yacute; nghĩa phong thủy của c&acirc;y Lan &Yacute;</p>\n\n<p>Trong quan niệm phong thủy, c&acirc;y Lan &Yacute; giữ một vị tr&iacute; quan trọng. C&acirc;y l&agrave; biểu tượng của sự b&igrave;nh y&ecirc;n, gi&uacute;p con người tr&aacute;nh khỏi điều xui xẻo, mang đến nguồn năng lượng t&iacute;ch cực l&agrave; động lực để vượt qua ốm đau, bệnh tật. Đồng thời, Lan &Yacute; cũng gắn kết c&aacute;c th&agrave;nh vi&ecirc;n trong gia đ&igrave;nh, tổ chức, giải quyết m&acirc;u thuẫn v&agrave; tạo kh&ocirc;ng kh&iacute; h&ograve;a hợp. Người ta hay gọi c&acirc;y l&agrave; Huệ &ldquo;H&ograve;a B&igrave;nh&rdquo; cũng l&agrave; v&igrave; vậy.</p>\n\n<p>Ngo&agrave;i ra, lo&agrave;i c&acirc;y n&agrave;y lại mang đến may mắn v&agrave; tiền t&agrave;i cho người trồng. Lan &Yacute; trồng trong nh&agrave; lu&ocirc;n l&agrave; &ldquo;thần hộ mệnh&rdquo; cho t&igrave;nh y&ecirc;u v&agrave; cuộc sống của gia chủ. C&acirc;y vươn thẳng c&ograve;n gợi n&ecirc;n &yacute; ch&iacute; ki&ecirc;n cường, phấn đấu trước mọi kh&oacute; khăn thử th&aacute;ch của con người.</p>\n\n<p>C&acirc;y Lan &Yacute; hợp mệnh g&igrave;?</p>\n\n<p>C&acirc;y Lan &Yacute; hợp mệnh g&igrave;? C&acirc;u trả lời ch&iacute;nh x&aacute;c l&agrave; mệnh Kim v&agrave; Thủy. Bởi c&acirc;y c&oacute; hoa trắng muốt, lại thường được trồng thủy sinh. M&agrave; phong thủy Ngũ h&agrave;nh quy định m&agrave;u trắng l&agrave; m&agrave;u bản mệnh của mệnh Kim v&agrave; m&agrave;u tương sinh của mệnh Thủy. Do đ&oacute;, những người thuộc hai mệnh n&agrave;y rất th&iacute;ch hợp trồng c&acirc;y Lan &Yacute;.</p>\n\n<p>C&acirc;y Lan &Yacute; hợp tuổi g&igrave;?</p>\n\n<p>Nếu c&acirc;y Lan &Yacute; đ&atilde; hợp mệnh Kim v&agrave; Thủy, th&igrave; từ mệnh ch&uacute;ng ta c&oacute; thể suy ra tuổi hợp của c&acirc;y. C&acirc;y Lan &Yacute; hợp tuổi g&igrave;, đ&oacute; l&agrave; những người thuộc tuổi: B&iacute;nh T&yacute; (1936 v&agrave; 1996), Qu&yacute; Tỵ (1953 v&agrave; 2013), Nh&acirc;m Tuất (1982), Đinh Sửu (1937 v&agrave; 1997), Qu&yacute; Hợi (1983), Gi&aacute;p Th&acirc;n (1944 &ndash; 2004), Đinh M&ugrave;i (1967), Ất Dậu (1945 v&agrave; 2005), Nh&acirc;m Th&igrave;n (1952 v&agrave; 2012), Ất M&atilde;o (1975).</p>\n\n<p>Hoặc cũng c&oacute; thể l&agrave; sinh nhằm c&aacute;c năm: Canh Th&igrave;n (2000), T&acirc;n Tỵ (2001), Qu&yacute; Dậu (1993), Nh&acirc;m Th&acirc;n (1992), Gi&aacute;p T&yacute; (1984), Ất Sửu (1985), Canh Tuất (1970), T&acirc;n Hợi (1971), Qu&yacute; M&atilde;o (1963), Nh&acirc;m Dần (1962), Ất M&ugrave;i (1955, 2015), Gi&aacute;p Ngọ (1954, 2014).</p>\n\n<p>Hướng dẫn c&aacute;ch chăm s&oacute;c c&acirc;y Lan &Yacute; trồng đất</p>\n\n<p>Chọn đất trồng</p>\n\n<p>Đảm bảo đất trồng c&acirc;y Lan &Yacute; phải gi&agrave;u chất dinh dưỡng, m&agrave;u mỡ v&agrave; tho&aacute;t nước tốt. C&oacute; thể tạo hỗn hợp bao gồm đất c&aacute;t hoặc đất thịt nhẹ + l&aacute; mục (xơ dừa) + than b&ugrave;n + ph&acirc;n hữu cơ. Ngo&agrave;i ch&uacute; &yacute; đất trồng, c&ograve;n phải chọn chậu ph&ugrave; hợp, vừa vặn v&agrave; c&oacute; lỗ tho&aacute;t nước.</p>\n\n<p>Cung cấp nước</p>\n\n<p>C&acirc;y Lan &Yacute; trồng đất kh&ocirc;ng chịu được ẩm. Do đ&oacute; bạn chỉ cần tưới lượng nước vừa phải, v&agrave; n&ecirc;n tưới nước sạch. Chu kỳ l&agrave; 1 tuần tưới 1 lần, trời lạnh hoặc c&oacute; mưa th&igrave; giảm lượng nước tưới cũng như k&eacute;o d&agrave;i thời gian tưới lại.</p>\n\n<p>Bổ sung dinh dưỡng</p>\n\n<p>C&oacute; hai loại &ldquo;dinh dưỡng&rdquo; m&agrave; c&acirc;y Lan &Yacute; cần. Đầu ti&ecirc;n l&agrave; &aacute;nh s&aacute;ng. C&acirc;y sống tốt trong điều kiện b&oacute;ng r&acirc;m, m&ocirc;i trường thiếu s&aacute;ng hoặc &aacute;nh s&aacute;ng gi&aacute;n tiếp cũng như &aacute;nh đ&egrave;n huỳnh quang. Tuy thế, c&acirc;y cũng cần &aacute;nh s&aacute;ng tự nhi&ecirc;n để hoa nở đẹp hơn. Đ&oacute; l&agrave; l&yacute; do m&agrave; mỗi tuần khoảng 2-3 lần, ta n&ecirc;n mang Lan &Yacute; ra phơi nắng nhẹ trước 10h s&aacute;ng.</p>\n\n<p>Thứ hai, c&acirc;y cần ph&acirc;n b&oacute;n. Nếu c&acirc;y c&ograve;i cọc, chậm ph&aacute;t triển th&igrave; n&ecirc;n b&oacute;n ph&acirc;n đạm, ph&acirc;n l&acirc;n bằng c&aacute;ch h&ograve;a lo&atilde;ng c&ugrave;ng nước tạo dung dịch tưới. Trường hợp c&acirc;y &iacute;t ra hoa c&oacute; thể d&ugrave;ng B1 để k&iacute;ch hoa nở, rễ &uacute;ng th&igrave; cắt bỏ rễ, d&ugrave;ng thuốc k&iacute;ch ra rễ.</p>\n\n<p>Kỹ thuật trồng c&acirc;y Lan &Yacute; thủy sinh</p>\n\n<p>C&aacute;ch trồng c&acirc;y Lan &Yacute; thủy sinh theo quy tr&igrave;nh như sau:</p>\n\n<p>Bước 1: Nhẹ nh&agrave;ng t&aacute;ch c&acirc;y Lan &Yacute; ra khỏi chậu đất trồng cũ, ng&acirc;m bầu rễ v&agrave;o chậu/x&ocirc; nước sạch khoảng 2-3 ng&agrave;y cho trắng rễ.</p>\n\n<p>Bước 2: Sau khi ng&acirc;m rễ, bạn h&atilde;y d&ugrave;ng nh&iacute;p hoặc dao/k&eacute;o loại bỏ hết bụi bẩn v&agrave; đất trong c&aacute;c củ rễ. C&ugrave;ng với đ&oacute; l&agrave; cắt tỉa những rễ hư, rễ d&agrave;i loằng ngoằng.</p>\n\n<p>Bước 3: Pha một nắp chai dung dịch dinh dưỡng k&egrave;m nước sạch cho v&agrave;o chậu trồng, nhẹ nh&agrave;ng đặt c&acirc;y v&agrave;o sao cho nước ngập vừa đủ bầu rễ. Nước d&ugrave;ng để trồng c&acirc;y Lan &Yacute; thủy sinh đảm bảo kh&ocirc;ng mặn, kh&ocirc;ng ph&egrave;n, kh&ocirc;ng chứa clo hay axit.</p>\n\n<p>C&aacute;ch chăm s&oacute;c c&acirc;y Lan &Yacute; thủy sinh kh&aacute; đơn giản, chỉ cần thay nước định kỳ một tuần một lần v&agrave; bổ sung dung dịch dinh dưỡng 2 tuần/lần. Đồng thời, mang chậu Lan &Yacute; thủy sinh đặt nơi tho&aacute;ng m&aacute;t nhưng tr&aacute;nh gi&oacute; mạnh, nắng gắt.</p>\n',
    price: 100000.0,
    original_price: 120000.0,
    sku: 'TreeOTek6',
    qty: 999,
    rank_total: 0,
    features: [],
    option_groups: [],
    options: [],
    created_at: '2020-08-20T00:00:00.000+00:00',
    sell_count: 2,
  },
  {
    priceId: 2267,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 4030,
        thumbnail_url:
          '/upload/product/df05f6ca-d312-4220-ae42-c9f5b673e36e.jpg',
        url: '/upload/product/df05f6ca-d312-4220-ae42-c9f5b673e36e.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 1852,
    name: 'Bánh Cookie',
    category_id: 743,
    store_id: 15,
    short_description: 'Bánh của my way',
    full_description: '<p>B&aacute;nh cookie</p>\n',
    price: 24000.0,
    original_price: 25000.0,
    sku: 'MW01',
    qty: 20,
    rank_total: 0,
    features: [
      {
        global_feature_id: 1683,
        value: 'MY WAY',
        name: 'Thương hiệu',
      },
      {
        global_feature_id: 1684,
        value: '',
        name: 'Hương vị',
      },
      {
        global_feature_id: 1685,
        value: '',
        name: 'Nhu cầu dinh dưỡng',
      },
      {
        global_feature_id: 1686,
        value: 'Túi 200g',
        name: 'Cách đóng gói',
      },
    ],
    option_groups: [],
    options: [],
    created_at: '2020-07-22T00:00:00.000+00:00',
    sell_count: 10,
  },
  {
    priceId: 1280,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 1621,
        thumbnail_url:
          '/upload/product/4e94f819-132b-499a-b20d-91b586c00d27.jpg',
        url: '/upload/product/4e94f819-132b-499a-b20d-91b586c00d27.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 1622,
        thumbnail_url:
          '/upload/product/d948a01c-f899-49b7-ad0b-05091ea45c26.jpg',
        url: '/upload/product/d948a01c-f899-49b7-ad0b-05091ea45c26.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 1623,
        thumbnail_url:
          '/upload/product/069c9882-e31d-4d1f-848e-1e6c74db38e8.jpg',
        url: '/upload/product/069c9882-e31d-4d1f-848e-1e6c74db38e8.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 1049,
    name: 'Đồ chơi xúc xắc Nimo ',
    category_id: 580,
    store_id: 10,
    short_description:
      'Thương hiệu:         \t Nimo\nXuất xứ thương hiệu: \t Việt Nam\nNơi sản xuất: \t         Trung Quốc\nModel \t                         NM18.01.777.38B\nĐộ tuổi sử dụng:    \tTừ sơ sinh\nGiới tính:                \tBé trai , Bé gái\nChất liệu:               \tNhựa ABS\nMàu sắc: \t                Nhiều màu sắc\nÂm thanh:              \tCó\nPin:                         \tKhông',
    full_description:
      '<p>Trong giai đoạn đầu đời của trẻ nhỏ th&igrave; b&eacute; kh&ocirc;ng thể chơi qu&aacute; nhiều tr&ograve; chơi n&ecirc;n những m&oacute;n đồ chơi đơn giản như x&uacute;c xắc sẽ l&agrave;m cho c&aacute;c b&eacute; cảm thấy th&iacute;ch th&uacute;. <strong>Đồ chơi x&uacute;c xắc Nimo</strong> 18.01.777.38B sau đ&acirc;y sẽ l&agrave; một gợi &yacute; đ&aacute;ng y&ecirc;u m&agrave; bố mẹ c&oacute; thể d&agrave;nh tặng cho b&eacute; y&ecirc;u nh&agrave; m&igrave;nh trong giai đoạn n&agrave;y. Bộ đồ chơi được l&agrave;m bằng chất liệu nhựa cao cấp, đảm bảo an to&agrave;n với l&agrave;n da mỏng manh của trẻ nhỏ n&ecirc;n bố mẹ c&oacute; thể ho&agrave;n to&agrave;n y&ecirc;n t&acirc;m cho b&eacute; chơi. Với nhiều chiếc x&uacute;c xắc c&oacute; h&igrave;nh d&aacute;ng kh&aacute;c nhau v&agrave; nhiều m&agrave;u sắc kh&aacute;c nhau chắc chắn sẽ l&agrave;m cho c&aacute;c b&eacute; cảm thấy th&iacute;ch th&uacute;.</p>\n\n<p><img alt="hop-xuc-xac-120168-1" src="https://bibomart.com.vn/media/wysiwyg/2019/Thang_6/hop-xuc-xac-120168-1.jpg" style="height:410px; margin-left:auto; margin-right:auto; width:410px" /><br />\n<em>Đồ chơi x&uacute;c xắc Nimo</em></p>\n\n<p><br />\n<strong>Đặc điểm nổi bật của sản phẩm</strong></p>\n\n<p><br />\n<em><strong>Chất liệu cao cấp, an to&agrave;n</strong></em></p>\n\n<p>- Đồ chơi x&uacute;c xắc Nimo 18.01.777.38B được l&agrave;m bằng chất liệu nhựa ABS cao cấp, kh&ocirc;ng chứa c&aacute;c chất độc hại đảm bảo an to&agrave;n v&agrave; th&acirc;n thiện với l&agrave;n da mỏng manh, nhạy cảm của trẻ nhỏ.</p>\n\n<p>- Bề mặt x&uacute;c xắc được l&agrave;m nhẵn mịn, kh&ocirc;ng c&oacute; cạnh sắc nhọn đảm bảo an to&agrave;n với trẻ nhỏ n&ecirc;n bố mẹ ho&agrave;n to&agrave;n c&oacute; thể y&ecirc;n t&acirc;m cho b&eacute; chơi.</p>\n\n<p><br />\n<em><strong>Thiết kế ngộ nghĩnh gi&uacute;p b&eacute; ph&aacute;t triển c&aacute;c gi&aacute;c quan</strong></em></p>\n\n<p>- Bộ x&uacute;c xắc bao gồm 5 chi tiết nhỏ nhắn với h&igrave;nh d&aacute;ng ngộ nghĩnh kh&aacute;c nhau: x&uacute;c xắc h&igrave;nh m&aacute;y bay trực thăng, h&igrave;nh b&ocirc;ng hoa, h&igrave;nh bạn c&aacute;, lục lạc h&igrave;nh ng&ocirc;i sao v&agrave; đồ chơi điện thoại x&uacute;c xắc. Tất cả đều được thiết kế với c&aacute;c m&agrave;u sắc bắt mắt kh&aacute;c nhau chắc chắn sẽ l&agrave;m cho c&aacute;c b&eacute; cảm thấy th&iacute;ch th&uacute;.</p>\n\n<p><img alt="hop-xuc-xac-120168-3" src="https://bibomart.com.vn/media/wysiwyg/2019/Thang_6/hop-xuc-xac-120168-3.jpg" style="height:410px; margin-left:auto; margin-right:auto; width:410px" /><br />\n<em>H&igrave;nh d&aacute;ng x&uacute;c xắc ngộ nghĩnh, đ&aacute;ng y&ecirc;u</em></p>\n\n<p>- C&aacute;c hạt nhỏ x&uacute;c xắc b&ecirc;n trong tạo n&ecirc;n &acirc;m thanh vui nhộn khi b&eacute; cầm lắc, thu h&uacute;t b&eacute; k&iacute;ch th&iacute;ch th&iacute;nh gi&aacute;c ph&aacute;t triển.</p>\n\n<p>- C&aacute;c m&agrave;u sắc tươi s&aacute;ng, nổi bật của x&uacute;c xắc ph&aacute;t triển thị gi&aacute;c cho b&eacute;. Kết hợp thiết kế h&igrave;nh m&aacute;y bay trực thăng, con c&aacute;, b&ocirc;ng hoa ngộ nghĩnh với c&aacute;c họa tiết đ&aacute;ng y&ecirc;u, x&uacute;c xắc sẽ l&agrave; m&oacute;n đồ chơi gi&uacute;p b&eacute; tập h&igrave;nh th&agrave;nh nhận thức về thế giới xung quanh th&ocirc;ng qua h&igrave;nh ảnh, sự ph&acirc;n t&iacute;ch, li&ecirc;n tưởng, từ đ&oacute; k&iacute;ch th&iacute;ch tư duy v&agrave; n&atilde;o bộ của b&eacute; ph&aacute;t triển ngay từ giai đoạn đầu.</p>\n\n<p>- Thiết kế tay cầm kết hợp c&aacute;c bề mặt sần nổi gi&uacute;p b&eacute; dễ d&agrave;ng cầm nắm m&agrave; kh&ocirc;ng bị trơn, hơn nữa phần tay cầm được thiết kế rộng, vừa tầm tay của b&eacute;, b&eacute; cũng c&oacute; thể học c&aacute;ch cầm nắm v&agrave; cảm nhận đồ vật th&ocirc;ng qua việc tiếp x&uacute;c với đồ chơi.</p>\n\n<p>- Chiếc x&uacute;c xắc h&igrave;nh c&aacute; v&agrave; m&aacute;y bay kết hợp gặm nướu bằng silicon gi&uacute;p b&eacute; giảm nhanh ch&oacute;ng c&aacute;c cơn đau nhức lợi trong giai đoạn mọc răng. Bề mặt mềm mịn xen kẽ bề mặt c&oacute; c&aacute;c nốt sần m&aacute;t-xa nhẹ nh&agrave;ng, tạo hứng th&uacute; v&agrave; cảm gi&aacute;c mới lạ cho b&eacute; khi gặm nướu, gi&uacute;p b&eacute; cảm thấy thoải m&aacute;i, dễ chịu để chơi ngoan cả ng&agrave;y.</p>\n\n<p><img alt="hop-xuc-xac-120168-4" src="https://bibomart.com.vn/media/wysiwyg/2019/Thang_6/hop-xuc-xac-120168-4.jpg" style="height:310px; margin-left:auto; margin-right:auto; width:410px" /><br />\n<em>X&uacute;c xắc kết hợp phần gặm nướu tiện lợi cho b&eacute;</em></p>\n\n<p>&nbsp;</p>\n\n<p><strong>Th&ocirc;ng tin sản phẩm</strong></p>\n\n<p>&nbsp;</p>\n\n<p>- Thương hiệu: Nimo</p>\n\n<p>- Xuất xứ thương hiệu: Việt Nam</p>\n\n<p>- Sản xuất tại Trung Quốc</p>\n\n<p>- Độ tuổi sử dụng: Trẻ từ sơ sinh</p>\n\n<p>- K&iacute;ch thước hộp sản phẩm: 34 x 29 x 6 (cm)</p>\n',
    price: 170000.0,
    original_price: 189000.0,
    sku: 'DOCHOITREEM-SOSINH-001',
    qty: 1,
    rank_total: 0,
    features: [],
    option_groups: [],
    options: [],
    created_at: '2020-04-16T00:00:00.000+00:00',
    sell_count: 1,
  },
  {
    priceId: 1261,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 1542,
        thumbnail_url:
          '/upload/product/77ce011d-1d39-4d36-aa5c-971bf9c0d684.jpg',
        url: '/upload/product/77ce011d-1d39-4d36-aa5c-971bf9c0d684.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 1543,
        thumbnail_url:
          '/upload/product/5ac054aa-44ec-4bf1-92de-7c9ac8cf83e8.jpg',
        url: '/upload/product/5ac054aa-44ec-4bf1-92de-7c9ac8cf83e8.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 1544,
        thumbnail_url:
          '/upload/product/da07dc11-e873-4e27-b743-7787daa88c9f.jpg',
        url: '/upload/product/da07dc11-e873-4e27-b743-7787daa88c9f.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 1545,
        thumbnail_url:
          '/upload/product/ae2c3eb5-6a66-4ccb-847e-bcdb6ac3ed4c.jpg',
        url: '/upload/product/ae2c3eb5-6a66-4ccb-847e-bcdb6ac3ed4c.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 1546,
        thumbnail_url:
          '/upload/product/d1e9f20e-1605-4d15-ad7b-bf0d711f7d2a.jpg',
        url: '/upload/product/d1e9f20e-1605-4d15-ad7b-bf0d711f7d2a.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 1547,
        thumbnail_url:
          '/upload/product/ea8aa831-1d14-4e6c-8aa7-f8b413076689.jpg',
        url: '/upload/product/ea8aa831-1d14-4e6c-8aa7-f8b413076689.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 1548,
        thumbnail_url:
          '/upload/product/2ab80f7a-1f9c-425c-8fcb-ae23fff54323.jpg',
        url: '/upload/product/2ab80f7a-1f9c-425c-8fcb-ae23fff54323.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 1032,
    name: 'Tivi Samsung  Smart Tivi Cong Samsung 4K 49 inch UA49RU7300',
    category_id: 8,
    store_id: 8,
    short_description:
      'Đặc điểm nổi bật\n\n    Thiết kế màn hình 49 inch cong hiện đại, phong cách, bao trùm mọi góc nhìn.\n    Màn hình 4K sắc nét gấp 4 lần Full HD.\n    Tăng cường độ tương phản, sắc màu chi tiết hơn với công nghệ HDR10+.\n    Màu sắc tự nhiên, sống động với công nghệ PurColor.\n    Công nghệ UHD Dimming mang đến sắc đen sâu, sắc sáng sáng hơn.\n    Âm thanh đa chiều, lôi cuốn từ công nghệ Dolby Digital Plus.\n    Hệ điều hành Tizen OS trực quan, mượt mà.\n    Điều khiển tivi linh hoạt bằng điện thoại với ứng dụng SmartThings.\n    Dễ dàng trình chiếu màn hình điện thoại lên tivi với tính năng Screen Mirroring và Airplay 2.',
    full_description: '',
    price: 1.252e7,
    original_price: 1.789e7,
    sku: 'UA49RU7300',
    qty: 100,
    rank_total: 0,
    features: [
      {
        global_feature_id: 1,
        value: '',
        name: 'Xuất xứ',
      },
      {
        global_feature_id: 2,
        value: '',
        name: 'Thương Hiệu',
      },
      {
        global_feature_id: 6,
        value: '',
        name: 'Điện Áp',
      },
      {
        global_feature_id: 17,
        value: '',
        name: 'Bảo Hành ',
      },
    ],
    option_groups: [],
    options: [],
    created_at: '2020-03-05T00:00:00.000+00:00',
    sell_count: 0,
  },
  {
    priceId: 477,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 720,
        thumbnail_url:
          '/upload/product/cbca49bc-0c15-4dcb-8d90-8e573f7a0fd7.jpg',
        url: '/upload/product/cbca49bc-0c15-4dcb-8d90-8e573f7a0fd7.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 404,
    name: 'Máy Lọc Nước Tinh Khiết RO Thông Minh FujiE RO-900 CAB (Hàng Chính Hãng)',
    category_id: 49,
    store_id: 1,
    short_description:
      'Hệ thống lõi lọc 9 cấp lọc gồm :\n\nLõi lọc số 1 : PP Loại bỏ bùn đất, rỉ sét, các loại tạp chất thô, kim loại nặng ... có kích thước ≥ 5µm\n\nLõi lọc số 2 : OCB Hấp thụ kim loại nặng, chất hữu cơ, chất tẩy rửa, các loại hóa chất độc hại\n\nLõi lọc số 3 : CTO Loại bỏ bùn đất, rỉ sét, các loại tạp chất thô, kim loại nặng ...có kích thước ≥ 1µm\n\nLõi lọc số 4 ( màng R.O ) : Loại bỏ hoàn toàn chất rắn, các Ion kim loại nặng, vi sinh vật, vi khuẩn, siêu vi khuẩn, các chất hữu cơ làm cho nước trở nên hoàn toàn tinh khiết nhưng không làm thay đổi tính lý – hoá của nó. Đây là màng lọc quan trọng nhất của toàn bộ hệ thống, được sản xuất tại Mỹ, đáp ứng đầy đủ các tiêu chuẩn của thế giới\n\nLõi lọc số 5 : T33 Cân bằng pH, khử mùi hôi, tạo khoáng và vị ngọt của nước, diệt khuẩn và các loại nấm trong nước và các loại nấm mốc.\n\nLõi lọc số 6 : Lõi khoáng đá bên trong chứa các hạt đá khoáng có nguồn gốc tự nhiên, có tác dụng bổ sung khoáng cho nước tinh khiết là chính, song cũng có tác dụng nâng pH đáng kể cho nước\n\nLõi lọc số 7 : Lõi hồng ngoại bên trong chứa các hạt gốm mà sự tương tác giữa các hạt này tạo ra tia hồng ngoại xa giúp nước đi qua được hoạt hóa. Khi sử dụng nước này sẽ giúp cơ thể con người tăng quá trình trao đổi chất, tăng khả năng hấp thu oxy trong máu, các hạt gốm không tiếp xúc với nước do đó không làm thay đổi TDS hay pH của nước.\n\nLõi lọc số 8 : Nano bạc, Bên trong là lõi than ép bọc vải PP có phủ lớp nano bạc. Tác dụng chính của lõi là diệt khuẩn dựa vào lớp nano bạc, ngoài ra lõi cũng có khả năng hấp thụ màu, mùi và mốc.\n\nLõi lọc số 9 : Lõi Alkaline có chức năng làm tăng độ PH trong nước làm giảm quá trình lão hóa cho da. Khi cơ thể bị mất cân bằng PH trong cơ thể về lâu dài sẽ biến chứng các bệnh không tốt cho sức khỏe. Khi độ PH của cơ thể vượt quá mức cân bằng dư axit bạn sẽ cảm thấy cơ thể mệt mỏi, thiều năng lượng, thừa cân, tiêu hóa kém, đau nhức, rối loại đường ruột gây nên các bệnh như béo phì, bệnh tiểu đường, bệnh gout….Nước sau khi đi qua lõi lọc Alkaline có vị ngon ngọt hơn, tự nhiên hơn.\n\nCÁC THÔNG SỐ:\n\nLoại van : Van điện từ\n\nĐiện áp : 220V ~ 50Hz, qua bộ biến áp còn 24VDC\n\nDung tích bình trữ nước : 10 lít\n\nCông suất lọc : 10 lít/ giờ\n\nCông suất tiêu thụ điện : 24W/h',
    full_description:
      '<p>M&aacute;y lọc nước tinh khiết RO th&ocirc;ng minh FujiE RO-900 CAB ( 9 cấp lọc thế hệ mới 2019 - bao gồm tủ cường lực )</p>\n\n<p><strong>Tủ cường lực in c&ocirc;ng nghệ 3D, sử dụng mặt k&iacute;nh cường lực Temper (Kh&ocirc;ng vỡ), ch&acirc;n đế tủ l&agrave;m bằng nhựa ABS c&oacute; thể uốn dẻo v&agrave; kh&ocirc;ng vỡ.</strong></p>\n\n<p><strong>Tủ được trang bị v&ograve;i gạt inox cao cấp.</strong></p>\n\n<p>M&aacute;y lọc nước R.O FujiE - Chuẩn mực mới nhất l&agrave;m nước ngon.</p>\n\n<p><strong>Với hệ thống tự động kiểm so&aacute;t hoạt động, b&ecirc;n cạnh việc cung cấp nguồn nước tinh khiết, m&aacute;y lọc nước R.O FujiE c&ograve;n c&oacute; những t&iacute;nh năng vượt trội so với c&aacute;c m&aacute;y lọc nước kh&aacute;c c&oacute; tr&ecirc;n thị trường như:</strong></p>\n\n<p>- Tự động ngừng hoạt động khi &aacute;p lực đầu nguồn kh&ocirc;ng đủ.</p>\n\n<p>- Tự động ngừng hoạt động khi nước đầy b&igrave;nh (đủ &aacute;p trong b&igrave;nh &aacute;p).</p>\n\n<p>- Tự động xả nước thải.</p>\n\n<p>- Loại bỏ c&aacute;c tạp chất kim loại nặng độc tố g&acirc;y ung thư bệnh đường ti&ecirc;u ho&aacute;, vi&ecirc;m d&acirc;y thần kinh, tho&aacute;i ho&aacute; cột sống đau lưng</p>\n\n<p>- T&aacute;ch nh&oacute;m ph&acirc;n tử nước, tăng lượng oxy trong nước, ngăn ngừa kh&ocirc; cơ thể, chậm l&atilde;o h&oacute;a.</p>\n\n<p><strong>- M&agrave;ng R.O Filmtec sản xuất tại Mỹ</strong></p>\n\n<p>- C&ocirc;ng nghệ diệt khuẩn ti&ecirc;n tiến h&agrave;ng đầu NANO SILVER c&oacute; tại m&aacute;y lọc nước R.O FujiE</p>\n\n<p>- Loại bỏ tạp chất, vi khuẩn c&ograve;n s&oacute;t lại trong qu&aacute; tr&igrave;nh lọc c&oacute; k&iacute;ch thước lớn hơn &ge; 1 &micro;m</p>\n\n<p>- Tạo kho&aacute;ng chất cần thiết cho cơ thể- Ngăn chặn bẩn b&ugrave;n, đất rỉ s&eacute;t c&oacute; k&iacute;ch thước &ge; 5 &micro;m</p>\n\n<p>- Khả năng hấp thụ cao m&ugrave;i vị, chất hữu cơ, thuốc trừ s&acirc;u, chất diệt c&ocirc;n tr&ugrave;ng, kim loại nặng, Clo dư trong nước</p>\n',
    price: 4545000.0,
    original_price: 5050000.0,
    sku: '34567897654',
    qty: 15,
    rank_total: 0,
    features: [
      {
        global_feature_id: 148,
        value: 'Fujie',
        name: 'Thương hiệu',
      },
      {
        global_feature_id: 149,
        value: 'Nhật Bản',
        name: 'Xuất xứ thương hiệu',
      },
      {
        global_feature_id: 150,
        value: 'RO-900 ( CAB )',
        name: 'Model',
      },
      {
        global_feature_id: 151,
        value: 'Không',
        name: 'Đồng hồ hiển thị áp lực',
      },
      {
        global_feature_id: 152,
        value: '',
        name: 'Kích thước',
      },
      {
        global_feature_id: 153,
        value: ' 220V ~ 50Hz, qua bộ biến áp còn 24VDC',
        name: 'Điện áp',
      },
      {
        global_feature_id: 154,
        value: ' 10 lít',
        name: 'Công suất',
      },
      {
        global_feature_id: 155,
        value: '10 lít/ giờ',
        name: 'Công suất lọc',
      },
      {
        global_feature_id: 156,
        value: 'Van điện từ',
        name: 'loại van ',
      },
    ],
    option_groups: [],
    options: [],
    created_at: '2019-12-04T00:00:00.000+00:00',
    sell_count: 0,
  },
  {
    priceId: 460,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 702,
        thumbnail_url:
          '/upload/product/aa0b9c15-e37e-4738-8f68-55becc3f98e9.jpg',
        url: '/upload/product/aa0b9c15-e37e-4738-8f68-55becc3f98e9.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 387,
    name: '3 người Thầy vĩ đại',
    category_id: 9,
    store_id: 1,
    short_description: 'Tác giả: Robin Sharma\nNhà xuất bản: Lao động',
    full_description:
      '<p><strong>[ThaiHaBooks]</strong>&nbsp;&ldquo;T&ocirc;i đ&atilde; nếm trải nhiều thất bại trong h&agrave;nh tr&igrave;nh đi qua những th&aacute;ng ng&agrave;y của m&igrave;nh. Thế nhưng, mỗi chướng ngại cuối c&ugrave;ng đều lại ch&iacute;nh l&agrave; một b&agrave;n đạp đưa t&ocirc;i gần hơn nữa tới ch&acirc;n l&yacute; trong t&acirc;m khảm v&agrave; cuộc đời tốt đẹp nhất của m&igrave;nh.</p>\n\n<p>Cho d&ugrave; t&ocirc;i c&oacute; thu thập được bao nhi&ecirc;u t&agrave;i sản vật chất đi chăng nữa th&igrave; c&aacute;i thằng người m&agrave; t&ocirc;i nh&igrave;n thấy trong tấm gương ph&ograve;ng tắm mỗi buổi s&aacute;ng vẫn y nguy&ecirc;n &ndash; t&ocirc;i kh&ocirc;ng hề hạnh ph&uacute;c hơn v&agrave; kh&ocirc;ng hề cảm thấy tốt hơn t&iacute; n&agrave;o. Suy ngẫm nhiều hơn về thực trạng cuộc sống của m&igrave;nh, t&ocirc;i bắt đầu nhận thức được sự trống rỗng ngay trong tim m&igrave;nh. T&ocirc;i bắt đầu ch&uacute; &yacute; đến những tiếng thầm th&igrave; lặng lẽ của con tim, những điều chỉ dẫn t&ocirc;i rời bỏ nghề nghiệp m&igrave;nh đ&atilde; chọn v&agrave; bắt đầu qu&aacute; tr&igrave;nh t&igrave;m kiếm t&acirc;m hồn một c&aacute;ch nghi&ecirc;m t&uacute;c. T&ocirc;i bắt đầu suy nghĩ về l&yacute; do tại sao t&ocirc;i lại ở đ&acirc;y, tr&ecirc;n h&agrave;nh tinh n&agrave;y, v&agrave; nhiệm vụ cụ thể của t&ocirc;i l&agrave; g&igrave;. T&ocirc;i tự hỏi tại sao cuộc đời m&igrave;nh lại kh&ocirc;ng c&oacute; t&aacute;c dụng v&agrave; cần phải thực hiện những thay đổi s&acirc;u sắc n&agrave;o để gi&uacute;p m&igrave;nh đi đ&uacute;ng hướng. T&ocirc;i xem x&eacute;t những niềm tin cốt l&otilde;i, những giả định, v&agrave; những lăng k&iacute;nh m&agrave; m&igrave;nh nh&igrave;n ra thế giới, v&agrave; t&ocirc;i tự cam kết l&agrave;m sạch những lăng k&iacute;nh kh&ocirc;ng l&agrave;nh mạnh.&rdquo;</p>\n\n<p><em>&ldquo;Cuốn s&aacute;ch n&agrave;y l&agrave; một t&aacute;c phẩm hư cấu. Đ&acirc;y l&agrave; c&acirc;u chuyện về một người đ&agrave;n &ocirc;ng c&oacute; t&ecirc;n Jack Valentine m&agrave; đường đời c&oacute; nhiều điểm giống với t&ocirc;i. C&oacute; cảm nhận rất kh&ocirc;ng đầy đủ với tư c&aacute;ch một con người, anh ấy l&ecirc;n kế hoạch t&igrave;m kiếm tri thức để sống một cuộc sống hạnh ph&uacute;c hơn, khoẻ khoắn hơn v&agrave; đẹp hơn.&rdquo;</em></p>\n\n<p>Những &ldquo;C&acirc;u hỏi cuối c&ugrave;ng&rdquo; l&agrave; một điều k&igrave; lạ m&agrave; Jack nghe được từ người bệnh nh&acirc;n gi&agrave; nằm c&ugrave;ng ph&ograve;ng với anh &ndash; &ocirc;ng Cal. Chỉ sau một buổi tối tr&ograve; chuyện c&ugrave;ng &ocirc;ng, Jack đ&atilde; nhận thấy những sự thay đổi đang diễn ra trong m&igrave;nh. V&agrave; từ đ&acirc;y, chuyến h&agrave;nh tr&igrave;nh đến Rome, Hawaii v&agrave; New York c&ugrave;ng những kh&aacute;m ph&aacute; mới mẻ m&agrave; anh học được từ ba người thầy vĩ đại trong cuộc đời đ&atilde; gi&uacute;p anh trả lời được ba c&acirc;u hỏi m&agrave; cha anh &ndash; Cal Valentine đ&atilde; n&oacute;i trước khi &ocirc;ng qua đời:</p>\n\n<p>&ndash; Ta đ&atilde; SỐNG một c&aacute;ch KH&Ocirc;N NGOAN chưa?</p>\n\n<p>&ndash; Ta đ&atilde; Y&Ecirc;U THƯƠNG chưa?</p>\n\n<p>&ndash; Ta đ&atilde; CỐNG HIẾN thật nhiều chưa?&rsquo;</p>\n\n<p><strong>Cuốn sổ m&agrave; cha Mike &ndash; người thầy đầu ti&ecirc;n ở Rome đưa cho Jack đ&atilde; đ&uacute;c kết 10 điều m&agrave; anh đ&atilde; học được trong suốt cuộc h&agrave;nh tr&igrave;nh:</strong></p>\n\n<p>1. C&ocirc;ng việc ch&iacute;nh của mọi con người l&agrave; c&ocirc;ng việc nội t&acirc;m.</p>\n\n<p>2. H&atilde;y xem cuộc sống của m&igrave;nh như một trường học dạy c&aacute;ch trưởng th&agrave;nh.</p>\n\n<p>3. H&atilde;y th&agrave;nh thật với ch&iacute;nh m&igrave;nh &ndash; cuộc sống tốt đẹp nhất l&agrave; cuộc sống ch&acirc;n thật.</p>\n\n<p>4. H&atilde;y nhớ rằng ch&uacute;ng ta thu nhận những g&igrave; ch&uacute;ng ta ph&aacute;t ra.</p>\n\n<p>5. Ch&uacute;ng ta nh&igrave;n nhận thế giới kh&ocirc;ng như ch&iacute;nh n&oacute; m&agrave; như ch&uacute;ng ta nghĩ.</p>\n\n<p>6. H&atilde;y sống bằng tr&aacute;i tim của ban &ndash; tri thức của n&oacute; kh&ocirc;ng bao giờ n&oacute;i dối.</p>\n\n<p>7. H&atilde;y đắm m&igrave;nh trong sự t&ograve; m&ograve; của cuộc đời bạn.</p>\n\n<p>8. H&atilde;y chăm lo cho ch&iacute;nh bạn.</p>\n\n<p>9. H&atilde;y x&acirc;y dựng những kết nối của con người.</p>\n\n<p>10. H&atilde;y để lại một di sản.</p>\n',
    price: 79000.0,
    original_price: 86000.0,
    sku: 'Book8',
    qty: 50,
    rank_total: 0,
    features: [],
    option_groups: [],
    options: [],
    created_at: '2019-12-03T00:00:00.000+00:00',
    sell_count: 0,
  },
  {
    priceId: 260,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 495,
        thumbnail_url:
          '/upload/product/83cab95a-cb4b-4031-8ce8-700572e41d64.png',
        url: '/upload/product/83cab95a-cb4b-4031-8ce8-700572e41d64.png',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 496,
        thumbnail_url:
          '/upload/product/64d39645-eb35-4c9a-814e-72f8e8fdd0c2.jpg',
        url: '/upload/product/64d39645-eb35-4c9a-814e-72f8e8fdd0c2.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 206,
    name: 'Tủ lạnh Panasonic Inverter 550 lít NR-DZ600MBVN - Hàng Chính Hãng',
    category_id: 27,
    store_id: 1,
    short_description:
      'Kiểu tủ:Multi Door\n\nDung tích: 550 lít\n\nCông nghệ Inverter\n\nCông nghệ làm lạnh:Panorama\n\nCông nghệ kháng khuẩn Ag Clean với tinh thể bạc Ag+',
    full_description:
      '<p>Tủ lạnh 4 cửa hiện đại, mới lạ</p>\n\n<p>Tủ lạnh Panasonic Inverter 550 l&iacute;t NR-DZ600BMVN với thiết kế 4 cửa kh&ocirc;ng chỉ tối ưu h&oacute;a kh&ocirc;ng gian lưu trữ thực phẩm m&agrave; n&oacute; c&ograve;n t&ocirc; điểm cho gian bếp của bạn một vẻ đẹp sang trọng v&agrave; hiện đại kh&ocirc;ng ngờ. Nếu gia đ&igrave;nh bạn c&oacute; tr&ecirc;n 5 người th&igrave; chiếc tủ lạnh Panasonic 550 l&iacute;t n&agrave;y ch&iacute;nh l&agrave; một sự lựa chọn đ&aacute;ng c&acirc;n nhắc.</p>\n\n<p><img alt="" src="https://salt.tikicdn.com/ts/product/15/b1/71/e64d4b76bdadd341155159ee74ecf0c8.jpg" /></p>\n\n<p>&nbsp;</p>\n\n<p>C&ocirc;ng nghệ Inverter tiết kiệm điện tối ưu</p>\n\n<p>C&ocirc;ng nghệ n&agrave;y gi&uacute;p tủ lạnh Panasonic Inverter vận h&agrave;nh một c&aacute;ch ổn định v&agrave; tiết kiệm điện, hoạt động &ecirc;m &aacute;i, bền bỉ, kh&ocirc;ng g&acirc;y tiếng ồn kh&oacute; chịu cho gia đ&igrave;nh bạn.</p>\n\n<p><img alt="" src="https://salt.tikicdn.com/ts/product/d2/65/df/8b4179d54f44d6a9a32d2d67b25d9cae.jpg" /></p>\n\n<p>&nbsp;</p>\n\n<p>C&ocirc;ng nghệ Econavi hiện đại</p>\n\n<p>Cảm biến Econavi l&agrave; c&ocirc;ng nghệ mới độc quyền với 4 cảm biến b&ecirc;n trong v&agrave; b&ecirc;n ngo&agrave;i tủ, gi&uacute;p tủ lạnh Inverter n&agrave;y lu&ocirc;n duy tr&igrave; nhiệt độ ổn định đảm bảo việc bảo quản thực phẩm tốt hơn, qua đ&oacute; c&ocirc;ng nghệ Econavi c&ograve;n gi&uacute;p bạn tiết kiệm điện l&ecirc;n đến 17%.</p>\n\n<p><img alt="" src="https://salt.tikicdn.com/ts/product/9d/8f/d8/105eae7230e61de329c41706446b37af.jpg" /></p>\n\n<p>&nbsp;</p>\n\n<p>C&ocirc;ng nghệ Panorama gi&uacute;p l&agrave;m lạnh ngăn tủ đồng đều</p>\n\n<p>C&ocirc;ng nghệ l&agrave;m lạnh Panorama đưa kh&iacute; lạnh đến mọi g&oacute;c của c&aacute;c ngăn chứa, gi&uacute;p thực phẩm được l&agrave;m lạnh nhanh ch&oacute;ng v&agrave; giảm điện năng ti&ecirc;u thụ.</p>\n\n<p><img alt="" src="https://salt.tikicdn.com/ts/product/e6/4e/55/891f4fda59a7e6b7fff330d59efa6b43.jpg" /></p>\n\n<p>&nbsp;</p>\n\n<p>C&ocirc;ng nghệ kh&aacute;ng khuẩn Ag Clean với tinh thể bạc Ag+ đ&aacute;nh bay m&ugrave;i h&ocirc;i</p>\n\n<p>Ag Clean l&agrave; c&ocirc;ng nghệ sử dụng c&aacute;c ion bạc Ag+ trong qu&aacute; tr&igrave;nh kh&aacute;ng khuẩn v&agrave; khử m&ugrave;i b&ecirc;n trong tủ lạnh gi&uacute;p thực phẩm tươi sống giữ được tươi ngon v&agrave; kh&ocirc;ng bị biến chất trong thời gian d&agrave;i, bảo vệ sức khỏe cho gia đ&igrave;nh bạn. Giờ đ&acirc;y nỗi &aacute;m ảnh về m&ugrave;i h&ocirc;i kh&oacute; chịu trong tủ lạnh hoặc thực phẩm bị lẫn m&ugrave;i v&agrave;o nhau sẽ kh&ocirc;ng c&ograve;n khiến bạn phải lo lắng v&agrave; bận t&acirc;m nữa.</p>\n\n<p><img alt="" src="https://salt.tikicdn.com/ts/product/04/8d/c4/92611ccc7a9454f8c900724d9d1a7970.jpg" /></p>\n\n<p>&nbsp;</p>\n\n<p>Ngăn cấp đ&ocirc;ng mềm Prime Fresh cho thực phẩm tươi ngon</p>\n\n<p>Ở nhiệt độ -3 độ C, ngăn cấp đ&ocirc;ng mềm sẽ gi&uacute;p đ&ocirc;ng lạnh bề mặt thịt, c&aacute;, nhờ vậy ch&uacute;ng c&oacute; thể giữ nguy&ecirc;n được c&aacute;c dưỡng chất v&agrave; kh&ocirc;ng bị đ&oacute;ng băng. Gi&uacute;p cho những bữa ăn của bạn lu&ocirc;n trọn vẹn hương vị thơm ngon m&agrave; kh&ocirc;ng phải tốn nhiều thời gian r&atilde; đ&ocirc;ng trước khi nấu nướng.</p>\n\n<p><img alt="" src="https://salt.tikicdn.com/ts/product/0b/b8/0c/d6712b7d442992efe96220f0835fa70e.jpg" /></p>\n',
    price: 3.89664e7,
    original_price: 4.3296e7,
    sku: '12345678656453',
    qty: 10,
    rank_total: 0,
    features: [
      {
        global_feature_id: 1,
        value: 'Thái Lan ',
        name: 'Xuất xứ',
      },
      {
        global_feature_id: 2,
        value: 'panasonic ',
        name: 'Thương Hiệu',
      },
      {
        global_feature_id: 6,
        value: '220v ',
        name: 'Điện Áp',
      },
      {
        global_feature_id: 12,
        value: '550L',
        name: 'Dung Tích',
      },
      {
        global_feature_id: 13,
        value: 'Multi Door',
        name: 'Loại tủ',
      },
      {
        global_feature_id: 17,
        value: '',
        name: 'Bảo Hành ',
      },
      {
        global_feature_id: 18,
        value: 'inveter',
        name: 'Công nghệ',
      },
      {
        global_feature_id: 19,
        value: 'Cao 184.6 cm - Rộng 80.5 cm - Sâu 79.0 cm - Nặng 102kg',
        name: 'Kích Thước ',
      },
      {
        global_feature_id: 40,
        value: 'Đen ',
        name: 'màu ',
      },
      {
        global_feature_id: 41,
        value: ' NR-DZ600MBVN ',
        name: 'Model',
      },
    ],
    option_groups: [],
    options: [],
    created_at: '2019-11-20T00:00:00.000+00:00',
    sell_count: 0,
  },
  {
    priceId: 241,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 461,
        thumbnail_url:
          '/upload/product/9dfa264c-67e0-4d7f-8ba2-0b72858e8dc9.jpg',
        url: '/upload/product/9dfa264c-67e0-4d7f-8ba2-0b72858e8dc9.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 462,
        thumbnail_url:
          '/upload/product/e00357a0-a697-477f-ba8f-a0bb1771d2ef.jpg',
        url: '/upload/product/e00357a0-a697-477f-ba8f-a0bb1771d2ef.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 463,
        thumbnail_url:
          '/upload/product/3d962f5a-167b-4a68-9b04-3504512df722.jpg',
        url: '/upload/product/3d962f5a-167b-4a68-9b04-3504512df722.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 464,
        thumbnail_url:
          '/upload/product/b55b1f66-f80b-445a-b271-f741b9dec20f.jpg',
        url: '/upload/product/b55b1f66-f80b-445a-b271-f741b9dec20f.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 191,
    name: 'Laptop HP Pavilion Power 15-cb540TX 4BN72PA',
    category_id: 7,
    store_id: 1,
    short_description:
      'Laptop HP Pavilion Power 15-cb540TX 4BN72PA Core i5-7300HQ/Win10 (15.6 inch) - Hàng Chính Hãng\n',
    full_description:
      '<p>Chip: Intel Core i5-7300HQ (2.5GHz up to 3.5GHz, 6MB)</p>\n\n<p>RAM: 8GB DDR4 2400MHz</p>\n\n<p>Ổ cứng: HDD 1TB 7200rpm, x1 slot SSD M.2 SATA</p>\n\n<p>Chipset đồ họa: NVIDIA GeForce GTX 1050 4GB GDDR5 + Intel HD Graphics 630</p>\n\n<p>M&agrave;n h&igrave;nh: 15.6 inch Full HD (1920 x 1080) Diagonal IPS BrightView WLED-backlit</p>\n\n<p>Hệ điều h&agrave;nh: Windows 10 bản quyền</p>\n\n<p>Pin: 4 Cell 70 Whr</p>\n',
    price: 2.50897e7,
    original_price: 2.78775e7,
    sku: '4BN72PA',
    qty: 10,
    rank_total: 0,
    features: [],
    option_groups: [],
    options: [],
    created_at: '2019-11-19T00:00:00.000+00:00',
    sell_count: 0,
  },
  {
    priceId: 154,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 288,
        thumbnail_url:
          '/upload/product/ad40e087-621e-43b5-839c-9a96ef69b000.jpg',
        url: '/upload/product/ad40e087-621e-43b5-839c-9a96ef69b000.jpg',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 289,
        thumbnail_url:
          '/upload/product/c9365d00-068d-4f2c-b3f2-4f654794060c.jpg',
        url: '/upload/product/c9365d00-068d-4f2c-b3f2-4f654794060c.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 105,
    name: 'Tủ lạnh Samsung RL4364SBABS/SV ngăn đá dưới 458 lít',
    category_id: 27,
    store_id: 1,
    short_description:
      'Công nghệ làm lạnh: Công nghệ làm lạnh vòm\nCông nghệ kháng khuẩn, khử mùi: Bộ Lọc khử mùi\nCông nghệ bảo quản thực phẩm: Hộc rau quả giữ ẩm\nTiện ích: Lấy nước ngoài\nKiểu tủ: Ngăn đá dưới\nSố cửa: 2 cửa\nChất liệu cửa tủ lạnh: \nChất liệu khay ngăn: Kính chịu lực\nĐèn chiếu sáng: Đèn LED',
    full_description:
      '<p>Thiết kế hiện đại</p>\n\n<p><strong><a href="https://dienmaynguoiviet.vn/tu-lanh-samsung-rl4364sbabssv-ngan-da-duoi-458-lit/" target="_blank">Tủ lạnh Samsung ngăn đ&aacute; dưới&nbsp;RL4364SBABS/SV 458 l&iacute;t</a></strong>&nbsp;với thiết kế phẳng tinh giản gi&uacute;p n&acirc;ng tầm kh&ocirc;ng gian sang trọng với n&eacute;t đẹp thời thượng.&nbsp;<strong>Tủ lạnh Samsung&nbsp;</strong>với thiết kế phẳng giảm thiểu chi tiết đem lại sự tao nh&atilde; sang trọng, thư th&aacute;i v&agrave; phong c&aacute;ch vượt thời gian tạo n&ecirc;n dấu ấn ri&ecirc;ng cho căn bếp của bạn.</p>\n\n<p><img alt="Tủ lạnh Samsung ngăn đá dưới RL4364SBABS/SV 458 lít" src="https://dienmaynguoiviet.vn/media/lib/4683_tu-lanh-samsung-rl4364sbabssv-ngan-da-duoi-458-lit-org-10.jpg" style="height:533px; width:800px" /></p>\n\n<p>Lấy nước ngo&agrave;i tiện dụng</p>\n\n<p>Với&nbsp;<strong>tủ lạnh Samsung ngăn đ&aacute; dưới&nbsp;RL4364SBABS/SV&nbsp;</strong>bạn c&oacute; thể dễ d&agrave;ng&nbsp;Dễ d&agrave;ng c&oacute; ngay những ly nước m&aacute;t lạnh tuyệt vời m&agrave; kh&ocirc;ng cần mở tủ lạnh thường xuy&ecirc;n. Ngăn Lấy Nước Ngo&agrave;i tiện &iacute;ch l&ecirc;n đến 2L v&agrave; được thiết kế ho&agrave;n hảo b&ecirc;n ngo&agrave;i tủ lạnh ở vị tr&iacute; vừa tầm sử dụng.</p>\n\n<p><img alt="tủ lạnh Samsung ngăn đá dưới RL4364SBABS/SV" src="https://dienmaynguoiviet.vn/media/lib/4683_tu-lanh-samsung-rl4364sbabssv-ngan-da-duoi-458-lit-org-11.jpg" style="height:500px; width:800px" /><br />\n<img alt="" src="https://dienmaynguoiviet.vn/tu-lanh-samsung-rl4364sbabssv-ngan-da-duoi-458-lit/t%E1%BB%A7%20l%E1%BA%A1nh%20Samsung%20ng%C4%83n%20%C4%91%C3%A1%20d%C6%B0%E1%BB%9Bi%20RL4364SBABS/SV" /></p>\n\n<p>C&ocirc;ng nghệ l&agrave;m lạnh v&ograve;m</p>\n\n<p>C&ocirc;ng nghệ l&agrave;m lạnh v&ograve;m được trang bị tr&ecirc;n&nbsp;<strong>tủ lạnh Samsung RL4364SBABS/SV&nbsp;</strong>gi&uacute;p duy tr&igrave; nhiệt độ v&agrave; độ ẩm lu&ocirc;n ổn định. Kh&ocirc;ng kh&iacute; lạnh lan tỏa đều khắp tủ, gi&uacute;p thực phẩm tươi ngon trong thời gian l&acirc;u hơn, l&agrave;m lạnh nhanh hơn v&agrave; nhiệt độ đồng đều.</p>\n\n<p><img alt="tủ lạnh Samsung RL4364SBABS/SV" src="https://dienmaynguoiviet.vn/media/lib/4683_tu-lanh-samsung-rl4364sbabssv-ngan-da-duoi-458-lit-org-12.jpg" style="width:800px" /></p>\n\n<p>&nbsp;Bộ lọc than hoạt t&iacute;nh</p>\n\n<p><strong>Tủ lạnh Samsung ngăn đ&aacute; dưới</strong>&nbsp;được trang bị bộ lọc than hoạt t&iacute;nh gi&uacute;p tinh lọc kh&ocirc;ng kh&iacute; b&ecirc;n trong, loại bỏ m&ugrave;i h&ocirc;i kh&oacute; chịu v&agrave; giữ lại lớp kh&ocirc;ng kh&iacute; trong l&agrave;nh, tươi m&aacute;t cho thức ăn lu&ocirc;n tươi ngon, trọn vị.</p>\n\n<p><img alt="Tủ lạnh Samsung ngăn đá dưới" src="https://dienmaynguoiviet.vn/media/lib/4683_tu-lanh-samsung-rl4364sbabssv-ngan-da-duoi-458-lit-org-13.jpg" style="height:450px; width:800px" /></p>\n\n<p><strong>Tiết kiệm điện năng hoạt động bền bỉ</strong></p>\n\n<p><strong><a href="https://dienmaynguoiviet.vn/tu-lanh/" target="_blank">Tủ lạnh</a>&nbsp;</strong>được trang bị m&aacute;y n&eacute;n biến tần kỹ thuật số Digital Inverter vận h&agrave;nh tự động điều chỉnh 5 cấp độ theo nhu cầu l&agrave;m lạnh. Tiết kiệm điện năng, giảm thiểu tiếng ồn v&agrave; giảm hao m&ograve;n động cơ gi&uacute;p k&eacute;o d&agrave;i tuổi thọ.</p>\n\n<p><img alt="Tiết kiệm điện năng hoạt động bền bỉ" src="https://dienmaynguoiviet.vn/media/lib/4683_tu-lanh-samsung-rl4364sbabssv-ngan-da-duoi-458-lit-org-14.jpg" style="width:800px" /></p>\n',
    price: 1.65132e7,
    original_price: 1.8348e7,
    sku: '138390138190830',
    qty: 10,
    rank_total: 0,
    features: [
      {
        global_feature_id: 1,
        value: 'TháiLan ',
        name: 'Xuất xứ',
      },
      {
        global_feature_id: 2,
        value: 'Samsunng ',
        name: 'Thương Hiệu',
      },
      {
        global_feature_id: 6,
        value: '220v',
        name: 'Điện Áp',
      },
      {
        global_feature_id: 12,
        value: '458 lít',
        name: 'Dung Tích',
      },
      {
        global_feature_id: 13,
        value: 'Ngăn Đá Dưới',
        name: 'Loại tủ',
      },
      {
        global_feature_id: 17,
        value: '',
        name: 'Bảo Hành ',
      },
      {
        global_feature_id: 18,
        value: 'Tủ Lạnh  Inverter',
        name: 'Công nghệ',
      },
      {
        global_feature_id: 19,
        value: ' Cao 185 cm - Rộng 70 cm - Sâu 70.5 cm',
        name: 'Kích Thước ',
      },
    ],
    option_groups: [],
    options: [],
    created_at: '2019-11-14T00:00:00.000+00:00',
    sell_count: 0,
  },
  {
    priceId: 1252,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 137,
        thumbnail_url:
          '/upload/product/fa0bde88-b9e5-424b-916c-0a858a1235f7.png',
        url: '/upload/product/fa0bde88-b9e5-424b-916c-0a858a1235f7.png',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 138,
        thumbnail_url:
          '/upload/product/680ab96a-a290-443c-92ff-847d24c23d50.png',
        url: '/upload/product/680ab96a-a290-443c-92ff-847d24c23d50.png',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 50,
    name: 'Tủ Lạnh SAMSUNG RS63R5571SL/SV',
    category_id: 413,
    store_id: 1,
    short_description:
      'Thiết kế tối giản, 3 cửa lưu trữ thông minh\n\nTiết kiệm điện, bền bỉ với công nghệ Digital Inverter\n\nThêm nhiều không gian lưu trữ hơn với công nghệ Spacemax\n\nGIữ nhiệt hiệu quả với tấm chắn Metal Cooling\n\nHơi lạnh toả đều và ổn định với công nghệ làm lạnh dạng vòm\n\nKháng khuẩn, khử mùi hiệu quả với bộ lọc than hoạt tính',
    full_description:
      '<p>Thiết kế tối giản, thời thượng</p>\n\n<p>Với thiết kế sang trọng v&agrave; t&ocirc;ng m&agrave;u thời thượng,&nbsp;tủ lạnh Samsung Inverter 634 l&iacute;t RS63R5571SL/SV&nbsp;sẽ l&agrave; điểm nhấn ch&iacute;nh, ho&agrave;n thiện vẻ đẹp tao nh&atilde; cho căn bếp hiện đại. Hơn nữa, chiếc tủ lạnh&nbsp;3 cửa&nbsp;n&agrave;y với thiết kế kh&ocirc;ng gian s&acirc;u sẽ gi&uacute;p cho tủ lạnh vừa vặn, h&agrave;i h&ograve;a với tổng thể kh&ocirc;ng gian bếp, kh&ocirc;ng để lộ bất kỳ chi tiết thừa n&agrave;o.</p>\n\n<p><img alt="Tủ lạnh Samsung Inverter 634 lít RS63R5571SL/SV" src="https://salt.tikicdn.com/ts/product/7a/cf/07/3eef37e69b107568c430a89281a67628.jpg" style="height:417px; width:780px" /></p>\n\n<p>Dung t&iacute;ch l&ecirc;n đến 634 l&iacute;t, thoải m&aacute;i cho gia đ&igrave;nh tr&ecirc;n 5 người sử dụng</p>\n\n<p>Sở hữu kh&ocirc;ng gian lưu trữ với dung t&iacute;ch l&ecirc;n đến&nbsp;634 l&iacute;t, chiếc&nbsp;tủ lạnh Samsung&nbsp;n&agrave;y sẽ gi&uacute;p bạn lưu trữ v&agrave; dễ d&agrave;ng sắp xếp thực phẩm cho suốt cả một tuần. Kh&ocirc;ng những thế, tủ c&ograve;n c&oacute; c&ocirc;ng nghệ Spacemax: viền si&ecirc;u mỏng tạo th&ecirc;m nhiều kh&ocirc;ng gian hơn cho bạn để bảo quản thực phẩm.</p>\n\n<p><img alt="Dung tích 634 lít - Tủ lạnh Samsung Inverter 634 lít RS63R5571SL/SV" src="https://salt.tikicdn.com/ts/product/92/6a/6f/6ecce87720dc580e40b7c539af18d187.jpg" style="height:433px; width:780px" /></p>\n\n<p>C&ocirc;ng nghệ Inverter&nbsp;tiết kiệm điện năng v&ocirc; c&ugrave;ng hiệu quả</p>\n\n<p>Để giảm bớt nỗi lo cho bạn về tiền điện h&agrave;ng th&aacute;ng th&igrave; c&ocirc;ng nghệ Inverter được h&atilde;ng t&iacute;ch hợp v&agrave;o sẽ gi&uacute;p bạn giải quyết vấn đề n&agrave;y nhờ v&agrave;o khả năng tiết kiệm điện v&ocirc; c&ugrave;ng hiệu quả. Hơn nữa, c&ograve;n gi&uacute;p cho&nbsp;tủ lạnhvận h&agrave;nh &ecirc;m, bền bỉ theo thời gian.</p>\n\n<p><img alt="Công nghệ Inverter- Tủ lạnh Samsung Inverter 634 lít RS63R5571SL/SV" src="https://salt.tikicdn.com/ts/product/59/15/52/6ab9235c1fcdf15d5eb3fa6a2ebf75a2.jpg" style="height:433px; width:780px" /></p>\n\n<p>C&ocirc;ng nghệ l&agrave;m lạnh v&ograve;m, gi&uacute;p thực phẩm tươi ngon l&acirc;u hơn</p>\n\n<p>C&ocirc;ng nghệ n&agrave;y lu&acirc;n chuyển hơi lạnh theo h&igrave;nh v&ograve;ng cung xung quanh tủ, gi&uacute;p l&agrave;m lạnh cho thực phẩm đều hơn, duy tr&igrave; độ ẩm tối ưu, tr&agrave;nh trường hợp lẫn m&ugrave;i, bảo quản cho thực phẩm được tươi ngon l&acirc;u hơn.</p>\n\n<p><img alt="Công nghệ làm lạnh vòm - Tủ lạnh Samsung Inverter 634 lít RS63R5571SL/SV" src="https://salt.tikicdn.com/ts/product/a8/fa/3f/9a1bef1500ce2b327b691e8da2028b79.jpg" style="height:433px; width:780px" /></p>\n\n<p>Lọc sạch kh&ocirc;ng kh&iacute; trong tủ với&nbsp;bộ lọc than hoạt t&iacute;nh</p>\n\n<p>M&agrave;n lọc Than Hoạt T&iacute;nh c&oacute; tr&ecirc;n chiếc&nbsp;tủ lạnh Inverter&nbsp;n&agrave;y với lưới kh&aacute;ng khuẩn gi&uacute;p khử m&ugrave;i li&ecirc;n tục v&agrave; loại bỏ vi khuẩn hiệu quả, mang đến luồng kh&ocirc;ng kh&iacute; lu&acirc;n chuyển trong l&agrave;nh v&agrave; giữ tủ lạnh lu&ocirc;n sạch sẽ,&nbsp;vệ sinh, bảo vệ sức khỏe cho gia đ&igrave;nh của bạn.</p>\n\n<p><img alt="Bộ lọc than hoạt tính - Tủ lạnh Samsung Inverter 634 lít RS63R5571SL/SV" src="https://salt.tikicdn.com/ts/product/4e/b8/e3/1d925e77bf7e450e7458587c61503b03.jpg" style="height:433px; width:780px" /></p>\n\n<p>GIữ nhiệt hiệu quả với tấm chắn giữ nhiệt Metal Cooling</p>\n\n<p>Tấm chắn giữ nhiệt Metal Cooling&nbsp;gi&uacute;p giữ lạnh hiệu quả v&agrave; hạn chế t&igrave;nh trạng mất nhiệt, v&igrave; vậy thực phẩm được bảo quản tốt hơn v&agrave; lu&ocirc;n tươi ngon, kể cả khi bạn thường xuy&ecirc;n mở cửa tủ lạnh. Tấm kim loại n&agrave;y kh&ocirc;ng chỉ dễ d&agrave;ng lau sạch m&agrave; c&ograve;n gi&uacute;p ho&agrave;n thiện vẻ ngo&agrave;i sang trọng cho tủ lạnh nh&agrave; bạn.</p>\n\n<p><img alt="Tấm chắn Metal Cooling - Tủ lạnh Samsung Inverter 634 lít RS63R5571SL/SV" src="https://salt.tikicdn.com/ts/product/ce/4e/36/e24278737b81f0581ae1d4ed0030d179.jpg" style="height:433px; width:780px" /></p>\n\n<p>Nh&igrave;n chung, nếu như bạn đang t&igrave;m kiếm một chiếc tủ lạnh cỡ lớn cho gia đ&igrave;nh đ&ocirc;ng th&agrave;nh vi&ecirc;n th&igrave; chắc chắn chiếc tủ lạnh Samsung Inverter 634 l&iacute;t RS63R5571SL/SV n&agrave;y sẽ kh&ocirc;ng l&agrave;m bạn thất vọng. Với những c&ocirc;ng nghệ hiện đại, tối ưu nhất được trang bị tr&ecirc;n tủ sẽ l&agrave;m bạn h&agrave;i l&ograve;ng trong qu&aacute; tr&igrave;nh sử dụng.</p>\n',
    price: 3.8061e7,
    original_price: 4.224e7,
    sku: '252851136717',
    qty: 10,
    rank_total: 0,
    features: [
      {
        global_feature_id: 1,
        value: '',
        name: 'Xuất xứ',
      },
      {
        global_feature_id: 2,
        value: 'SAMSUNG',
        name: 'Thương Hiệu',
      },
      {
        global_feature_id: 6,
        value: '220V-50HZ',
        name: 'Điện Áp',
      },
      {
        global_feature_id: 17,
        value: '',
        name: 'Bảo Hành ',
      },
      {
        global_feature_id: 12,
        value: '634L',
        name: 'Dung Tích',
      },
      {
        global_feature_id: 13,
        value: '',
        name: 'Loại tủ',
      },
      {
        global_feature_id: 18,
        value: '',
        name: 'Công nghệ',
      },
      {
        global_feature_id: 19,
        value: '',
        name: 'Kích Thước ',
      },
      {
        global_feature_id: 40,
        value: '',
        name: 'màu ',
      },
      {
        global_feature_id: 41,
        value: '',
        name: 'Model',
      },
    ],
    option_groups: [],
    options: [],
    created_at: '2019-11-08T00:00:00.000+00:00',
    sell_count: 3,
  },
  {
    priceId: 85,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 135,
        thumbnail_url:
          '/upload/product/1e52f957-f670-49c5-b5ff-24aa971b5d6f.png',
        url: '/upload/product/1e52f957-f670-49c5-b5ff-24aa971b5d6f.png',
        sort_order: 1,
      },
      {
        type: 'avatar',
        id: 136,
        thumbnail_url:
          '/upload/product/e6ff3ce2-13bc-4d08-ada6-01a6b517622e.png',
        url: '/upload/product/e6ff3ce2-13bc-4d08-ada6-01a6b517622e.png',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 48,
    name: 'Tủ Lạnh SAMSUNG RS62R5001M9/SV',
    category_id: 27,
    store_id: 1,
    short_description:
      'Đặc điểm nổi bật\n\nThiết kế tối giản, đen sang trọng\n\nTiết kiệm điện năng hiệu quả với công nghệ Digital Inverter\n\nHơi lạnh lan tỏa đồng đều đến mọi nơi thông qua công nghệ làm lạnh dạng vòm\n\nLoại bỏ mùi hôi khó chịu nhờ bộ lọc khử mùi than hoạt tính\n\nThiết kế viền siêu mỏng, mở rộng không gian lưu trữ với công nghệ SpaceMax\n\nChuông báo cửa mở tránh lãng phí điện',
    full_description:
      '<p>Tủ lạnh Side by Side sang trọng, đẳng cấp</p>\n\n<p>Tủ lạnh Samsung Inverter 647 l&iacute;t RS62R5001M9/SV&nbsp;c&oacute; thiết kế&nbsp;Side by Side&nbsp;đẳng cấp, đi c&ugrave;ng gam m&agrave;u đen sang trọng, thời thượng, tủ lạnh sẽ l&agrave; điểm nhấn nổi bật, mang lại cho kh&ocirc;ng gian nội thất của gia đ&igrave;nh một vẻ đẹp hiện đại.</p>\n\n<p><img alt="Tủ lạnh Samsung Inverter 647 lít RS62R5001M9/SV" src="https://salt.tikicdn.com/ts/product/7c/f1/45/24ed0c552432f8d14bd2385fefff9b00.jpg" /></p>\n\n<p>Dung t&iacute;ch 647 l&iacute;t&nbsp;d&agrave;nh cho gia đ&igrave;nh đ&ocirc;ng th&agrave;nh vi&ecirc;n</p>\n\n<p>Với tổng dung t&iacute;ch sử dụng l&ecirc;n đến 647 l&iacute;t, chiếc&nbsp;tủ lạnh Samsung Side by Side&nbsp;n&agrave;y l&agrave; một sự lựa chọn l&yacute; tưởng cho gia đ&igrave;nh c&oacute; kh&ocirc;ng gian sống rộng r&atilde;i, c&ugrave;ng số th&agrave;nh vi&ecirc;n tr&ecirc;n 5 người.</p>\n\n<p>Kh&ocirc;ng chỉ vậy, c&ocirc;ng nghệ SpaceMax với thiết kế viền si&ecirc;u mỏng của tủ lạnh n&agrave;y gi&uacute;p tăng cường dung t&iacute;ch m&agrave; kh&ocirc;ng thay đổi k&iacute;ch thước ngo&agrave;i. Từ đ&oacute; kh&ocirc;ng gian lưu trữ được rộng r&atilde;i hơn, cho bạn thoải m&aacute;i bảo quản nhiều loại thực phẩm.</p>\n\n<p><img alt="Dung tích 647 lít - Tủ lạnh Samsung Inverter 647 lít RS62R5001M9/SV" src="https://salt.tikicdn.com/ts/product/7e/77/62/35c9b0ee47077f65cdca689d4107c957.jpg" /></p>\n\n<p>C&ocirc;ng Nghệ Digital Inverter&nbsp;mang đến khả năng tiết kiệm điện hiệu quả</p>\n\n<p>Mặc d&ugrave; l&agrave; một chiếc tủ lạnh c&oacute; dung t&iacute;ch lớn nhưng tủ lạnh Samsung&nbsp;RS62R5001M9/SV lại c&oacute; khả năng tiết kiệm điện hiệu quả nhờ c&ocirc;ng nghệ Digital Inverter. Với c&ocirc;ng nghệ n&agrave;y,&nbsp;tủ lạnh Samsung Inverter&nbsp;sẽ gi&uacute;p bạn x&oacute;a tan nỗi lo lắng về h&oacute;a đơn tiền điện h&agrave;ng th&aacute;ng khi sử dụng tủ lạnh dung t&iacute;ch lớn.</p>\n\n<p>Kh&ocirc;ng chỉ vậy, Samsung c&ograve;n ưu &aacute;i hơn cho kh&aacute;ch h&agrave;ng của m&igrave;nh khi c&oacute; ch&iacute;nh s&aacute;ch bảo h&agrave;nh l&ecirc;n đến 10 năm cho m&aacute;y n&eacute;n Inverter.</p>\n\n<p><img alt="Digital Inverter - Tủ lạnh Samsung Inverter 647 lít RS62R5001M9/SV" src="https://salt.tikicdn.com/ts/product/1d/60/10/7de180bf24ae23a247b6c23ac582f61a.jpg" /></p>\n\n<p>Mang hơi lạnh lan tỏa đồng đều đến mọi nơi b&ecirc;n trong tủ với&nbsp;c&ocirc;ng nghệ l&agrave;m lạnh v&ograve;m</p>\n\n<p>Trang bị c&ocirc;ng nghệ l&agrave;m lạnh dạng v&ograve;m,&nbsp;tủ lạnh&nbsp;sẽ mang đến luồng kh&iacute; lạnh dạng v&ograve;ng cung lan tỏa đồng đều đến mọi vị tr&iacute; ngay cả khi thực phẩm chứa đầy b&ecirc;n trong tủ. Nhờ vậy, thực phẩm kh&ocirc;ng chỉ được l&agrave;m lạnh nhanh ch&oacute;ng m&agrave; ch&uacute;ng c&ograve;n nhận được đầy đủ hơi lạnh th&iacute;ch hợp để tươi ngon l&acirc;u hơn trong qu&aacute; tr&igrave;nh bảo quản.</p>\n\n<p><img alt="Công nghệ làm lạnh vòm - Tủ lạnh Samsung Inverter 647 lít RS62R5001M9/SV" src="https://salt.tikicdn.com/ts/product/f3/26/12/a44bea26a34b8a013883c4131c9b5d80.jpg" /></p>\n\n<p>Bộ lọc khử m&ugrave;i than hoạt t&iacute;nh&nbsp;loại bỏ m&ugrave;i h&ocirc;i kh&oacute; chịu, duy tr&igrave; độ thơm ngon của thực phẩm</p>\n\n<p>Sở hữu bộ lọc khử m&ugrave;i than hoạt t&iacute;nh,&nbsp;tủ lạnh Samsung&nbsp;c&oacute; khả năng loại bỏ đi c&aacute;c m&ugrave;i h&ocirc;i kh&oacute; chịu do thực phẩm hoặc vi khuẩn g&acirc;y ra, trả lại bầu kh&ocirc;ng gian trong l&agrave;nh b&ecirc;n trong tủ, gi&uacute;p bạn kh&ocirc;ng c&ograve;n kh&oacute; chịu bởi m&ugrave;i h&ocirc;i mỗi lần mở tủ lạnh, cũng như gi&uacute;p cho những m&oacute;n thực phẩm của bạn duy tr&igrave; được độ thơm ngon, g&oacute;p phần đem đến cho bạn sự trải nghiệm tuyệt vời.</p>\n\n<p><img alt="Bộ lọc khử mùi than hoạt tính - Tủ lạnh Samsung Inverter 647 lít RS62R5001M9/SV" src="https://salt.tikicdn.com/ts/product/22/93/7f/c5f2e3439c8c765a0e6f7c4f185f7a25.jpg" /></p>\n\n<p>Chu&ocirc;ng b&aacute;o cửa mở&nbsp;tiện lợi, tr&aacute;nh l&atilde;ng ph&iacute; điện năng</p>\n\n<p>Nếu bạn c&oacute; lỡ qu&ecirc;n đ&oacute;ng cửa, tủ lạnh sẽ tự động ph&aacute;t ra tiếng chu&ocirc;ng th&ocirc;ng b&aacute;o cho bạn kịp thời nhận biết v&agrave; đ&oacute;ng cửa lại, tr&aacute;nh thất tho&aacute;t hơi lạnh g&acirc;y hư hỏng thực phẩm cũng như l&atilde;ng ph&iacute; điện năng v&ocirc; &iacute;ch.</p>\n\n<p><img alt="Chuông báo cửa mở - Tủ lạnh Samsung Inverter 647 lít RS62R5001M9/SV" src="https://salt.tikicdn.com/ts/product/5d/ef/c6/f6ff86fb9fdb20149d62ce1725b573a8.jpg" /></p>\n\n<p>Vừa được ra mắt trong năm 2019, chiếc tủ lạnh&nbsp;&nbsp;Samsung RS62R5001M9/SV kh&ocirc;ng chỉ mang kiểu d&aacute;ng Side by Side hiện đại, dung t&iacute;ch lớn m&agrave; n&oacute; c&ograve;n nổi bật nhờ c&ocirc;ng nghệ SpaceMax với thiết kế viền si&ecirc;u mỏng, gi&uacute;p khai ph&oacute;ng kh&ocirc;ng gian, cho ph&eacute;p bạn lưu trữ v&agrave; bảo quản được nhiều thực phẩm hơn những chiếc tủ lạnh đời cũ. Nếu gia đ&igrave;nh bạn c&oacute; th&oacute;i quen mua nhiều thực phẩm để d&ugrave;ng một lần th&igrave; chiếc tủ lạnh Samsung Side by Side n&agrave;y ch&iacute;nh l&agrave; một sự lựa chọn đ&aacute;ng c&acirc;n nhắc.</p>\n',
    price: 2.26908e7,
    original_price: 2.5212e7,
    sku: '7636952403496',
    qty: 10,
    rank_total: 0,
    features: [
      {
        global_feature_id: 1,
        value: '',
        name: 'Xuất xứ',
      },
      {
        global_feature_id: 2,
        value: 'SAMSUNG ',
        name: 'Thương Hiệu',
      },
      {
        global_feature_id: 6,
        value: '220V-50HZ',
        name: 'Điện Áp',
      },
      {
        global_feature_id: 12,
        value: '647L',
        name: 'Dung Tích',
      },
    ],
    option_groups: [],
    options: [],
    created_at: '2019-11-08T00:00:00.000+00:00',
    sell_count: 2,
  },
  {
    priceId: 51,
    reviewed: false,
    images: [
      {
        type: 'avatar',
        id: 77,
        thumbnail_url:
          '/upload/product/321aafe7-698d-48fb-8086-760fda6c952f.jpg',
        url: '/upload/product/321aafe7-698d-48fb-8086-760fda6c952f.jpg',
        sort_order: 1,
      },
    ],
    rating: 5.0,
    status: 10,
    id: 25,
    name: 'Máy sấy quần áo Electrolux EDS7552, 7.5kg (Trắng)',
    category_id: 28,
    store_id: 1,
    short_description:
      '- Khối lượng sấy lên đến 7.5kg\n- Chức năng sấy nhanh trong 40 phút\n- Nhiều tính năng sấy áo quần tiện lợi\n- Công nghệ sấy đảo chiều cho sợi vải mềm hơn',
    full_description:
      '<p><strong>M&agrave;u sắc trang nh&atilde;, ph&ugrave; hợp cho mọi kh&ocirc;ng gian nội thất<br />\nM&aacute;y sấy quần &aacute;o Electrolux EDS7552 7.5kg</strong>&nbsp;c&oacute; kiểu d&aacute;ng hiện đại, m&agrave;u sắc trang nh&atilde;, ph&ugrave; hợp lắp đặt trong nhiều kh&ocirc;ng gian nội thất kh&aacute;c nhau, c&oacute; thể đặt cạnh, treo tr&ecirc;n tường hoặc đặt l&ecirc;n tr&ecirc;n m&aacute;y giặt cửa trước của Electrolux. Hơn nữa, m&agrave;u sắc của sản phẩm c&ograve;n đ&aacute;p ứng sở th&iacute;ch nhiều đối tượng người d&ugrave;ng.</p>\n\n<p><img src="https://cdn02.static-adayroi.com/0/2017/05/29/1496032898644_5903447.jpg" /><br />\n<em>M&aacute;y sấy quần &aacute;o Electrolux EDS7552 7.5kg</em></p>\n\n<p><br />\n<strong>Nhiều chức năng, ph&ugrave; hợp với nhu cầu sử dụng</strong><br />\n<strong>M&aacute;y sấy quần &aacute;o Electrolux EDS7552</strong>&nbsp;được thiết lập c&aacute;c chương tr&igrave;nh sấy tự động, cho ph&eacute;p người d&ugrave;ng thoải m&aacute;i chọn lựa, điều khiển, điều chỉnh thời gian sấy kh&ocirc; gi&uacute;p tiết kiệm thời gian tr&aacute;nh việc sấy qu&aacute; kh&ocirc; g&acirc;y hư hại đến quần &aacute;o.&nbsp;C&ocirc;ng nghệ sấy đảo chiều, t&iacute;nh năng sấy t&aacute;c động k&eacute;p l&agrave;m cho sợi vải mềm mại hơn. Kỹ thuật sấy xoay tr&ograve;n Iron Aid l&agrave;m giảm thiểu tối đa nếp nhăn tr&ecirc;n quần &aacute;o ngay cả khi quần &aacute;o chưa kịp lấy ra sau khi sấy xong. Ngo&agrave;i ra, với chức năng sấy nhanh trong 40 ph&uacute;t cho khối lượng sấy 2kg quần &aacute;o th&ocirc;ng thường, sẽ gi&uacute;p mang đến sự tiện lợi tối ưu cho người sử dụng.</p>\n\n<p><img alt="Máy sấy quần áo Electrolux EDS7552S 7.5kg" src="https://cdn02.static-adayroi.com/0/2017/05/29/1496034564727_1891853.jpg" /><br />\n<em>Bảng điều khiển tiện lợi</em><br />\n<img alt="Máy sấy quần áo Electrolux EDS7552S 7.5kg" src="https://cdn02.static-adayroi.com/0/2017/05/29/1496032396717_1843902.jpg" /><br />\n<em>Thời gian sấy nhanh ch&oacute;ng</em></p>\n\n<p><br />\n<strong>C&ocirc;ng suất cao, chế độ cảm biến</strong><br />\n<strong>M&aacute;y sấy quần &aacute;o Electrolux EDS7552</strong>&nbsp;c&oacute; c&ocirc;ng suất 2250W c&ugrave;ng khối lượng 7.5&nbsp;kg đ&aacute;p ứng nhu cầu sấy của hầu hết mọi gia đ&igrave;nh. Ngo&agrave;i ra, m&aacute;y sẽ tự động ngừng sấy khi đạt độ kh&ocirc; chuẩn, tr&aacute;nh l&agrave;m hư vải quần &aacute;o cũng như tiết kiệm điện năng tối đa cho gia đ&igrave;nh.</p>\n\n<p><img alt="Máy sấy quần áo Electrolux EDS7552S 7.5kg" src="https://cdn02.static-adayroi.com/0/2017/05/29/1496055056066_5539635.jpg" /><br />\n<em>C&ocirc;ng suất hoạt động mạnh mẽ</em></p>\n\n<p><br />\n<strong>Vật liệu lồng th&eacute;p kh&ocirc;ng gỉ<br />\nLồng sấy quần &aacute;o Electrolux EDS7552&nbsp;</strong>l&agrave;m bằng chất liệu th&eacute;p kh&ocirc;ng gỉ cao cấp gi&uacute;p tăng tuổi thọ m&aacute;y, ngăn nấm mốc ph&aacute;t triển. Bạn sẽ dễ d&agrave;ng vệ sinh m&aacute;y, kh&ocirc;ng lo cặn bận b&aacute;m lại tr&ecirc;n th&agrave;nh sau mỗi lần sử dụng.</p>\n\n<p><img src="https://cdn02.static-adayroi.com/0/2017/05/29/1496033009483_3636294.jpg" /><br />\n<em>Lồng sấy bằng th&eacute;p kh&ocirc;ng gỉ cao cấp</em></p>\n\n<p><br />\n<strong>Th&ocirc;ng số kỹ thuật</strong></p>\n\n<p><img src="https://cdn02.static-adayroi.com/0/2017/05/29/149605566667_8182746.jpg" /></p>\n',
    price: 7389000.0,
    original_price: 7389000.0,
    sku: 'EDS7552',
    qty: 5,
    rank_total: 0,
    features: [
      {
        global_feature_id: 1,
        value: 'Thái Lan',
        name: 'Xuất xứ',
      },
      {
        global_feature_id: 2,
        value: 'Electrolux ',
        name: 'Thương Hiệu',
      },
      {
        global_feature_id: 6,
        value: '220V/5hz',
        name: 'Điện Áp',
      },
    ],
    option_groups: [],
    options: [],
    created_at: '2019-10-21T00:00:00.000+00:00',
    sell_count: 1,
  },
];

export default class FlashSaleView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      hasFlashSale: true,
      hour: 2,
      minute: 3,
      second: 4,
    };
  }
  componentDidMount() {
    // this.getEventInfo();
    setInterval(() => {
      flashSaleTime = flashSaleTime - 1;
      if (flashSaleTime > 0) {
        this.setState({hour: Math.floor(flashSaleTime / 3600)});
        this.setState({
          minute: Math.floor(
            flashSaleTime / 60 === 60 ? 0 : flashSaleTime / 60,
          ),
        });
        this.setState({second: flashSaleTime % 60});
      }
    }, 1000);
  }

  render() {
    let data = DATA;
    if (data === undefined || data.length === 0) {
      return null;
    }
    return (
      <View
        style={[
          {
            paddingHorizontal: 10,
            paddingVertical: 20,
          },
        ]}>
        <View>
          <View>
            <Text
              style={[Styles.text.text12,{
                color: ColorStyle.red,
                textAlign: 'right',
                marginRight: 5,
              }]}>
              {strings('viewAll')}
            </Text>
          </View>

          <View style={{flexDirection: 'row', marginTop: 8}}>
            <View
              style={{
                width: Styles.constants.widthScreen - 30,
                marginLeft: 9,
                marginRight: 20,
                height: Styles.constants.X * 1.2,
                backgroundColor: ColorStyle.tabActive,
                borderTopRightRadius: 12,
                borderTopLeftRadius: 12,
                justifyContent: 'space-between',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Text
                style={[
                  Styles.text.text24,
                  {marginLeft: Styles.constants.X * 1.2},
                ]}>
                {strings('flash_sale')}
              </Text>
              <View style={{flexDirection: 'row'}}>
                <View style={styles.boxText}>
                  <Text style={styles.textBox}>
                    {this.state.hour / 10 < 1
                      ? '0' + this.state.hour
                      : this.state.hour}
                  </Text>
                </View>
                <View style={styles.boxText}>
                  <Text style={styles.textBox}>
                    {this.state.minute / 10 < 1
                      ? '0' + this.state.minute
                      : this.state.minute}
                  </Text>
                </View>
                <View style={styles.boxText}>
                  <Text style={styles.textBox}>
                    {this.state.second / 10 < 1
                      ? '0' + this.state.second
                      : this.state.second}
                  </Text>
                </View>
              </View>
            </View>
            <Image
              style={{
                height: Styles.constants.X * 1.2,
                width: Styles.constants.X * 0.9,
                position: 'absolute',
              }}
              source={ImageHelper.flashSaleIcon}
            />
          </View>
        </View>

        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={data}
          keyExtractor={this.keyExtractor}
          renderItem={this._renderItem}
          removeClippedSubviews={true} // Unmount components when outside of window
          windowSize={10}
        />
      </View>
    );
  }

  keyExtractor = (item, index) => `flashSale_${index.toString()}`;
  _renderItem = ({item, index}) => (
    <FlashSaleProductItem item={item} index={index} />
  );

  // getFlashSaleList(id) {
  //   let param = {
  //     event_id: id,
  //     current_page: 0,
  //     page_size: 10,
  //   };
  //   this.props.showLoading(true);
  //   ProductHandle.getInstance().getProductList(
  //     param,
  //     (isSuccess, responseData, msg) => {
  //       this.props.showLoading(false);
  //       if (isSuccess) {
  //         if (responseData.data != null && responseData.data.list) {
  //           this.setState({data: responseData.data.list});
  //         }
  //       } else {
  //         // console.log('Failed: ' + hasFlashSale);
  //       }
  //     },
  //     true,
  //   );
  // }

  // getEventInfo() {
  //   let params = {
  //     sort_by: 'from',
  //     sort_order: 'asc',
  //   };
  //   EventHandle.getInstance().getEventProduct(
  //     params,
  //     (isSuccess, dataResponse) => {
  //       if (isSuccess) {
  //         let eventList = dataResponse.data.list;
  //         if (eventList !== undefined && eventList.length > 0) {
  //           let event = eventList[0];
  //           flashSaleTime = event.to - new Date().getTime();
  //           this.getFlashSaleList(event.id);
  //           this.setState({hasFlashSale: true});
  //         }
  //       }
  //     },
  //   );
  // }
}
const styles = StyleSheet.create({
  styleTitle: {
    width: 180,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  textBox: {
    color: 'white',
    fontSize: 12,
    marginHorizontal: 5,
    justifyContent: 'center',
  },
  boxText: {
    height: Styles.constants.X * 0.6,
    width: Styles.constants.X * 0.6,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    borderRadius: 2,
    justifyContent: 'center',
    marginHorizontal: 3,
  },
});
