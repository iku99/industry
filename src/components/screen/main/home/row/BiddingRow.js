import React, {Component} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../../../resource/Styles';
const isTablet = GlobalUtil.isTablet();
import {
  getBannerInfoAction,
  getCategoryAction,
  getProductAction,
} from '../../../../../actions';
import categoryReducers from '../../../../../reducers/categoryRendecers';
import CategoryItem from '../../../../elements/viewItem/CategoryItem/CategoryItem';
import GlobalUtil from '../../../../../Utils/Common/GlobalUtil';
import ColorStyle from '../../../../../resource/ColorStyle';
import ImageHelper from '../../../../../resource/images/ImageHelper';
import ViewProductItem from '../../../../elements/viewItem/ViewedProduct/ViewProductItem';
import productReducers from '../../../../../reducers/productRendecers';
import ProductUtils from '../../../../../Utils/ProductUtils';
import AppConstants from '../../../../../resource/AppConstants';
import ProductRowLoading from '../../../../elements/SkeletonPlaceholder/ProductRowLoading';
import ProductHandle from '../../../../../sagas/ProductHandle';
import {strings} from '../../../../../resource/languages/i18n';
import GroupHandle from '../../../../../sagas/GroupHandle';
import DataUtils from '../../../../../Utils/DataUtils';
import ItemSupplierView from '../../../../elements/viewItem/ItemSupplierView';
import Barchart from '../../../../elements/viewItem/ViewChart/Barchart';
import {Icon} from 'react-native-elements';
import DateUtil from '../../../../../Utils/DateUtil';
import fetchDataBidding from '../../../../../Api/fetchDataBidding';
import {Actions} from 'react-native-router-flux';
const X = Styles.constants.X;
const LIMIT = 20;
const Data = [
  {
    name: 'Sở Kế hoạch và Đầu tư tỉnh Hà Nam',
    price: '5000000',
    quantity: 22,
    color: '#FFC120',
  },
  {
    name: 'Bệnh viện thống nhất',
    price: '3800000',
    quantity: 18,
    color: '#014B56',
  },
  {
    name: 'Ban quản lý dự án 7',
    price: '2900000',
    quantity: 7,
    color: '#DC0C1F',
  },
  {
    name: 'Tập đoàn Công nghiệp- Viễn ...',
    price: '2500000',
    quantity: 7,
    color: '#BABABA',
  },
  {
    name: 'Sở Kế hoạch và Đầu tư tỉnh Hà Nam',
    price: '1000000',
    quantity: 7,
    color: '#046fc6',
  },
  {
    name: 'Ban quản lý đầu tư xây dựng ...',
    price: '900000',
    quantity: 7,
    color: '#9814a7',
  },
];
const Data1 = [
  {
    name: 'Sở Kế hoạch và Đầu tư tỉnh Hà Nam',
    createdDate: '2021-08-06T00:00:00.000+00:00',
  },
  {
    name: 'Sở Kế hoạch và Đầu tư tỉnh Hà Nam',
    createdDate: '2021-08-06T00:00:00.000+00:00',
  },
  {
    name: 'Sở Kế hoạch và Đầu tư tỉnh Hà Nam',
    createdDate: '2021-08-06T00:00:00.000+00:00',
  },
  {
    name: 'Sở Kế hoạch và Đầu tư tỉnh Hà Nam',
    createdDate: '2021-08-06T00:00:00.000+00:00',
  },
];
class BiddingRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: Data,
      loading: this.props.loading,
      pricerMax: 0,
      dataBidding: [],
    };
  }
  componentDidMount() {
    this.props.showLoading(true);
    let data = DataUtils.formatFour(fetchDataBidding.data[0].child_package);
    this.setState({dataBidding: data});
  }
  render() {
    if (this.state.data === undefined || this.state.data.length === 0) {
      return <ProductRowLoading />;
    }

    return (
      <View style={[Styles.containerItemHomeFullWidth]}>
        <Text
          style={[
            Styles.text.text24,
            {
              color: ColorStyle.textTitle24,
              fontWeight: '700',
              marginTop: 10,
              maxWidth: X * 4.6,
            },
          ]}
          numberOfLines={2}>
          {strings('bidding')}
        </Text>
        <Text
          style={[
            Styles.text.text14,
            {
              color: ColorStyle.tabBlack,
              width: Styles.constants.widthScreen,
              fontWeight: '700',
              marginTop: 20,
              paddingBottom: 5,
              borderBottomWidth: 1,
              borderBottomColor: ColorStyle.tabBlack,
            },
          ]}>
          {strings('top5Bidding')}
        </Text>
        <View
          style={{
            width: Styles.constants.widthScreen / 2.5,
            borderTopWidth: 2,
            borderColor: ColorStyle.tabActive,
          }}
        />
        <FlatList
          contentContainerStyle={{
            alignSelf: 'center',
          }}
          style={{marginVertical: 15}}
          data={Data}
          keyExtractor={this.keyExtractor}
          renderItem={this._renderItem}
          listKey={"Listdata"}
        />
        <View>
          <Text
            style={[
              Styles.text.text14,
              {
                color: ColorStyle.tabBlack,
                width: Styles.constants.widthScreen,
                fontWeight: '700',
                marginTop: 20,
                paddingBottom: 5,
                borderBottomWidth: 1,
                borderBottomColor: ColorStyle.tabBlack,
              },
            ]}>
            {strings('namePacket')}
          </Text>
          <View
            style={{
              width: Styles.constants.widthScreen / 2.5,
              borderTopWidth: 2,
              borderColor: ColorStyle.tabActive,
            }}
          />
          <FlatList
            contentContainerStyle={{
              alignSelf: 'center',
            }}
            style={{marginVertical: 15}}
            data={this.state.dataBidding}
            keyExtractor={this.keyExtractor}
            renderItem={this._renderItemPacket}
            listKey={"ListdataBidding"}
          />
        </View>
        <TouchableOpacity
          onPress={() => Actions.jump('biddingScreen')}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 20,
          }}>
          <Image source={ImageHelper.circle} style={Styles.icon.iconShow} />
          <Text
            style={[
              Styles.text.text10,
              {color: ColorStyle.tabActive, fontWeight: '700'},
            ]}>
            {strings('viewAll')}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
  keyExtractor = (item, index) => `bidding_${index.toString()}`;
  _renderItem = ({item, index}) => {
    let priceMax = DataUtils.formatPriceMax(this.state.data);
    return (
      <Barchart
        item={item}
        value={true}
        pricerMax={priceMax}
        index={index}
        loading={this.state.loading}
      />
    );
  };
  _renderItemPacket = ({item}) => {
    return (
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginBottom: 19,
          borderColor: ColorStyle.optionTextColor,
          borderBottomWidth: 0.5,
          paddingBottom: 5,
        }}>
        <Text
          style={[
            Styles.text.text12,
            {
              fontWeight: '500',
              color: ColorStyle.tabBlack,
              maxWidth: (Styles.constants.widthScreen / 3) * 2,
            },
          ]}
          numberOfLines={1}>
          {item.name_unit}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Icon name="clock" type="feather" size={10} />
          <Text
            style={[
              Styles.text.text10,
              {fontWeight: '400', color: ColorStyle.tabBlack, marginLeft: 5},
            ]}>
            {DateUtil.formatDate('HH:mm DD:MM:YYYY', item.createdDate)}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  getPricerMax(price) {}
}
const mapStateToProps = state => {
  return {
    // product: state.productReducers,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onGetProduct: param => {
      // dispatch(getProductAction(param));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(BiddingRow);
