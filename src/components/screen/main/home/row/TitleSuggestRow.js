import React, {Component} from 'react';
import {
  Dimensions,
  FlatList,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../../../resource/Styles';
import {strings} from '../../../../../resource/languages/i18n';
import ViewProductItem from '../../../../elements/viewItem/ViewedProduct/ViewProductItem';
import {getCategoryAction} from '../../../../../actions';
import GlobalUtil from '../../../../../Utils/Common/GlobalUtil';
export default class TitleSuggestRow extends Component {
  componentDidMount() {
    this.props.showLoading(false);
  }

  render() {
    let isTablet = GlobalUtil.isTablet();
    let columnNumber = isTablet ? 3 : 2;
    return (
      <View style={{...Styles.containerItemHome,marginBottom:20}}>
        <Text
          style={[
            Styles.text.textTitleHome,
            {
              width: Styles.constants.widthScreenMg24,
              maxWidth: Styles.constants.widthScreenMg24,
            },
          ]}>
          {strings('suggest')}
        </Text>
      </View>
    );
  }
}
