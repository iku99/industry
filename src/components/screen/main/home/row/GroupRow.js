import React, {Component} from 'react';
import {
  AsyncStorage,
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import {connect} from 'react-redux';
import Styles from '../../../../../resource/Styles';
const isTablet = GlobalUtil.isTablet();
import GlobalUtil from '../../../../../Utils/Common/GlobalUtil';
import ColorStyle from '../../../../../resource/ColorStyle';
import ProductRowLoading from '../../../../elements/SkeletonPlaceholder/ProductRowLoading';
import {strings} from '../../../../../resource/languages/i18n';
import GroupHandle from '../../../../../sagas/GroupHandle';
import BestGroupItem from '../../../../elements/viewItem/itemGroup/BestGroupItem';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import ItemGroupRow from '../../../../elements/viewItem/itemGroup/ItemGroupRow';
import GroupRowLoading from '../../../../elements/SkeletonPlaceholder/GroupRowLoading';
import RefreshGroupLoading from "../../../../elements/SkeletonPlaceholder/RefreshGroupLoading";
import AppConstants from "../../../../../resource/AppConstants";
import FuncUtils from "../../../../../Utils/FuncUtils";
const X = Styles.constants.X;
const LIMIT = 20;
class GroupRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 0,
      groups: [],
      loading:true,
      isLogged:true
    };

  }
  componentDidMount() {
      this.getBestGroupList();
    AsyncStorage.getItem(
      AppConstants.SharedPreferencesKey.IsLogin,
      (error, isLogin) => {
        this.setState({isLogged: isLogin === '1'});
        if(isLogin==='1'){
            this.getMyGroup()}
      },
    );

  }
  render() {
      if (this.state.loading) {
         return <GroupRowLoading />;
    }
    if(this.state.groups.length===0 && this.state.data.length===0) return <View/>
    return (
      <View style={[Styles.containerItemHomeFullWidth, {paddingHorizontal: 0}]}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginTop: 10,
            marginHorizontal: Styles.constants.marginHorizontalAll,
            display:this.state.isLogged?'flex':'none',
          }}>
          <Text
            style={[
              Styles.text.text24,
              {
                color: ColorStyle.textTitle24,
                left: 0,
                fontWeight: '700',
                maxWidth: X * 4.6,
              },
            ]}
            numberOfLines={2}>
            {strings('group')}
          </Text>
          <TouchableOpacity onPress={() => Actions.jump('groups', {index: 1})}>
            <Text
              style={[
                Styles.text.text11,
                {fontWeight: '700', color: ColorStyle.tabActive},
              ]}>
              {strings('viewAllGroup')}
            </Text>
          </TouchableOpacity>
        </View>
        <FlatList
          data={this.state.groups}
          horizontal={true}
          keyExtractor={this.keyExtractor}
          showsHorizontalScrollIndicator={false}
          style={{marginVertical: 10, paddingBottom: 10,   display:this.state.isLogged?'flex':'none',marginHorizontal:10}}
          renderItem={this._renderItemRow}
        />
          {this.state.data.length !==0 &&(
              <View style={{marginHorizontal: Styles.constants.marginHorizontalAll}}>
                  <View
                      style={{
                          width: Styles.constants.widthScreenMg24,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          marginTop: 10,
                      }}>
                      <View>
                          <Text style={[Styles.text.textTitleHome, {fontSize: this.state.isLogged?14:24}]}>
                              {strings('suggestedGroup')}
                          </Text>
                          <Text style={[Styles.text.text10, {fontSize: this.state.isLogged?10:16}]}>
                              {strings('desTitleGroup')}
                          </Text>
                      </View>
                      <TouchableOpacity
                          onPress={() => {
                              {
                                  this.setState({data: []},()=>{
                                      this.getBestGroupList();
                                  });

                              }
                          }}
                          style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                          }}>
                          <Icon
                              name="refresh"
                              type="font-awesome"
                              size={15}
                              color={ColorStyle.tabActive}
                          />
                          <Text
                              style={[
                                  Styles.text.text12,
                                  {marginLeft: 10, color: ColorStyle.tabActive},
                              ]}>
                              {strings('refresh')}
                          </Text>
                      </TouchableOpacity>
                  </View>
                  <FlatList
                      data={this.state.data}
                      keyExtractor={this.keyExtractor}
                      style={{marginVertical: 10}}
                      renderItem={this._renderItem}
                  />
                  {this.state.data.length === 0 && <RefreshGroupLoading />}
              </View>

          )}
      </View>
    );
  }
  getBestGroupList() {
      let params = {
          page_index: 1,
          page_size:4,
          type:AppConstants.GetGroups.SUGGEST_GROUP
      };
        GroupHandle.getInstance().getGroups(params, (isSuccess, responseData) => {
            if (isSuccess) {
                    if (responseData.data != null ) {
                        let data = responseData.data.data;
                        this.setState({data, loading:false});
                    }
                }
            },this.state.isLogged);
        return
  }
    getMyGroup() {
        let params = {
            page_index: 1,
            page_size:4,
            type:AppConstants.GetGroups.MY_GROUP
        };
        GroupHandle.getInstance().getGroups(params, (isSuccess, responseData) => {
            if (isSuccess) {
                if (responseData.data != null ) {
                    let data = responseData.data.data;
                    this.setState({groups:data, loading:false});
                }
            }
        },true);
        return
    }
  keyExtractor = (item, index) => `groupRow${index.toString()}`;
  _renderItem = ({item, index}) => {
    return (
      <BestGroupItem
        item={item}
        index={index}
        onPress={item => {
          if (this.props.onItemPress !== undefined) {
            this.props.onItemPress();
          }
            FuncUtils.getInstance().callRequireLogin(() => {
                Actions.jump('groupDetail', {item: item, checkRole: 3});
            })
        }}
      />
    );
  };
  _renderItemRow = ({item, index}) => {
    return (
      <ItemGroupRow
        item={item}
        index={index}
        onPress={item => {
          Actions.jump('groupDetail', {item: item, checkRole: 1});
        }}
      />
    );
  };
}
const mapStateToProps = state => {
  return {
    // product: state.productReducers,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onGetProduct: param => {
      // dispatch(getProductAction(param));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(GroupRow);
