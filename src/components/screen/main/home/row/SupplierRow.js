import React, {Component} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../../../resource/Styles';
import ColorStyle from '../../../../../resource/ColorStyle';
import ProductRowLoading from '../../../../elements/SkeletonPlaceholder/ProductRowLoading';
import {strings} from '../../../../../resource/languages/i18n';
import DataUtils from '../../../../../Utils/DataUtils';
import ItemSupplierView from '../../../../elements/viewItem/ItemSupplierView';
import {Actions} from 'react-native-router-flux';
import StoreHandle from "../../../../../sagas/StoreHandle";
const X = Styles.constants.X;
const LIMIT = 20;
class SupplierRow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
    };
  }
  componentDidMount() {
    this.getSupplier()
  }
  getSupplier(){
    let params={
      page_index:1,
      page_size:6
    }
    StoreHandle.getInstance().getTopStore(params,(isSuccess,res)=>{
      if(isSuccess){
        this.setState({data:res.data,loading:false})
      }
    })
  }
  render() {
    if (this.state.loading) {
      return <ProductRowLoading />;
    }else  if (this.state.data.length===0) {
        return <View/>
    }
    return (
      <View style={[Styles.containerItemHomeFullWidth,{paddingHorizontal:0}]}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginTop: 10,
            marginHorizontal:Styles.constants.marginHorizontal20
          }}>
          <Text
            style={[
              Styles.text.text24,
              {
                color: ColorStyle.textTitle24,
                marginBottom: 20,
                fontWeight: '700',
                maxWidth: X * 4.6,
              },
            ]}
            numberOfLines={2}>
            {strings('supplier')}
          </Text>
          <TouchableOpacity
            onPress={() => {
              Actions.jump('supplierScreen');
            }}>
            <Text
              style={[
                Styles.text.text11,
                {fontWeight: '700', color: ColorStyle.tabActive},
              ]}>
              {strings('viewAll')}
            </Text>
          </TouchableOpacity>
        </View>
        <FlatList
          contentContainerStyle={{
            alignSelf: 'center',
          }}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          style={{paddingBottom: 20}}
          // data={DataUtils.formatCategory(this.state.data)}
            data={this.state.data}
          keyExtractor={this.keyExtractor}
          renderItem={this._renderItem}
        />
      </View>
    );
  }
  keyExtractor = (item, index) => `supplierRow_${index.toString()}`;
  _renderItem = ({item, index}) => (
    <ItemSupplierView item={item} loading={this.state.loading} />
  );
}
const mapStateToProps = state => {
  return {
    // product: state.productReducers,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onGetProduct: param => {
      // dispatch(getProductAction(param));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SupplierRow);
