import React, {Component} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {EventRegister} from 'react-native-event-listeners';
import {strings} from '../../../../../resource/languages/i18n';
import ImageHelper from '../../../../../resource/images/ImageHelper';
import AppConstants from '../../../../../resource/AppConstants';
import ViewUtils from '../../../../../Utils/ViewUtils';
import Styles from '../../../../../resource/Styles';

const data = [
  {
    text: strings('menu'),
    icon: ImageHelper.danhMucItem,
    // action: () => {
    //   EventRegister.emit(AppConstants.EventName.GO_TO_TAB, 2);
    // },
  },
  {
    text: strings('endow'),
    icon: ImageHelper.uuDai,
    // action: () => {
    //   ViewUtils.showAlertDialog(strings('developing'), null);
    // },
  },
  {
    text: strings('bookRoom'),
    icon: ImageHelper.datPhongItem,
    // action: () => {
    //   ViewUtils.showAlertDialog(strings('developing'), null);
    // },
  },
  {
    text: strings('ticketBooking'),
    icon: ImageHelper.mayBayItem,
    // action: () => {
    //   ViewUtils.showAlertDialog(strings('developing'), null);
    // },
  },
  {
    text: strings('orderProducts'),
    icon: ImageHelper.mayBayItem,
    // action: () => {
    //   ViewUtils.showAlertDialog(strings('developing'), null);
    // },
  },
];
export default class QuickMenu extends Component {
  render() {
    return (
      <View style={{width: Dimensions.get('window').width,paddingTop: Styles.constants.marginTopAll,backgroundColor:'red'}}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={data}
          keyExtractor={this.keyExtractor}
          renderItem={this._renderItem}
        />
      </View>
    );
  }
  keyExtractor = (item, index) => `quickMenuRow_${index.toString()}`;
  _renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        style={Styles.quickMenuButton}
        // onPress={item.action}
      >
        <Image source={item.icon} style={Styles.quickMenuImage} />
        <Text style={{textAlign: 'center'}}>{item.text}</Text>
      </TouchableOpacity>
    );
  };
}
