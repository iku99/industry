import React, {Component} from 'react';
import {AsyncStorage, FlatList, Image, Text, TouchableOpacity, View,} from 'react-native';
import Styles from '../../../../resource/Styles';
import {getBannerInfoAction, getProductAction,} from '../../../../actions';
import {connect} from 'react-redux';
import AppConstants from '../../../../resource/AppConstants';
import AppBannerRow from './row/AppBannerRow';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import ColorStyle from '../../../../resource/ColorStyle';
import ImageHelper from '../../../../resource/images/ImageHelper';
import {strings} from '../../../../resource/languages/i18n';
import ProductMenu from './row/ProductMenu';
import ProductRow from './row/ProductRow';
import CommonSearchRow from './row/CommonSearchRow';
import BannerBigSaleRow from './row/BannerBigSaleRow';
import SuggestRow from './row/SuggestRow';
import ProductHandle from '../../../../sagas/ProductHandle';
import TitleSuggestRow from './row/TitleSuggestRow';
import BannerUtils from '../../../../Utils/BannerUtils';
import GlobalInfo from '../../../../Utils/Common/GlobalInfo';

import GroupRow from './row/GroupRow';
import SupplierRow from './row/SupplierRow';
import BiddingRow from './row/BiddingRow';
import {UIActivityIndicator} from 'react-native-indicators';
import AnimatedHeader from '../../../elements/animation/HeaderAnimation';
import {EventRegister} from "react-native-event-listeners";
import MediaUtils from "../../../../Utils/MediaUtils";
import TradePromotionRow from "./row/TradePromotionRow";

const LIMIT = 10;

const defaulData = [
  {
    type: AppConstants.homeItemType.BANNER,
  },
  {
    type: AppConstants.homeItemType.PRODUCT_MENU,
  },
  {
    type: AppConstants.homeItemType.PRODUCT_DAY,
  },
  {
    type: AppConstants.homeItemType.PRODUCT_BEST_SELLING,
  },
  {
    type: AppConstants.homeItemType.COMMON_SEARCH,
  },
  {
    type: AppConstants.homeItemType.GROUP,
  },
  {
    type: AppConstants.homeItemType.TRADE_PROMOTION,
  },
  {
    type: AppConstants.homeItemType.SUPPLIERS,
  },
  {
    type: AppConstants.homeItemType.BIDDING,
  },
  {
    type: AppConstants.homeItemType.TITLE_SUGGEST,
  },
];
class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
      page: 1,
      canLoadData: true,
      totalCart: 0,
      selectCategory: undefined,
      showList: true,
      statusBarAlpha: 1,
      refreshing: false,
      userName: undefined,
      isLogged: false,
    };
  }

  componentDidMount() {
    this.setState({data: defaulData}, () => {
      this.getProductSuggestList();
      EventRegister.addEventListener(
          AppConstants.EventName.CHANGE_AVATAR,
          data => {
            this.setState({userName:{}},()=>{this.setState({userName:data})});
          },
      );
      this.setState({userName: GlobalInfo.userInfo})
      AsyncStorage.getItem(
        AppConstants.SharedPreferencesKey.IsLogin,
        (error, isLogin) => {
          this.setState({isLogged: isLogin === '1'});
        },
      );
    });
  }
  componentWillMount() {
    this.listener = EventRegister.addEventListener(
      AppConstants.EventName.LOGIN,
      isLogin => {
        this.setState({isLogged: isLogin});
      },
    );
  }

  componentWillUnmount() {
    EventRegister.removeEventListener(this.listener);
  }
  render() {
    return (
      <AnimatedHeader
        style={{flex: 1}}
        bodyText="Back"
        renderLeft={() => (
          <TouchableOpacity onPress={() => Actions.drawerOpen()}>
            <Icon
              name="menu"
              type="entypo"
              size={25}
              style={{marginLeft: Styles.constants.X/2}}
              color={ColorStyle.tabWhite}
            />
          </TouchableOpacity>
        )}
        renderRight={() => (
          <TouchableOpacity onPress={() => Actions.jump('profile')}>
            {!this.state.isLogged?(<Icon
                name="user"
                type="antdesign"
                size={25}
                color={ColorStyle.tabWhite}
                style={{marginRight: Styles.constants.X/2}}
            />):(
                <Image source={MediaUtils.getAvatar(this.state.userName)} style={Styles.icon.iconAvatarSmall} />
            )}
          </TouchableOpacity>
        )}
        renderSearch={() => (
          <TouchableOpacity
            style={Styles.search.searchInput}
            onPress={() => Actions.jump('searchScreen')}>
                <Icon name={'search'} type={'evilicons'} size={25} color={'#a59999'}/>
            <Text
              style={Styles.search.searchText}
              underlineColorAndroid="transparent">
              {strings('textSearch')}
            </Text>
          </TouchableOpacity>
        )}
        backTextStyle={{fontSize: 14, color: '#000'}}
        titleStyle={{fontSize: 22, left: 20, bottom: 20, color: '#000'}}
        headerMaxHeight={Styles.constants.X *3.5}
        imageSource={ImageHelper.logo}
        toolbarColor={ColorStyle.tabActive}
        disabled={false}>
        <FlatList
          showsVerticalScrollIndicator={false}
          horizontal={false}
          data={this.state.data}
          keyExtractor={this.keyExtractor}
          renderItem={this._renderItem}
          ListFooterComponent={this.renderFooter.bind(this)}
          onEndReachedThreshold={0.4}
          onEndReached={() => this.handleLoadMore()}
          ref={ref => {
            this.listRef = ref;
          }}
          refreshing={this.state.refreshing}
          removeClippedSubviews={true} // Unmount components when outside of window
          windowSize={10}
        />
      </AnimatedHeader>
    );
  }

  keyExtractor = (item, index) => `home_${index.toString()}`;

  _renderItem = ({item}) => {
    switch (item.type) {
      case AppConstants.homeItemType.BANNER:
        return (
          <AppBannerRow
            showLoading={loading => this.setState({loading})}
            type={AppConstants.BANNER_POSITION.MAIN}
            statusBarAlpha={this.state.statusBarAlpha}
            containerStyle={{
              maxHeight: (Styles.constants.widthScreen * 2.5) / 3,
            }}
            backgroundStyle={{height: (Styles.constants.widthScreen * 1.5) / 3}}
          />
        );
      case AppConstants.homeItemType.PRODUCT_MENU:
        return (
          <ProductMenu showLoading={loading => this.setState({loading})} />
        );
      case AppConstants.homeItemType.PRODUCT_DAY:
        return (
          <ProductRow
            showLoading={loading => this.setState({loading})}
            type={'today'}
            size={8}
            title={strings('product_of_day')}
          />
        );

      case AppConstants.homeItemType.PRODUCT_BEST_SELLING:
        return (
          <ProductRow
            showLoading={loading => this.setState({loading})}
            type={'best-seller'}
            size={10}
            title={strings('product_best_selling')}
          />
        );
      case AppConstants.homeItemType.COMMON_SEARCH:
        return (
          <CommonSearchRow showLoading={loading => this.setState({loading})} />
        );
      case AppConstants.homeItemType.GROUP:
        return <GroupRow showLoading={loading => this.setState({loading})} />;
      case AppConstants.homeItemType.TRADE_PROMOTION:
        return (
          <TradePromotionRow showLoading={loading => this.setState({loading})}/>
        );
      case AppConstants.homeItemType.SUPPLIERS:
        return (
          <SupplierRow showLoading={loading => this.setState({loading})} />
        );
      case AppConstants.homeItemType.BIDDING:
        return <BiddingRow showLoading={loading => this.setState({loading})} />;
      case AppConstants.homeItemType.TITLE_SUGGEST:
        return <TitleSuggestRow showLoading={loading => this.setState({loading})} />
      case AppConstants.homeItemType.SUGGEST:
        return <SuggestRow itemList={item.data} />;
      case AppConstants.homeItemType.END:
        return <View style={{marginBottom: 10}} />;
      default:
        return null;
    }
  };
  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <UIActivityIndicator color={ColorStyle.tabActive} size={20} />;
  };

  handleLoadMore() {
    if (this.state.loading) return ;
    if (!this.state.canLoadData) return;
    this.setState({loading: true, page: this.state.page + 1}, this.getProductSuggestList);
  }
  getProductSuggestList() {
    let param = {
      page_index: this.state.page,
      page_size: LIMIT,
    };
    ProductHandle.getInstance().getProductList(
      param,
      (isSuccess, responseData) => {
        if (isSuccess) {
          if (responseData.data != null ) {
            if (responseData.data.length < LIMIT) {
              this.setState({canLoadData: false});
            }
            let listData = this.state.data;
            let abc = {
              data: responseData.data.data,
              type: AppConstants.homeItemType.SUGGEST,
            };
            listData = [...listData, ...[abc]];
            this.setState({data: listData, loading: false});
          }
        } else {
          console.log('Failed');
        }
      },
      true,
    );
  }
}

const mapStateToProps = state => {
  return {
    banner: state.bannerReducers,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetBannerInfo: param => {
      dispatch(getBannerInfoAction(param));
    },
    onGetListProduct: param => {
      dispatch(getProductAction(param));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
