import Styles from "../../../../resource/Styles";
import ColorStyle from "../../../../resource/ColorStyle";

const X=Styles.constants.X
export default {
    viewHeader:{
        height: X,
        paddingHorizontal: X/4,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:ColorStyle.tabWhite,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    toolbar:{
        ...Styles.toolbar.toolbar,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: Styles.constants.X * 0.4,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
    }
}
