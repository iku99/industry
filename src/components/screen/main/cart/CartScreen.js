import React, {Component} from 'react';
import {
  AsyncStorage,
  FlatList,
  Image,
  RefreshControl,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import GlobalUtil from '../../../../Utils/Common/GlobalUtil';
import ViewUtils from '../../../../Utils/ViewUtils';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import CartHandle from '../../../../sagas/CartHandle';
import Styles from '../../../../resource/Styles';
import {strings} from '../../../../resource/languages/i18n';
import CartUtils from '../../../../Utils/CartUtils';
import {Actions} from 'react-native-router-flux';
import CartItem from '../../../elements/viewItem/cartItem/CartItem';
import {Icon} from 'react-native-elements';
import ColorStyle from '../../../../resource/ColorStyle';
import CurrencyFormatter from '../../../../Utils/CurrencyFormatter';
import EmptyView from '../../../elements/reminder/EmptyView';
import {getCategoryAction} from '../../../../actions';
import {connect} from 'react-redux';
import {EventRegister} from 'react-native-event-listeners';
import AppConstants from '../../../../resource/AppConstants';
import CheckLogin from '../../../elements/checkLogin';
import RenderLoading from '../../../elements/RenderLoading';
import styles from './styles';
let productSelectId;
let listCartSelect = [];
class CartScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartList: [],
      selectAll:
        this.props.selectAll !== undefined ? this.props.selectAll : false,
      total: 0,
      originalTotal: 0,
      update: true,
      countItem: 0,
      shipping: 20000,
      isLogged: false,
      loading: true,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem(
      AppConstants.SharedPreferencesKey.IsLogin,
      (error, isLogin) => {
        this.setState({isLogged: isLogin === '1'});
      },
    );
    productSelectId = this.props.productSelectId;
    EventRegister.addEventListener(AppConstants.EventName.LIST_CART, total => {
      this.getCart();
    });
    this.getCart();
  }
  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return GlobalUtil.shallowCompareState(this, nextState);
  }
  onRefresh() {
    this.getCart();
  }
  toolBar() {
    return (
      <View style={styles.toolbar}>
        <Text
          style={[
            Styles.text.text20,
            {
              color: ColorStyle.tabWhite,
              fontWeight: '700',
              textAlign: 'center',
            },
          ]}>
          {strings('yourCart')}
        </Text>

        <TouchableOpacity onPress={() => Actions.jump('profile')}>
          <Icon
            name="user"
            type="antdesign"
            size={25}
            color={ColorStyle.tabWhite}
          />
        </TouchableOpacity>
      </View>
    );
  }
  render() {
    return (
      <View style={Styles.container}>
        {this.toolBar()}
        {this.renderCheckLogin()}
        <RenderLoading loading={this.state.loading} />
      </View>
    );
  }
  renderCheckLogin() {
    return (
      <CheckLogin
        status={this.state.isLogged}
        views={
          this.state.cartList.length === 0 && !this.state.loading ? (
            <EmptyView text={strings('notProduct')} />
          ) : (
            this.renderBody()
          )
        }
      />
    );
  }

  keyExtractor = (item, index) => `cart_${index.toString()}`;
  renderBody() {
    return (
      <View style={{flex: 1}}>
        {this._renderHeader()}
        <FlatList
          style={{
            flex: 2,
          }}
          refreshControl={
            <RefreshControl
              // refreshing={this.state.isRefreshing}
              onRefresh={() => this.onRefresh()}
            />
          }
          keyExtractor={this.keyExtractor}
          showsVerticalScrollIndicator={false}
          data={this.state.cartList}
          renderItem={this.renderItem}
        />
        {this.state.countItem > 0 && this._renderBill()}
      </View>
    );
  }
  _renderBill() {
    return (
      <TouchableOpacity
        onPress={() => this.goToCheckout()}
        style={{
          flexDirection: 'row',
          height: 50,
          marginVertical: 20,
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: 25,
          marginHorizontal: 16,
          backgroundColor: ColorStyle.tabActive,
        }}>
        <Text
          style={{
            ...Styles.text.text14,
            color: ColorStyle.tabWhite,
            fontWeight: '700',
          }}>
          {strings('price')}:{CurrencyFormatter(this.state.total)}
        </Text>
      </TouchableOpacity>
    );
  }

  _renderHeader() {
    return (
      <View style={styles.viewHeader}>
        <View style={{width: '33%'}}>
          <Text
            style={{
              ...Styles.text.text14,
              color:
                this.state.countItem !== 0
                  ? ColorStyle.tabActive
                  : ColorStyle.bgButtonFal,
              fontWeight: this.state.countItem !== 0 ? '600' : '400',
            }}>
            {this.state.countItem} {strings('item')}
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            this.setState({selectAll: !this.state.selectAll}, () => {
              this.selectAll();
            });
          }}
          style={{
            width: '33%',
            height: Styles.constants.X,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon
            name={'check'}
            type={'antdesign'}
            size={15}
            color={
              this.state.selectAll
                ? ColorStyle.tabActive
                : ColorStyle.bgButtonFal
            }
          />
          <Text
            style={{
              ...Styles.text.text14,
              color: this.state.selectAll
                ? ColorStyle.tabActive
                : ColorStyle.bgButtonFal,
              fontWeight: this.state.selectAll ? '600' : '400',
            }}>
            {strings('selectAll')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            ViewUtils.showAskAlertDialog(
              strings('removeProductSelect'),
              () => {
                this.DeleteSelect();
              },
              () => {},
            );
          }}
          style={{
            width: '33%',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            height: Styles.constants.X,
          }}>
          <Icon
            name={'delete'}
            type={'antdesign'}
            size={15}
            color={ColorStyle.bgButtonFal}
          />
          <Text style={{...Styles.text.text14, color: ColorStyle.bgButtonFal}}>
            {strings('deleteSelect')}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  DeleteSelect() {
    let data = [];
    let cartList = this.state.cartList;
    cartList.forEach(item => {
      let products = item.products;
      products.forEach(product => {
        if (product.checked) {
          data.push(product.id_order_detail);
        }
      });
    });
    let params = {
      id: data,
    };
    CartHandle.getInstance().removeProductFromCart(params, (isSuccess, res) => {
      if (isSuccess) {
        this.setState({cartList: []}, () => {
          this.getCart();
        });
      }
    });
  }
  renderItem = ({item, index}) => {
    return (
      <CartItem
        key={index}
        cartItem={item}
        parentIndex={index}
        selectAll={this.state.selectAll}
        onProduceRemoved={(storeIndex, productIndex) => {
          this.onRemoveProduct(storeIndex, productIndex);
        }}
        changeSelectCallback={cartItem => {
          let cartList = this.state.cartList;
          cartList[index] = cartItem;
          this.updateCartList(cartList);
        }}
      />
    );
  };
  getCart() {
    CartHandle.getInstance().getCart((isSuccess, dataResponse) => {
      console.log(dataResponse);
      if (isSuccess) {
        if (dataResponse.data.store.length !== 0) {
          let productList = dataResponse.data.store;
          if (productList !== undefined) {
            productList = productList.map(item => {
              let checked = this.state.selectAll;
              let item_product = item.products.map(product => {
                return {
                  ...product,
                  checked: this.state.selectAll,
                };
              });
              return {
                ...item,
                checked: checked,
                products: item_product,
              };
            });
            this.updateCartList(productList);
          }
          return;
        }
        this.updateCartList([]);
      }
    });
  }

  onRemoveProduct(storeIndex, productIndex) {
    let cartList = this.state.cartList;
    let cartItem = cartList[storeIndex];
    let productList = cartItem.products;
    let newProducts = [
      // xoa product trong cart
      ...productList.slice(0, productIndex),
      ...productList.slice(productIndex + 1),
    ];
    if (newProducts.length === 0) {
      //should remove store
      cartList = [
        ...cartList.slice(0, storeIndex),
        ...cartList.slice(storeIndex + 1),
      ];
    } else {
      cartItem.products = newProducts;
      cartList[storeIndex] = cartItem;
    }
    this.setState({cartList: []}, () => {
      setTimeout(() => this.updateCartList(cartList), 100);
    });
  }

  getTotal(cartList) {
    let total = 0;
    let totalProduct = 0;
    let countItem = 0;
    cartList.forEach(item => {
      let products = item.products;
      totalProduct = totalProduct + products.length;
      products.forEach(product => {
        if (product.checked) {
          total = total + product.price * product.quantity;
          countItem = countItem + 1;
        }
      });
    });
    this.setState({total, countItem});
    CartUtils.updateTotal(totalProduct);
  }
  updateCartList(cartList) {
    this.setState({cartList: []}, () => {
      this.setState({cartList: cartList, loading: false});
      this.getTotal(cartList);
    });
  }
  goToCheckout() {
    let productsCart = [];
    if (this.state.cartList.length === 0) {
      ViewUtils.showAskAlertDialog(
        strings('youHaveNotProductInCart'),
        () => {
          Actions.popTo('home');
        },
        null,
      );
      return;
    } else {
      let listCart = this.state.cartList;
      let newItem = {};
      listCart.forEach(item => {
        if (item.checked) {
          let products = item.products;
          newItem = {
            store: {
              address: item.address,
              name: item.name,
              id: item.id,
              avatar: item.avatar,
              ship_unit_id: item.ship_unit_id,
            },
          };
          const mProduct = [];
          let total = 0;
          products.forEach((product, index) => {
            if (product.checked) {
              total = total + product.price * product.quantity;
              mProduct.push(product);
              return;
            }
          });
          newItem = {
            ...newItem,
            products: mProduct,
            total: total,
          };
          productsCart.push(newItem);
        }
      });
      if (productsCart.length === 0) {
        ViewUtils.showAlertDialog(
          strings('pleaseSelectAtLeastOneProductToBuy'),
          null,
        );
      } else {
        NavigationUtils.goToCheckout(productsCart, false);
      }
    }
  }
  selectAll() {
    let cartList = this.state.cartList;
    cartList.forEach((item, mIndex) => {
      item.checked = this.state.selectAll;
      let products = item.products;
      products.forEach((item, index) => {
        let product = item;
        product.checked = this.state.selectAll;
        item = product;
        products[index] = item;
      });
      item.products = products;
      cartList[mIndex] = item;
    });
    this.setState({cartList: []}, () => this.updateCartList(cartList));
  }
}
const mapStateToProps = state => {
  return {
    category: state.categoryReducers,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onGetCategory: param => {
      dispatch(getCategoryAction(param));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CartScreen);
