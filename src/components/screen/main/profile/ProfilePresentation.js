import React, {Component} from 'react';
import {
  AsyncStorage,
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
import ImageHelper from '../../../../resource/images/ImageHelper';
import {strings} from '../../../../resource/languages/i18n';
import {Icon} from 'react-native-elements';
import {Actions} from 'react-native-router-flux';
import {EventRegister} from 'react-native-event-listeners';
import AppConstants from '../../../../resource/AppConstants';
import FuncUtil from '../../../../Utils/FuncUtils';
import GlobalInfo from '../../../../Utils/Common/GlobalInfo';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import StoreHandle from '../../../../sagas/StoreHandle';
import MediaUtils from "../../../../Utils/MediaUtils";
import ViewUtils from "../../../../Utils/ViewUtils";
import AccountHandle from "../../../../sagas/AccountHandle";
const width =
  Styles.constants.widthScreen - Styles.constants.marginHorizontalAll * 2;
class ProfilePresentation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogged: FuncUtil.getInstance().isLogged,
      store: undefined,
      hasStore: undefined,
      avatar: undefined,
      showChangeLanguage: false,
        data:undefined
    };
  }
  componentDidMount() {
      this.setState({data: GlobalInfo.userInfo})
      AsyncStorage.getItem(
          AppConstants.SharedPreferencesKey.IsLogin,
          (error, isLogin) => {
              this.setState({isLogged: isLogin === '1'}, () => {
                  if (this.state.isLogged) {
                      this.getMyStoreInfo();
                  }
              });
          },
      );
    EventRegister.addEventListener(AppConstants.EventName.ADD_NEW_STORE, _ => {
      this.setState({hasStore: true});
    });
    EventRegister.addEventListener(
      AppConstants.EventName.CHANGE_AVATAR,
      data => {
        this.setState({data:{}},()=>{this.setState({data:data})});
      },
    );

  }
  render() {
    if (this.state.isLogged) {
      let name =this.state.data?.full_name;
      let mail =this.state.data?.email;
      return (
        <View style={Styles.container}>
          <View
            style={{
              width: Styles.constants.widthScreen,
              top: 0,
              position: 'absolute',
              backgroundColor: ColorStyle.tabActive,
              height: Styles.constants.widthScreen / 3.5,
              borderBottomRightRadius: 40,
              borderBottomLeftRadius: 40,
            }}
          />
          <View
            style={{
              position: 'absolute',
              width: Styles.constants.widthScreen,
            }}>
            <Text
              style={[
                Styles.text.text20,
                {
                  color: ColorStyle.tabWhite,
                  fontWeight: '700',
                  justifyContent: 'center',
                  alignItems: 'center',
                  textAlign: 'center',
                  paddingVertical: Styles.constants.X
                },
              ]}
              numberOfLines={1}>
              {strings('profile')}
            </Text>
            <TouchableOpacity
              style={{
                position: 'absolute',
                paddingVertical: Styles.constants.X ,
                paddingHorizontal: Styles.constants.X * 0.4,
              }}
              onPress={() => Actions.pop()}>
              <Icon
                name="left"
                type="antdesign"
                size={25}
                color={ColorStyle.tabWhite}
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: Styles.constants.widthScreen,
              height: Styles.constants.widthScreen / 2,
                backgroundColor:'transition',
              top:
                Styles.constants.widthScreen / 2 -
                Styles.constants.widthScreen / 3.5,
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => Actions.jump('personal')}
              style={{
                width:
                  Styles.constants.widthScreen -
                  Styles.constants.marginHorizontalAll * 2,
                backgroundColor: ColorStyle.tabWhite,
                height: Styles.constants.widthScreen / 4,
                borderRadius: 20,
                  shadowColor: 'rgba(0, 0, 0, 0.25)',
                  shadowOffset: { width: 0, height: 1 },
                  shadowOpacity: 0.8,
                  shadowRadius: 1,
                elevation: 10,
                alignItems: 'center',
                justifyContent: 'space-around',
                flexDirection: 'row',
              }}>
              <Image source={MediaUtils.getAvatar(this.state.data)} style={[Styles.icon.icon_avatar]} />
              <View
                style={{
                  width:
                    Styles.constants.widthScreen -
                    Styles.constants.marginHorizontalAll * 2 -
                    Styles.constants.X * 3,
                  height: Styles.constants.widthScreen / 5,
                  justifyContent: 'space-around',
                }}>
                <Text
                  numberOfLines={1}
                  style={[
                    Styles.text.text24,
                    {
                      color: ColorStyle.tabBlack,
                      fontWeight: 'bold',
                      maxWidth:
                        Styles.constants.widthScreen -
                        Styles.constants.marginHorizontalAll * 2 -
                        Styles.constants.X * 3,
                    },
                  ]}>
                  {name}
                </Text>
                <Text
                  numberOfLines={1}
                  style={[
                    Styles.text.text14,
                    {
                      color: ColorStyle.tabBlack,
                      fontWeight: 'bold',
                      maxWidth:
                        Styles.constants.widthScreen -
                        Styles.constants.marginHorizontalAll * 2 -
                        Styles.constants.X * 3,
                    },
                  ]}>
                  {mail}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <ScrollView>
            <View
              style={{
                marginTop: 20,
                paddingVertical: 16,
                paddingHorizontal: Styles.constants.marginHorizontalAll,
                backgroundColor: ColorStyle.tabWhite,
                borderColor: ColorStyle.borderItemHome,
                elevation: 2,
                  shadowColor: ColorStyle.borderItemHome,
                  shadowOffset: { width: 0, height: 3 },
                  shadowOpacity: 0.8,
                  shadowRadius: 1,
                marginBottom: 8,
              }}>
              <Text
                style={[
                  Styles.text.text14,
                  {fontWeight: '500', marginBottom: 25},
                ]}>
                {strings('followOrder')}
              </Text>
              <View style={{flexDirection: 'row'}}>
                {this.renderDeliveryStatus(
                  strings('to_pay'),
                  ImageHelper.icon_all,
                  () => NavigationUtils.goToOrderList(0),
                )}
                {this.renderDeliveryStatus(
                  strings('confirmed'),
                  ImageHelper.icon_confirmed,
                  () => NavigationUtils.goToOrderList(2),
                )}
                {this.renderDeliveryStatus(
                  strings('delivering'),
                  ImageHelper.icon_delivering,
                  () => NavigationUtils.goToOrderList(3),
                )}
                {this.renderDeliveryStatus(
                  strings('userCancel'),
                  ImageHelper.icon_cancelled,
                  () => NavigationUtils.goToOrderList(4),
                )}
                {this.renderDeliveryStatus(
                  strings('success'),
                  ImageHelper.icon_success,
                  () => NavigationUtils.goToOrderList(6),
                )}
              </View>
            </View>
            <View
              style={{
                width: Styles.constants.widthScreen,
                paddingHorizontal: Styles.constants.marginHorizontalAll,
                backgroundColor: ColorStyle.tabWhite,
                borderColor: ColorStyle.borderItemHome,
                elevation: 2,
                  shadowColor: ColorStyle.borderItemHome,
                  shadowOffset: { width: 0, height: 3 },
                  shadowOpacity: 0.8,
                  shadowRadius: 1,
                marginBottom: 8,
              }}>
              {this.renderOptionItem(
                strings('order'),
                '#FE9870',
                ImageHelper.icon_cart,
                () => Actions.jump('order'),
              )}
              {this.renderOptionItem(
                strings('address'),
                '#6266F9',
                ImageHelper.icon_house,
                () => Actions.jump('address'),
              )}
              {/*{this.renderOptionItem(*/}
              {/*  strings('payment'),*/}
              {/*  '#0EAD69',*/}
              {/*  ImageHelper.icon_dollar,*/}
              {/*  () => Actions.jump('payment'),*/}
              {/*)}*/}
            </View>
            <View
              style={{
                width: Styles.constants.widthScreen,
                paddingHorizontal: Styles.constants.marginHorizontalAll,
                backgroundColor: ColorStyle.tabWhite,
                borderColor: ColorStyle.borderItemHome,
                elevation: 2,
                marginBottom: 8,
              }}>
              {this.renderOptionItem(
                strings('reviews'),
                '#FF9417',
                ImageHelper.icon_review,
                () => Actions.jump('productReview'),
              )}
              {this.renderOptionItem(
                strings(this.state.hasStore?'store':'saleRegistration'),
                '#62BAF9',
                ImageHelper.icon_shops,
                () => {
                  this.state.hasStore
                    ? Actions.jump('sellerDetail')
                    : Actions.jump('flashSellerScreen');
                },
              )}
              {this.renderOptionItem(
                strings('group'),
                '#F96298',
                ImageHelper.icon_groups,
                () => Actions.jump('groups'),
              )}
            </View>
            <View
              style={{
                width: Styles.constants.widthScreen,
                paddingHorizontal: Styles.constants.marginHorizontalAll,
                backgroundColor: ColorStyle.tabWhite,
                borderColor: ColorStyle.borderItemHome,
                elevation: 2,
                marginBottom: 8,
                  shadowColor: ColorStyle.borderItemHome,
                  shadowOffset: { width: 0, height: 3 },
                  shadowOpacity: 0.8,
                  shadowRadius: 1,
              }}>
              {this.renderOptionItem(
                strings('setting'),
                '#3B5998',
                ImageHelper.icon_setting,
                () => Actions.jump('setting'),
              )}
              {/*{this.renderOptionItem(*/}
              {/*  strings('held'),*/}
              {/*  '#2E294E',*/}
              {/*  ImageHelper.icon_help,*/}
              {/*  () => NavigationUtils.goToHelpScreen(),*/}
              {/*)}*/}
                <TouchableOpacity
                    onPress={()=>{
                        ViewUtils.showAskAlertDialog(
                            strings('areYouLogout'),
                            () => {
                                AccountHandle.getInstance().logout();
                            },
                            undefined,
                        )

                    }}
                    style={{
                        width: width,
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginVertical: 16,
                    }}>
                    <View
                        style={{
                            flex: 2,
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}>
                        <View
                            style={{
                                padding: 10,
                                backgroundColor: '#202f8b',
                                borderRadius: 100,
                                alignItems: 'center',
                            }}>
                            <Icon name={'log-in-outline'} type={'ionicon'} size={20} color={ColorStyle.tabWhite}/>
                        </View>
                        <Text
                            style={[
                                Styles.text.text14,
                                {
                                    fontWeight: '500',
                                    marginLeft: Styles.constants.X * 0.5,
                                    maxWidth: '70%',
                                },
                            ]}>
                            {strings('logout')}
                        </Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'flex-end'}}>
                        <Icon
                            name="chevron-right"
                            type="feather"
                            size={25}
                            color={ColorStyle.gray}
                        />
                    </View>
                </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      );
    }
    return (
        <View style={Styles.container}>
          <View
            style={{
              width: Styles.constants.widthScreen,
              backgroundColor: ColorStyle.tabActive,
              height: Styles.constants.widthScreen / 3,
              borderBottomRightRadius: 40,
              borderBottomLeftRadius: 40,
              paddingRight: 20,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>

            <TouchableOpacity
              onPress={() => Actions.jump('login')}
              style={{
                paddingHorizontal: 10,
                paddingVertical: 5,
                borderColor: ColorStyle.tabWhite,
                borderWidth: 1,
                marginRight: 10,
              }}>
              <Text style={[Styles.text.text18, {fontWeight: '500'}]}>
                {strings('signIn')}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => Actions.jump('register')}
              style={{
                paddingHorizontal: 10,
                paddingVertical: 5,
                borderColor: ColorStyle.tabWhite,
                borderWidth: 1,
                marginRight: 10,
              }}>
              <Text style={[Styles.text.text18, {fontWeight: '500'}]}>
                {strings('signUp')}
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={{
              position: 'absolute',
              paddingVertical: Styles.constants.X * 1.2,
              paddingHorizontal: Styles.constants.X * 0.4,
            }}
            onPress={() => Actions.pop()}>
            <Icon
              name="left"
              type="antdesign"
              size={25}
              color={ColorStyle.tabWhite}
            />
          </TouchableOpacity>
          <ScrollView>
            <View
              style={{
                marginTop: 20,
                paddingVertical: 16,
                paddingHorizontal: Styles.constants.marginHorizontalAll,
                backgroundColor: ColorStyle.tabWhite,
                borderColor: ColorStyle.borderItemHome,
                elevation: 2,
                marginBottom: 8,
              }}>
              <Text
                style={[
                  Styles.text.text14,
                  {fontWeight: '500', marginBottom: 25},
                ]}>
                {strings('followOrder')}
              </Text>
              <View style={{flexDirection: 'row'}}>
                {this.renderDeliveryStatus(
                  strings('all'),
                  ImageHelper.icon_all,
                  () => Actions.jump('login'),
                )}
                {this.renderDeliveryStatus(
                  strings('confirmed'),
                  ImageHelper.icon_confirmed,
                  () => Actions.jump('login'),
                )}
                {this.renderDeliveryStatus(
                  strings('delivering'),
                  ImageHelper.icon_delivering,
                  () => Actions.jump('login'),
                )}
                {this.renderDeliveryStatus(
                  strings('userCancel'),
                  ImageHelper.icon_cancelled,
                  () => Actions.jump('login'),
                )}
                {this.renderDeliveryStatus(
                  strings('success'),
                  ImageHelper.icon_success,
                  () => Actions.jump('login'),
                )}
              </View>
            </View>
            <View
              style={{
                width: Styles.constants.widthScreen,
                paddingHorizontal: Styles.constants.marginHorizontalAll,
                backgroundColor: ColorStyle.tabWhite,
                borderColor: ColorStyle.borderItemHome,
                elevation: 2,
                marginBottom: 8,
              }}>
              {this.renderOptionItem(
                strings('order'),
                '#FE9870',
                ImageHelper.icon_cart,
                () => Actions.jump('login'),
              )}
              {this.renderOptionItem(
                strings('address'),
                '#6266F9',
                ImageHelper.icon_house,
                () => Actions.jump('login'),
              )}
              {this.renderOptionItem(
                strings('payment'),
                '#0EAD69',
                ImageHelper.icon_dollar,
                () => Actions.jump('login'),
              )}
            </View>
            <View
              style={{
                width: Styles.constants.widthScreen,
                paddingHorizontal: Styles.constants.marginHorizontalAll,
                backgroundColor: ColorStyle.tabWhite,
                borderColor: ColorStyle.borderItemHome,
                elevation: 2,
                marginBottom: 8,
              }}>
              {this.renderOptionItem(
                strings('reviews'),
                '#FF9417',
                ImageHelper.icon_review,
                () => Actions.jump('login'),
              )}
              {this.renderOptionItem(
                strings('saleRegistration'),
                '#62BAF9',
                ImageHelper.icon_shops,
                () => {Actions.jump('login');
                },
              )}
              {this.renderOptionItem(
                strings('group'),
                '#F96298',
                ImageHelper.icon_groups,
                () => Actions.jump('login'),
              )}
            </View>
            <View
              style={{
                width: Styles.constants.widthScreen,
                paddingHorizontal: Styles.constants.marginHorizontalAll,
                backgroundColor: ColorStyle.tabWhite,
                borderColor: ColorStyle.borderItemHome,
                elevation: 2,
                marginBottom: 8,
              }}>
              {this.renderOptionItem(
                strings('setting'),
                '#3B5998',
                ImageHelper.icon_setting,
                () => Actions.jump('setting'),
              )}
              {/*{this.renderOptionItem(*/}
              {/*  strings('held'),*/}
              {/*  '#2E294E',*/}
              {/*  ImageHelper.icon_help,*/}
              {/*  () => NavigationUtils.goToHelpScreen(),*/}
              {/*)}*/}

            </View>
          </ScrollView>
        </View>
    );
  }
  renderDeliveryStatus(title, icon, onPress) {
    return (
      <TouchableOpacity
        style={{
          alignItems: 'center',
          width: Styles.constants.widthIcon,
          justifyContent: 'space-between',
          marginHorizontal: Styles.constants.X * 0.1,
        }}
        onPress={onPress}>
        <Image source={icon} style={Styles.icon.iconOrder} />
        <Text style={{...Styles.text.text11,textAlign:'center'}}>{title}</Text>
      </TouchableOpacity>
    );
  }
  renderOptionItem(title, color, icon, onPress) {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={{
          width: width,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginVertical: 16,
        }}>
        <View
          style={{
            flex: 2,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View
            style={{
              padding: 10,
              backgroundColor: color,
              borderRadius: 100,
              alignItems: 'center',
            }}>
            <Image source={icon} style={Styles.icon.iconProfile} />
          </View>
          <Text
            style={[
              Styles.text.text14,
              {
                fontWeight: '500',
                marginLeft: Styles.constants.X * 0.5,
                maxWidth: '70%',
              },
            ]}>
            {title}
          </Text>
        </View>
        <View style={{flex: 1, alignItems: 'flex-end'}}>
          <Icon
            name="chevron-right"
            type="feather"
            size={25}
            color={ColorStyle.gray}
          />
        </View>
      </TouchableOpacity>
    );
  }
  async getMyStoreInfo() {
    StoreHandle.getInstance().getMyStore(
      (isSuccess, responseData) => {
        if (isSuccess && responseData !== undefined) {
            console.log('responseData:',responseData)
          let store;
          if (responseData.data != null) {
            store = responseData.data;
            this.setState({store, hasStore: true});
            return;
          }
        }
        this.setState({hasStore: false});
      },
    );
  }
}
//lấy dữ liệu về
const mapStateToProps = state => {
  return {};
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {
    onFetchMovies: () => {},
  };
};
const ProfileScreen = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfilePresentation);
export default ProfileScreen;
