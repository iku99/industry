import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../../../resource/Styles';
import Toolbar from '../../../../elements/toolbar/Toolbar';
import {strings} from '../../../../../resource/languages/i18n';
import ColorStyle from '../../../../../resource/ColorStyle';
import ViewUtils from '../../../../../Utils/ViewUtils';
import AccountHandle from '../../../../../sagas/AccountHandle';
import {Icon} from 'react-native-elements';
import {Actions} from "react-native-router-flux";
class SettingScreen extends Component {
  render() {
    return (
      <View style={Styles.container}>
        <Toolbar
          title={strings('setting')}
          backgroundColor={ColorStyle.tabActive}
          textColor={ColorStyle.tabWhite}
        />
       <View style={{marginVertical:10,backgroundColor:ColorStyle.tabWhite}}>
         {this.renderItem(strings('aboutVietChemDes'),()=>{Actions.jump('aboutVietChem')})}
         {this.renderItem(strings('policyTitle'),()=>{Actions.jump('policyApp')})}
         {this.renderItem(strings('customerServiceTitle'),()=>{Actions.jump('customerService')})}
       </View>
      </View>
    );
  }
  renderItem(title, callback) {
    return (
      <TouchableOpacity style={{alignItems:'center',justifyContent:'space-between',flexDirection:'row',borderBottomColor:'#DADADA',borderBottomWidth:1,paddingVertical:15,paddingHorizontal:Styles.constants.marginHorizontalAll}} onPress={callback}>
        <Text style={{...Styles.text.text14,color:ColorStyle.tabBlack,fontWeight:'500'}}>{title}</Text>
        <Icon
          name={'right'}
          type={'antdesign'}
          size={20}
          color={ColorStyle.gray}
        />
      </TouchableOpacity>
    );
  }
}
const mapStateToProps = state => {
  return {
    //gọi đến reducers
    category: state.categoryReducers,
  };
};

//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(SettingScreen);
