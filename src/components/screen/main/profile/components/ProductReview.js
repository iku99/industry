import React, {Component} from 'react';
import {connect} from 'react-redux';
import {FlatList, Text, View} from "react-native";
import Styles from "../../../../../resource/Styles";
import Toolbar from "../../../../elements/toolbar/Toolbar";
import { strings } from "../../../../../resource/languages/i18n";
import ColorStyle from "../../../../../resource/ColorStyle";
import RatingHandle from "../../../../../sagas/RatingHandle";
import EmptyView from "../../../../elements/reminder/EmptyView";
import {UIActivityIndicator} from "react-native-indicators";
import MyFastImage from "../../../../elements/MyFastImage";
import ProductUtils from "../../../../../Utils/ProductUtils";
import RatingStar from "../../../../RatingStar";
const LIMIT=10
class ProductReview extends Component {
  constructor(props) {
    super(props);
    this.state={
      data:[],
      page:1,
      canLoadData: true,
      loading: false,
    }
  }
  componentDidMount() {
    this.getRatingUser()
  }

  getRatingUser() {
    let params = {
      page_index:this.state.page,
      page_size:LIMIT,
    };
    RatingHandle.getInstance().getRatingUser(
        params,
        (isSuccess, dataResponse) => {
          if (isSuccess) {
            let newData = [];
            let data=this.state.data
            if (dataResponse.data !== undefined) {
              newData = dataResponse.data.data;
              if(newData<LIMIT) return this.setState({canLoadData:true})
              data = data.concat(newData);
              this.setState({loading: false,data:data})
            }
          }
        },
    );
  }
  renderItem =({item,index})=>{
    console.log(item)
    return(
        <View style={{margin:10,backgroundColor:ColorStyle.tabWhite,padding:5,borderRadius:10}}>
           <View style={{flexDirection: 'row', justifyContent: 'space-between',marginVertical:10}}>
             <MyFastImage
                 style={{flex: 1, height: 70}}
                 source={{
                   uri: ProductUtils.getImages(item.product)[0],
                   headers: {Authorization: 'someAuthToken'},
                 }}
                 resizeMode={'cover'}
             />
             <View style={{flex: 3}}>
               <Text
                   style={[
                     Styles.text.text14,
                     {
                       fontWeight: '500',
                       marginHorizontal: 20,
                     },
                   ]}>
                 {item.product.name}
               </Text>
             </View>
           </View>

          <View style={{flexDirection: 'row', justifyContent: 'flex-start',alignItems:'center',marginTop:10}}>
            <Text style={{...Styles.text.text14,color:ColorStyle.tabBlack,fontWeight:'600',marginRight:10}}>{strings('rate')}:</Text>
            <RatingStar size={20} rating={item.rate} />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'flex-start',marginTop:10}}>
            <Text style={{...Styles.text.text14,color:ColorStyle.tabBlack,fontWeight:'600',marginRight:10}}>{strings('content1')}</Text>
            <Text style={{...Styles.text.text14,color:ColorStyle.gray,fontWeight:'600'}}>{item.des}</Text>
          </View>
        </View>
    )
  }

  keyExtractor = (item, index) => `list_rating_${index.toString()}`;
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) {
      return null;
    }
    return <UIActivityIndicator color={ColorStyle.tabActive} size={20} />;
  };
  handleLoadMore() {
    if (this.state.loading) {
      return;
    }
    if (!this.state.canLoadData) {
      return;
    }
    this.setState({loading: true, page: this.state.page + 1}, () =>
        this.getRatingUser,
    );
  }
  render() {
    return (
      <View style={Styles.container}>
        <Toolbar
          title={strings('reviews')}
          backgroundColor={ColorStyle.tabActive}
          textColor={ColorStyle.tabWhite}
        />
        <FlatList data={this.state.data} renderItem={this.renderItem}/>
        {this.state.data.length === 0 && (<EmptyView
            containerStyle={{flex: 1}}
            text={strings('notRating')}
        />)}
        {this.state.data.length > 0 && (
            <FlatList
                contentInset={{right: 0, top: 0, left: 0, bottom: 0}}
                showsVerticalScrollIndicator={false}
                data={this.state.products}
                keyExtractor={this.keyExtractor}
                renderItem={this._renderItem}
                ListFooterComponent={this.renderFooter.bind(this)}
                style={{flex: 1, height: '100%', backgroundColor: 'transparent'}}
                onEndReachedThreshold={0.4}
                onEndReached={() => this.handleLoadMore()}
            />
        )}
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    //gọi đến reducers
    category: state.categoryReducers,
  };
};

//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductReview);
