import React, {Component} from "react";
import {Text, View} from "react-native";
import Styles from "../../../../../resource/Styles";
import Toolbar from "../../../../elements/toolbar/Toolbar";
import {strings} from "../../../../../resource/languages/i18n";
import ColorStyle from "../../../../../resource/ColorStyle";
import ElevatedView from "react-native-elevated-view";

export default class CustomerService extends Component{

    render() {
        return(
            <View style={Styles.container}>
                <Toolbar
                    title={strings('customerServiceTitle')}
                    backgroundColor={ColorStyle.tabActive}
                    textColor={ColorStyle.tabWhite}
                />
                <ElevatedView
                    elevation={1}
                    style={{
                        alignSelf:'center',
                        marginBottom: 10,
                        paddingHorizontal: '2%',
                    }}>
                    <Text
                        style={{
                            fontSize:20,
                            fontWeight:'600',
                            color:ColorStyle.tabBlack,
                            paddingHorizontal: 10,
                            paddingTop: 20,
                        }}>
                        {strings('VietChemCompany')}
                    </Text>
                    <Text
                        style={{
                            fontSize:15,
                            color:ColorStyle.tabBlack,
                            paddingHorizontal: 20,
                            lineHeight: 25,
                            paddingVertical: 15,
                        }}>
                        {strings('VietChemDes')}
                    </Text>
                </ElevatedView>
            </View>
        )
    }
}
