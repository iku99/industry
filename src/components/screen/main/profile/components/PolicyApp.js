import React, {Component} from "react";
import {ScrollView, Text, View} from "react-native";
import Styles from "../../../../../resource/Styles";
import Toolbar from "../../../../elements/toolbar/Toolbar";
import {strings} from "../../../../../resource/languages/i18n";
import ColorStyle from "../../../../../resource/ColorStyle";
import SimpleToast from "react-native-simple-toast";
import ElevatedView from "react-native-elevated-view";
import Ripple from "react-native-material-ripple";
import NavigationUtils from "../../../../../Utils/NavigationUtils";
import constants from "../../../../../Api/constants";
import Accordion from "react-native-collapsible/Accordion";
import {Icon} from "react-native-elements";
let policyOptionList = [];
export default class PolicyApp extends Component{
    constructor(props) {
        super(props);
        this.state = {
            activeAboutSections: [0],
            activePolicySections: [0],
            activeCustomerSections: [0],
        };
        policyOptionList = [
            {
                text: strings('policyChinhSach'),
                onPress: () => {
                    SimpleToast.show('123');
                },
                staticUrl: '/static-pages/chinh-sach-bao-mat',
            },
            {
                text: strings('policyQuyCheHD'),
                onPress: () => {
                    SimpleToast.show('123');
                },
                staticUrl: '/static-pages/quy-che-hoat-dong-cua-hoi-nhom',
            },
            {
                text: strings('policyChinhSachVanChuyen'),
                onPress: () => {
                    SimpleToast.show('123');
                },
                staticUrl: '/static-pages/chinh-sach-van-chuyen',
            },
            {
                text: strings('policyBaoHanh'),
                onPress: () => {
                    SimpleToast.show('123');
                },
                staticUrl: '/static-pages/chinh-sach-bao-hanh',
            },
            {
                text: strings('policyThanhToan'),
                onPress: () => {
                    SimpleToast.show('123');
                },
                staticUrl: '/static-pages/quy-dinh-va-hinh-thuc-thanh-toan',
            },
            {
                text: strings('policyDoiTra'),
                onPress: () => {
                    SimpleToast.show('123');
                },
                staticUrl: '/static-pages/chinh-sach-doi-tra-hoan-tien',
            },
            {
                text: strings('policyViPham'),
                onPress: () => {
                    SimpleToast.show('123');
                },
                staticUrl: '/static-pages/chinh-sach-xu-ly-hanh-vi-vi-pham',
            },
        ];
    }
    render() {
        return(
            <View style={Styles.container}>
                <Toolbar
                    title={strings('policyTitle')}
                    backgroundColor={ColorStyle.tabActive}
                    textColor={ColorStyle.tabWhite}
                />
                {this.renderOptionView(
                    strings('policyTitle'),
                    policyOptionList,
                    'activePolicySections',
                )}

            </View>
        )
    }
    renderOptionView(title, optionList, activeSections) {
        let optionView = optionList.map(item => {
            return (
                <Ripple
                    style={{marginTop:10}}
                    onPress={() => {
                        NavigationUtils.goToStaticWenView(constants.host + item.staticUrl);
                    }}>
                    <Text style={{color:ColorStyle.tabBlack,fontWeight:'600',fontSize:16}}>-{item.text}<Text style={{color:ColorStyle.tabBlack,fontWeight:'400'}}>{item.des}</Text></Text>
                </Ripple>
            );
        });
        return (
            <ElevatedView
                elevation={1}
                style={{
                    marginBottom: 10,
                    marginTop:10,
                    paddingHorizontal: '2%',
                    width: '100%',
                    height:'100%',

                }}>
                {optionView}
            </ElevatedView>
        );
    }
}
