import React, {Component} from 'react';
import {FlatList, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../../../resource/Styles';
import ColorStyle from '../../../../../resource/ColorStyle';
import Toolbar from '../../../../elements/toolbar/Toolbar';
import {strings} from '../../../../../resource/languages/i18n';
import {Icon} from 'react-native-elements';
const Data = [
  {
    icon: 'dollar-bill',
    type: 'foundation',
    backgroundColor: '#FD576C',
    title: 'Payment in cash on delivery',
  },
  {
    icon: 'atm',
    type: 'materialcommunityicons',
    backgroundColor: '#6266F9',
    title: 'Payment by domestic ATM card',
  },
  {
    icon: 'cc-visa',
    type: 'fontawesome5brands',
    backgroundColor: '#5BE2FF',
    title: 'Payment by international card',
  },
];
class Payment extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View
        style={[Styles.container, {backgroundColor: ColorStyle.colorPersonal}]}>
        <Toolbar
          title={strings('payment')}
          backgroundColor={ColorStyle.colorPersonal}
          status={this.state.statusEdit}
          onPress={() => this.setState({statusEdit: !this.state.statusEdit})}
        />
        <FlatList
          data={Data}
          renderItem={this.renderItem}
          style={{marginHorizontal: 24}}
        />
      </View>
    );
  }
  renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          backgroundColor: ColorStyle.tabWhite,
          paddingVertical: 10,
          marginVertical: 8,
          paddingHorizontal: 17,
          alignItems: 'center',
          borderRadius: 12,
        }}>
        <View
          style={{
            backgroundColor: item.backgroundColor,
            width: 50,
            height: 50,
            borderRadius: 100,
            alignItems: 'center',
            justifyContent: 'center',
            marginRight: 20,
          }}>
          <Icon
            name={item.icon}
            type={item.type}
            size={25}
            color={ColorStyle.tabWhite}
          />
        </View>
        <Text
          numberOfLines={1}
          style={[Styles.text.text14, {fontWeight: '500'}]}>
          {item.title}
        </Text>
      </TouchableOpacity>
    );
  };
}
//lấy dữ liệu về
const mapStateToProps = state => {
  return {};
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {
    onFetchMovies: () => {},
  };
};
const PaymentScreen = connect(mapStateToProps, mapDispatchToProps)(Payment);
export default PaymentScreen;
