import React, {Component} from 'react';
import {FlatList, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../../../../resource/Styles';
import ColorStyle from '../../../../../../resource/ColorStyle';
import Toolbar from '../../../../../elements/toolbar/Toolbar';
import {strings} from '../../../../../../resource/languages/i18n';
import AppConstants from '../../../../../../resource/AppConstants';
import ViewUtils from '../../../../../../Utils/ViewUtils';
import AddressHandle from '../../../../../../sagas/AddressHandle';
import NavigationUtils from '../../../../../../Utils/NavigationUtils';
import {Icon} from 'react-native-elements';
import {DotIndicator} from 'react-native-indicators';
import {Actions} from 'react-native-router-flux';
class Address extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading: true,
            refreshing: false,
        };
    }
    componentDidMount() {
        this.getAddressList();
    }

    render() {
        return (
            <View
                style={[Styles.container, {backgroundColor: ColorStyle.colorPersonal}]}>
                <Toolbar
                    title={strings('address')}
                    backgroundColor={ColorStyle.tabActive}
                    textColor={ColorStyle.tabWhite}
                    add={true}
                    onPressAdd={() =>
                        NavigationUtils.goToAddressModify(undefined, (createNew, item) => {
                            this.getAddressList();
                        })
                    }
                />
                <FlatList
                    data={this.state.data}
                    showsVerticalScrollIndicator={false}
                    renderItem={this.renderItem}
                    style={{marginHorizontal: Styles.constants.marginHorizontalAll}}
                    ListFooterComponent={this.renderFooter.bind(this)}
                    ref={ref => {
                        this.listRef = ref;
                    }}
                    refreshing={this.state.refreshing}
                />
            </View>
        );
    }
    renderFooter = () => {
        if (!this.state.loading) {
            return null;
        }
        return <DotIndicator color={ColorStyle.tabActive} size={10} />;
    };
    renderItem = ({item, index}) => {
        return (
            <TouchableOpacity
                onPress={
                    () =>
                        this.props.type
                            ? (this.props.onCallback(item), Actions.pop())
                            : this.onEdit(item)
                }
                style={{
                    marginTop: 20,
                    backgroundColor: ColorStyle.tabWhite,
                    paddingVertical: 16,
                    paddingHorizontal: 20,
                    borderColor:ColorStyle.borderItemHome,
                    shadowColor: ColorStyle.borderItemHome,
                    shadowOffset: { width: 0, height: 2},
                    shadowOpacity: 0.8,
                    shadowRadius: 1,
                    elevation:2
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                    }}>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabBlack,
                            fontWeight: '500',
                        }}>
                        {item.address_category ? strings('company') : strings('home')}
                    </Text>
                    <TouchableOpacity onPress={() => this.onEdit(item)}>
                        <Icon
                            name={'pencil'}
                            type="foundation"
                            size={15}
                            color={ColorStyle.tabActive}
                        />
                    </TouchableOpacity>
                </View>

                <Text style={{...Styles.text.text14, color: ColorStyle.tabBlack,marginVertical:5}}>
                    {item.name} | {item.phone}
                </Text>
                <Text
                    style={{...Styles.text.text14, color: ColorStyle.gray ,lineHeight:20,marginBottom:10}}>
                    {item.address_detail?.address}
                </Text>
                <Text
                    style={{
                        ...Styles.text.text14,
                        color: ColorStyle.tabActive,
                        fontWeight: '400',
                        position:'absolute',
                        bottom:5,
                        right:5,
                        alignSelf: 'flex-end',
                        display: item.default_address===AppConstants.ADDRESS_DEFAULT.DEFAULT ? 'flex' : 'none',
                    }}>
                    {strings('statusAddress')}
                </Text>
            </TouchableOpacity>
        );
    };
    getAddressList() {
        AddressHandle.getInstance().getAddressList((isSuccess, dataResponse) => {
            if (
                isSuccess &&
                dataResponse.data !== undefined
            ) {
                let mData = dataResponse.data;
                this.setState({data: mData, loading: false});
            }
        });
    }

    onEdit(item) {
        NavigationUtils.goToAddressModify(item, (createNew, item) => {
            this.getAddressList();
        },()=>this.getAddressList());
    }

}
//lấy dữ liệu về
const mapStateToProps = state => {
    return {};
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
    return {
        onFetchMovies: () => {},
    };
};
const AddressScreen = connect(mapStateToProps, mapDispatchToProps)(Address);
export default AddressScreen;
