import React, {Component} from 'react';
import {
    FlatList,
    LogBox,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from 'react-native';
import Styles from '../../../../../../resource/Styles';
import ColorStyle from '../../../../../../resource/ColorStyle';
import Toolbar from '../../../../../elements/toolbar/Toolbar';
import {strings} from '../../../../../../resource/languages/i18n';
import DataUtils from '../../../../../../Utils/DataUtils';
import LocationHandle from '../../../../../../sagas/LocationHandle';
import AppAutoComplete from '../../../../../elements/AppAutoComplete';
import SimpleToast from 'react-native-simple-toast';
import AddressHandle from '../../../../../../sagas/AddressHandle';
import {Actions} from 'react-native-router-flux';
import ViewUtils from '../../../../../../Utils/ViewUtils';
import AppCheckBox from '../../../../../elements/AppCheckBox';
import AppConstants from '../../../../../../resource/AppConstants';
let searchAddressId = -1;
const Data=[
    {
        type:1,
    },
    {
        type:2,
    },
    {
        type:3,
    },
    {
        type:4,
    }

]
export default class AddressModify extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fullName: '',
            phone: '',
            address: '',
            searchAddress: '',
            validName: false,
            validPhone: false,
            provinces: [],
            districts: [],
            wards: [],
            provinceId: undefined,
            districtId: undefined,
            wardId: undefined,
            validAdd: false,
            defaultAddress: false,
            companyAddress: false,
            addressId: undefined,
            province: undefined,
            district: undefined,
            ward: undefined,
        };
    }

    componentDidMount() {
        this.getProvinces();
        let addressItem = this.props.addressItem;
        if (addressItem !== undefined) {
            console.log('address_detail:',addressItem.address_detail)
            this.setState(
                {
                    addressId: addressItem.id,
                    fullName: addressItem.name,
                    phone: addressItem.phone,
                    address: addressItem?.unit,
                    province: {name: addressItem.address_detail.city.name},
                    district: {name: addressItem.address_detail.district.name},
                    ward: {name: addressItem.address_detail.ward.name},
                    defaultAddress:
                        addressItem.default_address === AppConstants.ADDRESS_DEFAULT.DEFAULT
                            ? true
                            : false,
                    companyAddress:
                        addressItem.address_category === AppConstants.ADDRESS_CATEGORY.HOME
                            ? true
                            : false,
                },
                () => {
                    this.checkName();
                    this.checkPhone();
                    this.checkAddress();
                    // this.onChangeProvince();
                },
            );
        }

        // });
    }

    render() {
        return (
            <View
                style={{
                    ...Styles.container,
                    backgroundColor: ColorStyle.colorPersonal,
                }}>
                <Toolbar
                    title={
                        this.state.addressId === undefined
                            ? strings('newAddress')
                            : strings('editAddress')
                    }
                    backgroundColor={ColorStyle.tabActive}
                    textColor={ColorStyle.tabWhite}
                />
                <FlatList showsVerticalScrollIndicator={false}
                          keyExtractor={this.keyExtractor}
                          data={Data}
                          renderItem={this.renderItem}
                          style={{marginHorizontal: Styles.constants.marginHorizontalAll}}/>


            </View>
        );
    }
    renderItem=({item,index})=>{
        switch (item.type) {
            case 1: return this.renderContact()
            case 2: return this.renderAddress()
            case 3: return this.renderCheckBox()
            case 4: return this.renderBottom()
        }
    }
    renderBottom(){
        return(
            <View>
                <TouchableOpacity
                    onPress={() => this.onDelete(this.props.addressItem.id)}
                    style={[
                        Styles.button.buttonLogin,
                        {
                            width:'100%',
                            marginHorizontal: 0,
                            marginTop: Styles.constants.X * 0.5,
                            alignItems: 'center',
                            borderRadius: 35,
                            borderColor: '#BABABA',
                            borderWidth: 1,
                            backgroundColor: ColorStyle.tabWhite,
                            display: this.state.addressId === undefined ? 'none' : 'flex',
                        },
                    ]}>
                    <Text
                        style={[
                            Styles.text.text14,
                            {color: ColorStyle.tabActive, fontWeight: '500'},
                        ]}>
                        {strings('delete').toUpperCase() + ' ' + strings('address1').toUpperCase()}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => this.onSave()}
                    style={[
                        Styles.button.buttonLogin,
                        {
                            width:'100%',
                            marginHorizontal: 0,
                            backgroundColor: ColorStyle.tabActive,
                            borderRadius: 35,
                            marginVertical: Styles.constants.X * 0.5,
                        },
                    ]}>
                    <Text
                        style={[
                            Styles.text.text14,
                            {color: ColorStyle.tabWhite, fontWeight: '500'},
                        ]}>
                        {strings('save').toUpperCase()}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
    keyExtractor = (item, index) => `addAddress_${index.toString()}`;
    renderCheckBox(){
        return(
            <View>
                <View>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabBlack,
                            fontWeight: '500',
                            marginTop: 12}}>{strings('address1') + ' ' + strings('category')}</Text>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <AppCheckBox
                            checked={this.state.companyAddress}
                            text={strings('house')}
                            isSquareType={true}
                            onChange={checked => {
                                this.changeIsCompanyAdd(checked);
                            }}
                        />
                        <AppCheckBox
                            checked={!this.state.companyAddress}
                            text={strings('company')}
                            isSquareType={true}
                            onChange={checked => {
                                this.changeIsCompanyAdd(!checked);
                            }}
                        />
                    </View>
                </View>
                <View>
                    <Text
                        style={{
                            ...Styles.text.text14,
                            color: ColorStyle.tabBlack,
                            fontWeight: '500',
                            marginTop: 12,
                        }}>
                        {strings('textSetAddress')}
                    </Text>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <AppCheckBox
                            checked={this.state.defaultAddress}
                            text={strings('on')}
                            isSquareType={true}
                            onChange={checked => {
                                this.changeDefaultAddress(checked);
                            }}
                        />
                        <AppCheckBox
                            checked={!this.state.defaultAddress}
                            text={strings('off')}
                            isSquareType={true}
                            onChange={checked => {
                                this.changeDefaultAddress(!checked);
                            }}
                        />
                    </View>
                </View>
            </View>
        )
    }
    renderAddress(){
        return(
            <View>
                <Text
                    style={{
                        ...Styles.text.text14,
                        fontWeight: '500',
                        color: 'rgba(0, 0, 0, 0.6)',
                        marginTop: 16,
                    }}>
                    {strings('address1')}
                </Text>

                {this.state.provinces.length > 0 && (
                    <AppAutoComplete
                        dataList={this.state.provinces}
                        alwaysShowOnTop={true}
                        placeholder={strings('province')}
                        defaultValue={
                            this.state.province !== undefined
                                ? this.state.province?.name
                                : ''
                        }
                        ref={input => {
                            this.provinceRef = input;
                        }}
                        onItemChange={item => {
                            this.onChangeProvince(item);
                        }}
                    />
                )}
                {!this.state.provinces.length > 0 && (
                    <TextInput
                        style={{...Styles.input.codeInput, marginVertical: 7}}
                        placeholder={strings('province')}
                        placeholderTextColor={ColorStyle.textInput}
                    />
                )}
                {this.state.districts.length > 0 && (
                    <AppAutoComplete
                        dataList={this.state.districts}
                        alwaysShowOnTop={true}
                        placeholder={strings('district')}
                        defaultValue={
                            this.state.district !== undefined
                                ? this.state.district.name
                                : ''
                        }
                        ref={input => {
                            this.districtRef = input;
                        }}
                        onItemChange={item => {

                            this.onChangeDistrict(item);
                        }}
                    />
                )}
                {!this.state.districts.length > 0 && (
                    <TextInput
                        style={{...Styles.input.codeInput, marginVertical: 5}}
                        placeholder={strings('district')}
                        placeholderTextColor={ColorStyle.textInput}
                    />
                )}

                {this.state.wards.length > 0 && (
                    <AppAutoComplete
                        dataList={this.state.wards}
                        alwaysShowOnTop={true}
                        placeholder={strings('ward')}
                        defaultValue={
                            this.state.ward !== undefined ? this.state.ward.name : ''
                        }
                        ref={input => {
                            this.wardRef = input;
                        }}
                        onItemChange={item => {
                            this.onChangeWard(item);
                        }}
                    />
                )}
                {!this.state.wards.length > 0 && (
                    <TextInput
                        style={{...Styles.input.codeInput, marginVertical: 5}}
                        placeholderTextColor={ColorStyle.textInput}
                        placeholder={strings('ward')}
                    />
                )}
                {this.renderTextInputWithTitle(
                    strings('storeAdd'),
                    'address',
                    'validAdd',
                    'checkAddId',
                    () => this.checkAddress(),
                    '',
                    'next',
                    input => {
                        this.addressRef = input;
                    },
                    () => {},
                )}
            </View>
        )
    }
    renderContact(){
        return(
            <View>
                <Text
                    style={{
                        ...Styles.text.text14,
                        fontWeight: '500',
                        color: 'rgba(0, 0, 0, 0.6)',
                        marginTop: 15,
                    }}>
                    {strings('contact')}
                </Text>
                {this.renderTextInputWithTitle(
                    `${strings('name')}`,
                    'fullName',
                    'validName',
                    'checkNameId',
                    () => this.checkName(),
                    '',
                    'next',
                    input => {
                        this.nameRef = input;
                    },
                    () => {
                        this.phoneRef.focus();
                    },
                )}
                {this.renderTextInputWithTitle(
                    strings('phone'),
                    'phone',
                    'validPhone',
                    'checkPhoneId',
                    () => this.checkPhone(),
                    'phone-pad',
                    'done',
                    input => {
                        this.phoneRef = input;
                    },
                    () => {
                        if(this.provinceRef !== undefined)this.provinceRef.focus()
                    },
                )}
            </View>
        )
    }
    checkName() {
        this.setState({validName: this.state.fullName.length > 3});
    }
    checkPhone() {
        return this.setState({validPhone: DataUtils.validPhone(this.state.phone)});
    }
    renderTextInputWithTitle(
        title,
        stateName,
        validStateName,
        checkId,
        checkFunc,
        keyboardType,
        returnKeyType,
        ref,
        onSubmitEditing,
    ) {
        let mValidStateName = this.state[validStateName];
        return (
            <View style={{height: Styles.constants.X * 1.1, marginTop: 12}}>
                <TextInput
                    style={{...Styles.input.codeInput, paddingRight: 30}}
                    value={this.state[stateName]}
                    placeholder={title}
                    placeholderTextColor={ColorStyle.gray}
                    onChangeText={text => {
                        this.setState({[stateName]: text}, () => {
                            if (!DataUtils.stringNullOrEmpty(checkId) && checkFunc != null) {
                                clearTimeout(this[checkId]);
                                this[checkId] = setTimeout(() => {
                                    checkFunc();
                                }, 500);
                            }
                        });
                    }}
                    ref={ref}
                    onSubmitEditing={onSubmitEditing}
                    returnKeyType={returnKeyType}
                    keyboardType={
                        !DataUtils.stringNullOrEmpty(keyboardType)
                            ? keyboardType
                            : 'ascii-capable'
                    }
                />
            </View>
        );
    }
    checkAddress() {
        return this.setState(
            {validAdd: this.state.address?.trim().length > 5},
            () => {
                if (this.state.validAdd) {
                    clearTimeout(searchAddressId);
                    searchAddressId = setTimeout(() => {}, 2000);
                }
            },
        );
    }
    changeDefaultAddress(checked) {
        this.setState({defaultAddress: checked});
    }
    changeIsCompanyAdd(checked) {
        this.setState({companyAddress: checked});
    }
    searchProvinces() {
        // city_name
    }
    getProvinces() {
        LocationHandle.getInstance().getProvinces((isSuccess, responseData) => {
            if (isSuccess && responseData.data !== undefined) {
                let provinces = responseData.data;

                if (provinces === undefined) {
                    provinces = [];
                }
                let province;
                if (this.state.province !== undefined) {
                    provinces.forEach(item => {
                        if (item.id === this.props.addressItem.address_detail.city.id) {
                            province = item;
                        }
                    });
                }
                this.setState({provinces, province}, () => {
                    if (province !== undefined) {
                        this.getDistricts(province.id);
                    }
                });
            }
        });
    }
    getDistricts(id) {
        LocationHandle.getInstance().getDistrictsByProvince(
            id,
            (isSuccess, responseData) => {
                if (isSuccess && responseData.data !== undefined) {
                    let districts = responseData.data;
                    if (districts === undefined) {
                        districts = [];
                    }
                    let district;
                    if (this.state.district !== undefined) {
                        districts.forEach(item => {
                            if (item.id === this.props.addressItem.address_detail.district.id) {
                                district = item;
                            }
                        });
                    }
                    this.setState({districts, district}, () => {
                        if (district !== undefined) {
                            this.getWards(district.id);
                        }
                    });
                }
            },
        );
    }
    getWards(id) {
        LocationHandle.getInstance().getWardsByDistrict(
            id,
            (isSuccess, responseData) => {
                if (isSuccess && responseData.data !== undefined) {
                    let wards = responseData.data;
                    if (wards === undefined) {
                        wards = [];
                    }
                    let ward;
                    if (this.state.ward !== undefined) {
                        wards.forEach(item => {
                            if (item.id === this.props.addressItem.address_detail.ward.id) {
                                ward = item;
                            }
                        });
                    }
                    this.setState({wards, ward});
                }
            },
        );
    }

    onChangeProvince(item) {
        if (this.state.provinceId !== item.id) {
            this.setState(
                {province: item, provinceId: item.id, districts: []},
                () => {
                    this.getDistricts(item.id);
                },
            );
        }
    }

    onChangeDistrict(item) {
        if (this.state.districtId !== item.id) {
            this.setState({district: item, districtId: item.id, wards: []}, () => {
                this.getWards(item.id);
            });
        }
    }

    onChangeWard(item) {
        if (this.state.wardId !== item.id) {
            this.setState({ward: item, wardId: item.id});
        }
    }

    onSave() {
        if (!this.state.validName) {
            SimpleToast.show(strings('validateName'), SimpleToast.SHORT);
            return;
        }
        if (!this.state.validPhone) {
            SimpleToast.show(strings('validatePhone'), SimpleToast.SHORT);
            return;
        }
        if (this.state.province === undefined || !this.provinceRef.isSelected()) {
            SimpleToast.show(strings('pleaseSelectProvince'), SimpleToast.SHORT);
            return;
        }
        if (this.state.district === undefined || !this.districtRef.isSelected()) {
            SimpleToast.show(strings('pleaseSelectDistrict'), SimpleToast.SHORT);
            return;
        }
        if (this.state.ward === undefined || !this.wardRef.isSelected()) {
            SimpleToast.show(strings('pleaseSelectWard'), SimpleToast.SHORT);
            return;
        }
        if (!this.state.validAdd) {
            SimpleToast.show(strings('pleaseSelectAddress'), SimpleToast.SHORT);
            return;
        }

        let params = {
            name: this.state.fullName,
            phone: this.state.phone,
            address_detail: {
                city: {
                    id:this.state.province.id,
                    name:this.state.province.name,
                    ghn_id:this.state.province.ghn_id
                },
                district: {
                    id:this.state.district.id,
                    name:this.state.district.name,
                    ghn_id:this.state.district.ghn_id
                },
                ward: {
                    id:this.state.ward.id,
                    name:this.state.ward.name,
                    ghn_id:this.state.ward.ghn_id
                },
            },
            unit: this.state.address,
            default_address: this.state.defaultAddress
                ? AppConstants.ADDRESS_CATEGORY.HOME
                : AppConstants.ADDRESS_CATEGORY.COMPANY,
            address_category: this.state.companyAddress
                ? AppConstants.ADDRESS_DEFAULT.DEFAULT
                : AppConstants.ADDRESS_DEFAULT.UN_DEFAULT,
        };
        if (this.state.addressId === undefined) {
            //Them moi
            AddressHandle.getInstance().createAddress(
                params,
                (isSuccess, dataResponse) => {
                    if (isSuccess) {
                        console.log(params)
                        if (this.props.callBackFunc != null) {
                            this.props.callBackFunc(true, params);
                        }
                        ViewUtils.showAlertDialog(strings('createAddressSuccess'), () => {
                            Actions.pop();
                        });
                    } else {
                        ViewUtils.showAlertDialog(strings('createAddressFailed'), null);
                    }
                },
            );
        } else {
            //Sua
            AddressHandle.getInstance().updateAddress(
                this.state.addressId,
                params,
                (isSuccess, dataResponse) => {
                    if (isSuccess) {
                        if (this.props.callBackFunc != null) {
                            this.props.callBackFunc(false, params);
                        }
                        ViewUtils.showAlertDialog(strings('updateAddressSuccess'), () => {
                            Actions.pop();
                        });
                    } else {
                        ViewUtils.showAlertDialog(strings('updateAddressFailed'), null);
                    }
                },
            );
        }
    }
    onDelete(id) {
        ViewUtils.showAskAlertDialog(
            strings('askDeleteAddress'),
            () => {
                AddressHandle.getInstance().deleteAddress(
                    id,
                    (isSuccess, dataResponse) => {
                        if (isSuccess) {
                            ViewUtils.showAlertDialog(strings('deleteAddressSuccess'), () => {
                                this.props.onDelete(false, undefined);
                                Actions.pop();
                            });
                        } else {
                            ViewUtils.showAlertDialog(strings('deleteAddressFailed', null));
                        }
                    },
                );
            },
            () => {},
        );
    }
    onCancel() {
        Actions.pop();
    }
}
