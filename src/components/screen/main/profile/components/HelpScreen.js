import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import NavigationUtils from '../../../../../Utils/NavigationUtils';
import constants from '../../../../../Api/constants';
import Ripple from 'react-native-material-ripple';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'react-native-elements';
import SimpleToast from 'react-native-simple-toast';
import {strings} from '../../../../../resource/languages/i18n';
import ElevatedView from 'react-native-elevated-view';
import Accordion from 'react-native-collapsible/Accordion';
import Styles from '../../../../../resource/Styles';
import Toolbar from '../../../../elements/toolbar/Toolbar';
import ColorStyle from '../../../../../resource/ColorStyle';
let aboutOptionList = [];
let policyOptionList = [];
let customerOptionList = [];
class HelpScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeAboutSections: [0],
      activePolicySections: [0],
      activeCustomerSections: [0],
    };
    aboutOptionList = [
      {
        text: strings('aboutVietChemDes'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/gioi-thieu',
      },
      {
        text: strings('aboutVietChemRecruit'),
        des:strings('desAboutVietChemRecruit'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/tuyen-dung',
      },
      {
        text: strings('aboutVietChemPartner'),
        des:strings('desAboutVietChemPartner'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/moi-hop-tac-phat-trien-tt',
      },
      {
        text: strings('aboutVietChemPublic'),
        des:strings('desAboutVietChemPublic'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/tieu-chuan-cong-dong',
      },
      {
        text: strings('aboutVietChem1'),
        des:strings('desAboutVietChem1'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/tieu-chuan-cong-dong',
      },
      {
        text: strings('aboutVietChem2'),
        des:strings('desAboutVietChem2'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/tieu-chuan-cong-dong',
      },
      {
        des: strings('des'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/tieu-chuan-cong-dong',
      },
    ];
    policyOptionList = [
      {
        text: strings('policyQuyCheSan'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/dieu-khoan-dich-vu',
      },
      {
        text: strings('policyChinhSach'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/chinh-sach-bao-mat',
      },
      {
        text: strings('policyQuyCheHD'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/quy-che-hoat-dong-cua-hoi-nhom',
      },
      {
        text: strings('policyChinhSachVanChuyen'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/chinh-sach-van-chuyen',
      },
      {
        text: strings('policyBaoHanh'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/chinh-sach-bao-hanh',
      },
      {
        text: strings('policyThanhToan'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/quy-dinh-va-hinh-thuc-thanh-toan',
      },
      {
        text: strings('policyDoiTra'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/chinh-sach-doi-tra-hoan-tien',
      },
      {
        text: strings('policyViPham'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/chinh-sach-xu-ly-hanh-vi-vi-pham',
      },
    ];
    customerOptionList = [
      {
        text: strings('HD_MuaHang'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/huong-dan-mua-hang',
      },
      {
        text: strings('HD_BanHang'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/huong-dan-ban-hang',
      },
      {
        text: strings('HoTroChupAnh'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/ho-tro-chup-anh-sp-tao-gian-hang',
      },
      {
        text: strings('HoTroThuongMai'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/ho-tro-thuong-mai-quoc-te',
      },
      {
        text: strings('HD_TraGop'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/huong-dan-mua-tra-gop',
      },
      {
        text: strings('HIAC'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/gioi-thieu-hiac',
      },
      {
        text: strings('BaoCao'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/bao-cao-gian-lan-lua-dao',
      },
      {
        text: strings('LienHe'),
        onPress: () => {
          SimpleToast.show('123');
        },
        staticUrl: '/static-pages/lien-he',
      },
    ];
  }
  render() {
    return (
      <View style={Styles.container}>
        <Toolbar
          title={strings('held')}
          backgroundColor={ColorStyle.tabActive}
          textColor={ColorStyle.tabWhite}
        />
        <ScrollView
          contentContainerStyle={{width: '100%', alignItems: 'center'}}>
          {this.renderOptionView(
            strings('aboutVietChemDes'),
            aboutOptionList,
            'activeAboutSections',
          )}
          {this.renderOptionView(
            strings('policyTitle'),
            policyOptionList,
            'activePolicySections',
          )}
          {this.renderOptionView(
            strings('customerServiceTitle'),
            customerOptionList,
            'activeCustomerSections',
          )}
          <ElevatedView
            elevation={1}
            style={{
              marginBottom: 10,
              paddingHorizontal: '2%',
            }}>
            <Text
              style={{
                paddingHorizontal: 10,
                paddingTop: 20,
              }}>
              {strings('VietChemCompany')}
            </Text>
            <Text
              style={{
                paddingHorizontal: 20,
                lineHeight: 25,
                paddingVertical: 15,
              }}>
              {strings('VietChemDes')}
            </Text>
          </ElevatedView>
        </ScrollView>
      </View>
    );
  }
  renderOptionView(title, optionList, activeSections) {
    let optionView = optionList.map(item => {
      return (
        <Ripple
          onPress={() => {
            NavigationUtils.goToStaticWenView(constants.host + item.staticUrl);
          }}>
          <Text style={{color:ColorStyle.tabBlack,fontWeight:'600'}}>{item.text}<Text style={{color:ColorStyle.tabBlack,fontWeight:'400'}}>{item.des}</Text></Text>
        </Ripple>
      );
    });
    return (
      <ElevatedView
        elevation={1}
        style={{
          marginBottom: 10,
          paddingHorizontal: '2%',
          width: '100%',
        }}>
        <Accordion
          sections={[{}]}
          activeSections={this.state[activeSections]}
          renderHeader={(section, _, isActive) => {
            return (
              <View
                style={{
                  paddingVertical: 10,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  paddingHorizontal: '2%',
                }}>
                <Text>{title}</Text>
                <Icon
                  name={isActive?'down':'up'}
                  type={'antdesign'}
                  size={15}
                  color={ColorStyle.tabBlack}
                />
                {/*<Image*/}
                {/*  source={ImageHelper.arrow}*/}
                {/*  style={{*/}
                {/*    width: 7,*/}
                {/*    height: 12,*/}
                {/*    transform: [{rotate: isActive ? '90deg' : '0deg'}],*/}
                {/*  }}*/}
                {/*  resizeMode="stretch"*/}
                {/*/>*/}
              </View>
            );
          }}
          renderContent={_ => {
            return <View>{optionView}</View>;
          }}
          onChange={actives => {
            this.setState({[activeSections]: actives});
          }}
          touchableComponent={Ripple}
        />
      </ElevatedView>
    );
  }
}
const mapStateToProps = state => {
  return {
    //gọi đến reducers
  };
};

//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(HelpScreen);
