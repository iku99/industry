import React, {Component} from 'react';
import {
    AsyncStorage,
    Image,
    Keyboard,
    ScrollView,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../../../resource/Styles';
import Toolbar from '../../../../elements/toolbar/Toolbar';
import {strings} from '../../../../../resource/languages/i18n';
import ColorStyle from '../../../../../resource/ColorStyle';
import {HelperText, TextInput} from 'react-native-paper';
import ViewUtils from '../../../../../Utils/ViewUtils';
import DateUtil from '../../../../../Utils/DateUtil';
import GlobalInfo from '../../../../../Utils/Common/GlobalInfo';
import ImagePicker from 'react-native-image-crop-picker';
import PermissionUtils from '../../../../../Utils/PermissionUtils';
import {PERMISSIONS} from 'react-native-permissions';
import AccountHandle from '../../../../../sagas/AccountHandle';
import {Icon} from 'react-native-elements';
import { Actions } from "react-native-router-flux";
import {EventRegister} from "react-native-event-listeners";
import MediaHandle from "../../../../../sagas/MediaHandle";
import AppConstants from "../../../../../resource/AppConstants";
import AppDatePicker from "../../../../elements/DatePicker/AppDatePicker";
class Personal extends Component {
    constructor(props) {
        super(props);
        let startDate = new Date();
        this.state = {
            statusEdit: false,
            name: '',
            userName: '',
            password: '',
            phone: '',
            showPassword: false,
            validName: false,
            validPass: false,
            validUser: false,
            validBirthday: false,
            validSdt: false,
            loading: false,
            dateOfBirth:'',
            startDate: startDate,
            showDate: false,
            imageAvatar: 'https://www.evolutionsociety.org/userdata/news_picupload/pic_sid189-0-norm.jpg',
            gender: 1,
            canUpdatePassword: false,
            showChangePassword: false,
            userInfo:undefined
        };
    }
    componentDidMount() {
        this.getData();
    }

    async getData() {
        let userInfo = await GlobalInfo.getUserInfo();
        let dob ='' ;
        if (userInfo.date_of_birth !== null) {
            dob =  userInfo.date_of_birth;
        }
        this.setState(
            {
                userInfo:userInfo,
                name: userInfo.full_name,
                userName: userInfo.email,
                password: '',
                phone: userInfo?.phone,
                validName: false,
                validPass: false,
                validUser: false,
                validBirthday: false,
                validSdt: false,
                loading: false,
                dateOfBirth: dob,
                showDate: false,
                imageAvatar: userInfo.avatar,
                gender: userInfo.gender,
            },
            () => {
                this.checkUser();
                this.checkBirthday();
                this.checkPassword();
                this.checkSdt();
                this.checkName();
            },
        );
    }
    render() {

        return (
            <View style={[Styles.container, {backgroundColor: ColorStyle.colorPersonal}]}>
                <Toolbar
                    title={strings('personal')}
                    backgroundColor={ColorStyle.tabActive}
                    textColor={ColorStyle.tabWhite}
                    edit={true}
                    status={this.state.statusEdit}
                    onPress={() => this.setState({statusEdit: !this.state.statusEdit})}
                />

                <ScrollView>
                    <View
                        style={{
                            height: Styles.constants.heightScreen / 4,
                            backgroundColor: ColorStyle.colorPersonal,
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}>
                        <Image
                            source={this.getAvatar(this.state.imageAvatar)}
                            style={{
                                ...Styles.icon.img_avatar,
                                borderColor: ColorStyle.gray,
                                borderWidth: 1,
                                borderRadius: 200,
                            }}
                        />
                        <TouchableOpacity
                            style={{
                                padding: 5,
                                marginBottom: 20,
                                display: this.state.statusEdit ? 'flex' : 'none',
                            }}
                            onPress={() => {
                                if (
                                    PermissionUtils.checkPermission(PERMISSIONS.IOS.PHOTO_LIBRARY)
                                ) {
                                    ImagePicker.openPicker({
                                        width: 400,
                                        height: 400,
                                        compressImageMaxWidth: 800,
                                        compressImageMaxHeight: 800,
                                        cropping: true,
                                    }).then(image => {
                                        MediaHandle.getInstance().uploadProfileAvatar(image,(_)=>{
                                        },(isSuccess, responseData)=>{
                                            console.log(responseData)
                                            if(isSuccess){
                                                let data=responseData._response;
                                                this.setState({imageAvatar: JSON.parse(data).url})}
                                        })
                                    });
                                }
                            }}>
                            <Text
                                style={{...Styles.text.text14, color: ColorStyle.tabActive}}>
                                {strings('changeAvatar')}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{
                            backgroundColor: this.state.statusEdit
                                ? undefined
                                : ColorStyle.tabWhite,
                            borderTopLeftRadius: 20,
                            borderTopRightRadius: 20,
                            marginTop: 8,
                            width: Styles.constants.widthScreen,
                            height: this.state.statusEdit
                                ? undefined
                                : Styles.constants.heightScreen -
                                Styles.constants.heightScreen / 4,
                        }}>
                        {this.renderTextInput(
                            strings('name'),
                            this.state.name,
                            false,
                            v => {
                                this.setState({name: v});
                            },
                        )}
                        {this.renderTextInput(
                            strings('email'),
                            this.state.userName,
                            false,
                            v => {
                                this.setState({userName: v});
                            },
                        )}
                        <TouchableOpacity disabled={!this.state.statusEdit} style={{
                            overflow: 'hidden',
                            backgroundColor: ColorStyle.tabWhite,
                            borderRadius: 5,
                            marginTop: 8,
                            width: Styles.constants.widthScreenMg24,
                            marginHorizontal: Styles.constants.marginHorizontalAll,
                            height:Styles.constants.X*1.5,
                            paddingHorizontal:Styles.constants.X/4,
                            paddingVertical:Styles.constants.X/4
                        }} onPress={()=>{  this.setState({showDate: true})}}>
                            <Text style={{fontSize: 13,color:ColorStyle.gray}}>{strings('dateBirth')}</Text>
                            <Text style={{fontSize: 14,marginTop:5,color:ColorStyle.tabBlack}}>{DateUtil.formatDate('DD/MM/YYYY',this.state.dateOfBirth)}</Text>
                        </TouchableOpacity>
                        {/*{this.renderTextInput(*/}
                        {/*    strings('dateBirth'),*/}
                        {/*    this.state.dateOfBirth,*/}
                        {/*    false,*/}
                        {/*    v => {*/}
                        {/*        this.setState({dateOfBirth: v});*/}
                        {/*        this.checkBirthday()*/}
                        {/*    },*/}
                        {/*)}*/}
                        {this.renderTextInput(
                            strings('phone'),
                            this.state.phone,
                            false,
                            v => {
                                this.setState({phone: v});
                            },
                        )}
                        <TouchableOpacity
                            onPress={()=>Actions.jump('changePassword')}
                            style={{
                                backgroundColor: ColorStyle.tabWhite,
                                borderRadius: 10,
                                marginTop: 8,
                                width: Styles.constants.widthScreenMg24,
                                marginHorizontal: Styles.constants.marginHorizontalAll,
                                shadowColor: 'rgba(0, 0, 0, 0.2)',
                                shadowOffset: { width: 0, height: 2 },
                                shadowOpacity: 1,
                                shadowRadius: 1,
                                elevation: 4,
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                flexDirection: 'row',
                                padding: 15}}>
                            <Text style={{...Styles.text.text14,color:ColorStyle.tabBlack,fontWeight:'500'}}>{strings('changePassword')}</Text>
                            <Icon
                                name="chevron-right"
                                type="feather"
                                size={25}
                                color={ColorStyle.gray}
                            />
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <TouchableOpacity
                    onPress={() => this.saveInfo()}
                    style={[
                        Styles.button.buttonLogin,
                        {
                            backgroundColor: ColorStyle.tabActive,
                            display: this.state.statusEdit ? 'flex' : 'none',
                            borderRadius: 35,
                            marginVertical: Styles.constants.X * 0.5,

                        },
                    ]}>
                    <Text style={[Styles.text.text14, {fontWeight: '500',color: ColorStyle.tabWhite}]}>
                        {strings('savePersonal')}
                    </Text>
                </TouchableOpacity>
                {this.state.showDate &&  (
                    <AppDatePicker
                        onFinish={(isSelect, date) => {
                            let preDate = this.state.startDate;
                            if (isSelect) {
                                preDate = date;
                            }
                            this.setState(
                                {
                                    startDate: preDate,
                                    showDate: false,
                                    dateOfBirth: preDate,
                                },
                                () =>{}
                            );
                        }}
                        minimumDate={new Date()}
                        maximumDate={DateUtil.addYear(new Date(), 100)}
                        date={this.state.startDate}
                    />
                )}
            </View>
        );
    }
    getAvatar(image){
        let avatar = '';
        if (
            image !== null
        ) {
            let url = image;
            if (url.startsWith(AppConstants.IMG_ONL)) {
                avatar = {
                    uri:url
                };
            } else {
                avatar = {
                    uri:GlobalInfo.initApiEndpoint(url)
                };
            }
        } else {
            avatar = {
                uri:''
            };
        }
        return avatar;
    }
    saveInfo() {
        let dob=this.state.dateOfBirth;
        let date_of_birth=DateUtil.formatDate('YYYY-MM-DD',dob)
        if (!this.state.validName || !this.state.validUser) {
            ViewUtils.showAlertDialog(strings('dataInvalid'), undefined);
            return;
        }
        let param = {
            full_name: this.state.name,
            email: this.state.userName,
            date_of_birth:date_of_birth,
            phone:this.state.phone,
            avatar:this.state.imageAvatar
        };
        AccountHandle.getInstance().updateUserInfo(param, (isSuccess, res) => {
            if (isSuccess) {
             let userInfo = {
                    ...this.state.userInfo,
                    ...param
                }

                console.log('usser:',userInfo)
                EventRegister.emit(AppConstants.EventName.CHANGE_AVATAR, userInfo);
                GlobalInfo.userInfo = userInfo;
                ViewUtils.showAlertDialog(strings('success'),
                    undefined,
                );
                return
            }
            ViewUtils.showAlertDialog(
                strings('updateUserFail'),
                undefined,
            );
        });
    }
    renderTextInput(label, value, statue, onChangeText) {
        return (
            <View
                style={{
                    overflow: 'hidden',
                    backgroundColor: ColorStyle.tabWhite,
                    borderRadius: 5,
                    marginTop: 8,
                    width: Styles.constants.widthScreenMg24,
                    marginHorizontal: Styles.constants.marginHorizontalAll,

                }}>
                <TextInput
                    mode="flat"
                    label={label}
                    placeholderTextColor="white"
                    value={value}
                    editable={this.state.statusEdit}
                    onChangeText={onChangeText}
                    // error={true}
                    underlineColor={'transform'}
                    outlineColor={'green'}
                    style={{
                        color: ColorStyle.tabBlack,
                        fontWeight: '500',
                        borderRadius: 5,
                        width: Styles.constants.widthScreenMg24,
                        backgroundColor: ColorStyle.tabWhite,
                        fontSize: 14,
                    }}
                    theme={{
                        colors: {text: ColorStyle.tabBlack, primary: ColorStyle.tabActive},
                    }}
                />
                <HelperText
                    type="error"
                    style={{display: this.state.validName ? 'none' : 'flex'}}
                    visible={this.state.name?.length === 0}>
                    Error!
                </HelperText>
            </View>
        );
    }

    checkUser() {
        this.setState({
            validUser:
                this.state.userName !== undefined && this.state.userName.length > 3,
        });
    }

    checkName() {
        this.setState({
            validName: this.state.name !== undefined && this.state.name.length > 3,
        });
    }

    checkBirthday() {
        this.setState({
            validBirthday:
                this.state.dateOfBirth !== undefined
        });
    }

    checkSdt() {
        this.setState({
            validSdt: this.state.sdt !== undefined && this.state.sdt?.length > 3,
        });
    }

    checkPassword() {
        this.setState({
            validPass:
                this.state.password !== undefined && this.state.password.length > 3,
        });
    }
}
//lấy dữ liệu về
const mapStateToProps = state => {
    return {};
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
    return {
        onFetchMovies: () => {},
    };
};
const PersonalScreen = connect(mapStateToProps, mapDispatchToProps)(Personal);
export default PersonalScreen;
