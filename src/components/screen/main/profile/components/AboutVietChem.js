import React, {Component} from 'react';
import Styles from "../../../../../resource/Styles";
import Toolbar from "../../../../elements/toolbar/Toolbar";
import {strings} from "../../../../../resource/languages/i18n";
import ColorStyle from "../../../../../resource/ColorStyle";
import {Image, ScrollView, Text, View} from "react-native";
import ElevatedView from "react-native-elevated-view";
import SimpleToast from "react-native-simple-toast";
import Ripple from "react-native-material-ripple";
import NavigationUtils from "../../../../../Utils/NavigationUtils";
import constants from "../../../../../Api/constants";
import Accordion from "react-native-collapsible/Accordion";
import {Icon} from "react-native-elements";
let aboutOptionList = [];
let policyOptionList = [];
let customerOptionList = [];
export default class AboutVietChem extends Component{
    constructor(props) {
        super(props);
        this.state = {
            activeAboutSections: [0],
            activePolicySections: [0],
            activeCustomerSections: [0],
        };
        aboutOptionList = [
            {
                text: strings('aboutVietChemRecruit'),
                des:strings('desAboutVietChemRecruit'),
                onPress: () => {
                    SimpleToast.show('123');
                },
                staticUrl: '/static-pages/tuyen-dung',
            },
            {
                text: strings('aboutVietChemPartner'),
                des:strings('desAboutVietChemPartner'),
                onPress: () => {
                    SimpleToast.show('123');
                },
                staticUrl: '/static-pages/moi-hop-tac-phat-trien-tt',
            },
            {
                text: strings('aboutVietChemPublic'),
                des:strings('desAboutVietChemPublic'),
                onPress: () => {
                    SimpleToast.show('123');
                },
                staticUrl: '/static-pages/tieu-chuan-cong-dong',
            },
            {
                text: strings('aboutVietChem1'),
                des:strings('desAboutVietChem1'),
                onPress: () => {
                    SimpleToast.show('123');
                },
                staticUrl: '/static-pages/tieu-chuan-cong-dong',
            },
            {
                text: strings('aboutVietChem2'),
                des:strings('desAboutVietChem2'),
                onPress: () => {
                    SimpleToast.show('123');
                },
                staticUrl: '/static-pages/tieu-chuan-cong-dong',
            },
            {
                des: strings('des'),
                onPress: () => {
                    SimpleToast.show('123');
                },
                staticUrl: '/static-pages/tieu-chuan-cong-dong',
            },
        ];
    }
    render() {
        return(
            <View style={Styles.container}>
                <Toolbar
                    title={strings('aboutVietChemDes')}
                    backgroundColor={ColorStyle.tabActive}
                    textColor={ColorStyle.tabWhite}
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{width: '95%', alignSelf: 'center'}}>
                    <Text style={{fontSize:18,marginVertical:10,color:ColorStyle.tabBlack,fontWeight:'700'}}>{strings('linhvuc')}</Text>
                    {this.renderOptionView(
                        strings('aboutVietChemDes'),
                        aboutOptionList,
                        'activeAboutSections',
                    )}
                    <Image source={{uri:'https://sudospaces.com/vietchem-com-vn/2018/09/ff9da6848bcd6b9332dc.jpg'}} style={{width:'100%',height:Styles.constants.heightScreen/3,marginTop:10}}/>
                    <Image source={{uri:'https://sudospaces.com/vietchem-com-vn/2018/09/4e24c0a2eceb0cb555fa.jpg'}} style={{width:'100%',height:Styles.constants.heightScreen/3,marginTop:10}}/>
                    <Text
                        style={{
                            paddingHorizontal: 10,
                            paddingTop: 20,
                        }}>
                        {strings('des1')}
                    </Text>
                    <ElevatedView
                        elevation={1}
                        style={{
                            marginBottom: 10,
                            paddingHorizontal: '2%',
                        }}>
                        <Text
                            style={{
                                paddingHorizontal: 10,
                                paddingTop: 20,
                            }}>
                            {strings('VietChemCompany')}
                        </Text>
                        <Text
                            style={{
                                paddingHorizontal: 20,
                                lineHeight: 25,
                                paddingVertical: 15,
                            }}>
                            {strings('VietChemDes')}
                        </Text>
                    </ElevatedView>
                </ScrollView>

            </View>
        )
    }
    renderOptionView(title, optionList) {
        let optionView = optionList.map(item => {
            return (
                <Ripple
                    style={{marginTop:10}}
                    onPress={() => {
                        NavigationUtils.goToStaticWenView(constants.host + item.staticUrl);
                    }}>
                    <Text style={{color:ColorStyle.tabBlack,fontWeight:'600'}}>{item.text}<Text style={{color:ColorStyle.tabBlack,fontWeight:'400'}}>{item.des}</Text></Text>
                </Ripple>
            );
        });
        return (
            <ElevatedView
                elevation={1}
                style={{
                    marginBottom: 10,
                    paddingHorizontal: '2%',
                    width: '100%',
                }}>
                {optionView}
            </ElevatedView>
        );
    }
}
