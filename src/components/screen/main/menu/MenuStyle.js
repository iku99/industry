import {Platform} from 'react-native';
import ColorStyle from '../../../../resource/ColorStyle';
import Styles from "../../../../resource/Styles";

let isAndroid = Platform.OS === 'android';
export default {
  floatingBT: {
    position: 'absolute',
    bottom: 10,
    right: 50,
    backgroundColor: ColorStyle.tabWhite,
    width: 50,
    height: 50,
    borderRadius: 25,
    borderColor: ColorStyle.white,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 2,
  },
  mainCategory: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 8,
    paddingHorizontal: 5,
  },
  mainCategoryImg: {
    width: 30,
    height: 30,
  },
  mainCategoryText: {
    width: '100%',
    textAlign: 'center',
    fontSize: 11,
    lineHeight: 13,
    marginTop: 10,
  },
  childCategoryBox: {
    marginBottom: 8,
    marginEnd: 8,
    paddingHorizontal: 8,
    // ...AppStyle.boxShadow,
    // borderRadius:4,
    backgroundColor: ColorStyle.white,
  },
  childCategoryTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: '#2D2D2D',
    paddingBottom: 16,
    paddingTop: 16,
    flex: 1,
  },
  childItem: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 16,
  },
  childItemImg: {
    width: Styles.constants.X,
    height:Styles.constants.X,
    aspectRatio: 1,
    overflow: 'hidden',
    borderRadius: Styles.constants.X
  },
  childItemText: {
    fontSize: 11,
    textAlign: 'center',
    marginTop: 9,
    maxLines: 1,
  },
  viewAllButton: {},
  childCategoryTitleButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
  },
};
