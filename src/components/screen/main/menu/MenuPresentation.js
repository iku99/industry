import React, {Component} from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import ToolbarMain from '../../../elements/toolbar/ToolbarMain';
import {strings} from '../../../../resource/languages/i18n';
import Styles from '../../../../resource/Styles';
import CategoryHandle from '../../../../sagas/CategoryHandle';
import {SearchBar} from 'react-native-elements';
import ColorStyle from '../../../../resource/ColorStyle';
import constants from '../../../../Api/constants';
import MenuStyle from './MenuStyle';
import MyFastImage from '../../../elements/MyFastImage';
import AppConstants from '../../../../resource/AppConstants';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import {EventRegister} from 'react-native-event-listeners';
import CategoryStyle from "../../../elements/viewItem/CategoryItem/CategoryStyle";
let searchId = -1;
class MenuPresentation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      index: 0,
      isSearching: false,
      childCategories: [],
      selectedMainCategory: 0, showLoading:true
    };
  }
  componentDidMount() {
    EventRegister.addEventListener(
      AppConstants.EventName.GO_TO_CATEGORY,
      index => {
        this.onMainCategorySelect(index)
        this.setState({selectedMainCategory: index});
        this.getCategory();
      },
    );
    this.getCategory();
  }

  render() {
    return (
      <View style={Styles.container}>
        <ToolbarMain title={strings('category')} iconShopping={true} callbackShopping={()=> EventRegister.emit(AppConstants.EventName.GO_TO_TAB, 3)}/>
        <SearchBar
          ref={input => {
            this.searchRef = input;
          }}
          platform={'ios'}
          placeholder={strings('hintSearchCategory')}
          onChangeText={query => {
            this.setState({searchText: query}, () => this.queryCategory(query));
          }}
          value={this.state.searchText}
          inputStyle={{fontSize: 14, backgroundColor: ColorStyle.tabWhite}}
          inputContainerStyle={{
            backgroundColor: ColorStyle.tabWhite,
            alignSelf: 'center',
              shadowColor: 'black',
              shadowOffset: { width: 0, height: 1 },
              shadowOpacity: 0.8,
              shadowRadius: 1,
              elevation: 2,

          }}
          onCancel={() => this.setState({isSearching: false})}
        />

        {this.renderMainContent()}
      </View>
    );
  }

  renderMainContent() {
    return (
      <View
        style={{
          width: Styles.constants.widthScreen,
          alignSelf: 'center',
          marginTop: 10,
          height:'100%',
        }}>
        <FlatList
          style={{
            height:Styles.constants.X,
            paddingBottom: 15,
            borderBottomWidth: 1,
            borderBottomColor: ColorStyle.borderItemHome,
          }}
          showsHorizontalScrollIndicator={false}
          data={this.state.data}
          horizontal={true}
          keyExtractor={this.keyExtractor}
          renderItem={this._renderItem}
        />
        <View
          style={{
            flex: 5,
            width: Styles.constants.widthScreenMg24,
            alignSelf: 'center',
          }}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.childCategories}
            numColumns={2}
            contentContainerStyle={{
              display: 'flex',
              justifyContent: 'space-between',
            }}
            keyExtractor={this.keyExtractorChildCategory}
            renderItem={this._renderItemChildCategory}
          />
        </View>
      </View>
    );
  }

  keyExtractor = (item, index) => `menu_${index.toString()}`;
  _renderItem = ({item, index}) => {
    let isSelected = index === this.state.selectedMainCategory;
    return (
      <TouchableOpacity
        style={{
          borderRightWidth: isSelected ? 2 : 0,
          borderColor: ColorStyle.tabActive,
          alignItems: 'center',
          justifyContent: 'center',
            marginLeft:Styles.constants.X * 0.3,
          backgroundColor: isSelected ? ColorStyle.tabActive : '#F8F8F8',
          paddingHorizontal: 15,
          borderRadius: 20,
        }}
        onPress={() => {
            this.onMainCategorySelect(index)
          // this.onMainCategorySelect(item.id,index);
        }}>
        <Image
          style={CategoryStyle.icon}
          source={{
            uri:this.getImageUrl(item.image_url,index),
            headers: {Authorization: 'someAuthToken'},
          }}
          resizeMode={'cover'}
        />
        <Text
          numberOfLines={2}
          style={{
              marginTop:10,
            color: isSelected ? ColorStyle.tabWhite : ColorStyle.tabBlack,
            fontWeight: isSelected ? '700' : '400',
            maxWidth: Styles.constants.widthScreen / 4,
            textAlign: 'center',
          }}>
          {item.name}
        </Text>
      </TouchableOpacity>
    );
  };
  keyExtractorChildCategory = (item, index) => index.toString();
  _renderItemChildCategory = ({item, index}) => {
      return (
        <View>
          <TouchableOpacity
            style={{
              alignItems: 'center',
              width: Styles.constants.widthScreenMg24 / 2,
              marginVertical: 10,
            }}
            onPress={() => this.goToProductListOfCategory(item)}>
            <MyFastImage
              style={MenuStyle.childItemImg}
              source={{
                uri: this.getImageUrl(item.image_url,index),
                headers: {Authorization: 'someAuthToken'},
              }}
              resizeMode={AppConstants.IMAGE_SCALE_TYPE.CONTAIN}
            />
            <Text
              style={{
                ...Styles.text.text12,
                color: ColorStyle.tabBlack,
                fontWeight: '400',
                marginTop: 5,
                maxWidth: '70%',
              }}
              numberOfLines={1}>
              {item?.name}
            </Text>
          </TouchableOpacity>
        </View>
      );
  };
    onMainCategorySelect(index){
        let rootCategories = this.state.data;
        let childCategories = [];
        if(index < rootCategories.length && index >= 0 && rootCategories[index] !== undefined) {
            childCategories = rootCategories[index].children;
        }
        if(childCategories === undefined) childCategories = [];
        this.setState({selectedMainCategory:index, childCategories})
    }
    getCategory(){
        CategoryHandle.getInstance().getAllCategory((data)=> this.setState({data},()=>{
            this.onMainCategorySelect(this.state.selectedMainCategory)
        }));
    }
  goToProductListOfCategory(category) {
    NavigationUtils.goToProductCategory(category);
  }
  searchCategory() {
    this.setState({isSearching: true}, () => this.searchRef.focus());
  }
  queryCategory(query) {
    clearTimeout(this[searchId]);
    searchId = setTimeout(() => {
      CategoryHandle.getInstance().queryCategory(query, categories => {
        this.setState({data: categories}, () => {
          this.onMainCategorySelect(0);
        });
      });
    }, 300);
  }
    getImageUrl(images,index) {
        if (images != null ) {
            return constants.host + images;
        } else {
            return 'https://picsum.photos/130/214?' + index;
        }
    }
}

const mapStateToProps = state => {
  return {
    //gọi đến reducers
    category: state.categoryReducers,
  };
};

//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {};
};
const MenuScreen = connect(
  mapStateToProps,
  mapDispatchToProps,
)(MenuPresentation);
export default MenuScreen;
