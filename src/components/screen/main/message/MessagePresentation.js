import React, {Component} from 'react';
import {
  AsyncStorage,
  FlatList,
  Image,
  RefreshControl,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Styles from '../../../../resource/Styles';
import {connect} from 'react-redux';
import ToolbarMain from '../../../elements/toolbar/ToolbarMain';
import {strings} from '../../../../resource/languages/i18n';
import {SearchBar} from 'react-native-elements';
import {EventRegister} from 'react-native-event-listeners';
import AppConstants from '../../../../resource/AppConstants';
import SocketBusiness from '../../../../Utils/Socket/SocketBusiness';
import GlobalInfo from '../../../../Utils/Common/GlobalInfo';
import MessageHandle from '../../../../sagas/MessageHandle';
import ChatUtils from '../../../../Utils/ChatUtils';
import DataUtils from '../../../../Utils/DataUtils';
import ColorStyle from '../../../../resource/ColorStyle';
import ChatItem from '../../../elements/chat/ChatItem';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import CheckLogin from '../../../elements/checkLogin';
import EmptyView from "../../../elements/reminder/EmptyView";
class MessageScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
      page: 0,
      canLoadData: true,
      type: this.props.type,
      searchText: '',
      isSearching: false,
      isLogged: false,
    };
  }
  componentDidMount() {
    AsyncStorage.getItem(
      AppConstants.SharedPreferencesKey.IsLogin,
      (error, isLogin) => {
        this.setState({isLogged: isLogin === '1'});
      },
    );
    this.getMessage();

    EventRegister.addEventListener(
      AppConstants.EventName.ADD_NEW_CONVERSATION,
      () => {
        this.setState({page: 0, loading: false, canLoadData: true}, () =>
          this.getMessage(),
        );
      },
    );
    SocketBusiness.getInstance().addListener({
      id: SocketBusiness.CONVERSATION_LIST_ID,
      callback: data => {
        this.receiveMessage(data);
      },
    });
  }
  componentWillUnmount() {
    EventRegister.removeEventListener(
      AppConstants.EventName.ADD_NEW_CONVERSATION,
    );
    SocketBusiness.getInstance().removeListener(
      SocketBusiness.CONVERSATION_LIST_ID,
    );
  }
  renderCheckLogin() {
    return (
      <CheckLogin status={this.state.isLogged} views={this.renderContent()} />
    );
  }

  render() {
    return (
      <View style={Styles.container}>
        <ToolbarMain title={strings('message')} iconShopping={true} callbackShopping={()=> EventRegister.emit(AppConstants.EventName.GO_TO_TAB, 3)}/>
        {this.renderCheckLogin()}
      </View>
    );
  }
  renderContent() {
    return (
      <View style={{flex:1}}>
        <SearchBar
            ref={input => {
              this.searchRef = input;
            }}
            platform={'ios'}
            placeholder={strings('hintSearchMessage')}
            onChangeText={query => {
              // this.setState({searchText: query}, () => this.queryCategory(query));
            }}
            value={this.state.searchText}
            inputStyle={{fontSize: 14, backgroundColor: ColorStyle.tabWhite}}
            inputContainerStyle={{
              backgroundColor: ColorStyle.tabWhite,
              alignSelf: 'center',
              shadowColor: 'black',
              shadowOffset: { width: 0, height: 1 },
              shadowOpacity: 0.8,
              shadowRadius: 1,
              elevation: 2,
            }}
            onCancel={() => this.setState({isSearching: false})}/>
        {this.state.data?.length > 0 && (
            <FlatList
                data={this.state.data}
                keyExtractor={this.keyExtractor}
                renderItem={this.renderItem}
                style={{marginTop: 10}}
            />
        )}
        {this.state.data?.length === 0 && (
            <EmptyView
                text={strings('notMess')}
            />
        )}
      </View>
    );
  }
  keyExtractor = (item, index) => `mess_${index.toString()}`;
  renderItem = ({item, index}) => {
    return (
      <ChatItem
        item={item}
        index={index}
        count={this.state.data.length}
        type={this.props.type}
        goToConversation={userId => {
          try {
            item = {
              ...item,
              unread_count: 0,
            };
            let data = this.state.data;
            data[index] = item;
            this.setState({data});
          } catch (e) {}
          NavigationUtils.goToConversation(userId);
        }}
      />
    );
  };
  getMessage() {
    let params = {
      current_page: this.state.page,
      page_size: 20,
    };
    MessageHandle.getInstance().getChatInboxs(
      params,
      (isSuccess, responseData) => {
        this.handelResponse(isSuccess, responseData);
      },
    );
  }
  handelResponse(isSuccess, responseData) {
    let data = this.state.page === 0 ? [] : this.state.data;
    let newData = [];
    let canLoadData = true;
    if (
      isSuccess &&
      responseData.data !== undefined &&
      !DataUtils.listNullOrEmpty(responseData.data.list)
    ) {
      newData = responseData.data.list;
    }
    if (newData.length < 20) {
      canLoadData = false;
    }
    data = data.concat(newData);
    this.setState({data: data, canLoadData, loading: false});
  }
  receiveMessage(messageData) {
    var {data} = this.state;
    let size = data.length;
    let founded = false;
    for (let i = 0; i < size; i++) {
      let item = data[i];
      try {
        if (item.user.id === messageData.user.id) {
          let unread =
            item.user.id !== GlobalInfo.userInfo.id &&
            item.user.id === ChatUtils.getInstance().getConversationId();
          let body = messageData.body;
          if (body !== undefined) {
            item = {
              ...item,
              last_message: body,
              unread_count: unread ? 0 : item.unread_count + 1,
            };
          }
          let typing = messageData.typing;
          if (typing !== undefined) {
            item = {
              ...item,
              typing,
            };
          }
          data[i] = item;
          founded = true;
          this.setState({data});
          break;
        }
      } catch (e) {}
    }
    //Nhan conversation moi
    if (!founded && messageData.body !== undefined) {
      this.setState(
        {
          data: [],
          loading: true,
          page: 0,
          canLoadData: true,
          type: this.props.type,
        },
        () => this.getMessage(),
      );
    }
  }
}
//lấy dữ liệu về
const mapStateToProps = state => {
  return {};
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {
    onFetchMovies: () => {},
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(MessageScreen);
