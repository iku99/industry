import ImagePicker from 'react-native-image-crop-picker';
import {EventRegister} from 'react-native-event-listeners';
import React, {Component} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  Keyboard,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import ChatUtils from '../../../../Utils/ChatUtils';
import {Actions} from 'react-native-router-flux';
import GlobalInfo from '../../../../Utils/Common/GlobalInfo';
import AccountHandle from '../../../../sagas/AccountHandle';
import SocketBusiness from '../../../../Utils/Socket/SocketBusiness';
import MessageHandle from '../../../../sagas/MessageHandle';
import DataUtils from '../../../../Utils/DataUtils';
import DateUtil from '../../../../Utils/DateUtil';
import {strings} from '../../../../resource/languages/i18n';
import AppConstants from '../../../../resource/AppConstants';
import PermissionUtils from '../../../../Utils/PermissionUtils';
import {PERMISSIONS} from 'react-native-permissions';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import ViewUtils from '../../../../Utils/ViewUtils';
import MessageItem from '../../../elements/chat/MessageItem';
import Styles from '../../../../resource/Styles';
import {Icon} from 'react-native-elements';
import ColorStyle from '../../../../resource/ColorStyle';

let user_info = {};
let LIMIT = 40;
export default class ConversationView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: this.props.userId,
      messages: [],
      messageText: '',
      page: 0,
      isLoading: false,
      canLoadMore: true,
      scrollEnable: false,
      bottomMargin: 10,
      userName: this.props.userName,
      user: {},
      typing: false,
    };
  }
  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
    ChatUtils.getInstance().setConversationId(undefined);
  }

  _keyboardDidShow() {
    this.setState({bottomMargin: 40});
  }

  _keyboardDidHide() {
    this.setState({bottomMargin: 10});
  }

  async componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardWillShow',
      () => {
        this._keyboardDidShow();
      },
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardWillHide',
      () => {
        this._keyboardDidHide();
      },
    );
    let userId = this.state.userId;
    ChatUtils.getInstance().setConversationId(userId);
    if (userId === undefined) {
      Actions.pop();
    }
    this.getMessageList();
    user_info = await GlobalInfo.getUserInfo();
    AccountHandle.getInstance().getUserInfo(userId, (isSuccess, user) => {
      if (isSuccess) {
        this.setState({userName: user.full_name, user});
      }
    });

    SocketBusiness.getInstance().addListener({
      id: userId,
      callback: data => {
        this.receiveMessage(data);
      },
    });
  }

  render() {
    return (
      <View style={Styles.container}>
        {this.renderHeadView()}
        {/*<ScrollView*/}
        {/*    keyExtractor={(item, index) => `key-${index}`}*/}
        {/*  onScrollBeginDrag={() => {*/}
        {/*    Keyboard.dismiss();*/}
        {/*  }}*/}
        {/*  contentContainerStyle={{flex: 1, width: '100%'}}*/}
        {/*  showsVerticalScrollIndicator={false}*/}
        {/*  keyboardShouldPersistTaps={'handle'}*/}
        {/*>*/}
          <FlatList
            onScrollBeginDrag={() => Keyboard.dismiss()}
            contentContainerStyle={{flexGrow: 1}}
            inverted
            style={{marginHorizontal: Styles.constants.marginHorizontalAll}}
            keyExtractor={this.keyExtractor}
            renderItem={this._renderItem}
            data={this.state.messages}
            onEndReachedThreshold={0.01}
            onEndReached={() => this.handleLoadMore()}
            showsVerticalScrollIndicator={false}
            //keyboardShouldPersistTaps={'handle'}
            ListFooterComponent={this.renderFooterList.bind(this)}
          />
          {this.renderFooter()}
          {this.state.typing && (
            <Text
              style={{
                paddingHorizontal: 20,
                fontSize: 13,
              }}>
              {`${this.state.user.full_name} ${strings('isTyping')}`}
            </Text>
          )}
        {/*</ScrollView>*/}
      </View>
    );
  }
  handleLoadMore() {
    if (this.state.isLoading) {
      return;
    }
    if (!this.state.canLoadMore) {
      return;
    }
    this.setState({isLoading: true, page: this.state.page + 1}, () => {
      this.getMessageList();
    });
  }
  getMessageList() {
    let params = {
      page_size: LIMIT,
      current_page: this.state.page,
    };
    MessageHandle.getInstance().getChatMessageList(
      this.state.userId,
      params,
      (isSuccess, responseData) => {
        let msgs = this.state.messages;
        let newData = [];
        let canLoadMore = true;
        if (
          isSuccess &&
          responseData !== undefined &&
          responseData.data !== undefined &&
          responseData.data.list !== undefined
        ) {
          newData = responseData.data.list;
          newData.forEach(message => {
            message = {
              messageType: DataUtils.getMessageType(message),
              ...message,
            };
            if (msgs.length > 0) {
              let lastMessage = msgs[msgs.length - 1];
              if (
                !DateUtil.isSameMessageCluster(
                  message.created_at,
                  lastMessage.created_at,
                )
              ) {
                msgs = this.addDateMessage(msgs, true);
              }
            }
            msgs = [...msgs, message];
          });
          if (newData.length < LIMIT) {
            canLoadMore = false;
            if (msgs.length > 0) {
              msgs = this.addDateMessage(msgs, true);
            }
          }
        }
        this.setState({messages: msgs, canLoadMore, isLoading: false});
      },
    );
  }
  addDateMessage(msgs, addToTop, created_at) {
    let lastMessage = addToTop ? msgs[msgs.length - 1] : msgs[0];
    let messageTime =
      created_at !== undefined ? created_at : lastMessage.created_at;
    let dateText = '';
    if (DateUtil.isToday(messageTime)) {
      dateText = `${strings('toDay')}${DateUtil.formatDate(
        'HH:mm',
        messageTime,
      )}`;
    } else if (DateUtil.isYesterday(messageTime)) {
      dateText = `${strings('yesterday')}${DateUtil.formatDate(
        'HH:mm',
        messageTime,
      )}`;
    } else {
      dateText = DateUtil.formatDate('DD/MM/YYYY HH:mm', messageTime);
    }
    if (addToTop) {
      msgs = [
        ...msgs,
        {
          messageType: AppConstants.MESSAGE_TYPE.DATE,
          messageTime: dateText,
        },
      ];
    } else {
      msgs = [
        {
          messageType: AppConstants.MESSAGE_TYPE.DATE,
          messageTime: dateText,
        },
        ...msgs,
      ];
    }
    return msgs;
  }
  renderHeadView() {
    let item = this.state.messages;
    return (
      <View
        style={{
          backgroundColor: ColorStyle.tabWhite,
          paddingTop: Styles.constants.X,
          paddingBottom: Styles.constants.X * 0.2,
          paddingHorizontal: Styles.constants.X * 0.65,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20,
          elevation: 2,
        }}>
        <TouchableOpacity onPress={() => Actions.pop()}>
          <Icon
            name="left"
            type="antdesign"
            size={25}
            color={ColorStyle.tabActive}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            source={{uri: this.getAvatarUrl(item)}}
            style={{
              width:Styles.constants.X*0.7,
              height:Styles.constants.X*0.7,
              borderRadius: 20,
              overflow: 'hidden',
            }}
          />
          <Text
            style={[
              Styles.text.text20,
              {
                marginLeft: 10,
                color: this.props.textColor,
                fontWeight: 'bold',
                textAlign: 'center',
              },
            ]}
            numberOfLines={1}>
            {this.getName(item[0])}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.props.onPress}>
          <Icon
            name={'dots-three-vertical'}
            type="entypo"
            size={25}
            color={ColorStyle.tabActive}
          />
        </TouchableOpacity>
      </View>
    );
  }
  getName(item) {
    if (item === undefined || item.user === undefined) {
      return this.state.userName;
    }
    return item.user.full_name;
  }
  renderFooter() {
    return (
      <View
        style={{
          width: Styles.constants.widthScreenMg24,
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: this.state.bottomMargin,
        }}>
        <View
          style={{
            width: '85%',
            height: Styles.constants.X*1.3,
            marginHorizontal: Styles.constants.marginHorizontalAll,
            justifyContent:'center',
          }}>
          <TextInput
            style={{
              backgroundColor: '#EDEEF0',
              paddingVertical:Styles.constants.X/3,
              paddingLeft: 20,
              borderRadius: 10,
            }}
            value={this.state.messageText}
           autoCorrect={false}
            onChangeText={messageText => this.setState({messageText})}
            underlineColorAndroid="transparent"
            placeholder={strings('enterMessage')}
            onBlur={() => {
              this.setState({scrollEnable: false});
              SocketBusiness.getInstance().sendTyping(this.state.userId, false);
            }}
            onFocus={() => {
              this.setState({scrollEnable: true});
              SocketBusiness.getInstance().sendTyping(this.state.userId, false);
            }}
          />
        </View>
        <TouchableOpacity
          style={{
            padding: 10,
            backgroundColor: ColorStyle.tabActive,
            borderRadius: 5,
            display: this.state.messageText !== '' ? 'flex' : 'none',
          }}
          onPress={() => {
            if (this.state.messageText !== '') {
              this.sendTextMsg(this.state.messageText);
            }
          }}>
          <Icon
            name="send"
            color={ColorStyle.tabWhite}
            type="font-awesome"
            size={20}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            padding: 10,
            backgroundColor: ColorStyle.tabActive,
            borderRadius: 5,
            display: this.state.messageText !== '' ? 'none' : 'flex',
          }}
          onPress={() => {
            this.selectImage();
          }}>
          <Icon
            name="camera"
            color={ColorStyle.tabWhite}
            type="font-awesome"
            size={20}
          />
        </TouchableOpacity>
      </View>
    );
  }
  getAvatarUrl(item) {
    if (
      item === undefined ||
      item.user === undefined ||
      item.user.avatar === undefined
    ) {
      return 'https://picsum.photos/300/300?avatar='+this.props.userId;
    }
    return item.user.avatar;
  }
  keyExtractor = (item, index) => `conversation_${index.toString()}`;
  _renderItem = ({item, index}) => <View>{this.renderItem(item, index)}</View>;
  renderFooterList = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.isLoading) {
      return null;
    }
    return <ActivityIndicator style={{color: '#000'}} />;
  };
  renderItem(item, index) {
    let sameNextMsgSender = false;
    let data = this.state.messages;
    if (index === 0) {
      sameNextMsgSender = false;
    } else if (
      data[index - 1].user !== undefined &&
      item.user !== undefined &&
      data[index - 1].user.id === item.user.id
    ) {
      sameNextMsgSender = true;
    }
    return (
      <MessageItem
        item={item}
        user_info={user_info}
        sameNextMsgSender={sameNextMsgSender}
      />
    );
  }
  selectImage() {
    if (PermissionUtils.checkPermission(PERMISSIONS.IOS.PHOTO_LIBRARY)) {
      ImagePicker.openPicker({
        cropping: true,
        freeStyleCropEnabled: true,
        compressImageMaxWidth: 800,
        compressImageMaxHeight: 800,
      }).then(image => {
        NavigationUtils.goToSendImage(image, (msg, image) => {
          if (user_info === undefined) {
            return;
          }
          let newMsg = {
            body: msg,
            user: {
              id: user_info.id,
            },
            created_at: DateUtil.getCurrentDate(),
          };
          if (!DataUtils.stringNullOrEmpty(image.path)) {
            newMsg = {
              ...newMsg,
              image_link: AppConstants.LOCAL_SIGNAL + image.path,
              messageType: AppConstants.MESSAGE_TYPE.IMAGE,
            };
          }
          this.addSendMSg(newMsg);
          this.sendImageMsg(msg, image);
        });
      });
    }
  }
  sendImageMsg(msg, image) {
    let params = {
      body: msg,
      user_id: this.state.userId,
    };
    MessageHandle.getInstance().sendImgMsg(
      image,
      params,
      progress => {
        // console.log(Math.round(progress * 100))
      },
      (isSuccess, _) => {
        if (isSuccess) {
        } else {
          ViewUtils.showAlertDialog('Khong the tai anh len');
        }
      },
    );
  }
  sendTextMsg(text) {
    let userId = this.state.userId;
    if (user_info === undefined) {
      return;
    }
    let payload = {
      to: userId, // user_id
      body: text, // message
      local_id: '123',
    };
    SocketBusiness.getInstance().sendData(payload);
    let newMsg = {
      body: text,
      // created_at: responseData.data,
      user: {
        id: user_info.id,
      },
      messageType: AppConstants.MESSAGE_TYPE.TEXT,
      created_at: DateUtil.getCurrentDate(),
    };
    this.addSendMSg(newMsg);
    // let params = {
    //     user_id : this.state.userId,
    //     body:this.state.messageText
    // };
    // MessageHandle.getInstance().sendTextMsg(params, (isSuccess, responseData)=>{
    //     if(isSuccess){
    //         let newMsg = {
    //             body: text,
    //             created_at: responseData.data,
    //             user: {
    //                 id: user_info.id,
    //             }
    //         };
    //         this.addSendMSg(newMsg)
    //     }else {
    //         ViewUtil.showAlertDialog('error')
    //     }
    // })
  }
  addSendMSg(newMsg) {
    let messages = this.state.messages;
    if (messages.length === 0) {
      this.notiNewConversation();
    }
    if (messages.length > 0) {
      let lastMessage = messages[0];
      if (
        !DateUtil.isSameMessageCluster(
          newMsg.created_at,
          lastMessage.created_at,
        )
      ) {
        messages = this.addDateMessage(messages, false, newMsg.created_at);
      }
    }
    messages = [newMsg, ...messages];
    this.setState({messages, messageText: ''});
  }
  notiNewConversation() {
    EventRegister.emit(AppConstants.EventName.ADD_NEW_CONVERSATION, true);
  }
  receiveMessage(messageData) {
    if (messageData.user === undefined) {
      return;
    }
    if (this.state.userId === messageData.user.id) {
      let body = messageData.body;
      if (body !== undefined) {
        let newMsg = {
          body,
          // created_at: responseData.data,
          user: this.state.user,
        };
        newMsg = {
          ...newMsg,
          messageType: DataUtils.getMessageType(newMsg),
        };
        this.addSendMSg(newMsg);
      } else {
        let typing = messageData.typing;
        if (typing !== undefined) {
          if (typing !== this.state.typing) {
            this.setState({typing});
          }
        }
      }
    }
  }
}
