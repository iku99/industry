import LinearGradient from 'react-native-linear-gradient';
import React, {Component} from 'react';
import {
  Image,
  Keyboard,
  ScrollView,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {Icon} from 'react-native-elements';
import DataUtils from '../../../../Utils/DataUtils';
import ViewUtils from '../../../../Utils/ViewUtils';
import {strings} from '../../../../resource/languages/i18n';
import {Actions} from 'react-native-router-flux';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
export default class SendImageScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messageText: '',
      bottomMargin: 10,
    };
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow() {
    this.setState({bottomMargin: 40});
  }

  _keyboardDidHide() {
    this.setState({bottomMargin: 10});
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardWillShow',
      () => {
        this._keyboardDidShow();
      },
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardWillHide',
      () => {
        this._keyboardDidHide();
      },
    );
  }

  render() {
    let image = this.props.image;
    return (
      <View
        style={{
          width: Styles.constants.widthScreen,
          height: '100%',
        }}>
        <View style={{width: '100%', flex: 1}}>
          <Image
            source={{uri: image.path}}
            style={{
              width: Styles.constants.widthScreen,
              height: Styles.constants.heightScreen,
              resizeMode: 'contain',
            }}
          />
        </View>
        <ScrollView
          contentContainerStyle={{
            width: '100%',
            height: '100%',
            justifyContent: 'flex-end',
            position: 'absolute',
            top: 0,
            bottom: 0,
          }}>
          <LinearGradient
            colors={['rgba(25,47,106,0)', 'rgba(34,34,34,0.82)']}
            style={{
              flexDirection: 'row',
              paddingTop: 10,
              justifyContent: 'center',
              paddingHorizontal: Styles.constants.marginHorizontalAll,
              paddingBottom: this.state.bottomMargin,
            }}>
            <View
              style={{
                borderWidth: 1,
                width: '80%',
                backgroundColor: 'transparent',
                height: 45,
                justifyContent: 'center',
              }}>
              <TextInput
                value={this.state.messageText}
                onChangeText={messageText => this.setState({messageText})}
                placeholder={strings('enterMessage')}
                placeholderTextColor="white"
              />
            </View>
            <TouchableOpacity
              style={{
                padding: 10,
                marginLeft:20,
                backgroundColor: ColorStyle.tabActive,
                borderRadius: 5,
              }}
              onPress={() => {
                let messageText = this.state.messageText;
                if (DataUtils.stringNullOrEmpty(messageText.trim())) {
                  ViewUtils.showAlertDialog(strings('pleaseEnterMsg'));
                  return;
                }
                if (this.props.onSend != null) {
                  this.props.onSend(messageText, image);
                }
                Actions.pop();
              }}>
              <Icon
                name="send"
                type="font-awesome"
                size={20}
                color={ColorStyle.tabWhite}
              />
            </TouchableOpacity>
          </LinearGradient>
        </ScrollView>
        <TouchableOpacity
          style={{padding: 5, position: 'absolute', top: 30, left: 30}}
          onPress={() => Actions.pop()}>
          <Icon name="close" type="font-awesome" size={25} />
        </TouchableOpacity>
      </View>
    );
  }
}
