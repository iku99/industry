import React, {Component} from 'react';
import {
  AsyncStorage,
  FlatList,
  RefreshControl,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import {strings} from '../../../../resource/languages/i18n';
import ToolbarMain from '../../../elements/toolbar/ToolbarMain';
import {Icon} from 'react-native-elements';
import Styles from '../../../../resource/Styles';
import ColorStyle from '../../../../resource/ColorStyle';
import ItemNotificationOrder from '../../../elements/viewItem/itemNotification/ItemNotificationOrder';
import NavigationUtils from '../../../../Utils/NavigationUtils';
import AppConstants from '../../../../resource/AppConstants';
import NotificationHandle from '../../../../sagas/NotificationHandle';
import {UIActivityIndicator} from 'react-native-indicators';
import EmptyView from '../../../elements/reminder/EmptyView';
import ViewUtils from '../../../../Utils/ViewUtils';
import OrderHandle from '../../../../sagas/OrderHandle';
import CheckLogin from '../../../elements/checkLogin';
const X = Styles.constants.X;
const LIMIT = 1;
class NotificationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      data: [],
      loading: false,
      canLoadData: false,
      isLogged: false,
      countPromotion: 0,
      countActive: 0,
      countRate: 0,
      countOrder: 0,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem(
      AppConstants.SharedPreferencesKey.IsLogin,
      (error, isLogin) => {
        this.setState({isLogged: isLogin === '1'});
      },
    );
    this.getNotificationOrder();
  }
  keyExtractor = (item, index) => `notification_${index.toString()}`;

  renderCheckLogin() {
    return (
      <CheckLogin
        status={this.state.isLogged}
        views={
          <View style={{flex: 1}}>
            {this.renderHeader()}
            {this.renderTitle()}
            <FlatList
              showsVerticalScrollIndicator={false}
              style={{marginTop: 10}}
              data={this.state.data}
              renderItem={this.renderItemOrder}
              onEndReachedThreshold={0.01}
              keyExtractor={this.keyExtractor}
              onEndReached={() => this.handleLoadMore()}
              keyboardShouldPersistTaps={'handle'}
              ListFooterComponent={this.renderFooter.bind(this)}
            />

            {this.state.data.length === 0 && this.state.loading && (
              <EmptyView
                containerStyle={{flex: 1}}
                text={strings('emptyNotification')}
              />
            )}
          </View>
        }
      />
    );
  }
  renderHeader() {
    return (
      <View
        style={{
          marginHorizontal: Styles.constants.marginHorizontalAll,
          marginBottom: 10,
        }}>
        {this.renderItemTitle(
          {
            name: 'burst-sale',
            type: 'foundation',
            color: '#FFBA49',
          },
          strings('promotion'),
          strings('promotion_des'),
          this.state.countPromotion,
          () => {
            NavigationUtils.goToNotificationList(
              AppConstants.typeNotification.Promotion,
              () => {
                this.getCountNotification(
                  AppConstants.typeNotification.Promotion,
                  'countPromotion',
                );
              },
            );
          },
        )}
        {this.renderItemTitle(
          {
            name: 'bells',
            type: 'antdesign',
            color: '#4ECB71',
          },
          strings('active'),
          strings('active'),
          this.state.countActive,
          () => {
            NavigationUtils.goToActive();
          },
        )}

        {this.renderItemTitle(
          {
            name: 'star-outlined',
            type: 'entypo',
            color: '#FFD233',
          },
          strings('rate'),
          strings('rate_des'),
          this.state.countRate,
          () => {
            NavigationUtils.goToNotificationList(
              AppConstants.typeNotification.Rate,
              () => {
                this.getCountNotification(
                  AppConstants.typeNotification.Rate,
                  'countRate',
                );
              },
            );
          },
        )}
      </View>
    );
  }
  renderTitle() {
    return (
      <View
        style={{
          paddingHorizontal: Styles.constants.marginHorizontalAll,
          backgroundColor: ColorStyle.tabWhite,
          paddingVertical: Styles.constants.X / 4,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.2,
          shadowRadius: 1.41,
          elevation: 2,
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text
            style={[
              Styles.text.text12,
              {color: ColorStyle.tabBlack, fontWeight: '700'},
            ]}>
            {strings('listOrder')}
          </Text>
          <TouchableOpacity onPress={() => this.ReadAll()}>
            <Text
              style={[
                Styles.text.text10,
                {color: ColorStyle.tabInactive, fontWeight: '400'},
              ]}>
              {strings('readAll')} ({this.state.countOrder})
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  ReadAll() {
    NotificationHandle.getInstance().updateNotifications(
      undefined,
      (isSuccess, _) => {
        if (isSuccess) {
          this.getCountNotification(
            AppConstants.typeNotification.Order,
            'countOrder',
          );
          this.setState({data: [], page: 1}, () => {
            this.getNotificationOrder();
          });
        }
      },
    );
  }
  render() {
    if (this.props.title !== undefined) {
      return <View />;
    }
    return (
      <View style={Styles.container}>
        <ToolbarMain title={strings('notification')} iconProfile={true} />
        {this.renderCheckLogin()}
      </View>
    );
  }
  renderLoading() {
    if (this.state.loading) {
      return <UIActivityIndicator color={ColorStyle.tabActive} size={20} />;
    } else {
      return null;
    }
  }
  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    if (!this.state.canLoadData) {
      return null;
    }
    return <UIActivityIndicator color={ColorStyle.tabActive} size={20} />;
  };
  handleLoadMore() {
    if (!this.state.loading) {
      return;
    }
    if (this.state.canLoadData) {
      return;
    }
    this.setState(
      {loading: true, page: this.state.page + 1},
      () => this.getNotificationOrder,
    );
  }
  renderItemOrder = ({item, index}) => (
    <ItemNotificationOrder
      onNotificationClick={(item, index) =>
        this.onNotificationClick(item, index)
      }
      index={index}
      item={item}
    />
  );
  onNotificationClick(item, index) {
    if (item.status === 1) {
      this.updateNotificationStatus(item, index);
    }
    this.goToOrderDetail(item.type_id);
  }
  goToOrderDetail(item) {
    if (item !== undefined) {
      OrderHandle.getInstance().getOrderDetail(
        item,
        (isSuccess, dataResponse) => {
          if (isSuccess && dataResponse.data.length !== 0) {
            NavigationUtils.goToOrderDetail(
              dataResponse.data,
              false,
              null,
              null,
            );
            return;
          } else {
            ViewUtils.showAlertDialog(dataResponse.message);
          }
        },
      );
    }
  }
  updateNotificationStatus(item, index) {
    let param = {
      id: item.id,
    };
    NotificationHandle.getInstance().updateNotifications(
      param,
      (isSuccess, _) => {
        if (isSuccess) {
          let data = this.state.data;
          item = {
            ...item,
            status: true,
          };
          if (index >= 0 && index < data.length) {
            data[index] = item;
          }
          this.getCountNotification(
            AppConstants.typeNotification.Order,
            'countOrder',
          );
          this.setState({data});
        }
      },
    );
  }
  renderItemTitle(icon, title, des, count, onPress) {
    // let readed = NotificatioonUtils.checkRead(item.list);
    return (
      <TouchableOpacity
        onPress={onPress}
        style={{
          backgroundColor: ColorStyle.tabWhite,
          borderRadius: X * 0.2,
          marginTop: X * 0.2,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingVertical: X * 0.25,
          paddingHorizontal: X * 0.45,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.23,
          shadowRadius: 2.62,

          elevation: 4,
        }}>
        <Icon name={icon.name} type={icon.type} color={icon.color} size={30} />
        <View style={{left: X * 1.5, position: 'absolute'}}>
          <Text style={[Styles.text.text14, {fontWeight: '700'}]}>{title}</Text>
          <Text style={[Styles.text.text10]}>{des}</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              backgroundColor: ColorStyle.red,
              display: count === 0 ? 'none' : 'flex',
              paddingVertical: 3,
              paddingHorizontal: 8,
              borderRadius: 10000,
              alignItems: 'center',
            }}>
            <Text style={{fontWeight: '500', color: ColorStyle.tabWhite}}>
              {count}
            </Text>
          </View>
          <Icon
            name="chevron-right"
            type="feather"
            size={25}
            style={{right: 0}}
          />
        </View>
      </TouchableOpacity>
    );
  }
  getNotificationOrder() {
    let params = {
      page_index: this.state.page,
      page_size: LIMIT,
      type: AppConstants.typeNotification.Order,
    };
    NotificationHandle.getInstance().getNotifications(
      params,
      (isSuccess, responseData) => {
        console.log('res:', responseData);
        this.handelResponse(isSuccess, responseData);
        this.getCountNotification(
          AppConstants.typeNotification.Promotion,
          'countPromotion',
        );
        this.getCountNotification(
          AppConstants.typeNotification.Active,
          'countActive',
        );
        this.getCountNotification(
          AppConstants.typeNotification.Rate,
          'countRate',
        );
        this.getCountNotification(
          AppConstants.typeNotification.Order,
          'countOrder',
        );
      },
    );
  }
  getCountNotification(type, state) {
    let params = {
      type: type,
    };
    NotificationHandle.getInstance().getCountNotification(
      params,
      (isSuccess, responseData) => {
        this.setState({[state]: responseData.data.total_unread});
      },
    );
  }
  handelResponse(isSuccess, responseData) {
    let data = this.state.data;
    let newData = [];
    if (isSuccess && responseData.data !== undefined) {
      newData = responseData.data;
      if (newData.length < LIMIT) {
        this.setState({canLoadData: true});
        return;
      }
      data = data.concat(newData);
      this.setState({data, loading: false});
    }
  }
}
//lấy dữ liệu về
const mapStateToProps = state => {
  return {};
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {
    onFetchMovies: () => {},
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(NotificationScreen);
