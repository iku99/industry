import React, {Component} from 'react';
import {FlatList, View} from 'react-native';
import Styles from '../../../../../resource/Styles';
import {connect} from 'react-redux';
import ToolbarMain from '../../../../elements/toolbar/ToolbarMain';
import ItemNotificationOrder from '../../../../elements/viewItem/itemNotification/ItemNotificationOrder';
import NotificationHandle from '../../../../../sagas/NotificationHandle';
import AppConstants from '../../../../../resource/AppConstants';
import DataUtils from '../../../../../Utils/DataUtils';
import {strings} from '../../../../../resource/languages/i18n';
import EmptyView from '../../../../elements/reminder/EmptyView';
import {UIActivityIndicator} from 'react-native-indicators';
import ColorStyle from '../../../../../resource/ColorStyle';
import ItemNotidication from '../../../../elements/viewItem/itemNotification/ItemNotidication';
import OrderHandle from '../../../../../sagas/OrderHandle';
import NavigationUtils from '../../../../../Utils/NavigationUtils';
import ViewUtils from '../../../../../Utils/ViewUtils';
const LIMIT = 10;
class NotificationList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 0,
      canLoadData: true,
      loading: true,
    };
  }
  componentDidMount() {
    this.getNotification();
  }

  render() {
    return (
      <View style={{...Styles.container}}>
        <ToolbarMain title={this.getTitle()} iconBack={true} readAll={true} callBackReadAll={()=>this.readAllNotification()}/>
        <FlatList
          data={this.state.data}
          renderItem={this.renderItem}
          onEndReachedThreshold={0.01}
          onEndReached={() => this.handleLoadMore()}
          keyboardShouldPersistTaps={'handle'}
          ListFooterComponent={this.renderFooter.bind(this)}
          showsVerticalScrollIndicator={false}
        />
        {this.state.data.length === 0 && !this.state.loading && (
          <EmptyView
            containerStyle={{flex: 1}}
            text={strings('emptyNotification')}
          />
        )}
      </View>
    );
  }
  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return <UIActivityIndicator color={ColorStyle.tabActive} size={20} />;
  };
  handleLoadMore() {
    if (this.state.loading) {
      return;
    }
    if (!this.state.canLoadData) {
      return;
    }
    this.setState({loading: true, page: this.state.page + 1}, () =>
      this.getNotification,
    );
  }
  readAllNotification(){
    NotificationHandle.getInstance().updateNotifications(undefined,
        (isSuccess, _) => {
          if (isSuccess) {
            this.getCountNotification(AppConstants.typeNotification.Order,'countOrder')
            this.setState({data:[],page:1},()=>{
              this.getNotification();
            });
          }
        },
    );
  }
  getTitle() {
    let type = this.props.type;
    if (type === AppConstants.typeNotification.Active) {
      return strings('active');
    } else if (type === AppConstants.typeNotification.Promotion) {
      return strings('promotion');
    } else if(type === AppConstants.typeNotification.Rate) {
     return  strings('rate');
    }
  }
  renderItem = ({item, index}) => {
    return (
      <ItemNotidication
        onNotificationClick={(item, index) =>
          this.onNotificationClick(item, index)
        }
        index={index}
        item={item}
      />
    );
  };
  onNotificationClick(item, index) {
    if (item.status===1) {
      this.updateNotificationStatus(item, index);
    }
    this.goToOrderDetail(item.type_id);
  }
  updateNotificationStatus(item, index) {
    let param={
      id:item.id
    }
    NotificationHandle.getInstance().updateNotifications(
        param,
      (isSuccess, _) => {
        if (isSuccess) {
          let data = this.state.data;
          item = {
            ...item,
            status: true,
          };
          if (index >= 0 && index < data.length) {
            data[index] = item;
          }
          this.props.callback()
          this.setState({data});
        }
      },
    );
  }
  goToOrderDetail(item) {
    if (item!== undefined) {
      OrderHandle.getInstance().getOrderDetail(
        item,
        (isSuccess, dataResponse) => {
          console.log(dataResponse)
          if (isSuccess) {
            NavigationUtils.goToOrderDetail(
              dataResponse.data,
              false,
              null,
              null,
            );
          } else {
            ViewUtils.showAlertDialog(strings('cannotFoundOrder'));
          }
        },
      );
    }
  }
  getNotification() {
    let params = {
      page_index: this.state.page,
      page_size: 20,
      type: this.props.type,
    };
    NotificationHandle.getInstance().getNotifications(
      params,
      (isSuccess, responseData) => {
         this.handelResponse(isSuccess, responseData);
      },
    );
  }
  handelResponse(isSuccess, responseData) {
    let data = this.state.data;
    let newData = [];
    if (
      isSuccess &&
      responseData.data !== undefined
    ) {
      newData = responseData.data;
      if (newData.length < LIMIT) {
        this.setState({canLoadData: false});
      }
      data = data.concat(newData);
      this.setState({data, loading: false});
    }
  }
}
//lấy dữ liệu về
const mapStateToProps = state => {
  return {};
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {
    onFetchMovies: () => {},
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(NotificationList);
