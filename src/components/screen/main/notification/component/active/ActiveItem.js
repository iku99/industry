import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import ColorStyle from '../../../../../../resource/ColorStyle';
import MyFastImage from '../../../../../elements/MyFastImage';
import ProductUtils from '../../../../../../Utils/ProductUtils';
import Styles from '../../../../../../resource/Styles';
import CartUtils from '../../../../../../Utils/CartUtils';
import CurrencyFormatter from '../../../../../../Utils/CurrencyFormatter';
import {Icon} from 'react-native-elements';
import NavigationUtils from '../../../../../../Utils/NavigationUtils';
import { strings } from "../../../../../../resource/languages/i18n";
import TimeUtils from "../../../../../../Utils/TimeUtils";

export default class ActiveItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: this.props.item,
    };
  }
  render() {
    let hideActionBT = this.props.hideActionBT === true;
    let total = 0;
    let order = this.state.item;
    let index = this.props.index;
    // if (order.cart_info === undefined || order.cart_info.length <= 0) {
    //   return null;
    // }
    // let productList = order.cart_info.map((item, _) => {
    //   let product = item.product_snapshot;
    let product = order.images;
    //   total = total + product.price * item.qty;
    return (
      <TouchableOpacity
        style={{
          backgroundColor: ColorStyle.tabWhite,
          flexDirection: 'row',
          width: Styles.constants.widthScreenMg24,
          height: Styles.constants.X * 2,
          borderRadius: 4,
          alignSelf: 'center',
          marginBottom: 10,
          padding: 10,
        }}
        onPress={() => {
          // NavigationUtils.goToProductDetail(product);
        }}>
        <MyFastImage
          style={{width: 50, height: 50}}
          source={{
            uri: ProductUtils.getImages(product)[0],
            headers: {Authorization: 'someAuthToken'},
          }}
          resizeMode={'cover'}
        />
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            marginHorizontal: 8,
          }}>
          <Text
            style={[Styles.text.text14, {fontWeight: '500'}]}
            numberOfLines={1}>
            {order.name}
          </Text>
          <Text
            style={[
              Styles.text.text14,
              {fontWeight: '500', color: CartUtils.getColorText(order.status)},
            ]}
            numberOfLines={1}>{`${CartUtils.getCartStatus(
            order.status,
          )}`}</Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={[
                Styles.text.text11,
                {fontWeight: '500', color: ColorStyle.optionTextColor},
              ]}
              numberOfLines={1}>{`${strings('ordered')} ${TimeUtils.formatTime(
              order.created_at,
            )}`}</Text>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                style={[
                  Styles.text.text11,
                  {
                    fontWeight: '500',
                    color: ColorStyle.tabActive,
                    marginRight: 10,
                  },
                ]}
                numberOfLines={1}>
                {CurrencyFormatter(order.price)}
              </Text>
              <Icon
                name="arrow-forward-ios"
                type="materialicons"
                size={16}
                color={ColorStyle.optionTextColor}
              />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  goToStore(order) {
    NavigationUtils.goToShopDetail(order.store_id);
  }
}
