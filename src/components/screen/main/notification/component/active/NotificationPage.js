import React, {Component} from 'react';
import {FlatList, View} from 'react-native';
import EmptyView from '../../../../../elements/reminder/EmptyView';
import {strings} from '../../../../../../resource/languages/i18n';
import Styles from '../../../../../../resource/Styles';
import {UIActivityIndicator} from 'react-native-indicators';
import ColorStyle from '../../../../../../resource/ColorStyle';
import NotificationHandle from '../../../../../../sagas/NotificationHandle';
import AppConstants from '../../../../../../resource/AppConstants';
import ItemNotification from '../../../../../elements/viewItem/itemNotification/ItemNotidication';
import ItemNotificationInvite from '../../../../../elements/viewItem/ItemNotificationInvite';
const LIMIT = 1;
export default class NotificationPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 0,
      canLoadData: false,
      loading: true,
    };
  }
  componentDidMount() {
    this.getNotification();
  }
  readAllNotification() {
    NotificationHandle.getInstance().updateNotifications(
      undefined,
      (isSuccess, _) => {
        if (isSuccess) {
          this.getCountNotification(
            AppConstants.typeNotification.Order,
            'countOrder',
          );
          this.setState({data: [], page: 1}, () => {
            this.getNotification();
          });
        }
      },
    );
  }
  renderFooter = () => {
    console.log(this.state.canLoadData, this.state.loading);

    if (!this.state.loading) {
      return null;
    }
    if (this.state.canLoadData) {
      return null;
    }
    return <UIActivityIndicator color={ColorStyle.tabActive} size={20} />;
  };
  handleLoadMore() {
    if (!this.state.loading) {
      return;
    }
    if (!this.state.canLoadData) {
      return;
    }
    this.setState(
      {loading: true, page: this.state.page + 1},
      () => this.getNotification,
    );
  }
  renderItem = ({item, index}) => {
    if (item.type === AppConstants.typeNotification.Teams) {
      return (
        <ItemNotification
          onNotificationClick={(item, index) =>
            this.onNotificationClick(item, index)
          }
          index={index}
          item={item}
        />
      );
    }
    return (
      <ItemNotificationInvite
        item={item}
        onNotificationClick={(item, index) =>
          this.onNotificationClick(item, index)
        }
        index={index}
      />
    );
  };
  updateNotificationStatus(item, index) {
    let param = {
      id: item.id,
    };
    NotificationHandle.getInstance().updateNotifications(
      param,
      (isSuccess, _) => {
        if (isSuccess) {
          let data = this.state.data;
          item = {
            ...item,
            status: true,
          };
          if (index >= 0 && index < data.length) {
            data[index] = item;
          }
          this.props.callback();
          this.setState({data});
        }
      },
    );
  }
  onNotificationClick(item, index) {
    if (item.status === 1) {
      // this.updateNotificationStatus(item, index);
    }
    // this.goToOrderDetail(item.type_id);
  }
  getNotification() {
    let params = {
      page_index: this.state.page,
      page_size: LIMIT,
      type: this.props.type,
    };
    NotificationHandle.getInstance().getNotifications(
      params,
      (isSuccess, responseData) => {
        let data = this.state.data;
        let newData = [];
        if (isSuccess && responseData.code === 0) {
          newData = responseData.data;
          if (newData.length < LIMIT) {
            this.setState({canLoadData: true});
            return;
          }
          data = data.concat(newData);
          this.setState({data, loading: false});
        }
      },
    );
  }
  render() {
    return (
      <View style={{...Styles.container}}>
        <FlatList
          data={this.state.data}
          renderItem={this.renderItem}
          onEndReachedThreshold={0.01}
          onEndReached={() => this.handleLoadMore()}
          keyboardShouldPersistTaps={'handle'}
          ListFooterComponent={this.renderFooter.bind(this)}
          showsVerticalScrollIndicator={false}
        />
        {this.state.data.length === 0 && (
          <EmptyView
            containerStyle={{flex: 1}}
            text={strings('emptyNotification')}
          />
        )}
      </View>
    );
  }
}
