import React, {Component} from 'react';
import {ActivityIndicator, FlatList, View} from 'react-native';
import AppConstants from '../../../../../../resource/AppConstants';
import EmptyView from '../../../../../elements/reminder/EmptyView';
import {strings} from '../../../../../../resource/languages/i18n';
import {PacmanIndicator} from 'react-native-indicators';
import ActiveItem from './ActiveItem';
import SimpleToast from 'react-native-simple-toast';
import OrderHandle from '../../../../../../Api/https/ExecuteHttp';
import DataUtils from '../../../../../../Utils/DataUtils';
import ToolbarMain from '../../../../../elements/toolbar/ToolbarMain';
import {SceneMap, TabView} from 'react-native-tab-view';
import ViewUtils from '../../../../../../Utils/ViewUtils';
import ColorStyle from '../../../../../../resource/ColorStyle';
import Styles from '../../../../../../resource/Styles';
import NotificationPage from './NotificationPage';
export default class ActiveTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: false,
      page: 0,
      canLoadData: true,
      index: this.props.index === undefined ? 0 : this.props.index,

      routes: [
        {key: 'TabNotiGroup', title: strings('group')},
        {key: 'TabOther', title: strings('other')},
      ],
    };
  }

  componentDidMount() {}
  _renderScene = SceneMap({
    TabNotiGroup: () => (
      <NotificationPage type={AppConstants.typeNotification.Teams} />
    ),
    TabOther: () => <NotificationPage type={23} />,
  });
  _renderTabBar = props => {
    return ViewUtils.renderTab(
      props,
      ColorStyle.tabWhite,
      this.state.index,
      i => {
        this.setState({index: i});
      },
    );
  };
  render() {
    return (
      <View style={Styles.container}>
        <ToolbarMain
          title={strings('active')}
          iconBack={true}
          iconShopping={true}
        />
        <TabView
          navigationState={this.state}
          renderScene={this._renderScene}
          renderTabBar={this._renderTabBar}
          onIndexChange={this._handleIndexChange}
        />
      </View>
    );
  }
  keyExtractor = (item, index) => `activeTab_${index.toString()}`;
  _renderItem = ({item, index}) => <View>{this.renderItem(item, index)}</View>;
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
    if (!this.state.loading) {
      return null;
    }
    return <ActivityIndicator style={{color: '#000'}} />;
  };
  handleLoadMore() {
    if (this.state.loading) {
      return;
    }
    if (!this.state.canLoadData) {
      return;
    }
    this.setState({loading: true, page: this.state.page + 1}, () =>
      this.getOrders(),
    );
  }
  renderLoading() {
    if (this.state.loading) {
      return (
        <View>
          <PacmanIndicator color="orange" size={40} />
        </View>
      );
    } else {
      return null;
    }
  }
  getOrders(type) {
    let param = {
      page_size: AppConstants.LIMIT,
      current_page: this.state.page,
    };
    switch (this.props.type) {
      case AppConstants.ORDER_STATUS.ALL:
        break;
      default:
        param = {
          ...param,
          status: [this.props.type],
        };
        break;
    }
    // if (this.state.isStore) {
    //   param = {
    //     ...param,
    //     isStore: true,
    //   };
    // }
    // OrderHandle.getInstance().getOrderByUser(
    //   param,
    //   (isSuccess, dataResponse) => {
    //     let mData = this.state.data;
    //     let newData = [];
    //     if (
    //       isSuccess &&
    //       dataResponse.data !== undefined &&
    //       dataResponse.data.list
    //     ) {
    //       newData = dataResponse.data.list;
    //     }
    //     let data = mData.concat(newData);
    //     let canLoadData = newData.length === Constants.LIMIT;
    //     this.setState({data, canLoadData, loading: false});
    //   },
    // );
  }

  renderItem(item, index) {
    switch (this.props.type) {
      case AppConstants.ORDER_STATUS.ALL:
        return (
          <ActiveItem
            item={item}
            index={index}
            cancelOrderFunc={(item, index) => {
              this.cancelOrder(item, index);
            }}
            completeOrder={(item, index) => {
              this.completeOrder(item, index);
            }}
            hideActionBT={this.state.isStore}
          />
        );
      default:
        return (
          <ActiveItem
            item={item}
            index={index}
            cancelOrderFunc={(item, index) => {
              this.cancelOrder(item, index);
            }}
            completeOrder={(item, index) => {
              this.completeOrder(item, index);
            }}
            hideActionBT={this.state.isStore}
          />
        );
    }
  }
  cancelOrder(item, index) {
    let params = {
      status: AppConstants.ORDER_STATUS.USER_CANCEL,
      reason: strings('userYeuCau'),
    };
    this.updateOrderStatusFunc(item, index, params, isSuccess =>
      SimpleToast.show(
        strings(isSuccess ? 'completeOrderSuccessful' : 'completeOrderFailed'),
        SimpleToast.SHORT,
      ),
    );
  }
  completeOrder(item, index) {
    let params = {
      status: AppConstants.TrangThaiDonHang.COMPLETE,
      reason: strings('userMarkComplete'),
    };
    this.updateOrderStatusFunc(item, index, params, isSuccess =>
      SimpleToast.show(
        strings(isSuccess ? 'orderCancelSuccessful' : 'orderCancelFailed'),
        SimpleToast.SHORT,
      ),
    );
  }
  updateOrderStatusFunc(item, index, params, showFunc) {
    OrderHandle.getInstance().cancelOrder(
      item.id,
      params,
      (isSuccess, responseData) => {
        if (isSuccess) {
          if (this.props.onDataChange != null) {
            this.props.onDataChange(item, index, item.status, params.status);
          }
        }
        showFunc(isSuccess);
      },
    );
  }
  onDataChange(item, index, preStatus, newStatus) {
    let data = this.state.data;
    if (index < 0 || index >= data.length) {
      return;
    }
    item = {
      ...item,
      status: newStatus,
    };
    if (this.props.type === AppConstants.TrangThaiDonHang.TOAN_BO) {
      //Neu tab Toan bo thi chi cap nhat lai trang thai
      data[index] = item;
      this.setState({data: []}, () => {
        this.setState({data});
      });
    } else if (this.props.type === preStatus) {
      //Neu type = preStatus thi xoa item di
      for (let i = 0; i < data.length; i++) {
        let mItem = data[i];
        if (mItem.id === item.id) {
          data = DataUtils.removeItem(data, i);
          this.setState({data: []}, () => {
            this.setState({data});
          });
          return;
        }
      }
    } else if (this.props.type === newStatus) {
      //Neu type = newStatus thi them item do
      data = [item, ...data];
      this.setState({data: []}, () => {
        this.setState({data});
      });
    }
  }
}
