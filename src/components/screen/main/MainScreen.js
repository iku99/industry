import React, {Component} from 'react';
import {Actions} from 'react-native-router-flux';
import {BackHandler} from 'react-native';
import {strings} from '../../../resource/languages/i18n';
import SimpleToast from 'react-native-simple-toast';
import AppConstants from '../../../resource/AppConstants';
import {EventRegister} from 'react-native-event-listeners';
import {BottomNavigation} from 'react-native-paper';
import ColorStyle from '../../../resource/ColorStyle';
import HomeScreen from './home/HomePresentation';
import MenuScreen from './menu/MenuPresentation';
import MessageScreen from './message/MessagePresentation';
import NotificationScreen from './notification/NotificationScreen';
import CartScreen from './cart/CartScreen';
let backCount = 0;
export default class MainScreen extends Component {
  backAction = () => {
    if (Actions.currentScene === '_home') {
      backCount = backCount + 1;
      if (backCount === 2) {
        BackHandler.exitApp();
      } else {
        SimpleToast.show(strings('dupBackToExit'), SimpleToast.SHORT);
      }
      setTimeout(() => {
        backCount = 0;
      }, 1000);
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      index: this.props.index === undefined ? 0 : this.props.index,
      routes: [
        {
          key: 'home',
          title: strings('home'),
          icon: 'home',
        },
        {
          key: 'notification',
          title: strings('notification'),
          icon: 'bell',
        },
        {
          key: 'menu',
          title: strings('category'),
          icon: 'view-grid-outline',
        },
        {
          key: 'cart',
          title: strings('cart'),
          icon: 'cart-minus',
        },
        {
          key: 'message',
          title: strings('message'),
          icon: 'message-processing-outline',
        },
      ],
    };
  }
  componentDidMount() {
    EventRegister.addEventListener(AppConstants.EventName.GO_TO_TAB, index => {
      this.setState({index});
    });
    BackHandler.addEventListener('hardwareBackPress', this.backAction);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backAction);
  }

  _handleIndexChange = index => this.setState({index});

  _renderScene = BottomNavigation.SceneMap({
    home: HomeScreen,
    cart: CartScreen,
    notification: NotificationScreen,
    menu: MenuScreen,
    message: MessageScreen,
  });

  render() {
    return (
      <BottomNavigation
        navigationState={this.state}
        onIndexChange={this._handleIndexChange}
        renderScene={this._renderScene}
        barStyle={{
          backgroundColor: ColorStyle.bottomNavColor,
          height: 73,
          paddingTop: 10,
        }}

        activeColor={ColorStyle.tabActive}
        inactiveColor={'rgba(0, 0, 0, 0.6)'}
        shifting={false}
      />
    );
  }
}
