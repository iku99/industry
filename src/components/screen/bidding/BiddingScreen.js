import React, {Component} from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Styles from '../../../resource/Styles';
import {strings} from '../../../resource/languages/i18n';
import {connect} from 'react-redux';
import Toolbar from '../../elements/toolbar/Toolbar';
import ColorStyle from '../../../resource/ColorStyle';
import DataUtils from '../../../Utils/DataUtils';
import Barchart from '../../elements/viewItem/ViewChart/Barchart';
import {Icon} from 'react-native-elements';
import ContentTab from './ContentTab';
import fetchDataBidding from '../../../Api/fetchDataBidding';
import AppConstants from "../../../resource/AppConstants";
const Data = [
  {
    name: 'Ban quản lý dự án đầu tư xây dựng thành phố Tuy Hòa',
    price: '5000000',
    quantity: 22,
    color: '#FFC120',
  },
  {
    name: 'Công ty Điện lực Thanh Hoá',
    price: '3800000',
    quantity: 18,
    color: '#014B56',
  },
  {
    name: 'Ban quản lý dự án 7',
    price: '2900000',
    quantity: 7,
    color: '#DC0C1F',
  },
  {
    name: 'Công ty cổ phần kiến trúc xây dựng Minh Đức',
    price: '2500000',
    quantity: 7,
    color: '#BABABA',
  },
  {
    name: 'Chi nhánh Tập đoàn Dầu khí Việt Nam Ban Quản lý dự án Điện lực Dầu khí Sông Hậu 1',
    price: '1000000',
    quantity: 7,
    color: '#046fc6',
  },
  {
    name: 'Trung tâm Y tế huyện Bắc Sơn',
    price: '900000',
    quantity: 7,
    color: '#9814a7',
  },
];
const DataDefa=[
    {
        type:AppConstants.BIDDING.BIDDING_VALUE,
    },
    {
        type:AppConstants.BIDDING.SOLICITORS,
    }
]
const X = Styles.constants.X;
class BiddingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: Data,
      loading: this.props.loading,
      pricerMax: 0,
      index: 0,
      dataChile: {},
    };
  }
  componentDidMount() {
    this.selectData(0);
  }

  render() {
    return (
      <View style={Styles.container}>
        <Toolbar
          title={strings('bidding')}
          iconBack={true}
          backgroundColor={ColorStyle.tabActive}
          textColor={ColorStyle.tabWhite}
        />
          <FlatList

              showsVerticalScrollIndicator={false}
              data={DataDefa}
              keyExtractor={this.keyExtractor1}
              renderItem={this.renderItem}
              ListFooterComponent={() => (
                  this.renderTabBidding()
              )}
          />
      </View>
    );
  }
    keyExtractor1 = (item, index) => `biddingScreen1_${index.toString()}`;
    renderItem=({item,index})=>{
        switch (item.type) {
            case AppConstants.BIDDING.BIDDING_VALUE:
                return this.renderValueBidding()
            case AppConstants.BIDDING.SOLICITORS:
                return this.renderQuantityBidding()

        }
    }
  renderValueBidding() {
    return (
      <View style={[Styles.containerItemHomeFullWidth]}>
        <Text
          style={[
            Styles.text.text24,
            {
              color: ColorStyle.textTitle24,
              fontWeight: '700',
              marginTop: 10,
              maxWidth: X * 4.6,
            },
          ]}
          numberOfLines={2}>
          {strings('bidding')}
        </Text>
        <Text
          style={[
            Styles.text.text14,
            {
              color: ColorStyle.tabBlack,
              fontWeight: '700',
              marginTop: 20,
              paddingBottom: 5,
            },
          ]}>
          {strings('top5Bidding')}
        </Text>
        <View
          style={{
            width: Styles.constants.widthScreen,
            borderTopWidth: 1,
            borderColor: ColorStyle.tabBlack,
          }}
        />
        <FlatList
          style={{marginVertical: 15}}
          data={Data}
          keyExtractor={this.keyExtractor}
          renderItem={this._renderItem}
        />
      </View>
    );
  }
  renderQuantityBidding() {
    return (
      <View style={[Styles.containerItemHomeFullWidth]}>
        <Text
          style={[
            Styles.text.text14,
            {
              color: ColorStyle.tabBlack,
              fontWeight: '700',
              marginTop: 20,
              paddingBottom: 5,
            },
          ]}>
          {strings('solicitors')}
        </Text>
        <View
          style={{
            width: Styles.constants.widthScreen,
            borderTopWidth: 1,
            borderColor: ColorStyle.tabBlack,
          }}
        />
        <FlatList
          style={{marginVertical: 15}}
          data={Data}
          keyExtractor={this.keyExtractor}
          renderItem={this._renderItemQuantity}
        />
      </View>
    );
  }
  renderTabBidding() {
    let i = this.state.index;
    return (
      <View
        style={{}}>
        <View
          style={{
            flexDirection: 'row',
            borderColor: ColorStyle.tabBlack,
            borderBottomWidth: 1,
            paddingTop:10,
            backgroundColor: ColorStyle.tabWhite, marginTop: 20, flex: 1
          }}>
          <TouchableOpacity
            onPress={() => this.selectData(0)}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              paddingBottom: 16,
              borderBottomWidth: i === 0 ? 3 : 1,
              borderColor: i === 0 ? ColorStyle.tabActive : ColorStyle.tabBlack,
            }}>
            <Icon
              name={'shopping-bag'}
              color={i === 0 ? ColorStyle.tabActive : ColorStyle.tabBlack}
              type={'feather'}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.selectData(1)}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              paddingBottom: 16,
              borderBottomWidth: i === 1 ? 3 : 1,
              borderColor: i === 1 ? ColorStyle.tabActive : ColorStyle.tabBlack,
            }}>
            <Icon
              name={'construct-outline'}
              color={i === 1 ? ColorStyle.tabActive : ColorStyle.tabBlack}
              type={'ionicon'}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.selectData(2)}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              paddingBottom: 16,
              borderBottomWidth: i === 2 ? 3 : 1,
              borderColor: i === 2 ? ColorStyle.tabActive : ColorStyle.tabBlack,
            }}>
            <Icon
              name={'ios-chatbubbles-outline'}
              color={i === 2 ? ColorStyle.tabActive : ColorStyle.tabBlack}
              type={'ionicon'}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.selectData(3)}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              paddingBottom: 16,
              borderBottomWidth: i === 3 ? 3 : 1,
              borderColor: i === 3 ? ColorStyle.tabActive : ColorStyle.tabBlack,
            }}>
            <Icon
              name={'shuttle-van'}
              color={i === 3 ? ColorStyle.tabActive : ColorStyle.tabBlack}
              type={'font-awesome-5'}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.selectData(4)}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              paddingBottom: 16,
              borderBottomWidth: i === 4 ? 3 : 1,
              borderColor: i === 4 ? ColorStyle.tabActive : ColorStyle.tabBlack,
            }}>
            <Icon
              name={'chemistry'}
              color={i === 4 ? ColorStyle.tabActive : ColorStyle.tabBlack}
              type={'simple-line-icon'}
              style={15}
            />
          </TouchableOpacity>
        </View>
        {this.renderTab(i)}
      </View>
    );
  }

  renderTab(i) {
    if (i === 0) {
      return <ContentTab item={this.state.dataChile} />;
    } else if (i === 1) {
      return <ContentTab item={this.state.dataChile} />;
    } else if (i === 2) {
      return <ContentTab item={this.state.dataChile} />;
    } else if (i === 3) {
      return <ContentTab item={this.state.dataChile} />;
    } else {
      return <ContentTab item={this.state.dataChile} />;
    }
  }
  keyExtractor = (item, index) => `biddingScreen_${index.toString()}`;
  _renderItem = ({item, index}) => {
    let priceMax = DataUtils.formatPriceMax(this.state.data);
    return (
      <Barchart
        item={item}
        value={true}
        pricerMax={priceMax}
        index={index}
        loading={this.state.loading}
      />
    );
  };
  _renderItemQuantity = ({item, index}) => {
    let priceMax = DataUtils.formatValueMax(this.state.data);
    return (
      <Barchart
        value={false}
        item={item}
        pricerMax={priceMax}
        index={index}
        loading={this.state.loading}
      />
    );
  };

  selectData(index) {
    let data = fetchDataBidding.data;
    let newData = {};
    data.forEach(item => {
      if (item.id === index) {
        newData = {
          ...item,
        };
      }
    });
    this.setState({dataChile: newData,index:index});
  }
}
//lấy dữ liệu về
const mapStateToProps = state => {
  return {};
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {
    onFetchMovies: () => {},
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(BiddingScreen);
