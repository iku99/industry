import React, {Component} from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import Styles from '../../../resource/Styles';
import ToolbarMain from '../../elements/toolbar/ToolbarMain';
import {strings} from '../../../resource/languages/i18n';
import {SearchBar} from 'react-native-screens';
import {connect} from 'react-redux';
import ColorStyle from '../../../resource/ColorStyle';
import CurrencyFormatter from '../../../Utils/CurrencyFormatter';
import ImageHelper from '../../../resource/images/ImageHelper';
import {Icon} from 'react-native-elements';
import DateUtil from '../../../Utils/DateUtil';
import RBSheet from 'react-native-raw-bottom-sheet';
class ContentTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: {},
    };
  }

  render() {
    let data = this.props.item;
    return (
      <View style={{}}>
        <View
          style={{
            borderBottomColor: ColorStyle.borderItemHome,
            elevation: 0.5,
            borderBottomWidth: 1,
            paddingHorizontal: Styles.constants.marginHorizontalAll,

            backgroundColor: ColorStyle.tabWhite,
          }}>
          <Text
            style={{
              ...Styles.text.text18,
              color: ColorStyle.tabActive,
              fontWeight: '700',
              marginVertical: 10,
            }}>
            {data.name}
          </Text>
          <Text
            style={{
              ...Styles.text.text10,
              color: ColorStyle.tabActive,
              fontWeight: '500',
            }}>
            {CurrencyFormatter(data.price)}
          </Text>
          <Text
            style={{
              ...Styles.text.text10,
              color: ColorStyle.tabActive,
              fontWeight: '500',
              marginVertical: 10,
            }}>
            {data.child_package?.length} {strings('biddingPa')}
          </Text>
        </View>
        <View
          style={{
            marginTop: 20,
            paddingHorizontal: Styles.constants.marginHorizontalAll,
            backgroundColor: ColorStyle.tabWhite,
          }}>
          <Text
            style={[
              Styles.text.text14,
              {
                color: ColorStyle.tabBlack,
                fontWeight: '700',
                marginTop: 20,
                paddingBottom: 5,
              },
            ]}>
            {strings('namePacket')}
          </Text>
          <View
            style={{
              width: Styles.constants.widthScreenMg24,
              borderTopWidth: 1,
              borderColor: ColorStyle.tabBlack,
            }}
          />
          <FlatList
            style={{marginVertical: 15}}
            data={data.child_package}
            keyExtractor={this.keyExtractor}
            renderItem={this._renderItemPacket}
          />
        </View>
        {this.renderViewInfo()}
      </View>
    );
  }
  renderViewInfo() {
    let data = this.state.item;
    return (
      <RBSheet
        ref={ref => {
          this.RBSheet = ref;
        }}
        height={(Styles.constants.heightScreen / 2)+20}
        openDuration={250}
        closeOnDragDown={true}
        closeOnPressMask={true}
        customStyles={{
          container: {},
        }}>
        <View
          style={{
            width: Styles.constants.widthScreen,
            justifyContent: 'space-between',
              paddingHorizontal:Styles.constants.X/4,

          }}>
          <Text
            style={
              {
                  ...Styles.text.text24,
                color: ColorStyle.textTitle24,
                fontWeight: '700',
                marginTop: 10,
                  textAlign:'center'
              }}>
            {strings('details')}
          </Text>
          <View
            style={{
              width: '100%',
              marginTop: 20,

              backgroundColor: ColorStyle.tabWhite,
              borderRadius: 15,
              elevation: 2,
              borderColor: 'rgba(51, 51, 51, 0.25)',
            }}>
            <View
              style={{
                width: '100%',
                alignItems: 'center',
                flexDirection: 'row',
                borderBottomWidth: 1,
                borderColor: 'rgba(51, 51, 51, 0.25)',
                padding: 14,

              }}>
              <Image
                source={ImageHelper.icon_bidPacket}
                style={Styles.icon.iconOrder}
              />
             <View style={{marginLeft:10}}>
               <Text
                 style={{
                   ...Styles.text.text14,
                   fontWeight: '700',

                   color: ColorStyle.tabBlack,
                 }}>
                 {strings('namePacket')}
               </Text>
               <Text
                 style={{
                   ...Styles.text.text14,
                   color: ColorStyle.tabBlack,
                 }}>
                 {data.name_project}
               </Text>
             </View>
            </View>
              <View
                style={{
                  alignItems: 'center',
                  flexDirection: 'row',
                  borderBottomWidth: 1,
                  borderColor: 'rgba(51, 51, 51, 0.25)',
                  padding: 14,

                }}>
                <Image
                  source={ImageHelper.icon_bid}
                  style={Styles.icon.iconOrder}
                />
                <View style={{marginLeft:10}}>
                  <Text
                    style={{
                      ...Styles.text.text14,
                      fontWeight: '700',

                      color: ColorStyle.tabBlack,
                    }}>
                    {strings('solicitor')}
                  </Text>
                  <Text
                    numberOfLines={1}
                    style={{
                      ...Styles.text.text14,
                      color: ColorStyle.tabBlack,
                    }}>
                    {data.name_unit}
                  </Text>
                </View>
              </View>
              <View
                  style={{
                      alignItems: 'center',
                      flexDirection: 'row',
                      borderBottomWidth: 1,
                      borderColor: 'rgba(51, 51, 51, 0.25)',
                      padding: 14,

                  }}>
                  <Image
                      source={ImageHelper.icon_bidTime}
                      style={Styles.icon.iconOrder}
                  />
                  <View style={{marginLeft:10}}>
                      <Text
                          style={{
                              ...Styles.text.text14,
                              fontWeight: '700',

                              color: ColorStyle.tabBlack,
                          }}>
                          {strings('bidDate')}
                      </Text>
                      <Text
                          numberOfLines={1}
                          style={{
                              ...Styles.text.text14,
                              color: ColorStyle.tabBlack,
                          }}>
                          {DateUtil.formatDate('DD.MM.YYYY', data.createdDate)}
                      </Text>
                  </View>
              </View>
              <View
                  style={{
                      alignItems: 'center',
                      flexDirection: 'row',
                      borderBottomWidth: 1,
                      borderColor: 'rgba(51, 51, 51, 0.25)',
                      padding: 14,

                  }}>
                 <Icon name={'location'} type={'entypo'} size={20} color={ColorStyle.tabActive}/>
                  <View style={{marginLeft:10}}>
                      <Text
                          style={{
                              ...Styles.text.text14,
                              fontWeight: '700',

                              color: ColorStyle.tabBlack,
                          }}>
                          {strings('bidAddress')}
                      </Text>
                      <Text
                          numberOfLines={1}
                          style={{
                              ...Styles.text.text14,
                              color: ColorStyle.tabBlack,
                          }}>
                          {data.address_perform}
                      </Text>
                  </View>
              </View>
          </View>
        </View>
      </RBSheet>
    );
  }
  _renderItemPacket = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          this.RBSheet.open(), this.setState({item: item});
        }}
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginBottom: 19,
          borderColor: ColorStyle.optionTextColor,
          borderBottomWidth: 0.5,
          paddingBottom: 5,
        }}>
        <Text
          style={[
            Styles.text.text12,
            {
              fontWeight: '500',
              color: ColorStyle.tabBlack,
              flex: 2,
            },
          ]}
          numberOfLines={1}>
          {item.name_project}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center',
            flex: 1,
          }}>
          <Icon name="clock" type="feather" size={10} />
          <Text
            style={[
              Styles.text.text10,
              {fontWeight: '400', color: ColorStyle.tabBlack, marginLeft: 5},
            ]}>
            {DateUtil.formatDate('HH:mm DD.MM.YYYY', item.createdDate)}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
}
//lấy dữ liệu về
const mapStateToProps = state => {
  return {};
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {
    onFetchMovies: () => {},
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ContentTab);
