import React, {Component} from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import Styles from '../../../resource/Styles';
import TitleLogin from '../../elements/TitleLogin';
import {strings} from '../../../resource/languages/i18n';
import ColorStyle from '../../../resource/ColorStyle';
import Ripple from 'react-native-material-ripple';
import OtherLogin from '../../elements/OtherLogin';
import {Actions} from 'react-native-router-flux';
import FloatLabelTextField from '../../elements/FloatLabelTextInput';
import {Icon} from 'react-native-elements';
import RegisterByPhone from './RegisterByPhone';
import RegisterByEmail from './RegisterByEmail';
import { DotIndicator } from "react-native-indicators";
import LoadingView from "../../elements/LoadingView";
let checkUserId = -1;
let checkPasswordId = -1;
class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      registerByEmail: true,
      token: '',
      loading:false
    };
  }

  render() {
    return (
      <ScrollView horizontal={false} showsVerticalScrollIndicator={false} style={Styles.container}>
        <TitleLogin title={strings('signUp')} />
        {this.renderInputForm()}
        <View>
          <OtherLogin
            title={strings('userNameYes')}
            titleBT={strings('signIn')}
            callback={() => {
              Actions.jump('login');
            }}
          />
          <Text
            style={[
              Styles.text.text12,
              {
                color: ColorStyle.optionTextColor,
                maxWidth: Styles.constants.widthScreen - 100,
                flexDirection: 'row',
                marginVertical: 10,
                marginHorizontal: 50,
                alignItems: 'center',
                textAlign: 'center',
              },
            ]}>
            {strings('note')}
            {
              <Text style={{color: ColorStyle.tabBlack}}>
                {' '}
                {strings('termsService')}{' '}
              </Text>
            }
            {strings('and')}
            {
              <Text style={{color: ColorStyle.tabBlack}}>
                {' '}
                {strings('privacyPolicy')}
              </Text>
            }
          </Text>
        </View>
          <LoadingView loading={this.state.loading}/>
      </ScrollView>
    );
  }
  renderInputForm() {
    if (this.state.registerByEmail) {
      return (
        <RegisterByEmail
          onChangeType={v => {
            this.onChange(v);
          }}
          token={this.state.token}
          setLoading={(t)=>this.setState({loading:t})}
        />
      );
    } else {
      return (
        <RegisterByPhone
          onChangeType={() => {
            this.onChange(true);
          }}
          getToken={token => {
            this.setState({registerByEmail: true, token: token});
          }}
        />
      );
    }
  }

  onChange(registerByEmail) {
    this.setState({registerByEmail: registerByEmail});
  }
}
//lấy dữ liệu về
const mapStateToProps = state => {
  return {};
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {};
};
const RegisterScreen = connect(mapStateToProps, mapDispatchToProps)(Register);
export default RegisterScreen;
