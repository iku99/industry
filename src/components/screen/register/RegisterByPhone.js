import React, {Component} from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import GlobalUtil from '../../../Utils/Common/GlobalUtil';
import {strings} from '../../../resource/languages/i18n';
import FloatLabelTextField from '../../elements/FloatLabelTextInput';
import {Icon} from 'react-native-elements';
import Styles from '../../../resource/Styles';
import ColorStyle from '../../../resource/ColorStyle';
import Ripple from 'react-native-material-ripple';
import DataUtils from '../../../Utils/DataUtils';
import SimpleToast from "react-native-simple-toast";

let checkPhoneId = -1;
let checkNameId = -1;
let phoneNumber = '';
export default class RegisterByPhone extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      otp: '',
      stepEnterOTP: true,
      validPhone: false,
      validOTP: false,
      canSendOtpAgain: true,
      countDownTimeSendOtp: '60',
    };
  }
  render() {
    return (
      <View>
        {this.renderInput('phone', strings('phone'), 'validPhone', () => {
          this.setState({validPhone: DataUtils.validPhone(this.state.phone)});
        })}
        {this.state.stepEnterOTP &&
          this.renderInput('otp', strings('otp'), 'validOTP', () => {
            this.setState({validOTP: true});
            // this.setState({validOTP:this.state.otp.length === 6})
          })}
        <View
          animation="fadeInLeftBig"
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-end',
            padding: 15,
            width: '100%',
          }}>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => {
              this.props.onChangeType(false);
            }}>
            <Text style={{color: ColorStyle.tabActive}}>{`${strings(
              'signUpEmail',
            )}?`}</Text>
            {/*<Icon name='arrow-right' type='feather' size={15} containerStyle={{paddingHorizontal: 5}}/>*/}
          </TouchableOpacity>
        </View>
        {this.state.stepEnterOTP && (
          <TouchableOpacity
            style={[
              Styles.button.buttonLogin,
              {
                backgroundColor: this.state.canSendOtpAgain
                  ? ColorStyle.tabActive
                  : ColorStyle.tabWhite,

                borderColor: 'rgba(250, 111, 52, 0.5)',
                borderWidth: 1,
                marginTop: 24,
              },
            ]}
            onPress={() => {
                SimpleToast.show(strings('test'),SimpleToast.SHORT)
              // this.getOTP();
            }}>
            <Text
              style={{
                color: this.state.canSendOtpAgain
                  ? ColorStyle.tabWhite
                  : ColorStyle.tabActive,
                fontSize: 16,
              }}>
              {this.state.canSendOtpAgain
                ? strings('sendOtpAgain')
                : `${strings('sendOtpAgain')} (${
                    this.state.countDownTimeSendOtp
                  })`}
            </Text>
          </TouchableOpacity>
        )}
        <TouchableOpacity
          style={[
            Styles.button.buttonLogin,
            {
              backgroundColor: this.state.canSendOtpAgain
                ? ColorStyle.bgButtonFal
                : ColorStyle.tabActive,
              borderColor: this.state.canSendOtpAgain
                ? ColorStyle.bgButtonFal
                : ColorStyle.tabActive,
              borderWidth: 1,
              marginTop: 24,
              marginBottom: 50,
            },
          ]}
          activeOpacity={this.state.canSendOtpAgain ? 1 : 0.7}
          onPress={() => !this.state.canSendOtpAgain && alert('hieu')}>
          <Ripple animation="fadeInUpBig">
            <Text style={[Styles.text.text18, {fontWeight: '500'}]}>
              {strings('signIn')}
            </Text>
          </Ripple>
        </TouchableOpacity>
      </View>
    );
  }

  renderInput(valueName, placeholder, validState, callback) {
    let mValidStateName = this.state[valueName];
    return (
      <View style={{marginTop: 10}}>
        <TextInput
          style={[Styles.input.inputLogin, {alignSelf: 'center'}]}
          placeholder={placeholder}
          value={this.state[valueName]}
          placeholderTextColor={ColorStyle.textInput}
          onChangeText={text => {
            this.setState({[valueName]: text}, callback);
          }}
        />
        {!DataUtils.stringNullOrEmpty(validState) && (
          <Icon
            name={mValidStateName ? '' : 'close'}
            type="antdesign"
            size={18}
            color={mValidStateName ? ColorStyle.tabActive : ColorStyle.red}
            containerStyle={{
              position: 'absolute',
              right: 30,
              display: this.state[valueName] === '' ? 'none' : 'flex',
              alignItems: 'flex-end',
              bottom: '30%',
            }}
          />
        )}
      </View>
    );
  }

  coundownTime() {
    setTimeout(() => {
      if (this.state.countDownTimeSendOtp > 0) {
        this.setState(
          {countDownTimeSendOtp: this.state.countDownTimeSendOtp - 1},
          () => this.coundownTime(),
        );
      } else {
        this.setState({canSendOtpAgain: true});
      }
    }, 1000);
  }
  getOTP() {
    // if (
    //   !this.state.canSendOtpAgain ||
    //   !this.state.validPhone ||
    //   !this.state.canSendOtpAgain
    // ) {
    //   return;
    // }
    this.setState({canSendOtpAgain: false});
    // phoneNumber = this.state.phone;
    // let param = {
    //   phone: phoneNumber,
    // };
    // AccountHandle.getInstance().registerPhone(
    //   param,
    //   (isSuccess, dataResponse) => {
    //     if (isSuccess && dataResponse.data) {
    //       this.setState({
    //         stepEnterOTP: true,
    //         countDownTimeSendOtp: 60,
    //         canSendOtpAgain: false,
    //       });
    this.coundownTime();
    //     } else {
    //       ViewUtil.showAlertDialog(strings('phoneAlreadyRegister'));
    //     }
    //   },
    // );
  }
  verifyPhone() {
    // if (this.state.otp === '') {
    //   return;
    // }
    // let params = {
    //   otp: this.state.otp,
    //   phone: phoneNumber,
    // };
    // AccountHandle.getInstance().verifyPhone(
    //   params,
    //   (isSuccess, dataResponse) => {
    //     if (isSuccess && dataResponse.data) {
    //       let token = dataResponse.data.register_token;
    //       if (token != null) {
    //         this.props.getToken(token);
    //         return;
    //       }
    //     }
    //     ViewUtil.showAlertDialog(strings('phoneVerifyFailed'));
    //   },
    // );
  }
}
