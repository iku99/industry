import React, {Component} from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import {strings} from '../../../resource/languages/i18n';
import {Icon} from 'react-native-elements';
import Styles from '../../../resource/Styles';
import ColorStyle from '../../../resource/ColorStyle';
import DataUtils from '../../../Utils/DataUtils';
import {getBannerInfoAction, postRegister} from '../../../actions';
import {connect} from 'react-redux';
import ViewUtils from '../../../Utils/ViewUtils';
import AccountHandle from '../../../sagas/AccountHandle';
import {EventRegister} from 'react-native-event-listeners';
import AppConstants from '../../../resource/AppConstants';
import {Actions} from 'react-native-router-flux';
import SimpleToast from 'react-native-simple-toast';
class RegisterByEmail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      email: '',
      name: '',
      password: '',
      confirmPassword: '',
      validEmail: false,
      validEmailExist: false,
      validName: false,
      validPass: false,
      validConfirmPass: false,
      showPass: false,
      showConfirmPass: false,
    };
  }
  render() {
    return (
      <View>
        {this.renderInput('name', strings('name'), 'validName', () =>
          this.checkName(),
        )}
        {this.renderInput('email', strings('email'), 'validEmail', () =>
          this.checkEmail(),
        )}

        {this.renderInputPass(
          'password',
          strings('password'),
          'validPass',
          () => this.checkPassword(),
          'showPass',
        )}
        {this.renderInputPass(
          'confirmPassword',
          strings('passwordRe'),
          'validConfirmPass',
          () => this.checkConfirmPass(),
          'showConfirmPass',
        )}
        <View
          animation="fadeInLeftBig"
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-end',
            padding: 15,
            width: '100%',
          }}>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => {
              this.props.onChangeType(false);
            }}>
            <Text style={{color: ColorStyle.tabActive}}>{`${strings(
              'signUpPhone',
            )}?`}</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={() => this.register()}
          animation="fadeInUpBig"
          style={[
            Styles.button.buttonLogin,
            {
              backgroundColor: ColorStyle.tabActive,
              borderColor: 'rgba(250, 111, 52, 0.5)',
              borderWidth: 1,
              marginTop: 24,
              marginBottom: 50,
            },
          ]}>
          <Text style={[Styles.text.text18, {fontWeight: '500'}]}>
            {strings('signUp')}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
  renderInput(valueName, placeholder, validState, callback) {
    let mValidStateName = this.state[validState];
    return (
      <View style={{marginTop: 10}}>
        <TextInput
          style={[Styles.input.inputLogin, {alignSelf: 'center'}]}
          placeholder={placeholder}
          value={this.state[valueName]}
          placeholderTextColor={ColorStyle.textInput}
          onChangeText={text => {
            this.setState({[valueName]: text}, callback);
          }}
        />
        {mValidStateName !== null && (
          <Icon
            name={mValidStateName ? '' : 'close'}
            type="antdesign"
            size={18}
            color={mValidStateName ? ColorStyle.tabActive : ColorStyle.red}
            containerStyle={{
              position: 'absolute',
              right: 30,
              display: this.state[valueName] === '' ? 'none' : 'flex',
              alignItems: 'flex-end',
              bottom: '30%',
            }}
          />
        )}
      </View>
    );
  }

  renderInputPass(valueName, placeholder, validState, callback, showPass) {
    let mValidStateName = this.state[validState];
    let mShowPass = this.state[showPass];
    return (
      <View style={{marginTop: 10}}>
        <TextInput
          style={[Styles.input.inputLogin, {alignSelf: 'center'}]}
          placeholder={placeholder}
          value={this.state[valueName]}
          placeholderTextColor={ColorStyle.textInput}
          secureTextEntry={!mShowPass}
          onChangeText={text => {
            this.setState({[valueName]: text}, callback);
          }}
        />
        {mShowPass !== null && (
          <Icon
            name={mShowPass ? 'eye' : 'eye-with-line'}
            type="entypo"
            onPress={() => this.setState({[showPass]: !mShowPass})}
            size={18}
            color={ColorStyle.tabBlack}
            containerStyle={{
              position: 'absolute',
              right: 30,
              alignItems: 'flex-end',
              bottom: '30%',
            }}
          />
        )}
        {mValidStateName !== null && (
          <Icon
            name={mValidStateName ? '' : 'close'}
            type="antdesign"
            size={20}
            color={ColorStyle.tabActive}
            containerStyle={{
              position: 'absolute',
              right: 50,
              bottom: 15,
              display: this.state[valueName] === '' ? 'none' : 'flex',
            }}
          />
        )}
      </View>
    );
  }
  checkName() {
    let name = this.state.name;
    let result = false;
    if (name.length >= 6) {
      result = true;
    }
    this.setState({validName: result});
  }
  checkEmail() {
    this.setState({validEmail: DataUtils.validMail(this.state.email)});
  }
  checkEmailApi() {
    let param = {
      email: this.state.email,
    };
    AccountHandle.getInstance().checkExistedEmail(param, (isSuccess, data) => {
      let valid = false;
      if (isSuccess && data.code != 1) {
        valid = true;
      }
      this.setState({validEmailExist: valid});
    });
  }
  checkPassword() {
    this.checkEmailApi();
    let password = this.state.password;
    let result = false;
    if (password.length >= 8) {
      result = true;
    }
    this.setState({validPass: result});
  }
  checkConfirmPass() {
    let password = this.state.password;
    let confirmPass = this.state.confirmPassword;
    let result = false;
    if (this.state.validPass && password === confirmPass) {
      result = true;
    }
    this.setState({validConfirmPass: result});
  }
  register() {
    if (!this.state.validName) {
      SimpleToast.show(strings('registerInfoInvalidName'), SimpleToast.SHORT);
      return;
    } else if (!this.state.validEmail) {
      SimpleToast.show(strings('registerInfoEmail'), SimpleToast.SHORT);
      return;
    } else if (!this.state.validConfirmPass) {
      SimpleToast.show(strings('registerPassword'), SimpleToast.SHORT);
      return;
    } else if (!this.state.validEmailExist) {
      SimpleToast.show(strings('registerEmail'), SimpleToast.SHORT);
      return;
    }
    let params = {
      full_name: this.state.name,
      email: this.state.email,
      user_name: this.state.email,
      password: this.state.password,
    };
    this.props.setLoading(true);
    AccountHandle.getInstance().register(params, (isSuccess, responseData) => {
      console.log(responseData);
      if (isSuccess) {
        if (responseData.data !== undefined) {
          this.props.setLoading(false);
          let params = {
            user_name: this.state.email,
            password: this.state.password,
            ecommerce_id: AppConstants.EventName.ECOMMERCE,
          };
          Actions.jump('checkOtp', {params: params});
          return;
        }
      }
      this.props.setLoading(false);
      ViewUtils.showAlertDialog(strings('cannotRegister'), () =>
        this.props.setLoading(false),
      );
    });
  }
}
const mapStateToProps = state => {
  return {
    // banner: state.bannerReducers,
  };
};
//gửi giữ liệu lên
const mapDispatchToProps = dispatch => {
  return {
    onGetBannerInfo: param => {
      dispatch(getBannerInfoAction(param));
    },
    postRegister: param => {
      dispatch(postRegister(param));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(RegisterByEmail);
