import React, {Component} from 'react';
import {FlatList, Text, TextInput, TouchableOpacity, View} from 'react-native';
import Styles from '../../../resource/Styles';
import ToolbarMain from '../../elements/toolbar/ToolbarMain';
import {strings} from '../../../resource/languages/i18n';
import ColorStyle from '../../../resource/ColorStyle';
import DateUtil from '../../../Utils/DateUtil';
import {CheckBox} from 'react-native-elements';
import styles from "./styles";
import AppConstants from "../../../resource/AppConstants";
import VoucherHandle from "../../../sagas/VoucherHandle";
import TimeUtils from "../../../Utils/TimeUtils";
import VoucherEcommerceItem from "../../elements/viewItem/ItemVoucher/VoucherEcommerceItem";
import VoucherStoreItem from "../../elements/viewItem/ItemVoucher/VoucherStoreItem";
import EmptyView from "../../elements/reminder/EmptyView";
import CheckoutUtils from "../../../Utils/CheckoutUtils";
import {Actions} from "react-native-router-flux";
const listData=[
    {
        type:1
    },
    {
        type:2
    },
    {
        type:3
    }
]
export default class VoucherList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: listData,
            listVoucherTransport:[],
            listVoucherProduct:[],
            indexSelected: 0,
            page:1,
            textSearch:'',
            showListSearch:false,
            errorCode:false,
            listSearch:[],
            listProduct:undefined
        };
    }

    componentDidMount() {
        let data=CheckoutUtils.getVoucherEco(this.props.item.list_product)
        let list_product={
            sum:this.props.item.discount_detail.total,
            list_product:data
        }
        this.setState({listProduct:list_product})
        this.getVoucherTransport(list_product)
        this.getVoucherProduct(list_product)

    }
    selectVoucherTransport(data){
        let idVoucher=this.props.item.voucher?.transport?.id
        console.log(idVoucher)
        if(idVoucher !==undefined){
            let newArray = data.map(e => {
                if (idVoucher === e.id) {
                    return {
                        ...e,
                        checked: !e.checked,
                    };
                }
                return {
                    ...e,
                    checked: false,
                };
            });
            this.setState({listVoucherTransport:newArray})
            return
        }


    }
    selectVoucherProduct(data){
        let idVoucher=this.props.item.voucher?.product?.id
        if(idVoucher !==undefined){
            let newArray = data.map(e => {
                if (idVoucher === e.id) {
                    return {
                        ...e,
                        checked: !e.checked,
                    };
                }
                return {
                    ...e,
                    checked: false,
                };
            });
            this.setState({listVoucherProduct:newArray})
            return
        }
    }
    getVoucherTransport(data){
        let body=data
        let params={
            page_index:1,
            page_size:10,
            voucher_type:0
        }
        VoucherHandle.getInstance().getListVoucherEco(params,body,(isSuccess,responseData)=>{
            console.log('responseData:',responseData);
            if(isSuccess && responseData.code===0){
                let data=this.state.listVoucherTransport;
                let mData=responseData.data.data;
                mData=mData.map(item=>{
                    return {
                        ...item,
                        checked:false
                    }
                })
                // if(mData.length <1){
                //     this.setState({canLoadData:true})
                //     return
                // }
                // data=data.concat(mData)
                this.setState({listVoucherTransport:mData,loading:false},()=>{
                    if(this.props.item.voucher!==undefined){
                        this.selectVoucherTransport(mData)
                    }
                })
            }
        })
    }
    getVoucherProduct(data){
        let body=data
        let params={
            page_index:1,
            page_size:10,
            voucher_type:1
        }
        VoucherHandle.getInstance().getListVoucherEco(params,body,(isSuccess,responseData)=>{
            if(isSuccess && responseData.code===0){
                let mData=responseData.data.data;
                mData=mData.map(item=>{
                    return {
                        ...item,
                        checked:false
                    }
                })

                this.setState({listVoucherProduct:mData,loading:false},()=>{
                    if(this.props.item.voucher!==undefined){
                        this.selectVoucherProduct(mData)
                    }
                })
            }
        })
    }
    render() {
        return (
            <View style={Styles.container}>
                <ToolbarMain
                    title={strings('titleVoucher')}
                    iconProfile={true}
                    iconBack={true}
                />
                {this.renderSearchVoucher()}

                {this.renderBody()}
                {this.renderBottom()}
            </View>
        );
    }
    renderBody(){
        if(this.state.showListSearch){
            return(
                <View style={{...styles.body}}>
                    <Text style={styles.textTitle}>{strings('kq')}</Text>
                    <FlatList data={this.state.listSearch}
                              keyExtractor={this.keyExtractorAll}
                              renderItem={this.renderItemSearch}

                    />
                </View>
            )
        }else {
            return (
                <FlatList data={this.state.data}
                          keyExtractor={this.keyExtractorAll}
                          renderItem={({item})=>{return  this._renderItem(item)}}
                />
            )
        }
    }
    _renderItem = (item) => {
        switch (item.type) {
            case 1:
                return this.renderVoucherTransport();
            case 2:
                return this.renderVoucherProduct();
            default:
                break;
        }
    }
    searchVoucher(){
        let param={
            code:this.state.textSearch,
            ...this.state.listProduct
        }
        if(this.state.textSearch ===''){
            this.setState({errorCode:true})
            return
        }
        VoucherHandle.getInstance().getVoucherByCode(param,(isSuccess,responseData)=>{
            if(isSuccess && responseData.code===0){
                let mData=responseData.data;
                mData=mData.map(item=>{
                    return {
                        ...item,
                        checked:false
                    }
                })
                this.setState({showListSearch:true,listSearch:mData})
                return
            }
            this.setState({errorCode:true})
        })
      }
    renderSearchVoucher(){
        return(
            <View style={{...styles.body,marginTop:0,}}>
                <View style={{height:Styles.constants.X*0.9,flexDirection:'row'}}>
                    <TextInput style={styles.viewSearch}
                               value={this.state.textSearch}
                               placeholder={strings('searchVoucher')}
                               placeholderTextColor={ColorStyle.gray}
                               onChangeText={(text)=>{
                                   this.setState({textSearch:text})
                               }}
                    />
                    <TouchableOpacity style={styles.btnSearch} onPress={()=>this.searchVoucher()}>
                        <Text style={styles.textSearch}>{strings('apply')}</Text>
                    </TouchableOpacity>
                </View>
                <Text style={{...styles.errorCode,display:!this.state.errorCode?'none':'flex'}}>{strings('errorCode')}</Text>
            </View>
        )
    }
    keyExtractor = (item, index) => `renderVoucherTransport_${index.toString()}`;
    keyExtractor1 = (item, index) => `renderVoucherProduct_${index.toString()}`;
    keyExtractorAll = (item, index) => `renderVoucherProduct_${index.toString()}`;
    renderVoucherTransport(){
        return(
            <View style={{...styles.body}}>
                <Text style={styles.textTitle}>{strings('voucherTransport')}</Text>
                <FlatList
                    data={this.state.listVoucherTransport}
                    style={{marginVertical:Styles.constants.X/5}}
                    keyExtractor={this.keyExtractor}
                    showsHorizontalScrollIndicator={false}
                    renderItem={this.renderItem} />
            </View>
        )
    }
    renderVoucherProduct(){
        return(
            <View style={{...styles.body}}>
                <Text style={styles.textTitle}>{strings('voucherProduct')}</Text>
                <FlatList
                    data={this.state.listVoucherProduct}
                    style={{marginVertical:Styles.constants.X/5}}
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={this.keyExtractor1}
                    renderItem={this.renderItemStore} />
                    {this.state.listVoucherProduct.length===0 &&(
                        <View
                            style={{

                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                            <EmptyView text={strings('notVoucher')} />

                        </View>
                    )}
            </View>
        )
    }
    renderBottom(){
        return(
            <TouchableOpacity onPress={()=>this.onAddVoucher()} style={styles.renderBottom}>
                <Text style={styles.textSearch}>{strings('apply')}</Text>

            </TouchableOpacity>
        )
    }
    onAddVoucher(){
        let listVoucherTransport=this.state.listVoucherTransport
        let listVoucherProduct=this.state.listVoucherProduct
        let listVoucherSearch=this.state.listSearch
        let itemVoucherTransport;
        let itemVoucherProduct;
        let quantityVoucher=0
        if(this.state.showListSearch){
            listVoucherSearch.map(e => {
                if(e.checked){
                    if(e.voucher_type !==AppConstants.VOUCHER.VOUCHER_TYPE[0].value){
                        return itemVoucherTransport=e
                    }
                    return itemVoucherProduct=e
                }
            })
        }
        if(!this.state.showListSearch){
            listVoucherTransport.map(e => {
                if(e.checked){
                    return itemVoucherTransport=e
                }
            })
            listVoucherProduct.map(e => {
                if(e.checked){
                    return itemVoucherProduct=e
                }
                return
            })
        }

        let listProduct=this.state.listProduct;
        let param=listProduct
        let voucher={};
        if(itemVoucherTransport !==undefined){
            param={
                ...param,
                voucher_transport:{
                    ...itemVoucherTransport.detail_discount[0],
                    id:itemVoucherTransport.id
                }
            }
            quantityVoucher=quantityVoucher+1,
             voucher={
                 ...voucher,
                 transport:{
                     id:itemVoucherTransport.id
                 }
             }
        }
        if (itemVoucherProduct !==undefined){
            param={
                ...param,
                voucher_product:{
                    ...itemVoucherProduct.detail_discount[0],
                    id:itemVoucherProduct.id
                }
            }
            quantityVoucher=quantityVoucher+1,
            voucher={
                ...voucher,
                product:{
                    id:itemVoucherProduct.id
                }
            }
        }
        VoucherHandle.getInstance().applyVoucher(param,(isSuccess,responseData)=>{
            if(responseData.code===0){
                console.log(JSON.stringify(responseData.data))
                this.props.callback(responseData.data,quantityVoucher,voucher)
                Actions.pop()
            }
        })
    }
    renderItemStore = ({item, index}) => (<VoucherStoreItem item={item} onChange={(item)=>{
        let data=this.state.listVoucherProduct
        let newArray = data.map(e => {
            if (item.id === e.id) {
                return {
                    ...e,
                    checked: !e.checked,
                };
            }
            return {
                ...e,
                checked: false,
            };
        });
        this.setState({listVoucherProduct:newArray})
    }}/>)
    renderItemSearch = ({item, index}) => (
        <VoucherStoreItem item={item}
                          onChange={(item)=>{
                              let data=this.state.listSearch
                              let newArray = data.map(e => {
                                  if (item.id === e.id) {
                                      return {
                                          ...e,
                                          checked: !e.checked,
                                      };
                                  }
                                  return {
                                      ...e,
                                      checked: false,
                                  };
                              });

                              this.setState({listSearch:newArray})
                          }}/>)
    renderItem = ({item, index}) => (
        <VoucherStoreItem item={item}
                          onChange={(item)=>{
                            let data=this.state.listVoucherTransport
                            let newArray = data.map(e => {
                                if (item.id === e.id) {
                                    return {
                                        ...e,
                                        checked: !e.checked,
                                    };
                                }
                                return {
                                    ...e,
                                    checked: false,
                                };
                            });

                            this.setState({listVoucherTransport:newArray})
    }}/>)
}
