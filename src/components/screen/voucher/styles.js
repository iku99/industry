import Styles from "../../../resource/Styles";
import ColorStyle from "../../../resource/ColorStyle";

const X=Styles.constants.X
export default {
    body:{
        width: '100%',
        backgroundColor:ColorStyle.tabWhite,
        padding:X/5,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
    marginTop:X/5,
        elevation: 3,
    },
    viewSearch:{
        width: '70%',
        backgroundColor:ColorStyle.tabWhite,
        padding:X/5,
        borderColor:ColorStyle.borderButton,
        borderWidth:1
    },
    btnSearch:{
        width: '25%',
        marginLeft:X/5,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:ColorStyle.tabActive,
        borderRadius:X/9
    },
    textSearch:{
        ...Styles.text.text15,
        color:ColorStyle.tabWhite
    },
    textTitle:{
        ...Styles.text.text16,
        color:ColorStyle.tabActive,
        fontWeight:'600'
    },
    viewImg:{
        width:  X*4,
        height:X*2,
        alignItems:'center',
        justifyContent:'center',
        // borderStyle:'dotted',
        // borderRightColor:ColorStyle.tabBlack,
        // borderRightWidth:2,

    },
    textImg:{
        fontWeight: '700',
        fontSize:X/3,
        color:'#0D8350'
    },
    itemVoucherAdmin:{
        // backgroundColor:'red',
        alignItems: 'center',
        width:  X*4,
        marginRight:X/6,
        borderColor: ColorStyle.blue,
        borderWidth: 1,
        borderRadius: X/10
    },
    itemVoucherAdminSelect:{
        // backgroundColor:'red',
        alignItems: 'center',
        width:  X*4,
        marginRight:X/6,
        borderColor: ColorStyle.tabActive,
        borderWidth: 2,
        borderRadius: X/10
    },
    des:{
        ...Styles.text.text12,
        color:ColorStyle.tabWhite,
        fontWeight:'600'
    },
    viewDes:{
        width:'100%',
        position:'absolute',
        bottom:0,
        alignItems: 'center',
        paddingVertical:X/10,
        backgroundColor:ColorStyle.tabActive,
    },
    bodyText:{
        marginVertical: X/5
    },
    textName:{
        ...Styles.text.text12,
        color:ColorStyle.tabBlack,
        fontWeight:'600'
    },
    textDate:{
        ...Styles.text.text10,
        color:ColorStyle.tabBlack,
        fontWeight:'600',
        marginTop:X/10
    },
    bodyRow:{
        flexDirection:'row',
        alignItems:'center',
    },
    renderBottom: {
        position: 'absolute',
        bottom: X / 2,
        marginTop:X/3,
        width: Styles.constants.widthScreen / 2,
        height: X,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: ColorStyle.tabActive,
        borderRadius: X / 7
    },
    textAD:{
        ...Styles.text.text14,
        color:ColorStyle.tabBlack,
        fontWeight:'600'

    },
    errorCode:{
        ...Styles.text.text11,
        color:ColorStyle.red,
        fontWeight:'400',
        marginLeft:X/10,
        marginTop:X/5
    }
}
