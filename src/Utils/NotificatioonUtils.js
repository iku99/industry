export default {
  checkRead(data) {
    let index = 0;
    data.forEach(i => {
      if (i.seen === false) {
        index = index + 1;
      }
    });
    if (index === 0) {
      return (index = 0);
    }
    return index;
  },
};
