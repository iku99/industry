let data={
    list_product:[],
    discount_detail:{
        shipping_fee:0,
        product_price:0,
        reduce_shipping_fee:0,
        reduce_product_price:0,
        total:0
    }
}
export default {
    formatData(values){
        data.list_product=values;
        let totalSum=0
        data.list_product= data.list_product.map((elm,i)=>{
            let totalProduct=0
            elm.products.map(item=>{
                totalProduct=totalProduct+item.price*item.quantity
                totalSum=totalSum+item.price*item.quantity
            })
            return {
                ...elm,
                ship_fee:{},
                voucher_store:undefined,
                voucher_ecm:undefined,
                discount_detail:{
                    shipping_fee:0,
                    product_price:totalProduct,
                    reduce_shipping_fee:0,
                    reduce_product_price:0,
                    total:totalProduct
                }
            }
        })
       return {
            ...data,
            discount_detail: {
                ...data.discount_detail,
                product_price:totalSum,
                total:totalSum
            }
        }
    },
    addShipping(list,shipping,index){
        list.list_product[index].ship_fee=shipping
        let mData= this.updateData(list)
        return mData
    },
    updateData(state){
        let totalSum=0
        let totalVoucherProduct=0;
        let totalVoucherShip=0;
        let totalShipping=0;
        state.list_product= state?.list_product.map((elm,i)=>{
            let voucherStore =0;
            let voucherEcmShip =0;
            let voucherEcmProduct=0;
            if(elm.voucher_store !== undefined){
                voucherStore=voucherStore+elm.voucher_store.reduce_value
            }
            if(elm.voucher_ecm !==undefined){
                voucherEcmShip=voucherEcmShip+elm.voucher_ecm.reduce_shipping_fee
                voucherEcmProduct=voucherEcmProduct+elm.voucher_ecm.reduce_product_price
            }
            totalShipping=totalShipping+elm.ship_fee.total
            totalSum=totalSum+elm.total
            totalVoucherShip=totalVoucherShip+voucherEcmShip;
            totalVoucherProduct=totalVoucherProduct+voucherStore+voucherEcmProduct
            return {
                ...elm,
                discount_detail:{
                    ...elm.discount_detail,
                    reduce_shipping_fee:voucherEcmShip,
                    reduce_product_price:voucherStore+voucherEcmProduct,
                    shipping_fee:totalShipping,
                    product_price:elm.total,
                    total:elm.total+totalShipping-(voucherStore+voucherEcmProduct+voucherEcmShip)
                }
            }
        })
        return {
            ...state,
            discount_detail: {
                reduce_shipping_fee:totalVoucherShip,
                reduce_product_price:totalVoucherProduct,
                shipping_fee:totalShipping,
                product_price:totalSum,
                total:((totalShipping+totalSum)-(totalVoucherProduct+totalVoucherShip))
            }
        }
    },
    getVoucherEco(items){
        let list=[], sum=0;
        items.map((item,index) => {
            let mData ={
                store_id: item.store.id,
                ship_fee:item.ship_fee.total
            };
            item.products=item.products.map((ele)=>{
                sum=sum+ele.price*ele.quantity
                return {
                    ...ele,
                    id_order_detail: ele.id_order_detail,
                    product_id:ele.id,
                    quantity:ele.quantity,
                }
            });
            mData= {
                ...mData,
                sum:item.total,
                products:item.products,

            }
            list.push(mData)
        });
        return list;
    },
    convertVoucher(state,stateUpdate,voucher){
        let m1=state.list_product
        let m2=stateUpdate
        m1.map((item,index)=>{
         return item.voucher_ecm=m2[index].discount_detail
        })
        state={
            ...state,
            list_product:m1,
            voucher:voucher
        }
        console.log('state:',JSON.stringify(state))
       let list=this.updateData(state)
        console.log('list:',JSON.stringify(list))
        return list
    },
    convertVoucherStore(state,voucherStore){
        let idStore=voucherStore.store_id
        let list_product=state.list_product;
        list_product.map(elm=>{
            if(elm.store.id ==idStore){
              return   elm.voucher_store=voucherStore
            }
        })
        state={
            ...state,
            list_product:list_product,
        }
        let list=this.updateData(state)
        return list
    },
    convertProduct(product){
        let data=[];
        let body={
            voucher_transport:undefined,
            voucher_product:undefined
        };
        product.list_product.map(item=>{
            let mData ={ store_id: item.store.id};
            item.products=item.products.map((ele)=>{
                    return {
                        id_order_detail: ele.id_order_detail,
                        product_id:ele.product_id===undefined?ele.id:ele.product_id,
                        quantity:ele.quantity,
                    }
                });
            mData={
                ...mData,
                products:item.products,
                discount_detail:item.discount_detail,

            }
            data.push(mData)
        })
        body={
            list_product:data,
            total_price: product.discount_detail.total,
        }
        if(product.voucher?.transport !==undefined){
            body={
                ...body,
                voucher_transport:product.voucher.transport.id,
            }
        }
        if(product.voucher?.product !==undefined){
            body={
                ...body,
                voucher_product:product.voucher.product.id            }
        }
        // console.log(J)
    return body
    }
}
