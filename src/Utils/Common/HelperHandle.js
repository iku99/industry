import {Actions} from 'react-native-router-flux';
import GlobalInfo from './GlobalInfo';
import TokenGuad from '../Auth/TokenGuad';
import GlobalUtil from './GlobalUtil';
import ResultCode from '../../Api/https/ResultCode';
import {strings} from '../../resource/languages/i18n';
import ExecuteHttp from '../../Api/https/ExecuteHttp';

export default class HelperHandle {
  static helperHandle = null;

  static getInstance() {
    if (HelperHandle.helperHandle == null) {
      HelperHandle.helperHandle = new HelperHandle();
    }
    return this.helperHandle;
  }

  async tokenExpire() {
    GlobalInfo.clearData();
    await TokenGuad.getInstance().logout();

    Actions.popTo('login');
  }

  getResultCodeValue(responseData) {
    if (responseData !== undefined) {
      return responseData.errorCode;
    }
    return undefined;
  }

  setResultResponse(outputObj, resultCode, message) {
    if (!GlobalUtil.checkIsNull(outputObj)) {
      outputObj.errorCode = resultCode;
      outputObj.errorMsg = message;
      return outputObj;
    } else {
      return {
        errorCode: resultCode,
        errorMsg: message,
      };
    }
  }
  async executeAPI(method, urlEndpoint, requestData, callbackFunction) {
    let responseData = {};
    switch (method) {
      case 'GET_WITHOUT_HANDLE':
        responseData = await this.executeGetOriginalResponse(urlEndpoint);
        break;
      case 'GET':
        responseData = await this.executeGet(urlEndpoint);
        break;
      case 'POST':
        responseData = await this.executePost(urlEndpoint, requestData);
        break;
    }

    if (!GlobalUtil.checkIsNull(responseData)) {
      switch (this.getResultCodeValue(responseData)) {
        case ResultCode.SUCCESS:
          return callbackFunction(true, responseData, '');
        default:
          return callbackFunction(false, responseData, responseData.errorMsg);
      }
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, {}, strings('notification_get_data_error'));
    }
  }

  async executePostAuth(urlEndpoint, requestData) {
    return await ExecuteHttp.getInstance()
      .authService(GlobalInfo.initApiEndpoint(urlEndpoint), requestData)
      .then(responseJson => {
        if (!GlobalUtil.checkIsNull(responseJson)) {
          let cookies = responseJson.cookies;
          TokenGuad.getInstance().setAccessToken(cookies);
          return responseJson;
        } else {
          return this.setResultResponse(
            responseJson,
            ResultCode.INTERNAL_SERVER_ERROR,
            ResultCode.getMessage(ResultCode.INTERNAL_SERVER_ERROR),
          );
        }
      });
  }

  async executePost(urlEndpoint, requestData) {

    return await ExecuteHttp.getInstance()
      .post(GlobalInfo.initApiEndpoint(urlEndpoint), requestData)
      .then(responseJson => {
        if (!GlobalUtil.checkIsNull(responseJson)) {
          switch (this.getResultCodeValue(responseJson)) {
            case ResultCode.BAD_REQUEST:
              return this.tokenExpire();
            default:
              return responseJson;
          }
        } else {
          return this.setResultResponse(
            responseJson,
            ResultCode.INTERNAL_SERVER_ERROR,
            ResultCode.getMessage(ResultCode.INTERNAL_SERVER_ERROR),
          );
        }
      });
  }
  async executePostList(urlEndpoint, requestData) {
    return await ExecuteHttp.getInstance()
      .postList(GlobalInfo.initApiEndpoint(urlEndpoint), requestData)
      .then(responseJson => {
        if (!GlobalUtil.checkIsNull(responseJson)) {
          switch (this.getResultCodeValue(responseJson)) {
            case ResultCode.BAD_REQUEST:
              return this.tokenExpire();
            default:
              return responseJson;
          }
        } else {
          return this.setResultResponse(
            responseJson,
            ResultCode.INTERNAL_SERVER_ERROR,
            ResultCode.getMessage(ResultCode.INTERNAL_SERVER_ERROR),
          );
        }
      });
  }
  async executePostWithoutToken(urlEndpoint, requestData) {
    return await ExecuteHttp.getInstance()
      .postWithoutToken(
        GlobalInfo.initApiEndpoint(urlEndpoint),
        requestData,
      )
      .then(responseJson => {
        if (!GlobalUtil.checkIsNull(responseJson)) {
          switch (this.getResultCodeValue(responseJson)) {
            case ResultCode.BAD_REQUEST:
              return this.tokenExpire();
            default:
              return responseJson;
          }
        } else {
          return this.setResultResponse(
            responseJson,
            ResultCode.INTERNAL_SERVER_ERROR,
            ResultCode.getMessage(ResultCode.INTERNAL_SERVER_ERROR),
          );
        }
      });
  }
  async executePut(urlEndpoint, requestData) {
    return await ExecuteHttp.getInstance()
      .put(GlobalInfo.initApiEndpoint(urlEndpoint), requestData)
      .then(responseJson => {
        if (!GlobalUtil.checkIsNull(responseJson)) {
          switch (this.getResultCodeValue(responseJson)) {
            case ResultCode.BAD_REQUEST:
              return this.tokenExpire();
            default:
              return responseJson;
          }
        } else {
          return this.setResultResponse(
            responseJson,
            ResultCode.INTERNAL_SERVER_ERROR,
            ResultCode.getMessage(ResultCode.INTERNAL_SERVER_ERROR),
          );
        }
      });
  }
  async executeDelete(urlEndpoint, params) {
    return await ExecuteHttp.getInstance()
      .delete(GlobalInfo.initApiEndpoint(urlEndpoint), params)
      .then(result => {
        if (result === 200) {
          return true;
        } else {
          return false;
        }
      });
  }
  async executeGet(urlEndpoint) {
    return await ExecuteHttp.getInstance()
      .get(GlobalInfo.initApiEndpoint(urlEndpoint))
      .then(responseJson => {
        if (!GlobalUtil.checkIsNull(responseJson)) {
          switch (this.getResultCodeValue(responseJson)) {
            case ResultCode.BAD_REQUEST:
              return this.tokenExpire();
            default:
              return responseJson;
          }
        } else {
          return this.setResultResponse(
            responseJson,
            ResultCode.INTERNAL_SERVER_ERROR,
            ResultCode.getMessage(ResultCode.INTERNAL_SERVER_ERROR),
          );
        }
      });
  }
  async executeGetWithoutToken(urlEndpoint) {
    return await ExecuteHttp.getInstance()
      .getWithoutToken(GlobalInfo.initApiEndpoint(urlEndpoint))
      .then(responseJson => {
        if (!GlobalUtil.checkIsNull(responseJson)) {
          switch (this.getResultCodeValue(responseJson)) {
            case ResultCode.BAD_REQUEST:
              return this.tokenExpire();
            default:
              return responseJson;
          }
        } else {
          return this.setResultResponse(
            responseJson,
            ResultCode.INTERNAL_SERVER_ERROR,
            ResultCode.getMessage(ResultCode.INTERNAL_SERVER_ERROR),
          );
        }
      });
  }

  async executeGetOriginalResponse(urlEndpoint) {
    return await ExecuteHttp.getInstance()
      .getOriginalResponse(GlobalInfo.initApiEndpoint(urlEndpoint))
      .then(responseJson => {
        if (!GlobalUtil.checkIsNull(responseJson)) {
          responseJson = this.setResultResponse(
            responseJson,
            ResultCode.SUCCESS,
            ResultCode.getMessage(ResultCode.SUCCESS),
          );
          return responseJson;
        } else {
          return this.setResultResponse(
            responseJson,
            ResultCode.INTERNAL_SERVER_ERROR,
            ResultCode.getMessage(ResultCode.INTERNAL_SERVER_ERROR),
          );
        }
      });
  }
  async executePostUpload(urlEndpoint, requestData, onProgress, onFinish) {
    ExecuteHttp.getInstance().postUpload(
      GlobalInfo.initApiEndpoint(urlEndpoint),
      requestData,
      progress => {
        onProgress(progress);
      },
      (isSuccess, responseData) => {
        onFinish(isSuccess, responseData);
      },
    );
  }
  executePutUpload(urlEndpoint, requestData, onProgress, onFinish) {
    ExecuteHttp.getInstance().putUpload(
      GlobalInfo.initApiEndpoint(urlEndpoint),
      requestData,
      progress => {
        onProgress(progress);
      },
      (isSuccess, responseData) => {
        onFinish(isSuccess, responseData);
      },
    );
  }

  async executeExternalGet(urlEndpoint) {
    return await ExecuteHttp.getInstance()
      .getWithoutToken(urlEndpoint)
      .then(responseJson => {
        if (!GlobalUtil.checkIsNull(responseJson)) {
          switch (this.getResultCodeValue(responseJson)) {
            case ResultCode.BAD_REQUEST:
              return this.tokenExpire();
            default:
              return responseJson;
          }
        } else {
          return this.setResultResponse(
            responseJson,
            ResultCode.INTERNAL_SERVER_ERROR,
            ResultCode.getMessage(ResultCode.INTERNAL_SERVER_ERROR),
          );
        }
      });
  }
}
