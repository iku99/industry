import SharePreferenceHelper from './SharePreferenceHelper';
import React from 'react';
import validator from 'validator';
import DeviceInfo from 'react-native-device-info';
import AppConstants from '../../resource/AppConstants';

export default {
  getViewHeight(event) {
    return event.nativeEvent.layout.height;
  },
  getRandomColor() {
    return (
      'rgb(' +
      Math.floor(Math.random() * 256) +
      ',' +
      Math.floor(Math.random() * 256) +
      ',' +
      Math.floor(Math.random() * 256) +
      ')'
    );
  },
  convertBase64ToImg(base64Value) {
    return 'data:image/png;base64,' + base64Value;
  },

  checkIsNull(inputData) {
    if (inputData !== undefined && inputData !== null) {
      return false;
    }
    return true;
  },

  checkStringIsNull(inputData) {
    if (inputData === undefined || inputData == null || inputData === '') {
      return true;
    }
    return false;
  },

  subString(input, length) {
    if (this.checkIsNull(length)) {
      length = 50;
    }
    input = input.substr(0, length) + '...';
    return input;
  },
  // dismissKeyboard(){
  //     Keyboard.dismiss()
  // },
  formatMoney(amount, decimalCount = 2, decimal = '.', thousands = ',') {
    try {
      decimalCount = Math.abs(decimalCount);
      decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

      const negativeSign = amount < 0 ? '-' : '';

      let i = parseInt(
        (amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)),
      ).toString();
      let j = i.length > 3 ? i.length % 3 : 0;

      return (
        negativeSign +
        (j ? i.substr(0, j) + thousands : '') +
        i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands) +
        (decimalCount
          ? decimal +
            Math.abs(amount - i)
              .toFixed(decimalCount)
              .slice(2)
          : '')
      );
    } catch (e) {}
  },

  formatInt(amount, thousands = ',') {
    try {
      const negativeSign = amount < 0 ? '-' : '';

      let i = parseInt(
        (amount = Math.abs(Number(amount) || 0).toFixed(0)),
      ).toString();
      let j = i.length > 3 ? i.length % 3 : 0;

      return (
        negativeSign +
        (j ? i.substr(0, j) + thousands : '') +
        i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousands)
      );
    } catch (e) {}
  },
  getAvartarKey(callback) {
    SharePreferenceHelper.getInstance().retrieveDataWithCallback(
      AppConstants.USER_ENCRYPTION,
      '',
      value => {
        let key = `${AppConstants.avataxrKey}-${value}`;
        callback(key);
      },
    );
  },

  getAvatar(key, callback) {
    SharePreferenceHelper.getInstance().retrieveDataWithCallback(
      key,
      null,
      avatar => {
        callback(avatar);
      },
    );
  },

  getPreviousMonth() {
    let date = new Date();
    date.setMonth(0, 1);
    return date;
  },

  shallowEqual(objA, objB) {
    if (objA === objB) {
      return true;
    }

    if (
      typeof objA !== 'object' ||
      objA === null ||
      typeof objB !== 'object' ||
      objB === null
    ) {
      return false;
    }

    let keysA = Object.keys(objA);
    let keysB = Object.keys(objB);

    if (keysA.length !== keysB.length) {
      return false;
    }

    // Test for A's keys different from B.
    let bHasOwnProperty = hasOwnProperty.bind(objB);
    for (let i = 0; i < keysA.length; i++) {
      if (
        !bHasOwnProperty(keysA[i]) ||
        JSON.stringify(objA[keysA[i]]) !== JSON.stringify(objB[keysA[i]])
      ) {
        return false;
      }
    }
    return true;
  },

  shallowCompareState(instance, nextState) {
    return !this.shallowEqual(instance.state, nextState);
  },

  shallowCompareProps(instance, nextProps) {
    return !this.shallowEqual(instance.props, nextProps);
  },

  shallowCompare(instance, nextState, nextProps) {
    return (
      !this.shallowEqual(instance.state, nextState) ||
      !this.shallowEqual(instance.props, nextProps)
    );
  },
  objToQueryString(obj) {
    const keyValuePairs = [];
    for (let i = 0; i < Object.keys(obj).length; i += 1) {
      keyValuePairs.push(
        `${encodeURIComponent(Object.keys(obj)[i])}=${encodeURIComponent(
          Object.values(obj)[i],
        )}`,
      );
    }
    return keyValuePairs.join('&');
  },
  isRequestSuccess(response) {
    if (
      response.code !== 0 ||
      response == null ||
      response.success === false ||
      response.error !== undefined
    ) {
      return false;
    } else {
      return true;
    }
  },
  isTablet() {
    return DeviceInfo.isTablet();
  },
  validatePhoneNumber(number) {
    const isValidPhoneNumber = validator.isMobilePhone(number);
    return isValidPhoneNumber && number.startsWith('0');
  },
  getParamFromUrl(url) {
    let regex = /[?&]([^=#]+)=([^&#]*)/g,
      params = {},
      match;
    while ((match = regex.exec(url))) {
      params[match[1]] = match[2];
    }
    return params;
  },
};
