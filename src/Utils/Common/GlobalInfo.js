import TokenGuad from '../Auth/TokenGuad';
import {AsyncStorage, Platform} from 'react-native';
import React from 'react';
import AppConstants from '../../resource/AppConstants';
import constants from '../../Api/constants';
import DataUtils from "../DataUtils";

export default class GlobalInfo {
  static globalInfo = null;
  static userInfo = {};
  static employeeInfo = {};
  static sessionId = '';
  static sessionLogId = '';
  static language = 'vi';
  static notificationCount = 0;
  static token_type = '';
  static access_token = '';
  static refresh_token = '';
  static expires_in = 0;
  static scope = '';
  static isParseUserInfo = false;

  static getInstance() {
    if (GlobalInfo.globalInfo == null) {
      GlobalInfo.globalInfo = new GlobalInfo();
    }
    return this.globalInfo;
  }

  async getUserLogin() {
    if (GlobalInfo.userInfo == null) {
      GlobalInfo.userInfo = await TokenGuad.getInstance().getCurrentUser();
    }
    return GlobalInfo.userInfo;
  }
  static async getUserInfo() {
    if (!this.isParseUserInfo) {
      let r = await AsyncStorage.getItem(
        AppConstants.SharedPreferencesKey.UserData,
        null,
      );
      this.userInfo = JSON.parse(r);
      this.isParseUserInfo = true;
      return this.userInfo;
    }

    return this.userInfo;
  }
  static async getAvatar() {
    await this.getUserInfo();
    let avatar = '';
    if (this.userInfo !== undefined && this.userInfo.avatar !== undefined) {
      avatar = this.userInfo.avatar;
      if (avatar.startsWith(AppConstants.LOCAL_SIGNAL)) {
        avatar = avatar.replace(AppConstants.LOCAL_SIGNAL, '');
      } else if (!(avatar.startsWith('http') || avatar.startsWith('https'))) {
        avatar = `${constants.host}${avatar}`;
      }
    }
    return (await DataUtils.checkImageURL(avatar)) ? avatar : '';
  }
  async getEmployeeInfo() {
    if (GlobalInfo.employeeInfo == null) {
      GlobalInfo.employeeInfo = await TokenGuad.getInstance().getEmployeeInfo();
    }
    return GlobalInfo.employeeInfo;
  }

  async logout() {
    GlobalInfo.userInfo = {};
    GlobalInfo.sessionId = '';
    return await TokenGuad.getInstance().logout();
  }

  static initApiEndpoint(urlEndpoint) {
    return constants.host + urlEndpoint;
  }

  static clearData() {
    GlobalInfo.userInfo = {};
    GlobalInfo.employeeInfo = {};
  }

  static getDevicePlatform() {
    return Platform.OS;
  }
}
