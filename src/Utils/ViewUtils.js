import {
  Alert,
  FlatList,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Image,
} from 'react-native';
import {Icon} from 'react-native-elements';
import React, {useEffect, useState} from 'react';
import FloatLabelTextField from '../components/elements/FloatLabelTextInput';
import {strings} from '../resource/languages/i18n';
import {DotIndicator, PacmanIndicator} from 'react-native-indicators';
import AppConstants from '../resource/AppConstants';
import FastImage from 'react-native-fast-image';
import Styles from '../resource/Styles';
import ColorStyle from '../resource/ColorStyle';
import GroupStyle from '../components/screen/group/GroupStyle';
export default {
  renderInput(
    stateName,
    placeholder,
    secure,
    validState,
    callback,
    onChangeTextValue,
    returnKeyType,
    onSubmitEditing,
    ref,
    keyboardTypeInput,
  ) {
    return (
      <View
        animation="fadeInLeftBig"
        style={[
          {
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between',
          },
        ]}>
        {/*<StatusBar barStyle="dark-content" backgroundColor="transparent" translucent={true}/>*/}
        <FloatLabelTextField
          style={Styles.input.inputLogin}
          valueText={stateName}
          onChangeTextValue={value => {
            onChangeTextValue(value);
            if (callback != null) {
              callback();
            }
          }}
          autoCapitalize="none"
          placeholder={placeholder}
          secureTextEntry={secure}
          returnKeyType={returnKeyType}
          onSubmitEditing={onSubmitEditing}
          refFunc={ref}
          keyboardTypeInput={keyboardTypeInput}
        />
        {validState != null && (
          <Icon
            ame={validState ? 'check' : 'alert-circle'}
            type="feather"
            size={18}
            color={validState ? ColorStyle.tabActive : 'red'}
            containerStyle={{
              position: 'absolute',
              right: 30,
              alignItems: 'flex-end',
              bottom: '30%',
            }}
          />
        )}
      </View>
    );
  },
  renderTab(props, backgroundColor, index, onPress) {
    return (
      <View
        style={{
          flexDirection: 'row',
          height: 50,
          borderBottomWidth: 0.5,
          backgroundColor: backgroundColor === undefined ? 'transparent' : backgroundColor,
            borderBottomColor: 'rgba(0, 0, 0, 0.05)',
            shadowColor: 'black',
            shadowOffset: {width: 2, height: 1},
            shadowOpacity: 0.26, elevation: 2,
        }}>
        {props.navigationState.routes.map((route, i) => {
          return (
            <TouchableOpacity
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                borderBottomColor:
                  i === index ? ColorStyle.tabActive : ColorStyle.tabBlack,
                borderBottomWidth: i === index ? 2 : 0,
              }}
              onPress={() => onPress(i)}>
              <Text
                style={{
                  ...Styles.text.text14,
                  color:
                    i === index ? ColorStyle.tabActive : ColorStyle.tabBlack,
                  fontWeight: i === index ? '700' : '400',
                }}>
                {route.title}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  },
  renderTabGroupManage(props, index, onPress) {
    return (
      <View
        style={{
          flexDirection: 'row',
          height: 70,
          justifyContent: 'space-evenly',
          borderBottomColor: ColorStyle.borderItemHome,
          elevation: 1,
          marginBottom: 12,
        }}>
        {props.navigationState.routes.map((route, i) => {
          return (
            <TouchableOpacity
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => onPress(i)}>
              <Icon
                name={route.icon}
                type={route.type}
                size={20}
                color={i === index ? ColorStyle.tabActive : ColorStyle.tabBlack}
              />
              <Text
                style={{
                  ...Styles.text.text14,
                  color:
                    i === index ? ColorStyle.tabActive : ColorStyle.tabBlack,
                  fontWeight: i === index ? '700' : '400',
                }}>
                {route.title}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  },

  renderTabGroups(props, index, onPress) {
    return (
      <View
        style={{
          flexDirection: 'row',
          height: 50,
          marginTop: 6,

          backgroundColor: ColorStyle.tabWhite,
          // borderRadius: 20,
        }}>
        {props.navigationState.routes.map((route, i) => {
          return (
            <TouchableOpacity
              style={[
                {
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor:
                    i === index ? ColorStyle.tabActive : ColorStyle.tabWhite,
                },
              ]}
              onPress={() => onPress(i)}>
              <Text
                style={[
                  Styles.text.text14,
                  {
                    color:
                      i === index ? ColorStyle.tabWhite : ColorStyle.tabBlack,
                    fontWeight: i === index ? '700' : '400',
                  },
                ]}>
                {route.title}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  },
  renderTabPostApproval(props, index, onPress) {
    return (
      <View
        style={{
          flexDirection: 'row',
          height: 50,
          marginTop: 6,
          marginHorizontal: Styles.constants.marginHorizontalAll,
          backgroundColor: ColorStyle.tabWhite,
            borderRadius:20,
            overflow:'hidden',

        }}>
        {props.navigationState.routes.map((route, i) => {
          return (
            <TouchableOpacity
              style={[
                {
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor:
                    i === index ? ColorStyle.tabActive : ColorStyle.tabWhite,
                },
              ]}
              onPress={() => onPress(i)}>
              <Text
                style={[
                  Styles.text.text18,
                  {
                    color:
                      i === index ? ColorStyle.tabWhite : ColorStyle.tabBlack,
                    fontWeight: i === index ? '700' : '400',
                  },
                ]}>
                {route.title}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  },

  renderTabShop(props, index, onPress) {
    return (
      <View
        style={{
          flexDirection: 'row',
          marginTop: 6,
          borderColor: 'gray',
          paddingHorizontal: 10,
          paddingVertical: 5,
          borderBottomWidth: 0.5,
          borderTopWidth: 0.5,
        }}>
        {props.navigationState.routes.map((route, i) => {
          return (
            <TouchableOpacity
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                paddingVertical: 6,
                borderRadius: 20,
                marginLeft: 10,
                paddingHorizontal: 3,
                backgroundColor:
                  i === index ? ColorStyle.tabActive : ColorStyle.tabWhite,
              }}
              onPress={() => onPress(i)}>
              <Text
                style={{
                  ...Styles.text.text14,
                  color:
                    i === index ? ColorStyle.tabWhite : ColorStyle.tabBlack,
                  fontWeight: i === index ? '700' : '400',
                }}>
                {route.title}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  },
  renderTabScroll(props, index, onPress) {
    return (
      <View>
        <ScrollView
          style={{flexDirection: 'row', height: 50}}
          horizontal={true}
          showsHorizontalScrollIndicator={false}>
          {props.navigationState.routes.map((route, i) => {
            return (
              <TouchableOpacity
                // style={
                //   i === index
                //     ? MessageStyle.tabSelected
                //     : MessageStyle.tabNormal
                // }
                onPress={() => onPress(i)}>
                <Text
                // style={
                //   i === index
                //     ? MessageStyle.itemSelectedText
                //     : MessageStyle.itemNormalText
                // }
                >
                  {route.title}
                </Text>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>
    );
  },
  renderTabScroll2(props, index, onPress) {
    return (
      <View>
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={props.navigationState.routes}
          keyExtractor={(item, index) => `viewutils_${index.toString()}`}
          scroll
          renderItem={item => {
            return (
              <TouchableOpacity
                // style={
                //   item.index === index
                //     ? MessageStyle.tabSelected
                //     : MessageStyle.tabNormal
                // }
                onPress={() => {
                  onPress(item.index);
                }}>
                <Text
                // style={
                //   item.index === index
                //     ? MessageStyle.itemSelectedText
                //     : MessageStyle.itemNormalText
                // }
                >
                  {item.item.title}
                </Text>
              </TouchableOpacity>
            );
          }}
          style={{width: '100%', backgroundColor: 'transparent'}}
        />
      </View>
    );
  },
  showAlertDialog(msg, onAccept) {
    Alert.alert(
      strings('notification'),
      msg,
      [
        {
          text: strings('accept'),
          onPress: () => {
            if (onAccept != null) {
              onAccept();
            }
          },
        },
      ],
      {cancelable: false},
    );
  },
  showAskAlertDialog(msg, onAccept, onCancel) {
    Alert.alert(
      strings('notification'),
      msg,
      [
        {
          text: strings('accept'),
          onPress: () => {
            if (onAccept != null) {
              onAccept();
            }
          },
        },
        {
          text: strings('cancel'),
          onPress: () => {
            if (onCancel != null) {
              onCancel();
            }
          },
        },
      ],
      {cancelable: true},
    );
  },

  renderLoading(loading, preventClick) {
    if (loading) {
      if (preventClick) {
        return (
          <View>
            <View>
              <DotIndicator color={ColorStyle.tabActive} size={10} />
            </View>
          </View>
        );
      } else {
        return (
          <View>
            <PacmanIndicator color="orange" size={40} />
          </View>
        );
      }
    } else {
      return null;
    }
  },

  renderNode: node => {
    let {parent, data} = node;
    if (parent && parent.name === 'img') {
      return (
        <FastImage
          source={{uri: node.attribs.src}}
          resizeMode={AppConstants.IMAGE_SCALE_TYPE.CONTAIN}
        />
      );
    }
  },
  RenderHTMLImage({uri, onPress, imagesMaxWidth}) {
    const [imageSize, setImageSize] = useState({});

    useEffect(() => {
      Image.getSize(uri, (width, height) => {
        setImageSize({
          width,
          height,
          aspectRatio: Number((width / height).toFixed(2)),
        });
      });
    }, []);
    return (
      <View
        style={{
          width:
            imageSize.width > imagesMaxWidth ? imagesMaxWidth : imageSize.width,
        }}>
        <TouchableOpacity onPress={() => onPress(uri)}>
          <Image
            source={{uri}}
            style={{
              width:
                imageSize.width > imagesMaxWidth
                  ? imagesMaxWidth
                  : imageSize.width,
              aspectRatio: imageSize.aspectRatio,
              resizeMode: 'cover',
            }}
          />
        </TouchableOpacity>
      </View>
    );
  },
};
