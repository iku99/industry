import AppConstants from "../resource/AppConstants";

export default {
    showLog(msg){
        console.log(`${AppConstants.LOG_TAG}${msg}`)
    }
}
