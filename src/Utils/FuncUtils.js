import {AsyncStorage} from 'react-native';
import {EventRegister} from 'react-native-event-listeners';
import AppConstants from '../resource/AppConstants';
import NavigationUtils from "./NavigationUtils";

export default class FuncHandle {
  static mFuncHandle = null;
  isLogged = undefined;

  static getInstance() {
    if (FuncHandle.mFuncHandle == null) {
      FuncHandle.mFuncHandle = new FuncHandle();
      EventRegister.addEventListener(AppConstants.EventName.LOGIN, isLogin => {
        this.mFuncHandle.isLogged = isLogin;
      });
    }
    return this.mFuncHandle;
  }
  async isLogged() {
    if (this.isLogged === undefined) {
      await AsyncStorage.getItem(
        AppConstants.SharedPreferencesKey.IsLogin,
        (error, isLogin) => {
          this.isLogged = isLogin === '1';
        },
      );
      return this.isLogged;
    }
  }
  async callRequireLogin(callback) {
    if (this.isLogged === undefined) {
      await AsyncStorage.getItem(
        AppConstants.SharedPreferencesKey.IsLogin,
        (error, isLogin) => {
          this.isLogged = isLogin === '1';
          this.onCall(true, callback);
        },
      );
    } else {
      this.onCall(true, callback);
    }
  }
  async callOptionLogin(callback) {
    if (this.isLogged === undefined) {
      await AsyncStorage.getItem(
        AppConstants.SharedPreferencesKey.IsLogin,
        (error, isLogin) => {
          this.isLogged = isLogin === '1';
          this.onCall(false, callback);
        },
      );
    } else {
      this.onCall(false, callback);
    }
  }
  async onCall(showLoginView, callback) {
    if (!this.isLogged) {
      if (showLoginView) {
        NavigationUtils.goToLoginView();
      }
    } else {
      callback();
    }
  }
}
