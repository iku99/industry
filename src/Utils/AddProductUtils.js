export default {
     filterPropertyProduct(list){
         if(list===undefined ||list.length ===0 ){
             return []
         }
        let data= list.filter(elm=>elm.value !==undefined)
       return data
     },
    convertProperty(state,stateUpdate){
        let data=[]
       state.map((item,index)=>{
            let checked=stateUpdate.filter(elm=>item.id===elm.id)
            if(checked.length>0){
              let  mItem={
                  ...item,
                  value:checked[0].value
              }
                data.push(mItem)
                return
            }
            data.push(item)
        })
        return data
    }
}
