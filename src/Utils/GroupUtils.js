import AppConstants from '../resource/AppConstants';
import { strings } from "../resource/languages/i18n";

export default {
  getButtonText(state, callback) {
    let canSend = true;
    let text = ' ';
    switch (state) {
      case AppConstants.GROUP_APPROVE_STATUS.PENDING:
        canSend = false;
        text = strings('pending');
        break;
      case AppConstants.GROUP_APPROVE_STATUS.REJECTED:
        canSend = false;
        text = strings('rejected');
        break;
      case AppConstants.GROUP_APPROVE_STATUS.APPROVED:
        canSend = false;
        text = strings('approved');
        break;
      default:
        canSend = true;
        text = strings('join');
        break;
    }
    callback(canSend, text);
  },
};
