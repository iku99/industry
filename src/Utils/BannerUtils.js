import {EventRegister} from 'react-native-event-listeners';
import BannerHandle from "../sagas/BannerHandle";
import AppConstants from "../resource/AppConstants";

let banners = [];
let data = [];
export default {
  getBanner() {
    BannerHandle.getInstance().getBannerList((isSuccess, dataResponse) => {
      if (isSuccess && dataResponse.data.list !== undefined) {
        banners = dataResponse.data.list;
        if (banners === undefined) {
          banners = [];
        }
        let objectId = [];
        banners.forEach(banner => {
          objectId.push(banner.id);
        });
        this.getBannerInfo(objectId);
      }
    });
  },
  getBannerInfo(idList) {
    let params = {
      object_ids: idList,
      object_type: AppConstants.IMAGE_UPLOAD_TYPE.BANNER,
    };
    BannerHandle.getInstance().getBannerInfo(
      params,
      (isSuccess, dataResponse) => {
        if (isSuccess && dataResponse.data.list !== undefined) {
          let finalBanners = [];
          let bannerInfoList = dataResponse.data.list;
          let bannerSize = banners.length;
          let bannerInfoSize = bannerInfoList.length;
          for (let i = 0; i < bannerSize; i++) {
            let banner = banners[i];
            for (let j = 0; j < bannerInfoSize; j++) {
              let bannerInfo = bannerInfoList[j];
              if (banner.id === bannerInfo.object_id) {
                banner = {
                  ...banner,
                  info: bannerInfo,
                };
                finalBanners.push(banner);
              }
            }
          }
          this.updateData(finalBanners);
        }
      },
    );
  },
  getData(type) {
    if (type === undefined) {
      return data;
    }
    let result = data;
    result = result !== undefined ? result : [];
    if (type === AppConstants.BANNER_POSITION.MAIN) {
    }
    return result[type] !== undefined ? result[type] : [];
  },
  updateData(finalBanners) {
    data = {};
    finalBanners.forEach(banner => {
      let key = banner.position;
      let oldData = data[key];
      oldData = oldData !== undefined ? oldData : [];
      oldData.push(banner);
      data[key] = oldData;
    });
    EventRegister.emit(AppConstants.EventName.LOAD_GROUP_PRODUCT, data);
  },
  registerListener(callback) {
    EventRegister.addEventListener(
      AppConstants.EventName.LOAD_GROUP_PRODUCT,
      data => {
        callback(data);
      },
    );
  },
};
