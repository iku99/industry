import AppConstants from '../resource/AppConstants';
import {EventRegister} from 'react-native-event-listeners';
import SuggestRow from "../components/screen/group/tab/row/SuggestRow";
import ListGroupRow from "../components/screen/group/tab/row/ListGroupRow";
import { View } from "react-native";
import React from "react";
let data = [];
const keyCategories = {};
export default {
  stringNullOrEmpty(str) {
    return str === undefined || str == null || str.trim() === '';
  },
  removeItem(data, index) {
    let newData = [...data.slice(0, index), ...data.slice(index + 1)];
    return newData;
  },
  listNullOrEmpty(items) {
    return items === undefined || items.length === 0;
  },
  getData(type) {
    if (type === undefined) {
      return data;
    }
    let result = data;
    result = result !== undefined ? result : [];
    if (type === AppConstants.BANNER_POSITION.MAIN) {
    }
    return result[type] !== undefined ? result[type] : [];
  },

  handleGetCategories(newData) {
    let mCategories = [];
    newData.forEach(category => {
      let catId = category.id;
      keyCategories[catId] = category;
      if (category.parent_id === 0) {
        //root node
        category = {
          ...category,
          children: this.getChildren(category.id, newData),
        };

        mCategories.push(category);
      }
    });
    return mCategories;
  },
  getChildren(parentId, newData) {
    let result = [];
    let childless = newData.filter(category => category.parent_id === parentId);
    childless.forEach(category => {
      result.push({
        ...category,
        children: this.getChildren(category.id, newData),
      });
    });
    return result;
  },
  registerListener(callback) {
    EventRegister.addEventListener(
      AppConstants.EventName.LOAD_GROUP_PRODUCT,
      data => {
        callback(data);
      },
    );
  },
  getValues(obj) {
    let vals = [];
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        vals.push(obj[key]);
      }
    }
    return vals;
  },
  getProperties(obj) {
    let vals = [];
    for (var key in obj) {
      vals.push(key);
    }
    return vals;
  },
  checkUserName(text){
    let resEmail =
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let resPhone= /^[0]?[1234560789]\d{8}$/;
    if (resEmail.test(text)) {
      return 1
    }
    if (resPhone.test(text)) {
      return 2
    }
    return 3
  },
  validMail(email) {
    // don't remember from where i copied this code, but this works.
    let re =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (re.test(email)) {
      // this is a valid email address
      // call setState({email: email}) to update the email
      // or update the data in redux store.
      return true;
    } else {
      // invalid email, maybe show an error to the user.
      return false;
    }
  },
  validPhone(text) {
    if (!text.startsWith('0')) {
      return false;
    }
    const reg = /^[0]?[1234560789]\d{8}$/;
    if (reg.test(text) === false) {
      return false;
    } else {
      return true;
    }
  },
  trimNumber(number) {
    return number.replace(/[^\d]/g, '');
  },
  remove0Number(number) {
    if (number.length > 1 && number.startsWith('0')) {
      number = number.substr(1, number.length);
    }
    if (number.length === 0) {
      number = '0';
    }
    return number;
  },
  formatNumber(num) {
    if (num === undefined) {
      return 0;
    }
    let s = Number(num)
      .toFixed(1)
      .replace(/\d(?=(\d{3})+\.)/g, '$&,');
    return s.substr(0, s.length - 2);
  },
  getMessageType(message) {
    if (!this.stringNullOrEmpty(message.image_link)) {
      return AppConstants.MESSAGE_TYPE.IMAGE;
    } else {
      return AppConstants.MESSAGE_TYPE.TEXT;
    }
  },
  async checkImageURL(url) {
    let res = await fetch(url);
    return res.status === 200;
  },
  formatCategory(item) {
    let data = [];
    for (let i = 0; i < 8; i++) {
      data.push(item[i]);
    }
    return data;
  },
  formatFour(item) {
    let data = [];
    for (let i = 0; i < 4; i++) {
      data.push(item[i]);
    }
    return data;
  },
  formatPriceMax(item) {
    let data = item.map(i => i.price);
    return data[0];
  },
  formatValueMax(item) {
    let data = item.map(i => i.quantity);
    return data[0];
  },
  formatMember(item) {
    let data = [];
    if(item.avatar!==undefined){
      for (let i = 0; i < item.length; i++) {
        data.push(item.avatar[i]);
      }
      return data
    }
  ;
  },
};
