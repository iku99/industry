import constants from '../Api/constants';
import AppConstants from '../resource/AppConstants';
import GlobalInfo from './Common/GlobalInfo';
import ImageHelper from "../resource/images/ImageHelper";
export default {
  getAvatar(item) {
    console.log(item)
    let avatar = '';
    if (
        item !== undefined
        &&
        item.avatar !== null
    ) {
      let url = item.avatar;
      if (url?.startsWith(AppConstants.IMG_ONL)) {
        avatar = {
          uri:url
        };
      } else {
        avatar = {
          uri:GlobalInfo.initApiEndpoint(url)
        };
      }
    } else {
      avatar = ImageHelper.profileDefault;
    }
    return avatar;
  },

  getAvatarUrl(item) {
    let avatar = '';
    if (
        item !== undefined
        &&
        item.customer !== undefined && item.customer.avatar !== null && item.customer.avatar !==undefined
    ) {
      let url = item.customer.avatar;
      if (url.startsWith(AppConstants.IMG_ONL)) {
        avatar = {
          uri:url
        };
      } else {
        avatar = {
          uri:GlobalInfo.initApiEndpoint(url)
        };
      }
    } else {
      avatar = {uri:'https://picsum.photos/50/50?avatar'+ item.customer.id};
    }
    return avatar;
  },
  getGroupAvatar(item, index) {
    let avatar = '';
    if (
      item !== undefined &&
      item.avatar !== undefined &&
      item.avatar.thumbnail_url !== undefined
    ) {
      let thumbnail = item.avatar.thumbnail_url;
      if (thumbnail.startsWith(AppConstants.LOCAL_SIGNAL)) {
        avatar = thumbnail.replace(AppConstants.LOCAL_SIGNAL, '');
      } else {
        avatar = GlobalInfo.initApiEndpoint(thumbnail);
      }
    } else {
      avatar = 'https://picsum.photos/84/84?' + index;
    }
    return avatar;
  },
  getGroupCover(item, index) {
    let cover = '';
    if (
      item !== undefined &&
      item.image_url !== null
    ) {
      let thumbnail =  item.image_url;
      if (thumbnail.startsWith(AppConstants.LOCAL_SIGNAL)) {
        cover = thumbnail.replace(AppConstants.LOCAL_SIGNAL, '');
      } else {
        cover = GlobalInfo.initApiEndpoint(thumbnail);
      }
    } else {
      cover = 'https://picsum.photos/300/300?productId=' + index;
    }
    return cover;
  },
  getProductImage(item, index) {
    let images = item.image_url;
    if (images != null && images.length > 0) {
      return constants.host + images[0].url;
    } else {
      return 'https://picsum.photos/130/214?' + index;
    }
  },
  getStoreCover(image,index){
    if(image===null || image=== undefined || image.length===0){
      return {uri: 'https://picsum.photos/300/300?avatar='+index}
    }else
      return {uri:constants.host+image[0].url}
  },
  getStoreAvatar(image,index){
    if(image===null || image=== undefined){
      return {uri: 'https://picsum.photos/300/300?avatar='+index}
    }else
      return {uri:constants.host+image}
  },
};
