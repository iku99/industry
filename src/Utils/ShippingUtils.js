import AppConstants from "../resource/AppConstants";
import {strings} from "../resource/languages/i18n";

export default {
    checkNameShipping(type){
        switch (type) {
            case AppConstants.SHIP_TYPE.GIAO_HANG_NHANH:
                return strings('typeShippingFl')
            case AppConstants.SHIP_TYPE.GIAO_HANG_TIET_KIEM:
                return strings('typeShippingTK')
            case AppConstants.SHIP_TYPE.GIAO_HANG_CHUAN:
                return strings('typeShippingChuan')
        }
    }
}
