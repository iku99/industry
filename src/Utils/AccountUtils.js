import {EventRegister} from 'react-native-event-listeners';
import GlobalInfo from './Common/GlobalInfo';
import CartUtils from './CartUtils';
import FuncUtils from './FuncUtils';
import AppConstants from '../resource/AppConstants';
import {AsyncStorage} from 'react-native';
import { Actions } from "react-native-router-flux";

export default class AccountUtils {
  static mAccountUtils = null;

  static getInstance() {
    if (AccountUtils.mAccountUtils == null) {
      AccountUtils.mAccountUtils = new AccountUtils();
    }
    return this.mAccountUtils;
  }

  // async checkExistedEmail(params, callbackFunction) {
  //   let responseData = await HelperHandle.getInstance()
  //     .executePostWithoutToken(Constants.apiEndpoint.checkExistedEmail, params);
  //   if (GlobalUtil.isRequestSuccess(responseData)) {
  //     callbackFunction(true, responseData, '')
  //   } else {
  //     callbackFunction(false, responseData, responseData.errorMsg)
  //   }
  // }
  // async registerPhone(params, callbackFunction) {
  //   let responseData = await HelperHandle.getInstance()
  //     .executePostWithoutToken(Constants.apiEndpoint.registerPhone +"?"+ GlobalUtil.objToQueryString(params), []);
  //   if (GlobalUtil.isRequestSuccess(responseData)) {
  //     callbackFunction(true, responseData, '')
  //   } else {
  //     callbackFunction(false, responseData, responseData.errorMsg)
  //   }
  // }
  // async verifyPhone(params, callbackFunction) {
  //   let responseData = await HelperHandle.getInstance()
  //     .executePostWithoutToken(Constants.apiEndpoint.phoneVerify +"?"+ GlobalUtil.objToQueryString(params), []);
  //   if (GlobalUtil.isRequestSuccess(responseData)) {
  //     callbackFunction(true, responseData, '')
  //   } else {
  //     callbackFunction(false, responseData, responseData.errorMsg)
  //   }
  // }
  // async register(params, callbackFunction) {
  //   let responseData = await HelperHandle.getInstance()
  //     .executePost(Constants.apiEndpoint.register, params);
  //   if (GlobalUtil.isRequestSuccess(responseData)) {
  //     callbackFunction(true, responseData, '')
  //   } else {
  //     callbackFunction(false, responseData, responseData.errorMsg)
  //   }
  // }
  // async updateUserInfo(params, callbackFunction) {
  //   let responseData = await HelperHandle.getInstance()
  //     .executePut(Constants.apiEndpoint.updateUserInfo, params);
  //   if (GlobalUtil.isRequestSuccess(responseData)) {
  //     callbackFunction(true, responseData, '')
  //   } else {
  //     callbackFunction(false, responseData, responseData.errorMsg)
  //   }
  // }
  // async changePassword(params, callbackFunction) {
  //   let responseData = await HelperHandle.getInstance()
  //     .executePut(Constants.apiEndpoint.changePassword, params);
  //   if (GlobalUtil.isRequestSuccess(responseData)) {
  //     callbackFunction(true, responseData, '')
  //   } else {
  //     callbackFunction(false, responseData, responseData.errorMsg)
  //   }
  // }
  // async login(params, callbackFunction) {
  //   let responseData = await HelperHandle.getInstance()
  //     .executePostWithoutToken(Constants.apiEndpoint.login, params);
  //   if (GlobalUtil.isRequestSuccess(responseData)) {
  //     if(responseData['data'] != null && responseData['data'].oauth != null){
  //       GlobalInfo.userInfo = responseData['data'].info;
  //       GlobalInfo.token_type = responseData['data'].oauth.token_type;
  //       GlobalInfo.access_token = responseData['data'].oauth.access_token;
  //       GlobalInfo.refresh_token = responseData['data'].oauth.refresh_token;
  //       GlobalInfo.expires_in = responseData['data'].oauth.expires_in;
  //       GlobalInfo.scope = responseData['data'].oauth.scope
  //     }
  //     FuncUtils.getInstance().isLogged = true;
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.UserData,JSON.stringify(GlobalInfo.userInfo), null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.IsLogin,'1',null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.token_type,GlobalInfo.token_type,null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.LOGIN_TYPE,AppConstants.LOGIN_TYPE.USER,null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.access_token,GlobalInfo.access_token,null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.user_name, params.email_phone, null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.userValidation, params.password, null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.lastTimeLogin, DateUtil.getValidateTime().toString(), null);
  //     callbackFunction(true, responseData, '');
  //     this.handleAfterLogin();
  //   } else {
  //     callbackFunction(false, responseData, responseData.errorMsg)
  //   }
  // }
  // async googleLogin(params, callbackFunction) {
  //   let responseData = await HelperHandle.getInstance()
  //     .executePostWithoutToken(Constants.apiEndpoint.googleLogin, params);
  //   if (GlobalUtil.isRequestSuccess(responseData)) {
  //     if(responseData['data'] != null && responseData['data'].oauth != null){
  //       GlobalInfo.userInfo = responseData['data'].info;
  //       GlobalInfo.token_type = responseData['data'].oauth.token_type;
  //       GlobalInfo.access_token = responseData['data'].oauth.access_token;
  //       GlobalInfo.refresh_token = responseData['data'].oauth.refresh_token;
  //       GlobalInfo.expires_in = responseData['data'].oauth.expires_in;
  //       GlobalInfo.scope = responseData['data'].oauth.scope
  //     }
  //     FuncUtils.getInstance().isLogged = true;
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.UserData,JSON.stringify(GlobalInfo.userInfo), null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.IsLogin,'1',null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.token_type,GlobalInfo.token_type,null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.LOGIN_TYPE,AppConstants.LOGIN_TYPE.GOOGLE,null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.access_token,GlobalInfo.access_token,null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.lastTimeLogin, DateUtil.getValidateTime().toString(), null);
  //     if(callbackFunction !== undefined) callbackFunction(true, responseData, '');
  //     this.handleAfterLogin();
  //     LogUtils.showLog("Login google success")
  //   } else {
  //     if(callbackFunction !== undefined) callbackFunction(false, responseData, responseData.errorMsg);
  //     LogUtils.showLog("Login google failed")
  //   }
  // }
  // async facebookLogin(params, callbackFunction) {
  //   let responseData = await HelperHandle.getInstance()
  //     .executePostWithoutToken(Constants.apiEndpoint.facebookLogin, params);
  //   if (GlobalUtil.isRequestSuccess(responseData)) {
  //     if(responseData['data'] != null && responseData['data'].oauth != null){
  //       GlobalInfo.userInfo = responseData['data'].info;
  //       GlobalInfo.token_type = responseData['data'].oauth.token_type;
  //       GlobalInfo.access_token = responseData['data'].oauth.access_token;
  //       GlobalInfo.refresh_token = responseData['data'].oauth.refresh_token;
  //       GlobalInfo.expires_in = responseData['data'].oauth.expires_in;
  //       GlobalInfo.scope = responseData['data'].oauth.scope
  //     }
  //     FuncUtils.getInstance().isLogged = true;
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.UserData,JSON.stringify(GlobalInfo.userInfo), null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.IsLogin,'1',null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.token_type,GlobalInfo.token_type,null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.LOGIN_TYPE,AppConstants.LOGIN_TYPE.FACEBOOK,null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.access_token,GlobalInfo.access_token,null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.lastTimeLogin, DateUtil.getValidateTime().toString(), null);
  //     callbackFunction(true, responseData, '');
  //     this.handleAfterLogin();
  //     LogUtils.showLog("Login facebook success");
  //   } else {
  //     callbackFunction(false, responseData, responseData.errorMsg);
  //     LogUtils.showLog("Login facebook failed");
  //   }
  // }
  //
  // async appleLogin(params, callbackFunction) {
  //   let responseData = await HelperHandle.getInstance()
  //     .executePostWithoutToken(Constants.apiEndpoint.appleLogin, params);
  //   if (GlobalUtil.isRequestSuccess(responseData)) {
  //     if(responseData['data'] != null && responseData['data'].oauth != null){
  //       GlobalInfo.userInfo = responseData['data'].info;
  //       GlobalInfo.token_type = responseData['data'].oauth.token_type;
  //       GlobalInfo.access_token = responseData['data'].oauth.access_token;
  //       GlobalInfo.refresh_token = responseData['data'].oauth.refresh_token;
  //       GlobalInfo.expires_in = responseData['data'].oauth.expires_in;
  //       GlobalInfo.scope = responseData['data'].oauth.scope
  //     }
  //     FuncUtils.getInstance().isLogged = true;
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.UserData,JSON.stringify(GlobalInfo.userInfo), null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.IsLogin,'1',null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.token_type,GlobalInfo.token_type,null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.LOGIN_TYPE,AppConstants.LOGIN_TYPE.APPLE,null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.access_token,GlobalInfo.access_token,null);
  //     await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.lastTimeLogin, DateUtil.getValidateTime().toString(), null);
  //
  //     callbackFunction(true, responseData, '');
  //     this.handleAfterLogin();
  //     LogUtils.showLog("Login facebook success");
  //   } else {
  //     callbackFunction(false, responseData, responseData.errorMsg);
  //     LogUtils.showLog("Login facebook failed");
  //   }
  // }
  //
  // handleAfterLogin(){
  //   SocketBusiness.getInstance().connectSocket()
  // }
  // async forgotPassword(params, callbackFunction) {
  //   let responseData = await HelperHandle.getInstance()
  //     .executePost(Constants.apiEndpoint.forgetPassword, params);
  //   if (GlobalUtil.isRequestSuccess(responseData)) {
  //     callbackFunction(true, responseData, '')
  //   } else {
  //     callbackFunction(false, responseData, responseData.errorMsg)
  //   }
  // }
  //
  // async getUserInfo(userId, callbackFunction){
  //   let userInfo = userInfoMap[userId];
  //   if(userInfo !== undefined){ //Da ton tai trong cache
  //     callbackFunction(true, userInfo, '');
  //     return
  //   }
  //   let responseData = await HelperHandle.getInstance()
  //     .executeGet(Constants.apiEndpoint.getUserInfo.replace("{userId}",userId));
  //   if (GlobalUtil.isRequestSuccess(responseData)) {
  //     let userInfo = responseData["data"];
  //     userInfoMap[userId] = userInfo;
  //     callbackFunction(true, userInfo, '')
  //   } else {
  //     callbackFunction(false, responseData, responseData.errorMsg)
  //   }
  // }
  async logout() {
    GlobalInfo.userInfo = {};
    GlobalInfo.access_token = undefined;
    CartUtils.updateTotal(0);
    FuncUtils.getInstance().isLogged = false;
    await AsyncStorage.removeItem(AppConstants.SharedPreferencesKey.UserData);
    await AsyncStorage.removeItem(AppConstants.SharedPreferencesKey.IsLogin);
    await AsyncStorage.removeItem(AppConstants.SharedPreferencesKey.user_name);
    await AsyncStorage.removeItem(
      AppConstants.SharedPreferencesKey.userValidation,
    );
    let type = AsyncStorage.getItem(
      AppConstants.SharedPreferencesKey.LOGIN_TYPE,
    );
    await AsyncStorage.removeItem(AppConstants.SharedPreferencesKey.LOGIN_TYPE);
    // switch (type) {
    //   case AppConstants.LOGIN_TYPE.GOOGLE:
    //     GoogleAuthen.getInstance().signOut();
    //     break;
    //   case AppConstants.LOGIN_TYPE.FACEBOOK:
    //     FacebookAuthen.getInstance().logOut();
    //     break;
    //   case AppConstants.LOGIN_TYPE.APPLE:
    //     AppleAuthen.getInstance().logout();
    //     break;
    //   default:
    //     break;
    // }
    EventRegister.emit(AppConstants.EventName.LOGIN, false);
    Actions.reset('login');
  }
}
