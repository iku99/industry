import AppConstants from '../resource/AppConstants';
import {strings} from '../resource/languages/i18n';
import CurrencyFormatter from './CurrencyFormatter';
import {EventRegister} from 'react-native-event-listeners';
import DateUtil from './DateUtil';
import ColorStyle from "../resource/ColorStyle";

let total = 0;
export default {
  getCartStatus(status) {
    switch (status) {
      case AppConstants.STATUS_ORDER.UNCONFIRMED:
        return strings('un_confirmed');
      case AppConstants.STATUS_ORDER.CONFIRMED:
        return strings('confirmed');
      case AppConstants.STATUS_ORDER.DELIVERING:
        return strings('delivering');
      case AppConstants.STATUS_ORDER.SUCCESS:
        return strings('success');
      case AppConstants.STATUS_ORDER.USER_CANCEL:
        return strings('userCancel');
      case AppConstants.STATUS_ORDER.SHOP_CANCEL:
        return strings('shopCancel');
      // case AppConstants.TrangThaiDonHang.COMPLETE:
      //   return strings('sus');
    }
  },
  getNotificationsOrder(status) {
    switch (status) {
      case AppConstants.STATUS_ORDER.TO_PAY:
        return strings('to_pay');
      case AppConstants.STATUS_ORDER.UNCONFIRMED:
        return strings('unConfirmed');
      case AppConstants.STATUS_ORDER.CONFIRMED:
        return strings('confirmed');
      case AppConstants.STATUS_ORDER.DELIVERING:
        return strings('delivering');
      case AppConstants.STATUS_ORDER.SUCCESS:
        return strings('success');
      case AppConstants.STATUS_ORDER.USER_CANCEL:
        return strings('cancelUser');
      case AppConstants.STATUS_ORDER.SHOP_CANCEL:
        return strings('shopCancel');
    }
  },
  getDesNotiOrder(status, date) {
    switch (status) {
      case AppConstants.STATUS_ORDER.TO_PAY:
        return (
            strings('desTextUnpaid') +
            ' ' +
            DateUtil.formatDate('DD/MM/YYYY', date)
        );
      case AppConstants.STATUS_ORDER.UNCONFIRMED:
        return (
          strings('desTextUnConfirmed') +
          ' ' +
          DateUtil.formatDate('DD/MM/YYYY', date)
        );
      case AppConstants.STATUS_ORDER.CONFIRMED:
        return (
          strings('desTextUnConfirmed') +
          ' ' +
          DateUtil.formatDate('DD/MM/YYYY', date)
        );
      case AppConstants.STATUS_ORDER.DELIVERING:
        return (
          strings('desTextDelivering') +
          ' ' +
          DateUtil.formatDate('DD/MM/YYYY', date)
        );
      case AppConstants.STATUS_ORDER.SUCCESS:
        return (
          strings('desTextCompleted') +
          ' ' +
          DateUtil.formatDate('DD/MM/YYYY', date)
        );
      case AppConstants.STATUS_ORDER.USER_CANCEL:
        return strings('desTextCanceled');
    }
  },
  getTimeNotiOrder(status, date) {
    switch (status) {
      case AppConstants.STATUS_ORDER.TO_PAY:
        return (
            strings('orderTime') +
            ' ' +
            DateUtil.formatDate('DD/MM/YYYY', date)
        );
      case AppConstants.STATUS_ORDER.UNCONFIRMED:
        return (
          strings('orderTime') +
          ' ' + DateUtil.formatDate('DD/MM/YYYY HH:mm ', date)
        );
      case AppConstants.STATUS_ORDER.CONFIRMED:
        return (
          strings('orderTime') +
          ' ' +
          DateUtil.formatDate('DD/MM/YYYY HH:mm ', date)
        );
      case AppConstants.STATUS_ORDER.DELIVERING:
        return (
          strings('orderShip') +
          ' ' +
          DateUtil.formatDate('DD/MM/YYYY HH:mm ', date)
        );
      case AppConstants.STATUS_ORDER.SUCCESS:
        return (
          strings('completedTime') +
          ' ' +
          DateUtil.formatDate('DD/MM/YYYY HH:mm ', date)
        );
      case AppConstants.STATUS_ORDER.USER_CANCEL:
        return (
          strings('cancelTime') +
          ' ' +
          DateUtil.formatDate('DD/MM/YYYY HH:mm ', date)
        );
    }
  },
  getColorText(status) {
    switch (status) {
      case AppConstants.STATUS_ORDER.UNCONFIRMED:
        return ColorStyle.tabActive;
      case AppConstants.STATUS_ORDER.CONFIRMED:
        return '#0092ce';
      case AppConstants.STATUS_ORDER.DELIVERING:
        return '#864329';
      case AppConstants.STATUS_ORDER.DA_GIAO:
        return '#0092ce';
      case AppConstants.STATUS_ORDER.USER_CANCEL:
        return '#eb1e25';
      case AppConstants.STATUS_ORDER.SHOP_CANCEL:
        return '#eb1e25';
      case AppConstants.STATUS_ORDER.SUCCESS:
        return '#5FBB77';
    }
  },
  getShipFee(final_shipping_fee) {
    if (final_shipping_fee === 0) {
      return strings('free');
    } else {
      return CurrencyFormatter(final_shipping_fee);
    }
  },
  getPayment(type) {
    switch (type) {
      case AppConstants.PAYMENT_TYPE.COD:
        return strings('shipCod');
      case AppConstants.PAYMENT_TYPE.CREDIT_CARD:
        return strings('shipCreditCard');
      case AppConstants.PAYMENT_TYPE.ATM:
        return strings('shipAtm');
    }
  },
  getTotal() {
    return total;
  },
  updateTotal(mTotal) {
    total = mTotal;
    EventRegister.emit(AppConstants.EventName.GET_TOTAL_CART, mTotal);
  },
  getBadgeCount() {
    total = total + 1;
    EventRegister.emit(AppConstants.EventName.GET_TOTAL_CART, total);
  },
  registerListener(callback) {
    EventRegister.addEventListener(
      AppConstants.EventName.GET_TOTAL_CART,
      total => {
        callback(total);
      },
    );
  },
};
