import AppConstants from '../resource/AppConstants';
import constants from '../Api/constants';
import {strings} from '../resource/languages/i18n';
import ColorStyle from '../resource/ColorStyle';

export default {
  convertListToRowList(items, type) {
    let itemList = []; // Item trên cùng 1 hàng
    let list = []; //số hàng
    let itemCountInRow = AppConstants.isTablet ? 3 : 2;
    let insertCount = 0;
    for (let i = 0; i < items.length; i++) {
      let item = items[i];
      if (insertCount < itemCountInRow) {
        itemList.push(item);
        insertCount = insertCount + 1;
      }
      if (insertCount === itemCountInRow) {
        let _item = {
          type: type,
          data: itemList,
        };
        list.push(_item);
        itemList = [];
        insertCount = 0;
      }
    }
    if (insertCount !== 0) {
      let _item = {
        type: type,
        data: itemList,
      };
      list.push(_item);
      itemList = [];
      insertCount = 0;
    }
    return list;
  },
  getPercent(item) {
    if (item.original_price === 0) {
      return 0;
    }
    let percent = Math.floor(
      (100 * (item.original_price - item.price)) / item.original_price,
    );
    return percent > 0 ? percent : 0;
  },
  isReviewed(item) {
    return item.reviewed;
  },
  getImageUrl(images,index) {
    if (images != null &&images != undefined && images.length > 0) {
      return constants.host + images[0].url;
    } else {
      return 'https://picsum.photos/130/214?' +index;
    }
  },
  getImages(item) {
    let imagesUrl = [];
    if (item !== undefined && item.image_url !== undefined || item?.image_url.length!==0) {
      let images = item?.image_url;
      images.forEach(item => {
        let url = item.url;
        if (url.startsWith(AppConstants.LOCAL_SIGNAL)) {
          imagesUrl.push(url.replace(AppConstants.LOCAL_SIGNAL, ''));
        } else {
          imagesUrl.push(constants.host + item.url);
        }
      });
    }
    if (imagesUrl.length === 0) {
      imagesUrl.push('https://picsum.photos/300/300?productId=' + item.id);
    }
    return imagesUrl;
  },
  getQuantityText(item) {
    if (item.qty === undefined || item.qty === 0) {
      return strings('hetHang');
    }
    return `Còn lại ${item.qty} sản phẩm`;
  },
  getBarcodeUrl(item) {
    return 'https://www.qr-code-generator.com/wp-content/themes/qr/new_structure/markets/core_market_full/generator/dist/generator/assets/images/websiteQRCode_noFrame.png';
  },
  getProductOptionDes(item, productOptionId) {
    let result = '';
    let optionGroups = item.option_groups;
    let options = item.options;
    if (options === undefined || optionGroups === undefined) {
      return;
    }
    let productOption = [];
    options.forEach((item, index) => {
      if (item.id === productOptionId) {
        let groupIndex = item.groups_index;
        item.groups_index.forEach((option, index) => {
          productOption.push(optionGroups[index].values[option]);
        });
        result = productOption.join(' - ');
      }
    });
    return result;
  },
  getProductStatus(product) {
    switch (product.status) {
      case AppConstants.PRODUCT_STATUS.NOT_YET_REVIEW:
        return strings('notReviewProduct');
      case AppConstants.PRODUCT_STATUS.REVIEWED:
        return '';
      case AppConstants.PRODUCT_STATUS.REVIEWED_NOT_OK:
        return strings('reviewedNotOk');
      default:
        return 'N/A';
    }
  },
  getProductStatusColor(product) {
    switch (product.status) {
      case AppConstants.PRODUCT_STATUS.NOT_YET_REVIEW:
      case AppConstants.PRODUCT_STATUS.REVIEWED_NOT_OK:
        return ColorStyle.maskSeller;
      case AppConstants.PRODUCT_STATUS.REVIEWED:
        return 'transparent';
      default:
        return '#ffffff';
    }
  },
  getShareLinkProduct(id) {
    return `${constants.hostWeb}/eplaza/p${id}`;
  },
  // getProductType(product){
  //     if ()
  // }
};
