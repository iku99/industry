import AppConstants from "../resource/AppConstants";
import {strings} from "../resource/languages/i18n";

export default {
  getIndex(type) {
    switch (type) {
      case AppConstants.ORDER_STATUS.ALL:
        return 0;
      case AppConstants.ORDER_STATUS.UNCONFIRMED:
        return 1;
      case AppConstants.ORDER_STATUS.CONFIRMED:
        return 2;
      case AppConstants.ORDER_STATUS.DELIVERING:
        return 3;
      case AppConstants.ORDER_STATUS.CANCELLED:
        return 4;
      case AppConstants.ORDER_STATUS.SHOP_CANCEL:
        return 5;
      case AppConstants.ORDER_STATUS.SUCCESS:
        return 6;
      default:
        return 0;
    }
  },
  getText(type) {
    switch (type) {
      case AppConstants.STATUS_ORDER.UNCONFIRMED:
        return strings('date_uncomf');
      case AppConstants.STATUS_ORDER.CONFIRMED:
        return strings('date_comf');
      case AppConstants.STATUS_ORDER.DELIVERING:
        return  strings('date_dev');
      case AppConstants.STATUS_ORDER.SUCCESS:
        return  strings('date_success');
      case AppConstants.STATUS_ORDER.TO_PAY:
        return  strings('date_checkout');
      default:
        return 0;
    }
  },
};
