import {strings} from '../resource/languages/i18n';

const TimeUtils = {
  formatTimeString(start, end) {
    //10 giờ
    // let seconds = Math.floor(time / 1000);
    // let minutes = Math.floor(time / 60000);
    // let hours = Math.floor(time / 3600000);
    // let msecs = time % 1000;
    //
    // if (msecs < 10) {
    //   msecs = `00${msecs}`;
    // } else if (msecs < 100) {
    //   msecs = `0${msecs}`;
    // }
    //
    // let seconds = Math.floor(time / 1000);
    // let minutes = Math.floor(time / 60000);
    // let hours = Math.floor(time / 3600000);
    // seconds = seconds - minutes * 60;
    // minutes = minutes - hours * 60;
    // let formatted;
    // if (showMsecs) {
    //   formatted = `${hours < 10 ? 0 : ''}${hours}:${
    //     minutes < 10 ? 0 : ''
    //   }${minutes}:${seconds < 10 ? 0 : ''}${seconds}:${msecs}`;
    // } else {
    //   formatted = `${hours < 10 ? 0 : ''}${hours}:${
    //     minutes < 10 ? 0 : ''
    //   }${minutes}:${seconds < 10 ? 0 : ''}${seconds}`;
    // }
    //
    // return formatted;
  },
  formatTime(time) {
    let day = new Date(time).getDay();
    let month = new Date(time).getMonth();
    let year = new Date(time).getFullYear();
    let hours = new Date(time).getHours();
    let minutes = new Date(time).getMinutes('MM');
    return hours + ':' + minutes + ' ' + day + '/' + month + '/' + year;
  },
  formatDay(time) {
    let day = new Date(time).getDay();
    let month = new Date(time).getMonth();
    let year = new Date(time).getFullYear();
    return day + '/' + month + '/' + year;
  },
  timeDiff(time) {
    let result = '';
    const t = Date.parse(time);
    const now = new Date().getTime();
    const diff = Math.floor((now - t) / 1000);
    const months = Math.floor(diff / (60 * 60 * 24 * 30));
    if (months > 0) {
      result = `${months} ${strings('months')}`;
    } else {
      const day = Math.floor(diff / (60 * 60 * 24));
      if (day > 0) {
        result = `${day} ${strings('days')}`;
      } else {
        result = '';
      }
    }
    return result;
  },
};

export default TimeUtils;
