let conversationId;
export default class ChatUtils {
  static mChatUtils = null;

  static getInstance() {
    if (ChatUtils.mChatUtils == null) {
      ChatUtils.mChatUtils = new ChatUtils();
    }
    return this.mChatUtils;
  }
  setConversationId(mConversationId) {
    conversationId = mConversationId;
  }
  getConversationId() {
    return conversationId;
  }
}
