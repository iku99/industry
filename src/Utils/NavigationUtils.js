import {AsyncStorage} from 'react-native';
import {Actions} from 'react-native-router-flux';
import AppConstants from '../resource/AppConstants';
import {EventRegister} from 'react-native-event-listeners';
import ProductHandle from '../sagas/ProductHandle';

export default {
  isLogin(callback) {
    AsyncStorage.getItem(
      AppConstants.SharedPreferencesKey.IsLogin,
      (error, isLogin) => {
        if (isLogin === '1') {
          //Da dang nhap
          callback();
        } else {
          //Chua dang nhap
          Actions.jump('login');
        }
      },
    );
  },

  goToCart() {
    this.isLogin(() => {
      Actions.jump('main');
      EventRegister.emit(AppConstants.EventName.GO_TO_TAB, 3);
    });
  },
  goToProductDetail(product, groupId, hasFlashSale) {
    if (Actions.currentScene !== 'productDetail') {
      Actions.jump('productDetail', {
        item: product,
        groupId,
        hasFlashSale: hasFlashSale,
      });
    } else {
      Actions.push('productDetail', {
        item: product,
        groupId,
        hasFlashSale: hasFlashSale,
      });
    }
  },
  goToProductDetail1(product) {
    ProductHandle.getInstance().getProductDetail(
      product.id,
      undefined,
      (isSuccess, dataResponse) => {
        if (isSuccess) {
          let data = dataResponse.data;
          Actions.push('productDetail', {
            item: data,
          });
        }
      },
    );
  },
  goToCheckout(products, type) {
    Actions.jump('checkout', {products: products, type: type});
  },
  goToOrderList(index, isStore) {
    Actions.jump('order', {index, isStore});
  },
  goToOrderDetail(order, hideActionBT, cancelOrderFunc, completeOrder) {
    Actions.jump('orderDetail', {
      order,
      hideActionBT,
      cancelOrderFunc,
      completeOrder,
    });
  },
  goToOrderDetailSeller(order, hideActionBT, orderFunc) {
    Actions.jump('orderDetailSeller', {
      order,
      hideActionBT,
      orderFunc,
    });
  },
  goToNotificationList(type, callback) {
    Actions.jump('notificationList', {type, callback});
  },
  goToActive() {
    Actions.jump('active');
  },

  goToGroupDetail(group, checkRole, onRefresh) {
    Actions.jump('groupDetail', {item: group, checkRole, onRefresh});
  },
  goToConversation(userId, userName) {
    Actions.jump('conversationView', {userId, userName});
  },
  goToSendImage(image, onSend) {
    Actions.jump('sendImageScreen', {image, onSend});
  },
  goToRegisterStore() {
    Actions.jump('storeRegister');
  },
  goToStoreDetail(storeId) {
    Actions.jump('storeRegister', {storeId});
  },
  goToProductManageProfile() {
    Actions.jump('productManagerProfile');
  },
  goToUploadProduct(productId,idStore, index, onCallback) {
    Actions.jump('uploadProduct', {productId,idStore, index, onCallback});
  },
  goToAddressList(onCallback, type) {
    Actions.jump('address', {onCallback, type});
  },
  goToAddressModify(addressItem, callBackFunc, onDelete) {
    Actions.jump('addressModify', {addressItem, callBackFunc, onDelete});
  },
  goToProductManageGroup(groupId) {
    Actions.jump('productManage', {groupId});
  },
  goToWebviewScreen(url, onRedirect) {
    Actions.jump('webviewScreen', {url, onRedirect});
  },
  goToRoleManage(groupId) {
    Actions.jump('roleManager', {groupId});
  },
  goToRoleModify(onRefresh, groupId, roleId) {
    Actions.jump('newRole', {onRefresh, groupId, roleId});
  },
  goToListVoucher(item,callback){
    Actions.jump('voucherList',{item: item,callback});
  },
  goToUpdateSeller(storeId, callBackFunc) {
    Actions.jump('updateSeller', {storeId, callBackFunc});
  },
  goToPostDetail(
    post,
    groupId,
    groupName,
    index,
    onChangeItemData,
    deleteFunc,
  ) {
    Actions.jump('postDetail', {
      post,
      groupId,
      groupName,
      index,
      onChangeItemData,
      deleteFunc,
    });
  },
  goToNewsDetail(post, newsId, index, onChangeItemData, deleteFunc) {
    Actions.jump('newsDetail', {
      post,
      newsId,
      index,
      onChangeItemData,
      deleteFunc,
    });
  },
  goToLoginView() {
    Actions.jump('login');
  },
  goToGroupMembers(groupId) {
    Actions.jump('groupMember', {groupId});
  },
  goToPostManager(groupId) {
    Actions.jump('postManager', {groupId});
  },
  goToAddPost(item, callBack) {
    Actions.jump('addPostToGroup', {item, callBack});
  },
  goToMemberManager(groupId) {
    Actions.jump('memberManager', {groupId});
  },

  goToProductList(category) {
    Actions.jump('productList', {node: category});
  },
  goToProductCategory(data) {
    Actions.jump('productList', {node: data});
  },
  goToDiscoverGroup() {
    Actions.jump('discoverGroup');
  },
  goToFullViewImageList(images, initIndex, onDeleteImage) {
    Actions.jump('fullViewImage', {images, initIndex, onDeleteImage});
  },
  goToVoucherList() {
    Actions.jump('voucherList', {});
  },
  goToVoucherModify(voucher, onRefresh) {
    Actions.jump('voucherModify', {voucher, onRefresh});
  },
  goToStoreRefund(isStore, storeId) {
    Actions.jump('storeRefund', {isStore, storeId});
  },
  goToInformationGroup(data) {
    Actions.jump('informationGroup', {data});
  },
  goToInformationGroupManage(data) {
    Actions.jump('informationGroupManage', {data});
  },
  goToMemberGroup(idGroup, data) {
    Actions.jump('memberGroup', {idGroup, data});
  },
  goToStoreRefundDetail(refund, isStore) {
    Actions.jump('storeRefundDetail', {refund, isStore});
  },
  goToVoteProductList(isStore) {
    Actions.jump('voteProductList', {isStore});
  },
  goToHelpScreen() {
    Actions.jump('helpScreen');
  },
  goToStaticWenView(url) {
    Actions.jump('staticWebView', {url});
  },
  goToSearchProduct(item) {
    Actions.jump('searchScreen', {item});
  },
  goToShopDetail(storeId) {
    Actions.jump('storeDetail', {storeId});
  },
  goToRemindView() {
    Actions.jump('remindProduct');
  },
  goToContactInfo() {
    Actions.jump('contactInfo');
  },
  goToReview(idProduct) {
    Actions.jump('reviewScreen', {idProduct});
  },
  goToCartScreen() {
    Actions.jump('main');
    EventRegister.emit(AppConstants.EventName.GO_TO_TAB, 4);
  },
};
