// import messaging from "@react-native-firebase/messaging";
// import {Platform} from "react-native";
// import LogUtils from "../Base/LogUtils";
//
//
// class FirebaseService {
//     register = (onRegister, onNotification, onOpenNotification) => {
//         this.checkPermission(onRegister);
//         this.createNotificationListeners(onRegister, onNotification, onOpenNotification)
//     };
//
//     registerAppWithFCM = async() => {
//         if (Platform.OS === "ios") {
//             await messaging().registerDeviceForRemoteMessages();
//             await messaging().setAutoInitEnabled(true)
//         }
//     };
//     checkPermission = (onRegister) => {
//         messaging().hasPermission()
//             .then(enabled => {
//                 if (enabled) {
//                     // User has permissions
//                     this.getToken(onRegister)
//                 } else {
//                     // User doesn’t have permission
//                     this.requestPermission(onRegister)
//                 }
//             }).catch(error => {
//             LogUtils.showLog("[FCMService] Permission rejected " + error)
//         })
//     };
//
//     getToken = (onRegister) => {
//         messaging().getToken()
//             .then(fcmToken => {
//                 if (fcmToken) {
//                     onRegister(fcmToken);
//                     // this.sendTokenToServer(fcmToken)
//                 } else {
//                     LogUtils.showLog("[FCMService] User does not have a device token")
//                 }
//             }).catch(error => {
//             LogUtils.showLog("[FCMService] getToken rejected ", error)
//         })
//     };
//
//     requestPermission = (onRegister) => {
//         messaging().requestPermission()
//             .then(() => {
//                 this.getToken(onRegister)
//             }).catch(error => {
//             LogUtils.showLog("[FCMService] Request Permission rejected " + error)
//         })
//     };
//
//
//     deleteToken = () => {
//         LogUtils.showLog("[FCMService] deleteToken ");
//         messaging().deleteToken()
//             .catch(error => {
//                 LogUtils.showLog("[FCMService] Delete token error " + error)
//             })
//     };
//
//     createNotificationListeners = (onRegister, onNotification, onOpenNotification) => {
//         // When the application is running, but in the background
//         messaging()
//             .onNotificationOpenedApp(remoteMessage => {
//                 // LogUtils.showLog("[FCMService] onNotificationOpenedApp Notification caused app to open from background state:" + remoteMessage);
//                 if (remoteMessage) {
//                     onOpenNotification(remoteMessage)
//                     // this.removeDeliveredNotification(notification.notificationId)
//                 }
//             });
//         // When the application is opened from a quit state.
//         messaging()
//             .getInitialNotification()
//             .then(remoteMessage => {
//                 LogUtils.showLog("[FCMService] getInitialNotification Notification caused app to open from quit state:" + remoteMessage);
//                 if (remoteMessage) {
//                     const notification = remoteMessage.notification;
//                     onOpenNotification(notification)
//                     // this.removeDeliveredNotification(notification.notificationId)
//                 }
//             });
//         // Foreground state messages
//         this.messageListener = messaging().onMessage(async remoteMessage => {
//             // LogUtils.showLog("[FCMService] A new FCM message arrived!"+ JSON.stringify(remoteMessage));
//             if (remoteMessage) {
//                 let notification = remoteMessage;
//                 // if (Platform.OS === "ios") {
//                 //     notification = remoteMessage.data.notification
//                 // } else {
//                 //     notification = remoteMessage.notification
//                 // }
//                 onNotification(notification)
//             }
//         });
//         messaging().setBackgroundMessageHandler(async  remoteMessage =>{
//             // LogUtils.showLog("[FCMService Background] A new FCM message arrived!"+ JSON.stringify(remoteMessage));
//             if (remoteMessage) {
//                 let notification = null;
//                 if (Platform.OS === "ios") {
//                     notification = remoteMessage.data.notification
//                 } else {
//                     notification = remoteMessage.notification
//                 }
//                 onNotification(notification)
//             }
//         });
//         // Triggered when have new token
//         messaging().onTokenRefresh(fcmToken => {
//             // LogUtils.showLog("[FCMService] New token refresh: "+ fcmToken);
//             // this.sendTokenToServer(fcmToken);
//             onRegister(fcmToken)
//         })
//     };
//     unRegister = () => {
//         this.messageListener()
//     }
// }
//
// export const firebaseService = new FirebaseService();
