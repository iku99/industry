import constants from "../Api/constants";
import HelperHandle from "./Common/HelperHandle";
import GlobalUtil from "./Common/GlobalUtil";

export default class RefundMetaUtils {
  static U0 = {id: 0, name: 'Bạn đã gửi yêu cầu', color: 'green'};
  static U10 = {id: 10, name: 'Điều chỉnh yêu cầu', color: 'green'};
  static E10 = {id: 10, name: 'Chờ người bán chấp nhận', color: 'blue'};
  static E11 = {id: 11, name: 'VietChem đã từ chối', color: 'red'};
  static S20 = {id: 20, name: 'Shop chấp nhận yêu cầu', color: 'green'};
  static S21 = {id: 21, name: 'Shop đã từ chối', color: 'red'};
  static U22 = {id: 22, name: 'Bạn đã gửi hàng', color: 'blue'};
  static S23 = {
    id: 23,
    name: 'Shop đã nhận sản phẩm/ Chờ hoàn tiền',
    color: 'blue',
  };
  static S24 = {
    id: 24,
    name: 'Shop đã nhận sản phẩm/ Sản phẩm lỗi',
    color: 'blue',
  };
  static U30 = {id: 30, name: 'Huỷ yêu cầu', color: 'red'};
  static U31 = {id: 31, name: 'Yêu cầu VietChem xem xét', color: 'blue'};
  static U32 = {id: 32, name: 'Yêu cầu HIAC xem xét', color: 'blue'};
  static E33 = {id: 33, name: 'Xác nhận kết quả', color: 'blue'};
  static E40 = {id: 40, name: 'Đã hoàn tiền', color: 'green'};
  static C50 = {id: 50, name: 'Hoàn thành', color: 'green'};
  static REFUND_STATUS = [
    this.U0,
    this.E10,
    this.E11,
    this.S20,
    this.S21,
    this.U22,
    this.S23,
    this.S24,
    this.U30,
    this.U31,
    this.U32,
    this.E33,
    this.E40,
    this.C50,
  ];
  reasons = undefined;
  expectResult = undefined;
  static mRefundMetaUtils = null;
  static getInstance() {
    if (RefundMetaUtils.mRefundMetaUtils == null) {
      RefundMetaUtils.mRefundMetaUtils = new RefundMetaUtils();
    }
    return this.mRefundMetaUtils;
  }

  async getData(callback) {
    if (this.reasons === undefined || this.expectResult === undefined) {
      let responseData = await HelperHandle.getInstance().executeGet(
        constants.apiEndpoint.getRefundMeta,
      );
      if (GlobalUtil.isRequestSuccess(responseData)) {
        this.handleResponse(responseData);
        callback(true, this.reasons, this.expectResult);
      } else {
        //DucND: Don't something when error. Maybe to kill someone =))
        callback(false, undefined, undefined);
      }
    } else {
      callback(true, this.reasons, this.expectResult);
    }
  }
  handleResponse(responseData) {
    let data = responseData.data;
    if (data !== undefined) {
      this.reasons = data.reasons;
      this.expectResult = data.results;
    }
  }
  async refundOrder(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.refunds,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async updateRefundStatus(id, params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePut(
      constants.apiEndpoint.updateRefunds.replace('{id}', id),
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  static getStatusText(status) {
    let refundStatus = {};
    let size = this.REFUND_STATUS.length;
    for (let i = 0; i < size; i++) {
      let mStatus = this.REFUND_STATUS[i];
      if (mStatus.id === status) {
        refundStatus = mStatus;
        break;
      }
    }
    return refundStatus;
  }
  static getListUpdateStatus(status, isStore) {
    switch (status) {
      case this.U0.id:
        return isStore ? [] : [this.U30];
      case this.E11.id:
        return [];
      case this.E10.id:
        return isStore ? [this.S20, this.S21] : [this.U30];
      case this.S20.id:
        return isStore ? [] : [this.U30, this.U22];
      case this.S21.id:
        return isStore ? [] : [];
      case this.U22.id:
        return isStore ? [this.S23, this.S24] : [];
      case this.S23.id:
        return isStore ? [] : [];
      case this.S24.id:
        return isStore ? [] : [this.U10, this.U30, this.U31];
      case this.U31.id:
        return isStore ? [] : [this.U30, this.U32];
      case this.E40.id:
        return isStore ? [] : [this.C50];
      default:
        return [];
    }
  }
}
