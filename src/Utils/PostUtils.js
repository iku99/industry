import AppConstants from "../resource/AppConstants";
import { strings } from "../resource/languages/i18n";

export default {
  getStatusStr(status) {
    switch (status) {
      case AppConstants.POST_STATUS.PENDING:
        return strings('postPending');
      case AppConstants.POST_STATUS.REJECTED:
        return strings('postReject');
      case AppConstants.POST_STATUS.APPROVED:
        return strings('postApprove');
      default:
        return '';
    }
  },
};
