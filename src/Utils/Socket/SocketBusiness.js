import {io} from 'socket.io-client';
import constants from '../../Api/constants';
import GlobalInfo from '../Common/GlobalInfo';
let socket;
let listeners = [];
export default class SocketBusiness {
  static CONVERSATION_LIST_ID = -1;
  static instance = null;

  static getInstance() {
    if (SocketBusiness.instance == null) {
      SocketBusiness.instance = new SocketBusiness();
    }
    return this.instance;
  }
  connectSocket() {
    socket = io(constants.socketHost, {
      path: '/sock',
    });
    socket.on('connect', () => {
      socket.emit('authenticate', GlobalInfo.access_token);
      socket.onAny(function (event, data) {
        listeners.forEach(mListener => {
          mListener.callback(data);
        });
      });
    });
  }
  sendData(content) {
    if (socket === undefined) {
      this.connectSocket();
    }
    socket.emit('message.send', content);
  }
  sendTyping(to, typing) {
    let payload = {
      to,
      typing,
    };
    this.sendData(payload);
  }
  addListener(mListener) {
    listeners = [...listeners, mListener];
  }
  removeListener(id) {
    listeners = listeners.filter(item => item.id !== id);
  }
}
