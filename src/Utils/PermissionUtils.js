import {
  check,
  openSettings,
  PERMISSIONS,
  request,
  RESULTS,
} from 'react-native-permissions';
import AppConstants from '../resource/AppConstants';
import ViewUtils from './ViewUtils';
import {strings} from '../resource/languages/i18n';

export default {
  async checkPermission(permission) {
    if (AppConstants.IS_IOS) {
      check(permission).then(result => {
        if (result === RESULTS.GRANTED) {
          return true;
        } else {
          request(permission).then(result1 => {
            if (result1 === RESULTS.GRANTED) {
              return true;
            } else {
              ViewUtils.showAskAlertDialog(
                strings('pleaseGrantPhotoPermission'),
                () => {
                  openSettings().catch(() =>
                    console.warn('cannot open settings'),
                  );
                },
              );
              return false;
            }
          });
        }
      });
    } else {
      return true;
    }
  },
};
