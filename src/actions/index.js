import {
  BANNER_INFO,
  BANNER_LIST, GET_CART,
  GET_CATEGORY,
  GET_PRODUCT_LIST, GET_SELLER, LOGIN,
  REGISTER,
} from "./actionTypes";

// export const fetchMoviesAction = sort => {
//   return {
//     type: FETCH_MOVIE,
//     sort,
//   };
// };
export const getBannerAction = sort => {
  return {
    type: BANNER_LIST,
    sort,
  };
};

export const getBannerInfoAction = sort => {
  return {
    type: BANNER_INFO,
    sort,
  };
};
export const getCategoryAction = sort => {
  return {
    type: GET_CATEGORY,
    sort,
  };
};
export const getCartAction = sort => {
  return {
    type: GET_CART,
    sort,
  };
};
export const getProductAction = sort => {
  return {
    //lưu dưới đạng 1 type,2 dữ liệu
    // type -->Kiểm tra và gọi tới nó
    type: GET_PRODUCT_LIST,
    sort,
  };
};
export const getProductAllAction = sort => {
  return {
    type: GET_PRODUCT_LIST,
    sort,
  };
};
export const postRegister = sort => {
  return {
    type: REGISTER,
    sort,
  };
};
export const getSeller = payload => {
  return {
    type: GET_SELLER,
    payload,
  };
};
export const postLogin = sort => {
  return {
    type: LOGIN,
    sort,
  };
};
