import { FAILED, GET_CATEGORY, SUCCEEDED } from "../actions/actionTypes";
const categoryReducers = (list = [], action) => {
  switch (action.type) {
    case GET_CATEGORY:
      return action.sort;
    case FAILED:
      return [];
    default:
      return list;
  }
};

export default categoryReducers;
