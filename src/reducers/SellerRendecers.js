import {GET_SELLER} from '../actions/actionTypes';
const initData = {
  name: '',
  des: '',
  address: '',
  email: '',
  phone: '',
  // province_id: '',
  // district_id: '',
  // ward_id: '',
  // businessTypeIndex: '',
  // delivery: '',
};
const SellerReducers = (state = initData, {type, payload}) => {
  console.log('payload:',payload)
  switch (type) {
    case GET_SELLER:
      return {
        ...state,
        name: payload.name,
        des: payload.des,
        address: payload.address,
        email: payload.email,
        phone: payload.phone,
        // province_id: payload.province_id,
        // district_id: payload.district_id,
        // ward_id: payload.ward_id,
        // delivery: payload.delivery,
        // businessTypeIndex: payload.businessTypeIndex,
      };
    default:
      return state;
  }
};
export default SellerReducers;
