import { FAILED, GET_CART, LOGIN, SUCCEEDED } from "../actions/actionTypes";
const CartReducers= (list = [], action) => {
  switch (action.type) {
    case GET_CART:
      return action.sort;
    case FAILED:
      return [];
    default:
      return list;
  }
};
export default CartReducers;
