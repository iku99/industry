import { BANNER_INFO, FAILED, SUCCEEDED } from "../actions/actionTypes";
const bannerReducers = (list = [], action) => {
  switch (action.type) {
    case SUCCEEDED:
      return action.responseData;
    case FAILED:
      return [];
    default:
      return list;
  }
};
export default bannerReducers;
