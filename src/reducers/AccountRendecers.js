import {FAILED, LOGIN, SUCCEEDED} from '../actions/actionTypes';
const Accounts = (list = [], action) => {
  switch (action.type) {
    case LOGIN:
      return action;
    case FAILED:
      return [];
    default:
      return list;
  }
};
export default Accounts;
