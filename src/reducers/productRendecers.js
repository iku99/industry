import {FAILED, GET_PRODUCT_LIST, SUCCEEDED} from '../actions/actionTypes';
const data = [];
const productReducers = (list = [], action) => {
  switch (action.type) {
    case GET_PRODUCT_LIST:
      return action;
    case FAILED:
      return [];
    default:
      return list;
  }
};

export default productReducers;
