import bannerReducers from './bannerRendecers';
import categoryReducers from './categoryRendecers';
import productReducers from './productRendecers';
import Accounts from './AccountRendecers';
import SellerReducers from "./SellerRendecers";
const {combineReducers} = require('redux');
const allReducers = combineReducers({
  bannerReducers,
  categoryReducers,
  productReducers,
  Accounts,
  SellerReducers
});
export default allReducers;
