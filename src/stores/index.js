import createSagaMiddleware from 'redux-saga';
import {applyMiddleware, createStore} from 'redux';
import rootSaga from '../sagas/rootSaga';
import allReducers from '../reducers';

const sagaMiddleware = createSagaMiddleware();
let store = createStore(allReducers, applyMiddleware(sagaMiddleware));
export default store;
sagaMiddleware.run(rootSaga);
