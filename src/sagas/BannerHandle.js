import HelperHandle from '../Utils/Common/HelperHandle';
import constants from '../Api/constants';
import GlobalUtil from '../Utils/Common/GlobalUtil';

export default class BannerHandle {
  static mBannerHandle = null;

  static getInstance() {
    if (BannerHandle.mBannerHandle == null) {
      BannerHandle.mBannerHandle = new BannerHandle();
    }
    return this.mBannerHandle;
  }
  async getBannerList(param,callbackFunction) {
    console.log(  constants.apiEndpoint.getBanners+  '?' +
        GlobalUtil.objToQueryString(param))
    let responseData = await HelperHandle.getInstance().executeGetWithoutToken(
      constants.apiEndpoint.getBanners+  '?' +
        GlobalUtil.objToQueryString(param),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getBannerInfo(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePostWithoutToken(
      constants.apiEndpoint.getBannersInfo,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
}
