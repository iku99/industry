export default class LoginHandle {
  static loginHandle = null;

  static getInstance() {
    if (LoginHandle.loginHandle == null) {
      LoginHandle.loginHandle = new LoginHandle();
    }
    return this.loginHandle;
  }

  async doLogin(requestData, callbackFunction) {
    //this.getLoginRequest(userName, password)
    // let responseData = await SystemHandle.getInstance().accessLogin(requestData);
    callbackFunction(responseData);
  }

  async doLogout(callbackFunction) {
    // await SystemHandle.getInstance().logout();
    callbackFunction();
  }
}
