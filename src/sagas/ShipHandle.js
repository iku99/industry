import constants from "../Api/constants";
import HelperHandle from "../Utils/Common/HelperHandle";
import GlobalUtil from "../Utils/Common/GlobalUtil";
export default class ShipHandle {
    static mShipHandle = null;

    static getInstance() {
        if (ShipHandle.mShipHandle == null) {
            ShipHandle.mShipHandle = new ShipHandle();
        }
        return this.mShipHandle;
    }
    async createShipStore(params,callbackFunction) {
        let responseData = await HelperHandle.getInstance().executePost(constants.apiEndpoint.billShip,params);
        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            //DucND: Don't something when error. Maybe to kill someone =))
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
    async getShip(id,param,callbackFunction) {
        let responseData = await HelperHandle.getInstance().executePut(constants.apiEndpoint.getShip+id,param);
        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            //DucND: Don't something when error. Maybe to kill someone =))
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
    async getMethodShip(callbackFunction) {
        let responseData = await HelperHandle.getInstance().executeGetWithoutToken(constants.apiEndpoint.getMethodShip);
        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            //DucND: Don't something when error. Maybe to kill someone =))
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
    async billShip(params,callbackFunction) {
        let responseData = await HelperHandle.getInstance().executePost(constants.apiEndpoint.billShip,params);
        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            //DucND: Don't something when error. Maybe to kill someone =))
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
}
