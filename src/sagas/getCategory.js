import {call, put, takeLatest, takeEvery} from 'redux-saga/effects';
import {Api} from '../Api';
import constants from '../Api/constants';
import {
  BANNER_INFO,
  BANNER_LIST,
  FAILED,
  GET_CATEGORY,
  SUCCEEDED,
} from '../actions/actionTypes';
import ExecuteHttp from '../Api/https/ExecuteHttp';
import GlobalUtil from '../Utils/Common/GlobalUtil';
import HelperHandle from '../Utils/Common/HelperHandle';
let keyCategories = {};
function* getCategory(params) {
  try {
    let responseData = yield HelperHandle.getInstance().executeGetWithoutToken(
      constants.apiEndpoint.getCategoryMenu +
        '?' +
        GlobalUtil.objToQueryString(params),
    );
    yield put({type: GET_CATEGORY, responseData: responseData});
  } catch (err) {
    yield put({type: FAILED, err});
  }
}
export function* watchGetCategory() {
  yield takeLatest(GET_CATEGORY, getCategory);
}
