import HelperHandle from "../Utils/Common/HelperHandle";
import constants from "../Api/constants";
import GlobalUtil from "../Utils/Common/GlobalUtil";

export default class StatisticalHandle {
    static mStatisticalHandle = null;

    static getInstance() {
        if (StatisticalHandle.mStatisticalHandle == null) {
            StatisticalHandle.mStatisticalHandle = new StatisticalHandle();
        }
        return this.mStatisticalHandle;
    }

    async getStatistical(id, params, callbackFunction) {
        let responseData = await HelperHandle.getInstance().executeGetWithoutToken(constants.apiEndpoint.getStatistical + id +'?'+ GlobalUtil.objToQueryString(params),);
        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            //DucND: Don't something when error. Maybe to kill someone =))
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
}
