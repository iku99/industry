import HelperHandle from "../Utils/Common/HelperHandle";
import constants from "../Api/constants";
import GlobalUtil from "../Utils/Common/GlobalUtil";

export default class LocationHandle {
  static mLocationHandle = null;

  static getInstance() {
    if (LocationHandle.mLocationHandle == null) {
      LocationHandle.mLocationHandle = new LocationHandle();
    }
    return this.mLocationHandle;
  }

  async getProvinces(callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getProvinces,
    );
    if (!GlobalUtil.checkIsNull(responseData)) {
      if (GlobalUtil.isRequestSuccess(responseData)) {
        callbackFunction(true, responseData, '');
      } else {
        callbackFunction(false, responseData, responseData.errorMsg);
      }
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, {}, 'Co loi xay ra');
    }
  }
  async getDistrictsByProvince(provinceId, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getDistrict + provinceId ,
    );
    if (!GlobalUtil.checkIsNull(responseData)) {
      if (GlobalUtil.isRequestSuccess(responseData)) {
        callbackFunction(true, responseData, '');
      } else {
        callbackFunction(false, responseData, responseData.errorMsg);
      }
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, {}, 'Co loi xay ra');
    }
  }
  async getWardsByDistrict(districtId, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getWards + districtId,
    );
    if (!GlobalUtil.checkIsNull(responseData)) {
      if (GlobalUtil.isRequestSuccess(responseData)) {
        callbackFunction(true, responseData, '');
      } else {
        callbackFunction(false, responseData, responseData.errorMsg);
      }
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, {}, 'Co loi xay ra');
    }
  }
}
