import HelperHandle from '../Utils/Common/HelperHandle';
import constants from '../Api/constants';
import GlobalUtil from '../Utils/Common/GlobalUtil';
import GlobalInfo from '../Utils/Common/GlobalInfo';
import FuncUtils from '../Utils/FuncUtils';
import {AsyncStorage} from 'react-native';
import AppConstants from '../resource/AppConstants';
import DateUtil from '../Utils/DateUtil';
import LogUtils from '../Utils/LogUtils';
import SocketBusiness from '../Utils/Socket/SocketBusiness';
import CartUtils from '../Utils/CartUtils';
import GoogleAuthen from './auth/GoogleAuthen';
import FacebookAuthen from './auth/FacebookAuth';
import {EventRegister} from 'react-native-event-listeners';
import {Actions} from 'react-native-router-flux';

let userInfoMap = {};
export default class AccountHandle {
  static mAccountHandle = null;

  static getInstance() {
    if (AccountHandle.mAccountHandle == null) {
      AccountHandle.mAccountHandle = new AccountHandle();
    }
    return this.mAccountHandle;
  }
  async getUser(callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getUser,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      if (responseData !== undefined && responseData.data !== undefined) {
        CartUtils.updateTotal(responseData.data.length);
      }
      if (callbackFunction != null) {
        callbackFunction(true, responseData, '');
      }
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      if (callbackFunction != null) {
        callbackFunction(false, responseData, responseData.errorMsg);
      }
    }
  }
  async checkExistedEmail(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePostWithoutToken(
      constants.apiEndpoint.checkExistedEmail,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async registerPhone(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePostWithoutToken(
      constants.apiEndpoint.registerPhone +
        '?' +
        GlobalUtil.objToQueryString(params),
      [],
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async verifyUserName(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.userNameVerify,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async checkOTP(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.checkOTP,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async register(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.register,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async updateUserInfo(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePut(
      constants.apiEndpoint.updateUserInfo,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async changePassword(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePut(
      constants.apiEndpoint.changePassword,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async changeForgotPassword(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePostWithoutToken(
      constants.apiEndpoint.forgetPassword,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async login(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePostWithoutToken(
      constants.apiEndpoint.login,
      params,
    );

    if (GlobalUtil.isRequestSuccess(responseData)) {
      if (responseData.code === 0) {
        GlobalInfo.userInfo = responseData.data;
        GlobalInfo.access_token = responseData.data?.access_token;
        GlobalInfo.refresh_token = responseData.data?.refresh_token;
      }
      FuncUtils.getInstance().isLogged = true;
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.UserData,
        JSON.stringify(GlobalInfo.userInfo),
        null,
      );

      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.IsLogin,
        '1',
        null,
      );

      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.LOGIN_TYPE,
        AppConstants.LOGIN_TYPE.USER,
        null,
      );

      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.access_token,
        GlobalInfo.access_token,
        null,
      );
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.user_name,
        params.user_name,
        null,
      );
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.userValidation,
        params.password,
        null,
      );
      callbackFunction(true, responseData, '');
      this.handleAfterLogin();
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async googleLogin(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePostWithoutToken(
      constants.apiEndpoint.googleLogin,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      if (responseData.code === 0) {
        GlobalInfo.userInfo = responseData.data;
        GlobalInfo.access_token = responseData.data?.access_token;
        GlobalInfo.refresh_token = responseData.data?.refresh_token;
      }
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.LOGIN_TYPE,
        AppConstants.LOGIN_TYPE.GOOGLE,
        null,
      );
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.access_token,
        GlobalInfo.access_token,
        null,
      );

      FuncUtils.getInstance().isLogged = true;
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.UserData,
        JSON.stringify(GlobalInfo.userInfo),
        null,
      );

      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.IsLogin,
        '1',
        null,
      );
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.access_token,
        GlobalInfo.access_token,
        null,
      );
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.user_name,
        params.toString(),
        null,
      );
      // await AsyncStorage.setItem(
      //     AppConstants.SharedPreferencesKey.userValidation,
      //     params.password,
      //     null,
      // );
      if (callbackFunction !== undefined) {
        callbackFunction(true, responseData, '');
      }
      this.handleAfterLogin();
      LogUtils.showLog('Login google success');
    } else {
      if (callbackFunction !== undefined) {
        callbackFunction(false, responseData, responseData.errorMsg);
      }
      LogUtils.showLog('Login google failed');
    }
  }
  async facebookLogin(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePostWithoutToken(
      constants.apiEndpoint.facebookLogin,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      if (responseData.code === 0) {
        GlobalInfo.userInfo = responseData.data;
        GlobalInfo.access_token = responseData.data?.access_token;
        GlobalInfo.refresh_token = responseData.data?.refresh_token;
      }
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.LOGIN_TYPE,
        AppConstants.LOGIN_TYPE.FACEBOOK,
        null,
      );
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.access_token,
        GlobalInfo.access_token,
        null,
      );

      FuncUtils.getInstance().isLogged = true;
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.UserData,
        JSON.stringify(GlobalInfo.userInfo),
        null,
      );

      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.IsLogin,
        '1',
        null,
      );
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.access_token,
        GlobalInfo.access_token,
        null,
      );
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.user_name,
        params.toString(),
        null,
      );
      callbackFunction(true, responseData, '');
      this.handleAfterLogin();
      LogUtils.showLog('Login facebook success');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
      LogUtils.showLog('Login facebook failed');
    }
  }

  async appleLogin(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePostWithoutToken(
      constants.apiEndpoint.appleLogin,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      if (responseData.code === 0) {
        GlobalInfo.userInfo = responseData.data;
        GlobalInfo.access_token = responseData.data?.access_token;
        GlobalInfo.refresh_token = responseData.data?.refresh_token;
      }
      FuncUtils.getInstance().isLogged = true;
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.UserData,
        JSON.stringify(GlobalInfo.userInfo),
        null,
      );
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.IsLogin,
        '1',
        null,
      );
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.token_type,
        GlobalInfo.token_type,
        null,
      );
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.LOGIN_TYPE,
        AppConstants.LOGIN_TYPE.APPLE,
        null,
      );
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.access_token,
        GlobalInfo.access_token,
        null,
      );
      await AsyncStorage.setItem(
        AppConstants.SharedPreferencesKey.lastTimeLogin,
        DateUtil.getValidateTime().toString(),
        null,
      );

      callbackFunction(true, responseData, '');
      this.handleAfterLogin();
      LogUtils.showLog('Login facebook success');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
      LogUtils.showLog('Login facebook failed');
    }
  }

  handleAfterLogin() {
    SocketBusiness.getInstance().connectSocket();
  }
  async forgotPassword(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePostWithoutToken(
      constants.apiEndpoint.forgetPassword,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async getUserInfo(userId, callbackFunction) {
    let userInfo = userInfoMap[userId];
    if (userInfo !== undefined) {
      //Da ton tai trong cache
      callbackFunction(true, userInfo, '');
      return;
    }
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getUserInfo.replace('{userId}', userId),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      let userInfo = responseData.data;
      userInfoMap[userId] = userInfo;
      callbackFunction(true, userInfo, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async logout() {
    GlobalInfo.userInfo = {};
    GlobalInfo.access_token = undefined;
    CartUtils.updateTotal(0);
    FuncUtils.getInstance().isLogged = false;
    await AsyncStorage.removeItem(AppConstants.SharedPreferencesKey.UserData);
    await AsyncStorage.removeItem(AppConstants.SharedPreferencesKey.IsLogin);
    await AsyncStorage.removeItem(AppConstants.SharedPreferencesKey.user_name);
    await AsyncStorage.removeItem(
      AppConstants.SharedPreferencesKey.userValidation,
    );
    let type = await AsyncStorage.getItem(
      AppConstants.SharedPreferencesKey.LOGIN_TYPE,
    );
    await AsyncStorage.removeItem(AppConstants.SharedPreferencesKey.LOGIN_TYPE);
    switch (type) {
      case AppConstants.LOGIN_TYPE.GOOGLE:
        GoogleAuthen.getInstance().signOut();
        break;
      case AppConstants.LOGIN_TYPE.FACEBOOK:
        FacebookAuthen.getInstance().signOut();
        break;
      // case AppConstants.LOGIN_TYPE.APPLE:
      //   AppleAuthen.getInstance().logout();
      //   break;
      default:
        break;
    }
    EventRegister.emit(AppConstants.EventName.LOGIN, false);
    Actions.reset('flashscreen');
  }

  async searchUser(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.searchUser +
        '?' +
        GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
}
