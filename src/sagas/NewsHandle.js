import HelperHandle from "../Utils/Common/HelperHandle";
import constants from "../Api/constants";
import GlobalUtil from "../Utils/Common/GlobalUtil";

export default class NewsHandle {
    static mNewsHandle = null;
    static getInstance() {
        if (NewsHandle.mNewsHandle == null) {
            NewsHandle.mNewsHandle = new NewsHandle();
        }
        return this.mNewsHandle;
    }

    async getNews(params, callbackFunction,withoutToken) {
        console.log( constants.apiEndpoint.getNews +
            '?' +
            GlobalUtil.objToQueryString(params),)
        let responseData ;
        if(withoutToken){
            responseData= await HelperHandle.getInstance().executeGet(
                constants.apiEndpoint.getNews +
                '?' +
                GlobalUtil.objToQueryString(params),
            );
        }else {
            responseData = await HelperHandle.getInstance().executeGetWithoutToken(
                constants.apiEndpoint.getNews +
                '?' +
                GlobalUtil.objToQueryString(params),
            );
        }

        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
    async getNewsDetail(id, callbackFunction) {
        console.log( constants.apiEndpoint.getNewsDetail +id)
         let   responseData = await HelperHandle.getInstance().executeGetWithoutToken(
                constants.apiEndpoint.getNewsDetail +id,
            );

        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
    async getTradePromotion(params, callbackFunction) {
        let responseData = await HelperHandle.getInstance().executeGetWithoutToken(
          constants.apiEndpoint.getNews +
          '?' +
          GlobalUtil.objToQueryString(params),
        );
        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
    async getNewsCate(params, callbackFunction) {
        let responseData = await HelperHandle.getInstance().executeGetWithoutToken(
          constants.apiEndpoint.getNewsCate +
          '?' +
          GlobalUtil.objToQueryString(params),
        );
        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
}
