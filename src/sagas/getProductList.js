import {call, put, takeLatest, takeEvery} from 'redux-saga/effects';
import {Api} from '../Api';
import constants from '../Api/constants';
import {
  BANNER_INFO,
  BANNER_LIST,
  FAILED,
  GET_CATEGORY,
  GET_PRODUCT_LIST,
  SUCCEEDED,
} from '../actions/actionTypes';
import ExecuteHttp from '../Api/https/ExecuteHttp';
import GlobalUtil from '../Utils/Common/GlobalUtil';
import HelperHandle from '../Utils/Common/HelperHandle';
let keyCategories = {};
function* getProductList(params, withoutToken) {
  try {
    let responseData;
    if (withoutToken) {
      responseData = yield HelperHandle.getInstance().executeGetWithoutToken(
        constants.apiEndpoint.getProductList+'?'+GlobalUtil.objToQueryString(params.sort),
      );
    } else {
      responseData = yield HelperHandle.getInstance().executeGet(
        constants.apiEndpoint.getProductList+'?'+GlobalUtil.objToQueryString(params.sort),
      );
    }
    yield put({type: SUCCEEDED, responseData: responseData});
  } catch (err) {
    yield put({type: FAILED, err});
  }
}
export function* watchGetProductList() {
  yield takeLatest(GET_PRODUCT_LIST, getProductList);
}
