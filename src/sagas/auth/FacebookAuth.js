import {AccessToken, GraphRequest, GraphRequestManager, LoginManager, Settings} from "react-native-fbsdk-next";
import AccountHandle from "../AccountHandle";
export default class FacebookAuth {
  static mFacebookAuth= null;

  static getInstance() {
    if (FacebookAuth.mFacebookAuth == null) {
      FacebookAuth.mFacebookAuth = new FacebookAuth();
    }
    return this.mFacebookAuth;
  }

  async signInWithFacebook(callback) {
    const tokenObj = await AccessToken.getCurrentAccessToken();
    const infoRequest = new GraphRequest(
        '/me',
        {
          accessToken: tokenObj.accessToken,
          parameters: {
            fields: {
              string:
                  'email,name,first_name,middle_name,last_name,gender,address,picture.type(large)',
            },
          },
        },
        (error, res) => {
          if (res) {
            AccessToken.getCurrentAccessToken().then(accessToken => {
              let params = {
                accessToken: accessToken.accessToken.toString(),
                ...res
              };
              AccountHandle.getInstance().facebookLogin(
                  params,
                  (isSuccess, responseData) => {
                    callback(isSuccess);
                  },
              );
            });
          }
        },
    );
    new GraphRequestManager().addRequest(infoRequest).start();
  }
  loginFacebook(callback) {
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
        result => {
          if (result.isCancelled) {
            alert('Login cancelled');
              callback()
          } else {
            this.signInWithFacebook(callback);
          }
        },
        error => {
          // EventRegister.emit(AppConstants.EventName.LOADING_ORTHER_LOGIN, false);
          alert('Login fail with error: ' + error);
            callback()
            console.log(error)
        },
    );
  }

  signOut() {
    LoginManager.logOut();
  }
  //Create response callback.
  _responseInfoCallback = (error, result) => {
    if (error) {
      alert('Error fetching data: ' + error.toString());
    } else {
      alert('Result Name: ' + result.name);
    }
  };
}
