import appleAuth from "@invertase/react-native-apple-authentication";
import {AsyncStorage} from "react-native";
import AppConstants from "../../resource/AppConstants";
import DataUtils from "../../Utils/DataUtils";
import AccountHandle from "../AccountHandle";

export default class AppleAuth {
    static instance = null;

    static getInstance() {
        if (AppleAuth.instance == null) {
            AppleAuth.instance = new AppleAuth();
        }
        return this.instance
    }
    async login(callback, forceLogin = true){
        let tokenKey = "";
        if(forceLogin){

            // performs login request
            const appleAuthRequestResponse = await appleAuth.performRequest({
                requestedOperation: appleAuth.Operation.LOGIN,
                requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
            });
            // get current authentication state for user
            // /!\ This method must be tested on a real device. On the iOS simulator it always throws an error.
            const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);
            // use credentialState response to ensure the user is authenticated
            if (credentialState === appleAuth.State.AUTHORIZED) {
                tokenKey = appleAuthRequestResponse['identityToken']
            }
        }else {
            tokenKey = await AsyncStorage.getItem(AppConstants.SharedPreferencesKey.APPLE_KEY);
        }
        if(DataUtils.stringNullOrEmpty(tokenKey)){
            callback(false);
            this.logout();
            return ;
        }
        let params = {
            access_token:tokenKey
        };
        AccountHandle.getInstance().appleLogin(params,(isSuccess, dataResponse, msg)=>{
            callback(isSuccess);
            if(isSuccess){
                AsyncStorage.setItem(AppConstants.SharedPreferencesKey.APPLE_KEY,appleAuthRequestResponse['identityToken'],null);
            }
        })
    }
    logout(){
        appleAuth.Operation.LOGOUT;
        AsyncStorage.removeItem(AppConstants.SharedPreferencesKey.APPLE_KEY, null)
    }
}
