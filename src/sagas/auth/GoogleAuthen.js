import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import {Alert} from 'react-native';
import config from '../../components/elements/GoogleLogin/config';
import AccountHandle from '../AccountHandle';

export default class GoogleAuth {
  static mGoogleAuth = null;

  static getInstance() {
    if (GoogleAuth.mGoogleAuth == null) {
      GoogleAuth.mGoogleAuth = new GoogleAuth();
      GoogleAuth.mGoogleAuth.configureGoogleSignIn();
    }
    return this.mGoogleAuth;
  }

  configureGoogleSignIn() {
    GoogleSignin.configure({
      scopes: ['email'],
      webClientId: config.webClientId,
      offlineAccess: true,
    });
  }

  async isSignedIn() {
    return await GoogleSignin.isSignedIn();
  }
  async signInGoogle() {
    try {
      await GoogleSignin.hasPlayServices();
      await GoogleSignin.signIn();
      return true;
    } catch (error) {
      switch (error.code) {
        case statusCodes.SIGN_IN_CANCELLED:
          // sign in was cancelled
          return false;
        case statusCodes.IN_PROGRESS:
          // operation (eg. sign in) already in progress
          Alert.alert('In progress', error.code);
          return false;
        case statusCodes.PLAY_SERVICES_NOT_AVAILABLE:
          // android only
          Alert.alert('play services not available or outdated');
          return false;
        default:
          Alert.alert('Something went wrong', error.toString());
          return false;
      }
    }
  }
  async signInToApp(callback, forceSignInAgain) {
    let isLogin = await this.isSignedIn();
    if (isLogin) {
      await this.signInToAppImplLogin(callback);
    } else if (forceSignInAgain) {
      let loginSuccess = await this.signInGoogle();
      if (loginSuccess) {
        await this.signInToAppImpl(callback);
      } else {
        callback();
      }
    }
  }
  async signInToAppImpl(callback) {
    try {
      await GoogleSignin.signInSilently();
      const isSignedIn = await GoogleSignin.getTokens();
      const user = await GoogleSignin.getCurrentUser();
      console.log('u:', user);
      let params = {
        accessToken: isSignedIn.accessToken,
        ...user,
      };
      AccountHandle.getInstance().googleLogin(params, callback);
    } catch (e) {
      console.log('e:', e);
      callback(false);
    }
  }
  async signInToAppImplLogin(callback) {
    try {
      await GoogleSignin.signInSilently();
      const isSignedIn = await GoogleSignin.getTokens();
      const user = await GoogleSignin.getCurrentUser();
      console.log('u:', user);
      let params = {
        accessToken: isSignedIn.accessToken,
        ...user,
      };
      AccountHandle.getInstance().googleLogin(params, callback);
    } catch (e) {
      let loginSuccess = await this.signInGoogle();
      if (loginSuccess) {
        await this.signInToAppImpl(callback);
      } else {
        callback();
      }
    }
  }
  async signOut() {
    await GoogleSignin.revokeAccess();
    await GoogleSignin.signOut();
  }
}
