import {call, all, fork, delay, join} from 'redux-saga/effects';
import {watchGetBannerList} from './BannerSagas';
import {watchGetCategory} from './getCategory';
import {watchGetProductList} from './getProductList';
import { watchRegister } from "./AccountSaga";
import { watchLogin } from "./LoginSaga";
//call chạy 1 cái saga nào đó
//all chạy tất cả
// fock gọi ra song song
export default function* rootSaga() {
  yield all([watchGetBannerList(),watchRegister(),watchLogin()]);
}
