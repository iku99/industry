import constants from '../Api/constants';
import {call, put, takeLatest, takeEvery} from 'redux-saga/effects';
import {FAILED, LOGIN, SUCCEEDED} from '../actions/actionTypes';
import HelperHandle from '../Utils/Common/HelperHandle';
import GlobalUtil from '../Utils/Common/GlobalUtil';
import GlobalInfo from '../Utils/Common/GlobalInfo';
import FuncUtils from '../Utils/FuncUtils';
import {Actions} from 'react-native-router-flux';
import AppConstants from '../resource/AppConstants';
import {AsyncStorage} from 'react-native';
 function* login(params) {
  try {
    let responseData = yield HelperHandle.getInstance().executePostWithoutToken(
      constants.apiEndpoint.login,
      params.sort,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      GlobalInfo.userInfo = responseData.data.info;
      GlobalInfo.token_type = responseData.data.oauth.token_type;
      GlobalInfo.access_token = responseData.data.oauth.access_token;
      GlobalInfo.refresh_token = responseData.data.oauth.refresh_token;
      GlobalInfo.expires_in = responseData.data.oauth.expires_in;
      GlobalInfo.scope = responseData.data.oauth.scope;
      FuncUtils.getInstance().isLogged = true;
      // await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.UserData,JSON.stringify(GlobalInfo.userInfo), null);
      // await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.token_type,GlobalInfo.token_type,null);
      // await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.LOGIN_TYPE,AppConstants.LOGIN_TYPE.APPLE,null);
      // await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.access_token,GlobalInfo.access_token,null);
      //
      // await AsyncStorage.setItem(AppConstants.SharedPreferencesKey.lastTimeLogin, DateUtil.getValidateTime().toString(), null);
      Actions.reset('drawer');
      yield put({type: LOGIN, responseData: responseData.data.info});
    }
  } catch (err) {
    yield put({type: FAILED, err});
  }
}
export function* watchLogin() {
  yield takeLatest(LOGIN, login);
}
