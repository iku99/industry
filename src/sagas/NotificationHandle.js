import HelperHandle from '../Utils/Common/HelperHandle';
import constants from '../Api/constants';
import GlobalUtil from '../Utils/Common/GlobalUtil';

export default class NotificationHandle {
  static mNotificationHandle = null;

  static getInstance() {
    if (NotificationHandle.mNotificationHandle == null) {
      NotificationHandle.mNotificationHandle = new NotificationHandle();
    }
    return this.mNotificationHandle;
  }
  async getNotifications(params, callbackFunction) {
    console.log(constants.apiEndpoint.getNotifications +
        '?' +
        GlobalUtil.objToQueryString(params))
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getNotifications +
        '?' +
        GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getCountNotification(params, callbackFunction) {
    console.log(constants.apiEndpoint.getCountNotification +
        '?' +
        GlobalUtil.objToQueryString(params))
    let responseData = await HelperHandle.getInstance().executeGet(
        constants.apiEndpoint.getCountNotification +
        '?' +
        GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async updateNotifications(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePut(
      constants.apiEndpoint.updateNotifications ,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
}
