import HelperHandle from "../Utils/Common/HelperHandle";
import constants from "../Api/constants";
import GlobalUtil from "../Utils/Common/GlobalUtil";
import GlobalInfo from "../Utils/Common/GlobalInfo";

export default class VoucherHandle {
    static mVoucherHandle = null;

    static getInstance() {
        if (VoucherHandle.mVoucherHandle == null) {
            VoucherHandle.mVoucherHandle = new VoucherHandle();
        }
        return this.mVoucherHandle;
    }
    async createVoucher(params, callbackFunction) {
        let responseData = await HelperHandle.getInstance().executePost(
          constants.apiEndpoint.createVoucher,
          params,
        );
        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }

    async updateVoucher( params,id, callbackFunction) {
        let responseData = await HelperHandle.getInstance().executePut(
          constants.apiEndpoint.updateVoucher+id,
          params,
        );
        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
    async deleteVoucher( id, callbackFunction) {
        let responseData = await HelperHandle.getInstance().executeDelete(
          constants.apiEndpoint.deleteVoucher+id,
        );
        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
    async getListVoucher(params,body,callbackFunction) {
        let responseData = await HelperHandle.getInstance().executePost(
          constants.apiEndpoint.getVoucher+'?'+
          GlobalUtil.objToQueryString(params),body
        );
        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
    async getListVoucherEco(params,body,callbackFunction) {
        let responseData = await HelperHandle.getInstance().executePost(
          constants.apiEndpoint.getVoucherEco+'?'+
          GlobalUtil.objToQueryString(params),body
        );
        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
    async getListVoucherApply(params,callbackFunction) {
        let responseData = await HelperHandle.getInstance().executePost(
          constants.apiEndpoint.getVoucherStore,params
        );
        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
    async getVoucherByCode(body,callbackFunction) {

        let responseData = await HelperHandle.getInstance().executePost(
          constants.apiEndpoint.getVoucherByCode,body
        );
        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
    async applyVoucher(body,callbackFunction) {
        console.log( constants.apiEndpoint.applyVoucher)
        let responseData = await HelperHandle.getInstance().executePost(
          constants.apiEndpoint.applyVoucher,body
        );
        if (GlobalUtil.isRequestSuccess(responseData)) {
            callbackFunction(true, responseData, '');
        } else {
            callbackFunction(false, responseData, responseData.errorMsg);
        }
    }
}
