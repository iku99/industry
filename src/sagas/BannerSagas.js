import {call, put, takeLatest, takeEvery} from 'redux-saga/effects';
import {Api} from '../Api';
import constants from '../Api/constants';
import {
  BANNER_INFO,
  BANNER_LIST,
  FAILED,
  SUCCEEDED,
} from '../actions/actionTypes';
import ExecuteHttp from '../Api/https/ExecuteHttp';
import GlobalUtil from '../Utils/Common/GlobalUtil';
import HelperHandle from '../Utils/Common/HelperHandle';
import AppConstants from '../resource/AppConstants';
import {EventRegister} from 'react-native-event-listeners';
let banners = [];
const data = [];
function* getBannerInfo() {
  try {
    const receivedBanner = yield ExecuteHttp.getInstance().get(
      constants.apiEndpoint.getBanners,
    );
    banners = receivedBanner.data.list;
    if (banners === undefined) {
      banners = [];
    }
    let objectId = [];
    banners.forEach(banner => {
      objectId.push(banner.id);
    });
    let param = {
      object_ids: objectId,
      object_type: AppConstants.IMAGE_UPLOAD_TYPE.BANNER,
    };
    let responseData = yield HelperHandle.getInstance().executePostWithoutToken(
      constants.apiEndpoint.getBannersInfo,
      param,
    );
    let finalBanners = [];
    let bannerInfoList = responseData.data.list;
    let bannerSize = banners.length;
    let bannerInfoSize = bannerInfoList.length;
    for (let i = 0; i < bannerSize; i++) {
      let banner = banners[i];
      for (let j = 0; j < bannerInfoSize; j++) {
        let bannerInfo = bannerInfoList[j];
        if (banner.id === bannerInfo.object_id) {
          banner = {
            ...banner,
            info: bannerInfo,
          };
          finalBanners.push(banner);
        }
      }
    }
    let data = {};
    finalBanners.forEach(banner => {
      let key = banner.position;
      let oldData = data[key];
      oldData = oldData !== undefined ? oldData : [];
      oldData.push(banner);
      data[key] = oldData;
    });
    EventRegister.emit(AppConstants.EventName.LOAD_GROUP_PRODUCT, data);
    yield put({type: SUCCEEDED, responseData: finalBanners});
  } catch (err) {
    yield put({type: FAILED, err});
  }
}
export function* watchGetBannerList() {
  yield takeLatest(BANNER_INFO, getBannerInfo);
}
