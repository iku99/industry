import HelperHandle from '../Utils/Common/HelperHandle';
import constants from '../Api/constants';
import GlobalUtil from '../Utils/Common/GlobalUtil';

export default class MessageHandle {
  static mMessageHandle = null;

  static getInstance() {
    if (MessageHandle.MessageHandle == null) {
      MessageHandle.mMessageHandle = new MessageHandle();
    }
    return this.mMessageHandle;
  }
  async getChatInboxs(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getMessageInbox +'?'+GlobalUtil.objToQueryString(params)
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async sendTextMsg(params,id, callbackFunction) {
    console.log( constants.apiEndpoint.sendTextMsg.replace('{id}',id))
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.sendTextMsg.replace('{id}',id),
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async sendImgMsg(image, params, onProgress, callbackFunction) {
    let data = new FormData();
    let name = image.path.split('/');
    data.append('image_file', {
      uri: image.path,
      type: 'image/jpeg', // or photo.type
      name: name[name.length - 1],
    });
    let url =
      constants.apiEndpoint.sendImageMsg +
      '?' +
      GlobalUtil.objToQueryString(params);
    HelperHandle.getInstance().executePostUpload(
      url,
      data,
      progress => {
        onProgress(progress);
      },
      async (isSuccess, responseData) => {
        callbackFunction(isSuccess, responseData);
      },
    );
  }

  async getChatMessageList(id, params, callbackFunction) {
    console.log(constants.apiEndpoint.getMessageList+id)
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getMessageList+id
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
}
