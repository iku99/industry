import HelperHandle from '../Utils/Common/HelperHandle';
import constants from '../Api/constants';
import GlobalUtil from '../Utils/Common/GlobalUtil';
import CartUtils from "../Utils/CartUtils";

export default class CartHandle {
  static mCartHandle = null;

  static getInstance() {
    if (CartHandle.mCartHandle == null) {
      CartHandle.mCartHandle = new CartHandle();
    }
    return this.mCartHandle;
  }

  async addToCart(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.addToCart,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async getCart(callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getCart,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      if (responseData !== undefined && responseData.data !== undefined) {
        CartUtils.updateTotal(responseData.total);
      }
      if (callbackFunction != null) {
        callbackFunction(true, responseData, '');
      }
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      if (callbackFunction != null) {
        callbackFunction(false, responseData, responseData.errorMsg);
      }
    }
  }
  async removeProductFromCart(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeDelete(
      constants.apiEndpoint.deleteProductFromCart,
      params,
    );
    if (responseData) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async updateProductInCart(params,id, callbackFunction) {
    console.log(constants.apiEndpoint.updateProductFromCart+id)
    let responseData = await HelperHandle.getInstance().executePut(
      constants.apiEndpoint.updateProductFromCart+id,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
}
