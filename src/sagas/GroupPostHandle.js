import HelperHandle from '../Utils/Common/HelperHandle';
import constants from '../Api/constants';
import GlobalUtil from '../Utils/Common/GlobalUtil';

export default class GroupPostHandle {
  static mGroupPostHandle = null;

  static getInstance() {
    if (GroupPostHandle.mGroupPostHandle == null) {
      GroupPostHandle.mGroupPostHandle = new GroupPostHandle();
    }
    return this.mGroupPostHandle;
  }

  async getPostsOfGroup(groupId, params, callbackFunction) {
    console.log(constants.apiEndpoint.getGroupPost +groupId+ '?' +GlobalUtil.objToQueryString(params))
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getGroupPost +groupId+
        '?' +
        GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getPostsDetail(groupId, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(constants.apiEndpoint.getPostDetail+groupId);
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getPostsComment(params, callbackFunction) {
    console.log('data:',constants.apiEndpoint.getPostComment+'?' +
        GlobalUtil.objToQueryString(params))
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getPostComment+'?' +
        GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async getPostsLike(param, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getPostLike,param
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async likePost( params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.likePost, params);
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async ViewPost( params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
        constants.apiEndpoint.viewPost, params);
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getAllPosts(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getAllPost +
        '?' +
        GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async commentToPost( params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.commentToPost, params
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async deleteCommentOfPost(postId, params, callbackFunction) {
    console.log(constants.apiEndpoint.deleteGroupPostComment+postId)
    let responseData = await HelperHandle.getInstance().executeDelete(
      constants.apiEndpoint.deleteGroupPostComment+postId,params);
    if (responseData) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async editCommentOfPost(postId, params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePut(
      constants.apiEndpoint.insertGroupPostComment+postId,
      params,
    );
    if (responseData) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async updateStatusPost(postId, params, callbackFunction){
    console.log('params:',constants.apiEndpoint.updateStatusPost+postId)
    let responseData = await HelperHandle.getInstance().executePut(
        constants.apiEndpoint.updateStatusPost+postId,
        params
    );
    if (responseData) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async deletePost(postId, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeDelete(
      constants.apiEndpoint.deleteGroupPost+postId
    );
    if (responseData) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
}
