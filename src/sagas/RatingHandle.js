import constants from "../Api/constants";
import HelperHandle from "../Utils/Common/HelperHandle";
import GlobalUtil from "../Utils/Common/GlobalUtil";


export default class RatingHandle {
  static mRatingHandle = null;

  static getInstance() {
    if (RatingHandle.mRatingHandle == null) {
      RatingHandle.mRatingHandle = new RatingHandle();
    }
    return this.mRatingHandle;
  }

  async getProductRating(id, params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGetWithoutToken(constants.apiEndpoint.getRating + id +'?'+ GlobalUtil.objToQueryString(params),);
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getRatingUser( params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(constants.apiEndpoint.getRatingUser +'?'+ GlobalUtil.objToQueryString(params));
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async sendRating( param, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
        constants.apiEndpoint.sendRating,
        param,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getRatingCount(id, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
        constants.apiEndpoint.getRatingCount + id
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getRatingStats(id, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getRating + id + '/ratings/stats',
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getRatingStore(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
        constants.apiEndpoint.getRatingStore +'?'+ GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
}
