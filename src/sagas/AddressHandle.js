import HelperHandle from "../Utils/Common/HelperHandle";
import constants from "../Api/constants";
import GlobalUtil from "../Utils/Common/GlobalUtil";

export default class AddressHandle {
  static mAddressHandle = null;

  static getInstance() {
    if (AddressHandle.mAddressHandle == null) {
      AddressHandle.mAddressHandle = new AddressHandle();
    }
    return this.mAddressHandle;
  }
  async getAddressList(callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getAddressList,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async createAddress(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.createAddress,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async updateAddress(id, params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePut(
      constants.apiEndpoint.updateAddress + id,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async deleteAddress(id, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeDelete(
      constants.apiEndpoint.deleteAddress + id,
    );
    if (responseData) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
}
