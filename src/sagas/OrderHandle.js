import RNFetchBlob from 'rn-fetch-blob';
import HelperHandle from '../Utils/Common/HelperHandle';
import constants from '../Api/constants';
import GlobalUtil from '../Utils/Common/GlobalUtil';
import ExecuteHttp from "../Api/https/ExecuteHttp";
import GlobalInfo from "../Utils/Common/GlobalInfo";
import ResultCode from "../Api/https/ResultCode";

export default class OrderHandle {
  static mOrderHandle = null;

  static getInstance() {
    if (OrderHandle.mOrderHandle == null) {
      OrderHandle.mOrderHandle = new OrderHandle();
    }
    return this.mOrderHandle;
  }
  async getOrderByUser(params, callbackFunction) {
    console.log(constants.apiEndpoint.getOrderByUser +
        '?' +
        GlobalUtil.objToQueryString(params))
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getOrderByUser +
        '?' +
        GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getOrderByStore(params, callbackFunction) {
    console.log(constants.apiEndpoint.getOrderByStore+'?' +
        GlobalUtil.objToQueryString(params))
    let responseData = await HelperHandle.getInstance().executeGet(
        constants.apiEndpoint.getOrderByUser +
        '?' +
        GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async cancelOrder(orderId, params, callbackFunction) {
    console.log(constants.apiEndpoint.updateOrder + orderId)
    let responseData = await HelperHandle.getInstance().executePut(
      constants.apiEndpoint.updateOrder + orderId,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getOrderDetail(orderId, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(constants.apiEndpoint.getOrderDetail + orderId);
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async orderNew(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.order,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async updateOnePayStatus(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
        constants.apiEndpoint.onePay,
        params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getResultOrder(url, callbackFunction) {
    await ExecuteHttp.getInstance()
        .get(url)
        .then(responseJson => {
          callbackFunction(responseJson)
        });
  }
}
