// //put:dispatch action
// // takeLatest:theo dõi sự thay đổi action lấy 1 cái cuối
// import {call, put, takeLatest, takeEvery} from 'redux-saga/effects';
// import {Api} from '../Api';
// import constants from '../Api/constants';
// function* fetchMovies() {
//   try {
//     const receivedMovies = yield Api.GET(constants.apiEndpoint.urlGetMovies);
//     yield put({type: SUCCEEDED, receivedMovies: receivedMovies.data});
//   } catch (err) {
//     yield put({type: FAILED, err});
//   }
// }
//
// export function* watchFetchMovies() {
//   yield takeLatest(FETCH_MOVIE, fetchMovies);
// }
