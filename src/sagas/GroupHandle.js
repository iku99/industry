import HelperHandle from '../Utils/Common/HelperHandle';
import constants from '../Api/constants';
import GlobalUtil from '../Utils/Common/GlobalUtil';

export default class GroupHandle {
  static mGroupHandle = null;

  static getInstance() {
    if (GroupHandle.mGroupHandle == null) {
      GroupHandle.mGroupHandle = new GroupHandle();
    }
    return this.mGroupHandle;
  }


  async getSuggestionGroup(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getSuggestionGroup +
        '?' +
        GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getDetailGroup(id, callbackFunction) {
    console.log('team:',constants.apiEndpoint.getDetailGroup + id)
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getDetailGroup + id,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async requestJoinGroup( param, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.groupPath,
      param,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async getMyGroup(param, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getMyGroup +
        '?' +
        GlobalUtil.objToQueryString(param),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async updateGroup(id, param, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePut(
      constants.apiEndpoint.updateGroup + id,
      param,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async deleteGroup(ids, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeDelete(
      constants.apiEndpoint.deleteGroup+ ids,
    );
    callbackFunction(responseData);
  }
  async getProductOfGroup(groupId, param, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getProductGroup.replace('{id}', groupId) +
        '?' +
        GlobalUtil.objToQueryString(param),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async approveProduct(groupId, productId, params, callbackFunction) {
    let url = constants.apiEndpoint.approveGroupProduct
      .replace('{groupId}', groupId)
      .replace('{productId}', productId);
    let responseData = await HelperHandle.getInstance().executePut(url, params);
    if (responseData) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getRoleOfGroup(groupId, param, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getRoleGroup.replace('{id}', groupId) +
        '?' +
        GlobalUtil.objToQueryString(param),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getRoleDetailOfGroup(groupId, roleId, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getRoleGroupDetail
        .replace('{groupId}', groupId)
        .replace('{roleId}', roleId),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async deleteRoleOfGroup(groupId, params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeDelete(
      constants.apiEndpoint.deleteRoleGroup.replace('{groupId}', groupId),
      params,
    );
    if (responseData) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async updateRoleGroup(groupId, roleId, params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePut(
      constants.apiEndpoint.updateRoleGroup
        .replace('{groupId}', groupId)
        .replace('{roleId}', roleId),
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async addRoleGroup(groupId, params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.addRoleGroup.replace('{groupId}', groupId),
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async getMemberOfGroup(groupId, params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getGroupMembers+groupId+
        '?' +
        GlobalUtil.objToQueryString(params),
    );
    console.log(constants.apiEndpoint.getGroupMembers+groupId+
        '?' +
        GlobalUtil.objToQueryString(params))
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async updateMemberGroupStatus(id, params, callbackFunction) {

    let responseData = await HelperHandle.getInstance().executePut(
      constants.apiEndpoint.updateGroupMemberStatus+id,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async deleteMemberOfGroup( userId, params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeDelete(
        constants.apiEndpoint.updateGroupMemberStatus+userId,
      params,
    );
    if (responseData) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async updateMemberRoleGroup( userId, params, callbackFunction) {
    console.log(constants.apiEndpoint.updateRoleGroupMember+userId,params)
    let responseData = await HelperHandle.getInstance().executePut(
      constants.apiEndpoint.updateRoleGroupMember+userId,params,);
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async postProductToGroup(groupId, params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.postProductToGroup.replace('{groupId}', groupId),
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async postPostToGroup( params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.postPostToGroup,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  getGroupPostUrl(item) {
    return `${constants.hostWeb}/groups/${item.groupId}/posts/${item.id}`;
  }


  //new
  async registerGroup(param, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
        constants.apiEndpoint.registerGroup,
        param,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getGroups(params, callbackFunction,withoutToken) {
    let responseData;
    console.log(withoutToken)
    if(withoutToken){
       responseData = await HelperHandle.getInstance().executeGet(
          constants.apiEndpoint.getGroup + '?' +
          GlobalUtil.objToQueryString(params),
      );
    }else
      responseData = await HelperHandle.getInstance().executeGetWithoutToken(
        constants.apiEndpoint.getGroup + '?' +
        GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getGroupsWithoutToken(params, callbackFunction) {
    console.log('lll')
     let responseData = await HelperHandle.getInstance().executeGetWithoutToken(
          constants.apiEndpoint.getGroup +
          '?' +
          GlobalUtil.objToQueryString(params),
      );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async inviteUserName(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
        constants.apiEndpoint.inviteGroup ,params
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
}
