import constants from '../Api/constants';
import {call, put, takeLatest, takeEvery} from 'redux-saga/effects';
import {FAILED, REGISTER, SUCCEEDED} from '../actions/actionTypes';
import HelperHandle from '../Utils/Common/HelperHandle';
import { EventRegister } from "react-native-event-listeners";
let keyCategories = {};
function* register(params) {
  try {
    let responseData = yield HelperHandle.getInstance().executePost(
      constants.apiEndpoint.register,
      params.sort,
    );

    yield put({type: SUCCEEDED, responseData: responseData});
  } catch (err) {
    yield put({type: FAILED, err});
  }
}



export function* watchRegister() {
  yield takeLatest(REGISTER, register);
}
