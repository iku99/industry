import NavigationUtils from '../Utils/NavigationUtils';
import AppConstants from '../resource/AppConstants';
import HelperHandle from '../Utils/Common/HelperHandle';
import constants from '../Api/constants';
import GlobalUtil from '../Utils/Common/GlobalUtil';

let categories = [];
let allListCategories = [];
let keyCategories = {};
export default class CategoryHandle {
  static categoryHandle = null;

  static getInstance() {
    if (CategoryHandle.categoryHandle == null) {
      CategoryHandle.categoryHandle = new CategoryHandle();
    }
    return this.categoryHandle;
  }
  getAllCategory(onCallBack) {
    if (categories.length > 0) {
      return onCallBack(categories);
    } else {
      this.getCategoryMenu({}, (isSuccess, responseData) => {
        if (isSuccess) {
          if (responseData.data != null && responseData.data) {
            let newData = responseData.data;
            allListCategories = newData;
            keyCategories = {};
            categories = this.handleGetCategories(newData);
            onCallBack(categories);
            return;
          }
        }
        onCallBack(categories);
      });
    }
  }

  queryCategory(query, onCallBack) {
    if (categories.length > 0) {
      onCallBack(this.handleSearchLocal(query));
    } else {
      this.getAllCategory(() => onCallBack(this.handleSearchLocal(query)));
    }
  }
  getCategoryByParentId(parentId, onCallBack) {
    if (allListCategories.length > 0) {
      onCallBack(
        allListCategories.filter(
          category => category.parent_id === Number(parentId),
        ),
      );
    } else {
      this.getAllCategory(() => {
        onCallBack(
          allListCategories.filter(
            category => category.parent_id === Number(parentId),
          ),
        );
      });
    }
  }
  goToProductCategoryId(categoryId) {
    this.getCategoryByParentId(categoryId, items => {
      let category = {
        ...keyCategories[categoryId],
        children: items,
      };
      this.goToProductCategoryItem(category);
    });
  }
  goToProductCategoryItem(category) {
    NavigationUtils.goToProductList( category);
  }
  async getCategoryMenu(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGetWithoutToken(
      constants.apiEndpoint.getCategoryMenu + '?' + GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getCategoryChildren(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGetWithoutToken(
        constants.apiEndpoint.getCategoryChildren + '?' + GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getCategoryDetail(id, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGetWithoutToken(
      constants.apiEndpoint.getCategoryDetail + id,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  getChildren(parentId, newData) {
    let result = [];
    let childless = newData.filter(category => category.parent_id === parentId);
    childless.forEach(category => {
      result.push({
        ...category,
        children: this.getChildren(category.id, newData),
      });
    });
    return result;
  }
  handleGetCategories(newData) {
    let mCategories = [];
    newData.forEach(category => {
      let catId = category.id;
      keyCategories[catId] = category;
      if (category.parent_id === 0) {
        //root node
        category = {
          ...category,
          children: this.getChildren(category.id, newData),
        };

        mCategories.push(category);
      }
    });
    return mCategories;
  }
  handleSearchLocal(query) {
    if (query.length === 0) {
      return categories;
    }
    let searchCategories = {};
    let finalResult = [];
    try {
      const regex = new RegExp(`${query.trim()}`, 'i');
      let result = allListCategories.filter(
        category => category.name.search(regex) >= 0,
      );
      result.forEach(item => {
        searchCategories[item.id] = item;
        let mItem = item;
        while (mItem.parent_id !== 0) {
          let parentCategory = keyCategories[mItem.parent_id];
          searchCategories[parentCategory.id] = parentCategory;
          mItem = parentCategory;
        }
      });
      //Format lai du lieu
      Object.keys(searchCategories).forEach(function (key) {
        finalResult = [...finalResult, searchCategories[key]];
      });
      return this.handleGetCategories(finalResult);
    } catch (e) {
      return [];
    }
  }
}
