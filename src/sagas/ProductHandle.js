import HelperHandle from '../Utils/Common/HelperHandle';
import constants from '../Api/constants';
import GlobalUtil from '../Utils/Common/GlobalUtil';

export default class ProductHandle {
  static productHandle = null;

  static getInstance() {
    if (ProductHandle.productHandle == null) {
      ProductHandle.productHandle = new ProductHandle();
    }
    return this.productHandle;
  }

  async getProductCategoryList(params, callbackFunction, withoutToken) {
    let responseData;
    console.log(constants.apiEndpoint.getProductList+'?'+
        GlobalUtil.objToQueryString(params))
    if (withoutToken) {
      responseData = await HelperHandle.getInstance().executeGetWithoutToken(
          constants.apiEndpoint.getProductList+'?'+
          GlobalUtil.objToQueryString(params)
      );
    } else {
      responseData = await HelperHandle.getInstance().executeGet(
          constants.apiEndpoint.getProductList+'?'+
          GlobalUtil.objToQueryString(params)
      );
    }
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async getProductList(params, callbackFunction, withoutToken) {
    let responseData;
    console.log(constants.apiEndpoint.getProductList +
        '?' +
        GlobalUtil.objToQueryString(params),)
    if (withoutToken) {
      responseData = await HelperHandle.getInstance().executeGetWithoutToken(
        constants.apiEndpoint.getProductList +
          '?' +
          GlobalUtil.objToQueryString(params),
      );
    } else {
      responseData = await HelperHandle.getInstance().executeGet(
        constants.apiEndpoint.getProductList +
          '?' +
          GlobalUtil.objToQueryString(params),
      );
    }
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getProductDetail(id, params, callbackFunction) {
     let responseData = await HelperHandle.getInstance().executeGetWithoutToken(constants.apiEndpoint.getProductDetail + id , params);
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async deleteProducts(ids, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeDelete(
      constants.apiEndpoint.deleteProducts+ids
    );
    if (responseData) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async uploadProducts(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.uploadProduct,
      params,
    );
    if (responseData) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async updateProducts(productId, params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePut(
      constants.apiEndpoint.updateProduct + productId,
      params,
    );
    if (responseData) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getProductByIds(params, callbackFunction) {
    let url= constants.apiEndpoint.getProductByIds+'?id='+params.join(',')
    console.log(url)
    let responseData = await HelperHandle.getInstance().executeGet(
        url,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async checkBoughtProduct(productId, userId, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.checkBoughtProduct
        .replace('{productId}', productId)
        .replace('{userId}', userId),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async ratingProduct(productId, params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.ratingProduct.replace('{productId}', productId),
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async likeProduct( params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.likeProduct,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async checkLikeProduct(productId, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.likeProduct.replace('{productId}', productId),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getRatingCount(productId, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getRatingCount.replace('{productId}', productId),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async searchProduct(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
        constants.apiEndpoint.getProductList   + '?' +
    GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async FilterProduct(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
        constants.apiEndpoint.getProductList   + '?' +
        GlobalUtil.objToQueryString(params),
    );
    console.log(constants.apiEndpoint.searchProduct   + '?' +
        GlobalUtil.objToQueryString(params))
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getProductStore(params, callbackFunction) {
    console.log(constants.apiEndpoint.getProductList +'?' +
        GlobalUtil.objToQueryString(params))
      let responseData = await HelperHandle.getInstance().executeGetWithoutToken(
          constants.apiEndpoint.getProductList +'?' +
    GlobalUtil.objToQueryString(params))
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getProductFeature(params, callbackFunction) {
    console.log(  constants.apiEndpoint.getProductFeature  + '?' +
        GlobalUtil.objToQueryString(params))
    let responseData = await HelperHandle.getInstance().executeGet(
        constants.apiEndpoint.getProductFeature  + '?' +
        GlobalUtil.objToQueryString(params)
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getProductProperty(params, callbackFunction) {
    console.log(constants.apiEndpoint.getProductProperty  + '?' +
        GlobalUtil.objToQueryString(params))
    let responseData = await HelperHandle.getInstance().executeGet(
        constants.apiEndpoint.getProductProperty  + '?' +
        GlobalUtil.objToQueryString(params)
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
}
