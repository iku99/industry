import HelperHandle from '../Utils/Common/HelperHandle';
import constants from '../Api/constants';
import GlobalUtil from '../Utils/Common/GlobalUtil';

let shopInfoMap = {};
export default class StoreHandle {
  static mStoreHandle = null;

  static getInstance() {
    if (StoreHandle.mStoreHandle == null) {
      StoreHandle.mStoreHandle = new StoreHandle();
    }
    return this.mStoreHandle;
  }
  async createStore(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
      constants.apiEndpoint.createStore,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async viewStore(id, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePost(
        constants.apiEndpoint.viewStore+id
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async updateStore( params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executePut(
      constants.apiEndpoint.updateStore,
      params,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async getMyStore(callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getMyStore
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getStoreDetail(storeId, callbackFunction) {
    let shopInfo = shopInfoMap[storeId];
    if (shopInfo !== undefined) {
      //Da ton tai trong cache
      callbackFunction(true, shopInfo, '');
      return;
    }
    let responseData = await HelperHandle.getInstance().executeGetWithoutToken(
      constants.apiEndpoint.getStoreDetail + storeId,
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      let shopInfo = responseData.data;
      shopInfoMap[storeId] = shopInfo;
      callbackFunction(true, shopInfo, '');
    } else {
      callbackFunction(false, shopInfo, responseData.errorMsg);
    }
  }

  async getTopStore(params, callbackFunction) {
    console.log(constants.apiEndpoint.getTopStore + '?'+ GlobalUtil.objToQueryString(params))
    let responseData = await HelperHandle.getInstance().executeGet(
        constants.apiEndpoint.getTopStore + '?'+ GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getStoreRateCount(storeId, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getStoreRateCount.replace('{storeId}', storeId),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getStoreRateAvg(storeId, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getStoreRateAvg.replace('{storeId}', storeId),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  async countProductOfStory(storyId, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.countProductOfStore.replace('{storeId}', storyId),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getStoreCategory(storyId, params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getStoreCategory+ storyId +
        '?' +
        GlobalUtil.objToQueryString(params),
    );
    if (GlobalUtil.isRequestSuccess(responseData)) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
}
