import HelperHandle from "../Utils/Common/HelperHandle";
import constants from "../Api/constants";
import GlobalUtil from "../Utils/Common/GlobalUtil";

export default class MediaHandle {
  static mMediaHandle = null;

  static getInstance() {
    if (MediaHandle.mMediaHandle == null) {
      MediaHandle.mMediaHandle = new MediaHandle();
    }
    return this.mMediaHandle;
  }

  createFormData = (photo, body = {}) => {
    const data = new FormData();
    photo?.forEach(element => {
      data.append('ImageUrl', {
        uri: element.path,
        name:
            element.filename ||
            Math.floor(Math.random() * Math.floor(9999999999999)) + '.jpg',
        type: element.mime || 'image/jpeg',
      });
    });
    Object.keys(body).forEach(key => {
      data.append(key, body[key]);
    });
    return data;
  };

  async uploadImageFile(image, onProgress, callbackFunction) {
    let data = new FormData();
    image?.forEach(element => {
      data.append('files', {
        uri: element.item.node.image.uri,
        name: Math.floor(Math.random() * Math.floor(9999999999999)) + '.jpg',
        type: element.mime || 'image/jpeg',
      });
    });
    let url = constants.apiEndpoint.getImage  ;
    HelperHandle.getInstance().executePostUpload(
      url, data,
      progress => {
        onProgress(progress);
      },
      async (isSuccess, responseData) => {
        callbackFunction(isSuccess, responseData._response);
      },
    );
  }
  async uploadImageProduct(image, onProgress, callbackFunction) {
    let data = new FormData();
    image?.forEach(element => {
      data.append('files', {
        uri: element.node.image.uri,
        name: Math.floor(Math.random() * Math.floor(9999999999999)) + '.jpg',
        type: element.mime || 'image/jpeg',
      });
    });
    let url = constants.apiEndpoint.getImage  ;
    HelperHandle.getInstance().executePostUpload(
        url, data,
        progress => {
          onProgress(progress);
        },
        async (isSuccess, responseData) => {
          callbackFunction(isSuccess, responseData._response);
        },
    );
  }
  async deleteImageList(param, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeDelete(
      constants.apiEndpoint.deleteImage,
      param,
    );
    if (responseData) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }
  async getImageList(params, callbackFunction) {
    let responseData = await HelperHandle.getInstance().executeGet(
      constants.apiEndpoint.getImage +
        '?' +
        GlobalUtil.objToQueryString(params),
    );
    if (responseData) {
      callbackFunction(true, responseData, '');
    } else {
      //DucND: Don't something when error. Maybe to kill someone =))
      callbackFunction(false, responseData, responseData.errorMsg);
    }
  }

  uploadProfileAvatar(image, onProgress, callbackFunction) {
    let data = new FormData();
    data.append('file', {
      uri: image.path,
      type: 'image/jpeg', // or photo.type
      name:
          image?.filename ||
          Math.floor(Math.random() * Math.floor(9999999999999)) + '.jpg',

    });
    let url = constants.apiEndpoint.uploadImage;
    HelperHandle.getInstance().executePostUpload(
      url,
        data,
      progress => {
        if (onProgress !== undefined) {
          onProgress(progress);
        }
      },
      async (isSuccess, responseData) => {
        if (callbackFunction !== undefined) {
          callbackFunction(isSuccess, responseData);
        }
      },
    );
  }
}
